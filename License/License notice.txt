This project include files from other projects:
Sol, a library to easily bind c++ with LUA code (See https://sol2.readthedocs.io/en/latest/ or https://github.com/ThePhD/sol2) under MIT license.
CppOptimizationLibrary, a library with some solver to search local minimum of a function (See https://github.com/PatWie/CppNumericalSolvers) under MIT. This has been modified a bit to fit the needs of MERCI.
JSON a C++ interface to json (See https://github.com/nlohmann/json) under MIT license
RomainClotho to simulate clothoids (See http://www.inrialpes.fr/bipop/people/casati/publications/codes/ssc.html) under GPL

Most files of the project are licensed under CeCILL v2.1
A copyright note is included in the file when this is the case.
