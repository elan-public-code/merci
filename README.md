```
  __  __ ______ _____   _____ _____ 
 |  \/  |  ____|  __ \ / ____|_   _|
 | \  / | |__  | |__) | |      | |  
 | |\/| |  __| |  _  /| |      | |  
 | |  | | |____| | \ \| |____ _| |_ 
 |_|  |_|______|_|  \_\\_____|_____|
                                    
```                                   
Minimisation de l'Energie des Rubans en Courbures et Inextensibles

The source code MERCI is the official implementation of the chained and mixed ribbon models, these two algorithms being described in the two following publications :

- R. Charrondière, F. Bertails-Descoubes, S. Neukirch, V. Romero, "Numerical modeling of inextensible elastic ribbons with curvature-based elements", CMAME 2020 (https://inria.hal.science/hal-02515877v2)

- R. Charrondière, S. Neukirch, F. Bertails-Descoubes, "MERCI: Mixed curvature-based elements for computing equilibria of
thin elastic ribbons", to appear in ACM Trans. on Graphics in 2024.

# Authors

- Raphaël Charrondière (designer, code developer)

- Sébastien Neukirch and Florence Bertails-Descoubes (advisers)

# Affiliations

- UGA, Inria, Sorbonne Université

# License

MERCI is freely distributed under the CeCILL license (https://cecill.info/). Please check out the license terms in the repository `License`.

# Code usage

A minimalistic (in progress) documentation and installation guide (in English and French) is available in the repository `code`, see `Documentation-en-install+lua.md` and `Documentation-fr-install+lua.md`, respectively.

# References and contact

When using this code for your research or any other project, please acknowledge the code by citing the above papers for any result you may release (publication, report, presentation, etc.).

Authors are happy to receive any suggestion for improving the code, please do not hesitate to send feedback at:

florence.descoubes@inria.fr, sebastien.neukirch@cnrs.fr


