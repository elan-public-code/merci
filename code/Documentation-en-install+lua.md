```
  __  __ ______ _____   _____ _____
 |  \/  |  ____|  __ \ / ____|_   _|
 | \  / | |__  | |__) | |      | |  
 | |\/| |  __| |  _  /| |      | |  
 | |  | | |____| | \ \| |____ _| |_
 |_|  |_|______|_|  \_\\_____|_____|

```                                   
Minimisation de l'Energie des Rubans en Courbures et Inextensibles            

# About MERCI
MERCI is a C++/lua software for computing the statics of thin elastic ribbons discretised with curvature-based elements. It is based on the super-ribbon model described in [Charrondière et al. 2020, Charrondière et al. 2022], and relies on the free [IPOPT](https://coin-or.github.io/Ipopt/) optimisation software (coinor project) for the static solver. The ribbon can be clamped at one or both ends, and even closed. Contact is treated by contraints with planes. Once the setup is defined, the equilibrium of the ribbon under the specified boundary conditions, external forces, and constraints, is computed. MERCI can be used as a C++ library, or via its lua interface.

MERCI is currently only distributed by its original authors, through the license agreement `MERCI_SOFTWARE_LICENSE_AGREEMENT_CURRENT.pdf` available in the main repository. It can be used solely for academic research, and should not be distributed by external users.

# Installation
Note that the code has been tested on Macosx 10.13 and Linux. It has been successfully run on the following distributions: Ubuntu (version 20.04), Fedora (version 33,34), Archlinux.

Although we did not try to compile our code on Windows, the compilation and usage of MERCI on Windows should be possible. However, it might be a bit tricky to get a working version of all the libraries. It can be sometimes difficult to compile libraries, but compiled versions can be found online. The [releases page of IPOPT](https://github.com/coin-or/Ipopt/releases) can provide compiled versions of IPOPT for Visual Studio 2019. If you manage to have MERCI work on Windows, we would be happy to hear about it.

1. Prerequisite
  - You need a C, C++, fortran compiler (e.g. gfortran, gcc/g++, clang), CMake, CMakeGui or CCMake
  - Install the liblua developpement package (e.g. liblua5.3-dev for Ubuntu)
  - Install Eigen 3 (e.g. libeigen3-dev for Ubuntu)
  - Get a working version of IPOPT (https://github.com/coin-or/Ipopt/releases) and install IPOPT. Be sure to have a recent package, version >= 3.12. For manual installation you can use the tool named coinbrew, which is easy to install with all dependencies. In case of an older version, e.g. provided by your Ubuntu distribution (for instance the 3.11 version on Ubuntu 20), you may have compilation errors that can however be fixed manually (see below).
  - *Optional* Install CMinPack


2. Compilation
  - Create a build directory, go to it and run cmake:
    > mkdir build

    > cd build

    > cmake ..
  - Set some cmake options (either using `cmake-gui` or by directly editing the file `CMakeCache.txt`):
    + Set CMAKE_BUILD_TYPE to RelWithDebInfo
    + Check LUA_INTERFACE and USE_IPOPT
    + *Optional* Check USE_CMINPACK if you installed it
    + Be sure IPOPT_DIR all path are set (e.g. no xxx_NOT_FOUND in paths)
  - Rerun cmake, then type make:
    > cmake ..

    > make -j 4

If you are using a version of IPOPT older than 3.12, you may have this compilation error: `#error "don't have header file for stddef"`. In that case, a simple fix is to encapsulate the include lines to IPOPT headers between the tags `#define HAVE_CSTDDEF` and `#undef HAVE_CSTDDEF`. For instance, replace the code
```
#include <IpTNLP.hpp>```
in `src/IpoptSolvers/IpoptMixedRibbon.hpp` with
```
```
#define HAVE_CSTDDEF
 #include <IpTNLP.hpp>
#undef HAVE_CSTDDEF
```
This fix has been successfully tested on Ubuntu 20.

3. Installation and quick start

  - You can install MERCI by typing make install, but you also can use the executable from the build directory (folder build/src/LuaInterface), as

> cd src/LuaInterface

> ./MERCI ../../../experiments/testInput.lua

**Warning**, if you did not build using CMinPack, you will see an error message when running MERCI, however, you can ignore it.

For this example, a mesh file named `testMesh.obj`, together with a material file `testMesh.mtl` will be generated as an output. The obj file concatenes the initial (undeformed) ribbon configuration together with the final (equilibrium) configuration. You can display these two configurations using an obj viewer, for instance `meshlab` or `paraview`:

> meshlab testMesh.obj

You should see a flat (cyan and yellow) ribbon, which corresponds to the initial ribbon configuration, and a curly (red and blue) ribbon, which corresponds to the equilibrium found under gravity. The colors indicate the discretisation of each ribbon into elements (20 elements here).

For all possible (input and output) options of the software, please refer to the extensive documentation below.

# Super ribbons: script input documentation
The scripting system of the simulator relies on the lua language. It allows the user to do many experiments with only few efforts. For example, the user can test the impact of a variable with a single loop.

## Script files
The script files use lua language. Once written, the script have to be executed with the `./MERCI script.lua` command. The base, io, table, string and the math packages are available. You find demo scripts in the ribbon git (folder experiments), testInput.lua is an easy exemple.

## List of classes

### SuperRibbon

This class defines the parameters of a (chained) ribbon and its state.

**Members:**
* `width`          *(double)*                 The width of the ribbon.
* `thickness`      *(double)*                 The thickness of the ribbon, note this is not a physical parameter but is only used by the mesher class.
* `areaDensity`    *(double)*                 The area density of the ribbon.
* `D`              *(double)*                 The bending modulus, also defined as  $`\frac{Yh^3}{12(1-\nu^2)}`$ with $`Y`$ the Young modulus, $`\nu`$ the Poisson ratio, and $`h`$ the thickness of the ribbon.
* `position`       *(array of three doubles)* The position of the extremity of the ribbon.
* `frame`          *(array of nine doubles)*  The frame of the extremity of the ribbon, written row by row. The second column correspond to the normal of the surface, the third to the tangent of the midline, and the first is implied by the structure of frame, which belongs to $`\mathcal{SO}_3`$.
* `poissonRatio`   *(double)*                 The Poisson ratio $`\nu`$ of the ribbon.
* `numberSegments` *(int)*                    The number of segments in the ribbon. If it decreases, only the information about the first `numberSegments` segments will be kept. If it increases, new empty segments will be paddled.

**Methods:**
* `setRibbonStart(double omega, double eta)`                                     define $`\omega_1(0)`$ and  $`\eta(0)`$.
* `setRibbonPiece(int index, double length, double omegaPrime, double etaPrime)` for the segment with index `index` (indexing from 0), set the length of the segment and the constants $`\frac{d\omega_1(s)}{ds}`$, $`\frac{d\eta(s)}{ds}`$.
* `setNaturalCurvature(int index, double A, double B)`                           set the normal natural curvature along the segment `index` with the local polynomial $`AX+B`$.
* `copy()`                                                                       return a copy of the ribbon.
* `randomizeShape()`                                                             generate a random initial state of the ribbon
* `setRibbonPieceCurvature(int index, double omegaPrime, double etaPrime)`       same as `setRibbonPiece` without changing the length of the segment.
* `healthRepport()`                                                              print active constraints and violations on the terminal
* `array getPoint()`                                                             return the point of the ribbon
* `setPoint(array)`                                                              set the point of the ribbon
* `endPos()`                                                                     return the position at the end of the ribbon
* `endFrame()`                                                                   return the frame at the end of the ribbon
* `genDiscreteDescriptor(double prec, string filename)`                          generate some summary in JSON format file
* `printPoint()`                                                                 print the vecteur returned by getPoint()

### MixedSuperRibbon

This class defines the parameters of a mixed ribbon and its state.

**Constructors**

* `MixedSuperRibbon()`
* `MixedSuperRibbon(SuperRibbon)`
* `MixedSuperRibbon(filename)`

The ribbon can be build from a SuperRibbon if needed.

**Members:**
* `width`          *(double)*                 The width of the ribbon.
* `thickness`      *(double)*                 The thickness of the ribbon, note this is not a physical parameter but is only used by the mesher class.
* `areaDensity`    *(double)*                 The area density of the ribbon.
* `D`              *(double)*                 The bending modulus, also defined as  $`\frac{Yh^3}{12(1-\nu^2)}`$ with $`Y`$ the Young modulus, $`\nu`$ the Poisson ratio, and $`h`$ the thickness of the ribbon.
* `position`       *(array of three doubles)* The position of the first segment of the ribbon.
* `frame`          *(array of nine doubles)*  The frame of the first segment of the ribbon, written row by row. The second column correspond to the normal of the surface, the third to the tangent of the midline, and the first is implied by the structure of frame, which belongs to $`\mathcal{SO}_3`$.
* `poissonRatio`   *(double)*                 The Poisson ratio $`\nu`$ of the ribbon.
* `numberSegments` *(int)*                    The number of segments in the ribbon. If it decreases, only the information about the first `numberSegments` segments will be kept. If it increases, new empty segments will be paddled.

**Methods:**
* `setOmega1(int index, double a, double b)`                                     set the normal curvature of the segment index as $`ax+b`$
* `setNatOmega1(int index, double aNat, double bNat)`                            set the **natural** normal curvature of the segment index as $`a_{Nat} x+b_{Nat}`$
* `setEta(int index, double n, double m)`                                        set the etaof the segment index as $`nx+m`$
* `setMiniSys(int index, double a, double b, double n, double m)`                set the normal curvature and eta of the segment index
* `setMiniSys(int index, array of 4 double)`                                     version with an array
* `setLength(int index)`                                                         set the length of the segement index
* `setPosStart(int index, array of 3 doubles)`                                   set the starting position of segement index
* `setFrameStart(int index, array of 9 doubles)`                                 idem for frame
* `array of 2 double getOmega1(int index)`                                       get the normal curvature of the segment index
* `array of 2 double getNatOmega1(int index)`                                    get the natural normal curvature of the segment index
* `array of 2 double getEta(int index)`                                          get the eta of the segment index
* `array of 4 double getMiniSys(int index)`                                      get the (a,b,n,m) of the segment index (see above)
* `double getLength(int index)`                                                  getter
* `array of 3 doubles getPosStart(int index)`                                    getter
* `array of 9 doubles getFrameStart(int index)`                                  getter
* `reChain()`                                                                    match the start of a segement with the end of the previous segement.
* `endPos(int index)`                                                            return the position at the end of the segment of specified index
* `endFrame(int index)`                                                          return the frame at the end of the segment of specified index
* `copy()`                                                                       return a copy the ribbon
* `genDiscreteDescriptor(double prec, string filename)`                          generate some summary in JSON format file
* `saveToFile(string filename)`                                                  generate a file to save the ribbon


### Fibre

This class defines the parameters of a fiber (superclothoid) and its state.

**Members:**
* `position` *(array of three doubles)* The position of the extremity of the fiber.
* `frame` *(array of nine doubles)*  The frame of the extremity of the fiber, written row by row. It belongs to $`\mathcal{SO}_3`$.
* `linearDensity`  *(double)*                 The linear density of the fiber.
* `crossSection` *(array of two doubles)* The two diameter of the elliptic cross section of the fiber.
* `numberSegments` *(int)*                    The number of segments in the fiber. If it decreases, only the information about the first `numberSegments` segments will be kept. If it increases, new empty segments will be paddled.
* `point` *(vector of double)*                    The point of the system. The point is the initial omega, followed by derivative of omega for each segment.


**Methods:**
* `setElasticProperties(array of nine doubles)` Define the matrix of the elastic energy model
* `setElasticProperties(double YoungModulus, double poissonRatio, double width, double thickness)` Compute and set the matrix from the given parameters.
* `setElasticPropertiesRect(double YoungModulus, double poissonRatio, double width, double thickness)` Compute and set the matrix from the given parameters with a rectangular cross section.
* `getElasticMatrix()` Get the matrix of the elastic energy model
* `setLengthSegment(int segmentID, double length)` Explict name
* `setOmegaInit(double, double, double)` Explict name
* `setNaturalOmegaInit(double, double, double)` Explict name
* `setOmegaDerivative(int segmentID, double, double, double)` Explict name
* `setNaturalOmegaDerivative(int segmentID, double, double, double)` Explict name
* `endPos()`                                                                     return the position at the end of the fibre
* `endFrame()`                                                                   return the frame at the end of the fibre
* `copy()` Return a copy of the fiber.


### Options

This class allows to specify some parameters for IPOPT (Do not change the members, if you do not know how to set them).

**Members:**
* `checkDerivation` *(bool)*   Used to check the validity of the gradient and hessian, default is false.
* `maxIters`        *(int)*    The maximum number of iterations of IPOPT, default is 10000.
* `tol`             *(double)* The desired convergence tolerance, default is 1.e-12.
* `hessianApprox`   *(bool)*   Use LBFGS instead of the hessian, default is false.
* `limitedMemoryMaxHistory` *(int)*   History size for LBFGS, default is 6.
* `outputFile`      *(string)* The file where IPOPT will save the log output, default is "" meaning there is no outputed file.
* `logLevel`        *(int)* The print_level option of ipopt (0-12), default is 5
* `warmstartFromPrevious`   *(bool)* Use previous lagrange multipliers (also on variables bounds), do not set to true the first time (default is false) !

### HybridParams

This class allows to specify some parameters for the hybrid code from cminpack. All values has a default initialisation

**Members:**
* `xtol`   *(double)* relative error under which : convergence
* `maxfev` *(int)*    max number of iterations (evaluation of f)
* `epsfcn` *(double)* to compute the numerical derivate;
* `factor` *(double)* factor of the box to prevent jumping
* `nprint` *(int)*    =3 si on print les variables toutes les 3 iter. (code iflag dans fcnsho)

### IPOPTStats

This class containts computations of IPOPT

**Members:**
* `iterations`             *(int)* number of evaluations in IPOPT
* `time`                   *(double)* time to run
* `evalObjective`          *(int)* number of evaluations of the objective
* `evalGradientObjective`  *(int)* number of evaluations of the gradient of the objective
* `evalConstraints`        *(int)* number of evaluations of the constraints
* `evalJacConstraints`     *(int)* number of evaluations of the jacobian of the constraints
* `evalHessian`            *(int)* number of evaluations of the hessian of Lagrangian
* `dual_inf`               *(double)*
* `constr_viol`            *(double)*
* `complementarity`        *(double)*
* `ktt_error`              *(double)*
* `dual_inf_scaled`        *(double)*
* `constr_viol_scaled`     *(double)*
* `complementarity_scaled` *(double)*
* `ktt_error_scaled`       *(double)*
* `lambda`                 *(array of double)* lagrange multiplier
* `repulsionFields`        *(array of double)* energy of each rf
* `status`                 *(string)* final status of the solver

### Pressure

This class act as a wall for the ribbon, except it can be pushed like a spring to measure forces

**Constructors:**
* `PressureStdInterface(std::array<double, 3>, std::array<double, 3>, double)`
* `PressureStdInterface(std::array<double, 3>, std::array<double, 3>, double, double)`

**Members:**
* `position` *(double)* rest position of the plate
* `normale` *(array of double)* normale of the plate, should point out
* `k` *(double)* spring stiffness coefficient
* `distance` *(array of double)* distance from actual to rest position
* `doubleEnd` *(bool)* true if act on ribbon borders (default), false if acts on midline.

**Methods:**

### RepulsionField

This class act as repulsion "wall" for the ribbon acting like an energy.

**Constructors:**
* `RepulsionField(std::array<double, 3>, std::array<double, 3>, double)` position, normale
* `RepulsionField(std::array<double, 3>, std::array<double, 3>, double, double)` position, normale, intensity

**Members:**
* `k` *(double)* intensity
* `position` *std::array<double, 3>*
* `normale` *std::array<double, 3>*

**Methods:**
* `setField` *(int or string)* energy function
* `printFieldChoices` *(void)* as name states

### Experiment

This class is intended for setting all the parameters and compute the equilibrium of a (chained) ribbon.

**Members:**
* `gravity` *(array of three doubles)* The gravity vector $`\mathcal G`$.
* `name`    *(string)*                 A name for the experiment (This is actually useless).
* `ribbon`  *(SuperRibbon)*            The ribbon you are working with.
* `options` *(Options)*                The options for the algorithm.
* `stats`   *(IPOPTStats)*             The stats

**Methods:**
* `energy()`          Get the energy of the ribbon.
* `gradientNorm()`    Get the norm of the gradient of the energy of the ribbon.
* `findEquilibrium()` IPOPT is called to minimize the energy.
* `findEWB`           IPOPT is called to minimize the energy, but the bounds of the variables are not set.
* `newton()`          The Newton algorithm is called to perform the minimization, and there is no bound check
* `newtonC()`         The Newton-Gradient algorithm is called to perform the minimization, a regularization is used, there are bounds. Newton is used to compute the search direction, if this it a concave direction, the algorithm switch to a gradient descent step.
* `newtonR()`         The Newton algorithm is called to perform the minimization, and there is no bound check. There is a line search. The hessian is regularized if the minimum eigen value is small or negative.
* `gradientDescent()` The gradient descent algorithm is called to perform the minimization there is no bound check
* `BFGS()`            The BFGS algorithm is called to perform the minimization there is no bound check
* `runHybrid(HybridParams)` Call hybrid code from CMinPack
* `attachEnd(array pos, array frame)`       Fix the end of the ribbon, but this is only used by IPOPT
* `detachEnd()`       The extremity is free (by default it is true)
* `use3dotAlignementConstraint(bool)`       Use the 3dof constraint for the frame (defaut is false)

### MRFitter

This class is intended for setting all the parameters and compute the equilibrium of a ribbon.

**Members:**
* `gravity` *(array of three doubles)* The gravity vector $`\mathcal G`$.
* `name`    *(string)*                 A name for the experiment (This is actually useless).
* `ribbon`  *(MixedSuperRibbon)*       The ribbon you are working with.
* `options` *(Options)*                The options for the algorithm.
* `oriIsConstraint` *bool*             The clamping at origine can be considered as a constraint (is the default behaviour), or the DOF and constraints can be removed from the system.
* `stats`   *(IPOPTStats)*             The stats

**Methods:**
* `clampRuling(bool start, bool end)`       Fix eta(0) to zero and/or eta(L) to zero.
* `energy()`          Get the energy of the ribbon.
* `gradientNorm()`    Get the norm of the gradient of the energy of the ribbon.
* `findEquilibrium()` IPOPT is called to minimize the energy.
* `attachEnd(array pos, array frame)`       Fix the end of the ribbon, but this is only used by IPOPT.
* `detachEnd()`       The extremity is free (by default it is true).
* `int addOffPlane(array point, array normale)` Add a forbbiden zone to the ruban, the frontier is a plane defined by a point (any point belonging to the plan) and it's normale, point outwards the forbbiden zone. Consider only the midline. A number is returned, to change this plane later (see changeOffPlane).
* `int addOffPlaneDoubleEnd(array point, array normale)` Add a forbbiden zone to the ruban, the frontier is a plane defined by a point (any point belonging to the plan) and it's normale, point outwards the forbbiden zone. Consider the borders of the ribbon. A number is returned, to change this plane later (see changeOffPlaneDoubleEnd).
* `changeOffPlane(int at, array point, array normale)` Change the frontier of a forbbiden zone, at value is given by the addOffPlane function, please note, it is independent of the DoubleEnd variant. Return false if it fails (incorrect at value)
* `changeOffPlaneDoubleEnd(int at, array point, array normale)` Change the frontier of a forbbiden zone, at value is given by the addOffPlaneDoubleEnd function, please note don't you can't change an OffPlane to an OffPlaneDoubleEnd or reverse. Return false if it fails (incorrect at value)
* `clearOffPlanes()` As the name tells, reverse all calls to addOffPlane and addOffPlaneDoubleEnd.
* `int addPressure(Pressure)` Act like with offPlanes.
* `changePressure(int at, Pressure)` Act like with offPlanes.
* `getPressure(int at)` AIR.
* `detachOrClamping()` Remove the clamping at origin of the ribbon, bevare oriIsConstraint will be set to true each time you optimise (this boolean has no meaning if this method is called anyway).
* `reatachOrClamping()` Reverse of previous function.
* `useLooseClamping(array pos, double radius)` Place clamping point at origin in a sphere given by the parameters, it's only usefull if detachOrClamping is also called.
* `disableLooseClampin()` Reverse of previous function.
* `declareRibbonCyclic(bool value)` Make a loop of the ribbon. Beware this wont have any effect if you didn't call detachOrClamping before.
* `declareEtaCyclic(bool negate)` Force eta(0)=eta(L) or if negate is true eta(0)=-eta(L), cancel previous call to clampRuling, and is cancelled by any call to the clampRuling function.
* `extractLambdaOffPlane(int at)` return the Lagrangian multiplier for the choosen OffPlane constraint (the at value correspond to the value returned by the add function)
* `extractLambdaOffPlaneDoubleEnd(int at)` return the Lagrangian multiplier for the choosen OffPlaneDoubleEnd constraint (the at value correspond to the value returned by the add function)
* `extractLambdaPressure(int at)` return the Lagrangian multiplier for the choosen pressure constraint (the at value correspond to the value returned by the add function)
* `disableRibbonBounds()` disable bounds on the variables, however **Use only with Sadowsky** or the energy might diverge
* `reenableRibbonBounds()` reverse of disableRibbonBounds
* `alignBarycenterOnAxis(array position, array direction)` The barycenter of the object must be align with axis specified by his position and direction. Only works if the clamping at origin is detached
* `detachBarycenterFromAxis()` reverse from previous function.

### FExperiment

This class is intended for setting all the parameters and compute the equilibrium of a fiber.

**Members:**
* `gravity` *(array of three doubles)* The gravity vector $`\mathcal G`$.
* `name`    *(string)*                 A name for the experiment (This is actually useless).
* `fibre`  *(Fibre)*            The ribbon you are working with.
* `options` *(Options)*                The options for the algorithm.

**Methods:**
* `energy()`          Get the energy of the ribbon.
* `gradientNorm()`    Get the norm of the gradient of the energy of the ribbon.
* `findEquilibrium()` IPOPT is called to minimize the energy.
* `newton()`          The Newton algorithm is called to perform the minimization, and there is no bound check
* `newtonC()`         The Newton-Gradient algorithm is called to perform the minimization, a regularization is used. Newton is used to compute the search direction, if this it a concave direction, the algorithm switch to a gradient descent step.
* `newtonR()`         The Newton algorithm is called to perform the minimization, and there is no bound check. There is a line search. The hessian is regularized if the minimum eigen value is small or negative.
* `gradientDescent()` The gradient descent algorithm is called to perform the minimization there is no bound check
* `testGradient()` (Only for debuging) Test the gradient of the energie by finite differences
* `testHessian()` (Only for debuging) Test the hessien of the energie by finite differences

### Arrow

This class allows to define an arrow mesh

**Members:**
* `name`         *(string)*   Name the arrow
* `position`     *(array of three doubles)*    The position of the end point of the arrow (flat side).
* `direction`    *(array of three doubles)*    The direction of the needle (length is proportional to the norm)
* `resolution`   *(int)*                       Number of faces on the tube
* `length`       *(double)*                    Length of the body of the arrow
* `lengthHead`   *(double)*                    Length of the head of the arrow
* `diameter`     *(double)*                    Diameter of the body of the arrow
* `diameterHead` *(double)*                    Diameter of the head of the arrow
* `color`     *(array of three int)*        Color to use.

**Methods:**
* `setupDimensions(double length, double lengthHead, double diameter, double diameterHead)` Set the dimensions of the arrow

### PlaneMesh

This class allows to define a plane mesh

**Constructor:**
* `PlaneMesh()`
* `PlaneMesh({double,double,double} pos, {double,double,double} normale)`

**Members:**
* `position` center of the plane.
* `normal` normal of the plane.
* `size` width of the plane.
* `thickness` a second plane is placed under the first to avoid invisibility when looked from the back.
* `color` as name states.

**Methods:**
* `place({int,int,int} pos, {int,int,int} normale)` shortcut to set position and normal

### Mesher

This class is intended to generate OBJ's for a visual result.

**Members:**
* `precision`   *(double)*              Minimum resolution of the mesh.
* `mainColor`   *(array of three ints)* Color of even segments.
* `secondColor` *(array of three ints)* Color of odd segments.
* `ribbonName`  *(string)*              Name of the ribbon object

**Constructor:**
* `Mesher(string objfilename)`

**Methods:**
* `setColors({int,int,int} mainColor, {int,int,int} secondColor)` Set both colors of the ribbon at once.
* `add(SuperRibbon ribbon)` Add the ribbon shape to the OBJ file, using the last defined colors.
* `add(SuperRibbon ribbon, true)` Show the subsegments instead of the segments.
* `add(MixedSuperRibbon)` Add the ribbon shape to the OBJ file, using the last defined colors.
* `add(Fiber fiber)` Add the fiber shape to the OBJ file, using the last defined colors.
* `add(Arrow arrow)` Add the arrow shape to the OBJ file.
* `add(Plane plane)` Add the phane shape to the OBJ file.
* `close()` Close the obj file, other call to add function will be useless.

### Image

This class is intended to generate OBJ's for a visual result.

**Members:**
* `width`   *(double)*              The width of the image.
* `height`  *(double)*              The height of the image.

**Constructor:**
* `Image(unsigned int w, unsigned int h)` Construct an image of size wxh

**Methods:**
* `setPixel(unsigned int x, unsigned int y, (array of three unsigned ints) color)` Set the colors of the pixel (x,y) to color.
* `(array of three unsigned ints) getPixel(unsigned int x, unsigned int y)` Get the color of the pixel (x,y) of the image.
* `save(string filename)` Save the image as ppm


### Functions
* `IChooseSadowsky()` Switch to Sadowsky model. By default Wunderlich is used.
* `IChooseWunderlich()` Switch to Wunderlich model. Used by default.
* `IChooseRibext()` Switch to Ribext model. By default Wunderlich is used.
* `IChooseSadowskyFiber()` Switch to Sadowsky model with fiber energy (remove the squared eta term). By default Wunderlich is used.
* `IChooseWunderlichFiber()` Switch to Wunderlich model with fiber energy (remove the squared eta term). By default Wunderlich is used.
* `selModel(string model)` Choose model based on name, option are Wunderlich, Sadowsky, WunderlichFiber, SadowskyFiber, Ribext. See previous functions for details.
* `setEtaPrimeRegularizationFactor(double)` Change regularisation factor to bending energy $`K\int \eta'(s)^2ds`$. Initially this factor is null.
* `getEtaPrimeRegularizationFactor(double)` As the name states (see setter function).
* `ExperimentalyToggleSubdivision(bool)` If you want to use disable the subdivion or reenable it. By default it is on.
* `ExperimentalySetSeriesOrder(optional int)` As the name is saying, no argument set the value to needed value.
* `MRCoeffs(double pos, double frame)` Set coefficients of the constraints in the mixed code (same as calling the two functions bellow).
* `MRLinkCoeffs(double pos, double frame)` Set coefficients of the constraints in links beetween segments in the mixed code.
* `MREndCoeffs(double pos, double frame)` Set coefficients of the constraints for end clamping in the mixed code.
* `MRBarycenterCoeff(double a)` Set coefficient of the constraint for aligning the barycenter of the ribbon on the specified axis.
