Documentation développeur sur MERCI
=======================


```
  __  __ ______ _____   _____ _____ 
 |  \/  |  ____|  __ \ / ____|_   _|
 | \  / | |__  | |__) | |      | |
 | |\/| |  __| |  _  /| |      | |
 | |  | | |____| | \ \| |____ _| |_ 
 |_|  |_|______|_|  \_\\_____|_____|
                                    
```
##Minimisation de l'Energie des Rubans en Courbures et Inextensibles            

**Préambule :** Cette documentation est à usage du developpeur et non de l'usager. Elle concerne le code C++. Elle n'a pas pour but d'être exhaustive, mais de donner une vision d'ensemble au developpeur. Les noms des fonctions sont explicites, cependant les notations ne le sont pas forcément. Il est donc nécessaire de se familliariser avec celles-ci.

# Installation

1. Prérequis
  - Un compilateur C, C++, fortran (par exemple gfortran, gcc/g++, clang). 
  - CMake avec optionnellement une interface graphique telle que CMakeGui or CCMake.
  - La bibliothèque lua (liblua5.3-dev ou similaire)
  - La bibliothèque Eigen 3 (libeigen3-dev ou similaire)
  - La bibliothèque IPOPT, version >= 3.12 (attention certains OS n'ont pas de paquets à jour, un hotfix peut être utilisé comme indiqué dans la documentation en anglais, à utiliser à vos risques et périls). L'éditeur COIN fourni un outil très facile d'utilisation nommé coinbrew pour compiler/installer IPOPT avec les dépendances requises.
  - *Optionel* La bibliothèque CMinPack (non utile pour une utilisation standart, cette extension n'est là que pour reproduire les comparaisons de l'article Charrondière at al. CMAME 2020). Un avertissement vous indique si CMinPack est chargé ou non en éxécutant MERCI.

2. Compilation
  - Créer un dossier build et y éxécuter cmake (de préférence utiliser cmake-gui, ccmake pour éditer les options) `mkdir build && cd build && cmake ..`
  - Les options doivent être spécifiées : 
    + Pour CMAKE_BUILD_TYPE mettre RelWithDebInfo ou Release (Le mode Debug est bien trop lent pour un code fonctionnel)
    + Au besoin, si IPOPT a été compilé à la main, compléter IPOPT_DIR
    + Cocher LUA_INTERFACE pour profiter de l'interface lua (Très recommandé), sinon MERCI ne sera disponible que sous forme de bibliothèque C++
    + *Optionel* Cocher USE_CMINPACK pour tester libHybrid (Seulement utile pour des comparaisons d'algo avec le modèle SuperRibbon), la bibliothèque correspondante doit être installée
    + Compléter tout les chemins si besoin.
    + [Déprécié, Non documenté] Cocher RIBBON_SIMPLE_CORE pour la fonctionnalité de rubans à courbure constante par morceaux
    + [Non documenté] Cocher ROMAIN_CLOTHOIDS pour compiler le code des super-clothoides de Roman Casati
    + [Très incomplet, Non documenté] Cocher RIBBON_DYNAMIC pour utiliser les rubans avec un système pseudo-dynamique. Le code correspondant ne devrait pas être utilisé car incomplet
   - Reexecuter `cmake` jusqu'à ne plus avoir d'erreur (corriger les erreurs), puis éxécuter `make`
   - L'instruction `make install` permet d'installer MERCI en temps que bibliothèque
  
**Attention**, si l'extension CMinPack n'est pas installée un message indiquera que l'extension est manquante, vous pouvez alors ignorer ce message.

MERCI a été compilé et testé avec succès sous macosx 10.13,  Ubuntu (version 20.04), Fedora (version 33,34), Archlinux.


# Nommage et conventions

Le nommage des variables est cohérent avec la thèse Modélisation numérique de rubans par éléments en courbures (R. Charrondière).
Comme son nom l'indique MERCI est un code de simulation du rubans. Il fonction en mode statique.

Les rubans simulés sont rectangulaires d'épaisseur constante.
La courbure normale est nommée omega1, c'est une fonction linéaire omega1(s)=as+b.
La direction de la regle eta est une fonction linéaire eta(s)=ns+m.
L'abscisse linéaire est nommé s.
D=Yh^3^ /(12(1-poissonratio^2^)) est la rigidité de flexion. Elle est constante.

Note: AreaDensity veut dire masse surfacique pour l'auteur de code.

# Structure

MERCI est divisée en plusieurs «sous bibliothèques», chacune dans un répertoire différent. Certaines sont incomplètes ou obsolètes. On les liste ci-dessous :

   - Log : Comme son nom l'indique cette bibliothèque permet de créer des logs. Les logs sont des traces générées par l'application pour debugger facilement. Malgré son utilité, MERCI utilise très peu de logs.
   - ObjLib : Cette bibliothèque s'occupe de l'enregistrement de maillaiges 3D sous le format OBJ. 
   - RibbonObjLib : Cette bibliothèque fournit des classes d'aides pour ObjLib. Entre autre elle permet de traiter facilement certaines forme géométrique pour éviter la génération de maillage.
   - FibreObjLib : très similaire à RibbonObjLib mais pour les fibres (non documenté, intuitif après lecture de la doc de RibbonObjLib)
   - cppoptlib : Cette bibliothèque est reprise de la bibliothèque du même nom
   - BaseCore : Bibliothèque servant de base commune à RibbonCore, SuperClothoidCore, MixedSRCore
   - Hybrid : interface pour l'extension CMinpack, non documenté et à priori non utile
   - RibbonCore : embryonnaire (=incomplet mais fonctionnel) et non recommandée, bibliothèque pour les rubans en courbure constante par morceaux.
   - SuperRibbonCore : bibliothèque pour implémenter le modèle super rubans
   - MixedSRCore : le pendant de SuperRibbonCore pour le modèle mixte
   - IpoptSolvers : cette bibliothèque fait une première interface rapide avec Ipopt, elle servira de base pour décrire un problème Ipopt
   - SuperClothoidCore : comme pour superRuban mais avec des tiges.
   -  : interface pour LUA
   - GeneratedDynamic : à supprimer
   - PseudoDyn : non concluant sur la dynamique
   - SuperRibbonDynamite : idem
   - TestDynamite : encore des tests sur la dynamique
   
   
InterfaceAUTO, CodeFortran


### Log 

Cette bibliothèque est conçu pour sortir les logs sur plusieurs support de manière facile. La classe centrale `Logger` est un singleton, c'est-à-dire qu'un seul objet de ce type existe. On y accède via la fonction `getLogger()`. On pourra alors en utilisant la méthode adéquate choisir la sortie entre Console, fichier, fichier en format JSON, rien (BlackHole). Par defaut la sortie est la console. Une fois la sortie choisie, la seule methode pour écrire des données est `Write(level, moduleName, msg)`. Level est le niveau critique (est-ce un message d'info, un avertissement, etc.). L'argument moduleName devrait désigner la bibliothèque/classe qui donne le message, mais l'utilisateur peut mettre ce qu'il veut. Le message a une structure d'arbre. On commence d'abord par créer le message, puis on peut ajouter des données si besoin, comme le montre l'exemple.

```c++
LoggerMsg msg("Récapitulatif état");
msg.addChild("Mode","DIRECT");
auto smsg=msg.addChild("Système","courant");
smsg->addChild("position",x);
smsg->addChild("vitesse",v);
```
`addChild(nom, valeur)` renvoie un pointeur vers le sous message créé. valeur doit avoir un type compatible avec un ostream.

*Note :* ce système de log peut être assez lourd à utiliser, mais si l'on a pas grand chose à écrire, on peut être concis, par exemple : `getLogger()->Write(LoggerLevel::INFORMATION, "Lecteur params", "Pas de fichier spécifié, utilisation des paramètres par défaut");`

### ObjLib

La bibliothèque ObjLib est vraiment faite pour l'enregistrement de maillaige au format OBJ. La représentation du maillage qu'elle utilise n'est pas adaptée à autre chose. Si vous voulez utiliser cette bibliothèque, vous devrez sans doute définir certaines classes pour faciliter la tâche. La bibliothèque RibbonObjLib est construite sur ce principe et fourni donc des exemple d'utilisation d'ObjLib.

À savoir : dans un obj, il peut y avoir plusieurs objets, chacun scindé en plusieurs parties. Chaque partie est alors indépendante dans le sens où elle est représentée par un maillage. L'indexation repart à 0 pour chaque maillage. Un nouveau maillage est indiqué par un nouvel objet ou groupe.
PS: Dans le logiciel Blender, quand un fichier OBJ est importé, on peut choisir de considérer le fichier comme un maillage ou le scinder suivant les objets et/ou groupes. Je conseille donc de scinder par objet.

Note : cette bibliothèque utilise/est incluse dans le namespace qui lui est propre : `libobj`. N'oublier pas soit d'utiliser `using namespace libobj;` ou de préfixer les noms de classes par `libobj::`

On trouvera les classes `ObjObject`, `ObjGroup` dont le constructeur ne prend qu'un `string` indiquand le nom de l'objet/du groupe. La classe `Mtl` représente un matériel. Un matériel contient plusieurs propriétés (les noms des variables sont explicites), mais pour un usage basique le constructeur `Mtl(name, basecolor)` est fortement recommandé. Les couleurs sont ici des vecteurs 3D R,G,B à valeur dans [0-1], par exemple (1,0,0) représente le rouge.

Il est demandé d'utiliser pour tout nom, un mot qui serait un nom de variable valide en c++.

À bas niveau on trouve les classes `ObjVertex` et `ObjFace`. À moins de vouloir implementer un maillage non triangulaire, ces classes ne sont pas utiles.

Un maillage dérive de la classe abstraite `Mesh`. Les maillages triangulaires sont définis via la classe `TriMesh`. Aucun autre type de maillage n'est défini à ce jour, mais vous pouvez vous inspirer de l'implémentation de `TriMesh`. À ce jour, à chaque nœud est associé une position ET une normale, le format OBJ permettrait d'ajouter d'autres informations, mais cela requiert une adaptation du code.
Un maillage triangulaire est constitué de nœuds (=vertex/vertices au singulier puis pluriel) et de faces. Dans la classe correspondante les noeuds et faces sont indexées partir de 0, les faces contiennent alors les indices des trois nœuds qui les constituent. Il faut d'abord utiliser la méthode `resize` pour indiquer le nombre de nœuds et faces
Note : si vous connaissez le format OBJ, la classe `ObjFileWriter` réindexe correctement l'ensemble des éléments.

Un fois les maillages et matériaux définis, on peut facilement enregistrer via la classe `ObjFileWriter`. Elle fonctionne environ comme `std::ostream`, il faut donc utiliser l'opérateur de flux `<<`.

Important : la classe `Mesh` (et donc `TriMesh`) étant assez haut niveau va elle-même déclarer l'object ou groupe dont elle fait partie. Veillez-donc à bien nommer le maillage. S'il est définit en temps que groupe, il vous faudre déclarer l'objet à la main avant de déclarer les parties `writer<<ObjObject("Machin")<<part1<<part2<<part3`
    
Gestion des matériels : au fichier obj généré est attaché un fichier mtl contenant les matériaux utilisés. Ce fichier est créé quand l'opérateur appelle `close` ou quand le writer est détruit. Ce fichier est une liste des matériaux. Il est impératif que les matériaux utilisés n'aient pas le même nom, au cas échéant seul le dernier matériel envoyé au writer est conservé. Cela peut constituer un hack efficace si le nom du matériel est associé à sa couleur.

Pour un exemple d'utilisation, on peut regarder la bibliothèque RibbonObjLib.

Note importante : l'ordre des nœuds d'une face peut être important suivant le logiciel utilisé, ils doivent être définis dans le sens horaire par rapport à la face externe. L'autre face peut alors être noire. N'hésitez pas à recalculer les normales dans blender si besoin.

### RibbonObjLib

Cette bibliothèque est construite au dessus d'ObjLib. Son contenu est très inspirant pour l'étendre.

Commençons par les classes `TubeMesh`, `PlaneMesh`, `ArrowHD` et `ObjRibbonMesh`. Ces classes dérivent directement de `TriMesh`. Il est recommencé de s'en inspirer pour définir d'autres formes génériques de maillages.

Un certain nombre de classes aident à ajouter certains objets dans un obj. `FigureSuperRibbonMesher` sert par à illustrer un ruban (Super ruban uniquement) d'une seule couleur, sa centerline, le repère matériel et la règle en un nombre discret d'abscisse. `SuperRibbonMesher` gère les super rubans, deux couleurs sont utilisés pour différencier les segments, les couleurs sont ici des vecteurs RGB à valeurs dans [0-255]. Le mesh est généré en traçant les règles à intervalle réguliers, cet interval est défini par `MeshDiscreteLength`, de plus une épaisseur est définie. Les noms de fonctions sont sinon explicites. La classe équivalente pour les rubans mixtes est `MRMesher`.

La classe `RibbonPathGenerator` moins importante pour l'utilisateur,  pemert d'extraire position, repère matériel, direction de la règle à intervalle régulier pour les rubans.

La classe `FastMesherFile` permet de rapidement créer un fichier OBJ en ajoutant certains objets facilement.

### BaseCore

MERCI utile beaucoup le type real.
Le type real est en fait double, mais au cas où vous voudriez changer la précision, il suffirait d'adapter le fichier real.hpp. Pour être compatible avec le nommage utilisé par Eigen, les types tels MatrixXr, etc. sont disponibles.

Pour représenter les fonctions affines la classe `LinearScalar` représente le polynôme AX+B, compatible avec l'opérateur `<<` pour l'affichage.

Pour ajouter de la couleur à la sortie (seulement compatible unix, peut-être mac ?) `ColorMod` aporte la classe Modifier. Par exemple `std::cout<<Color::Modifier(Color::Code::BG_RED)<<"Hello";` affiche Hello avec un fond rouge.

La classe `preReal`, est sans grande importance, surtout là pour les algo d'optimisation (linesearch…). La classe contient un réel, peut le sauver puis comparer la valeur courante avec la valeur sauvegardée.

Le fichier `MathOperations.hpp` contient des opérations mathématiques utiles. Attention au namespace `MathOperations`. `contractGeneralDotP` sert à faire un produit scalaire entre matrice et vecteur, même si les coefficiants d'une des matrices (ou vecteur) sont des matrices. La méthode `product` permet une multiplication un peu plus générale. `compDeriv` aide pour les dérivées des composées. Etc.

`RepulsionField` correspond à une énergie de pénalisation. Un plan spécifié par un point et une normale. L'énergie résultante repousse un point de l'autre côté du plan s'il n'est pas dans l'espace «pointé par la normale». On a quatre modèles d'énergies : linéaire, quadratique, exponentiel et SoftWall (en 1/x). Cette classe n'est utilisée par MERCI, vu que des contraintes sont utilisées.

La bibliothèque BaseCore possède aussi certains algorithmes de convergence GradientDescent, NewtonDescent, BoundedNewtonDescent, BFGS, BFGSB(BFGS avec bornes) avec en complement une classe LineSearch. En pratique MERCI utilise plutôt la bibliothèque cppoptlib.

Finalement la classe `FiniteDifferencesTest` permet de tester l'implémentation des dérivations en comparant la dérivée donnée et le resultat par différences finies.

### SuperRibbonCore

Pour l'utilisateur les classes RibbonSystem et SuperRibbon sont les plus importantes.

La classe `SuperRibbon` décrit la cinématique (les degrés de libertés) du ruban ainsi que ses propriétés géométriques et physiques (taille, constantes physiques, position et orientation (posOr, frameOr correspondent au position, repère matériel en début de ruban), courbure normale naturelle). Attention, avant d'afficher les setters il ne faut pas oublier d'ajouter suffisament de segments au ruban. Les fonctions ont des noms explicites.

La classe `RibbonSystem` décrit le problème physique utilisant le ruban. En plus de permettre de fixer la physique externe au ruban (vecteur gravité), il permet de représenter le ruban par un point (vecteur contenant tout les degrés de liberté), et de calculer la valeur, le gradient et le hessien de l'énergie du système.

De manière non propre le modèle d'énergie élastique est choisi statiquement via la classe `ElasticEnergy`. Le modèle est choisit via la méthode `ChooseModel`. Les options Sadowsky et Wunderlich sont utilisables sans soucis, le modèle AudolyNeukirch est cependant probablement mal implémenté, toute autre option n'est pas implémentée pour l'instant. Les différentes classes spécialisées dans un ou deux modèle d'énergies sont préfixées par EBending.

La classe `SuperRibbonGeometrieSerie` implemente les séries sous-jacentes aux segments du rubans (voir la thèse pour des détails). La notation dXdY indique la dérivée de X par rapport à Y. La notation d2XdYZ indique la dérivée de X par rapport à Y puis par rapport à Z (second ordre). Cette classe possèpe un petit système de cache : tant que s ne change pas, la même quantité n'est pas calculée deux fois.

La classe `SRStaticProblem` est un petit wrapper pour un système de type `RibbonSystem`. Elle permet alors d'accéder facilement à la fonction energie du système sous forme fonctionnel f(x), ainsi qu'au gradient et hessien associé.

### MixedSRCore

Note : on peut reprocher aux modèles énérgétiques de SuperRibbonCore de ne pas être dans BaseCore, car ils aussi utilisé par le modèle mixte.

La classe `MixedSuperRibbon` permet de décrire le ruban pour le modèle mixte.

La classe `MixedPhY` décrit le système physique à simuler, elle est similaire à `RibbonSystem` pour `SuperRibbon`. La grande différence est l'ajout de contraintes diverses. Une fonction d'ajoute de contraintes renvoie une paire d'entier (index première contrainte, nombre de contraintes). L'ajout est définitif, il faut donc récréer tout le système si la suppression ou modification d'une contrainte est nécessaire. Le constructeur de `MixedPhY` prend un second argument `bool disableClamping` étant par défaut faux. Si cet argument est vrai, le ruban ne sera pas attaché en debut (ni position ni repère matériel). Cela sert par exemple pour poser le ruban.

Sur le vecteur représentant les degrés de liberté MixedPhY propose quelques fonctions statiques au nom explicite
```c++
  static int vpointIDOmega1(int segmentID);
  static int vpointIDEta(int segmentID);
  static int vpointIDPos(int segmentID);
  static int vpointIDFrame(int segmentID);
```
De manière plus détaillée le vecteur contient les degrés de liberté du premier segment, puis du second, etc jusqu'au dernier. Pour un segment la position est stockée puis le repère matériel (ligne par ligne) puis les variables a,b,n,m.

On liste les contraintes :
  + `addConstraintEtaStart()` pour fixer eta(0)=0
  + `addConstraintEtaEnd()` pour fixer eta(L)=0 où L est la longueur du ruban
  + `addConstraintEndPos(endPos)` pour fixer la position en fin de ruban
  + `addConstraintEndFrame(endFrame)` pour fixer le repère matériel en fin de ruban
  + `addConstraintOffPlane(pos, normale)` on peut gérer le contact entre un ruban et un plan. Le plan est défini par les arguments, ici un point appartenant au plan, et la normale au plan. Le ruban ne pourra être que dans la partie pointée par la normale. Les contraintes sont crées entre chaque segments sur la ligne médiane, donc tout autre point du ruban peut ne pas respecter le contact et traverser le plan.
  + `addConstraintOffPlaneDoubleEnd(pos, normale)` Similaire à addConstraintOffPlane, mais les contraintes sont définies aux extrémités des règles. Il y a donc deux fois plus de points de contact et le ruban peut mieux tenir sur la tranche.
  + `addConstraintLoopRibbon()` pour connecter les deux extrémités du ruban. Ne concerne que la position et le repère matériel. Comme précisé dans la thèse la même contrainte peut être utilisée pour un ruban de Moebius
  + `addConstraintLoopEta(bool negate)` pour fixer eta(0)=eta(L)  où L est la longueur du ruban ou si negate=true eta(0)=-eta(L) (Pour un ruban de Moebius)
  + `addConstraintBarycenterOnAxis(posAxis, dirAxis)` pour placer le barycentre du ruban sur un axe. L'axe est défini par un point (posAxis) et un vecteur directeur (dirAxis)
  + `addConstraintClampOrBoundingSphere(center, radius)` pour contraindre la position r(0) du ruban dans une sphère. Expérimentalement cette contrainte ne fonctionne pas… à investiger
   
Comment ajouter une contrainte ? (Pour s'amuser? on pourrait imaginer créer des extensions et charger dynamiquement des contraintes supplémentaires… )

Une contrainte va dériver de la classe `GenericExtraConstraint`. On imaginera donc par exemple une classe `class ExCl: public GenericExtraConstraint{};`.
Important, l'identificateur `constraintId` doit bien être renseigné, donc il est présent dans le constructeur comme montré ci dessous.

```c++
ExCl::ExCl(int constraintId, blabla mesArguments):
	GenericExtraConstraint(constraintId)
{...}
```
Quand on parle ici d'une contrainte, il s'agit plutôt d'un ensemble de contraintes scalaires. La méthode `size()` doit donc retourner combien de contraintes scalaires il y a. Ce nombre ne doit jamais changer.

Par défaut les contraintes sont de contraintes d'égalités de types f()=0. Il est possible de changer pour des contraintes X<=f()<=Y. Pour cela il faut inplémenter les méthodes `getUpperBound(Eigen::VectorXr& inpV)` et/ou `getLowerBound(Eigen::VectorXr& inpV);`. Comme pour size() ces bornes doivent être fixes. Pour l'implémentation, l'utilisateur modifie inpV (passé par référence). Il est important de modifier les bons éléments du vecteur, la partie à modifier est comprise entre l'indice constraintId et constraintId+size()-1. Exemple pour mettre la borne supérieure à + l'infini :

```c++
void ExCl::getUpperBound(Eigen::VectorXr& inpV)
{
  for(int i=0;i<size();i++)
    inpV[constraintId+i]=std::numeric_limits<real>::infinity();
}
```

Précision : par défaut, si ces méthodes ne sont pas surchargées elles remplissent le vecteur avec des zéros, d'où le «types f()=0». Si le remplissage est particulier, c'est parce que le vecteur inpV est commun à tout les vecteurs, donc écrire n'importe où peut provoquer des comportements étranges.

Quoi qu'il en soit, il y a cinq méthodes abstraites à surcharger. IMPORTANT : comme pour les bornes les vecteurs, maps, etc passés en arguments sont communs à toutes les contraintes, donc écrire n'importe où va impacter d'autres contraintes. On remarque que le système phy est passé en référence (constante), cela permet de facilement faire des calculs sur le ruban.

   + `virtual void eval(const MixedPhY& phy, Eigen::VectorXr& allConstraints);` L'évaluation porte bien son nom. Le résultat doit être enregistré dans le vecteur allConstraints à la bonne place, c'est à dire à partir de l'indice constraintId.
   + `virtual void jacobian(const MixedPhY& phy, std::map<Point, real> &liste);` Le jacobien est représenté avec une map. Le type Point est juste une paire. On utilisera par exemple la syntaxe `liste.at(Point(constraintId, MixedPhY::vpointIDEta(segment) + 1)) += value;`. Autrement dit dans Point on indique l'indice de la contrainte puis la variable concernée, et on ajoute la valeur avec l'opérateur +=. L'opérateur at peut être utilisé sans problème puisque la map a déja été initialisée.
   + `virtual void jacobianStruct(const MixedPhY& phy, std::map<Point, int> &liste);` La structure du jacobien doit être définie. Elle correspond aux points non nul de la matrice. Par exemple `liste[Point(constraintId, MixedPhY::vpointIDEta(segment) + 1)] ++;`. Précisions : par non null on entend qu'il existe un point pour lequel ce coefficiant du jacobien est non-null. 
   + `virtual void hessian(const MixedPhY& phy, const Eigen::VectorXr& scalar, std::map<Point, real> &liste);` Cette méthode est un peu particulière car elle ajoute un multiplicateur au hessien des différentes contraintes. Si la contrainte possède plusieurs contraintes scalaires, alors chacune a un coefficiant différent. Seul la somme importe, ainsi la liste représente une matrice creuse. Par exemple  `liste.at(Point(MixedPhY::vpointIDOmega1(segment) + j / 4, MixedPhY::vpointIDOmega1(segment) + (j % 4))) += scalar[constraintId+1]*hessReal(j/4,j%4);`. Ici += est bien l'opérateur à utiliser car la somme des hessiens est faite. Pour choisir le bon multiplicateur, il faut utiliser l'indice de la contrainte.
   + `virtual void hessianStruct(const MixedPhY& phy, std::map<Point, int> &liste);` Le pendant de jacobianStruct pour le hessien.
   
Quand la classe est implémenté il convient d'ajouter une méthode dans la classe `MixedPhy`. Cette nouvelle méthode resemble au code suivant :
```c++
std::pair<int,int> ExCl::addConstraintExCl(blabla mesArguments)
{
  theConstraints.push_back(std::shared_ptr<GenericExtraConstraint>(new ExCl(getNbConstraints(),mesArguments)));
  return {theConstraints.back()->offset(),theConstraints.back()->size()};
}
```
On pourrait même utiliser des templates pour réduire la taille du code !
   
Sur les plaques de pression :
Le code est capable de gérer les plaques de pression (cf thèse). Cela rajoute un degré de liberté par plaque (distance de la plaque à sa position au repos). La méthode statique `vpointIDPressures` renvoie l'indice où trouver ces dergrés de liberté dans le vecteur point. Bien évidement il faut ajouter des contraintes de non pénétration du ruban dans la plaque. Similairement au contact avec des plans on aura alors les deux methodes ` std::tuple<int, int, int> addPressure(Pressure pressure);` et `std::tuple<int, int, int> addPressureDoubleEnd(Pressure pressure);` (le contact est fait soit sur la ligne médiane, soit aux deux bords). En renvoi, on trouvera : l'indice de la plaque, l'indice de la première contrainte, et le nombre de contraintes.

On retrouvera aussi les `RepulsionField` dont il est fait référence plus haut.

Pistes d'améliorations futures : 
La classe `MixedPhy` commence a être trop chargée. On commence à avoir plusieurs objets en interactions. Il faudrait une classe `Physique` et des classes virtuelles `Objet`. Chaque objet a son énergie, ses contraintes… La classe `Physique` va alors rassembler les différents éléments dans un système commun.


Il y a deux classes un peu spéciales, pas forcément très utiles : `MixedPhyBoxOri` cette classe est vraiment à considérer comme un filtre pour `MixedPhy` : les degrés de liberté correspond à la position, au repère matériel du premier segment sont supprimés (et certaines contraintes correspondant à l'encastrement) du système. Si historiquement il est possible de construire ce système via un `MixedSuperRibbon` cela est fortement limitant : il vaut mieux le construire à partir du système `MixedPhy`.
La classe `MixedPhyRestrainOri` fait un peu la même chose, mais laisse un choix plus précis sur les degrés de liberté à supprimer avec la classe `Filter`, son usage n'est pas recommendé car les contraintes d'encastrement peuvent devenir nulles (0=0)


### IpoptSolvers

Cette bibliothèque implémenter les problèmes avec le modèle superruban, ruban mixte ou même fibre. Une partie des fonctions est utile pour Ipopt, et le lecteur devrait lire comment implémenter un problème avec Ipopt pour bien tout comprendre.

Un exemple d'utilisation sera alors 

```c++

	  MixedPhY phY(ribbon);
	  phY.setGravity(G);
	  phY.addConstraintLoopRibbon();
	  phY.addConstraintEtaStart();
	  phY.addConstraintEtaEnd();
	  auto [ids,idl] = phY.addConstraintOffPlaneDoubleEnd(planePos, planeNormal);
	  IpoptMixedRibbonVirtualBase *mynlpr = new IpoptMixedRibbon<MixedPhY>(phY);
	  SmartPtr<TNLP> mynlp = mynlpr;
	  SmartPtr<IpoptApplication> app = IpoptApplicationFactory();
	  app->Options()->SetNumericValue("tol", 1.e-10);
	  app->Options()->SetNumericValue("acceptable_tol", 1.e-9);
	  app->Options()->SetIntegerValue("print_level",0);
	  app->Options()->SetStringValue("fixed_variable_treatment","make_constraint");
	  app->Options()->SetNumericValue("bound_relax_factor",0);
	  app->Options()->SetIntegerValue("max_iter", 1000);
	  ApplicationReturnStatus status;
	  status = app->Initialize();
	  if (status != Solve_Succeeded) {
	    std::cout<<std::endl<<std::endl<<"*** Error during initialization!"<<std::endl;
	    lastRunStatus = IpoptGetStatusAsString(status);
	    return false;
	  }
	  status = app->OptimizeTNLP(mynlp);
	  if(status == Ipopt::NonIpopt_Exception_Thrown)
	  {
	    std::cout<<std::endl<<std::endl<<"*** Fatal Error during minimisation!"<<std::endl;
	    lastRunStatus = IpoptGetStatusAsString(status);
	    return false;
	  }
	  ribbon = mynlpr->getSolution();
  ```
  Là aussi il est raisonnable de lire la documentation d'Ipopt. Un exemple plus complet est implémenté dans la bibliothèque LuaInterface
  
### LuaInterface

Cette partie produit un éxécutable. Il est recommendé de se familiriser avec Sol une bibliothèque pour l'interfacage lua/c++. Tout le contenu correspond à l'interfacage.
Pour consulter la documentation LUA, un autre document est disponible.
