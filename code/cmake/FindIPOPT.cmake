# Try to locate the IPOPT library
#
# If the IPOPT_DIR is set, try to locate the package in the given
# directory, otherwise use pkg-config to locate it
#
# Create the following variables:
# IPOPT_INCLUDE_DIRS - Directories to include to use IPOPT
# IPOPT_LIBRARIES    - Default library to link against to use IPOPT
# IPOPT_DEFINITIONS  - Flags to be added to linker's options
# IPOPT_FOUND        - If false, don't try to use IPOPT
#
# Deprecated variables:
# IPOPT_LINK_FLAGS   - = IPOPT_DEFINITIONS, for compatibility

# Copyright (C) 2008-2010  RobotCub Consortium
# Authors: Ugo Pattacini
# CopyPolicy: Released under the terms of the GNU GPL v2.0.


if(APPLE)

	set(IPOPT_DIR $ENV{IPOPT})
	if(IPOPT_DIR_TEST)	
		set(IPOPT_DIR $ENV{IPOPT_DIR} CACHE PATH "Path to OOQP build directory")
	else()
		set(IPOPT_DIR /usr            CACHE PATH "Path to OOQP build directory")
	endif()

	find_path(IPOPT_INCLUDE_DIR IpoptConfig.h ${IPOPT_DIR}/include/coin ${IPOPT_DIR}/include/coin-or)


	find_library(IPOPT_LIBRARIES_BASE ipopt ${IPOPT_DIR}/lib NO_DEFAULT_PATH)
		#find_library(IPOPT_LIBRARIES_AST coinasl ${IPOPT_DIR}/lib NO_DEFAULT_PATH)
		#find_library(IPOPT_LIBRARIES_MUMPS coinmumps ${IPOPT_DIR}/lib NO_DEFAULT_PATH)
		#find_library(IPOPT_LIBRARIES_AMPLINTERFACE ipoptamplinterface ${IPOPT_DIR}/lib NO_DEFAULT_PATH)

	set(IPOPT_LIBRARIES
	  ${IPOPT_LIBRARIES_BASE} #${IPOPT_LIBRARIES_AST} ${IPOPT_LIBRARIES_MUMPS} ${IPOPT_LIBRARIES_AMPLINTERFACE}
	)
	set(IPOPT_FOUND TRUE)


elseif(UNIX)

    # in linux if the env var IPOPT_DIR is not set
    # we know we are dealing with an installed iCub package
    set(IPOPT_DIR_TEST $ENV{IPOPT_DIR})
    if(IPOPT_DIR_TEST)
        set(IPOPT_DIR $ENV{IPOPT_DIR} CACHE PATH "Path to IPOPT build directory")
    else()
        set(IPOPT_DIR /usr            CACHE PATH "Path to IPOPT build directory")
    endif()

    find_path(IPOPT_INCLUDE_DIR IpoptConfig.h ${IPOPT_DIR}/include/coin ${IPOPT_DIR}/include/coin-or)
    find_library(IPOPT_LIBRARIES ipopt ${IPOPT_DIR}/lib
                                       ${IPOPT_DIR}/lib/coin
				       ${IPOPT_DIR}/lib64
                                       ${IPOPT_DIR}/lib64/coin
                                       NO_DEFAULT_PATH)

    if(IPOPT_LIBRARIES)
      message( STATUS "IPOPT_LIBRARIES = ${IPOPT_LIBRARIES}" )
        find_file(IPOPT_DEP_FILE ipopt_addlibs_cpp.txt ${IPOPT_DIR}/share/doc/coin/Ipopt
                                                       ${IPOPT_DIR}/share/coin/doc/Ipopt
                                                       NO_DEFAULT_PATH)
        mark_as_advanced(IPOPT_DEP_FILE)

        if(IPOPT_DEP_FILE)
            # parse the file and acquire the dependencies
            file(READ ${IPOPT_DEP_FILE} IPOPT_DEP)
            string(REGEX REPLACE "-[^l][^ ]* " "" IPOPT_DEP ${IPOPT_DEP})
            string(REPLACE "-l"                "" IPOPT_DEP ${IPOPT_DEP})
            string(REPLACE "\n"                "" IPOPT_DEP ${IPOPT_DEP})
            string(REPLACE "ipopt"             "" IPOPT_DEP ${IPOPT_DEP})       # remove any possible auto-dependency
            separate_arguments(IPOPT_DEP)

            # use the find_library command in order to prepare rpath correctly
            foreach(LIB ${IPOPT_DEP})
                find_library(IPOPT_SEARCH_FOR_${LIB} ${LIB} ${IPOPT_DIR}/lib
                                                            ${IPOPT_DIR}/lib/coin
                                                            ${IPOPT_DIR}/lib/coin/ThirdParty
                                                            NO_DEFAULT_PATH)
                if(IPOPT_SEARCH_FOR_${LIB})
                    # handle non-system libraries (e.g. coinblas)
                    set(IPOPT_LIBRARIES ${IPOPT_LIBRARIES} ${IPOPT_SEARCH_FOR_${LIB}})
                else(IPOPT_SEARCH_FOR_${LIB})
                    # handle system libraries (e.g. gfortran)
                    set(IPOPT_LIBRARIES ${IPOPT_LIBRARIES} ${LIB})
                endif(IPOPT_SEARCH_FOR_${LIB})
                mark_as_advanced(IPOPT_SEARCH_FOR_${LIB})
            endforeach(LIB)
        endif()
    endif()
	
	include(FindPackageHandleStandardArgs)
	find_package_handle_standard_args(IPOPT DEFAULT_MSG IPOPT_INCLUDE_DIR IPOPT_LIBRARIES)

# Windows platforms
else()
    if( NOT DEFINED IPOPT_DIR )
      set(IPOPT_DIR_TEST $ENV{IPOPT_DIR})
      if(IPOPT_DIR_TEST)
        set(IPOPT_DIR $ENV{IPOPT_DIR} CACHE PATH "Path to IPOPT build directory")
      else()
	message( FATAL_ERROR "You must set IPOPT_DIR on Windows to the path of the IPOPT installation")
      endif()
    endif()

    message( STATUS "Buscando en ${IPOPT_DIR}/include}")
    find_path(IPOPT_INCLUDE_DIR IpoptConfig.h 
      PATHS ${IPOPT_DIR}/include/coin ${IPOPT_DIR}/include/coin-or 
      DOC "Path to IPOPT include"
      NO_DEFAULT_PATH)
      
    find_library(IPOPT_LIBRARY ipopt
      ${IPOPT_DIR}/lib
      ${IPOPT_DIR}/lib/coin
      NO_DEFAULT_PATH)

    if( IPOPT_LIBRARY )
      set( IPOPT_LIBRARIES ${IPOPT_LIBRARY} )
      find_library( IPOPT_MUMPS_LIBRARY coinmumps
        ${IPOPT_DIR}/lib
        ${IPOPT_DIR}/lib/coin
        NO_DEFAULT_PATH )
      if( IPOPT_MUMPS_LIBRARY )
        set( IPOPT_LIBRARIES ${IPOPT_MUMPS_LIBRARY} ${IPOPT_LIBRARIES} )
      endif( IPOPT_MUMPS_LIBRARY )
      find_library( IPOPT_ASL_LIBRARY coinasl
        ${IPOPT_DIR}/lib
        ${IPOPT_DIR}/lib/coin
        NO_DEFAULT_PATH )
      if( IPOPT_ASL_LIBRARY )
        set( IPOPT_LIBRARIES ${IPOPT_ASL_LIBRARY} ${IPOPT_LIBRARIES} )
      endif( IPOPT_ASL_LIBRARY )
      find_library( IPOPT_BLAS_LIBRARY coinblas
        ${IPOPT_DIR}/lib
        ${IPOPT_DIR}/lib/coin
        NO_DEFAULT_PATH )
      if( IPOPT_BLAS_LIBRARY )
        set( IPOPT_LIBRARIES ${IPOPT_BLAS_LIBRARY} ${IPOPT_LIBRARIES} )
      endif( IPOPT_BLAS_LIBRARY )
      find_library( IPOPT_LAPACK_LIBRARY coinblas
        ${IPOPT_DIR}/lib
        ${IPOPT_DIR}/lib/coin
        NO_DEFAULT_PATH )
      if( IPOPT_LAPACK_LIBRARY )
        set( IPOPT_LIBRARIES ${IPOPT_LAPACK_LIBRARY} ${IPOPT_LIBRARIES} )
      endif( IPOPT_LAPACK_LIBRARY )
    endif( IPOPT_LIBRARY )
    if(MSVC)
        set(IPOPT_DEFINITIONS "/NODEFAULTLIB:libcmt.lib;libcmtd.lib")
    else()
        set(IPOPT_DEFINITIONS "")
    endif()
		
	include(FindPackageHandleStandardArgs)
	find_package_handle_standard_args(IPOPT DEFAULT_MSG IPOPT_INCLUDE_DIR IPOPT_LIBRARIES)

	# Compatibility with previous versions
	set(IPOPT_LINK_FLAGS ${IPOPT_DEFINITIONS})
endif()

