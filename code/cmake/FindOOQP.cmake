set(OOQP_DIR $ENV{OOQP})
    if(IPOPT_DIR_TEST)
        set(OOQP_DIR $ENV{IPOPT_DIR} CACHE PATH "Path to OOQP build directory")
    else()
        set(OOQP_DIR /usr            CACHE PATH "Path to OOQP build directory")
    endif()

find_path(OOQP_INCLUDE_DIR ooqp/OoqpVersion.h ${OOQP_DIR}/include)


    find_library(OOQP_LIBRARIES_BOUND ooqpbound ${OOQP_DIR}/lib NO_DEFAULT_PATH)
    find_library(OOQP_LIBRARIES_DENSE ooqpdense ${OOQP_DIR}/lib NO_DEFAULT_PATH)
    find_library(OOQP_LIBRARIES_GENDENSE ooqpgendense ${OOQP_DIR}/lib NO_DEFAULT_PATH)
    find_library(OOQP_LIBRARIES_GENSPARSE ooqpgensparse ${OOQP_DIR}/lib NO_DEFAULT_PATH)
    find_library(OOQP_LIBRARIES_SPARSE ooqpsparse ${OOQP_DIR}/lib NO_DEFAULT_PATH)
    find_library(OOQP_LIBRARIES_GONDZIO ooqpgondzio ${OOQP_DIR}/lib NO_DEFAULT_PATH)
    find_library(OOQP_LIBRARIES_BASE ooqpbase ${OOQP_DIR}/lib NO_DEFAULT_PATH)
    find_library(OOQP_LIBRARIES_MEHROTRA ooqpmehrotra ${OOQP_DIR}/lib NO_DEFAULT_PATH)
    find_library(MA27_LIBRARY ma27 ${MA27_DIR}/lib NO_DEFAULT_PATH)


set(OOQP_LIBRARIES
  ${OOQP_LIBRARIES_BOUND} ${OOQP_LIBRARIES_DENSE} ${OOQP_LIBRARIES_GENDENSE} ${OOQP_LIBRARIES_GENSPARSE} ${OOQP_LIBRARIES_SPARSE} ${OOQP_LIBRARIES_GONDZIO} ${OOQP_LIBRARIES_BASE} ${OOQP_LIBRARIES_MEHROTRA} blas ${MA27_LIBRARY} gfortran
)
set(OOQP_FOUND TRUE)
