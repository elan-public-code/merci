set(OOQPEI_DIR $ENV{OOQP})
    if(IPOPT_DIR_TEST)
        set(OOQPEI_DIR $ENV{IPOPT_DIR} CACHE PATH "Path to OOQP Eigen interface build directory")
    else()
        set(OOQPEI_DIR /usr            CACHE PATH "Path to OOQP Eigen interface build directory")
    endif()

find_path(OOQPEI_INCLUDE_DIR OoqpEigenInterface.hpp ${OOQPEI_DIR}/include/OOQPEI/include)
#set(OOQPEI_INCLUDE_DIR "${OOQPEI_DIR}/include/OOQPEI/include" CACHE PATH "include dir")

  find_library(OOQPEI_LIBRARY ooqpei ${OOQPEI_DIR}/lib NO_DEFAULT_PATH)


set(OOQPEI_LIBRARY
  ${OOQPEI_LIBRARY} 
)
set(OOQPEI_FOUND TRUE)
