ribbon = SuperRibbon.new()

ribbon.width=0.1
ribbon.thickness=0.0001
length = 1
ribbon.areaDensity=0.2
ribbon.D=0.0002
ribbon.position={0.,0.,0.}
ribbon.frame={0.707106781187, -0.707106781187, 0,
          0,              0,               -1,
          0.707106781187, 0.707106781187,  0}
ribbon.poissonRatio=0.5
ribbon.numberSegments = 1;
ribbon:setRibbonStart(2.,1.)
ribbon:setRibbonPiece(0,length,0,0)
ribbon:setNaturalCurvature(0,0,80)

mesher = Mesher.new("VisuBreaking.obj")
mesher.precision=0.001
mesher:setColors({255,255,0},{0,255,255})
mesher:add(ribbon)

ribbonOri = ribbon:copy()
ribbonOri.frame=ribbon:endFrame()
ribbonOri.position=ribbon:endPos()
ribbonOri:setRibbonStart(2.,-1.)
mesher:setColors({255,0,0},{0,0,255})
mesher:add(ribbonOri)
