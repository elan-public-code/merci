require "Utils"

function frameTheta(theta)
      return {
              0, -math.sin(theta), math.cos(theta),
              1,               0,               0,
              0, math.cos(theta), math.sin(theta)}
    end
function frameThetaPhi(theta,phi)
    local sinTheta=math.sin(theta)
    local cosTheta=math.cos(theta)
    local sinPhi=math.sin(phi)
    local cosPhi=math.cos(phi)
    return {-sinPhi*sinTheta, -cosPhi*sinTheta, cosTheta,
              cosPhi,         -sinPhi,         0,
              sinPhi*cosTheta, cosPhi*cosTheta, sinTheta  
    }
end

function runExpe(name,ribbon,endPos,endFrame)
  options = Options.new()
  options.maxIters = 1000
  options.checkDerivation = false
  options.tol = 1.e-9
  options.logLevel=1
  
  local exp = MRFitter.new()
  exp.name="test101"
  exp.gravity={0.,0.,-1.}
  exp.ribbon=ribbon
  exp.options=options
  exp:attachEnd(endPos, endFrame)
  exp:clampRuling(true, true)
  
  print ("::::: Step "..name)
  exp.oriIsConstraint=false
  exp:findEquilibrium()
  print(exp.stats.status)
  return exp.ribbon
end

function runExpe2(name,ribbon,pos)
  options = Options.new()
  options.maxIters = 100
  options.checkDerivation = false
  options.tol = 1.e-9
  options.logLevel=0
  
  local exp = MRFitter.new()
  exp.name="test101"
  exp.gravity={0,0,0}--{0.,0.,-1.}
  exp.ribbon=ribbon
  exp.options=options
  exp:detachOrClamping()
  exp:declareRibbonCyclic(true)
  exp:clampRuling(true, true)
  exp:useLooseClamping(pos,0)
  
  print ("::::: Step "..name)
  exp.oriIsConstraint=false
  exp:findEquilibrium()
  print(exp.stats.status)
  return exp.ribbon
end

function runExpe3(name,ribbon,pos,posmid,normalmid,G)
  options = Options.new()
  options.maxIters = 100
  options.checkDerivation = false
  options.tol = 1.e-9
  options.logLevel=0
  
  local exp = MRFitter.new()
  exp.name="test101"
  exp.gravity=G
  exp.ribbon=ribbon
  exp.options=options
  exp:detachOrClamping()
  exp:declareRibbonCyclic(true)
  exp:clampRuling(true, true)
  exp:addOffPlaneDoubleEnd(posmid,normalmid)
  
  print ("::::: Step "..name)
  exp.oriIsConstraint=false
  exp:findEquilibrium()
  print(exp.stats.status)
  return exp.ribbon
end


function SimpleContactDetector(ribbon, plane)
  local plane = makePlane(plane)
  local t = ribbon.numberSegments
  for i=0, t-1, 1 do
    local pos = Vector.new(ribbon:getPosStart(i))
    local frame = Frame.new(ribbon:getFrameStart(i))
    local eta = ribbon:getEta(i)[2]
    local g = frame:generatrix(eta)
    local extr1 = g:cpy()
    extr1:scale(0.5*ribbon.width)
    extr1=pos+extr1
    local extr2 = g:cpy()
    extr2:scale(-0.5*ribbon.width)
    extr2=pos+extr2
    local d1=plane:distance(extr1)
    local d2=plane:distance(extr2)
    if math.abs(d1) < 1.e-5 then print("Contract "..i..",1 ("..d1..")") end
    if math.abs(d2) < 1.e-5 then print("Contract "..i..",2 ("..d2..")") end
  end
end

function TwistProtocol()
  ribbon = SuperRibbon.new()
  ribbon.width=0.01
  ribbon.thickness=0.0001
  length = 1
  ribbon.areaDensity=10
  ribbon.D=1
  ribbon.position={0.,0.,0.}
  ribbon.frame=frameTheta(0.)
  ribbon.poissonRatio=0.5
  ribbon.numberSegments = 100;
  ribbon:setRibbonStart(-math.pi*2./length,0.01)
  for i=0,ribbon.numberSegments-1,1 do
    ribbon:setRibbonPiece(i,length/ribbon.numberSegments,0.0001,0.0001)
    ribbon:setNaturalCurvature(i,0,0)
  end
  local sribbon = MixedSuperRibbon.new(ribbon)
  local mesher = Mesher.new("ExtProtocolMoebiusTranche.obj")
  mesher.precision=0.005
  mesher:setColors({255,255,0},{0,255,255})
  for step=0,1,0.2 do
    sribbon=runExpe("P"..step, sribbon, {0,0,0}, frameThetaPhi(0,step*math.pi))
  end
  for step=0.1,0.2,0.05 do
    sribbon.width=0.01*(1.-step)+0.25*(step)
    sribbon=runExpe("P"..(1.0+step), sribbon, {0,0,0}, frameThetaPhi(0,math.pi))
  end
  sribbon = runExpe2("P2.0",sribbon,{0,0,0})
  local posmid = sribbon:getPosStart(50)
  local framemid = sribbon:getFrameStart(50)
  local mnormalmid = {-framemid[2],-framemid[5],-framemid[8]}
  local normalmid = {framemid[2],framemid[5],framemid[8]}
  plane = PlaneMesh.new(posmid,mnormalmid)
  plane.color={255,255,255}
  mesher:add(plane)
  myArrow=Arrow.new()
  myArrow.position=posmid
  myArrow.direction=normalmid
  myArrow.color={255,255,0}
  myArrow.name="arrow"
  scale=0.1
  myArrow:setupDimensions(scale,0.15*scale,0.025*scale,0.05*scale)
  mesher:add(myArrow)
  local G = {-framemid[2],-framemid[5],-framemid[8]}
  --for ad=10,300,10 do
  ad=10
    ribbon.areaDensity=ad
    sribbon = runExpe3("P3."..ad,sribbon,{0,0,0},posmid,normalmid,G)
    mesher:setColors({0,255,50},{255,255,205})
    mesher:add(sribbon)
    SimpleContactDetector(sribbon,{posmid,normalmid})
  --end
end


TwistProtocol()

