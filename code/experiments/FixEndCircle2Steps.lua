ILoveSadowsky(true)


ribbon = SuperRibbon.new()

function frameTheta(theta)
      return {
              0, -math.sin(theta), math.cos(theta),
              1,               0,               0,
              0, math.cos(theta), math.sin(theta)}
    end
    
    
X=function(t) return math.sin(t)/t end
Y=function(t) return (math.cos(math.pi+t)+1)/t end

t=math.pi*3./4.

ribbon.width=0.01
ribbon.thickness=0.0001
length = 1
ribbon.areaDensity=0
ribbon.D=1
ribbon.position={0.,0.,0.}
ribbon.frame=frameTheta(0.)
ribbon.poissonRatio=0.5
ribbon.numberSegments = 10;
ribbon:setRibbonStart(-0.02,0.01)
for i=0,ribbon.numberSegments-1,1 do
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,0.0001,0.0001)
  ribbon:setNaturalCurvature(i,0,-t/length)
end

ribbonOri = ribbon:copy()

options = Options.new()
options.maxIters = 1000
options.checkDerivation = false
options.tol = 1.e-12

exp = Experiment.new()
exp.name="test101"
exp.gravity={0.,0.,9.81}
exp.ribbon=ribbon
exp.options=options

exp:findEquilibrium()

mesher = Mesher.new("testCircle2Steps.obj")
mesher.precision=0.005
mesher:setColors({255,255,0},{0,255,255})
mesher:add(ribbon)
mesher:setColors({255,0,0},{0,0,255})
mesher:add(exp.ribbon)
exp.ribbon:printPoint()
exp:attachEnd({X(t),0,Y(t)},frameTheta(t))
for i=0,ribbon.numberSegments-1,1 do
  exp.ribbon:setNaturalCurvature(i,0,0)
end
exp:findEquilibrium()
mesher:add(exp.ribbon)
exp.ribbon:printPoint()
