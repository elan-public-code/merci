ribbon = SuperRibbon.new()

ribbon.width=0.001
ribbon.thickness=0.0001
length = 0.5
--ribbon.areaDensity=0.2
ribbon.D=0.0002
position0={0.,0.,0.}
position1={0.,0.,0.}
frame0={0.707106781187, -0.707106781187, 0,
          0,              0,               -1,
          0.707106781187, 0.707106781187,  0}
frame1={-0.707106781187, 0.707106781187, 0,
          0,              0,               -1,
          -0.707106781187, -0.707106781187,  0}
ribbon.position=position0
ribbon.frame=frame0
ribbon.poissonRatio=0.5
ribbon.numberSegments = 20;
ribbon:setRibbonStart(10.,0.)
for i=0,ribbon.numberSegments-1,1 do
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,0,0)
  ribbon:setNaturalCurvature(i,0,0)
end

ribbonOri = ribbon:copy()

options = Options.new()
options.maxIters = 1000
options.checkDerivation = false
options.tol = 1.e-9

exp = Experiment.new()
exp.name="test101"
exp.gravity={0.,0.,9.81}
exp.ribbon=ribbon
exp.options=options
exp:attachEnd(position1, frame1)

exp:findEquilibrium()
-- exp.ribbon.width=0.01
-- exp:findEquilibrium()

mesher = Mesher.new("Moebius.obj")
mesher.precision=0.001
mesher:setColors({255,255,0},{0,255,255})
mesher:add(ribbon)
mesher:setColors({255,0,0},{0,0,255})
mesher:add(exp.ribbon)
