function drawFrame(pos, frame, scale, mesher)
  local ox=Arrow.new()
  ox.position=pos
  ox.direction={frame[1],frame[4],frame[7]}
  ox.color={255,0,0}
  ox.name="frameX"
  ox:setupDimensions(scale,0.15*scale,0.025*scale,0.05*scale)
  local oy=Arrow.new()
  oy.position=pos
  oy.direction={frame[2],frame[5],frame[8]}
  oy.color={0,255,0}
  oy.name="frameY"
  oy:setupDimensions(scale,0.15*scale,0.025*scale,0.05*scale)
  local oz=Arrow.new()
  oz.position=pos
  oz.direction={frame[3],frame[6],frame[9]}
  oz.color={0,0,255}
  oz.name="frameZ"
  oz:setupDimensions(scale,0.15*scale,0.025*scale,0.05*scale)
  mesher:add(ox)
  mesher:add(oy)
  mesher:add(oz)
end

function drawTwoFrame(pos1, frame1, pos2, frame2, scale, mesher)
  drawFrame(pos1, frame1, scale, mesher)
  drawFrame(pos2, frame2, scale, mesher)
end


function frameTheta(theta)
      return {
              0, -math.sin(theta), math.cos(theta),
              1,               0,               0,
              0, math.cos(theta), math.sin(theta)}
    end
function frameThetaPhi(theta,phi)
    local sinTheta=math.sin(theta)
    local cosTheta=math.cos(theta)
    local sinPhi=math.sin(phi)
    local cosPhi=math.cos(phi)
    return {-sinPhi*sinTheta, -cosPhi*sinTheta, cosTheta,
              cosPhi,         -sinPhi,         0,
              sinPhi*cosTheta, cosPhi*cosTheta, sinTheta  
    }
end

function runExpe(name,ribbon,endPos,endFrame)
  options = Options.new()
  options.maxIters = 1000
  options.checkDerivation = false
  options.tol = 1.e-9
  options.logLevel=1
  
  local exp = MRFitter.new()
  exp.name="test101"
  exp.gravity={0.,0.,-1.}
  exp.ribbon=ribbon
  exp.options=options
  exp:attachEnd(endPos, endFrame)
  exp:clampRuling(true, true)
  
  print ("::::: Step "..name)
  exp.oriIsConstraint=false
  exp:findEquilibrium()
  print(exp.stats.status)
  return exp.ribbon
end

function runExpe2(name,ribbon,pos)
  options = Options.new()
  options.maxIters = 100
  options.checkDerivation = false
  options.tol = 1.e-9
  options.logLevel=0
  
  local exp = MRFitter.new()
  exp.name="test101"
  exp.gravity={0,0,0}--{0.,0.,-1.}
  exp.ribbon=ribbon
  exp.options=options
  exp:detachOrClamping()
  exp:declareRibbonCyclic(true)
  exp:clampRuling(true, true)
  exp:useLooseClamping(pos,0)
  
  print ("::::: Step "..name)
  exp.oriIsConstraint=false
  exp:findEquilibrium()
  print(exp.stats.status)
  return exp.ribbon
end

function runExpe3(name,ribbon,pos,posmid,normalmid,G)
  options = Options.new()
  options.maxIters = 100
  options.checkDerivation = false
  options.tol = 1.e-9
  options.logLevel=0
  
  local exp = MRFitter.new()
  exp.name="test101"
  exp.gravity=G
  exp.ribbon=ribbon
  exp.options=options
  exp:detachOrClamping()
  exp:declareRibbonCyclic(true)
  exp:clampRuling(true, true)
  exp:addOffPlaneDoubleEnd(posmid,normalmid)
  
  print ("::::: Step "..name)
  exp.oriIsConstraint=false
  exp:findEquilibrium()
  print(exp.stats.status)
  return exp.ribbon
end

function TwistProtocol()
  ribbon = SuperRibbon.new()
  ribbon.width=0.01
  ribbon.thickness=0.0001
  length = 1
  ribbon.areaDensity=10
  ribbon.D=1
  ribbon.position={0.,0.,0.}
  ribbon.frame=frameTheta(0.)
  ribbon.poissonRatio=0.5
  ribbon.numberSegments = 100;
  ribbon:setRibbonStart(-math.pi*2./length,0.01)
  for i=0,ribbon.numberSegments-1,1 do
    ribbon:setRibbonPiece(i,length/ribbon.numberSegments,0.0001,0.0001)
    ribbon:setNaturalCurvature(i,0,0)
  end
  local sribbon = MixedSuperRibbon.new(ribbon)
  local mesher = Mesher.new("ExtProtocolMoebiusTranche.obj")
  mesher.precision=0.005
  mesher:setColors({255,255,0},{0,255,255})
  --mesher:add(ribbon)
  for step=0,1,0.2 do
    sribbon=runExpe("P"..step, sribbon, {0,0,0}, frameThetaPhi(0,step*math.pi))
--     mesher:setColors({255,255*step,0},{0,255*step,255})
--     mesher:add(sribbon)
  end
  for step=0.1,0.2,0.05 do
    sribbon.width=0.01*(1.-step)+0.25*(step)
    sribbon=runExpe("P"..(1.0+step), sribbon, {0,0,0}, frameThetaPhi(0,math.pi))
--     mesher:setColors({255*(1.-step),255,0},{255*step,255,255})
--     mesher:add(sribbon)
  end
  sribbon = runExpe2("P2.0",sribbon,{0,0,0})
--   mesher:setColors({0,255,0},{255,255,255})
--   mesher:add(sribbon)
  local posmid = sribbon:getPosStart(50)
  local framemid = sribbon:getFrameStart(50)
  local mnormalmid = {-framemid[2],-framemid[5],-framemid[8]}
  local normalmid = {framemid[2],framemid[5],framemid[8]}
  plane = PlaneMesh.new(posmid,mnormalmid)
  plane.color={255,255,255}
  mesher:add(plane)
  myArrow=Arrow.new()
  myArrow.position=posmid
  myArrow.direction=normalmid
  myArrow.color={255,255,0}
  myArrow.name="arrow"
  scale=0.1
  myArrow:setupDimensions(scale,0.15*scale,0.025*scale,0.05*scale)
  mesher:add(myArrow)
  local G = {-framemid[2],-framemid[5],-framemid[8]}
  for ad=10,300,10 do
    ribbon.areaDensity=ad
    sribbon = runExpe3("P3."..ad,sribbon,{0,0,0},posmid,normalmid,G)
    mesher:setColors({0,255,50},{255,255,205})
    mesher:add(sribbon)
  end
end

--ILoveSadowsky(true)
TwistProtocol()

