-- ILoveSadowsky(true)

ribbon = SuperRibbon.new()

function frameTheta(theta)
return {
              0, -math.sin(theta), math.cos(theta),
              1,               0 ,               0,
              0, math.cos(theta) , math.sin(theta)
              }
end

function frameThetaPhi(theta,phi)
    local sinTheta=math.sin(theta)
    local cosTheta=math.cos(theta)
    local sinPhi=math.sin(phi)
    local cosPhi=math.cos(phi)

    return {
    -sinPhi*sinTheta , -cosPhi*sinTheta , cosTheta,
              cosPhi ,         -sinPhi  ,        0,
     sinPhi*cosTheta , cosPhi*cosTheta  , sinTheta
    }
end

X=function(t) return math.sin(t)/t end
Y=function(t) return (math.cos(math.pi+t)+1)/t end

ribbon.width=0.02
ribbon.thickness=0.001
length = 1
ribbon.areaDensity=0
ribbon.D=1
ribbon.position={0.,0.,0.}
ribbon.frame=frameTheta(0.)
ribbon.poissonRatio=0.4
ribbon.numberSegments = 15;
ribbon:setRibbonStart(6.28,0.0)
for i=0,ribbon.numberSegments-1,1 do
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,0.000,0.000)
  ribbon:setNaturalCurvature(i,0,0)
end

ribbonOri = ribbon:copy()

mesher = Mesher.new("Moebius_up_to_Link_1.obj")
mesher.precision=0.005
mesher:setColors({255,255,0},{0,255,255}) -- jaune / cyan
--mesher:add(ribbonOri)


options = Options.new()
options.maxIters = 500
options.checkDerivation = false
options.tol = 1.e-8
options.logLevel=3   -- 5 toutes les Iterations / 3 - juste la fin

exp = Experiment.new()
exp.name="test101"
exp.gravity={0.0,0.0,-1.0}
exp.ribbon=ribbon
exp.options=options

exp:use3dotAlignementConstraint(true)  -- true : alejandro_bis (off-diag)

print("seed")
exp.ribbon:printPoint()

t0=2*math.pi

-- ... Pour bien se caler sur la solution ovale pendante non twistée
exp:attachEnd({X(-t0),0,Y(-t0)},frameTheta(-t0))
exp:findEquilibrium()
print("Debut : cercle, we have")
exp.ribbon:printPoint()
print(" ")
--mesher:setColors({255,255,0},{0,255,255}) -- jaune/cyan
--mesher:add(exp.ribbon)
-- FIN Pour bien se caler sur la solution ovale pendante non twistée

io.output("Link_u0_eta0.txt") -- Link, u_2(0), eta(0)

phiStart=0.0
phiEnd=2*math.pi
for a=0.0, 1.001, 0.025 do -- je mets 1.001 pour que ca fasse 1.0
   print(" * * * * * * ",a)
   phi=phiStart + a*(phiEnd-phiStart)
   exp:attachEnd({X(-t0),0,Y(-t0)},frameThetaPhi(-t0,phi))
   exp:findEquilibrium()
   print( "phi=" .. phi .. ", we find:")
   exp.ribbon:printPoint()
   currentstate=exp.ribbon:getPoint()
   io.write(phi/(2*math.pi) , " " , currentstate[1] , " " , currentstate[2] , "\n")

   local stats=exp.stats
   print("lambda : " .. table.concat(stats.lambda,", "))
   -- print("KTT error : " .. stats.ktt_error)

   mesher:setColors({a*255,0,255*(1-a)},{0,255*(1-a),a*255})
   mesher:add(exp.ribbon)
   print(" - - - - - - ")
   print(" ")
end

-- mesher:add(exp.ribbon)

-- print(currentstate[1] , currentstate[2]) -- le u_2(0) et le eta(0) de la derniere config
