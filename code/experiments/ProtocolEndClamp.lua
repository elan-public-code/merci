function drawFrame(pos, frame, scale, mesher)
  local ox=Arrow.new()
  ox.position=pos
  ox.direction={frame[1],frame[4],frame[7]}
  ox.color={255,0,0}
  ox.name="frameX"
  ox:setupDimensions(scale,0.15*scale,0.025*scale,0.05*scale)
  local oy=Arrow.new()
  oy.position=pos
  oy.direction={frame[2],frame[5],frame[8]}
  oy.color={0,255,0}
  oy.name="frameY"
  oy:setupDimensions(scale,0.15*scale,0.025*scale,0.05*scale)
  local oz=Arrow.new()
  oz.position=pos
  oz.direction={frame[3],frame[6],frame[9]}
  oz.color={0,0,255}
  oz.name="frameZ"
  oz:setupDimensions(scale,0.15*scale,0.025*scale,0.05*scale)
  mesher:add(ox)
  mesher:add(oy)
  mesher:add(oz)
end

function drawTwoFrame(pos1, frame1, pos2, frame2, scale, mesher)
  drawFrame(pos1, frame1, scale, mesher)
  drawFrame(pos2, frame2, scale, mesher)
end

file=io.open("table.csv","w")
file:write("\\begin{tabular}{|l|r|r|r|r|r|l|}","\n")
file:write("\\hline","\n")
file:write("model & iters & ktt error & ktt error scaled & lambda max & time & status\\\\","\n")

function keep2dec(num)
  return string.format("%.2e",num)
end

function keep1decFP(num)
  return string.format("%.1f",num)
end

function statline(model, iters, kkte, kktes, lambda, time, status)
  file:write(model.." & "..iters.." & ")
  file:write(string.format("%s & %s & ", keep2dec(kkte), keep2dec(kktes)))
  file:write(string.format("%s & %s & %s", keep2dec(math.max(unpack(lambda))), keep1decFP(time), status).."\\\\","\n")
end


function runExpe(name,sribbon,endPos,endFrame)
  print("=============================================================================")
  print(name)
  print("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *")
  file:write("\\hhline{|=|=|=|=|=|=|=|}","\n")
  file:write("\\multicolumn{7}{|c|}{\\textbf{"..name.."}}\\\\","\n")
  file:write("\\hline","\n")
  options = Options.new()
  options.maxIters = 1000
  options.checkDerivation = false
  options.tol = 1.e-9
  options.logLevel=1
  local exp = Experiment.new()
  exp.name="test101"
  exp.gravity={0.,0.,1}
  exp.ribbon=sribbon
  exp.options=options
  exp:attachEnd(endPos, endFrame)
  exp:use3dotAlignementConstraint(false)
  
  print ("::::: Chainage - coeff")

  exp:findEquilibrium()
  local stats=exp.stats
  print("lambda : "..table.concat(stats.lambda,", "))
  statline("Chainage - coeff", stats.iterations, stats.ktt_error, stats.ktt_error_scaled, stats.lambda, stats.time, stats.status)

  local mesher = Mesher.new("T"..name..".obj")
  mesher.precision=0.005
  mesher:setColors({255,255,0},{0,255,255})
  mesher:add(ribbon)
  mesher:setColors({255,0,0},{0,0,255})
  mesher:add(exp.ribbon)
  mesher:setColors({79,0,109},{255,0,125})
  exp.ribbon=sribbon;
  exp:use3dotAlignementConstraint(true)
  print ("::::: Chainage - orthogonal")
  exp:findEquilibrium()
  local stats=exp.stats
  print("lambda : "..table.concat(stats.lambda,", "))
  file:write("\\hline","\n")
  statline("Chainage - orthogonal", stats.iterations, stats.ktt_error, stats.ktt_error_scaled, stats.lambda, stats.time, stats.status)
  mesher:add(exp.ribbon)
  
  local exp = MRFitter.new()
  local ribbon = MixedSuperRibbon.new(sribbon)
  exp.name="test101"
  exp.gravity={0.,0.,-1.}
  exp.ribbon=ribbon
  exp.options=options
  exp:attachEnd(endPos, endFrame)
  
  print ("::::: Mixed")
  exp.oriIsConstraint=false
  exp:findEquilibrium()
  local stats=exp.stats
  print("lambda : "..table.concat(stats.lambda,", "))
  file:write("\\hline","\n")
  statline("Raccord - orthogonal", stats.iterations, stats.ktt_error, stats.ktt_error_scaled, stats.lambda, stats.time, stats.status)
  mesher:setColors({0,125,0},{125,255,0})
  mesher:add(exp.ribbon)
  
  print ("::::: Mixed DOF")
  exp.oriIsConstraint=true
  exp.ribbon=ribbon;
  exp:findEquilibrium()
  local stats=exp.stats
  print("lambda : "..table.concat(stats.lambda,", "))
  file:write("\\hline","\n")
  statline("Raccord cut - orthogonal", stats.iterations, stats.ktt_error, stats.ktt_error_scaled, stats.lambda, stats.time, stats.status)
  mesher:setColors({0,125,0},{125,255,0})
  mesher:add(exp.ribbon)
  exp.ribbon=ribbon;
  drawTwoFrame(ribbon.position,ribbon.frame,endPos,endFrame,0.05,mesher)
end

ILoveSadowsky(false)


ribbon = SuperRibbon.new()

function frameTheta(theta)
      return {
              0, -math.sin(theta), math.cos(theta),
              1,               0,               0,
              0, math.cos(theta), math.sin(theta)}
    end
function frameThetaPhi(theta,phi)
    local sinTheta=math.sin(theta)
    local cosTheta=math.cos(theta)
    local sinPhi=math.sin(phi)
    local cosPhi=math.cos(phi)
    return {-sinPhi*sinTheta, -cosPhi*sinTheta, cosTheta,
              cosPhi,         -sinPhi,         0,
              sinPhi*cosTheta, cosPhi*cosTheta, sinTheta  
    }
end
    
X=function(t) return math.sin(t)/t end
Y=function(t) return (math.cos(math.pi+t)+1)/t end

ribbon.width=0.01
ribbon.thickness=0.0001
length = 1
ribbon.areaDensity=10
ribbon.D=1
ribbon.position={0.,0.,0.}
ribbon.frame=frameTheta(0.)
ribbon.poissonRatio=0.5
ribbon.numberSegments = 20;
ribbon:setRibbonStart(-math.pi*2./length,0.01)
for i=0,ribbon.numberSegments-1,1 do
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,0.0001,0.0001)
  ribbon:setNaturalCurvature(i,0,0)
end

runExpe("Protocol1", ribbon, {0,0,0}, frameThetaPhi(0,math.pi/4.))
runExpe("Protocol2", ribbon, {0,0.1,0}, frameTheta(0))

ribbon:setRibbonStart(0.02,0.00)
for i=0,ribbon.numberSegments-1,1 do
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,0.0001,0.0001)
  ribbon:setNaturalCurvature(i,0,0)
end
t=0.1
runExpe("Protocol3", ribbon, {X(t),0,Y(t)},frameTheta(t))

ribbon:setRibbonStart(0.0,0.00)
local imid=ribbon.numberSegments/2
for i=0,imid-1,1 do
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,5,0)
  ribbon:setNaturalCurvature(i,0,0)
end
for i=imid,ribbon.numberSegments-1,1 do 
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,-5,0  )
  ribbon:setNaturalCurvature(i,0,0)
end
runExpe("Protocol4", ribbon, ribbon:endPos(),frameTheta(-math.pi/4.))

--Encastrement à 45 degres
ribbon.frame=frameThetaPhi(0,math.pi/4.)

ribbon:setRibbonStart(-math.pi*2./length,0.01)
for i=0,ribbon.numberSegments-1,1 do
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,0.0001,0.0001)
  ribbon:setNaturalCurvature(i,0,0)
end
runExpe("Protocol5", ribbon, {0,0.1,0},frameThetaPhi(0,math.pi/4.))

ribbon:setRibbonStart(0.02,0.00)
for i=0,ribbon.numberSegments-1,1 do
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,0.0001,0.0001)
  ribbon:setNaturalCurvature(i,0,0)
end
--runExpe("Protocol6", ribbon, {0.8,0,0},frameThetaPhi(0,math.pi/4.))


file:write("\\hline","\n")
file:write("\\end{tabular}","\n")
io.close(file)
