function drawFrame(pos, frame, scale, mesher)
  local ox=Arrow.new()
  ox.position=pos
  ox.direction={frame[1],frame[4],frame[7]}
  ox.color={255,0,0}
  ox.name="frameX"
  ox:setupDimensions(scale,0.15*scale,0.025*scale,0.05*scale)
  local oy=Arrow.new()
  oy.position=pos
  oy.direction={frame[2],frame[5],frame[8]}
  oy.color={0,255,0}
  oy.name="frameY"
  oy:setupDimensions(scale,0.15*scale,0.025*scale,0.05*scale)
  local oz=Arrow.new()
  oz.position=pos
  oz.direction={frame[3],frame[6],frame[9]}
  oz.color={0,0,255}
  oz.name="frameZ"
  oz:setupDimensions(scale,0.15*scale,0.025*scale,0.05*scale)
  mesher:add(ox)
  mesher:add(oy)
  mesher:add(oz)
end

function keep2dec(num)
  return string.format("%.2e",num)
end

function keep1decFP(num)
  return string.format("%.1f",num)
end

function statline(model, iters, kkte, kktes, lambda, time, status)
end


function runExpe(name,sribbon,endPos,endFrame)
  print("=============================================================================")
  print(name)
  print("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *")
  options = Options.new()
  options.maxIters = 1000
  options.checkDerivation = false
  options.tol = 1.e-9
  options.logLevel=5
  local exp = Experiment.new()
  exp.name="test101"
  exp.gravity={0.,0.,1}
  exp.ribbon=sribbon
  exp.options=options
  exp:attachEnd(endPos, endFrame)
  exp:use3dotAlignementConstraint(false)
  
  print ("::::: Chainage - coeff")

  exp:findEquilibrium()
  local stats=exp.stats
  print("lambda : "..table.concat(stats.lambda,", "))
  statline("Chainage - coeff", stats.iterations, stats.ktt_error, stats.ktt_error_scaled, stats.lambda, stats.time, stats.status)

  
end

ILoveSadowsky(false)


ribbon = SuperRibbon.new()

function frameTheta(theta)
      return {
              0, -math.sin(theta), math.cos(theta),
              1,               0,               0,
              0, math.cos(theta), math.sin(theta)}
    end
function frameThetaPhi(theta,phi)
    local sinTheta=math.sin(theta)
    local cosTheta=math.cos(theta)
    local sinPhi=math.sin(phi)
    local cosPhi=math.cos(phi)
    return {-sinPhi*sinTheta, -cosPhi*sinTheta, cosTheta,
              cosPhi,         -sinPhi,         0,
              sinPhi*cosTheta, cosPhi*cosTheta, sinTheta  
    }
end
    
X=function(t) return math.sin(t)/t end
Y=function(t) return (math.cos(math.pi+t)+1)/t end

ribbon.width=0.01
ribbon.thickness=0.0001
length = 1
ribbon.areaDensity=10
ribbon.D=1
ribbon.position={0.,0.,0.}
ribbon.frame=frameTheta(0.)
ribbon.poissonRatio=0.5
ribbon.numberSegments = 20;
ribbon:setRibbonStart(-math.pi*2./length,0.01)
for i=0,ribbon.numberSegments-1,1 do
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,0.0001,0.0001)
  ribbon:setNaturalCurvature(i,0,0)
end

runExpe("Protocol1", ribbon, {0,0,0}, frameThetaPhi(0,math.pi/4.))
runExpe("Protocol2", ribbon, {0,0.1,0}, frameTheta(0))

ribbon:setRibbonStart(0.02,0.00)
for i=0,ribbon.numberSegments-1,1 do
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,0.0001,0.0001)
  ribbon:setNaturalCurvature(i,0,0)
end
t=0.1
runExpe("Protocol3", ribbon, {X(t),0,Y(t)},frameTheta(t))

ribbon:setRibbonStart(0.0,0.00)
local imid=ribbon.numberSegments/2
for i=0,imid-1,1 do
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,5,0)
  ribbon:setNaturalCurvature(i,0,0)
end
for i=imid,ribbon.numberSegments-1,1 do 
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,-5,0  )
  ribbon:setNaturalCurvature(i,0,0)
end
runExpe("Protocol4", ribbon, ribbon:endPos(),frameTheta(-math.pi/4.))

--Encastrement à 45 degres
ribbon.frame=frameThetaPhi(0,math.pi/4.)

ribbon:setRibbonStart(-math.pi*2./length,0.01)
for i=0,ribbon.numberSegments-1,1 do
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,0.0001,0.0001)
  ribbon:setNaturalCurvature(i,0,0)
end
runExpe("Protocol5", ribbon, {0,0.1,0},frameThetaPhi(0,math.pi/4.))

ribbon:setRibbonStart(0.02,0.00)
for i=0,ribbon.numberSegments-1,1 do
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,0.0001,0.0001)
  ribbon:setNaturalCurvature(i,0,0)
end
--runExpe("Protocol6", ribbon, {0.8,0,0},frameThetaPhi(0,math.pi/4.))


