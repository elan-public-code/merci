function drawFrame(pos, frame, scale, mesher)
  local ox=Arrow.new()
  ox.position=pos
  ox.direction={frame[1],frame[4],frame[7]}
  ox.color={255,0,0}
  ox.name="frameX"
  ox:setupDimensions(scale,0.15*scale,0.025*scale,0.05*scale)
  local oy=Arrow.new()
  oy.position=pos
  oy.direction={frame[2],frame[5],frame[8]}
  oy.color={0,255,0}
  oy.name="frameY"
  oy:setupDimensions(scale,0.15*scale,0.025*scale,0.05*scale)
  local oz=Arrow.new()
  oz.position=pos
  oz.direction={frame[3],frame[6],frame[9]}
  oz.color={0,0,255}
  oz.name="frameZ"
  oz:setupDimensions(scale,0.15*scale,0.025*scale,0.05*scale)
  mesher:add(ox)
  mesher:add(oy)
  mesher:add(oz)
end

function drawTwoFrame(pos1, frame1, pos2, frame2, scale, mesher)
  drawFrame(pos1, frame1, scale, mesher)
  drawFrame(pos2, frame2, scale, mesher)
end

function keep2dec(num)
  return string.format("%.2e",num)
end

function keep1decFP(num)
  return string.format("%.1f",num)
end

function statline(iters, kkte, kktes, lambda, time, status)
  print("Iterations : "..iters)
  print("NLP error : "..keep2dec(kkte))
  print("NLP error (scaled) : "..keep2dec(kktes))
  print("lambda max : "..keep2dec(math.max(unpack(lambda))))
  print("Time : "..keep1decFP(time).."s")
  print("Status : "..status)
end

function plot(mesher, ribbonOr, ribbonF, pos1, frame1, pos2, frame2)
  mesher:setColors({255,255,0},{0,255,255})
  
  mesher:add(ribbonOr)
  drawTwoFrame(pos1,frame1,pos2,frame2, 0.05, mesher)
  
  mesher:setColors({255,0,0},{0,0,255})
  mesher:add(ribbonF)
end

function doubleRun(exp)
  print(" ")
  print("=================================================")
  print("=================================================")
  print(" ")
  exp.options.logLevel=0
  exp:findEquilibrium()
  print("Midrun status: "..exp.stats.status)
  exp.options.logLevel=0
  while(exp.stats.iterations == exp.options.maxIters) do
    exp:findEquilibrium()
    print("Midrun status: "..exp.stats.status)
  end
end

function slowMoveInit(file, numSegments)
  local phi=math.pi/4.
  local deltaL=0.8
  deltaD=0.01
  local frame1={0,-math.sin(phi), math.cos(phi),
                0, math.cos(phi), math.sin(phi), 
                -1, 0,             0}
  local ePhi=-math.pi/4.
  local frame2={0,-math.sin(ePhi), math.cos(ePhi),
                0, math.cos(ePhi), math.sin(ePhi), 
                -1, 0,             0}
--   local G = {0.,1.,0.}
  local G = {0.,0.,-1.}
  local pos1={0.,0.,0.}
  local pos2={1-deltaL,0.,deltaD}
  ribbon = MixedSuperRibbon.new()
  ribbon.width=0.05
  ribbon.thickness=0.0001
  length = 1
  ribbon.areaDensity=10.0
  ribbon.D=1
  ribbon.poissonRatio=0.5
  ribbon.numberSegments = numSegments;
  ribbon.position=pos1
  ribbon.frame=frame1
  for i=0,ribbon.numberSegments-1,1 do
    ribbon:setLength(i,1./ribbon.numberSegments)
    ribbon:setMiniSys(i, {0.0001,0.00001,0.0001,0.00001})
  end
  ribbon:reChain()
  local options = Options.new()
  options.maxIters = 100
  options.checkDerivation = false
  options.tol = 1.e-9
  options.logLevel=5
  local exp = MRFitter.new()
  exp.name="test101"
  exp.gravity=G
  exp.ribbon=ribbon
  exp.options=options
  exp:attachEnd(pos2, frame2)
  exp:clampRuling(true, true)
  doubleRun(exp)
  
  local frame1={0, 0, 1,
                0, 1, 0, 
                -1, 0, 0}
  exp.ribbon.frame=frame1
  doubleRun(exp)
  
  ePhi=-math.pi/20.
  local frame2={0,-math.sin(ePhi), math.cos(ePhi),
                0, math.cos(ePhi), math.sin(ePhi), 
                -1, 0,             0}
  local deltaL=0.8
  local pos2={1-deltaL,0.,deltaD}
  exp:attachEnd(pos2, frame2)
  doubleRun(exp)
  
  local ePhi=0.
  local frame2={0,-math.sin(ePhi), math.cos(ePhi),
                0, math.cos(ePhi), math.sin(ePhi), 
                -1, 0,             0}
  local deltaL=0.7
  local pos2={1-deltaL,0.,deltaD}
  exp:attachEnd(pos2, frame2)
  doubleRun(exp)
  local deltaL=0.6
  local pos2={1-deltaL,0.,deltaD}
  exp:attachEnd(pos2, frame2)
  doubleRun(exp)
  local deltaL=0.5
  local pos2={1-deltaL,0.,deltaD}
  exp:attachEnd(pos2, frame2)
  doubleRun(exp)
  
  local hmid = exp.ribbon:getPosStart(ribbon.numberSegments/2+1)[2]
  print ("dw: "..deltaD..", hmid: "..math.abs(hmid))
  file:write(deltaD.." "..math.abs(hmid),"\n")
  local stats=exp.stats
  statline(stats.iterations, stats.ktt_error, stats.ktt_error_scaled, stats.lambda, stats.time, stats.status)
  
--   local mesher = Mesher.new("ProtocolHannaSlowMove.obj")
--   mesher.precision=0.005
--   plot(mesher, ribbon, exp.ribbon, pos1, frame1, pos2, frame2)
  return exp.ribbon
end

function slowMoveStep(ribbon, deltaD,file)
--   local G = {0.,1.,0.}
  local G = {0.,0.,-1.}
  local deltaL=0.5
  local pos2={1-deltaL,0.,deltaD}
  local frame2={0, 0, 1,
                0, 1, 0, 
              -1, 0, 0}
  
  local options = Options.new()
  options.maxIters = 1000
  options.checkDerivation = false
  options.tol = 1.e-9
  options.logLevel=5
  local exp = MRFitter.new()
  exp.name="test101"
  exp.gravity=G
  exp.ribbon=ribbon
  exp.options=options
  exp:attachEnd(pos2, frame2)
--   exp:clampRuling(false, false)
--   doubleRun(exp)
  exp:clampRuling(true, true)
  doubleRun(exp)
  
  local hmid = exp.ribbon:getPosStart(ribbon.numberSegments/2+1)[2]
  print ("dw: "..deltaD..", hmid: "..math.abs(hmid))
  file:write(deltaD.." "..math.abs(hmid),"\n")
  local stats=exp.stats
  statline(stats.iterations, stats.ktt_error, stats.ktt_error_scaled, stats.lambda, stats.time, stats.status)
  
  return exp.ribbon
end

function runExpe(nbSegts, fnObj, fnData)
  --theta=0
  --deltaL=0.25
  --w/L=1/20
  local mesher = Mesher.new(fnObj)
  mesher.precision=0.005
  --   plot(mesher, ribbon, exp.ribbon, pos1, frame1, pos2, frame2)
  local file=io.open(fnData,"w")
  rib=slowMoveInit(file, nbSegts)
  mesher:setColors({255,0.1*255,0},{0,255-0.1*255,255})
  mesher:add(rib)
--   for dw=0.02,0.75,0.005 do
  for dw=0.02,0.6,0.005 do
    rib=slowMoveStep(rib,dw,file)
    mesher:setColors({255,dw*255,0},{0,255-dw*255,255})
    mesher:add(rib)
  end
--   for dw=0.74,0.01,-0.005 do
--     rib=slowMoveStep(rib,dw,file)
--     mesher:setColors({255,dw*255,0},{0,255-dw*255,255})
--     mesher:add(rib)
--   end
  io.close(file)
end

--ILoveSadowsky(true)

--runExpe(200, "ProtocolHannaRev_0GUHD.obj", "Rev_0GUHD.gnudat")
runExpe(200, "ProtocolHannaRev_0G.obj", "Rev_0G.gnudat")
-- runExpe(100, "ProtocolHannaRev_Sadowsky_0G.obj", "Rev_Sadowsky_0G.gnudat")
--runExpe(50, "ProtocolHannaRev_0GLow.obj", "Rev_0GLow.gnudat")
