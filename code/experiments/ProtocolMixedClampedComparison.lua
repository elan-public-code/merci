function drawFrame(pos, frame, scale, mesher)
  local ox=Arrow.new()
  ox.position=pos
  ox.direction={frame[1],frame[4],frame[7]}
  ox.color={255,0,0}
  ox.name="frameX"
  ox:setupDimensions(scale,0.15*scale,0.025*scale,0.05*scale)
  local oy=Arrow.new()
  oy.position=pos
  oy.direction={frame[2],frame[5],frame[8]}
  oy.color={0,255,0}
  oy.name="frameY"
  oy:setupDimensions(scale,0.15*scale,0.025*scale,0.05*scale)
  local oz=Arrow.new()
  oz.position=pos
  oz.direction={frame[3],frame[6],frame[9]}
  oz.color={0,0,255}
  oz.name="frameZ"
  oz:setupDimensions(scale,0.15*scale,0.025*scale,0.05*scale)
  mesher:add(ox)
  mesher:add(oy)
  mesher:add(oz)
end

function drawTwoFrame(pos1, frame1, pos2, frame2, scale, mesher)
  drawFrame(pos1, frame1, scale, mesher)
  drawFrame(pos2, frame2, scale, mesher)
end

function runExpe(name,sribbon,endPos,endFrame)
  print("=============================================================================")
  print(name)
  print("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *")
  options = Options.new()
  options.maxIters = 1000
  options.checkDerivation = false
  options.tol = 1.e-12
  options.logLevel=5
 

  local mesher = Mesher.new("T"..name..".obj")
  mesher.precision=0.005
  
  local exp = MRFitter.new()
  local ribbon = MixedSuperRibbon.new(sribbon)
  exp.name="test101"
  exp.gravity={0.,0.,-1.}
  exp.ribbon=ribbon
  exp.options=options
  exp:attachEnd(endPos, endFrame)
  
  print ("::::: 1")
  MRCoeffs(1.,1.)
  exp:findEquilibrium()
  local stats=exp.stats
  print(table.concat(stats.lambda,", "))
  print("Je converge en "..stats.time.."s, soit en "..stats.iterations.. "iterations.")

  mesher:setColors({255,255,0},{0,255,255})
  mesher:add(exp.ribbon)
  print ("::::: 1/c")
  MRCoeffs(1./3.,1./9.)
  exp:findEquilibrium()
  mesher:setColors({255,0,0},{0,0,255})
  mesher:add(exp.ribbon)
  
  print ("::::: 1./n")
  MRCoeffs(1./ribbon.numberSegments,1./ribbon.numberSegments)
  exp:findEquilibrium()
  mesher:setColors({79,0,109},{255,0,125})
  mesher:add(exp.ribbon)
  
  print ("::::: 1./(cn)")
  MRCoeffs(1./(3.*ribbon.numberSegments),1./(9.*ribbon.numberSegments))
  exp:findEquilibrium()
  mesher:setColors({0,125,0},{125,255,0})
  mesher:add(exp.ribbon)
  exp.ribbon=ribbon;
  drawTwoFrame(ribbon.position,ribbon.frame,endPos,endFrame,0.05,mesher)
--   local f = ribbon:getFrameStart(ribbon.numberSegments-1)
--   local d = f[1]*f[5]*f[9]+f[2]*f[6]*f[7]+f[3]*f[4]*f[8]-f[3]*f[5]*f[7]-f[2]*f[4]*f[9]-f[1]*f[6]*f[8]
--   print("d="..d)
end

ILoveSadowsky(false)


ribbon = SuperRibbon.new()

function frameTheta(theta)
      return {
              0, -math.sin(theta), math.cos(theta),
              1,               0,               0,
              0, math.cos(theta), math.sin(theta)}
    end
function frameThetaPhi(theta,phi)
    local sinTheta=math.sin(theta)
    local cosTheta=math.cos(theta)
    local sinPhi=math.sin(phi)
    local cosPhi=math.cos(phi)
    return {-sinPhi*sinTheta, -cosPhi*sinTheta, cosTheta,
              cosPhi,         -sinPhi,         0,
              sinPhi*cosTheta, cosPhi*cosTheta, sinTheta  
    }
end
    
X=function(t) return math.sin(t)/t end
Y=function(t) return (math.cos(math.pi+t)+1)/t end

ribbon.width=0.01
ribbon.thickness=0.0001
length = 1
ribbon.areaDensity=10
ribbon.D=1
ribbon.position={0.,0.,0.}
ribbon.frame=frameTheta(0.)
ribbon.poissonRatio=0.5
ribbon.numberSegments = 20;
ribbon:setRibbonStart(-math.pi*2./length,0.01)
for i=0,ribbon.numberSegments-1,1 do
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,0.0001,0.0001)
  ribbon:setNaturalCurvature(i,0,0)
end

runExpe("Protocol1", ribbon, {0,0,0}, frameThetaPhi(0,math.pi/4.))
runExpe("Protocol2", ribbon, {0,0.1,0}, frameTheta(0))

ribbon:setRibbonStart(0.02,0.00)
for i=0,ribbon.numberSegments-1,1 do
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,0.0001,0.0001)
  ribbon:setNaturalCurvature(i,0,0)
end
t=0.1
runExpe("Protocol3", ribbon, {X(t),0,Y(t)},frameTheta(t))

ribbon:setRibbonStart(0.0,0.00)
local imid=ribbon.numberSegments/2
for i=0,imid-1,1 do
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,5,0)
  ribbon:setNaturalCurvature(i,0,0)
end
for i=imid,ribbon.numberSegments-1,1 do 
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,-5,0  )
  ribbon:setNaturalCurvature(i,0,0)
end
runExpe("Protocol4", ribbon, ribbon:endPos(),frameTheta(-math.pi/4.))

--Encastrement à 45 degres
ribbon.frame=frameThetaPhi(0,math.pi/4.)

ribbon:setRibbonStart(-math.pi*2./length,0.01)
for i=0,ribbon.numberSegments-1,1 do
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,0.0001,0.0001)
  ribbon:setNaturalCurvature(i,0,0)
end
runExpe("Protocol5", ribbon, {0,0.1,0},frameThetaPhi(0,math.pi/4.))

ribbon:setRibbonStart(0.02,0.00)
for i=0,ribbon.numberSegments-1,1 do
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,0.0001,0.0001)
  ribbon:setNaturalCurvature(i,0,0)
end
--runExpe("Protocol6", ribbon, {0.8,0,0},frameThetaPhi(0,math.pi/4.))
