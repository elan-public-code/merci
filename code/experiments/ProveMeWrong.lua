ILoveSadowsky(true)


ribbon = SuperRibbon.new()

function frameTheta(theta)
      return {
              0, -math.sin(theta), math.cos(theta),
              1,               0,               0,
              0, math.cos(theta), math.sin(theta)}
    end
function frameThetaPhi(theta,phi)
    local sinTheta=math.sin(theta)
    local cosTheta=math.cos(theta)
    local sinPhi=math.sin(phi)
    local cosPhi=math.cos(phi)
    return {-sinPhi*sinTheta, -cosPhi*sinTheta, cosTheta,
              cosPhi,         -sinPhi,         0,
              sinPhi*cosTheta, cosPhi*cosTheta, sinTheta  
    }
end
    
X=function(t) return math.sin(t)/t end
Y=function(t) return (math.cos(math.pi+t)+1)/t end

ribbon.width=0.01
ribbon.thickness=0.0001
length = 1
ribbon.areaDensity=0
ribbon.D=1
ribbon.position={0.,0.,0.}
ribbon.frame=frameTheta(0.)
ribbon.poissonRatio=0.5
ribbon.numberSegments = 10;
ribbon:setRibbonStart(-0.02,0.01)
for i=0,ribbon.numberSegments-1,1 do
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,0.0001,0.0001)
  ribbon:setNaturalCurvature(i,0,0)
end

ribbon:randomizeShape()

options = Options.new()
options.maxIters = 1
options.checkDerivation = true
options.tol = 1.e-12

exp = Experiment.new()
exp.name="test101"
exp.gravity={0.,0.,9.81}
exp.ribbon=ribbon
exp.options=options
exp:attachEnd({1,0,0}, frameTheta(0))
exp:use3dotAlignementConstraint(true)

exp.ribbon:printPoint()


t=0.01
exp:attachEnd({X(t),0,Y(t)},frameTheta(t))
exp:findEquilibrium()

