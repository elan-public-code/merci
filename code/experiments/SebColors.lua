ILoveSadowsky(true)

ribbon = SuperRibbon.new()

function frameTheta(theta)
return {
              0, -math.sin(theta), math.cos(theta),
              1,               0 ,               0,
              0, math.cos(theta) , math.sin(theta)
              }
end

function frameThetaPhi(theta,phi)
    local sinTheta=math.sin(theta)
    local cosTheta=math.cos(theta)
    local sinPhi=math.sin(phi)
    local cosPhi=math.cos(phi)

    return {
    -sinPhi*sinTheta , -cosPhi*sinTheta , cosTheta,
              cosPhi ,         -sinPhi  ,        0,
     sinPhi*cosTheta , cosPhi*cosTheta  , sinTheta
    }
end

X=function(t) return math.sin(t)/t end
Y=function(t) return (math.cos(math.pi+t)+1)/t end

ribbon.width=0.01
ribbon.thickness=0.0001
length = 1
ribbon.areaDensity=0
ribbon.D=1
ribbon.position={0.,0.,0.}
ribbon.frame=frameTheta(0.)
ribbon.poissonRatio=0.5
ribbon.numberSegments = 10;
ribbon:setRibbonStart(0.15,0.0)
-- ribbon:setRibbonStart(-0.01,0.0)
for i=0,ribbon.numberSegments-1,1 do
  -- ribbon:setRibbonPiece(i,length/ribbon.numberSegments,0.0001,0.0001)
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,0.000,0.000)
  ribbon:setNaturalCurvature(i,0,0)
end

-- ribbon:randomizeShape()
ribbonOri = ribbon:copy()

mesher = Mesher.new("testCircle_N_Steps.obj")
mesher.precision=0.005
mesher:setColors({255,255,0},{0,255,255}) -- jaune / cyan
mesher:add(ribbonOri)


options = Options.new()
options.maxIters = 1000
options.checkDerivation = false
options.tol = 1.e-9
options.logLevel = 0

exp = Experiment.new()
exp.name="test101"
exp.gravity={0.0,0.0,-1.0}
exp.ribbon=ribbon
exp.options=options
-- exp:attachEnd({1,0,0}, frameTheta(0))
exp:use3dotAlignementConstraint(false)  -- true : alejandro

print("seed")
exp.ribbon:printPoint()

t0=0.21
exp:attachEnd({X(-t0),0,Y(-t0)},frameTheta(-t0))
exp:findEquilibrium()
print("1ere expe")
exp.ribbon:printPoint()
mesher:setColors({255,0,0},{0,0,255}) -- rouge/bleu
mesher:add(exp.ribbon)

for i=0,6,0.21 do

  t=t0+i
  exp:attachEnd({X(-t),0,Y(-t)},frameTheta(-t))
  exp:findEquilibrium()
  print( i .. " eme expe")
  --exp.ribbon:printPoint()
  local cCol=50+30*i
  mesher:setColors({255-cCol,cCol,0},{0,255-cCol,cCol}) -- ? / ?bleu
  mesher:add(exp.ribbon)

end

t1=2*math.pi
exp:attachEnd({X(-t1),0,Y(-t1)},frameTheta(-t1))
exp:findEquilibrium()
print("final expe")
exp.ribbon:printPoint()
mesher:setColors({0,255,0},{0,0,255}) -- vert/bleu
mesher:add(exp.ribbon)
