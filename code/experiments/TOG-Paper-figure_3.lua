-- December 2022
-- We compare the equilibrium solution between a ribbon and a rod with rectangular cross-section

function framePhi(phi) --subroutine
  -- columns are d1 d2 d3
    return {0            ,       0       ,  1,
            math.cos(phi), -math.sin(phi),  0,
            math.sin(phi),  math.cos(phi),  0
    }
    -- so here d3=ex, and (d1,d2) are rotated around ex
end


-- Main starts here
print()
print(' = * = * TOG-Paper-figure_3 * = * = ')
print()

-- Common variables
local totalMass = 0.8
local phi = -1.2 -- Frame orientation angle
local length = 2 -- total contour length set to 1 (adim protocol)
local width = 0.2
local thickness=0.002
local D=1.0 -- Bending rigidity set to 1 (adim protocol)
local nu=0.5 -- Poisson ratio
local Nelts=20  -- Number of elements
local clampedPosition = {0.0,0.0,0.0}  -- Position of the point at s=0

-- Ribbon
-- IChooseRibext()
local sribbon
ribbon = SuperRibbon.new()
ribbon.width=width -- width of the ribbon
ribbon.areaDensity=totalMass/(length*width) -- 1.0 -- total mass of the ribbon is 0.4
ribbon.thickness=thickness -- this is used by Mesher (drawing purpose, nothing physical)
ribbon.D=D -- bending rigidity set to 1 (adim protocol)
ribbon.position=clampedPosition
ribbon.frame=framePhi(phi) -- skewed frame to yield 3D unfolding under gravity
ribbon.poissonRatio=nu -- no role since no natural curvature
ribbon.numberSegments = Nelts -- number of segments along the ribbon
ribbon:setRibbonStart(0.0,0.0) -- kappa(s=0) et eta(s=0)
  -- we will start with a seed having kappa(s)= 2 pi and eta(s) = cos(2 pi s)
  -- the idea behind is "twisted circular ring", but it would not be a developpable configuration
  -- so this starting configuration is just a 3D 's-shaped' twisted ribbon
local kA = -5.0 -- linear term of the affine natural curvature
local kB = 0.0 -- constant term of the affine natural curvature
for i=0,ribbon.numberSegments-1,1 do
  local lengthi = length/ribbon.numberSegments
  local si = i * lengthi -- arc length at start of segment i
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,0.0,0.0) -- kappa'(si)=0 and eta'(si)=0
  ribbon:setNaturalCurvature(i,kA,kB) -- normal natural curvature
  kB = kB+kA*lengthi -- updating kB
end

sribbon = MixedSuperRibbon.new(ribbon) -- Use the mixed representation (positions, frame, and curvatures)
-- for i=0,ribbon.numberSegments-1,1 do
--   sribbon:setNatOmega1(i,-5*i,0) -- normal natural curvature
-- end

-- Fibre
fibre = Fibre.new()
fibre.position=clampedPosition  -- position of the point at s=0
fibre.frame=framePhi(phi) -- skewed frame to yield 3D unfolding under gravity
fibre.linearDensity=totalMass/length -- 0.2 -- so that total mass is like that of the ribbon, 0.4  
fibre.crossSection={width, thickness} --{ribbon.thickness, ribbon.width}
local K11 = D*width -- formula from our CMAME paper, eq. (18)
local K22 = 200 -- infinite value
local K33 = 2*K11/(1+nu) -- taking cJ = 1 (see eq. (18) from CMAME paper)
fibre:setElasticProperties({K11, 0.0, 0.0, 0.0, K22, 0.0, 0.0, 0.0, K33})
fibre:setNaturalOmegaInit(0.0, 0.0, 0.0) -- Constant part of the affine natural curvature of the first element (Omega.B)
fibre.numberSegments = Nelts -- number of segments along the fibre
for i=0,fibre.numberSegments-1,1 do
  local si = i * length/fibre.numberSegments -- arc length at start of segment i
  fibre:setLengthSegment(i,length/fibre.numberSegments)
  fibre:setNaturalOmegaDerivative(i,-5,0.0,0.0) -- Linear part of the natural curvature of each element (Omega.A), the constant parts are automatically filled in by continuity
end

mesher = Mesher.new("TOG-Paper-gravity.obj") -- use e.g. meshlab to open the obj file
mesher.precision=0.005
-- mesher:setColors({255,255,0},{0,255,255}) -- turquoise
-- mesher:add(sribbon) -- we draw the seed configuration (that is sent to the minimization algo)

-- IPOPT options
options = Options.new()
options.maxIters = 500
options.checkDerivation = false
options.tol = 1.e-9
options.logLevel=1 -- put 3 if you want info on each IP-OPT iteration

-- FIBRE AT REST
local exp_fiber_rest = FExperiment.new() -- minimization with super-clothoid model, rest position
exp_fiber_rest.name="TOG-Paper-rest-fibre"
exp_fiber_rest.gravity={0.,0.,0.}
exp_fiber_rest.fibre=fibre
exp_fiber_rest.options=options
exp_fiber_rest:findEquilibrium() -- IP-OPT is called here

-- FIBRE UNDER GRAVITY
local exp_fiber_eq = FExperiment.new() -- minimization with super-clothoid model, rest position
exp_fiber_eq.name="TOG-Paper-gravity-fibre"
exp_fiber_eq.gravity={0.,0.,-10.}
exp_fiber_eq.fibre=fibre
exp_fiber_eq.options=options
exp_fiber_eq:findEquilibrium() -- IP-OPT is called here

-- RIBBON AT REST
local exp_rest = MRFitter.new() -- minimization with mixed ribbon model, rest position
exp_rest.name="TOG-Paper-figure-rest-ribbon"
exp_rest.gravity={0.,0.,0.}
exp_rest.ribbon=sribbon
exp_rest.options=options
exp_rest:clampRuling(true, false) -- eta(0)=0
exp_rest:findEquilibrium() -- IP-OPT is called here

-- RIBBON UNDER GRAVITY
local exp_eq = MRFitter.new() -- minimization with mixed ribbon model, equilibrium configuration under gravity
exp_eq.name="TOG-Paper-gravity-ribbon"
exp_eq.gravity={0.,0.,-10.}
exp_eq.ribbon=sribbon
exp_eq.options=options
exp_eq:clampRuling(true, false) -- eta(0)=0
exp_eq:findEquilibrium() -- IP-OPT is called here


-- DRAWING

-- mesher:setColors({255,255,255},{220,220,220}) -- white
-- mesher:add(exp_fiber_rest.fibre) -- we draw the rest fibre configuration

mesher:setColors({250,146,119},{248,96,57}) -- orange
mesher:add(exp_fiber_eq.fibre) -- we draw the final fibre configuration

-- mesher:setColors({255,255,255},{220,220,220}) -- white
-- mesher:add(exp_rest.ribbon) -- we draw the rest ribbon configuration

mesher:setColors({42,148,227},{43,191,227}) -- turquoise
mesher:add(exp_eq.ribbon) -- we draw the final ribbon configuration

print()