function runExpe(name,sribbon,endPos,endFrame)
  print("=============================================================================")
  print(name)
  print("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *")
  options = Options.new()
  options.maxIters = 1000
  options.checkDerivation = true
  options.tol = 1.e-12
  options.logLevel=5
 

  
  local exp = MRFitter.new()
  local ribbon = MixedSuperRibbon.new(sribbon)
--   local exp = Experiment.new()
--   local ribbon = sribbon
  exp.name="test101"
  exp.gravity={0.,0.,1.}
  exp.ribbon=ribbon
  exp.options=options
  --exp:attachEnd(endPos, endFrame)
  --exp:addOffPlaneDoubleEnd({0,0,0},{0,0,1})
  exp:addOffPlane({0,0,0},{0,0,1})
  --exp:useLooseClamping({1,2,3},10)
  exp:detachOrClamping()
  --exp:declareRibbonCyclic(true)
  
  print ("::::: 1")
  MRCoeffs(1.,1.)
  exp:findEquilibrium()
  local stats=exp.stats
  print("lambda : "..table.concat(stats.lambda,", "))
end

-- ILoveSadowsky(false)


ribbon = SuperRibbon.new()

function frameTheta(theta)
      return {
              0, -math.sin(theta), math.cos(theta),
              1,               0,               0,
              0, math.cos(theta), math.sin(theta)}
    end
function frameThetaPhi(theta,phi)
    local sinTheta=math.sin(theta)
    local cosTheta=math.cos(theta)
    local sinPhi=math.sin(phi)
    local cosPhi=math.cos(phi)
    return {-sinPhi*sinTheta, -cosPhi*sinTheta, cosTheta,
              cosPhi,         -sinPhi,         0,
              sinPhi*cosTheta, cosPhi*cosTheta, sinTheta  
    }
end
    
X=function(t) return math.sin(t)/t end
Y=function(t) return (math.cos(math.pi+t)+1)/t end

ribbon.width=0.01
ribbon.thickness=0.0001
length = 1
ribbon.areaDensity=10
ribbon.D=1
ribbon.position={0.,0.,0.}
ribbon.frame=frameTheta(0.)
ribbon.poissonRatio=0.5
ribbon.numberSegments = 5;
ribbon:setRibbonStart(-math.pi*2./length,0.01)
for i=0,ribbon.numberSegments-1,1 do
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,0.0001,0.0001)
  ribbon:setNaturalCurvature(i,0,0)
end

runExpe("Protocol1", ribbon, {0,0,0}, frameThetaPhi(math.pi/3.,math.pi/4.))
