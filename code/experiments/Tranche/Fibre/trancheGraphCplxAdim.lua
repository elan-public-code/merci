fibre = Fibre.new()

fibre.crossSection={0.005,0.0001}
length = 1
fibre.linearDensity=0.2*0.005
fibre:setElasticProperties({1.,0.,0.,
                            0.,1e6,0.,
                            0.,0.,2.})
fibre.position={0.,0.,0.}
fibre.frame={1, 0, 0,
              0, 1, 0,
              0, 0, 1}
fibre.numberSegments = 20;

fibre:setOmegaInit(0,0,0)
for i=0,fibre.numberSegments-1,1 do
  fibre:setLengthSegment(i,length/fibre.numberSegments)
  fibre:setOmegaDerivative(i,0,0,0)
end

options = Options.new()
options.maxIters = 1250
options.checkDerivation = false
options.tol = 1.e-10
options.logLevel = 0

exp = FExperiment.new()
exp.name="test101"
exp.gravity={1,0.,0.}
exp.fibre=fibre
exp.options=options



function run(experiment, linearDensity, varmul)
  local w=varmul*1./length
  experiment.fibre:setOmegaInit(w,w,w)
  experiment.fibre.linearDensity=linearDensity
  for i=0,experiment.fibre.numberSegments-1,1 do
    experiment.fibre:setLengthSegment(i,length/fibre.numberSegments)
    experiment.fibre:setOmegaDerivative(i,0,0,0)
  end
  experiment:findEquilibrium()
  local fpoint=experiment.fibre.point
  local gamma=linearDensity
  local line = gamma..", "..fpoint[1]..", "..fpoint[2]..", "..fpoint[3]
  --[[mesher = Mesher.new("fibreI"..length..".obj")
  mesher.precision=0.001
  mesher:setColors({255,255,0},{0,255,255})
  mesher:add(experiment.fibre)]]
  return line
end

file = io.open("trancheGraphCplxPlxLin.csv","w")
for l = 1, 30, 0.2 do 
  file:write(run(exp,l,-1).."\n")
  file:write(run(exp,l,0).."\n")
  file:write(run(exp,l,1).."\n")
  file:flush()
end
file:close()
