ILoveSadowsky(true) -- commenter pour faire wunderlich
ribbon = SuperRibbon.new()

ribbon.width=0.005
ribbon.thickness=0.0001
length = 0.149
length = 0.148
ribbon.areaDensity=0.2
ribbon.D=0.0002
ribbon.position={0.,0.,0.}
ribbon.frame={1, 0, 0,
              0, 1, 0,
              0, 0, 1}
ribbon.poissonRatio=0.5
ribbon.numberSegments = 1;

lcrit=math.pow(0.5,1./6.)*math.pow((48.*ribbon.D)/(ribbon.areaDensity*9.81),1./3.)
gammacrit=48./math.sqrt(2)
gamma=ribbon.areaDensity*9.81*math.pow(length,3)/ribbon.D

paramsline="lcrit="..lcrit..", length="..length..", gammacrit="..gammacrit..", gamma="..gamma
print(paramsline)

--n=-12*ribbon.D/(9.81*length*length*length*ribbon.areaDensity)
--n=n*0.1
--print(n)
n=-0.1
w=1

ribbon:setRibbonStart(w,n)
ribbon:setRibbonPiece(0,length,0,0)

ribbonOri = ribbon:copy()

options = Options.new()
options.maxIters = 1250
options.checkDerivation = false
options.tol = 1.e-10

exp = Experiment.new()
exp.name="test101"
exp.gravity={9.81,0.,0.}
exp.ribbon=ribbon
exp.options=options

arrow = Arrow.new()
arrow.position={0,0,0}
arrow.direction=exp.gravity
arrow.color={255,125,0}
arrow.resolution=20
arrow:setupDimensions(0.1,0.01,0.005,0.01)

--exp:findEquilibrium()
exp:gradientDescent()
local fpoint=ribbon:getPoint()
for i = 1, #fpoint do 
  print(fpoint[i]..", ")
end
local fpoint=exp.ribbon:getPoint()
for i = 1, #fpoint do 
  print(fpoint[i]..", ")
end

mesher = Mesher.new("tranche.obj")
mesher.precision=0.001
mesher:setColors({255,0,0},{0,0,255})
mesher:add(ribbon)
mesher.precision=0.001
mesher:setColors({0,0,255},{255,0,255})
mesher:add(exp.ribbon)
mesher:add(arrow)

print(paramsline)
