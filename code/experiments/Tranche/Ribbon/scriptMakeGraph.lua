ILoveSadowsky(true) -- commenter pour faire wunderlich
ribbon = SuperRibbon.new()

ribbon.width=0.005
ribbon.thickness=0.0001
length = 0.149
ribbon.areaDensity=0.2
ribbon.D=0.0002
ribbon.position={0.,0.,0.}
ribbon.frame={1, 0, 0,
              0, 1, 0,
              0, 0, 1}
ribbon.poissonRatio=0.5
ribbon.numberSegments = 1;

n=-0.1
w=10
ribbon:setRibbonStart(w,n)
ribbon:setRibbonPiece(0,length,0,0)

options = Options.new()
options.maxIters = 1250
options.checkDerivation = false
options.tol = 1.e-10
options.logLevel = 5

exp = Experiment.new()
exp.name="test101"
exp.gravity={9.81,0.,0.}
exp.ribbon=ribbon
exp.options=options

function theoricPred(gamma)
  local gammacrit=48./math.sqrt(2)
  if gamma<gammacrit then 
    return 0.0
  else 
    return 1.0
  end
end
      

function run(experiment, length)
  experiment.ribbon:setRibbonStart(w,n)
  experiment.ribbon:setRibbonPiece(0,length,0,0)
  experiment:findEquilibrium()
  local fpoint=experiment.ribbon:getPoint()
  local gamma=experiment.ribbon.areaDensity*9.81*math.pow(length,3)/experiment.ribbon.D
  local line = gamma..", "..fpoint[1]..", "..fpoint[2]..", "..theoricPred(gamma)
  return line
end

file = io.open("trancheGraph3.csv","w")
for l = 0.1, 0.2, 0.001 do 
  file:write(run(exp,l).."\n")
  file:flush()
end
file:close()
