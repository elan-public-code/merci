ILoveSadowsky(true) -- commenter pour faire wunderlich
ribbon = SuperRibbon.new()

ribbon.width=0.005
ribbon.thickness=0.0001
length = 0.149
ribbon.areaDensity=0.2
ribbon.D=0.0002
ribbon.position={0.,0.,0.}
ribbon.frame={1, 0, 0,
              0, 1, 0,
              0, 0, 1}
ribbon.poissonRatio=0.5
ribbon.numberSegments = 20;

n=-0.1
w=10
ribbon:setRibbonStart(w,n)
for i=0,ribbon.numberSegments-1,1 do
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,0,0)
end

options = Options.new()
options.maxIters = 1250
options.checkDerivation = false
options.tol = 1.e-10
options.logLevel = 5

exp = Experiment.new()
exp.name="test101"
exp.gravity={9.81,0.,0.}
exp.ribbon=ribbon
exp.options=options

function theoricPred(gamma)
  local gammacrit=48./math.sqrt(2)
  if gamma<gammacrit then 
    return 0.0
  else 
    return 1.0
  end
end
      

function run(experiment, length)
  experiment.ribbon:randomizeShape()
--   local fpoint=experiment.ribbon:getPoint()
--   for i=1,#fpoint,2 do
--     fpoint[i]=fpoint[i]/(2.*6.3)*length
--     fpoint[i+1]=fpoint[i+1]/10
--   end
--   experiment.ribbon:setPoint(fpoint)
  experiment:findEquilibrium()
  local fpoint=experiment.ribbon:getPoint()
  local gamma=experiment.ribbon.areaDensity*9.81*math.pow(length,3)/experiment.ribbon.D
  local line = gamma..", "..fpoint[1]..", "..fpoint[2]..", "..theoricPred(gamma)
  return line
end

file = io.open("trancheGraphAlea2.csv","w")
for l = 0.1, 0.2, 0.001 do 
  file:write(run(exp,l).."\n")
  file:flush()
end
file:close()

arrow = Arrow.new()
arrow.position={0,0,0}
arrow.direction=exp.gravity
arrow.color={255,125,0}
arrow.resolution=20
arrow:setupDimensions(0.1,0.01,0.005,0.01)
mesher = Mesher.new("tranche.obj")
mesher.precision=0.001
mesher:setColors({255,0,0},{0,0,255})
mesher:add(ribbon)
mesher.precision=0.001
mesher:setColors({0,0,255},{255,0,255})
mesher:add(exp.ribbon)
mesher:add(arrow)
