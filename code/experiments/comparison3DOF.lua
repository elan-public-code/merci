ILoveSadowsky(false)


ribbon = SuperRibbon.new()

function frameTheta(theta)
      return {
              0, -math.sin(theta), math.cos(theta),
              1,               0,               0,
              0, math.cos(theta), math.sin(theta)}
    end
function frameThetaPhi(theta,phi)
    local sinTheta=math.sin(theta)
    local cosTheta=math.cos(theta)
    local sinPhi=math.sin(phi)
    local cosPhi=math.cos(phi)
    return {-sinPhi*sinTheta, -cosPhi*sinTheta, cosTheta,
              cosPhi,         -sinPhi,         0,
              sinPhi*cosTheta, cosPhi*cosTheta, sinTheta  
    }
end
    
X=function(t) return math.sin(t)/t end
Y=function(t) return (math.cos(math.pi+t)+1)/t end

ribbon.width=0.01
ribbon.thickness=0.0001
length = 1
ribbon.areaDensity=0
ribbon.D=1
ribbon.position={0.,0.,0.}
ribbon.frame=frameTheta(0.)
ribbon.poissonRatio=0.5
ribbon.numberSegments = 10;
ribbon:setRibbonStart(-math.pi*2./length,0.01)
for i=0,ribbon.numberSegments-1,1 do
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,0.0001,0.0001)
  ribbon:setNaturalCurvature(i,0,0)
end

ribbonOri = ribbon:copy()

options = Options.new()
options.maxIters = 1000
options.checkDerivation = false
options.tol = 1.e-8
options.logLevel=3

exp = Experiment.new()
exp.name="test101"
exp.gravity={0.,0.,9.81}
exp.ribbon=ribbon
exp.options=options
exp:attachEnd({0,0,0}, frameTheta(0))
exp:use3dotAlignementConstraint(false)

exp:findEquilibrium()

mesher = Mesher.new("Comparison2.obj")
mesher.precision=0.005
mesher:setColors({255,255,0},{0,255,255})
mesher:add(ribbon)
mesher:setColors({255,0,0},{0,0,255})
mesher:add(exp.ribbon)
mesher:setColors({125,0,255},{255,0,125})
exp.ribbon:printPoint()



exp:attachEnd({0,0,0},frameThetaPhi(0,math.pi/4.))
exp:findEquilibrium()
exp:attachEnd({0,0,0},frameThetaPhi(0,math.pi*2./4.))
exp:findEquilibrium()
exp:attachEnd({0,0,0},frameThetaPhi(0,math.pi*3./4.))
exp:findEquilibrium()
exp:attachEnd({0,0,0},frameThetaPhi(0,math.pi))
exp:findEquilibrium()
mesher:add(exp.ribbon)

--Avec 12 contraintes : 14s
--Avec 6  contraintes : 15s
