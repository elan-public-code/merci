ILoveSadowsky(false)

ribbon = SuperRibbon.new()

ribbon.width=0.02
ribbon.thickness=0.0001
length = 1
ribbon.areaDensity=100
ribbon.D=1
ribbon.position={0.,0.,0.}
ribbon.frame={-0.707106781187, 0.707106781187, 0,
          0,              0,               -1,
          -0.707106781187, -0.707106781187,  0}
ribbon.poissonRatio=0.5
ribbon.numberSegments = 50;
ribbon:setRibbonStart(0.,0.)
for i=0,ribbon.numberSegments-1,1 do
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,40,0)
  ribbon:setNaturalCurvature(i,40,40*i*length/ribbon.numberSegments)
end


options = Options.new()
options.maxIters = 1000
options.checkDerivation = false
options.tol = 1.e-12

mesher = Mesher.new("comparison3r.obj")
mesher.precision=0.005

exp = Experiment.new()
exp.name="test101"
exp.gravity={0.,0.,-1}
exp.ribbon=ribbon
exp.options=options

exp:findEquilibrium()
mesher:setColors({0,125,0},{125,255,0})
mesher:add(exp.ribbon)

msr=MixedSuperRibbon.new(exp.ribbon)
--msr.width=0.02
--[[for i=0,ribbon.numberSegments-1,1 do
  msr:setNatOmega1(i,40,40*i*length/ribbon.numberSegments)
end]]

exp = MRFitter.new()
exp.name="test101"
exp.gravity={0.,0.,1}
exp.ribbon=msr
exp.options=options

exp:findEquilibrium()

mesher:setColors({255,255,0},{0,255,255})
mesher:add(ribbon)
mesher:setColors({255,0,0},{0,0,255})
mesher:add(exp.ribbon)

