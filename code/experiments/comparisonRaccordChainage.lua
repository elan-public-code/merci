ILoveSadowsky(false)

ribbon = SuperRibbon.new()

function frameTheta(theta)
      return {
              0,              0,               1.,
              math.sin(theta), math.cos(theta),0.,
              -math.cos(theta), math.sin(theta),0.}
    end

    
X=function(t) return math.sin(t)/t end
Y=function(t) return (math.cos(math.pi+t)+1)/t end

ribbon.width=0.01
ribbon.thickness=0.0001
length = 1
ribbon.areaDensity=100
ribbon.D=1
ribbon.position={0.,0.,0.}
ribbon.frame=frameTheta(math.pi/4)
ribbon.poissonRatio=0.5
ribbon.numberSegments = 20;
ribbon:setRibbonStart(10.0,0.00)
for i=0,ribbon.numberSegments-1,1 do
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,0.000,0.000)
  ribbon:setNaturalCurvature(i,0,10)
end

msr=MixedSuperRibbon.new(ribbon)

options = Options.new()
options.maxIters = 1000
options.checkDerivation = false
options.tol = 1.e-12

exp = MRFitter.new()
exp.name="test101"
exp.gravity={0.,0.,1}
exp.ribbon=msr
exp.options=options

exp:findEquilibrium()

mesher = Mesher.new("testCircle2.obj")
mesher.precision=0.005
mesher:setColors({255,255,0},{0,255,255})
mesher:add(ribbon)
mesher:setColors({255,0,0},{0,0,255})
mesher:add(exp.ribbon)

exp = Experiment.new()
exp.name="test101"
exp.gravity={0.,0.,-1}
exp.ribbon=ribbon
exp.options=options

exp:findEquilibrium()
mesher:setColors({0,125,0},{125,255,0})
mesher:add(exp.ribbon)
