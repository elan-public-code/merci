--------------------------------------------------------------------------------
-- pour tracer les frames --
----------------------------
function drawFrame(pos, frame, scale, mesher)
  local ox=Arrow.new()
  ox.position=pos
  ox.direction={frame[1],frame[4],frame[7]}
  ox.color={255,0,0}
  ox.name="frameX"
  ox:setupDimensions(scale,0.15*scale,0.025*scale,0.05*scale)
  local oy=Arrow.new()
  oy.position=pos
  oy.direction={frame[2],frame[5],frame[8]}
  oy.color={0,255,0}
  oy.name="frameY"
  oy:setupDimensions(scale,0.15*scale,0.025*scale,0.05*scale)
  local oz=Arrow.new()
  oz.position=pos
  oz.direction={frame[3],frame[6],frame[9]}
  oz.color={0,0,255}
  oz.name="frameZ"
  oz:setupDimensions(scale,0.15*scale,0.025*scale,0.05*scale)
  mesher:add(ox)
  mesher:add(oy)
  mesher:add(oz)
end

function drawTwoFrame(pos1, frame1, pos2, frame2, scale, mesher)
  drawFrame(pos1, frame1, scale, mesher)
  drawFrame(pos2, frame2, scale, mesher)
end
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- 1ere colonne : d1 , etc...
function frameTheta(theta)
 return {
              0,              0,               1.,
              math.sin(theta), math.cos(theta),0.,
              -math.cos(theta), math.sin(theta),0.
              }
 end
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
 function framePhiL(phi)
  return {
               -math.cos(phi), math.sin(phi)  ,  0. ,
                math.sin(phi), math.cos(phi)  ,  0. ,
                    0        ,       0        , -1.
               }
  end
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
ILoveSadowsky(false)

ribbon = SuperRibbon.new()
ribbon.width=0.01
ribbon.thickness=0.0001
length = 1
ribbon.areaDensity=1.1
ribbon.D=1
ribbon.position={0.,0.,0.}
ribbon.frame=frameTheta(0.5*math.pi)
ribbon.poissonRatio=0.5
ribbon.numberSegments = 100;
ribbon:setRibbonStart(1.50,0.00)
for i=0,ribbon.numberSegments-1,1 do
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,0.000,0.000)
  ribbon:setNaturalCurvature(i,0,0)
end

mesher = Mesher.new("Comparison_g75.obj")
mesher.precision=0.005
mesher:setColors({255,255,0},{0,255,255}) -- jaune/cyan : graine
mesher:add(ribbon)

arrivalpoint={0.7,0,-0.5}
arrivalangle=0.25*math.pi  -- 0.5 pi : sans twist , on veut 0.25 pi
drawTwoFrame(ribbon.position,ribbon.frame,arrivalpoint,framePhiL(arrivalangle),0.1,mesher)


options = Options.new()
options.maxIters = 500
options.checkDerivation = false
options.tol = 1.e-8




msr=MixedSuperRibbon.new(ribbon)


exp = MRFitter.new() -- raccord coeff
exp.name="test101"
exp.gravity={0.,0.,1}
exp.ribbon=msr
exp.options=options
-- exp:attachEnd(arrivalpoint,framePhiL(arrivalangle))
-- --exp:clampRuling(true,true)
-- exp:findEquilibrium()
-- mesher:setColors({255,0,0},{0,0,255}) -- rouge/bleu : raccord coeff
-- mesher:add(exp.ribbon)


--[[exp = Experiment.new() -- chainage
exp.name="test101"
exp.gravity={0.,0.,-1}
exp.ribbon=ribbon
exp.options=options
]]

arrivalangle=0.4*math.pi  -- 0.5 pi : sans twist , on veut 0.25 pi
exp:attachEnd(arrivalpoint,framePhiL(arrivalangle))
exp:findEquilibrium()
print( " * * * * * * ")
--exp.ribbon:printPoint()
print( " * * * * * * ")
mesher:setColors({0,125,0},{125,255,0}) -- vert/vert : chainage
mesher:add(exp.ribbon)



arrivalangle=0.35*math.pi  -- 0.5 pi : sans twist , on veut 0.25 pi
exp:attachEnd(arrivalpoint,framePhiL(arrivalangle))
exp:findEquilibrium()
print( " * * * * * * ")
--exp.ribbon:printPoint()
print( " * * * * * * ")
exp:findEquilibrium()
mesher:setColors({0,125,0},{125,255,0}) -- vert/vert : chainage
mesher:add(exp.ribbon)
