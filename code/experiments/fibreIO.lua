--Read the parameters for a fiber and run the simulation
--the input file param.dat should contain line by line :
--theta, the width of the fiber, the thickness of the fiber, the linear density, EI1,E12,muJ, the length of the fiber, kappa0
--NOTE : the gravity is set to ez. (For ribbon, it was 9.81ez)
--the input file xk should specify line by line:
--omega_1(0),omega_2(0),omega_3(0), then on each segment, the derivatives omega_1',omega_2',omega_3'

io.input("param.dat")
local theta = io.read("*number")
local width = io.read("*number")
local thickness = io.read("*number")
local linearDensity = io.read("*number")
local K1 = io.read("*number")
local K2 = io.read("*number")
local K3 = io.read("*number")
local totallength = io.read("*number")
local kappa0 = io.read("*number")

io.input("xk.dat")
local state = {}
for line in io.lines() do
  table.insert(state,tonumber(line))
end
local numberSegments = (#state-3)/3

fibre = Fibre.new()
 
fibre.crossSection={width,thickness}
fibre.linearDensity=linearDensity
fibre.position={0.,0.,0.}
fibre.frame={      0,                0, 1,
    -math.sin(theta), -math.cos(theta), 0,
     math.cos(theta), -math.sin(theta), 0}
fibre:setElasticProperties({K1,0.,0.,
                            0.,K2,0.,
                            0.,0.,K3})
fibre.numberSegments = numberSegments
fibre:setNaturalOmegaInit(kappa0, 0., 0.)
fibre:setOmegaInit(state[1],state[2],state[3])
for i=0,fibre.numberSegments-1,1 do
 fibre:setLengthSegment(i,totallength/fibre.numberSegments)
 fibre:setOmegaDerivative(i,state[4+3*i],state[5+3*i],state[6+3*i])
end

fibreOri = fibre:copy()
 
options = Options.new()
options.maxIters = 10000
options.tol = 1.e-12
 
exp = FExperiment.new()
exp.name="first fibre"
exp.gravity={0.,0.,1.}
exp.fibre=fibre
exp.options=options

mesher = Mesher.new("fibreI.Oobj")
mesher.precision=0.001
mesher:setColors({255,255,0},{0,255,255})
mesher:add(fibre)
exp:newtonC()--ou alors newton() ou alors gradientDescent
mesher:setColors({255,0,0},{0,0,255})
mesher:add(exp.fibre)
