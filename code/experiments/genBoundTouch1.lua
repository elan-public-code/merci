ribbon = SuperRibbon.new()

ribbon.width=0.02
ribbon.thickness=0.0001
length = 0.3
ribbon.areaDensity=0.2
ribbon.D=0.0002
ribbon.position={0.,0.,0.}
ribbon.frame={-0.707106781187, 0.707106781187, 0,
          0,              0,               -1,
          -0.707106781187, -0.707106781187,  0}
ribbon.poissonRatio=0.5
ribbon.numberSegments = 40;
ribbon:setRibbonStart(-80.,0.)
for i=0,ribbon.numberSegments-1,1 do
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,0,0)
  ribbon:setNaturalCurvature(i,100,100*i*length/ribbon.numberSegments)
end

ribbonOri = ribbon:copy()

options = Options.new()
options.maxIters = 100
options.checkDerivation = false
options.tol = 1.e-12

exp = Experiment.new()
exp.name="test101"
exp.gravity={0.,0.,9.81}
exp.ribbon=ribbon
exp.options=options

exp:findEquilibrium()

mesher = Mesher.new("BoundTouch1.obj")
mesher.precision=0.001
mesher:setColors({255,255,0},{0,255,255})
mesher:add(ribbon)
mesher:setColors({255,0,0},{0,0,255})
mesher:add(exp.ribbon)
