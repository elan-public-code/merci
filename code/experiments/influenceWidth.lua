ribbon = SuperRibbon.new()

ribbon.width=0.01
ribbon.thickness=0.0001
length = 0.3
ribbon.areaDensity=0.132
ribbon.D=0.0002
ribbon.position={0.,0.,0.}
ribbon.frame={0, 0, 1,
              0, -1, 0,
              1, 0, 0}
ribbon.poissonRatio=0.48
ribbon.numberSegments = 20;
ribbon:setRibbonStart(0.,0.)
for i=0,ribbon.numberSegments-1,1 do
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,0,0)
  ribbon:setNaturalCurvature(i,0,62.8)
end

ribbonOri = ribbon:copy()

options = Options.new()
options.maxIters = 100
options.checkDerivation = false
options.tol = 1.e-12

exp = Experiment.new()
exp.name="angl"
exp.gravity={0.,0.,9.81}
exp.ribbon=ribbon
exp.options=options

mesher = Mesher.new("influenceWidth.obj")
mesher.precision=0.001
mesher:setColors({255,255,0},{0,255,255})

for i=1,4,1 do
  ratio = math.pow(2,i)
  exp.ribbon.width = ribbonOri.width/ratio;
  exp.ribbon.D = ribbonOri.D*ratio;
  exp.ribbon.areaDensity = ribbon.areaDensity *ratio
  exp.ribbon.thickness = ribbonOri.thickness*(1+0.05*i);
  exp:findEquilibrium()

  mesher:setColors({255-50*i,0,50*i},{255-50*i,0,50*i})
  mesher:add(exp.ribbon)
end
