
function setupRibbon(K, gamma)
  local ribbon = SuperRibbon.new()

  ribbon.width=0.005
  ribbon.thickness=0.0001
  length = 1.
  ribbon.areaDensity=gamma 
  ribbon.D=1
  ribbon.position={0.,0.,0.}
  local t=math.pi/2
  ribbon.frame={0, 0, 1,
                math.cos(t), -math.sin(t), 0,
                math.sin(t), math.cos(t), 0}
  ribbon.poissonRatio=0.5
  ribbon.numberSegments = 20;
  ribbon:setRibbonStart(0.,0.)
  --local K=0.5*2.*math.pi
  for i=0,ribbon.numberSegments-1,1 do
    ribbon:setRibbonPiece(i,length/ribbon.numberSegments,0,0)
    ribbon:setNaturalCurvature(i,0,-K)
  end
  return ribbon
end

function runExpe(saveMesh, K, gamma, file)
  
  options = Options.new()
  options.maxIters = 1000
  options.checkDerivation = false
  options.tol = 1.e-12
  options.logLevel=0
  
  exp = Experiment.new()
  exp.name="test101"
  exp.gravity={0.,0.,1}
  exp.ribbon=setupRibbon(K*2.*math.pi, gamma)
  exp.options=options
  if saveMesh then
    local mesher = Mesher.new("testMesh.obj")
    mesher.precision=0.001
    mesher:setColors({255,255,0},{0,255,255})
    mesher:add(exp.ribbon)
    mesher:setColors({255,0,0},{0,0,255})
    arrow = Arrow.new()
    arrow.position={0,0,0}
    arrow.direction=exp.gravity
    arrow:setupDimensions(0.05,0.01,0.002,0.004)
    arrow.color={0,0,0}
    mesher:add(arrow)
  end

  
  --[[for nu =0.1,0.5,0.1 do
    exp.ribbon.poissonRatio=nu
    exp:findEquilibrium()
    if saveMesh then
      mesher:add(exp.ribbon)
    end
    print(nu.." "..exp.ribbon:endPos()[3])
  end]]
  
  exp.ribbon.poissonRatio=0.1
  exp:findEquilibrium()
  local h1=exp.ribbon:endPos()[3]
  exp.ribbon.poissonRatio=0.5
  exp:findEquilibrium()
  local h2=exp.ribbon:endPos()[3]
  file:write(K.." "..(h2-h1),"\n")
end

--exp.ribbon:genDiscreteDescriptor(0.1,"out.json")
function protocol(gamma)
  local file=io.open("nucirc"..gamma,"w")
  for a=0.0,2,0.05 do
    runExpe(false, a, gamma, file)
    print (a/2)
  end
  io.close(file)
end

for i=30,90,10 do
  protocol(i)
end
