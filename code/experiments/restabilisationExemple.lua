fibre = Fibre.new()
fibre.position={0,0,0}
fibre.frame={0,0,1,
             0,1,0,
            -1,0,0}
fibre.linearDensity=100
local h=0.01
fibre.crossSection={h,h}
fibre.numberSegments=20
fibre:setElasticProperties({1,0,0,0,1,0,0,0,1})
for i=0,fibre.numberSegments-1,1 do
  fibre:setLengthSegment(i,1./fibre.numberSegments)
end
fibre:setNaturalOmegaInit(1,0,0)
fibre:setOmegaInit(1,0.1,-0.1)

options = Options.new()
options.maxIters = 1000
options.checkDerivation = false
options.tol = 1.e-12
options.logLevel=0
  
exp = FExperiment.new()
exp.gravity={0,-1,0}
-- exp.fibre=fibre
exp.options=options

-- exp:findEquilibrium()
  
local mesher = Mesher.new("testRestab.obj")
mesher.precision=0.005
for K=1,50,0.25 do
  print(K.."/"..50)
  fibre:setNaturalOmegaInit(K,0,0)
  fibre:setOmegaInit(K,5,-5)
  exp.fibre=fibre
  exp:findEquilibrium()
--   mesher:setColors({255,255,0},{0,255,255})
--   mesher:add(fibre)
  mesher:setColors({255,0,0},{0,0,255})
  mesher:add(exp.fibre)
end
arrow = Arrow.new()
arrow.position={0.3,0,0}
arrow.direction=exp.gravity
arrow:setupDimensions(0.5,0.1,0.02,0.04)
arrow.color={0,0,0}
mesher:add(arrow)
