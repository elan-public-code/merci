function addConstraintOffPlane(exp, P, N)
  --exp:addOffPlane(P,N)
  exp:addOffPlaneDoubleEnd(P,N)
end

function doubleRun(exp)
  print(" ")
  print("=================================================")
  print("=================================================")
  print(" ")
  exp.options.logLevel=3
  exp:findEquilibrium()
  print("Midrun status: "..exp.stats.status)
  exp.options.logLevel=3
  while(exp.stats.iterations == exp.options.maxIters) do
    exp:findEquilibrium()
    print("Midrun status: "..exp.stats.status)
  end
end

local frame1={0, 0, 1,
              0, 1, 0, 
             -1, 0, 0}
local G = {0.,-1.,0.}
local pos1={0.,0.1,0.}
ribbon = MixedSuperRibbon.new()
ribbon.width=0.1
ribbon.thickness=0.0001
length = 1
ribbon.areaDensity=10
ribbon.D=1
ribbon.poissonRatio=0.5
ribbon.numberSegments = 50;
ribbon.position=pos1
ribbon.frame=frame1
for i=0,ribbon.numberSegments-1,1 do
  ribbon:setLength(i,1./ribbon.numberSegments)
  ribbon:setMiniSys(i, {0.0001,0.00001,0.0001,0.00001})
  end
ribbon:reChain()
local options = Options.new()
options.maxIters = 100
options.checkDerivation = false
options.tol = 1.e-9
options.logLevel=3

local exp = MRFitter.new()
exp.gravity=G
exp.ribbon=ribbon
exp.options=options
addConstraintOffPlane(exp, {0,0.1001,0}, {0,-1,0})
exp:attachEnd({0.8,0.1,0.}, ribbon.frame)
--exp:clampRuling(true, true)

local mesher = Mesher.new("rides.obj")
mesher.precision=0.005
mesher:setColors({255,255,0},{0,255,255})
mesher:add(ribbon)
mesher:setColors({255,0,0},{0,0,255})

local aG=Arrow.new()
  aG.position=ribbon.position
  aG.direction=G
  aG.color={0,255,0}
  aG.name="G"
  scale=0.05
  aG:setupDimensions(scale,0.15*scale,0.025*scale,0.05*scale)
  mesher:add(aG)
  
  
-- exp:clearOffPlanes()
-- addConstraintOffPlane(exp,{0,0.1001,0},{0,-1,0})
doubleRun(exp)
mesher:add(exp.ribbon)
  
addConstraintOffPlane(exp,{0,-0.1,0},{0,1,0})
doubleRun(exp)
mesher:add(exp.ribbon)

addConstraintOffPlane(exp,{0,-0.05,0},{0,1,0})
doubleRun(exp)
mesher:add(exp.ribbon)

exp:clearOffPlanes()
addConstraintOffPlane(exp,{0,0.1001,0},{0,-1,0})
addConstraintOffPlane(exp,{0,0,0},{0,1,0})
doubleRun(exp)
mesher:add(exp.ribbon)

exp:attachEnd({0.7,0.1,0.}, ribbon.frame)
doubleRun(exp)
mesher:add(exp.ribbon)
