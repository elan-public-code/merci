ILoveSadowsky(true)

ribbon = SuperRibbon.new()
local frame1={
              0, 1, 0,
             -1, 0, 0,
              0, 0, 1}
local frame2={
              0, 0,-1,
              -math.sqrt(0.5),   math.sqrt(0.5),0,
              math.sqrt(0.5), math.sqrt(0.5),0}

ribbon.width=0.03
ribbon.thickness=0.0001
length = 1
ribbon.areaDensity=10
ribbon.D=1
ribbon.position={0.,0.,0.}
ribbon.frame=frame1
ribbon.poissonRatio=0.4
ribbon.numberSegments = 20;
ribbon:setRibbonStart(-0.02,0.01)
for i=0,ribbon.numberSegments-1,1 do
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,0.0001,0.0001)
  ribbon:setNaturalCurvature(i,0,0)
end

ribbonOri = ribbon:copy()

options = Options.new()
options.maxIters = 1000
options.checkDerivation = false
options.tol = 1.e-9
options.logLevel=5

exp = MRFitter.new()
--exp=Experiment.new()
exp.name="test101"
exp.gravity={0.,0.,1}
exp.ribbon=MixedSuperRibbon.new(ribbon)
exp.options=options
exp:attachEnd({-0.5,0,0.7}, frame2)
--exp:use3dotAlignementConstraint(false)

exp:findEquilibrium()

mesher = Mesher.new("G75.obj")
mesher.precision=0.005
mesher:setColors({255,255,0},{0,255,255})
mesher:add(ribbon)
mesher:setColors({255,0,0},{0,0,255})
mesher:add(exp.ribbon)
