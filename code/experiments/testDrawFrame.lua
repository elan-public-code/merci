function drawFrame(pos, frame, scale, mesher)
  local ox=Arrow.new()
  ox.position=pos
  ox.direction={frame[1],frame[4],frame[7]}
  ox.color={255,0,0}
  ox.name="frameX"
  ox:setupDimensions(scale,0.15*scale,0.025*scale,0.05*scale)
  local oy=Arrow.new()
  oy.position=pos
  oy.direction={frame[2],frame[5],frame[8]}
  oy.color={0,255,0}
  oy.name="frameY"
  oy:setupDimensions(scale,0.15*scale,0.025*scale,0.05*scale)
  local oz=Arrow.new()
  oz.position=pos
  oz.direction={frame[3],frame[6],frame[9]}
  oz.color={0,0,255}
  oz.name="frameZ"
  oz:setupDimensions(scale,0.15*scale,0.025*scale,0.05*scale)
  mesher:add(ox)
  mesher:add(oy)
  mesher:add(oz)
end

function drawTwoFrame(pos1, frame1, pos2, frame2, scale, mesher)
  drawFrame(pos1, frame1, scale, mesher)
  drawFrame(pos2, frame2, scale, mesher)
end

-- function frameThetaPhi(theta,phi)
--     local sinTheta=math.sin(theta)
--     local cosTheta=math.cos(theta)
--     local sinPhi=math.sin(phi)
--     local cosPhi=math.cos(phi)
-- 
--     return {
--     -sinPhi*sinTheta , -cosPhi*sinTheta , cosTheta,
--               cosPhi ,         -sinPhi  ,        0,
--      sinPhi*cosTheta , cosPhi*cosTheta  , sinTheta
--     }
-- end
-- 
-- X=function(t) return math.sin(t)/t end
-- Y=function(t) return (math.cos(math.pi+t)+1)/t end
-- 
-- mesher = Mesher.new("testframe.obj")
-- 
-- for i=0,20,1 do
--   t=(math.pi*2.0*i)/20
--   print("Step "..i..", t="..t)
--   
--   drawFrame({X(t),0,Y(t)},frameThetaPhi(t,t),0.1,mesher)
-- end 
