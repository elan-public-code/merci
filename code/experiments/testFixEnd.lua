ILoveSadowsky(true)
ribbon = SuperRibbon.new()

function frameTheta(theta)
      return {0, 0, 1,
              math.cos(theta), math.sin(theta), 0,
             -math.sin(theta), math.cos(theta), 0}
    end

ribbon.width=0.01
ribbon.thickness=0.0001
length = 1
ribbon.areaDensity=0
ribbon.D=1
ribbon.position={0.,0.,0.}
ribbon.frame=frameTheta(math.pi/2)
ribbon.poissonRatio=0.5
ribbon.numberSegments = 5;
ribbon:setRibbonStart(0.02,0.01)
for i=0,ribbon.numberSegments-1,1 do
  ribbon:setRibbonPiece(i,length/ribbon.numberSegments,0.0001,0.0001)
  ribbon:setNaturalCurvature(i,0,0)
end

ribbonOri = ribbon:copy()

options = Options.new()
options.maxIters = 100
options.checkDerivation = false
options.tol = 1.e-12

exp = Experiment.new()
exp.name="test101"
exp.gravity={0.,0.,9.81}
exp.ribbon=ribbon
exp.options=options
exp:attachEnd({0.95,0,0}, frameTheta(math.pi/2))

exp:findEquilibrium()

mesher = Mesher.new("testMesh.obj")
mesher.precision=0.001
mesher:setColors({255,255,0},{0,255,255})
mesher:add(ribbon)
mesher:setColors({255,0,0},{0,0,255})
mesher:add(exp.ribbon)

exp:attachEnd({0.95,0,0}, frameTheta(0.99*math.pi/2))
exp:findEquilibrium()
exp:attachEnd({0.95,0,0}, frameTheta(0.98*math.pi/2))
exp:findEquilibrium()
exp:attachEnd({0.95,0,0}, frameTheta(0.97*math.pi/2))
exp:findEquilibrium()
exp:attachEnd({0.95,0,0}, frameTheta(0.96*math.pi/2))
exp:findEquilibrium()
exp:attachEnd({0.95,0,0}, frameTheta(0.95*math.pi/2))
exp:findEquilibrium()
mesher:setColors({0,255,0},{255,0,0})
mesher:add(exp.ribbon)
