function drawFrame(pos, frame, scale, mesher)
  local ox=Arrow.new()
  ox.position=pos
  ox.direction={frame[1],frame[4],frame[7]}
  ox.color={255,0,0}
  ox.name="frameX"
  ox:setupDimensions(scale,0.15*scale,0.025*scale,0.05*scale)
  local oy=Arrow.new()
  oy.position=pos
  oy.direction={frame[2],frame[5],frame[8]}
  oy.color={0,255,0}
  oy.name="frameY"
  oy:setupDimensions(scale,0.15*scale,0.025*scale,0.05*scale)
  local oz=Arrow.new()
  oz.position=pos
  oz.direction={frame[3],frame[6],frame[9]}
  oz.color={0,0,255}
  oz.name="frameZ"
  oz:setupDimensions(scale,0.15*scale,0.025*scale,0.05*scale)
  mesher:add(ox)
  mesher:add(oy)
  mesher:add(oz)
end

function drawTwoFrame(pos1, frame1, pos2, frame2, scale, mesher)
  drawFrame(pos1, frame1, scale, mesher)
  drawFrame(pos2, frame2, scale, mesher)
end


mesher = Mesher.new("testPlan.obj")
mesher.precision=0.005

myArrow=Arrow.new()
myArrow.position={0,0,0}
myArrow.direction={0,0,1}
myArrow.color={255,0,0}
myArrow.name="arrow"
scale=1
myArrow:setupDimensions(scale,0.15*scale,0.025*scale,0.05*scale)
mesher:add(myArrow)

plan = Plane.new({0,0,0},{0,0,1})
plan.thickness=0.1
plan.color={0,255,0}
mesher:add(plan)
