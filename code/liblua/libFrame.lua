--[[
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
]]
require "libVector"
Frame = {}

Frame.mt = {}

function Frame.new(array_base)
  local frame={array_base[1],array_base[2],array_base[3],
               array_base[4],array_base[5],array_base[6],
               array_base[7],array_base[8],array_base[9]}
  setmetatable(frame, Frame.mt)
  return frame
end

function Frame.tostring(f)
  return f[1]..", "..f[2]..", "..f[3].."| "..
         f[4]..", "..f[5]..", "..f[6].."| "..
         f[7]..", "..f[8]..", "..f[9]
end

function Frame.col(f, i)
  return Vector.new({f[1+i],f[4+i],f[7+i]})
end

function Frame.row(f, i)
  return Vector.new({f[1+i*3],f[2+i*3],f[3+i*3]})
end

function Frame.multiply(f1, f2)
  local array_base={0,0,0,0,0,0,0,0,0}
  for i=0,2,1 do
    for j=0,2,1 do
      array_base[1+i+3*j]=f1:row(j):dot(f2:col(i))
    end
  end
  return Frame.new(array_base)
end

function Frame.add(f1, f2)
  local array_base={0,0,0,0,0,0,0,0,0}
  for i=1,9,1 do
    array_base[i]=f1[i]+f2[i]
  end
  return Frame.new(array_base)
end

function Frame.sub(f1, f2)
  local array_base={0,0,0,0,0,0,0,0,0}
  for i=1,9,1 do
    array_base[i]=f1[i]-f2[i]
  end
  return Frame.new(array_base)
end

function Frame.generatrix(frame, eta)
  local c0=frame:col(0)
  local c2=frame:col(2)
  c2:scale(eta)
  return c0+c2
end

Frame.mt.__add=Frame.add
Frame.mt.__sub=Frame.sub
Frame.mt.__tostring=Frame.tostring
Frame.mt.__index=Frame
Frame.mt.__mul=Frame.multiply

function makeFrame(frame)
  if(getmetatable(frame)~=Frame.mt) then return Frame.new(frame)
else
  return frame
end
end
