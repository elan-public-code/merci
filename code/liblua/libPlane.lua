--[[
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
]]
require "libVector"

Plane = {}

Plane.mt = {}

function Plane.new(pos, nml)
  local plane={pos=makeVector(pos),nml=makeVector(nml)}
  setmetatable(plane, Plane.mt)
  return plane
end

function Plane.tostring(p)
  return "["..tostring(p.pos).."|"..tostring(p.nml).."]"
end

function Plane.distance(plane,pt)
  return (pt-plane.pos):dot(plane.nml)
end


Plane.mt.__tostring=Plane.tostring
Plane.mt.__index=Plane

function makePlane(plane)
  if(getmetatable(plane)~=Plane.mt) then return Plane.new(plane[1], plane[2])
else
  return plane
end
end
