--[[
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
]]
Vector = {}

Vector.mt = {}

function Vector.new(array_base)
  local v={array_base[1],array_base[2],array_base[3]}
  setmetatable(v, Vector.mt)
  return v
end

function Vector.add(v1,v2)
  return Vector.new({v1[1]+v2[1],
               v1[2]+v2[2],
               v1[3]+v2[3]})
end

function Vector.sub(v1,v2)
  return Vector.new({v1[1]-v2[1],
               v1[2]-v2[2],
               v1[3]-v2[3]})
end

function Vector.dot(v1,v2)
  return v1[1]*v2[1]
         +v1[2]*v2[2]
         +v1[3]*v2[3]
end

function Vector.scale(v, scalar)
  v[1]=scalar*v[1]
  v[2]=scalar*v[2]
  v[3]=scalar*v[3]
end

function Vector.cpy(v)
  return Vector.new({v[1], v[2], v[3]})
end

function Vector.tostring(v)
  return v[1]..", "..v[2]..", "..v[3]
end

function Vector.norm(v)
  return math.sqrt(v[1]*v[1] + v[2]*v[2] + v[3]*v[3])
end

Vector.mt.__add=Vector.add
Vector.mt.__sub=Vector.sub
Vector.mt.__mul=Vector.dot
Vector.mt.__tostring=Vector.tostring
Vector.mt.__index=Vector

function makeVector(vector)
  if(getmetatable(vector)~=Vector.mt) then return Vector.new(vector)
else
  return vector
end
end
