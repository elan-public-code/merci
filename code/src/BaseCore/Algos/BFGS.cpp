/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "BFGS.hpp"
#include "../Log/Logger.hpp"
#include <iostream>

BFGS::BFGS(int systemSize,
           std::function<Eigen::VectorXr()> getSystemPoint,
           std::function<void(const Eigen::VectorXr &)> setSystemPoint,
           std::function<real(const Eigen::VectorXr &)> getEnergyValueAt,
           std::function<Eigen::VectorXr(const Eigen::VectorXr &)> getEnergyGradientAt,
           std::function<bool(const Eigen::VectorXr &)> haltCriterion):
  getPoint(getSystemPoint),
  setPoint(setSystemPoint),
  value(getEnergyValueAt),
  gradient(getEnergyGradientAt),
  stopCriterion(haltCriterion)
{
  hessianApprox.resize(systemSize, systemSize);
  hessianApprox.setIdentity();
}

BFGS::BFGS(int systemSize,
           std::function<Eigen::VectorXr()> getSystemPoint,
           std::function<void(const Eigen::VectorXr &)> setSystemPoint,
           std::function<real(const Eigen::VectorXr &)> getEnergyValueAt,
           std::function<Eigen::VectorXr(const Eigen::VectorXr &)> getEnergyGradientAt) :
  getPoint(getSystemPoint),
  setPoint(setSystemPoint),
  value(getEnergyValueAt),
  gradient(getEnergyGradientAt)
{
  hessianApprox.resize(systemSize, systemSize);
  hessianApprox.setIdentity();
}

BFGS::BFGSStatus BFGS::run(int MaxIters)
{
  //see detail at https://fr.wikipedia.org/wiki/BFGS

  Eigen::VectorXr x = getPoint();
  LoggerMsg startingMessage("Starting");
  startingMessage.addChild("initial value", std::to_string(value(x)));
  getLogger()->Write(LoggerLevel::INFORMATION, "BFGS", startingMessage);
  hessianApprox.setIdentity();
  for (int i = 0; i < MaxIters; i++) {
    //1. find p
    Eigen::VectorXr grad = gradient(x);
    if (stopCriterion(grad))
    {
      LoggerMsg outMessage("Convergence criterion reached");
      getLogger()->Write(LoggerLevel::INFORMATION, "BFGS", outMessage);
      setPoint(x);
      return BFGSStatus::SUCCESS;
    }
    Eigen::LLT<Eigen::MatrixXr> solver(hessianApprox);
    if (solver.info() != Eigen::ComputationInfo::Success) {
      LoggerMsg warningMessage("Solver failed");
      getLogger()->Write(LoggerLevel::WARNING, "BFGS", warningMessage);
      setPoint(x);
      return BFGSStatus::SOLVERFAIL;
    }
    Eigen::VectorXr p = solver.solve(-grad);
    if (solver.info() != Eigen::ComputationInfo::Success) {
      LoggerMsg warningMessage("Solver failed");
      getLogger()->Write(LoggerLevel::WARNING, "BFGS", warningMessage);
      setPoint(x);
      return BFGSStatus::SOLVERFAIL;
    }
    //2. Line search
    real alpha = 1.;
    real E0 = value(x);
    LoggerMsg iterMessage("Iteration");
    iterMessage.addChild("number", std::to_string(i));
    iterMessage.addChild("initial value", std::to_string(E0));
    getLogger()->Write(LoggerLevel::INFORMATION, "BFGS", iterMessage);
    real E;
    do {
      E = value(x + alpha * p);
      alpha /= 2.;
    } while (E > E0 && (alpha > tolerence));
    if (std::isnan(E)) {
      LoggerMsg errorMessage("NAN Error");
      getLogger()->Write(LoggerLevel::ERROR, "BFGS", errorMessage);
      setPoint(x);
      return BFGSStatus::NANISSUE;
    }
    if (!(alpha > tolerence)) {
      LoggerMsg errorMessage("Line search fail");
      std::stringstream ss;
      ss<<p.transpose();
      errorMessage.addChild("Search direction",ss.str());
      getLogger()->Write(LoggerLevel::ERROR, "BFGS", errorMessage);
      setPoint(x);
      return BFGSStatus::LINESEARCHFAIL;
    }
    alpha *= 2.;
    p *= alpha;
    LoggerMsg lsMessage("Line search");
    lsMessage.addChild("alpha", std::to_string(alpha));
    lsMessage.addChild("value", std::to_string(E));
    getLogger()->Write(LoggerLevel::INFORMATION, "BFGS", lsMessage);
    //3. set y
    Eigen::VectorXr y =  gradient(x + p) - grad;
    x += p;
    //4. update B
    Eigen::MatrixXr Bplus = (1. / (y.dot(p))) * y * y.transpose();
    Eigen::MatrixXr Bmoins = (1. / (p.dot(hessianApprox * p))) * hessianApprox * p * p.transpose() * hessianApprox;
    hessianApprox += Bplus - Bmoins;
  }
  LoggerMsg warningMessage("Solver did not converge");
  getLogger()->Write(LoggerLevel::WARNING, "BFGS", warningMessage);
  setPoint(x);
  return BFGSStatus::NOTCONVERGED;
}

BFGS::BFGSStatus BFGS::runinv(int MaxIters)
{
  Eigen::MatrixXr hessianinvApprox;
  hessianinvApprox.resizeLike(hessianApprox);
  hessianinvApprox.setIdentity();
  Eigen::VectorXr x = getPoint();
  LoggerMsg startingMessage("Starting");
  startingMessage.addChild("initial value", std::to_string(value(x)));
  getLogger()->Write(LoggerLevel::INFORMATION, "BFGS inv", startingMessage);
  for (int i = 0; i < MaxIters; i++) {
    //1. find p
    Eigen::VectorXr grad = gradient(x);
    if(stopCriterion(grad))
    {
      LoggerMsg outMessage("Convergence criterion reached");
      getLogger()->Write(LoggerLevel::INFORMATION, "BFGS inv", outMessage);
      setPoint(x);
      return BFGSStatus::SUCCESS;
    }
    Eigen::VectorXr p = -hessianinvApprox * grad;
    //2. Line search
    real alpha = 1.;
    real E0 = value(x);
    LoggerMsg iterMessage("Iteration");
    iterMessage.addChild("number", std::to_string(i));
    iterMessage.addChild("initial value", std::to_string(E0));
    getLogger()->Write(LoggerLevel::INFORMATION, "BFGS inv", iterMessage);
    real E;
    do {
      E = value(x + alpha * p);
      //os<<"   alpha: "<<alpha<<", value: "<<E<<std::endl;
      alpha /= 2.;
    } while (E > E0 && (alpha > tolerence));
    if (!(alpha > tolerence)) {
      LoggerMsg errorMessage("Line search fail");
      std::stringstream ss;
      ss<<p.transpose();
      errorMessage.addChild("Search direction",ss.str());
      getLogger()->Write(LoggerLevel::ERROR, "BFGS inv", errorMessage);
      setPoint(x);
      return BFGSStatus::LINESEARCHFAIL;
    }
    if (std::isnan(E)) {
      LoggerMsg errorMessage("NAN Error");
      getLogger()->Write(LoggerLevel::ERROR, "BFGS inv", errorMessage);
      setPoint(x);
      return BFGSStatus::NANISSUE;
    }
    alpha *= 2.;
    p *= alpha;
    LoggerMsg lsMessage("Line search");
    lsMessage.addChild("alpha", std::to_string(alpha));
    lsMessage.addChild("value", std::to_string(E));
    getLogger()->Write(LoggerLevel::INFORMATION, "BFGS inv", lsMessage);
    //3. set y
    Eigen::VectorXr y =  gradient(x + p) - grad;
    x += p;
    //4. update B
    real sty = y.dot(p);
    Eigen::MatrixXr Binvplus = ((sty + y.transpose() * hessianinvApprox * y) / (sty * sty)) * (p * p.transpose());
    Eigen::MatrixXr Binvminus = (1. / sty) * (hessianinvApprox * y * p.transpose() + p * y.transpose() * hessianinvApprox);
    hessianinvApprox += Binvplus - Binvminus;
  }
  LoggerMsg warningMessage("Solver did not converge");
  getLogger()->Write(LoggerLevel::WARNING, "BFGS inv", warningMessage);
  setPoint(x);
  return BFGSStatus::NOTCONVERGED;
}

BFGS::BFGSStatus BFGS::fullrun(int MaxPasses, int MaxIters)
{
  BFGSStatus status=BFGSStatus::SUCCESS;
  for(int i=0;i<MaxPasses;i++)
  {
    status = run(MaxIters);
    if(status==BFGSStatus::SUCCESS)return status;
    if(status==BFGSStatus::NANISSUE)return status;
  }
  return status;
}

BFGS::BFGSStatus BFGS::fullruninv(int MaxPasses, int MaxIters)
{
  BFGSStatus status=BFGSStatus::SUCCESS;
  for(int i=0;i<MaxPasses;i++)
  {
    status = runinv(MaxIters);
    if(status==BFGSStatus::SUCCESS)return status;
    if(status==BFGSStatus::NANISSUE)return status;
  }
  return status;
}

