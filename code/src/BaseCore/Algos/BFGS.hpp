/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef BFGS_H
#define BFGS_H

#include <Eigen/Dense>
#include <functional>
#include <iostream>
#include "real.hpp"

class BFGS
{
public:
  BFGS(int systemSize,
       std::function<Eigen::VectorXr()> getSystemPoint,
       std::function<void(const Eigen::VectorXr &)> setSystemPoint,
       std::function<real(const Eigen::VectorXr &)> getEnergyValueAt,
       std::function<Eigen::VectorXr(const Eigen::VectorXr &)> getEnergyGradientAt,
       std::function<bool(const Eigen::VectorXr &)> haltCriterion);
  BFGS(int systemSize,
       std::function<Eigen::VectorXr()> getSystemPoint,
       std::function<void(const Eigen::VectorXr &)> setSystemPoint,
       std::function<real(const Eigen::VectorXr &)> getEnergyValueAt,
       std::function<Eigen::VectorXr(const Eigen::VectorXr &)> getEnergyGradientAt);

  enum class BFGSStatus
  {
    SUCCESS, LINESEARCHFAIL, SOLVERFAIL, NANISSUE, NOTCONVERGED
  };

  BFGSStatus fullrun(int MaxPasses = 10000, int MaxIters = 1000);
  BFGSStatus fullruninv(int MaxPasses = 10000, int MaxIters = 1000);
  BFGSStatus run(int MaxIters = 100);
  BFGSStatus runinv(int MaxIters = 100);

private:
  Eigen::MatrixXr hessianApprox;
  std::function<Eigen::VectorXr()> getPoint;
  std::function<void(const Eigen::VectorXr &)> setPoint;
  std::function<real(const Eigen::VectorXr &)> value;
  std::function<Eigen::VectorXr(const Eigen::VectorXr &)> gradient;
  std::function<bool(const Eigen::VectorXr &)> stopCriterion = [](const Eigen::VectorXr &gradient)->bool {return gradient.lpNorm<Eigen::Infinity>() < 1.e-10; };
  real tolerence = 1.e-8;
};

#endif // BFGS_H
