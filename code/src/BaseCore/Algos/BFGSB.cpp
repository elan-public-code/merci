/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "BFGSB.hpp"
#include "../Log/Logger.hpp"
#include <iostream>

BFGSB::BFGSB(int systemSize,
             Eigen::VectorXr lowerBound,
             Eigen::VectorXr upperBound,
             std::function<Eigen::VectorXr()> getSystemPoint,
             std::function<void(const Eigen::VectorXr &)> setSystemPoint,
             std::function<real(const Eigen::VectorXr &)> getEnergyValueAt,
             std::function<Eigen::VectorXr(const Eigen::VectorXr &)> getEnergyGradientAt,
             std::function<bool(const Eigen::VectorXr &)> haltCriterion):
  lowerBound(lowerBound),
  upperBound(upperBound),
  getPoint(getSystemPoint),
  setPoint(setSystemPoint),
  value(getEnergyValueAt),
  gradient(getEnergyGradientAt),
  stopCriterion(haltCriterion)
{
  hessianApprox.resize(systemSize, systemSize);
  hessianApprox.setIdentity();
}

BFGSB::BFGSB(int systemSize,
             Eigen::VectorXr lowerBound,
             Eigen::VectorXr upperBound,
             std::function<Eigen::VectorXr()> getSystemPoint,
             std::function<void(const Eigen::VectorXr &)> setSystemPoint,
             std::function<real(const Eigen::VectorXr &)> getEnergyValueAt,
             std::function<Eigen::VectorXr(const Eigen::VectorXr &)> getEnergyGradientAt) :
  lowerBound(lowerBound),
  upperBound(upperBound),
  getPoint(getSystemPoint),
  setPoint(setSystemPoint),
  value(getEnergyValueAt),
  gradient(getEnergyGradientAt)
{
  hessianApprox.resize(systemSize, systemSize);
  hessianApprox.setIdentity();
}

Eigen::VectorXr BFGSB::reproject(Eigen::VectorXr pt)
{
  int t = pt.size();
  for (int i = 0; i < t; i++) {
    if (pt[i] < lowerBound[i])pt[i] = lowerBound[i];
    if (pt[i] > upperBound[i])pt[i] = upperBound[i];
  }
  return pt;
}

BFGSB::BFGSBStatus BFGSB::run(int MaxIters)
{
  //see detail at https://fr.wikipedia.org/wiki/BFGSB

  Eigen::VectorXr x = getPoint();
  LoggerMsg startingMessage("Starting");
  startingMessage.addChild("initial value", std::to_string(value(x)));
  getLogger()->Write(LoggerLevel::INFORMATION, "BFGSB", startingMessage);
  hessianApprox.setIdentity();
  for (int i = 0; i < MaxIters; i++) {
    //1. find p
    Eigen::VectorXr grad = gradient(x);
    if (stopCriterion(grad)) {
      LoggerMsg outMessage("Convergence criterion reached");
      getLogger()->Write(LoggerLevel::INFORMATION, "BFGSB", outMessage);
      setPoint(x);
      return BFGSBStatus::SUCCESS;
    }
    Eigen::LLT<Eigen::MatrixXr> solver(hessianApprox);
    if (solver.info() != Eigen::ComputationInfo::Success) {
      LoggerMsg warningMessage("Solver failed");
      getLogger()->Write(LoggerLevel::WARNING, "BFGSB", warningMessage);
      setPoint(x);
      return BFGSBStatus::SOLVERFAIL;
    }
    Eigen::VectorXr p = solver.solve(-grad);
    if (solver.info() != Eigen::ComputationInfo::Success) {
      LoggerMsg warningMessage("Solver failed");
      getLogger()->Write(LoggerLevel::WARNING, "BFGSB", warningMessage);
      setPoint(x);
      return BFGSBStatus::SOLVERFAIL;
    }
    //2. Line search
    real alpha = 1.;
    real E0 = value(x);
    LoggerMsg iterMessage("Iteration");
    iterMessage.addChild("number", std::to_string(i));
    iterMessage.addChild("initial value", std::to_string(E0));
    getLogger()->Write(LoggerLevel::INFORMATION, "BFGSB", iterMessage);
    real E;
    Eigen::VectorXr newX;
    do {
      newX = reproject(x + alpha * p);
      E = value(newX);
      alpha /= 2.;
    } while (E > E0 && (alpha > tolerence));
    if (std::isnan(E)) {
      LoggerMsg errorMessage("NAN Error");
      getLogger()->Write(LoggerLevel::ERROR, "BFGSB", errorMessage);
      setPoint(x);
      return BFGSBStatus::NANISSUE;
    }
    if (!(alpha > tolerence)) {
      LoggerMsg errorMessage("Line search fail");
      std::stringstream ss;
      ss << p.transpose();
      errorMessage.addChild("Search direction", ss.str());
      getLogger()->Write(LoggerLevel::ERROR, "BFGSB", errorMessage);
      setPoint(x);
      return BFGSBStatus::LINESEARCHFAIL;
    }
    p = x - newX;
    LoggerMsg lsMessage("Line search");
    lsMessage.addChild("alpha", std::to_string(alpha));
    lsMessage.addChild("value", std::to_string(E));
    getLogger()->Write(LoggerLevel::INFORMATION, "BFGSB", lsMessage);
    //3. set y
    Eigen::VectorXr y =  gradient(newX) - grad;
    x = newX;
    //4. update B
    Eigen::MatrixXr Bplus = (1. / (y.dot(p))) * y * y.transpose();
    Eigen::MatrixXr Bmoins = (1. / (p.dot(hessianApprox * p))) * hessianApprox * p * p.transpose() * hessianApprox;
    hessianApprox += Bplus - Bmoins;
  }
  LoggerMsg warningMessage("Solver did not converge");
  getLogger()->Write(LoggerLevel::WARNING, "BFGSB", warningMessage);
  setPoint(x);
  return BFGSBStatus::NOTCONVERGED;
}

BFGSB::BFGSBStatus BFGSB::runinv(int MaxIters)
{
  Eigen::MatrixXr hessianinvApprox;
  hessianinvApprox.resizeLike(hessianApprox);
  hessianinvApprox.setIdentity();
  Eigen::VectorXr x = getPoint();
  LoggerMsg startingMessage("Starting");
  startingMessage.addChild("initial value", std::to_string(value(x)));
  getLogger()->Write(LoggerLevel::INFORMATION, "BFGSB inv", startingMessage);
  for (int i = 0; i < MaxIters; i++) {
    //1. find p
    Eigen::VectorXr grad = gradient(x);
    if (stopCriterion(grad)) {
      LoggerMsg outMessage("Convergence criterion reached");
      getLogger()->Write(LoggerLevel::INFORMATION, "BFGSB inv", outMessage);
      setPoint(x);
      return BFGSBStatus::SUCCESS;
    }
    Eigen::VectorXr p = -hessianinvApprox * grad;
    //2. Line search
    real alpha = 1.;
    real E0 = value(x);
    LoggerMsg iterMessage("Iteration");
    iterMessage.addChild("number", std::to_string(i));
    iterMessage.addChild("initial value", std::to_string(E0));
    getLogger()->Write(LoggerLevel::INFORMATION, "BFGSB inv", iterMessage);
    real E;
    Eigen::VectorXr newX;
    do {
      newX = reproject(x + alpha * p);
      E = value(newX);
      //os<<"   alpha: "<<alpha<<", value: "<<E<<std::endl;
      alpha /= 2.;
    } while (E > E0 && (alpha > tolerence));
    if (!(alpha > tolerence)) {
      LoggerMsg errorMessage("Line search fail");
      std::stringstream ss;
      ss << p.transpose();
      errorMessage.addChild("Search direction", ss.str());
      getLogger()->Write(LoggerLevel::ERROR, "BFGSB inv", errorMessage);
      setPoint(x);
      return BFGSBStatus::LINESEARCHFAIL;
    }
    if (std::isnan(E)) {
      LoggerMsg errorMessage("NAN Error");
      getLogger()->Write(LoggerLevel::ERROR, "BFGSB inv", errorMessage);
      setPoint(x);
      return BFGSBStatus::NANISSUE;
    }
    alpha *= 2.;
    p = x - newX;
    LoggerMsg lsMessage("Line search");
    lsMessage.addChild("alpha", std::to_string(alpha));
    lsMessage.addChild("value", std::to_string(E));
    getLogger()->Write(LoggerLevel::INFORMATION, "BFGSB inv", lsMessage);
    //3. set y
    Eigen::VectorXr y =  gradient(newX) - grad;
    x = newX;
    //4. update B
    real sty = y.dot(p);
    Eigen::MatrixXr Binvplus = ((sty + y.transpose() * hessianinvApprox * y) / (sty * sty)) * (p * p.transpose());
    Eigen::MatrixXr Binvminus = (1. / sty) * (hessianinvApprox * y * p.transpose() + p * y.transpose() * hessianinvApprox);
    hessianinvApprox += Binvplus - Binvminus;
  }
  LoggerMsg warningMessage("Solver did not converge");
  getLogger()->Write(LoggerLevel::WARNING, "BFGSB inv", warningMessage);
  setPoint(x);
  return BFGSBStatus::NOTCONVERGED;
}

BFGSB::BFGSBStatus BFGSB::fullrun(int MaxPasses, int MaxIters)
{
  BFGSBStatus status=BFGSBStatus::SUCCESS;
  for (int i = 0; i < MaxPasses; i++) {
    status = run(MaxIters);
    if (status == BFGSBStatus::SUCCESS)return status;
    if (status == BFGSBStatus::NANISSUE)return status;
  }
  return status;
}

BFGSB::BFGSBStatus BFGSB::fullruninv(int MaxPasses, int MaxIters)
{
  BFGSBStatus status=BFGSBStatus::SUCCESS;
  for (int i = 0; i < MaxPasses; i++) {
    status = runinv(MaxIters);
    if (status == BFGSBStatus::SUCCESS)return status;
    if (status == BFGSBStatus::NANISSUE)return status;
  }
  return status;
}

