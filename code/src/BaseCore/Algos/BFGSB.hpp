/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef BFGSB_H
#define BFGSB_H

#include <Eigen/Dense>
#include <functional>
#include <iostream>
#include "real.hpp"

class BFGSB
{
public:
  BFGSB(int systemSize,
       Eigen::VectorXr lowerBound,
       Eigen::VectorXr upperBound,
       std::function<Eigen::VectorXr()> getSystemPoint,
       std::function<void(const Eigen::VectorXr &)> setSystemPoint,
       std::function<real(const Eigen::VectorXr &)> getEnergyValueAt,
       std::function<Eigen::VectorXr(const Eigen::VectorXr &)> getEnergyGradientAt,
       std::function<bool(const Eigen::VectorXr &)> haltCriterion);
  BFGSB(int systemSize,
       Eigen::VectorXr lowerBound,
       Eigen::VectorXr upperBound,
       std::function<Eigen::VectorXr()> getSystemPoint,
       std::function<void(const Eigen::VectorXr &)> setSystemPoint,
       std::function<real(const Eigen::VectorXr &)> getEnergyValueAt,
       std::function<Eigen::VectorXr(const Eigen::VectorXr &)> getEnergyGradientAt);

  enum class BFGSBStatus
  {
    SUCCESS, LINESEARCHFAIL, SOLVERFAIL, NANISSUE, NOTCONVERGED
  };

  BFGSBStatus fullrun(int MaxPasses = 10000, int MaxIters = 10);
  BFGSBStatus fullruninv(int MaxPasses = 10000, int MaxIters = 10);
  BFGSBStatus run(int MaxIters = 100);
  BFGSBStatus runinv(int MaxIters = 100);

private:
  Eigen::VectorXr lowerBound;
  Eigen::VectorXr upperBound;
  Eigen::MatrixXr hessianApprox;
  std::function<Eigen::VectorXr()> getPoint;
  std::function<void(const Eigen::VectorXr &)> setPoint;
  std::function<real(const Eigen::VectorXr &)> value;
  std::function<Eigen::VectorXr(const Eigen::VectorXr &)> gradient;
  std::function<bool(const Eigen::VectorXr &)> stopCriterion = [](const Eigen::VectorXr &gradient)->bool {return gradient.lpNorm<Eigen::Infinity>() < 1.e-10; };
  real tolerence = 1.e-8;
  Eigen::VectorXr reproject(Eigen::VectorXr pt);
};

#endif // BFGS_H
