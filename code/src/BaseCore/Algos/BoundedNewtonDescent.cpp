/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "BoundedNewtonDescent.hpp"
#include "../Log/Logger.hpp"
#include "../preReal.hpp"
#include <iostream>
#include <sstream>
#include <Eigen/LU>


BoundedNewtonDescent::BoundedNewtonDescent(
  std::function<Eigen::VectorXr()> getSystemPoint,
  std::function<Eigen::VectorXr()> getUpperBound,
  std::function<Eigen::VectorXr()> getLowerBound,
  std::function<void(const Eigen::VectorXr &)> setSystemPoint,
  std::function<real(const Eigen::VectorXr &)> getEnergyValueAt,
  std::function<Eigen::VectorXr(const Eigen::VectorXr &)> getEnergyGradientAt,
  std::function<Eigen::MatrixXr(const Eigen::VectorXr &)> getEnergyHessianAt):
  getPoint(getSystemPoint),
  upperBound(getUpperBound),
  lowerBound(getLowerBound),
  setPoint(setSystemPoint),
  value(getEnergyValueAt),
  gradient(getEnergyGradientAt),
  hessian(getEnergyHessianAt)
{
}

BoundedNewtonDescent::BoundedNewtonDescent(
  std::function<Eigen::VectorXr()> getSystemPoint,
  std::function<Eigen::VectorXr()> getUpperBound,
  std::function<Eigen::VectorXr()> getLowerBound,
  std::function<void(const Eigen::VectorXr &)> setSystemPoint,
  std::function<real(const Eigen::VectorXr &)> getEnergyValueAt,
  std::function<Eigen::VectorXr(const Eigen::VectorXr &)> getEnergyGradientAt,
  std::function<Eigen::MatrixXr(const Eigen::VectorXr &)> getEnergyHessianAt,
  std::function<bool(const Eigen::VectorXr &)> haltCriterion):
  getPoint(getSystemPoint),
  upperBound(getUpperBound),
  lowerBound(getLowerBound),
  setPoint(setSystemPoint),
  value(getEnergyValueAt),
  gradient(getEnergyGradientAt),
  hessian(getEnergyHessianAt),
  stopCriterion(haltCriterion)
{
}

bool BoundedNewtonDescent::run(int MaxIters)
{
  const Eigen::VectorXr ub = upperBound();
  const Eigen::VectorXr lb = lowerBound();
  const int sizePt = ub.size();
  auto reproject = [&ub, &lb, &sizePt](Eigen::VectorXr pt)->Eigen::VectorXr {
    //return pt;
    for (int i = 0; i < sizePt; i++)
    {
      if (pt[i] < lb[i])pt[i] = lb[i];
      if (pt[i] > ub[i])pt[i] = ub[i];
    }
    return pt;
  };

  auto alphamax = [&ub, &lb, &sizePt](Eigen::VectorXr pt, Eigen::VectorXr dir)->real {
    real res = std::numeric_limits<real>::infinity();
    for (int i = 0; i < sizePt; i++)
    {
      if (dir[i] == 0.)continue;
      if (dir[i] < 0.)res = std::min(res, (lb[i] - pt[i]) / dir[i]);
      else res = std::min(res, (ub[i] - pt[i]) / dir[i]);
    }
    return res;
  };

  auto toString = [](real v)->std::string {std::stringstream ss; ss << v; return ss.str();};
  Eigen::VectorXr x = getPoint();
  x=reproject(x);
  LoggerMsg startingMessage("Starting");
  startingMessage.addChild("initial value", toString(value(x)));
  getLogger()->Write(LoggerLevel::INFORMATION, "Newton descent", startingMessage);
  //Use scientific notation
  for (int i = 0; i < MaxIters; i++) {
    //1. find p
    Eigen::VectorXr grad = gradient(x);
    if (stopCriterion(grad)) {
      LoggerMsg outMessage("Convergence criterion reached");
      getLogger()->Write(LoggerLevel::INFORMATION, "Newton descent", outMessage);
      setPoint(x);
      return true;
    }
    Eigen::VectorXr p = -grad;
    Eigen::MatrixXr h = hessian(x);
    real hdet = h.determinant();
    if (true || hdet > 1e-5) {
      p = h.fullPivLu().solve(p);
    } else {
      LoggerMsg lowdetMessage("Low determinant of hessian");
      lowdetMessage.addChild("determinant", toString(hdet));
      lowdetMessage.addChild("trace", toString(h.trace()));
      getLogger()->Write(LoggerLevel::WARNING, "Newton descent", lowdetMessage);
    }
    //2. Line search
    preReal E = value(x);
    E.save();
    real alphaMax = alphamax(x, p);
    preReal alpha = std::min(static_cast<real>(1.), alphaMax);
    LoggerMsg iterMessage("Iteration");
    iterMessage.addChild("number", toString(i));
    iterMessage.addChild("initial value", toString(E));
    Eigen::VectorXr newX = reproject(x + static_cast<real>(alpha) * p);
    E = value(newX);
    if (E.hasDecreased()) 
    { //try to be more agressive
      do {
        E.save();
        alpha.save();
        alpha = std::min(alpha * 2., alphaMax);
        newX = reproject(x + static_cast<real>(alpha) * p);
        E = value(newX);
      } while (E.hasDecreased());
      alpha.restore();
      E.restore();
      newX = reproject(x + static_cast<real>(alpha) * p);
    } 
    else
    {
      do {
        alpha.save();
        alpha = alpha / 2.;
        newX = reproject(x + static_cast<real>(alpha) * p);
        E = value(newX);
      } while (E.hasIncreased() && (alpha > tolerence));
    }
    if (!(alpha > tolerence)) 
    {
      LoggerMsg errorMessage("Line search fail");
      std::stringstream ss;
      ss << p;
      errorMessage.addChild("Search direction", ss.str());
      getLogger()->Write(LoggerLevel::INFORMATION, "Newton descent", iterMessage);
      getLogger()->Write(LoggerLevel::ERROR, "Newton descent", errorMessage);
      setPoint(x);
      return false;
    }
    iterMessage.addChild("line search alpha", toString(alpha));
    iterMessage.addChild("line search value", toString(E));
    getLogger()->Write(LoggerLevel::INFORMATION, "Newton descent", iterMessage);
    x = newX;
  }
  LoggerMsg warningMessage("Solver did not converge");
  getLogger()->Write(LoggerLevel::WARNING, "Newton descent", warningMessage);
  setPoint(x);
  return true;
}
