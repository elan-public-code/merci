/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef BOUNDEDNEWTONDESCENT_H
#define BOUNDEDNEWTONDESCENT_H

#include <Eigen/Dense>
#include <functional>
#include "real.hpp"

class BoundedNewtonDescent
{
  public:
  BoundedNewtonDescent(
       std::function<Eigen::VectorXr()> getSystemPoint,
       std::function<Eigen::VectorXr()> getUpperBound,
       std::function<Eigen::VectorXr()> getLowerBound,
       std::function<void(const Eigen::VectorXr &)> setSystemPoint,
       std::function<real(const Eigen::VectorXr &)> getEnergyValueAt,
       std::function<Eigen::VectorXr(const Eigen::VectorXr &)> getEnergyGradientAt,
       std::function<Eigen::MatrixXr(const Eigen::VectorXr &)> getEnergyHessianAt,
       std::function<bool(const Eigen::VectorXr &)> haltCriterion);
  BoundedNewtonDescent(
       std::function<Eigen::VectorXr()> getSystemPoint,
       std::function<Eigen::VectorXr()> getUpperBound,
       std::function<Eigen::VectorXr()> getLowerBound,
       std::function<void(const Eigen::VectorXr &)> setSystemPoint,
       std::function<real(const Eigen::VectorXr &)> getEnergyValueAt,
       std::function<Eigen::VectorXr(const Eigen::VectorXr &)> getEnergyGradientAt,
       std::function<Eigen::MatrixXr(const Eigen::VectorXr &)> getEnergyHessianAt);
  
  bool run(int MaxIters = 100);
  
private:
  std::function<Eigen::VectorXr()> getPoint;
  std::function<Eigen::VectorXr()> upperBound;
  std::function<Eigen::VectorXr()> lowerBound;
  std::function<void(const Eigen::VectorXr &)> setPoint;
  std::function<real(const Eigen::VectorXr &)> value;
  std::function<Eigen::VectorXr(const Eigen::VectorXr &)> gradient;
  std::function<Eigen::MatrixXr(const Eigen::VectorXr &)> hessian;
  std::function<bool(const Eigen::VectorXr &)> stopCriterion = [](const Eigen::VectorXr &gradient)->bool {return gradient.lpNorm<Eigen::Infinity>() < 1.e-7; };
  real tolerence = 1.e-10;
};

#endif // BOUNDEDNEWTONDESCENT_H
