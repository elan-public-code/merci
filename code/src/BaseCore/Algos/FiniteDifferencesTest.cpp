/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "FiniteDifferencesTest.hpp"

FiniteDifferencesTest::FiniteDifferencesTest(std::function<Eigen::VectorXr()> getSystemPoint,
    std::function<real(const Eigen::VectorXr &)> getEnergyValueAt,
    std::function<Eigen::VectorXr(const Eigen::VectorXr &)> getEnergyGradientAt):
  getPoint(getSystemPoint),
  value(getEnergyValueAt),
  gradient(getEnergyGradientAt)
{

}

void FiniteDifferencesTest::setNaming(std::function<std::string(int)> namer)
{
  this->getName = namer;
}

void FiniteDifferencesTest::setParams(real dT, real eT)
{
  deltaTest = dT;
  errorTolerance = eT;
}

bool FiniteDifferencesTest::run(std::string name)
{
  bool sucess = true;
  Eigen::VectorXr p = getPoint();
  Eigen::VectorXr pc = p;
  Eigen::VectorXr ev;
  int n = p.size();
  ev.resize(n);
  real Em, Ep;
  real d = 2.*deltaTest;
  for (int i = 0; i < n; i++) {
    pc(i) = p(i) - deltaTest;
    Em = value(pc);
    pc(i) = p(i) + deltaTest;
    Ep = value(pc);
    ev(i) = (Ep - Em) / d;
    pc(i) = p(i);
  }
  pc = gradient(p);
  assert(pc.size()==n);
  real error;
  std::cout << std::setw(67) << std::setfill('_') << '_' << std::endl;
  std::cout << '|' << std::setw(65) << std::setfill(' ') << std::left << ("  FINITE DIFFERENCES TEST " + name) << '|' << std::endl;
  std::cout << '|' << std::setw(65) << std::setfill('-') << '-' << '|' << std::endl << std::setfill(' ');
  std::cout << '|' << std::setw(20) << " Name " << '|' << std::setw(14) << "Rel. Error " << '|' << std::setw(14) << " Estim" << '|' << std::setw(14) << " Grad " << '|' << std::endl;
  std::cout << '|' << std::setw(65) << std::setfill('-') << '-' << '|' << std::endl << std::setfill(' ');
  for (int i = 0; i < n; i++) {
    std::cout << '|' << colNameColor << colNameBackground << std::setw(20) << std::left << std::right << getName(i);
    std::cout << Color::Modifier(Color::FG_DEFAULT) << Color::Modifier(Color::BG_DEFAULT) << "|";
    error = std::abs(ev(i) - pc(i)) / (0.5*(std::abs(ev(i))+std::abs(pc(i))) + 1.e-20);
    if (error < errorTolerance) {
      std::cout << colGoodValueColor << colGoodValueBackground << std::setw(14) << std::scientific << error;
    } else {
      sucess = false;
      std::cout << colBadValueColor << colBadValueBackground << std::setw(14) << std::scientific << error;
    }
    std::cout << Color::Modifier(Color::FG_DEFAULT) << Color::Modifier(Color::BG_DEFAULT) << "|" << std::setw(14) << ev(i); 
    std::cout << '|' << std::setw(14) << pc(i) << '|';
    std::cout << std::endl;
  }
  std::cout << '|' << std::setw(20) << std::setfill('_') << '_' << '|' << std::setw(14) << std::setfill('_') << '_' << '|' << std::setw(14) << std::setfill('_') << '_' << '|' << std::setw(14) << std::setfill('_') << '_' << '|' << std::endl << std::setfill(' ');
  return sucess;
}

FiniteDifferencesTestSecondOrder::FiniteDifferencesTestSecondOrder(std::function<Eigen::VectorXr()> getSystemPoint,
    std::function<real(const Eigen::VectorXr &)> getEnergyValueAt,
    std::function<Eigen::MatrixXr(const Eigen::VectorXr &)> getEnergyHessianAt):
  getPoint(getSystemPoint),
  value(getEnergyValueAt),
  hessian(getEnergyHessianAt)
{
}

void FiniteDifferencesTestSecondOrder::setNaming(std::function<std::string(int)> namer)
{
  this->getName = namer;
}

void FiniteDifferencesTestSecondOrder::setParams(real dT, real eT)
{
  deltaTest = dT;
  errorTolerance = eT;
}

bool FiniteDifferencesTestSecondOrder::run(std::string name)
{
  bool sucess = true;
  Eigen::VectorXr p = getPoint();
  Eigen::VectorXr pc = p;
  Eigen::MatrixXr hv,hess;
  int n = p.size();
  hv.resize(n,n);
  real Eref = value(pc);
  hess=hessian(pc);
  real Equad, Evi, Evj;
  const real h = deltaTest;
  const real h2 = h*h;
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      if(i==j)
      {
        pc(i) = p(i) + 2.*deltaTest;
        Equad = value(pc);
        pc(i) = p(i) + deltaTest;
        Evi = Evj = value(pc);
        Evi = value(pc);
        pc(i) = p(i);
        hv(i,j)=(Equad-Evi-Evj+Eref)/h2;
      }
      else
      {
        pc(i) = p(i) + deltaTest;
        pc(j) = p(j) + deltaTest;
        Equad = value(pc);
        pc(i) = p(i);
        Evj = value(pc);
        pc(j) = p(j);
        pc(i) = p(i) + deltaTest;
        Evi = value(pc);
        pc(i) = p(i);
        hv(i,j)=(Equad-Evi-Evj+Eref)/h2;
      }
    }
  }
  assert(pc.size()==n);
  real error;
  std::cout << std::setw(67) << std::setfill('_') << '_' << std::endl;
  std::cout << '|' << std::setw(65) << std::setfill(' ') << std::left << ("  FINITE DIFFERENCES TEST " + name) << '|' << std::endl;
  std::cout << '|' << std::setw(65) << std::setfill('-') << '-' << '|' << std::endl << std::setfill(' ');
  std::cout << '|' << std::setw(20) << " Name " << '|' << std::setw(14) << "Rel. Error " << '|' << std::setw(14) << " Estim" << '|' << std::setw(14) << " Hess " << '|' << std::endl;
  std::cout << '|' << std::setw(65) << std::setfill('-') << '-' << '|' << std::endl << std::setfill(' ');
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      std::stringstream ss; ss<<getName(i)<<','<<getName(j);
      std::cout << '|' << colNameColor << colNameBackground << std::setw(20) << std::left << std::right << ss.str();
      std::cout << Color::Modifier(Color::FG_DEFAULT) << Color::Modifier(Color::BG_DEFAULT) << "|";
      error = std::abs(hv(i,j) - hess(i,j)) / (0.5*(std::abs(hv(i,j))+std::abs(hess(i,j))) + 1.e-20);
      if (error < errorTolerance) {
        std::cout << colGoodValueColor << colGoodValueBackground << std::setw(14) << std::scientific << error;
      } else {
        sucess = false;
        std::cout << colBadValueColor << colBadValueBackground << std::setw(14) << std::scientific << error;
      }
      std::cout << Color::Modifier(Color::FG_DEFAULT) << Color::Modifier(Color::BG_DEFAULT) << "|" << std::setw(14) << hv(i,j); 
      std::cout << '|' << std::setw(14) << hess(i,j) << '|';
      std::cout << std::endl;
    }
  }
  std::cout << '|' << std::setw(20) << std::setfill('_') << '_' << '|' << std::setw(14) << std::setfill('_') << '_' << '|' << std::setw(14) << std::setfill('_') << '_' << '|' << std::setw(14) << std::setfill('_') << '_' << '|' << std::endl << std::setfill(' ');
  return sucess;
}
