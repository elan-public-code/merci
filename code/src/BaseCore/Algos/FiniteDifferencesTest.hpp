/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef FINITEDIFFERENCESTEST_H
#define FINITEDIFFERENCESTEST_H

#include <functional>
#include <string>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <Eigen/Dense>
#include "ColorMod.hpp"
#include "real.hpp"


class FiniteDifferencesTest
{
public:
  FiniteDifferencesTest(
    std::function<Eigen::VectorXr()> getSystemPoint,
    std::function<real(const Eigen::VectorXr &)> getEnergyValueAt,
    std::function<Eigen::VectorXr(const Eigen::VectorXr &)> getEnergyGradientAt);

  void setNaming(std::function<std::string(int i)> namer);
  void setParams(real deltaTest, real errorTolerance);
  bool run(std::string name = "");
private:
  std::function<Eigen::VectorXr()> getPoint;
  std::function<real(const Eigen::VectorXr &)> value;
  std::function<Eigen::VectorXr(const Eigen::VectorXr &)> gradient;
  std::function<std::string(int i)> getName = [](int i)->std::string {std::stringstream ss; ss << i; return ss.str();};

  real errorTolerance = 1.e-4;
  real deltaTest = 1.e-6;
  bool showOnlyErrorLines = false;
  Color::Modifier colNameColor {Color::FG_LIGHT_BLUE};
  Color::Modifier colNameBackground {Color::BG_DEFAULT};
  Color::Modifier colGoodValueColor {Color::FG_LIGHT_GREEN};
  Color::Modifier colBadValueColor {Color::FG_BLACK};
  Color::Modifier colGoodValueBackground {Color::BG_DEFAULT};
  Color::Modifier colBadValueBackground {Color::BG_LIGHT_RED};
};

class FiniteDifferencesTestSecondOrder
{
public:
  FiniteDifferencesTestSecondOrder(
    std::function<Eigen::VectorXr()> getSystemPoint,
    std::function<real(const Eigen::VectorXr &)> getEnergyValueAt,
    std::function<Eigen::MatrixXr(const Eigen::VectorXr &)> getEnergyHessianAt);

  void setNaming(std::function<std::string(int i)> namer);
  void setParams(real deltaTest, real errorTolerance);
  bool run(std::string name = "");
private:
  std::function<Eigen::VectorXr()> getPoint;
  std::function<real(const Eigen::VectorXr &)> value;
  std::function<Eigen::MatrixXr(const Eigen::VectorXr &)> hessian;
  std::function<std::string(int i)> getName = [](int i)->std::string {std::stringstream ss; ss << i; return ss.str();};

  real errorTolerance = 1.e-4;
  real deltaTest = 1.e-6;
  bool showOnlyErrorLines = false;
  Color::Modifier colNameColor {Color::FG_LIGHT_BLUE};
  Color::Modifier colNameBackground {Color::BG_DEFAULT};
  Color::Modifier colGoodValueColor {Color::FG_LIGHT_GREEN};
  Color::Modifier colBadValueColor {Color::FG_BLACK};
  Color::Modifier colGoodValueBackground {Color::BG_DEFAULT};
  Color::Modifier colBadValueBackground {Color::BG_LIGHT_RED};
};

#endif // FINITEDIFFERENCESTEST_H
