/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef GRADIENT_DESCENT_H
#define GRADIENT_DESCENT_H

#include <Eigen/Dense>
#include <functional>
#include "real.hpp"

class GradientDescent
{
public:
  GradientDescent(
       std::function<Eigen::VectorXr()> getSystemPoint,
       std::function<void(const Eigen::VectorXr &)> setSystemPoint,
       std::function<real(const Eigen::VectorXr &)> getEnergyValueAt,
       std::function<Eigen::VectorXr(const Eigen::VectorXr &)> getEnergyGradientAt,
       std::function<bool(const Eigen::VectorXr &)> haltCriterion);
  GradientDescent(
       std::function<Eigen::VectorXr()> getSystemPoint,
       std::function<void(const Eigen::VectorXr &)> setSystemPoint,
       std::function<real(const Eigen::VectorXr &)> getEnergyValueAt,
       std::function<Eigen::VectorXr(const Eigen::VectorXr &)> getEnergyGradientAt);
  
  bool run(int MaxIters = 100);
  
private:
  std::function<Eigen::VectorXr()> getPoint;
  std::function<void(const Eigen::VectorXr &)> setPoint;
  std::function<real(const Eigen::VectorXr &)> value;
  std::function<Eigen::VectorXr(const Eigen::VectorXr &)> gradient;
  std::function<bool(const Eigen::VectorXr &)> stopCriterion = [](const Eigen::VectorXr &gradient)->bool {return gradient.lpNorm<Eigen::Infinity>() < 1.e-7; };
  real tolerence = 1.e-8;
};

#endif // GRADIENT DESCENT_H
