/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "LineSearch.hpp"
#include <iostream>

LineSearch::LineSearch(
  std::function<Eigen::VectorXr()> getSystemPoint,
  std::function<void(const Eigen::VectorXr &)> setSystemPoint,
  std::function<real(const Eigen::VectorXr &)> getEnergyValueAt,
  std::function<Eigen::VectorXr(const Eigen::VectorXr &)> getDirectionAt):
  getPoint(getSystemPoint),
  setPoint(setSystemPoint),
  value(getEnergyValueAt),
  gradient(getDirectionAt)
{
}

bool LineSearch::run(int MaxIters)
{
  Eigen::VectorXr x = getPoint();
  std::cout << "Line search, initial value " << value(x) << std::endl;
  //1. find p
  Eigen::VectorXr p = gradient(x);
  //2. Line search
  real alpha = 100.;
  real E0 = value(x);
  real E;
  do {
    E = value(x + alpha * p);
    //std::cout<<"   alpha: "<<alpha<<", value: "<<E<<std::endl;
    alpha /= 2.;
  } while (E > E0 && (alpha > tolerence));
  if (!(alpha > tolerence)) {
    return false;
  }
  alpha *= 2.;
  std::cout << "Line search, final alpha=" << alpha << " , E=" << E << std::endl;
  //3. x
  x += alpha * p;
  setPoint(x);
  return true;
}
