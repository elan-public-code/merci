/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef LINESEARCH_H
#define LINESEARCH_H

#include <functional>
#include <Eigen/Dense>
#include "real.hpp"

class LineSearch
{
public:
  LineSearch(
    std::function<Eigen::VectorXr()> getSystemPoint,
    std::function<void(const Eigen::VectorXr &)> setSystemPoint,
    std::function<real(const Eigen::VectorXr &)> getEnergyValueAt,
    std::function<Eigen::VectorXr(const Eigen::VectorXr &)> getDirectionAt);

  bool run(int MaxIters = 100);

private:
  std::function<Eigen::VectorXr()> getPoint;
  std::function<void(const Eigen::VectorXr &)> setPoint;
  std::function<real(const Eigen::VectorXr &)> value;
  std::function<Eigen::VectorXr(const Eigen::VectorXr &)> gradient;
  real tolerence = 1.e-8;
};

#endif // LINESEARCH_H
