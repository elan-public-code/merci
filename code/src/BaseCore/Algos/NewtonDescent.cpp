/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "NewtonDescent.hpp"
#include "../Log/Logger.hpp"
#include <iostream>
#include <sstream>
#include <Eigen/LU>

NewtonDescent::NewtonDescent(
  std::function<Eigen::VectorXr()> getSystemPoint,
  std::function<void(const Eigen::VectorXr &)> setSystemPoint,
  std::function<real(const Eigen::VectorXr &)> getEnergyValueAt,
  std::function<Eigen::VectorXr(const Eigen::VectorXr &)> getEnergyGradientAt,
  std::function<Eigen::MatrixXr(const Eigen::VectorXr &)> getEnergyHessianAt):
  getPoint(getSystemPoint),
  setPoint(setSystemPoint),
  value(getEnergyValueAt),
  gradient(getEnergyGradientAt),
  hessian(getEnergyHessianAt)
{
}

NewtonDescent::NewtonDescent(
  std::function<Eigen::VectorXr()> getSystemPoint,
  std::function<void(const Eigen::VectorXr &)> setSystemPoint,
  std::function<real(const Eigen::VectorXr &)> getEnergyValueAt,
  std::function<Eigen::VectorXr(const Eigen::VectorXr &)> getEnergyGradientAt,
  std::function<Eigen::MatrixXr(const Eigen::VectorXr &)> getEnergyHessianAt,
  std::function<bool(const Eigen::VectorXr &)> haltCriterion):
  getPoint(getSystemPoint),
  setPoint(setSystemPoint),
  value(getEnergyValueAt),
  gradient(getEnergyGradientAt),
  hessian(getEnergyHessianAt),
  stopCriterion(haltCriterion)
{
}

bool NewtonDescent::run(int MaxIters)
{
  auto toString = [](real v)->std::string {std::stringstream ss; ss << v; return ss.str();};
  Eigen::VectorXr x = getPoint();
  LoggerMsg startingMessage("Starting");
  startingMessage.addChild("initial value", toString(value(x)));
  getLogger()->Write(LoggerLevel::INFORMATION, "Newton descent", startingMessage);
  //Use scientific notation
  for (int i = 0; i < MaxIters; i++) {
    //1. find p
    Eigen::VectorXr grad = gradient(x);
    if (stopCriterion(grad)) {
      LoggerMsg outMessage("Convergence criterion reached");
      getLogger()->Write(LoggerLevel::INFORMATION, "Newton descent", outMessage);
      setPoint(x);
      return true;
    }
    Eigen::VectorXr p = -grad;
    Eigen::MatrixXr h = hessian(x);
    real hdet = h.determinant();
    if (false || hdet > 1e-5) {
      p = h.fullPivLu().solve(p);
    } else {
      LoggerMsg lowdetMessage("Low determinant of hessian");
      lowdetMessage.addChild("determinant", toString(hdet));
      lowdetMessage.addChild("trace", toString(h.trace()));
      getLogger()->Write(LoggerLevel::WARNING, "Newton descent", lowdetMessage);
    }
    //2. Line search
    real E0 = value(x);
    real alpha = 1.;
    LoggerMsg iterMessage("Iteration");
    iterMessage.addChild("number", toString(i));
    iterMessage.addChild("initial value", toString(E0));
    real E;
    E = value(x + alpha * p);
    if (E < E0) { //try to be more agressive
      real EO;
      do {
        EO = E;
        alpha *= 2.;
        E = value(x + alpha * p);
      } while (E < E0 && E < EO);
      alpha /= 2;
      if (!(E < EO)) {
        E = E0;
      }
    } else {
      do {
        alpha /= 2.;
        E = value(x + alpha * p);
        //std::cout<<"   alpha: "<<alpha<<", value: "<<E<<std::endl;
      } while (E > E0 && (alpha > tolerence));
    }
    if (!(alpha > tolerence)) {
      LoggerMsg errorMessage("Line search fail");
      std::stringstream ss;
      ss << p;
      errorMessage.addChild("Search direction", ss.str());
      getLogger()->Write(LoggerLevel::INFORMATION, "Newton descent", iterMessage);
      getLogger()->Write(LoggerLevel::ERROR, "Newton descent", errorMessage);
      setPoint(x);
      return false;
    }
    p *= alpha;
    iterMessage.addChild("line search alpha", toString(alpha));
    iterMessage.addChild("line search value", toString(E));
    getLogger()->Write(LoggerLevel::INFORMATION, "Newton descent", iterMessage);
    x += p;
  }
  LoggerMsg warningMessage("Solver did not converge");
  getLogger()->Write(LoggerLevel::WARNING, "Newton descent", warningMessage);
  setPoint(x);
  return true;
}
