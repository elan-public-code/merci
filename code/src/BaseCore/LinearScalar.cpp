/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "LinearScalar.hpp"

LinearScalar::LinearScalar(real A, real B): A(A), B(B)
{
}

LinearScalar::LinearScalar(): A(0.), B(0.)
{
}

std::ostream &operator<<(std::ostream &os, const LinearScalar &ls)
{
  os << "l:[" << ls.A << "->" << ls.B << ']';
  return os;
}
