/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef LINEARSCALAR_H
#define LINEARSCALAR_H

#include <iostream>
#include "real.hpp"

class LinearScalar
{
public:
  LinearScalar(real A, real B);
  LinearScalar();
  // represent AX+B
  real A;
  real B;
};

std::ostream &operator<<(std::ostream &os, const LinearScalar &ls);

#endif // LINEARSCALAR_H
