/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "MathOperations.hpp"

real MathOperations::contractGeneralDotP(Eigen::Matrix3r A, Eigen::Matrix3r B)
{
  real res=0;
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      res += A(i, j) * B(i, j);
  return res;
}

real MathOperations::contractGeneralDotP(Eigen::Vector3r A, Eigen::Vector3r B)
{
  real res=0;
  for (int i = 0; i < 3; i++)
    res += A(i) * B(i);
  return res;
}

Eigen::Vector3r MathOperations::compDerivfg(Eigen::Vector3r segment, Eigen::Matrix<Eigen::Vector3r, 3, 1> pre)
{
  Eigen::Vector3r res;
  for(int i=0;i<3;i++)
    res[i]=pre[i].dot(segment);
  return res;
}

Eigen::Vector3r MathOperations::compDerivfg(Eigen::Matrix3r segment, Eigen::Matrix<Eigen::Matrix3r, 3, 1> pre)
{
  Eigen::Vector3r res;
  for (int a = 0; a < 3; a++)
    res(a) = contractGeneralDotP(segment, pre(a));

  return res;
}

Eigen::Matrix3r MathOperations::compDerivfg(Eigen::Matrix3r segment, Eigen::Matrix<Eigen::Matrix3r, 3, 3> pre)
{
   Eigen::Matrix3r res;
  for (int a = 0; a < 3; a++)
    for (int b = 0; b < 3; b++)
      res(a, b) = contractGeneralDotP(segment, pre(a, b));

  return res;
}

Eigen::Matrix4r MathOperations::compDeriv2(Eigen::Matrix<Eigen::Vector4r, 3, 3> d2XdYdK, Eigen::Matrix<Eigen::Vector4r, 3, 3> dYdK)
{
  Eigen::Matrix4r d2XdK2;
  d2XdK2.setZero();
  Eigen::Matrix3r d2XdYdK1, d2XdYdK2, d2XdYdK3, d2XdYdK0;
  for(int i=0;i<3;i++)
  {  for(int j=0;j<3;j++)
    {
      d2XdYdK0(i,j)=d2XdYdK(i,j)[0];
      d2XdYdK1(i,j)=d2XdYdK(i,j)[1];
      d2XdYdK2(i,j)=d2XdYdK(i,j)[2];
      d2XdYdK3(i,j)=d2XdYdK(i,j)[3];
    }
  }
  d2XdK2.row(0)=contractGeneralDotP(d2XdYdK0,dYdK);
  d2XdK2.row(1)=contractGeneralDotP(d2XdYdK1,dYdK);
  d2XdK2.row(2)=contractGeneralDotP(d2XdYdK2,dYdK);
  d2XdK2.row(3)=contractGeneralDotP(d2XdYdK3,dYdK);
  return d2XdK2;
}

Eigen::Matrix6r MathOperations::compDeriv2(Eigen::Matrix<Eigen::Vector6r, 3, 3> d2XdYdK, Eigen::Matrix<Eigen::Vector6r, 3, 3> dYdK)
{
  
  Eigen::Matrix6r d2XdK2;
  d2XdK2.setZero();
  Eigen::Matrix3r d2XdYdK0, d2XdYdK1, d2XdYdK2, d2XdYdK3, d2XdYdK4, d2XdYdK5;
  for(int i=0;i<3;i++)
  {  for(int j=0;j<3;j++)
    {
      d2XdYdK0(i,j)=d2XdYdK(i,j)[0];
      d2XdYdK1(i,j)=d2XdYdK(i,j)[1];
      d2XdYdK2(i,j)=d2XdYdK(i,j)[2];
      d2XdYdK3(i,j)=d2XdYdK(i,j)[3];
      d2XdYdK4(i,j)=d2XdYdK(i,j)[4];
      d2XdYdK5(i,j)=d2XdYdK(i,j)[5];
    }
  }
  d2XdK2.row(0)=contractGeneralDotP(d2XdYdK0,dYdK);
  d2XdK2.row(1)=contractGeneralDotP(d2XdYdK1,dYdK);
  d2XdK2.row(2)=contractGeneralDotP(d2XdYdK2,dYdK);
  d2XdK2.row(3)=contractGeneralDotP(d2XdYdK3,dYdK);
  d2XdK2.row(4)=contractGeneralDotP(d2XdYdK4,dYdK);
  d2XdK2.row(5)=contractGeneralDotP(d2XdYdK5,dYdK);
  return d2XdK2;
}
