/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef MATHOPERATIONS_H
#define MATHOPERATIONS_H

#include "real.hpp"
#include <Eigen/Dense>

namespace MathOperations
{
  real contractGeneralDotP(Eigen::Matrix3r A, Eigen::Matrix3r B);
  real contractGeneralDotP(Eigen::Vector3r A, Eigen::Vector3r B);
  template<typename T, int i, int j> T contractGeneralDotP(Eigen::Matrix<T, i, j> A, Eigen::Matrix<real,i,j> B)
  {
    T res; res.setZero();
    for (int a = 0; a < i; a++)
      for (int b = 0; b < j; b++)
        res += A(a, b) * B(a, b);
    return res;
  }
  template<typename T, int i, int j> T contractGeneralDotP(Eigen::Matrix<real,i,j> B, Eigen::Matrix<T, i, j> A)
  {
    T res; res.setZero();
    for (int a = 0; a < i; a++)
      for (int b = 0; b < j; b++)
        res += A(a, b) * B(a, b);
    return res;
  }
  
  template<typename T, int i, int j, int k> Eigen::Matrix<T, i, k> product(Eigen::Matrix<real,i,j> A, Eigen::Matrix<T, j, k> B)
  {
    Eigen::Matrix<T, i, k> res;
    for(int i_ind = 0; i_ind<i; i_ind++)
    {
      for(int k_ind = 0; k_ind<j; k_ind++)
      {
        res(i_ind, k_ind)=A(i_ind,0)*B(0,k_ind);
        for(int j_ind=1;j_ind<j;j_ind++)
          res(i_ind, k_ind)+=A(i_ind,j_ind)*B(j_ind,k_ind);
      }
    }
    return res;
  }
  
  template<typename T, int i, int j, int k, int l> Eigen::Matrix<T, i, j> compDeriv(Eigen::Matrix<Eigen::Matrix<real, k, l>, i, j> segment, Eigen::Matrix<T, k, l> pre)
  {
    Eigen::Matrix<T, i, j> res;
    for (int a = 0; a < i; a++)
      for (int b = 0; b < j; b++)
        res(a, b) = contractGeneralDotP(segment(a, b), pre);

  return res;
  }
  Eigen::Vector3r compDerivfg(Eigen::Vector3r gradg, Eigen::Matrix<Eigen::Vector3r, 3, 1> gradf);
  Eigen::Vector3r compDerivfg(Eigen::Matrix3r gradg, Eigen::Matrix<Eigen::Matrix3r, 3, 1> gradf);
  Eigen::Matrix3r compDerivfg(Eigen::Matrix3r gradg, Eigen::Matrix<Eigen::Matrix3r, 3, 3> gradf);
  
  
  template<typename T, int i, int j> Eigen::Matrix<real, i, j> extractDerivation(Eigen::Matrix<T,i,j> from, int comp)
  {
    Eigen::Matrix<real, i, j> res;
    for (int k = 0; k < i; k++)
      for (int l = 0; l < j; l++)
        res(k, l) = from(k, l)[comp];
    
  return res;
  }
  
  template<typename T, int i, int j> Eigen::Matrix<real, i, j> extractDerivation(Eigen::Matrix<T,i,j> from, int comp1, int comp2)
  {
    Eigen::Matrix<real, i, j> res;
    for (int k = 0; k < i; k++)
      for (int l = 0; l < j; l++)
        res(k, l) = from(k, l)(comp1,comp2);
    
  return res;
  }
  
  template<typename T> T addTransposeToItself(T mat)
  {
    return mat+mat.transpose().eval();
  }
  
  Eigen::Matrix4r compDeriv2(Eigen::Matrix<Eigen::Vector4r, 3, 3> d2XdYdK, Eigen::Matrix<Eigen::Vector4r, 3, 3> dYdK);
  Eigen::Matrix6r compDeriv2(Eigen::Matrix<Eigen::Vector6r, 3, 3> d2XdYdK, Eigen::Matrix<Eigen::Vector6r, 3, 3> dYdK);
};

#endif // MATHOPERATIONS_H
