/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "RepulsionField.hpp"
#include <cmath>

double RepulsionField::fun(double x) const
{
  switch(model){
    case Model::Linear:
      return std::max(x,0.);
    case Model::Exp:
      return x<0.?0.:1-std::exp(-x*x);
    case Model::Quadratic:
      return x<0.?0.:x*x;
    case Model::SoftWall:
      return -1.e-4/x;
  }
  throw std::string("Not in switch, ")+std::to_string(static_cast<int>(model));
}

double RepulsionField::dfun(double x) const
{
  switch(model){
    case Model::Linear:
      return x>0.?1.:0.;
    case Model::Exp:
      return x<0.?0.:2.*x*std::exp(-x*x);
    case Model::Quadratic:
      return x<0.?0.:2.*x;
    case Model::SoftWall:
      return 1.e-4/(x*x);
  }
  throw std::string("Not in switch, ")+std::to_string(static_cast<int>(model));
}

double RepulsionField::d2fun(double x) const
{
  switch(model){
    case Model::Linear:
      return 0.;
    case Model::Exp:
      return x<0.?0.:(2.-4.*x*x)*std::exp(-x*x);
    case Model::Quadratic:
      return x<0.?0.:2.;
    case Model::SoftWall:
      return -2.e-4/(x*x*x);
  }
 throw std::string("Not in switch, ")+std::to_string(static_cast<int>(model));
}

RepulsionField::RepulsionField(Eigen::Vector3r position, Eigen::Vector3r normale, real k, RepulsionField::Model model) :
  position(position),
  normale(-normale),
  k(k),
  model(model)
{
}

void RepulsionField::changeK(double newK)
{
  k=newK;
}

void RepulsionField::changeModel(RepulsionField::Model newModel)
{
  model=newModel;
}

real RepulsionField::energy(Eigen::Vector3r pt) const
{
  real d = (pt-position).dot(normale);
  return k*fun(d);
}

Eigen::Vector3r RepulsionField::gradient(Eigen::Vector3r pt) const
{
  real d = (pt-position).dot(normale);
  return k*dfun(d)*normale;
}

Eigen::Matrix3r RepulsionField::hessian(Eigen::Vector3r pt) const
{
  real d = (pt-position).dot(normale);
  return k*d2fun(d)*(normale*normale.transpose());
}
