/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef REPULSIONFIELD_H
#define REPULSIONFIELD_H

#include <Eigen/Dense>
#include "real.hpp"

class RepulsionField
{
public:
  enum class Model{Linear, Exp, Quadratic, SoftWall};
  RepulsionField(Eigen::Vector3r position, Eigen::Vector3r normale, real k=1., Model model=Model::Quadratic);
  void changeK(double newK);
  void changeModel(Model newModel);
  real energy(Eigen::Vector3r pt) const;
  Eigen::Vector3r gradient(Eigen::Vector3r pt) const;
  Eigen::Matrix3r hessian(Eigen::Vector3r pt) const;
private:
  Eigen::Vector3r position;
  Eigen::Vector3r normale;
  real k;
  Model model;
  
  double fun(double x) const;
  double dfun(double x) const;
  double d2fun(double x) const;
};

#endif // REPULSIONFIELD_H
