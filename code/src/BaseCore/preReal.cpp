/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "preReal.hpp"

preReal::preReal(real val): 
  current(val)
{
}

preReal::preReal()
{
}

preReal &preReal::operator=(const real val)
{
  current = val;
  return *this;
}


real &preReal::operator()()
{
  return current;
}


void preReal::save()
{
  old = current;
}


void preReal::restore()
{
  current = old;
}


preReal::operator real()
{
  return current;
}


bool preReal::hasDecreased()
{
  return current < old;
}


bool preReal::hasIncreased()
{
  return current > old;
}

