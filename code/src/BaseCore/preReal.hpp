/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef PREREAL_HPP
#define PREREAL_HPP

#include "real.hpp"

class preReal
{
public:
  preReal(real val);
  preReal();
  preReal &operator=(const real val);
  real &operator()();
  void save();
  void restore();
  operator real();
  bool hasDecreased();
  bool hasIncreased();
private:
  real current = 0.;
  real old = 0.;
};
#endif // PREREAL_HPP
