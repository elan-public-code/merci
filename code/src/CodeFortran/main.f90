program main
    use nrtype 
    implicit none
    
    integer(I4B):: i,k,nbrtotalappel,infos,nshoot,nshootRAPH,itermax
    integer(I4B), parameter :: segments=10
    integer(I4B), parameter :: n=2*segments+2
    integer(I4B), parameter :: sadowsky=1
    real(dp), dimension(n) ::  x, x0, fvec
    REAL(dp)::xk,error,gradientMax,energy

    ! === hybrid ===
    integer(I4B) maxfev,ml,mu,mode,nprint,info,nfev,ldfjac,lr,nbriter
    real(dp) xtol,epsfcn,factor
    real(dp), dimension(n,n) ::  fjac
    real(dp), dimension(n) :: diag,qtf,wa1,wa2,wa3,wa4
    real(dp), dimension(n*(n+1)/2) :: r

    INTERFACE  ! comme hybrid n est pas dans ce fichier, il faut lui dire ce que c est
    subroutine hybrd(fcn,n,x,fvec,xtol,maxfev,ml,mu,epsfcn,diag,mode,factor, &
                       nprint,info,nfev,fjac,ldfjac,r,lr,qtf,wa1,wa2,wa3,wa4,nbriter)
        integer n,maxfev,ml,mu,mode,nprint,info,nfev,ldfjac,lr,nbriter
        double precision xtol,epsfcn,factor
        double precision x(n),fvec(n),diag(n),fjac(ldfjac,n),r(lr),qtf(n),wa1(n),wa2(n),wa3(n),wa4(n)
        external fcn
    end subroutine hybrd
    END INTERFACE    
!-------------------------------------------------------------------------------

! --- structure du vecteur x
! Sur chaque segment on a
! w_1(s)=a_i s + b_i   et   eta(s)=n_i s + m_i
! (le s est relatif au segment)
! pour n=3 segments : 
! x = (b_1, m_1, a_1, n_1, a_2, n_2, a_3, n_3)
! i=1 : s=0;
! i=2 : s=1/3
! i=3 : s=2/3
! FIN structure du vecteur x

call ilovesadowsky(sadowsky)

call initRibbon(segments)
call createState(n,x0)

open(102,file="found.txt",status="unknown")
open(103,file="steps.txt",status="unknown")

 open(unit = 100, file = 'xk_g2.txt', status = 'old', action = 'read')
! open(unit = 100, file = 'xk_g1.txt', status = 'old', action = 'read')
! open(unit = 100, file = 'xk_config1_benchmark0p75_5seg.txt', status = 'old', action = 'read')
! open(unit = 100, file = 'xk_config1_benchmark0p75.txt', status = 'old', action = 'read')
! open(unit = 100, file = 'xk_config1_benchmark.txt', status = 'old', action = 'read')
! open(unit = 100, file = 'xk_K010_10seg.txt', status = 'old', action = 'read')
! open(unit = 100, file = 'xk.txt', status = 'old', action = 'read')
do k = 1,  n
    read(100,*) xk
    x0(k)=xk
    !print *, 'xk = ', x(k)
end do

nshootRAPH=1       ! nombre de tir avec methode raphael
nshoot=0       ! nombre de tir avec hybrd
!nbrtrouve=0     ! nombre de points isoles trouves
nbrtotalappel=0 ! nombre total d apels a la fonction
!nbrconverged=0  ! nombre de fois que newton a converge

call usrfun(n,x0,fvec,1) ! j ai mis 1 pour que usrfun n affiche rien
nbrtotalappel=nbrtotalappel+1
print*,'Config avec ',segments,' segments'
!print*,' x0    = ',(x0(k),k=1,n)
!print*,' f(x0) = ',(fvec(k),k=1,n)
error=sum(abs(fvec))
print*,'Erreur Norm[f(x0)] a l entree = ',error
print*,'Energy a l entree = ',energy(n,x0)
call savemesh('forme_debut.obj'); ! il faudrait passer l etat 'x' en argument
    
do i=1,nshoot ! si on utilise hybrd
    x=x0

! explications pour hybrid        
!      fcnsho ! in : nonlinear function of which we want to find roots
!      n      ! in : number of variables of the nonlinear function  
!      x      ! in : guessed variables
!      fvec     ! out: valeur de la fonction au point de sortie
       xtol=5.0d-8 ! in : relative error under which : convergence
       maxfev=200  ! in : max number of iterations (evaluation of f)
       ml=n-1    ! in : because it is not a band matrix
       mu=n-1    ! in : because it is not a band matrix
       epsfcn=1D-10 ! in : to compute the numerical derivate
!      diag         ! in : array of multiplicative factors for variables
       mode=1       ! in : =1 : variables scaled internaly 
       factor=1.0D2 ! in : factor of the box to prevent jumping
       nprint=1     ! in : =3 si on print les variables toutes les 3 iter. (code iflag dans fcnsho)
!      info   ! out : 1=ok, 2=reached max iter , etc.
!      nfev   ! out : number of call to the fonction
!      fjac   ! out : array to do with q r fact.
       ldfjac=n     ! in : leading dim in the q r factorisation
!      r            ! out : array of length lnx
       lr=n*(n+1)/2 ! in  : integer : n n+1 / 2 (set as parameter)
!      qtf          ! out : array
!      wa1 2 3 4       ! out : arrays of length n
!      nbriter  ! out : number of iterations done

    call hybrd(usrfun,n,x,fvec,xtol,maxfev,ml,mu,epsfcn,diag,mode,factor, &
              nprint,info,nfev,fjac,ldfjac,r,lr,qtf,wa1,wa2,wa3,wa4,nbriter)

    nbrtotalappel=nbrtotalappel+nfev
    
    infos=1
              
    if (infos.ge.1) then 
        if (info.eq.1) then
            print*,' Shooting converged! ',nfev,' calls to function and',nbriter,'iterations'
            if (infos.ge.2) then 
                print*,' Near : x=',(x0(k),k=1,n)
                print*,' Found: x=',(x(k),k=1,n)
                print*,' -----'
            endif
        endif
        if (info.eq.2) print*,'No convergence: max steps reached : ',nfev,'calls to function et', nbriter,'iterations'
        if (info.eq.3) print*,'No convergence: you ask for too small value of function'
        if ((info.eq.4).OR.(info.eq.5)) print*,'Converging too slowly (',nfev,' calls to function)'
    endif
        
enddo   
 
do i=1,nshootRAPH   ! si on utilise Newton Descent
    itermax=30
    gradientMax=1d-8
    call setpoint(n, x0)
    infos=1
    call newton(itermax, gradientMax, x,infos)
enddo    

call usrfun(n,x,fvec,1) ! j ai mis 1 pour que usrfun n affiche rien
nbrtotalappel=nbrtotalappel+1
error=sum(abs(fvec))   
    
!print*,' End At   : x=',(x(k),k=1,n)
!print*,'          : f(x)=',(fvec(k),k=1,n)
print*,'Erreur Norm[f(x)] a la sortie = ',error
print*,'Energy a la sortie = ',energy(n,x)
!print*,nbrtotalappel,' evaluations de la fonction.'
if (error < 1.0D-7) then
    !write(*,*) 'on converge vers ', x
    !write(102,"(1E17.8)") (x(k),k=1,n)  ! fichier found.txt
endif

call healthrepport()
call savemesh('forme_fin.obj');
! axes x en bleu, y en rouge et z en vert

end ! program main
!===============================================================================




!===============================================================================
SUBROUTINE initRibbon(segments)
    USE nrtype
    IMPLICIT NONE
    integer(I4B) i,segments
    REAL(dp), dimension(segments)::segmentsLength
    REAL(dp), dimension(segments+1)::segmentsNatCurv
    REAL(dp)::theta,width,thickness,surfassicMass,D,poissonRatio,totallength
    real(dp) kappa0

!-------------------------------------------------------------------------------
    open(unit = 101, file = 'param_g1g2.txt', status = 'old', action = 'read')
!    open(unit = 101, file = 'param_config1_benchmark.txt', status = 'old', action = 'read')
!    open(unit = 101, file = 'param_K010_Gxxx.txt', status = 'old', action = 'read')
    read(101,*) theta
    read(101,*) width
    read(101,*) thickness
    read(101,*) surfassicMass
    read(101,*) D
    read(101,*) poissonRatio
    read(101,*) totallength
    read(101,*) kappa0
    
    call setParameters(segments,theta,width,thickness,surfassicMass,D,poissonRatio)
    do i = 1, segments
        segmentsLength(i)=totallength/segments
    end do
!   print*,' setting lengths'
    call setLengths(segments,segmentsLength)
    do i = 1, segments+1
        segmentsNatCurv(i)=kappa0 
    end do 
!   print*,' setting natural curvature'
    call setNatCurvs(segments+1,segmentsNatCurv)
!   print*,' finilize'
    call finilizeRibbon()

END SUBROUTINE initRibbon
!===============================================================================

!===============================================================================
SUBROUTINE createState(n,x)
    USE nrtype
    IMPLICIT NONE
    integer(I4B) i,getDataSize,n
    REAL(DP), DIMENSION(n), INTENT(OUT) :: x

    ! CETTE ROUTINENE SERT A RIEN, ON PEUT LA COURT CIRCUITER, NON ?
    ! CETTE ROUTINENE SERT A RIEN, ON PEUT LA COURT CIRCUITER, NON ?
                
!-------------------------------------------------------------------------------
!   print*,' setting parameters'
  call getInitPoint(n,x)

END SUBROUTINE createState
!===============================================================================



!===============================================================================
SUBROUTINE usrfun(n,x,fvec,iflag)
    USE nrtype
    IMPLICIT NONE
    integer(I4B) n,iflag,i,k
    REAL(DP), DIMENSION(n), INTENT(IN) :: x
    REAL(DP), DIMENSION(n), INTENT(OUT) :: fvec
    real(dp)::energy, e


    !e=energy(n,x)
    !print*,' energy = ',e
    call gradient(n,x,fvec)
    ! mettre iflag=-1 si probleme enorme : abort !
    
    if (iflag.eq.0) then
        !write(103,"(100E17.8)") (x(k),k=1,n)  ! fichier steps.txt
        print*,' (usrfun)---> x = ',(x(k),k=1,3)
    endif

END SUBROUTINE usrfun
!===============================================================================
