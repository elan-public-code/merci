/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "FibreMesher.hpp"
#include <SuperClothoidGeometrieSerie.hpp>
#include <iomanip>

FibreMesher::FibreMesher()
{
  meshMainColor = Eigen::Vector3i(200, 0, 0);
  meshSecondColor = Eigen::Vector3i(255, 150, 0);
}

FibreMesher::FibreMesher(Eigen::Vector3i maincolor, Eigen::Vector3i secondcolor):
  meshMainColor(maincolor),
  meshSecondColor(secondcolor)
{
}

std::vector< ObjFibreVertex > FibreMesher::setupFibrePart(FibrePartSetup& setup)
{
  SuperClothoidGeometrieSerie fibre;
  fibre.Place(setup.posOr, setup.frameOr);
  fibre.Setup(setup.omega1, setup.omega2,setup.omega3);
  setup.smax = fibre.estimSMax();
  real s1 = setup.smax;
  if (s1 + setup.s0 >= setup.segmentLength) {
    s1 = setup.segmentLength - setup.s0;
    setup.lastPart = true;
  }
  std::vector<ObjFibreVertex> path;
  int segment = static_cast<int>(s1 / meshDiscreteLength.value_or(std::max(setup.crossSectionWidth,setup.crossSectionHeight)) + 1.);
  for (int i = 0; i <= segment; i++) {
    real s = (static_cast<real>(i) / segment) * s1;
    auto [pos, frame] = fibre.PosFrame(s);
    ObjFibreVertex v;
    for (int j = 0; j < 9; j++)v.frame(j % 3, j / 3) = frame(j % 3, j / 3);
    for (int j = 0; j < 3; j++)v.pos[j] = pos(j);
    path.push_back(v);
  }
  setup.sout = setup.s0 + s1;
  std::tie(setup.posF, setup.frameF) = fibre.PosFrame(s1);
  return path;
}

void FibreMesher::computeSegment(SuperClothoid &data, int segmentNumber, Eigen::Vector3r &pos, Eigen::Matrix3r &frame, int &MeshOffset)
{
  LinearScalar omega1 = data.getOmega1(segmentNumber);
  LinearScalar omega2 = data.getOmega2(segmentNumber);
  LinearScalar omega3 = data.getOmega3(segmentNumber);
  std::vector<std::vector<ObjFibreVertex> > preMeshes;
  FibrePartSetup rps;
  rps.frameOr = frame;
  rps.posOr = pos;
  rps.omega1 = omega1;
  rps.omega2 = omega2;
  rps.omega3 = omega3;
  rps.s0 = 0.;
  rps.segmentLength = data.getLength(segmentNumber);
  rps.crossSectionWidth = data.getCrossSection().x();
  rps.crossSectionHeight = data.getCrossSection().y();
  do {
    preMeshes.push_back(setupFibrePart(rps));
    rps.frameOr = rps.frameF;
    rps.posOr = rps.posF;
    rps.s0 = rps.sout;
    rps.omega1.B = omega1.B + rps.s0 * omega1.A;
    rps.omega1.A = omega1.A;
    rps.omega2.B = omega2.B + rps.s0 * omega2.A;
    rps.omega2.A = omega2.A;
    rps.omega3.B = omega3.B + rps.s0 * omega3.A;
    rps.omega3.A = omega3.A;
    rps.partnb++;
  } while (!rps.lastPart);
  frame = rps.frameF;
  pos = rps.posF;

  uint t = preMeshes.size();
  for (uint i = 0; i < t; i++) {
    std::vector<ObjFibreVertex> path = preMeshes[i];
    theFibre.push_back(FibrePart(ObjFibreMesh(path, csw, csh),(segmentNumber + 1) % 2));
  }
  MeshOffset += t;
}

void FibreMesher::setColors(Eigen::Vector3i main, Eigen::Vector3i second)
{
  meshMainColor = main;
  meshSecondColor = second;
}

void FibreMesher::set(SuperClothoid ribbon)
{
  theFibre.clear();
  int t = ribbon.getNbSegments();
  Eigen::Vector3r pos = ribbon.getPosOr();
  Eigen::Matrix3r frame = ribbon.getFrameOr();
  int Mo = 0;
  for (int i = 0; i < t; i++)
    computeSegment(ribbon, i, pos, frame, Mo);
}

void FibreMesher::setMeshDiscreteLength(std::optional<double> mdl)
{
  meshDiscreteLength = mdl;
}

void FibreMesher::setCrossSection(double w, double h)
{
  csw=w;
  csh=h;
}

void FibreMesher::setFibreName(std::string name)
{
  fibreName = name;
}

void FibreMesher::clear()
{
  theFibre.clear();
}

void FibreMesher::save(libobj::ObjFileWriter &os)
{
  std::stringstream ssmaincolor;
  ssmaincolor << "m" << std::setw(2) << std::setfill('0') << std::hex << meshMainColor.x() << std::setw(2) << std::setfill('0') << std::hex << meshMainColor.y() << std::setw(2) << std::setfill('0') << std::hex << meshMainColor.z();
  std::string namemaincolor = ssmaincolor.str();
  Eigen::Vector3f colmain(meshMainColor.x() / 255., meshMainColor.y() / 255., meshMainColor.z() / 255.);
  libobj::Mtl matmain(namemaincolor, colmain);

  std::stringstream sssecondcolor;
  sssecondcolor << "m" << std::setw(2) << std::setfill('0') << std::hex << meshSecondColor.x() << std::setw(2) << std::setfill('0') << std::hex << meshSecondColor.y() << std::setw(2) << std::setfill('0') << std::hex << meshSecondColor.z();
  std::string namesecondcolor = sssecondcolor.str();
  Eigen::Vector3f colsecond(meshSecondColor.x() / 255., meshSecondColor.y() / 255., meshSecondColor.z() / 255.);
  libobj::Mtl matsecond(namesecondcolor, colsecond);

  static size_t fibreIdx = 0;
  if(!fibreName.empty())
  {
    os << libobj::ObjObject(fibreName);
  }
  else
  {
    std::stringstream ss;
    ss << "fibre" << fibreIdx++;
    os << libobj::ObjObject(ss.str());
  }
  for (auto ent : theFibre) {
    if (ent.maincolor) os << matmain;
    else os << matsecond;
    os << ent.mesh;
  }
}
