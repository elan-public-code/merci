/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef FIBREMESHER_H
#define FIBREMESHER_H

#include <vector>
#include <SuperClothoidCore/SuperClothoid.hpp>
#include <optional>
#include "ObjFibreMesh.hpp"


class FibreMesher
{
public:
  FibreMesher();
  FibreMesher(Eigen::Vector3i maincolor, Eigen::Vector3i secondcolor);
  void setFibreName(std::string name);
  void set(SuperClothoid ribbon);
  void setMeshDiscreteLength(std::optional<double> mdl);
  void setCrossSection(double w, double h);
  void clear();
  void setColors(Eigen::Vector3i main, Eigen::Vector3i second);
  void save(libobj::ObjFileWriter& os);
private:
  struct FibrePart {
    FibrePart(ObjFibreMesh m,bool mc):mesh(m),maincolor(mc){}
    ObjFibreMesh mesh;
    bool maincolor;
  };

  struct FibrePartSetup {
    uint partnb;
    real s0;
    real sout;
    real smax;
    real segmentLength;
    real crossSectionWidth;
    real crossSectionHeight;
    bool lastPart = false;
    LinearScalar omega1;
    LinearScalar omega2;
    LinearScalar omega3;
    Eigen::Vector3r posOr;
    Eigen::Vector3r posF;
    Eigen::Matrix3r frameOr;
    Eigen::Matrix3r frameF;
  };
  std::optional<double> meshDiscreteLength;
  double csw;
  double csh;
  Eigen::Vector3i meshMainColor;
  Eigen::Vector3i meshSecondColor;
  std::string fibreName;
  std::vector<FibrePart> theFibre;
  std::vector<ObjFibreVertex> setupFibrePart(FibrePartSetup&);
  void computeSegment(SuperClothoid& data, int segmentNumber, Eigen::Vector3r& pos, Eigen::Matrix3r& frame, int& MeshOffset);
};

#endif // FIBREMESHER_H
