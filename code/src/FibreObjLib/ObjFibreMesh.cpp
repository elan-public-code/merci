/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "ObjFibreMesh.hpp"


ObjFibreMesh::ObjFibreMesh(std::vector<ObjFibreVertex> path, double width, double height): libobj::TriMesh()
{
  size_t subdivision = path.size()-1;
  resize(8*(1 * subdivision + 1 ) + 18, 16 * subdivision+16);
  auto idxOf=[](int triid, int vertex){return 8*triid+(vertex%8);};
//INDICES
  int t = subdivision;
  for (int i = 0; i < t; i++) {
    for(int j=0;j<8;j++)
    {
      setFace(16* i + j, idxOf(i,j), idxOf(i+1,j), idxOf(i,j+1));
      setFace(16* i + 8 + j, idxOf(i,j+1), idxOf(i+1,j), idxOf(i+1,j+1));
    }
  }

  int offset = 16*t;
  
  t = path.size();
  for (int i = 0; i < 8; i++) {
    setFace(offset + i, 8*t+i,8*t+((i+1)%8), 8*t+8);
    setFace(offset + 8 + i, 8*t+9+i, 8*t+9+8, 8*t+9+((i+1)%8));
  }

//VERTICES
  const double lambda=1./sqrt(2.);
  for (int i = 0; i < t; i++) {
    Eigen::Vector3d midpos = path.at(i).pos;
    Eigen::Matrix3d frame = path.at(i).frame;
    Eigen::Vector3d frameX(frame(0, 0), frame(1, 0), frame(2, 0)), frameY(frame(0, 1), frame(1, 1), frame(2, 1));
    Eigen::Vector3d diag=0.5*width*lambda*frameX + 0.5*height*lambda * frameY;
    Eigen::Vector3d diagsym=0.5*width*lambda*frameX - 0.5*height*lambda * frameY;
    Eigen::Vector3d diagdir=diag.normalized();
    Eigen::Vector3d diagdirsym=diagsym.normalized();
    setVertex(8 * i + 0, midpos + 0.5*width*frameX,  frameX);
    setVertex(8 * i + 1, midpos + diagsym,           diagdirsym);
    setVertex(8 * i + 2, midpos - 0.5*height*frameY, -frameY);
    setVertex(8 * i + 3, midpos - diag,              -diagdir);
    setVertex(8 * i + 4, midpos - 0.5*width*frameX,  -frameX);
    setVertex(8 * i + 5, midpos - diagsym,           -diagdirsym);
    setVertex(8 * i + 6, midpos + 0.5*height*frameY, frameY);
    setVertex(8 * i + 7, midpos + diag,              diagdir);
  }
  {
    Eigen::Vector3d midpos = path.at(0).pos;
    Eigen::Matrix3d frame = path.at(0).frame;
    Eigen::Vector3d frameX(frame(0, 0), frame(1, 0), frame(2, 0)), frameY(frame(0, 1), frame(1, 1), frame(2, 1)), frameZ(frame(0, 2), frame(1, 2), frame(2, 2));
    Eigen::Vector3d diag=0.5*width*lambda*frameX + 0.5*height*lambda * frameY;
    Eigen::Vector3d diagsym=0.5*width*lambda*frameX - 0.5*height*lambda * frameY;
    
    setVertex(8 * t + 0, midpos + 0.5*width*frameX,  -frameZ);
    setVertex(8 * t + 1, midpos + diagsym,           -frameZ);
    setVertex(8 * t + 2, midpos - 0.5*height*frameY, -frameZ);
    setVertex(8 * t + 3, midpos - diag,              -frameZ);
    setVertex(8 * t + 4, midpos - 0.5*width*frameX,  -frameZ);
    setVertex(8 * t + 5, midpos - diagsym,           -frameZ);
    setVertex(8 * t + 6, midpos + 0.5*height*frameY, -frameZ);
    setVertex(8 * t + 7, midpos + diag,              -frameZ);
    setVertex(8 * t + 8, midpos,                     -frameZ);
  }
  {
    Eigen::Vector3d midpos = path.at(t-1).pos;
    Eigen::Matrix3d frame = path.at(t-1).frame;
    Eigen::Vector3d frameX(frame(0, 0), frame(1, 0), frame(2, 0)), frameY(frame(0, 1), frame(1, 1), frame(2, 1)), frameZ(frame(0, 2), frame(1, 2), frame(2, 2));
    Eigen::Vector3d diag=0.5*width*lambda*frameX + 0.5*height*lambda * frameY;
    Eigen::Vector3d diagsym=0.5*width*lambda*frameX - 0.5*height*lambda * frameY;
    
    setVertex(8 * t + 9 + 0, midpos + 0.5*width*frameX,  frameZ);
    setVertex(8 * t + 9 + 1, midpos + diagsym,           frameZ);
    setVertex(8 * t + 9 + 2, midpos - 0.5*height*frameY, frameZ);
    setVertex(8 * t + 9 + 3, midpos - diag,              frameZ);
    setVertex(8 * t + 9 + 4, midpos - 0.5*width*frameX,  frameZ);
    setVertex(8 * t + 9 + 5, midpos - diagsym,           frameZ);
    setVertex(8 * t + 9 + 6, midpos + 0.5*height*frameY, frameZ);
    setVertex(8 * t + 9 + 7, midpos + diag,              frameZ);
    setVertex(8 * t + 9 + 8, midpos,                     frameZ);
  }
  defineAsGroup(true);
}

std::string ObjFibreMesh::getObjName()
{
  static int idx = 0;
  return "fibre_part_" + std::to_string(idx);
}
