/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef OBJFIBREMESH_H
#define OBJFIBREMESH_H

#include <ObjLib/TriMesh.hpp>
#include <vector>
#include <BaseCore/real.hpp>

class ObjFibreVertex
{
public:
  Eigen::Vector3d pos;
  Eigen::Matrix3d frame;
};

class ObjFibreMesh : public libobj::TriMesh
{
public:
  
  ObjFibreMesh(std::vector<ObjFibreVertex> path, double width, double height);
  virtual std::string getObjName() override;
};

#endif // OBJFIBREMESH_H
