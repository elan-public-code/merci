/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include <cminpack.h>
#include <functional>
#include "interface.hpp"


int awfnc(void* paramStructData,int n, const double* x,double* fvec,int iflag)
{
  RibbonSystem* phY=reinterpret_cast<RibbonSystem*>(paramStructData);
  Eigen::VectorXr px(n);
  for(int i=0;i<n;i++)
    px[i]=x[i];
  phY->setPoint(px);
  auto g=phY->gradient();
  for(int i=0;i<n;i++)
    fvec[i]=g[i];
  return 0;
}

int callHybridOnSystem(RibbonSystem& phY, HybridParams params)
{
  Eigen::VectorXr pt=phY.getPoint();
  int n=pt.size();
  double* x = new double[n];
  for(int i=0;i<n;i++) x[i]=pt[i];
  double* fvec = new double[n];
  int ml=n-1; // because it is not a band matrix
  int mu=n-1; // because it is not a band matrix
  int mode=1;
  double* diag=new double[n];
  int* nfev=new int;
  double* fjac=new double[n*n];
  int ldfjac=n; // leading dim in the q r factorisation
  int lr = n*(n+1)/2; // whatever
  double* r=new double[lr];
  double* qtf=new double[n];
  double* wa1=new double[n];
  double* wa2=new double[n];
  double* wa3=new double[n];
  double* wa4=new double[n];
  
  void* paramStructData=reinterpret_cast<void*>(&phY);
  
  int result=hybrd(awfnc,
        paramStructData,
        n,x,fvec,params.xtol,params.maxfev,
        ml,mu, params.epsfcn,diag,mode,
        params.factor,params.nprint,nfev,
        fjac,ldfjac,r,lr,qtf,
        wa1,wa2,wa3,wa4);
  
  for(int i=0;i<n;i++) pt[i]=x[i];
  phY.setPoint(pt);
  
  delete[] x;
  delete[] fvec;
  delete[] diag;
  delete nfev;
  delete[] fjac;
  delete[] r;
  delete[] wa1;
  delete[] wa2;
  delete[] wa3;
  delete[] wa4;
  return result;
}

void initHybridParamsClass(sol::state & lua)
{
    lua.new_usertype<HybridParams>(
      "HybridParams", sol::constructors<HybridParams()>(),
      "xtol",&HybridParams::xtol,
      "maxfev",&HybridParams::maxfev,
      "epsfcn", &HybridParams::epsfcn,
      "factor", &HybridParams::factor,
      "nprint", &HybridParams::nprint
    );
}
