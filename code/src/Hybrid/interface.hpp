/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef INTERFACE_HYBRID_HPP
#define INTERFACE_HYBRID_HPP

#include <RibbonSystem.hpp>
#include "sol.hpp"

struct HybridParams
{
  double xtol=5.0e-8; // relative error under which : convergence
  int maxfev=200; // max number of iterations (evaluation of f)
  double epsfcn=1e-10; // to compute the numerical derivate;
  double factor=1.e2; // factor of the box to prevent jumping
  int nprint=1; //=3 si on print les variables toutes les 3 iter. (code iflag dans fcnsho)
};

extern "C"{
void initHybridParamsClass(sol::state &lua);

int callHybridOnSystem(RibbonSystem& phY, HybridParams params);
}
#endif
