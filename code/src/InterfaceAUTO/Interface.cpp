/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "Interface.hpp"
#include <SuperRibbon.hpp>
#include <RibbonSystem.hpp>
#include <SuperRibbonMesher.hpp>
#include <ArrowMesh.hpp>
#include <SRStaticProblem.hpp>
#include <cppoptlib/solver/newtondescentsolver.h>
#include <Logger.hpp>

SuperRibbon sr;

//RibbonSystem sys(Eigen::Vector3r(0.,0.,-9.81));
RibbonSystem sys(Eigen::Vector3r(-1.,0.,0.)); // by seb 22 fev 2019

void setparameters_(int* nbSegments, double* ptheta, double* width, double* thickness, double* areaDensity, double* D, double* poissonRatio)
{
  sr=SuperRibbon();
  for(int i=0;i<*nbSegments;i++)
    sr.addSegmentAt(i);
  sr.setWidth(*width);
  sr.setThickness(*thickness);
  sr.setAreaDensity(*areaDensity);
  sr.setD(*D);
  sr.setPoissonRatio(*poissonRatio);
  Eigen::Matrix3r f;
  double theta=*ptheta;
/*  f<<0,0,1,
    -sin(theta), -cos(theta), 0,
     cos(theta), -sin(theta), 0;
*/
  f<<    cos(theta), -sin(theta), 0, // by seb 22 fev 2019
         sin(theta),  cos(theta), 0,
         0         ,  0         , 1  ;

  sr.setFrameOr(f);
}

void setlengths_(int* N, double* data)
{
  for(int i=0;i<*N;i++)
    sr.setLength(i,data[i]);
}

void setnatcurvs_(int* N, double* data)
{
  for(int i=0;i<*N-1;i++)
    sr.setNatOmega1(i,LinearScalar( (data[i+1]-data[i])/sr.getLength(i),data[i]));
}

void finilizeribbon_()
{
  sys.set(sr);
}

double normg_(int* N, double* data)
{
  setpoint_(N,data);
  return sys.gradient().squaredNorm();
}

double energy_(int* N, double* data)
{
  setpoint_(N,data);
  double en=sys.energy();
  return en;
}

void gradient_(int* N, double* data, double* res)
{
  setpoint_(N,data);
  auto g=sys.gradient();
  for(int i=0;i<*N;i++)
    res[i]=g[i];
}

int getdatasize_()
{
  return sys.vpointSize();
}

void getinitpoint_(int* N, double* data)
{
  Eigen::VectorXr datapt = sys.getPoint();
  for(int i=0;i<*N;i++)
    data[i]=datapt[i];
}

void setpoint_(int* N, double* data)
{
  Eigen::VectorXr datapt = sys.getPoint();
  for(int i=0;i<*N;i++)
    datapt[i]=data[i];
  sys.setPoint(datapt);
}

void newton_(int* itermax, double* gradientMax, double* res, int* info)
{
  if(*info)
    getLogger()->switchToConsole();
  else
    getLogger()->switchToBlackHole();
  RibbonSystem phY(Eigen::Vector3r(0.,0.,-9.81));
  SRStaticProblem pb(sys);
  cppoptlib::NewtonDescentSolver<SRStaticProblem> solver;
  cppoptlib::NewtonDescentSolver<SRStaticProblem>::TCriteria m_stop =
  cppoptlib::NewtonDescentSolver<SRStaticProblem>::TCriteria::defaults();
  m_stop.gradNorm = *gradientMax;
  m_stop.fDelta = 0.;
  m_stop.iterations = *itermax;
  solver.setStopCriteria(m_stop);
  solver.setDebug(cppoptlib::DebugLevel::High);
  Eigen::VectorXr pt = pb.getPoint();
  solver.minimize(pb, pt);
  for(int i=0;i<pt.size();i++)
    res[i]=pt[i];
  return;
}

void savemesh_(char* filename)
{
  std::string fn=filename;
  size_t found=fn.find(".obj");
  if(found!=std::string::npos)
  {
    fn=fn.substr(0,found+4);
  }
  libobj::ObjFileWriter writer;
  writer.open(fn);
  Eigen::Vector3i green(78,154,6);
  Eigen::Vector3i purple(173,127,168);
  SuperRibbonMesher mesher(purple, green);
  mesher.setMeshDiscreteLength(0.001);
  sr=sys.get();
  mesher.setRibbonThickness(sr.getThickness());
  mesher.set(sr);
  mesher.save(writer);
  sr.setOmega1BOr(sr.getNatOmega1(0).value_or(LinearScalar(0.,0.)).B);
  sr.setEtaMOr(0);
  for(size_t i=0;i<sr.getNbSegments();i++)
  {
    sr.setOmega1A(i,sr.getNatOmega1(i).value_or(LinearScalar(0.,0.)).A);
    sr.setEtaN(i,0.);
  }
  Eigen::Vector3i orange(236,135,2);
  Eigen::Vector3i blue(25,201,218);
  mesher.setColors(orange,blue);
  mesher.set(sr);
  mesher.save(writer);
  double scale=2.0*sr.getWidth();
  {
    static const std::string name = "frameX";
    libobj::Mtl matX("frameXMat",Eigen::Vector3f(0.,0.,1.));
    ArrowMesh aX(name,sr.getPosOr(),Eigen::Vector3d(1.,0.,0.),scale);
    writer<<matX<<aX;
  }
  {
    static const std::string name = "frameY";
    libobj::Mtl matY("frameYMat",Eigen::Vector3f(1.,0.,0.));
    ArrowMesh aY(name,sr.getPosOr(),Eigen::Vector3d(0.,1.,0.),scale);
    writer<<matY<<aY;
  }
  {
    static const std::string name = "frameZ";
    libobj::Mtl matZ("frameZMat",Eigen::Vector3f(0.,1.,0.));
    ArrowMesh aZ(name,sr.getPosOr(),Eigen::Vector3d(0.,0.,1.),scale);
    writer<<matZ<<aZ;
  }
  writer.close();
}

void healthrepport_()
{
  sys.logHealthReport();
}

void ilovesadowsky_(int* iwantsadowsky)
{
  ElasticEnergy::YesIWantToUseSadowsky(*iwantsadowsky);
}
