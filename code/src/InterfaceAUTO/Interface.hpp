/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef INTERFACE_AUTO_H
#define INTERFACE_AUTO_H

#ifdef __cplusplus
extern "C" {
#endif

//Call this first
void setparameters_(int* nbSegments, double* theta, double* width, double* thickness, double* areaDensity, double* D, double* poissonRatio);

//Then this
void setlengths_(int* N, double* data);

//And finally that (Defines curves between nodes, i.e. nbSegments+1 curvatures)
void setnatcurvs_(int* N, double* data);

//Call this after calling the three fonctions above and before calling getDataSize
void finilizeribbon_();

// !WARNING not calling the functions above in order will result in mistaken state/not working code

double normg_(int* N, double* data);
double energy_(int* N, double* data);
void gradient_(int* N, double* data, double* res);

int getdatasize_();
void getinitpoint_(int* N, double* data);
void setpoint_(int* N, double* data);

void savemesh_(char* filename);
void healthrepport_();

void newton_(int* itermax, double* gradientMax, double* res, int* info);

void ilovesadowsky_(int* iwantsadowsky);

#ifdef __cplusplus
};
#endif

#endif //INTERFACE_AUTO_H
