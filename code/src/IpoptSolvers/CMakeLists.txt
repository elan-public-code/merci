project(IpoptSolvers)
include_directories(${CMAKE_SOURCE_DIR}/src/SuperRibbonCore)
include_directories(${CMAKE_SOURCE_DIR}/src/MixedSRCore)
include_directories(${CMAKE_SOURCE_DIR}/src/SuperClothoidCore)
include_directories(${CMAKE_SOURCE_DIR}/src/BaseCore)
#include_directories(${CMAKE_SOURCE_DIR}/src/RibbonCore)

find_package(Eigen3 REQUIRED)
find_package(IPOPT REQUIRED)
include_directories(
   SYSTEM ${EIGEN3_INCLUDE_DIR}
   ${IPOPT_INCLUDE_DIR}
)

link_directories(${IPOPT_LIBRARY_DIR})

set(IpoptSolvers_HEADERS 
          IpoptMixedRibbon.hpp
          IpoptSuperRibbon.hpp
          IpoptFibre.hpp
)

set(IpoptSolvers_CPP 
          IpoptMixedRibbon.cpp
          IpoptSuperRibbon.cpp
          IpoptFibre.cpp
          )
          
add_library(IpoptSolvers SHARED ${IpoptSolvers_CPP})
target_link_libraries(IpoptSolvers 
    SuperRibbonCore 
    SuperClothoidCore
    MixedSRCore
    BaseCore
    ${IPOPT_LIBRARIES}
    )

install(TARGETS IpoptSolvers EXPORT MERCITargets LIBRARY DESTINATION lib)
install(FILES ${IpoptSolvers_HEADERS} DESTINATION include/IpoptSolvers)
