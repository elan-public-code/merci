/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "IpoptFibre.hpp"
#include <limits>
//See https://www.coin-or.org/Ipopt/documentation/node23.html

#include <IpIpoptCalculatedQuantities.hpp>
#include <IpIpoptData.hpp>
#include <IpTNLPAdapter.hpp>
#include <IpOrigIpoptNLP.hpp>
  
IpoptFibre::IpoptFibre(FiberSystem phY)
  : phY(phY)
{
}


void IpoptFibre::finalize_solution(Ipopt::SolverReturn status, Ipopt::Index n, const Ipopt::Number *x, const Ipopt::Number *z_L, const Ipopt::Number *z_U, Ipopt::Index m, const Ipopt::Number *g, const Ipopt::Number *lambda, Ipopt::Number obj_value, const Ipopt::IpoptData *ip_data, Ipopt::IpoptCalculatedQuantities *ip_cq)
{
  setPoint(n, x);
  outsc=phY.get();
}

void IpoptFibre::setPoint(Ipopt::Index n, const Ipopt::Number *x)
{
  Eigen::VectorXr pt(n);
  for (Ipopt::Index i = 0; i < n; i++)
    pt(i) = x[i];
  phY.setPoint(pt);
}

bool IpoptFibre::eval_h(Ipopt::Index n, const Ipopt::Number *x, bool new_x, Ipopt::Number obj_factor, Ipopt::Index m, const Ipopt::Number *lambda, bool new_lambda, Ipopt::Index nele_hess, Ipopt::Index *iRow, Ipopt::Index *jCol, Ipopt::Number *values)
{
  if (values == nullptr) {
    int i = 0;
    for(int a=0;a<n;a++)
    {
      for(int b=a;b<n;b++)
      {
        iRow[i]=a;
        jCol[i]=b;
        i++;
      }
    }
  } else {

    if (new_x)setPoint(n, x);
    auto hessian=phY.hessian();
    int i = 0;
    for(int a=0;a<n;a++)
    {
      for(int b=a;b<n;b++)
      {
        values[i]=obj_factor*hessian(a,b);
        i++;
      }
    }
  }

  return true;
}

bool IpoptFibre::eval_jac_g(Ipopt::Index n, const Ipopt::Number *x, bool new_x, Ipopt::Index m, Ipopt::Index nele_jac, Ipopt::Index *iRow, Ipopt::Index *jCol, Ipopt::Number *values)
{
  //No constraints
  return true;
}

bool IpoptFibre::eval_g(Ipopt::Index n, const Ipopt::Number *x, bool new_x, Ipopt::Index m, Ipopt::Number *g)
{
  if (new_x)setPoint(n, x);
  //No constraints
  return true;
}

bool IpoptFibre::eval_grad_f(Ipopt::Index n, const Ipopt::Number *x, bool new_x, Ipopt::Number *grad_f)
{
  if (new_x)setPoint(n, x);
  auto g = phY.gradient();
  for (int i = 0; i < n; i++)
    grad_f[i] = g[i];

  return true;
}

bool IpoptFibre::eval_f(Ipopt::Index n, const Ipopt::Number *x, bool new_x, Ipopt::Number &obj_value)
{
  if (new_x)setPoint(n, x);
  obj_value = phY.energy();
  return true;
}

bool IpoptFibre::get_starting_point(Ipopt::Index n, bool init_x, Ipopt::Number *x, bool init_z, Ipopt::Number *z_L, Ipopt::Number *z_U, Ipopt::Index m, bool init_lambda, Ipopt::Number *lambda)
{
  assert(init_z == false);//We do not provide an initial value for the bound multipliers z_L and z_U;
  assert(init_lambda == false);//We do not provide an initial value for the constraint multipliers lambda

  auto pt = phY.getPoint();
  for (Ipopt::Index i = 0; i < n; i++)
    x[i] = pt[i];

  return true;
}

bool IpoptFibre::get_bounds_info(Ipopt::Index n, Ipopt::Number *x_l, Ipopt::Number *x_u, Ipopt::Index m, Ipopt::Number *g_l, Ipopt::Number *g_u)
{
  //Bounds on the variables
  for (Ipopt::Index i = 0; i < n; i++) {
    x_l[i] = -std::numeric_limits<real>::infinity();
    x_u[i] = std::numeric_limits<real>::infinity();
  }

  //constraints
  for (Ipopt::Index i = 0; i < m; i++) {
    g_l[i] = 0.;
    g_u[i] = 0.;
  }

  return true;
}

bool IpoptFibre::get_nlp_info(Ipopt::Index &n, Ipopt::Index &m, Ipopt::Index &nnz_jac_g, Ipopt::Index &nnz_h_lag, Ipopt::TNLP::IndexStyleEnum &index_style)
{
  //Number of variables
  n = phY.vpointSize();
  //Number of constraints
  m = 0;

  //Non zero jacobian entries (constraints)
  nnz_jac_g = 0;
  //Non zero hessian entries
  //And as it is symmetric (upper triangle of the hessian)
  nnz_h_lag = (n*(n+1))/2;
  //C indexing (from 0)
  index_style = TNLP::C_STYLE;
  return true;
}

SuperClothoid IpoptFibre::getSolution()
{
  return outsc;
}

  
bool IpoptFibre::intermediate_callback(Ipopt::AlgorithmMode mode,
                                       Ipopt::Index iter, Ipopt::Number obj_value,
                                       Ipopt::Number inf_pr, Ipopt::Number inf_du,
                                       Ipopt::Number mu, Ipopt::Number d_norm,
                                       Ipopt::Number regularization_size,
                                       Ipopt::Number alpha_du, Ipopt::Number alpha_pr,
                                       Ipopt::Index ls_trials,
                                       const Ipopt::IpoptData* ip_data,
                                       Ipopt::IpoptCalculatedQuantities* ip_cq)
{
  Ipopt::TNLPAdapter* tnlp_adapter = NULL;
  if( ip_cq != NULL )
  {
    Ipopt::OrigIpoptNLP* orignlp;
    orignlp = dynamic_cast<Ipopt::OrigIpoptNLP*>(GetRawPtr(ip_cq->GetIpoptNLP()));
    if( orignlp != NULL )
      tnlp_adapter = dynamic_cast<Ipopt::TNLPAdapter*>(GetRawPtr(orignlp->nlp()));
    if(tnlp_adapter)
    {
      int n = phY.vpointSize();
      double* primals = new double[n];
      tnlp_adapter->ResortX(*ip_data->curr()->x(), primals);
      Eigen::VectorXr pt(n);
      for (Ipopt::Index i = 0; i < n; i++)
        pt(i) = primals[i];
      pts.push_back(pt);
    }
  }
  return true;
}

std::vector<Eigen::VectorXr> IpoptFibre::getLogs()
{
  return pts;
}
