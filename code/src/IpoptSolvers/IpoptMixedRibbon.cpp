/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "IpoptMixedRibbon.hpp"

template<> 
IpoptMixedRibbon<MixedPhY>::IpoptMixedRibbon(MixedPhY phY): phY(phY)
{
}

template<> 
IpoptMixedRibbon<MixedPhyBoxOri>::IpoptMixedRibbon(MixedPhY phY): phY(phY)
{
}

template<> 
IpoptMixedRibbon<MixedPhyRestrainOri>::IpoptMixedRibbon(MixedPhY phY, Filter filter): phY(phY, filter)
{
}

template<typename MPhyT> 
void IpoptMixedRibbon<MPhyT>::finalize_solution(Ipopt::SolverReturn status, Ipopt::Index n, const Ipopt::Number *x, const Ipopt::Number *z_L, const Ipopt::Number *z_U, Ipopt::Index m, const Ipopt::Number *g, const Ipopt::Number *lambda, Ipopt::Number obj_value, const Ipopt::IpoptData *ip_data, Ipopt::IpoptCalculatedQuantities *ip_cq)
{
  setPoint(n, x);
  msr = phY.get();
  this->lambda.resize(m);
  for(int i=0;i<m;i++)this->lambda[i]=lambda[i];
  Eigen::VectorXr pd = phY.getPressureDistances();
  this->lambda.resize(m);
  zU.resize(n);
  zL.resize(n);
  for(int i=0;i<n;i++){
    this->zU[i]=z_U[i];
    this->zL[i]=z_L[i];
  }
  int t=pd.size();
  pressureDistances.resize(t);
  for(int i=0;i<t;i++)
    pressureDistances[i]=pd[i];
  auto rf = phY.getRepulsionEnergyPerField();
  t=rf.size();
  repulsionForces.resize(t);
  for(int i=0;i<t;i++)
    repulsionForces[i]=rf[i];
}

template<typename MPhyT> 
void IpoptMixedRibbon<MPhyT>::setPoint(Ipopt::Index n, const Ipopt::Number *x)
{
  Eigen::VectorXd pt(n);
  for (Ipopt::Index i = 0; i < n; i++)
    pt(i) = x[i];
  phY.setPoint(pt);
  //std::cout<<"Setting point"<<std::endl;
  /*std::cout<<"Nb pts:"<<phY.get().getNbSegments()<<std::endl;
  for(int i=0;i<phY.get().getNbSegments();i++)
  {
    std::cout<<i<<", w="<<phY.get().getOmega1(i)<<", n="<<phY.get().getEta(i)<<std::endl;
    std::cout<<"  pos"<<phY.get().getPosStart(0).transpose()<<std::endl;
  }*/
  //std::cout<<pt.transpose()<<std::endl;
}

template<typename MPhyT> 
bool IpoptMixedRibbon<MPhyT>::eval_jac_g(Ipopt::Index n, const Ipopt::Number *x, bool new_x, Ipopt::Index m, Ipopt::Index nele_jac, Ipopt::Index *iRow, Ipopt::Index *jCol, Ipopt::Number *values)
{
  if (values == nullptr) {
    int i = 0;
    for (auto p : constraintsjacStruct) {
      iRow[i] = p.first.first;
      jCol[i] = p.first.second;
      i++;
    }
  } else {

    if (new_x)setPoint(n, x);
    std::map<Point, double> liste;
    for (auto p : constraintsjacStruct) liste[p.first] = 0.;
    phY.fillConstraintsJac(liste);
    int i = 0;
    for (auto p : liste) {
      values[i++] = p.second;
    }
  }

  return true;
}

template<typename MPhyT> 
bool IpoptMixedRibbon<MPhyT>::eval_g(Ipopt::Index n, const Ipopt::Number *x, bool new_x, Ipopt::Index m, Ipopt::Number *g)
{
  if (new_x)setPoint(n, x);
  auto ctr = phY.constraints();
  for (int i = 0; i < m; i++)
    g[i] = ctr[i];
  return true;
}

template<typename MPhyT> 
bool IpoptMixedRibbon<MPhyT>::eval_grad_f(Ipopt::Index n, const Ipopt::Number *x, bool new_x, Ipopt::Number *grad_f)
{
  if (new_x)setPoint(n, x);
  auto gg = phY.getGravityEnergyGradient();
  auto ge = phY.getElasticEnergyGradient();
  auto gp = phY.getPressureEnergyGradient();
  auto gf = phY.getRepulsionEnergyGradient();
  assert(gg.size() == ge.size());
  assert(gg.size() == gp.size());
  assert(gg.size() == gf.size());
  auto g = gg + ge + gp + gf;
  for (int i = 0; i < n; i++)
    grad_f[i] = g[i];

  return true;
}

template<typename MPhyT> 
bool IpoptMixedRibbon<MPhyT>::eval_f(Ipopt::Index n, const Ipopt::Number *x, bool new_x, Ipopt::Number &obj_value)
{
  if (new_x)setPoint(n, x);
  double vg = phY.getGravityEnergy();
  double ve = phY.getElasticEnergy();
  double vp = phY.getPressureEnergy();
  double vf = phY.getRepulsionEnergy();
  obj_value = vg + ve + vp +vf;
  return true;
}

template<typename MPhyT> 
bool IpoptMixedRibbon<MPhyT>::eval_h(Ipopt::Index n, const Ipopt::Number *x, bool new_x, Ipopt::Number obj_factor, Ipopt::Index m, const Ipopt::Number *lambda, bool new_lambda, Ipopt::Index nele_hess, Ipopt::Index *iRow, Ipopt::Index *jCol, Ipopt::Number *values)
{
  if (values == nullptr) {
    int i = 0;
    for (auto p : constraintshessianStruct) {
      iRow[i] = p.first.first;
      jCol[i] = p.first.second;
      i++;
    }
  } else {

    if (new_x)setPoint(n, x);
    std::map<Point, double> liste;
    for (auto p : constraintshessianStruct) liste[p.first] = 0.;
    phY.fillElasticHessian(liste, obj_factor);
    phY.fillGravityHessian(liste, obj_factor);
    phY.fillPressureHessian(liste, obj_factor);
    phY.fillRepulsionHessian(liste, obj_factor);
    Eigen::VectorXr lambdaE(m);
    for (Ipopt::Index i = 0; i < m; i++)
      lambdaE[i]=lambda[i];
    phY.fillConstraintsHessian( liste, lambdaE);
    int i = 0;
    for (auto p : liste) {
      if(p.first.first<=p.first.second)
        values[i++] = p.second;
      else
        values[i++] = 0.;
    }
  }

  return true;
}


template<typename MPhyT> 
bool IpoptMixedRibbon<MPhyT>::get_starting_point(Ipopt::Index n, bool init_x, Ipopt::Number *x, bool init_z, Ipopt::Number *z_L, Ipopt::Number *z_U, Ipopt::Index m, bool init_lambda, Ipopt::Number *lambda)
{
  if(init_z && zU.size()!=n){//We do not provide an initial value for the bound multipliers z_L and z_U;
    std::string reason;
    if(zU.size()==0)reason="no previous z";
    else reason="Size mismatch";
    std::cout<< "Warning, z cannot be initialized ("<<reason<<")"<<std::endl;
    return false;
  }
  else if(init_z){
    for (Ipopt::Index i = 0; i < n; i++){
      z_U[i]=zU[i];
      z_L[i]=zL[i];
    }
  }
  if(init_lambda && this->lambda.size()!=m){//We do not provide an initial value for the bound multipliers z_L and z_U;
    std::cout<< "Warning, lambda cannot be initialized"<<std::endl;
    return false;
  }
  else if(init_lambda){
    for (Ipopt::Index i = 0; i < m; i++){
      lambda[i]=this->lambda[i];
    }
  }

  //
  /*std::cout<<"Starting point"<<std::endl;
  for(int i=0;i<phY.get().getNbSegments();i++)
  {
    std::cout<<i<<", w="<<phY.get().getOmega1(i)<<", n="<<phY.get().getEta(i)<<std::endl;
    std::cout<<"  pos"<<phY.get().getPosStart(0).transpose()<<std::endl;
  }*/
  
  Eigen::VectorXd pt = phY.getPoint();
  assert(pt.size()==n);
  for (Ipopt::Index i = 0; i < n; i++)
    x[i] = pt[i];
  setPoint(n,x);
  return true;
}

template<typename MPhyT> 
bool IpoptMixedRibbon<MPhyT>::get_bounds_info(Ipopt::Index n, Ipopt::Number *x_l, Ipopt::Number *x_u, Ipopt::Index m, Ipopt::Number *g_l, Ipopt::Number *g_u)
{
  //Bounds on the variables
  auto lb = phY.getLowerBound();
  auto ub = phY.getUpperBound();
  assert(lb.size() == n);
  assert(ub.size() == n);
  for (Ipopt::Index i = 0; i < n; i++) {
    x_l[i] = lb[i];
    x_u[i] = ub[i];
  }

  //constraints
  auto clb = phY.getConstraintsLowerBound();
  auto cub = phY.getConstraintsUpperBound();
  for (Ipopt::Index i = 0; i < m; i++) {
    g_l[i] = clb[i];
    g_u[i] = cub[i];
  }

  return true;
}

template<typename MPhyT> 
bool IpoptMixedRibbon<MPhyT>::get_nlp_info(Ipopt::Index &n, Ipopt::Index &m, Ipopt::Index &nnz_jac_g, Ipopt::Index &nnz_h_lag, Ipopt::TNLP::IndexStyleEnum &index_style)
{
  //Number of variables
  n = phY.getNbVars();
  //Number of constraints
  m = phY.getNbConstraints();

  //Non zero jacobian entries
  constraintsjacStruct.clear();
  phY.fillConstraintsJacStruct(constraintsjacStruct);
  nnz_jac_g = constraintsjacStruct.size();
  //Non zero hessian entries
  //And as it is symmetric
  constraintshessianStruct.clear();
  phY.fillElasticHessianStruct(constraintshessianStruct);
  phY.fillGravityHessianStruct(constraintshessianStruct);
  phY.fillPressureHessianStruct(constraintshessianStruct);
  phY.fillRepulsionHessianStruct(constraintshessianStruct);
  phY.fillConstraintsHessianStruct(constraintshessianStruct);
  nnz_h_lag = constraintshessianStruct.size();
  //C indexing (from 0)
  index_style = TNLP::C_STYLE;
  return true;
}

template<typename MPhyT> 
MixedSuperRibbon IpoptMixedRibbon<MPhyT>::getSolution()
{
  return msr;
}

template<typename MPhyT> std::vector<double> IpoptMixedRibbon<MPhyT>::getLambda()
{
  return lambda;
}

template<typename MPhyT> std::vector<double> IpoptMixedRibbon<MPhyT>::getWSzL()
{
  return zL;
}

template<typename MPhyT> std::vector<double> IpoptMixedRibbon<MPhyT>::getWSzU()
{
  return zU;
}

template<typename MPhyT> void IpoptMixedRibbon<MPhyT>::setupWS(std::vector<double> lambda, std::vector<double> zU, std::vector<double> zL)
{
  this->lambda=lambda;
  this->zU=zU;
  this->zL=zL;
}

template<typename MPhyT> std::vector<double> IpoptMixedRibbon<MPhyT>::getPressureDistances()
{
  return pressureDistances;
}

template<typename MPhyT> std::vector<double> IpoptMixedRibbon<MPhyT>::getRepulsionEnergyPerField()
{
  return repulsionForces;
}

template class IpoptMixedRibbon<MixedPhY>;
template class IpoptMixedRibbon<MixedPhyBoxOri>;
