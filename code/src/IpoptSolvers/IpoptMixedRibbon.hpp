/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef IPOPTMIXEDRIBBON_H
#define IPOPTMIXEDRIBBON_H

#include <IpTNLP.hpp>
#include <MixedSRCore/MixedPhY.hpp>
#include <MixedSRCore/MixedPhyBoxOri.hpp>
#include <MixedSRCore/MixedPhyRestrainOri.hpp>

class IpoptMixedRibbonVirtualBase : public Ipopt::TNLP
{
public:
  virtual MixedSuperRibbon getSolution()=0; 
  virtual std::vector<double> getLambda()=0;
  virtual std::vector<double> getWSzU()=0;
  virtual std::vector<double> getWSzL()=0;
  virtual std::vector<double> getPressureDistances()=0;
  virtual std::vector<double> getRepulsionEnergyPerField()=0;
  virtual void setupWS(std::vector<double> lambda, std::vector<double> zU, std::vector<double> zL)=0;
};

template<typename MPhyT>
class IpoptMixedRibbon : public IpoptMixedRibbonVirtualBase//Ipopt::TNLP
{
public:
  IpoptMixedRibbon(MixedPhY phY);
  IpoptMixedRibbon(MixedPhY phY, Filter filter);

  virtual void finalize_solution(Ipopt::SolverReturn status, Ipopt::Index n, const Ipopt::Number *x, const Ipopt::Number *z_L, const Ipopt::Number *z_U, Ipopt::Index m, const Ipopt::Number *g, const Ipopt::Number *lambda, Ipopt::Number obj_value, const Ipopt::IpoptData *ip_data, Ipopt::IpoptCalculatedQuantities *ip_cq) override;

  virtual bool eval_jac_g(Ipopt::Index n, const Ipopt::Number *x, bool new_x, Ipopt::Index m, Ipopt::Index nele_jac, Ipopt::Index *iRow, Ipopt::Index *jCol, Ipopt::Number *values) override;

  virtual bool eval_g(Ipopt::Index n, const Ipopt::Number *x, bool new_x, Ipopt::Index m, Ipopt::Number *g) override;

  virtual bool eval_grad_f(Ipopt::Index n, const Ipopt::Number *x, bool new_x, Ipopt::Number *grad_f) override;

  virtual bool eval_f(Ipopt::Index n, const Ipopt::Number *x, bool new_x, Ipopt::Number &obj_value) override;

  virtual bool eval_h(Ipopt::Index n, const Ipopt::Number *x, bool new_x,
                      Ipopt::Number obj_factor, Ipopt::Index m, const Ipopt::Number *lambda,
                      bool new_lambda, Ipopt::Index nele_hess,
                      Ipopt::Index *iRow, Ipopt::Index *jCol, Ipopt::Number *values) override;

  virtual bool get_starting_point(Ipopt::Index n, bool init_x, Ipopt::Number *x, bool init_z, Ipopt::Number *z_L, Ipopt::Number *z_U, Ipopt::Index m, bool init_lambda, Ipopt::Number *lambda) override;

  virtual bool get_bounds_info(Ipopt::Index n, Ipopt::Number *x_l, Ipopt::Number *x_u, Ipopt::Index m, Ipopt::Number *g_l, Ipopt::Number *g_u) override;

  virtual bool get_nlp_info(Ipopt::Index &n, Ipopt::Index &m, Ipopt::Index &nnz_jac_g, Ipopt::Index &nnz_h_lag, Ipopt::TNLP::IndexStyleEnum &index_style) override;

  MixedSuperRibbon getSolution() override;
  
  std::vector<double> getLambda() override;
  std::vector<double> getWSzU() override;
  std::vector<double> getWSzL() override;
  std::vector<double> getPressureDistances() override;
  std::vector<double> getRepulsionEnergyPerField() override;
  void setupWS(std::vector<double> lambda, std::vector<double> zU, std::vector<double> zL) override;
  
private:
  void setPoint(Ipopt::Index n, const Ipopt::Number *x);
  using Point = std::pair<int, int>;
  MPhyT phY;
  std::map<Point, int> constraintsjacStruct;
  std::map<Point, int> constraintshessianStruct;
  MixedSuperRibbon msr;
  std::vector<double> lambda;
  std::vector<double> zU,zL;
  std::vector<double> pressureDistances;
  std::vector<double> repulsionForces;
};

#endif // IPOPTMIXEDRIBBON_H
