/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "IpoptSuperRibbon.hpp"
//See https://www.coin-or.org/Ipopt/documentation/node23.html

#include <IpIpoptCalculatedQuantities.hpp>
#include <IpIpoptData.hpp>
#include <IpTNLPAdapter.hpp>
#include <IpOrigIpoptNLP.hpp>
#include <fstream>

IpoptSuperRibbon::IpoptSuperRibbon(RibbonSystem phY, bool desactivateBounds)
  : phY(phY), desactivateBounds(desactivateBounds)
{
}

IpoptSuperRibbon::IpoptSuperRibbon(RibbonSystem phY, IpoptSuperRibbonFixEnd fixEnd, bool desactivateBounds)
  : phY(phY), fixEnd(fixEnd), desactivateBounds(desactivateBounds)
{
}


void IpoptSuperRibbon::finalize_solution(Ipopt::SolverReturn status, Ipopt::Index n, const Ipopt::Number *x, const Ipopt::Number *z_L, const Ipopt::Number *z_U, Ipopt::Index m, const Ipopt::Number *g, const Ipopt::Number *lambda, Ipopt::Number obj_value, const Ipopt::IpoptData *ip_data, Ipopt::IpoptCalculatedQuantities *ip_cq)
{
  setPoint(n, x);
  outsr=phY.get();
  this->lambda.resize(m);
  for(int i=0;i<m;i++)this->lambda[i]=lambda[i];
}

void IpoptSuperRibbon::setPoint(Ipopt::Index n, const Ipopt::Number *x)
{
  Eigen::VectorXr pt(n);
  for (Ipopt::Index i = 0; i < n; i++)
    pt(i) = x[i];
  phY.setPoint(pt);
}

bool IpoptSuperRibbon::eval_h(Ipopt::Index n, const Ipopt::Number *x, bool new_x, Ipopt::Number obj_factor, Ipopt::Index m, const Ipopt::Number *lambda, bool new_lambda, Ipopt::Index nele_hess, Ipopt::Index *iRow, Ipopt::Index *jCol, Ipopt::Number *values)
{
  if (values == nullptr) {
    int i = 0;
    for(int a=0;a<n;a++)
    {
      for(int b=a;b<n;b++)
      {
        iRow[i]=a;
        jCol[i]=b;
        i++;
      }
    }
  } else {

    if (new_x)setPoint(n, x);
    Eigen::MatrixXr hessian=obj_factor*phY.hessian();
    if(fixEnd.has_value())
    {
      Eigen::Matrix<Eigen::MatrixXr,3,1> hep = phY.hessEndPos();
      Eigen::Matrix<Eigen::MatrixXr,3,3> hef = phY.hessEndFrame();
      for(int cd=0;cd<3;cd++)
          hessian+=lambda[cd]*hep[cd];
      if(fixEnd.value().constraintFrameDir)
      {
        for(int cd=0;cd<3;cd++)
        {
            hessian+=lambda[3+cd]*(fixEnd.value().frame(0,(cd+1)%3)*hef(0,cd)
                                  +fixEnd.value().frame(1,(cd+1)%3)*hef(1,cd)
                                  +fixEnd.value().frame(2,(cd+1)%3)*hef(2,cd)
            );
        }
      }
      else
      {
      for(int cd=0;cd<9;cd++)
          hessian+=lambda[3+cd]*hef(cd/3,cd%3);
      }
    }
    int i = 0;
    for(int a=0;a<n;a++)
    {
      for(int b=a;b<n;b++)
      {
        values[i]=hessian(a,b);
        i++;
      }
    }
  }

  return true;
}

bool IpoptSuperRibbon::eval_jac_g(Ipopt::Index n, const Ipopt::Number *x, bool new_x, Ipopt::Index m, Ipopt::Index nele_jac, Ipopt::Index *iRow, Ipopt::Index *jCol, Ipopt::Number *values)
{
  if(!fixEnd.has_value())return true;
  //No constraints
  if (values == nullptr) {
    int i = 0;
    for(int cd=0;cd<(fixEnd.value().constraintFrameDir?6:12);cd++)
    {
      for(int b=0;b<n;b++)
      {
        iRow[i]=cd;
        jCol[i]=b;
        i++;
      }
    }
  } else {
    if (new_x)setPoint(n, x);
    Eigen::Matrix<Eigen::VectorXr,3,1> gep = phY.gradEndPos();
    Eigen::Matrix<Eigen::VectorXr,3,3> gef = phY.gradEndFrame();
    int i = 0;
    for(int cd=0;cd<3;cd++)
    {
      for(int b=0;b<n;b++)
      {
        values[i]=gep[cd][b];
        i++;
      }
    }
    if(fixEnd.value().constraintFrameDir)
    {
      for(int cd=0;cd<3;cd++)
      {
        for(int b=0;b<n;b++)
        {
          values[i]=(fixEnd.value().frame(0,(cd+1)%3)*gef(0,cd)[b]
                    +fixEnd.value().frame(1,(cd+1)%3)*gef(1,cd)[b]
                    +fixEnd.value().frame(2,(cd+1)%3)*gef(2,cd)[b]
            );
          i++;
        }
      }
    }
    else
    {
      for(int cd=0;cd<9;cd++)
      {
        for(int b=0;b<n;b++)
        {
          values[i]=gef(cd/3,cd%3)[b];
          i++;
        }
      }
    }
  }
  return true;
}

bool IpoptSuperRibbon::eval_g(Ipopt::Index n, const Ipopt::Number *x, bool new_x, Ipopt::Index m, Ipopt::Number *g)
{
  if (new_x)setPoint(n, x);
  Eigen::Vector3r p=phY.getEndPos();
  Eigen::Matrix3r f=phY.getEndFrame();
  if(fixEnd.has_value())
  {
    for (Ipopt::Index i = 0; i < 3; i++) {
      g[i] = p[i];
    }
    if(fixEnd.value().constraintFrameDir)
    {
      for (Ipopt::Index i = 0; i < 3; i++) {
        Eigen::Vector3d vEnd=f.col(i), vFix=fixEnd.value().frame.col((i+1)%3);
        g[i+3] = vEnd.dot(vFix);
      }
    }
    else
    {
      for (Ipopt::Index i = 0; i < 9; i++) {
        g[i+3] = f(i/3,i%3);
      }
    }
  }
  return true;
}

bool IpoptSuperRibbon::eval_grad_f(Ipopt::Index n, const Ipopt::Number *x, bool new_x, Ipopt::Number *grad_f)
{
  if (new_x)setPoint(n, x);
  auto g = phY.gradient();
  for (int i = 0; i < n; i++)
    grad_f[i] = g[i];

  return true;
}

bool IpoptSuperRibbon::eval_f(Ipopt::Index n, const Ipopt::Number *x, bool new_x, Ipopt::Number &obj_value)
{
  if (new_x)setPoint(n, x);
  obj_value = phY.energy();
  return true;
}

bool IpoptSuperRibbon::get_starting_point(Ipopt::Index n, bool init_x, Ipopt::Number *x, bool init_z, Ipopt::Number *z_L, Ipopt::Number *z_U, Ipopt::Index m, bool init_lambda, Ipopt::Number *lambda)
{
  assert(init_z == false);//We do not provide an initial value for the bound multipliers z_L and z_U;
  assert(init_lambda == false);//We do not provide an initial value for the constraint multipliers lambda

  auto pt = phY.getPoint();
  for (Ipopt::Index i = 0; i < n; i++)
    x[i] = pt[i];

  return true;
}

bool IpoptSuperRibbon::get_bounds_info(Ipopt::Index n, Ipopt::Number *x_l, Ipopt::Number *x_u, Ipopt::Index m, Ipopt::Number *g_l, Ipopt::Number *g_u)
{
  //Bounds on the variables
  auto lb = phY.getLowerBound();
  auto ub = phY.getUpperBound();
  assert(lb.size() == n);
  assert(ub.size() == n);
  if(desactivateBounds)
  {
    lb.setConstant(-std::numeric_limits<real>::infinity());
    ub.setConstant( std::numeric_limits<real>::infinity());
  }
  for (Ipopt::Index i = 0; i < n; i++) {
    x_l[i] = lb[i];
    x_u[i] = ub[i];
  }

  //constraints
  if(fixEnd.has_value())
  {
    for (Ipopt::Index i = 0; i < 3; i++) {
      g_l[i] = fixEnd->pos[i];
      g_u[i] = fixEnd->pos[i];
    }
    if(fixEnd.value().constraintFrameDir) {
      for (Ipopt::Index i = 0; i < 3; i++) {
        g_l[i+3] = 0.;
        g_u[i+3] = 0.;
      }
    }
    else
    {
      for (Ipopt::Index i = 0; i < 9; i++) {
        g_l[i+3] = fixEnd->frame(i/3,i%3);
        g_u[i+3] = fixEnd->frame(i/3,i%3);
      }
    }
  }
  return true;
}

bool IpoptSuperRibbon::get_nlp_info(Ipopt::Index &n, Ipopt::Index &m, Ipopt::Index &nnz_jac_g, Ipopt::Index &nnz_h_lag, Ipopt::TNLP::IndexStyleEnum &index_style)
{
  //Number of variables
  n = phY.vpointSize();
  //Number of constraints
  m = fixEnd.has_value()?(fixEnd.value().constraintFrameDir?6:12):0;

  //Non zero jacobian entries (constraints)
  nnz_jac_g = fixEnd.has_value()?m*n:0;
  //Non zero hessian entries
  //And as it is symmetric (upper triangle of the hessian)
  nnz_h_lag = (n*(n+1))/2;
  //C indexing (from 0)
  index_style = TNLP::C_STYLE;
  return true;
}

SuperRibbon IpoptSuperRibbon::getSolution()
{
  return outsr;
}

std::vector<double> IpoptSuperRibbon::getLambda()
{
  return lambda;
}



  
bool IpoptSuperRibbon::intermediate_callback(Ipopt::AlgorithmMode mode,
                                       Ipopt::Index iter, Ipopt::Number obj_value,
                                       Ipopt::Number inf_pr, Ipopt::Number inf_du,
                                       Ipopt::Number mu, Ipopt::Number d_norm,
                                       Ipopt::Number regularization_size,
                                       Ipopt::Number alpha_du, Ipopt::Number alpha_pr,
                                       Ipopt::Index ls_trials,
                                       const Ipopt::IpoptData* ip_data,
                                       Ipopt::IpoptCalculatedQuantities* ip_cq)
{
  Ipopt::TNLPAdapter* tnlp_adapter = NULL;
  if( ip_cq != NULL )
  {
    Ipopt::OrigIpoptNLP* orignlp;
    orignlp = dynamic_cast<Ipopt::OrigIpoptNLP*>(GetRawPtr(ip_cq->GetIpoptNLP()));
    if( orignlp != NULL )
      tnlp_adapter = dynamic_cast<Ipopt::TNLPAdapter*>(GetRawPtr(orignlp->nlp()));
    if(tnlp_adapter)
    {
      int n = phY.vpointSize();
      double* primals = new double[n];
      tnlp_adapter->ResortX(*ip_data->curr()->x(), primals);
      Eigen::VectorXr pt(n);
      for (Ipopt::Index i = 0; i < n; i++)
        pt(i) = primals[i];
      pts.push_back(pt);
//       RibbonSystem rs(Eigen::Vector3r(0.,0.,0.));
//       rs.set(phY.get());
//       rs.setPoint(pt);
//       std::ofstream os("ShiftYouHaveToDo.log",std::ios_base::app);
//       os<<rs.getPoint().transpose()<<std::endl;
    }
  }
  return true;
}

std::vector<Eigen::VectorXr> IpoptSuperRibbon::getLogs()
{
  return pts;
}
