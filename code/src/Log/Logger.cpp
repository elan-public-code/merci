/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "Logger.hpp"

std::shared_ptr<Logger> Logger::myInstance(new Logger());

std::shared_ptr<Logger> Logger::Instance()
{
  if(!myInstance) myInstance.reset(new Logger());
  return myInstance;
}

std::shared_ptr<Logger> getLogger()
{
  return Logger::Instance();
}

Logger::Logger()
{
  writer = new CmdLoggerWriter();
}

Logger::~Logger()
{
  if(writer)
    delete writer;
}

void Logger::switchToBlackHole()
{
  if(writer)
    delete writer;
  writer=nullptr;
}

void Logger::switchToConsole()
{
  if(writer)
    delete writer;
  writer = new CmdLoggerWriter();
}

void Logger::switchToFile(std::string filename)
{
  if(writer)
    delete writer;
  writer = new FileLoggerWriter(filename);
}

void Logger::switchToJsonFile(std::string filename)
{
  if(writer)
    delete writer;
  writer = new JSonLoggerWriter(filename);
}

void Logger::switchToFileAndConsole(std::string filename)
{
  if(writer)
    delete writer;
  writer = new LoggerDoubleInterface(new CmdLoggerWriter(), new FileLoggerWriter(filename));
}

void Logger::switchToJsonFileAndConsole(std::string filename)
{
  if(writer)
    delete writer;
  writer = new LoggerDoubleInterface(new CmdLoggerWriter(), new JSonLoggerWriter(filename));
}

void Logger::Write(LoggerLevel level, std::string moduleName, LoggerMsg msg)
{
  if(writer)
    writer->Write(level, moduleName, msg);
}
