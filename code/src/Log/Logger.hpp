/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef LOGGER_H
#define LOGGER_H

#include "LoggerMsg.hpp"
#include "LoggerWriter.hpp"
#include <memory>

class Logger
{
public:
  static std::shared_ptr<Logger> Instance();
  ~Logger();
  void Write(LoggerLevel level, std::string moduleName, LoggerMsg msg);
  void switchToConsole();
  void switchToFile(std::string filename);
  void switchToJsonFile(std::string filename);
  void switchToJsonFileAndConsole(std::string filename);
  void switchToFileAndConsole(std::string filename);
  void switchToBlackHole();
private:
  Logger();
  static std::shared_ptr<Logger> myInstance;
  LoggerWriter* writer;
};

std::shared_ptr<Logger> getLogger();

#endif // LOGGER_H
