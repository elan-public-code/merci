/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "LoggerLevel.hpp"

std::string toString(LoggerLevel ll)
{
  switch(ll)
  {
    case LoggerLevel::INFORMATION:
      return "Information";
    case LoggerLevel::LOWWARNING:
      return "Low warning";
    case LoggerLevel::WARNING:
      return "Warning";
    case LoggerLevel::HIGHWARNING:
      return "High Warning";
    case LoggerLevel::ERROR:
      return "Error";
    case LoggerLevel::CRITICAL:
      return "Critical";
    default:
      throw "Not implemented";
  }
};
