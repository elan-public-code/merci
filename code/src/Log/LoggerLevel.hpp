/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef LOGGERLEVEL_H
#define LOGGERLEVEL_H

#include <string>

enum class LoggerLevel
{
  INFORMATION = 0, 
  LOWWARNING = 1, 
  WARNING = 2, 
  HIGHWARNING = 3, 
  ERROR = 4, 
  CRITICAL = 5
};

std::string toString(LoggerLevel ll);

#endif
