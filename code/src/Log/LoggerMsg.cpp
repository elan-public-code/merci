/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "LoggerMsg.hpp"

LoggerMsg::LoggerMsg(std::string msg)
  : message(msg)
{
}

std::string LoggerMsg::getMessage() const
{
  return message;
}

std::shared_ptr<LoggerMsg> LoggerMsg::addChild(std::string fieldname, std::string msg)
{
  std::shared_ptr<LoggerMsg> ch(new LoggerMsg(msg));
  childs.push_back(std::make_pair(fieldname, ch));
  return ch;
}

LoggerMsg::ChildTypeIterator LoggerMsg::begin()
{
  return childs.begin();
}

LoggerMsg::ChildTypeIterator LoggerMsg::end()
{
  return childs.end();
}
