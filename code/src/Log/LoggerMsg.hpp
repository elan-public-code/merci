/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef LOGGERMSG_H
#define LOGGERMSG_H

#include <string>
#include <utility>
#include <memory>
#include <vector>
#include <sstream>

class LoggerMsg{
public:
  using ChildType = std::vector<std::pair<std::string, std::shared_ptr<LoggerMsg> > >;
  using ChildTypeIterator = ChildType::iterator;
  LoggerMsg(std::string message);
  ChildTypeIterator begin();
  ChildTypeIterator end();
  std::string getMessage() const;
  std::shared_ptr<LoggerMsg> addChild(std::string fieldname, std::string msg);
  template<typename T> std::shared_ptr<LoggerMsg> addChild(std::string fieldname, T msg){std::stringstream ss;ss<<msg; return addChild(fieldname,ss.str());}
private:
  std::string message;
  ChildType childs;
};

#endif // LOGGERMSG_H
