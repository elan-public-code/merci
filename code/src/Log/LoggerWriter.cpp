/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "LoggerWriter.hpp"
#include <iostream>
#include <iomanip>
#include <fstream>

LoggerWriter::~LoggerWriter()
{
}

OStreamWriter::OStreamWriter(std::ostream &os): os(os)
{
}

void OStreamWriter::Write(LoggerLevel level, std::string moduleName, LoggerMsg msg)
{
  os  << std::left << std::setw(20) << std::setfill(' ') << moduleName << '|' << std::setw(12) << toString(level) << '|' << msg.getMessage() << std::endl;
  Write(1, msg);
}

void OStreamWriter::Write(int indentLevel, LoggerMsg msg)
{
  for (auto sm : msg) {
    os  << std::left << std::setw(35 + 2 * indentLevel) << std::setfill(' ') << " " << "-> " << std::setw(20) << sm.first << ": " << sm.second->getMessage() << std::endl;
    Write(indentLevel + 1, *sm.second);
  }
}

JSonLoggerWriter::JSonLoggerWriter(std::string filename) : filename(filename)
{
}

JSonLoggerWriter::~JSonLoggerWriter()
{
  std::ofstream os(filename);
  os<<data;
}

nlohmann::json JSonLoggerWriter::toJson(LoggerMsg msg)
{
  nlohmann::json res;
  res["Descr"] = msg.getMessage();
  for(auto sm : msg)
    res[sm.first] = toJson(*sm.second);
  return res;
}

void JSonLoggerWriter::Write(LoggerLevel level, std::string moduleName, LoggerMsg msg)
{
  nlohmann::json j;
  j["module"] = moduleName;
  j["level"] = toString(level);
  j["details"] = toJson(msg);
  data.push_back(j);
}

CmdLoggerWriter::CmdLoggerWriter(): OStreamWriter(std::cout)
{
}

FileLoggerWriter::FileLoggerWriter(std::string filename): os(filename), writer(os)
{
}

void FileLoggerWriter::Write(LoggerLevel level, std::string moduleName, LoggerMsg msg)
{
  writer.Write(level, moduleName, msg);
}

LoggerDoubleInterface::LoggerDoubleInterface(LoggerWriter* theOne, LoggerWriter* theOther)
  : theOne(theOne),
  theOther(theOther)
{
}

LoggerDoubleInterface::~LoggerDoubleInterface()
{
  delete theOne;
  delete theOther;
}

void LoggerDoubleInterface::Write(LoggerLevel level, std::string moduleName, LoggerMsg msg)
{
  theOne->Write(level,moduleName,msg);
  theOther->Write(level,moduleName,msg);
}
