/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef LOGGERWRITER_H
#define LOGGERWRITER_H

#include "LoggerMsg.hpp"
#include "LoggerLevel.hpp"
#include "json.hpp"
#include <fstream>

class LoggerWriter
{
public:
  virtual ~LoggerWriter();
  virtual void Write(LoggerLevel level, std::string moduleName, LoggerMsg msg) = 0;
};

class OStreamWriter : public LoggerWriter
{
public:
  OStreamWriter(std::ostream& os);
  virtual void Write(LoggerLevel level, std::string moduleName, LoggerMsg msg);
private:
  void Write(int indentLevel, LoggerMsg msg);
  std::ostream& os;
};

class JSonLoggerWriter : public LoggerWriter
{
public:
  JSonLoggerWriter(std::string filename);
  ~JSonLoggerWriter();
  virtual void Write(LoggerLevel level, std::string moduleName, LoggerMsg msg);
private:
  nlohmann::json toJson(LoggerMsg msg);
  std::string filename;
  nlohmann::json data;
};

class CmdLoggerWriter : public OStreamWriter
{
public:
  CmdLoggerWriter();
};

class FileLoggerWriter : public LoggerWriter
{
public:
  FileLoggerWriter(std::string filename);
  virtual void Write(LoggerLevel level, std::string moduleName, LoggerMsg msg);
private:
  std::ofstream os;
  OStreamWriter writer;
};

class LoggerDoubleInterface : public LoggerWriter
{
  public:
  LoggerDoubleInterface(LoggerWriter* theOne, LoggerWriter* theOther); //destruction by the class
  virtual void Write(LoggerLevel level, std::string moduleName, LoggerMsg msg);
  ~LoggerDoubleInterface();
private:
  LoggerWriter* theOne;
  LoggerWriter* theOther;
};

#endif // LOGGERWRITER_H
