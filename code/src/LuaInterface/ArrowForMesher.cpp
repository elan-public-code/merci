/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "ArrowForMesher.hpp"

void ArrowForMesher::fastDirSetup(double l, double lh, double d, double dh)
{
  length=l;
  lengthHead=lh;
  diameter=d;
  diameterHead=dh;
}

void initArrowForMesherClass(sol::state &lua)
{
  lua.new_usertype<ArrowForMesher>(
    "Arrow",          sol::constructors<ArrowForMesher()>(),
    "name",           &ArrowForMesher::name,
    "position",       &ArrowForMesher::ori,
    "direction",      &ArrowForMesher::dir,
    "length",         &ArrowForMesher::length,
    "lengthHead",     &ArrowForMesher::lengthHead,
    "diameter",       &ArrowForMesher::diameter,
    "diameterHead",   &ArrowForMesher::diameterHead,
    "setupDimensions",&ArrowForMesher::fastDirSetup,
    "resolution",     &ArrowForMesher::resolution,
    "color",          &ArrowForMesher::color
    );
}
