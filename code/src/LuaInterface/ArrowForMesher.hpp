/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef ARROWFORMESHER_H
#define ARROWFORMESHER_H

#include <string>
#include <array>
#include "sol.hpp"

class ArrowForMesher
{
public:
  std::string name;
  std::array<double,3>  ori={0,0,0};
  std::array<double,3> dir={0,0,0};
  std::array<int,3> color={0,0,0};
  double length = 1.;
  double lengthHead = 0.1;
  double diameter = 0.05;
  double diameterHead = 0.1;
  void fastDirSetup(double l, double lh, double d, double dh);
  int resolution=10;
};

void initArrowForMesherClass(sol::state &lua);

#endif // ARROWFORMESHER_H
