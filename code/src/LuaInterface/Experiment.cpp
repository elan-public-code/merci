/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "Experiment.hpp"
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

#include <SRStaticProblem.hpp>
#include <ElasticEnergy.hpp>



double Experiment::energy()
{
  RibbonSystem phY(Eigen::Vector3r(Gravity[0],Gravity[1],Gravity[2]));
  phY.set(ribbon.getSR());
  return phY.energy();
}

double Experiment::gradientNorm()
{
  RibbonSystem phY(Eigen::Vector3r(Gravity[0],Gravity[1],Gravity[2]));
  phY.set(ribbon.getSR());
  return phY.gradient().norm();
}

void Experiment::attachEnd(std::array<double, 3> posEnd, std::array<double, 9> frameEnd)
{
  fixEnd=true;
  this->posEnd=posEnd;
  this->frameEnd=frameEnd;
}

void Experiment::detachEnd()
{
  fixEnd=false;
}

void Experiment::use3dotAlignementConstraint(bool val)
{
  fixEndShortDOF=val;
}


std::vector<std::vector<double>> Experiment::getLogs()
{
  std::vector<std::vector<double>> res;
  for(Eigen::VectorXr pt:logs)
  {
    std::vector<double> v;
    for(int i=0;i<pt.size();i++)
      v.push_back(pt(i));
    res.push_back(v);
  }
  return res;
}


void initExperimentClass(sol::state& lua)
{
  lua.new_usertype<Experiment>(
    "Experiment", sol::constructors<Experiment()>(),
    "gravity",        &Experiment::Gravity,
    "name",           &Experiment::name,
    "ribbon",         &Experiment::ribbon,
    "options",        &Experiment::options,
    "findEquilibrium",&Experiment::findEquilibrium,
    "findEWB",        &Experiment::findEquilibriumWithoutBound,
    "newton",         &Experiment::runNewton,
    "newtonC",        &Experiment::runBetterNewton,
    "newtonR",        &Experiment::runNewtonR,
    "BFGS",           &Experiment::runBFGS,
    "CG",             &Experiment::runCG,
    "gradientDescent",&Experiment::runGradientDescent,
    "runHybrid",      &Experiment::runHybrid,
    "energy",         &Experiment::energy,
    "gradientNorm",   &Experiment::gradientNorm,
    "attachEnd",      &Experiment::attachEnd,
    "detachEnd",      &Experiment::detachEnd,
    "iters",          &Experiment::getLogs,
    "use3dotAlignementConstraint", &Experiment::use3dotAlignementConstraint,
    "stats",          &Experiment::ostats
    );
  lua.set_function("IChooseSadowsky", [](){ElasticEnergy::ChooseModel(ElasticEnergy::Model::Sadowsky);});
  lua.set_function("IChooseWunderlich", [](){ElasticEnergy::ChooseModel(ElasticEnergy::Model::Wunderlich);});
  lua.set_function("IChooseSadowskyFiber", [](){ElasticEnergy::ChooseModel(ElasticEnergy::Model::FiberSadowsky);});
  lua.set_function("IChooseWunderlichFiber", [](){ElasticEnergy::ChooseModel(ElasticEnergy::Model::FiberWunderlich);});
  lua.set_function("IChooseRibext", [](){ElasticEnergy::ChooseModel(ElasticEnergy::Model::AudolyNeukirch);});
  
  lua.set_function("selModel", [](std::string model){
  if(model=="Wunderlich)")
    ElasticEnergy::ChooseModel(ElasticEnergy::Model::Sadowsky);
  else if (model=="Sadowsky")
    ElasticEnergy::ChooseModel(ElasticEnergy::Model::Wunderlich);
  if(model=="WunderlichFiber)")
    ElasticEnergy::ChooseModel(ElasticEnergy::Model::FiberSadowsky);
  else if (model=="SadowskyFiber")
    ElasticEnergy::ChooseModel(ElasticEnergy::Model::FiberWunderlich);
  else if(model=="Ribext")
    ElasticEnergy::ChooseModel(ElasticEnergy::Model::AudolyNeukirch);
  else
    throw ("I don't know model"+model);
    
  });
  
  lua.set_function("setEtaPrimeRegularizationFactor", [](double f){EBending_SW::etaPrimeRegularizationFactor=f;});
  lua.set_function("getEtaPrimeRegularizationFactor", [](){return EBending_SW::etaPrimeRegularizationFactor;});
}
