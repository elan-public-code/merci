/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef EXPERIMENT_H
#define EXPERIMENT_H
#include "../sol.hpp"
#include "RibbonStdInterface.hpp"
#include "IpoptOptions.hpp"
#include <interface.hpp>
#include <vector>

class Experiment
{
public:
  std::array<double,3> Gravity = {0., 0., -9.81};
  std::string name = "GiveMeAName";
  RibbonStdInterface ribbon;
  IpoptOptions options;
  int findEquilibrium();
  int findEquilibriumWithoutBound();
  void runNewton();
  void runNewtonR();
  void runGradientDescent();
  void runBetterNewton();
  void runBFGS();
  void runCG();
  int runHybrid(HybridParams params);
  double energy();
  double gradientNorm();
  void attachEnd(std::array<double,3> posEnd, std::array<double,9> frameEnd);
  void detachEnd();
  void use3dotAlignementConstraint(bool val);
  std::vector<std::vector<double>> getLogs();
  IpoptOutStats ostats;
private:
  int findEquilibriumFunction();
  bool fixEnd=false;
  bool fixEndShortDOF=false;
  std::array<double,3> posEnd;
  std::array<double,9> frameEnd;
  std::vector<Eigen::VectorXr> logs;
};

void initExperimentClass(sol::state &lua);

#endif // EXPERIMENT_H
