/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "Experiment.hpp"
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

#include <SRStaticProblem.hpp>
#include <cppoptlib/solver/cgsolver.h>
#include <ElasticEnergy.hpp>

void Experiment::runCG()
{
  RibbonSystem phY(Eigen::Vector3r(Gravity[0],Gravity[1],Gravity[2]));
  phY.set(ribbon.getSR());
  SRStaticProblem pb(phY);
  cppoptlib::MickaelNewtonDescentSolver<SRStaticProblem, cppoptlib::NewtonType::CG> solver;
  cppoptlib::MickaelNewtonDescentSolver<SRStaticProblem, cppoptlib::NewtonType::CG>::TCriteria m_stop =
  cppoptlib::MickaelNewtonDescentSolver<SRStaticProblem, cppoptlib::NewtonType::CG>::TCriteria::defaults();
  m_stop.gradNorm = options.tol;
  m_stop.fDelta = 0.;
  m_stop.iterations = options.maxIters;
  solver.setStopCriteria(m_stop);
  solver.setDebug(cppoptlib::DebugLevel::High);
  Eigen::VectorXr pt = pb.getPoint();
  solver.minimize(pb, pt);
  pb.setPoint(pt);
  
  SuperRibbon sr =pb.get();
  
  ribbon.setRibbonStart(sr.getOmega1(0).B,sr.getEta(0).B);
  for(int i=0;i<ribbon.getNbSegments();i++)
    ribbon.setRibbonCurv(i,sr.getOmega1(i).A,sr.getEta(i).A);
}

