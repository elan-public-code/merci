/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "Experiment.hpp"
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

#include <IpIpoptApplication.hpp>
#include <IpSolveStatistics.hpp>
#include <IpoptSuperRibbon.hpp>
#include <SRStaticProblem.hpp>
#include <ElasticEnergy.hpp>

using namespace Ipopt;

int Experiment::findEquilibrium()
{
  if(options.safeComputation)
    std::cerr<<"safe computation disabled"<<std::endl;
  if(false)
  {
    pid_t pid = fork();
    if (pid == 0) {
      //child
      findEquilibriumFunction();
      exit(0);
    } else if (pid > 0) {
      //parent
      int status;
      waitpid(pid, &status, 0);
    }
  }
  else
  {
    return findEquilibriumFunction();
  }
}

int Experiment::findEquilibriumFunction()
{
  RibbonSystem phY(Eigen::Vector3r(Gravity[0],Gravity[1],Gravity[2]));
  phY.set(ribbon.getSR());
  
  // Create a new instance of your nlp
  //  (use a SmartPtr, not raw)
  IpoptSuperRibbon *mynlpr;
  if(fixEnd){
    IpoptSuperRibbonFixEnd fe;
    fe.pos<<posEnd[0],posEnd[1],posEnd[2];
    fe.frame<<frameEnd[0],frameEnd[1],frameEnd[2],
      frameEnd[3],frameEnd[4],frameEnd[5],
      frameEnd[6],frameEnd[7],frameEnd[8];
      fe.constraintFrameDir=fixEndShortDOF;
    mynlpr = new IpoptSuperRibbon(phY,fe);
  }
  else
    mynlpr = new IpoptSuperRibbon(phY);
  SmartPtr<TNLP> mynlp = mynlpr;

  // Create a new instance of IpoptApplication
  //  (use a SmartPtr, not raw)
  // We are using the factory, since this allows us to compile this
  // example with an Ipopt Windows DLL
  SmartPtr<IpoptApplication> app = IpoptApplicationFactory();

  // Change some options
  // Note: The following choices are only examples, they might not be
  //       suitable for your optimization problem.
  app->Options()->SetNumericValue("tol", options.tol);
  //app->Options()->SetStringValue("mu_strategy", "adaptive");
  //app->Options()->SetStringValue("mu_strategy", "monotone");
  app->Options()->SetIntegerValue("print_level",options.logLevel);//12);
  app->Options()->SetStringValue("output_file", options.outputFile);
  if (options.checkDerivation)app->Options()->SetStringValue("derivative_test", "second-order");
  app->Options()->SetStringValue("fixed_variable_treatment","make_constraint");
  app->Options()->SetNumericValue("bound_relax_factor",0);
  app->Options()->SetIntegerValue("max_iter", options.maxIters);
  //app->Options()->SetIntegerValue("max_soc",0);
  if(options.hessianApprox)app->Options()->SetStringValue("hessian_approximation","limited-memory");
  if(options.warmstartFromPrevious)app->Options()->SetStringValue("warm_start_init_point","yes");

  // Intialize the IpoptApplication and process the options
  ApplicationReturnStatus status;
  status = app->Initialize();
  if (status != Solve_Succeeded) {
    std::cout<<std::endl<<std::endl<<"*** Error during initialization!"<<std::endl;
    return status;
  }

  // Ask Ipopt to solve the problem
  status = app->OptimizeTNLP(mynlp);

  //The logs already announce the status
  /*if (status == Solve_Succeeded) {
    std::cout<<std::endl<<std::endl<<"*** The problem was solved!"<<std::endl;
  } else {
    std::cout<<std::endl<<std::endl<<"*** The problem FAILED!"<<std::endl;
  }*/

  // As the SmartPtrs go out of scope, the reference count
  // will be decremented and the objects will automatically
  // be deleted.


  SuperRibbon sr = mynlpr->getSolution();
  
  ribbon.setRibbonStart(sr.getOmega1(0).B,sr.getEta(0).B);
  for(int i=0;i<ribbon.getNbSegments();i++)
    ribbon.setRibbonCurv(i,sr.getOmega1(i).A,sr.getEta(i).A);

  logs = mynlpr->getLogs();
  auto stats = app->Statistics ();
  ostats.iterationsCount=stats->IterationCount();
  ostats.totalTime=stats->TotalCPUTime();
  stats->NumberOfEvaluations(ostats.numerEvalObjective, ostats.numerEvalConstraints, ostats.numerEvalGradientObjective, ostats.numerEvalGradientConstraints, ostats.numerEvalHessian);
  stats->Infeasibilities(ostats.dual_inf, ostats.constr_viol, ostats.complementarity, ostats.ktt_error);
  stats->ScaledInfeasibilities(ostats.dual_inf_scaled, ostats.constr_viol_scaled, ostats.complementarity_scaled, ostats.ktt_error_scaled);
  ostats.lambda=mynlpr->getLambda();
  ostats.setStatus(status);
  return status;
}

int Experiment::findEquilibriumWithoutBound()
{
  RibbonSystem phY(Eigen::Vector3r(Gravity[0],Gravity[1],Gravity[2]));
  phY.set(ribbon.getSR());
  
  // Create a new instance of your nlp
  //  (use a SmartPtr, not raw)
  IpoptSuperRibbon *mynlpr;
  if(fixEnd){
    IpoptSuperRibbonFixEnd fe;
    fe.pos<<posEnd[0],posEnd[1],posEnd[2];
    fe.frame<<frameEnd[0],frameEnd[1],frameEnd[2],
      frameEnd[3],frameEnd[4],frameEnd[5],
      frameEnd[6],frameEnd[7],frameEnd[8];
      fe.constraintFrameDir=fixEndShortDOF;
    mynlpr = new IpoptSuperRibbon(phY, fe, true);
  }
  else
    mynlpr = new IpoptSuperRibbon(phY, true);
  SmartPtr<TNLP> mynlp = mynlpr;

  // Create a new instance of IpoptApplication
  //  (use a SmartPtr, not raw)
  // We are using the factory, since this allows us to compile this
  // example with an Ipopt Windows DLL
  SmartPtr<IpoptApplication> app = IpoptApplicationFactory();

  // Change some options
  // Note: The following choices are only examples, they might not be
  //       suitable for your optimization problem.
  app->Options()->SetNumericValue("tol", options.tol);
  //app->Options()->SetStringValue("mu_strategy", "adaptive");
  //app->Options()->SetStringValue("mu_strategy", "monotone");
  app->Options()->SetIntegerValue("print_level",options.logLevel);//12);
  app->Options()->SetStringValue("output_file", options.outputFile);
  if (options.checkDerivation)app->Options()->SetStringValue("derivative_test", "second-order");
  app->Options()->SetStringValue("fixed_variable_treatment","make_constraint");
  app->Options()->SetNumericValue("bound_relax_factor",0);
  app->Options()->SetIntegerValue("max_iter", options.maxIters);
  //app->Options()->SetIntegerValue("max_soc",0);
  if(options.hessianApprox)app->Options()->SetStringValue("hessian_approximation","limited-memory");

  // Intialize the IpoptApplication and process the options
  ApplicationReturnStatus status;
  status = app->Initialize();
  if (status != Solve_Succeeded) {
    std::cout<<std::endl<<std::endl<<"*** Error during initialization!"<<std::endl;
    return status;
  }

  // Ask Ipopt to solve the problem
  status = app->OptimizeTNLP(mynlp);

  //The logs already announce the status
  /*if (status == Solve_Succeeded) {
    std::cout<<std::endl<<std::endl<<"*** The problem was solved!"<<std::endl;
  } else {
    std::cout<<std::endl<<std::endl<<"*** The problem FAILED!"<<std::endl;
  }*/

  // As the SmartPtrs go out of scope, the reference count
  // will be decremented and the objects will automatically
  // be deleted.


  SuperRibbon sr = mynlpr->getSolution();
  
  ribbon.setRibbonStart(sr.getOmega1(0).B,sr.getEta(0).B);
  for(int i=0;i<ribbon.getNbSegments();i++)
    ribbon.setRibbonCurv(i,sr.getOmega1(i).A,sr.getEta(i).A);

  logs = mynlpr->getLogs();
  auto stats = app->Statistics ();
  ostats.iterationsCount=stats->IterationCount();
  ostats.totalTime=stats->TotalCPUTime();
  stats->NumberOfEvaluations(ostats.numerEvalObjective, ostats.numerEvalConstraints, ostats.numerEvalGradientObjective, ostats.numerEvalGradientConstraints, ostats.numerEvalHessian);
  stats->Infeasibilities(ostats.dual_inf, ostats.constr_viol, ostats.complementarity, ostats.ktt_error);
  stats->ScaledInfeasibilities(ostats.dual_inf_scaled, ostats.constr_viol_scaled, ostats.complementarity_scaled, ostats.ktt_error_scaled);
  ostats.lambda=mynlpr->getLambda();
  ostats.setStatus(status);
  return status;
}

