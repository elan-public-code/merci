/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "ExtensionCMinPack.hpp"
#include <iostream>
#include <fstream>

bool is_file_exist(const char *fileName)
{
    std::ifstream infile(fileName);
    return infile.good();
}

int dlcloseSecured(void *handle)
{
  if(handle!=nullptr)
  {
    return dlclose(handle);
  }
  return 0;
}


ExtensionCMinPack::ExtensionCMinPack()
{
  if(is_file_exist("libHybrid.so"))
    hnld.reset( dlopen("libHybrid.so", RTLD_NOW),dlcloseSecured);
  else if(is_file_exist("libHybrid.dylib"))
    hnld.reset( dlopen("libHybrid.dylib", RTLD_NOW),dlcloseSecured);
  else {
    std::cout<<"libHybrid missing"<<std::endl;
    fail=true;
    return;
  }
  if(!hnld){
    std::cout << "Can not load the CMinpack extension, reason is: "<< dlerror() << std::endl;
    fail=true;
  }
  else
  {
    std::cout << "libHybrid loaded" << std::endl;
    initHybridParamsClass_ptr = reinterpret_cast<initHybridParamsClassProto>(dlsym(hnld.get(), "initHybridParamsClass"));
    callHybridOnSystem_ptr = reinterpret_cast<callHybridOnSystemProto>(dlsym(hnld.get(),"callHybridOnSystem"));
    fail = false;
  }
}

bool ExtensionCMinPack::isLoaded()
{
  return !fail;
}

void ExtensionCMinPack::initHybridParamsClass(sol::state& lua)
{
  if(fail)throw "Call to not loaded extension";
  initHybridParamsClass_ptr(lua);
}

int ExtensionCMinPack::callHybridOnSystem(RibbonSystem& phY, HybridParams params)
{
  if(fail)throw "Call to not loaded extension";
  return callHybridOnSystem_ptr(phY, params);
}


ExtensionCMinPack::~ExtensionCMinPack()
{
}

std::shared_ptr<ExtensionCMinPack> ExtensionCMinPack::Instance()
{
  if(!myInstance){throw "Unexpected non existence";}
  return myInstance;
}

std::shared_ptr<ExtensionCMinPack> ExtensionCMinPack::myInstance(new ExtensionCMinPack());
