/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef EXTENSIONCMINPACK_H
#define EXTENSIONCMINPACK_H

#include <dlfcn.h>
#include <memory>
#include <sol.hpp>
#include <RibbonSystem.hpp>
#include <interface.hpp>

typedef void (*initHybridParamsClassProto)(sol::state &lua);
typedef int (*callHybridOnSystemProto)(RibbonSystem& phY, HybridParams params);

class ExtensionCMinPack
{
public:
  static std::shared_ptr<ExtensionCMinPack> Instance();
  ~ExtensionCMinPack();
  bool isLoaded();
  void initHybridParamsClass(sol::state &lua);
  int callHybridOnSystem(RibbonSystem& phY, HybridParams params);
private:
  ExtensionCMinPack();
  static std::shared_ptr<ExtensionCMinPack> myInstance;
  std::shared_ptr<void> hnld;
  bool fail;
  initHybridParamsClassProto initHybridParamsClass_ptr;
  callHybridOnSystemProto callHybridOnSystem_ptr;
};

#endif // EXTENSIONCMINPACK_H
