/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "FExperiment.hpp"
#include <FiberSystem.hpp>
#include <SCStaticProblem.hpp>
#include <cppoptlib/solver/gradientdescentsolver.h>
#include <cppoptlib/solver/newtondescentsolver.h>
#include <cppoptlib/solver/newtongradientdescentsolver.h>
#include <cppoptlib/solver/newtonregularizeddescentsolver.h>
#include <Algos/FiniteDifferencesTest.hpp>
#include <IpoptFibre.hpp>
#include <IpIpoptApplication.hpp>

using namespace Ipopt;

int FExperiment::findEquilibrium()
{
  FiberSystem phY(Eigen::Vector3r(Gravity[0],Gravity[1],Gravity[2]));
  phY.set(fibre.getSC());
  IpoptFibre *mynlpr = new IpoptFibre(phY);
  SmartPtr<TNLP> mynlp = mynlpr;

  // Create a new instance of IpoptApplication
  //  (use a SmartPtr, not raw)
  // We are using the factory, since this allows us to compile this
  // example with an Ipopt Windows DLL
  SmartPtr<IpoptApplication> app = IpoptApplicationFactory();

  // Change some options
  // Note: The following choices are only examples, they might not be
  //       suitable for your optimization problem.
  app->Options()->SetNumericValue("tol", options.tol);
  app->Options()->SetIntegerValue("print_level", options.logLevel);//12);
  app->Options()->SetStringValue("output_file", options.outputFile);
  if (options.checkDerivation)app->Options()->SetStringValue("derivative_test", "second-order");
  app->Options()->SetStringValue("fixed_variable_treatment","make_constraint");
  app->Options()->SetNumericValue("bound_relax_factor",0);
  app->Options()->SetIntegerValue("max_iter", options.maxIters);
  if(options.hessianApprox)app->Options()->SetStringValue("hessian_approximation","limited-memory");
  if(options.hessianApprox)app->Options()->SetIntegerValue("limited_memory_max_history",options.limitedMemoryMaxHistory);

  // Intialize the IpoptApplication and process the options
  ApplicationReturnStatus status;
  status = app->Initialize();
  if (status != Solve_Succeeded) {
    std::cout<<std::endl<<std::endl<<"*** Error during initialization!"<<std::endl;
    return status;
  }

  // Ask Ipopt to solve the problem
  status = app->OptimizeTNLP(mynlp);


  SuperClothoid sc = mynlpr->getSolution();
  
  fibre.setFiberStartCurvature(sc.getOmega1(0).B,sc.getOmega2(0).B,sc.getOmega3(0).B);
  for(int i=0;i<fibre.getNbSegments();i++)
    fibre.setFiberCurvatureDerivative(i,sc.getOmega1(i).A,sc.getOmega2(i).A,sc.getOmega3(i).A);
  
  logs = mynlpr->getLogs();
  return status;
}

FExperiment::FExperiment()
{
  namer=
    [](int i)->std::string{
      std::stringstream ss;
      ss<<'w'<<i%3+1;
      if(i<3)ss<<"(0)";
      else ss<<"'_"<<(i/3)-1;
      return ss.str();
    };
}


void FExperiment::testGradient()
{
  FiberSystem phY(Eigen::Vector3r(Gravity[0],Gravity[1],Gravity[2]));
  phY.set(fibre.getSC());
  std::function<Eigen::VectorXr()> getSystemPoint=[&phY]()->Eigen::VectorXr{return phY.getPoint();};
  std::function<real(const Eigen::VectorXr &)> getEnergyValueAt=[&phY](const Eigen::VectorXr & pt)->real{phY.setPoint(pt);return phY.energy();};
  std::function<Eigen::VectorXr(const Eigen::VectorXr &)> getEnergyGradientAt=[&phY](const Eigen::VectorXr & pt)->Eigen::VectorXr{phY.setPoint(pt);return phY.gradient();};
  FiniteDifferencesTest tester(getSystemPoint, getEnergyValueAt, getEnergyGradientAt);
  tester.setNaming(namer);
  tester.setParams(1.e-5, 1.e-5);
  tester.run("fibre gradient");
}

void FExperiment::testHessian()
{
  int idx=0;
  FiberSystem phY(Eigen::Vector3r(Gravity[0],Gravity[1],Gravity[2]));
  phY.set(fibre.getSC());
  int t=phY.getPoint().size();
  std::function<Eigen::VectorXr()> getSystemPoint=[&phY]()->Eigen::VectorXr{return phY.getPoint();};
  std::function<real(const Eigen::VectorXr &)> getEnergyValueAt=
    [&phY,&idx](const Eigen::VectorXr & pt)->real{
      phY.setPoint(pt);
      Eigen::VectorXr g= phY.gradient();
      return g[idx];
    };
  std::function<Eigen::VectorXr(const Eigen::VectorXr &)> getEnergyGradientAt=
    [&phY,&idx](const Eigen::VectorXr & pt)->Eigen::VectorXr{
      phY.setPoint(pt);
      Eigen::MatrixXr h = phY.hessian();
      return h.row(idx);
    };
  FiniteDifferencesTest tester(getSystemPoint, getEnergyValueAt, getEnergyGradientAt);
  tester.setNaming(namer);
  tester.setParams(1.e-5, 1.e-5);
  for(;idx<t;idx++)
    tester.run("fibre hessian "+namer(idx));
}

void FExperiment::testGradientPosX()
{
  FiberSystem phY(Eigen::Vector3r(Gravity[0],Gravity[1],Gravity[2]));
  phY.set(fibre.getSC());
  std::function<Eigen::VectorXr()> getSystemPoint=[&phY]()->Eigen::VectorXr{return phY.getPoint();};
  std::function<real(const Eigen::VectorXr &)> getEnergyValueAt=
    [&phY](const Eigen::VectorXr & pt)->real{
      phY.setPoint(pt);
      return phY.posX();
    };
  std::function<Eigen::VectorXr(const Eigen::VectorXr &)> getEnergyGradientAt=
    [&phY](const Eigen::VectorXr & pt)->Eigen::VectorXr{
      phY.setPoint(pt);
      return phY.gradPosX();
    };
  FiniteDifferencesTest tester(getSystemPoint, getEnergyValueAt, getEnergyGradientAt);
  tester.setNaming(namer);
  tester.setParams(1.e-5, 1.e-5);
  tester.run("gradient posX");
}

void FExperiment::testGradientFrameX()
{
  FiberSystem phY(Eigen::Vector3r(Gravity[0],Gravity[1],Gravity[2]));
  phY.set(fibre.getSC());
  std::function<Eigen::VectorXr()> getSystemPoint=[&phY]()->Eigen::VectorXr{return phY.getPoint();};
  std::function<real(const Eigen::VectorXr &)> getEnergyValueAt=
    [&phY](const Eigen::VectorXr & pt)->real{
      phY.setPoint(pt);
      return phY.frameX();
    };
  std::function<Eigen::VectorXr(const Eigen::VectorXr &)> getEnergyGradientAt=
    [&phY](const Eigen::VectorXr & pt)->Eigen::VectorXr{
      phY.setPoint(pt);
      return phY.gradFrameX();
    };
  FiniteDifferencesTest tester(getSystemPoint, getEnergyValueAt, getEnergyGradientAt);
  tester.setNaming(namer);
  tester.setParams(1.e-5, 1.e-5);
  tester.run("gradient frameX");
}

void FExperiment::testHessianPosX()
{
  int idx=0;
  FiberSystem phY(Eigen::Vector3r(Gravity[0],Gravity[1],Gravity[2]));
  phY.set(fibre.getSC());
  int t=phY.getPoint().size();
  std::function<Eigen::VectorXr()> getSystemPoint=[&phY]()->Eigen::VectorXr{return phY.getPoint();};
  std::function<real(const Eigen::VectorXr &)> getEnergyValueAt=
    [&phY,&idx](const Eigen::VectorXr & pt)->real{
      phY.setPoint(pt);
      Eigen::VectorXr g= phY.gradPosX();
      return g[idx];
    };
  std::function<Eigen::VectorXr(const Eigen::VectorXr &)> getEnergyGradientAt=
    [&phY,&idx](const Eigen::VectorXr & pt)->Eigen::VectorXr{
      phY.setPoint(pt);
      Eigen::MatrixXr h = phY.hessPosX();
      return h.row(idx);
    };
  FiniteDifferencesTest tester(getSystemPoint, getEnergyValueAt, getEnergyGradientAt);
  tester.setNaming(namer);
  tester.setParams(1.e-5, 1.e-5);
  for(;idx<t;idx++)
    tester.run("hessian posX "+namer(idx));
}

void FExperiment::testHessianFrameX()
{
  int idx=0;
  FiberSystem phY(Eigen::Vector3r(Gravity[0],Gravity[1],Gravity[2]));
  phY.set(fibre.getSC());
  int t=phY.getPoint().size();
  std::function<Eigen::VectorXr()> getSystemPoint=[&phY]()->Eigen::VectorXr{return phY.getPoint();};
  std::function<real(const Eigen::VectorXr &)> getEnergyValueAt=
    [&phY,&idx](const Eigen::VectorXr & pt)->real{
      phY.setPoint(pt);
      Eigen::VectorXr g= phY.gradFrameX();
      return g[idx];
    };
  std::function<Eigen::VectorXr(const Eigen::VectorXr &)> getEnergyGradientAt=
    [&phY,&idx](const Eigen::VectorXr & pt)->Eigen::VectorXr{
      phY.setPoint(pt);
      Eigen::MatrixXr h = phY.hessFrameX();
      return h.row(idx);
    };
  FiniteDifferencesTest tester(getSystemPoint, getEnergyValueAt, getEnergyGradientAt);
  tester.setNaming(namer);
  tester.setParams(1.e-5, 1.e-5);
  for(;idx<t;idx++)
    tester.run("hessian frameX "+namer(idx));
}


double FExperiment::energy()
{
  FiberSystem phY(Eigen::Vector3r(Gravity[0],Gravity[1],Gravity[2]));
  phY.set(fibre.getSC());
  return phY.gradient().norm();
}

double FExperiment::gradientNorm()
{
  FiberSystem phY(Eigen::Vector3r(Gravity[0],Gravity[1],Gravity[2]));
  phY.set(fibre.getSC());
  return phY.energy();
}

void FExperiment::runGradientDescent()
{
  FiberSystem phY(Eigen::Vector3r(Gravity[0],Gravity[1],Gravity[2]));
  phY.set(fibre.getSC());
  SCStaticProblem pb(phY);
  cppoptlib::GradientDescentSolver<SCStaticProblem> solver;
  cppoptlib::GradientDescentSolver<SCStaticProblem>::TCriteria m_stop =
  cppoptlib::GradientDescentSolver<SCStaticProblem>::TCriteria::defaults();
  m_stop.gradNorm = options.tol;
  m_stop.fDelta = 0.;
  m_stop.iterations = options.maxIters;
  solver.setStopCriteria(m_stop);
  solver.setDebug(cppoptlib::DebugLevel::High);
  Eigen::VectorXr pt = pb.getPoint();
  solver.minimize(pb, pt);
  pb.setPoint(pt);
  
  SuperClothoid sc =pb.get();
  
  fibre.setFiberStartCurvature(sc.getOmega1(0).B,sc.getOmega2(0).B,sc.getOmega3(0).B);
  for(int i=0;i<fibre.getNbSegments();i++)
    fibre.setFiberCurvatureDerivative(i,sc.getOmega1(i).A,sc.getOmega2(i).A,sc.getOmega3(i).A);
}

void FExperiment::runNewton()
{
  FiberSystem phY(Eigen::Vector3r(Gravity[0],Gravity[1],Gravity[2]));
  phY.set(fibre.getSC());
  SCStaticProblem pb(phY);
  cppoptlib::NewtonDescentSolver<SCStaticProblem> solver;
  cppoptlib::NewtonDescentSolver<SCStaticProblem>::TCriteria m_stop =
  cppoptlib::NewtonDescentSolver<SCStaticProblem>::TCriteria::defaults();
  m_stop.gradNorm = options.tol;
  m_stop.fDelta = 0.;
  m_stop.iterations = options.maxIters;
  solver.setStopCriteria(m_stop);
  solver.setDebug(cppoptlib::DebugLevel::High);
  Eigen::VectorXr pt = pb.getPoint();
  solver.minimize(pb, pt);
  pb.setPoint(pt);
  
  SuperClothoid sc =pb.get();
  
  fibre.setFiberStartCurvature(sc.getOmega1(0).B,sc.getOmega2(0).B,sc.getOmega3(0).B);
  for(int i=0;i<fibre.getNbSegments();i++)
    fibre.setFiberCurvatureDerivative(i,sc.getOmega1(i).A,sc.getOmega2(i).A,sc.getOmega3(i).A);
}

void FExperiment::runNewtonR()
{
  FiberSystem phY(Eigen::Vector3r(Gravity[0],Gravity[1],Gravity[2]));
  phY.set(fibre.getSC());
  SCStaticProblem pb(phY);
  cppoptlib::NewtonRegularizedDescentSolver<SCStaticProblem> solver;
  cppoptlib::NewtonRegularizedDescentSolver<SCStaticProblem>::TCriteria m_stop =
  cppoptlib::NewtonRegularizedDescentSolver<SCStaticProblem>::TCriteria::defaults();
  m_stop.gradNorm = options.tol;
  m_stop.fDelta = 0.;
  m_stop.iterations = options.maxIters;
  solver.setStopCriteria(m_stop);
  solver.setDebug(cppoptlib::DebugLevel::High);
  Eigen::VectorXr pt = pb.getPoint();
  solver.minimize(pb, pt);
  pb.setPoint(pt);
  
  SuperClothoid sc =pb.get();
  
  fibre.setFiberStartCurvature(sc.getOmega1(0).B,sc.getOmega2(0).B,sc.getOmega3(0).B);
  for(int i=0;i<fibre.getNbSegments();i++)
    fibre.setFiberCurvatureDerivative(i,sc.getOmega1(i).A,sc.getOmega2(i).A,sc.getOmega3(i).A);
}

void FExperiment::runBetterNewton()
{
  FiberSystem phY(Eigen::Vector3r(Gravity[0],Gravity[1],Gravity[2]));
  phY.set(fibre.getSC());
  SCStaticProblem pb(phY);
  cppoptlib::NewtonGradientDescentSolver<SCStaticProblem> solver;
  cppoptlib::NewtonGradientDescentSolver<SCStaticProblem>::TCriteria m_stop =
  cppoptlib::NewtonGradientDescentSolver<SCStaticProblem>::TCriteria::defaults();
  m_stop.gradNorm = options.tol;
  m_stop.fDelta = 0.;
  m_stop.iterations = options.maxIters;
  solver.setStopCriteria(m_stop);
  solver.setDebug(cppoptlib::DebugLevel::High);
  Eigen::VectorXr pt = pb.getPoint();
  solver.minimize(pb, pt);
  pb.setPoint(pt);
  
  SuperClothoid sc =pb.get();
  
  fibre.setFiberStartCurvature(sc.getOmega1(0).B,sc.getOmega2(0).B,sc.getOmega3(0).B);
  for(int i=0;i<fibre.getNbSegments();i++)
    fibre.setFiberCurvatureDerivative(i,sc.getOmega1(i).A,sc.getOmega2(i).A,sc.getOmega3(i).A);
}

std::vector<std::vector<double>> FExperiment::getLogs()
{
  std::vector<std::vector<double>> res;
  for(Eigen::VectorXr pt:logs)
  {
    std::vector<double> v;
    for(int i=0;i<pt.size();i++)
      v.push_back(pt(i));
    res.push_back(v);
  }
  return res;
}

void initFExperimentClass(sol::state& lua)
{
  lua.new_usertype<FExperiment>(
    "FExperiment", sol::constructors<FExperiment()>(),
    "gravity",        &FExperiment::Gravity,
    "name",           &FExperiment::name,
    "fibre",          &FExperiment::fibre,
    "options",        &FExperiment::options,
    "findEquilibrium",&FExperiment::findEquilibrium,
    "newton",         &FExperiment::runNewton,
    "newtonR",        &FExperiment::runNewtonR,
    "newtonC",        &FExperiment::runBetterNewton,
    "gradientDescent",&FExperiment::runGradientDescent,
    "energy",         &FExperiment::energy,
    "gradientNorm",   &FExperiment::gradientNorm,
    "testGradient",   &FExperiment::testGradient,
    "testHessian",    &FExperiment::testHessian,
    "testHessianPosX",&FExperiment::testHessianPosX,
    "testHessianFrameX",&FExperiment::testHessianFrameX,
    "testGradientPosX",&FExperiment::testGradientPosX,
    "testGradientFrameX",&FExperiment::testGradientFrameX,
    "iters",          &FExperiment::getLogs
    );
}
