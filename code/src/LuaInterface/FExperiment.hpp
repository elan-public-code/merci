/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef FEXPERIMENT_H
#define FEXPERIMENT_H

#include "../sol.hpp"
#include "FiberStdInterface.hpp"
#include "IpoptOptions.hpp"

class FExperiment
{
public:
  FExperiment();
  std::array<double,3> Gravity = {0., 0., -9.81};
  std::string name = "GiveMeAName";
  FiberStdInterface fibre;
  IpoptOptions options;
  int findEquilibrium();
  void runNewton();
  void runNewtonR();
  void runGradientDescent();
  void runBetterNewton();
  double energy();
  double gradientNorm();
  void testGradient();
  void testHessian();
  void testHessianPosX();
  void testHessianFrameX();
  void testGradientPosX();
  void testGradientFrameX();
  std::vector<std::vector<double>> getLogs();
private:
  std::function<std::string(int i)> namer;
  std::vector<Eigen::VectorXr> logs;
};

void initFExperimentClass(sol::state &lua);

#endif // FEXPERIMENT_H
