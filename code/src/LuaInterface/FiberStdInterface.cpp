/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "FiberStdInterface.hpp"
#include <SuperClothoidCore/CElasticEnergy.hpp>
#include <SuperClothoidCore/FiberSystem.hpp>

FiberStdInterface::FiberStdInterface():
  crossSection({1.e-3,1.e-3}),
  linearDensity(0.1),
  posOr(0.,0.,0.),
  omegaInit({0.,0.,0.}),
  naturalOmegaInit({0.,0.,0.})
{
  elasticMatrix=CElasticEnergy::ParamsToK(1.e9,0.5,1.e-3,1.e-3);
  frameOr.setIdentity();
}

void FiberStdInterface::setElasticMatrix(std::array<double, 9> K)
{
  elasticMatrix<<K[0],K[1],K[2],
                 K[3],K[4],K[5],
                 K[6],K[7],K[8];
}

std::array<double, 9> FiberStdInterface::getElasticMatrix()
{
  std::array<double, 9> res;
  for(int i=0;i<9;i++)res[i]=elasticMatrix(i%3,i/3);
  return res;
}


void FiberStdInterface::setElasticProperties(double YoungModulus, double poissonRatio, double width, double thickness)
{
  elasticMatrix=CElasticEnergy::ParamsToK(YoungModulus,poissonRatio,width,thickness);
}

void FiberStdInterface::setElasticMatrixForRectangle(double YoungModulus, double poissonRatio, double width, double thickness)
{
  const double a = width/2;
  const double b = thickness/2;
  const double E = YoungModulus;
  const double sigma = poissonRatio;
  const double a2 = a*a;
  const double a3 = a2*a;
  const double b2 = b*b;
  const double b3 = b2*b;
  double sumC=0.;
  for(int k=0;k<7;k++)
    sumC+=tanh((2*k+1)*M_PI*a/(2.0*b))/pow(2.*k+1.,5);
  const double J=16./3.*a*b3*(1.-192./(M_PI*M_PI*M_PI*M_PI*M_PI)*b/a*sumC);
  const double muJ = E/(2.*(1.+sigma))*J;
  const double prCoeff = 1./(1.-sigma*sigma);
  const double EI1 = prCoeff*E*4./3.*a*b3;
  const double EI2 = E*4./3.*a3*b;
  elasticMatrix<<EI1,0,0,
                 0,EI2,0,
                 0,0,muJ;
  //std::cout<<EI1<<", "<<EI2<<", "<<muJ<<std::endl;
}

void FiberStdInterface::setLinearDensity(double ld) {linearDensity=ld;} double FiberStdInterface::getLinearDensity() {return linearDensity;}
void FiberStdInterface::setCrossSection(std::array<double, 2> cs) {crossSection=cs;} std::array<double,2>FiberStdInterface::getCrossSection(){return crossSection;}

void FiberStdInterface::setFrameOr(std::array<double, 9> f)
{
  frameOr<<f[0],f[1],f[2],
           f[3],f[4],f[5],
           f[6],f[7],f[8];
}
void FiberStdInterface::setPosOr(std::array<double, 3> p)
{
  posOr<<p[0],p[1],p[2];
}

std::array<double, 9> FiberStdInterface::getFrameOr()
{
  return
  std::array<double, 9> {{frameOr(0,0),frameOr(0,1),frameOr(0,2),
                        frameOr(1,0),frameOr(1,1),frameOr(1,2),
                        frameOr(2,0),frameOr(2,1),frameOr(2,2)}};
}

std::array<double, 3> FiberStdInterface::getPosOr()
{
  return std::array<double, 3>{{posOr[0],posOr[1],posOr[2]}};
}

void FiberStdInterface::setNbSegments(int ns){pieces.resize(ns);} int FiberStdInterface::getNbSegments(){return pieces.size();}
  
void FiberStdInterface::setLengthSegment(int segmentId, double length)
{
  pieces.at(segmentId).length=length;
}

void FiberStdInterface::setFiberStartCurvature(double w1, double w2, double w3)
{
  omegaInit = {w1,w2,w3};
}

void FiberStdInterface::setFiberStartNaturalCurvature(double nw1, double nw2, double nw3)
{
  naturalOmegaInit = {nw1,nw2,nw3};
}

void FiberStdInterface::setFiberCurvatureDerivative(int sgtId, double w1d, double w2d, double w3d)
{
  pieces.at(sgtId).omegaD={w1d,w2d,w3d};
}

void FiberStdInterface::setFiberNaturalCurvatureDerivative(int sgtId, double nw1d, double nw2d, double nw3d)
{
  pieces.at(sgtId).naturalOmegaD={nw1d,nw2d,nw3d};
}

std::array<double, 3> FiberStdInterface::getEndPos()
{
  FiberSystem phY(Eigen::Vector3r(0.,0.,0.));
  phY.set(getSC());
  Eigen::Vector3r p = phY.getEndPos();
  return  {p[0],p[1],p[2]};
}

std::array<double, 9> FiberStdInterface::getEndFrame()
{
  FiberSystem phY(Eigen::Vector3r(0.,0.,0.));
  phY.set(getSC());
  Eigen::Matrix3r f = phY.getEndFrame();
  return
  std::array<double, 9> {{f(0,0),f(0,1),f(0,2),
                        f(1,0),f(1,1),f(1,2),
                        f(2,0),f(2,1),f(2,2)}};
}

void FiberStdInterface::setPoint(std::vector<double> data)
{
  assert(data.size()==3*(1+pieces.size()));
  omegaInit={data[0],data[1],data[2]};
  for(size_t i=0;i<pieces.size();i++)
    pieces[i].omegaD={data[3*i+3],data[3*i+4],data[3*i+5]};
}

std::vector<double> FiberStdInterface::getPoint()
{
  std::vector<double> res(3*(1+pieces.size()));
  for(size_t i=0;i<3;i++)res[i]=omegaInit[i];
  for(size_t i=0;i<pieces.size();i++)
    for(size_t j=0;j<3;j++)
      res[3*i+3+j]=pieces[i].omegaD[j];
  return res;
}

FiberStdInterface FiberStdInterface::copy()
{
  return FiberStdInterface(*this);
}

SuperClothoid FiberStdInterface::getSC()
{
  SuperClothoid sc;
  sc.setPosOr(posOr);
  sc.setFrameOr(frameOr);
  sc.setElasticMatrix(elasticMatrix);
  sc.setElpiticSection(crossSection[0],crossSection[1]);
  sc.setLinearDensity(linearDensity);
  int n=pieces.size();
  for(int i=0;i<n;i++)
  {
    sc.addSegmentAt(i);
    sc.setLength(i,pieces[i].length);
    sc.setOmegaPrime(i,pieces[i].omegaD[0],pieces[i].omegaD[1],pieces[i].omegaD[2]);
    sc.setNaturalOmegaPrime(i,pieces[i].naturalOmegaD[0],pieces[i].naturalOmegaD[1],pieces[i].naturalOmegaD[2]);
  }
  sc.setOmegaInit(omegaInit[0],omegaInit[1],omegaInit[2]);
  sc.setNaturalOmegaInit(naturalOmegaInit[0],naturalOmegaInit[1],naturalOmegaInit[2]);
  return sc;
}

void initFiberStdInterfaceClass(sol::state& lua)
{
    lua.new_usertype<FiberStdInterface>(
    "Fibre",                     sol::constructors<FiberStdInterface()>(),
    "setElasticProperties",      sol::overload(&FiberStdInterface::setElasticProperties,&FiberStdInterface::setElasticMatrix),
    "getElasticMatrix", &FiberStdInterface::getElasticMatrix,
    "setElasticPropertiesRect",  &FiberStdInterface::setElasticMatrixForRectangle,
    "crossSection",              sol::property(&FiberStdInterface::getCrossSection,     &FiberStdInterface::setCrossSection),
    "position",                  sol::property(&FiberStdInterface::setPosOr,            &FiberStdInterface::getPosOr),
    "frame",                     sol::property(&FiberStdInterface::setFrameOr,          &FiberStdInterface::getFrameOr),
    "linearDensity",             sol::property(&FiberStdInterface::getLinearDensity,    &FiberStdInterface::setLinearDensity),
    "point",                     sol::property(&FiberStdInterface::setPoint,            &FiberStdInterface::getPoint),
    "numberSegments",            sol::property(&FiberStdInterface::setNbSegments,       &FiberStdInterface::getNbSegments),
    "setLengthSegment",          &FiberStdInterface::setLengthSegment,
    "setOmegaInit",              &FiberStdInterface::setFiberStartCurvature,
    "setNaturalOmegaInit",       &FiberStdInterface::setFiberStartNaturalCurvature,
    "setOmegaDerivative",        &FiberStdInterface::setFiberCurvatureDerivative,
    "setNaturalOmegaDerivative", &FiberStdInterface::setFiberNaturalCurvatureDerivative,
    "endPos",                    &FiberStdInterface::getEndPos,
    "endFrame",                  &FiberStdInterface::getEndFrame,
    "copy",                      &FiberStdInterface::copy
    );
}
