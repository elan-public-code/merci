/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef FIBERSTDINTERFACE_H
#define FIBERSTDINTERFACE_H

#include "sol.hpp"
#include <array>
#include <vector>
#include <SuperClothoidCore/SuperClothoid.hpp>
class FiberStdInterface
{
public:
  FiberStdInterface();
  void setElasticMatrix(std::array<double,9> K);
  std::array<double,9> getElasticMatrix();
  void setElasticProperties(double YoungModulus, double poissonRatio, double width, double thickness);
  void setElasticMatrixForRectangle(double YoungModulus, double poissonRatio, double width, double thickness);
  void setLinearDensity(double ld);              double getLinearDensity();
  void setCrossSection(std::array<double,2> cs); std::array<double,2> getCrossSection();
  void setFrameOr(std::array<double,9> f);       std::array<double,9> getFrameOr();
  void setPosOr(std::array<double,3> p);         std::array<double,3> getPosOr();
  void setPoint(std::vector<double> data);       std::vector<double> getPoint();
  void setNbSegments(int ns);                    int getNbSegments();
  
  void setLengthSegment(int segmentId, double length);
  void setFiberStartCurvature(double w1, double w2, double w3);
  void setFiberStartNaturalCurvature(double nw1, double nw2, double nw3);
  void setFiberCurvatureDerivative(int sgtId, double w1d, double w2d, double w3d);
  void setFiberNaturalCurvatureDerivative(int sgtId, double nw1d, double nw2d, double nw3d);
  FiberStdInterface copy();
  std::array<double,3> getEndPos();
  std::array<double,9> getEndFrame();
  
  SuperClothoid getSC();
  
private:
  std::array<double,2> crossSection;
  double linearDensity;
  Eigen::Matrix3d elasticMatrix;
  Eigen::Vector3d posOr;
  Eigen::Matrix3d frameOr;
  std::array<double,3> omegaInit;
  std::array<double,3> naturalOmegaInit;
  struct Piece{double length; std::array<double,3> omegaD; std::array<double,3> naturalOmegaD;};
  std::vector<Piece> pieces;
};

void initFiberStdInterfaceClass(sol::state &lua);

#endif // FIBERSTDINTERFACE_H
