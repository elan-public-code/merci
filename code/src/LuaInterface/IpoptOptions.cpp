/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "IpoptOptions.hpp"

void IpoptOutStats::setStatus(Ipopt::ApplicationReturnStatus val)
{
  switch(val)
  {
  case Ipopt::Solve_Succeeded:
    status = "Solve Succeeded";
    break;
  case Ipopt::Solved_To_Acceptable_Level:
    status = "Solved To Acceptable Level";
    break;
  case Ipopt::Infeasible_Problem_Detected:
    status = "Infeasible Problem Detected";
    break;
  case Ipopt::Search_Direction_Becomes_Too_Small:
    status = "Search Direction Becomes Too Small";
    break;
  case Ipopt::Diverging_Iterates:
    status = "Diverging Iterates";
    break;
  case Ipopt::User_Requested_Stop:
    status = "User Requested Stop";
    break;
  case Ipopt::Feasible_Point_Found:
    status = "Feasible Point Found";
    break;
  case Ipopt::Maximum_Iterations_Exceeded:
    status = "Maximum Iterations Exceeded";
    break;
  case Ipopt::Restoration_Failed:
    status = "Restoration Failed";
    break;
  case Ipopt::Error_In_Step_Computation:
    status = "Error In Step Computation";
    break;
  case Ipopt::Maximum_CpuTime_Exceeded:
    status = "Maximum CpuTime Exceeded";
    break;
  case Ipopt::Not_Enough_Degrees_Of_Freedom:
    status = "Not Enough Degrees Of Freedom";
    break;
  case Ipopt::Invalid_Problem_Definition:
    status = "Invalid Problem Definition";
    break;
  case Ipopt::Invalid_Option:
    status = "Invalid Option";
    break;
  case Ipopt::Invalid_Number_Detected:
    status = "Invalid Number Detected";
    break;
  case Ipopt::Unrecoverable_Exception:
    status = "Unrecoverable Exception";
    break;
  case Ipopt::NonIpopt_Exception_Thrown:
    status = "NonIpopt Exception Thrown";
    break;
  case Ipopt::Insufficient_Memory:
    status = "Insufficient Memory";
    break;
  case Ipopt::Internal_Error:
    status = "Internal Error";
    break;
  }
}


void initIpoptOptionClass(sol::state& lua)
{
  lua.new_usertype<IpoptOptions>(
    "Options", sol::constructors<IpoptOptions()>(),
    "checkDerivation",&IpoptOptions::checkDerivation,
    "maxIters",&IpoptOptions::maxIters,
    "tol", &IpoptOptions::tol,
    "outputFile", &IpoptOptions::outputFile,
    "safeComputation", &IpoptOptions::safeComputation,
    "hessianApprox", &IpoptOptions::hessianApprox,
    "limitedMemoryMaxHistory",&IpoptOptions::limitedMemoryMaxHistory,
    "logLevel",&IpoptOptions::logLevel,
    "warmstartFromPrevious",&IpoptOptions::warmstartFromPrevious
    );
  
  lua.new_usertype<IpoptOutStats>(
    "IPOPTStats",             sol::constructors<IpoptOutStats()>(),
    "iterations",             &IpoptOutStats::iterationsCount,
    "time",                   &IpoptOutStats::totalTime,
    "evalObjective",          &IpoptOutStats::numerEvalObjective,
    "evalGradientObjective",  &IpoptOutStats::numerEvalGradientObjective,
    "evalConstraints",        &IpoptOutStats::numerEvalConstraints,
    "evalJacConstraints",     &IpoptOutStats::numerEvalGradientConstraints,
    "evalHessian",            &IpoptOutStats::numerEvalHessian,
    "dual_inf",               &IpoptOutStats::dual_inf,
    "constr_viol",            &IpoptOutStats::constr_viol,
    "complementarity",        &IpoptOutStats::complementarity,
    "ktt_error",              &IpoptOutStats::ktt_error,
    "dual_inf_scaled",        &IpoptOutStats::dual_inf_scaled,
    "constr_viol_scaled",     &IpoptOutStats::constr_viol_scaled,
    "complementarity_scaled", &IpoptOutStats::complementarity_scaled,
    "ktt_error_scaled",       &IpoptOutStats::ktt_error_scaled,
    "lambda",                 &IpoptOutStats::lambda,
    "repulsionFields",        &IpoptOutStats::repulsionFields,
    "status",                 &IpoptOutStats::status
                   );
}
