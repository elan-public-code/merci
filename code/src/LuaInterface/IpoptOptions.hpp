/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef IPOPTOPTIONS_H
#define IPOPTOPTIONS_H
#include "../sol.hpp"
#include <IpIpoptApplication.hpp>

class IpoptOptions
{
public:
  bool checkDerivation = false;//Only set to true to test the derivations, otherwise, it will slow the app
  bool warmstartFromPrevious = false;
  int maxIters = 10000;
  double tol=1.e-12;
  std::string outputFile;
  bool safeComputation = false;
  bool hessianApprox = false;
  int limitedMemoryMaxHistory = 6;
  int logLevel=5;
};

struct IpoptOutStats
  {
    int iterationsCount=0;
    double totalTime=0.;
    int numerEvalObjective=0;
    int numerEvalGradientObjective=0;
    int numerEvalConstraints=0;
    int numerEvalGradientConstraints=0;
    int numerEvalHessian=0;
    double dual_inf=0.;
    double constr_viol=0.;
    double complementarity=0.;
    double ktt_error=0.;
    double dual_inf_scaled=0.;
    double constr_viol_scaled=0.;
    double complementarity_scaled=0.;
    double ktt_error_scaled=0.;
    std::vector<double> lambda;
    std::vector<double> WSzU;
    std::vector<double> WSzL;
    std::vector<double> repulsionFields;
    std::string status;
    void setStatus(Ipopt::ApplicationReturnStatus val);
  };

void initIpoptOptionClass(sol::state &lua);
#endif // IPOPTOPTIONS_H
