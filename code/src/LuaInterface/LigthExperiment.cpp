/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "LigthExperiment.hpp"
#include <LightRS.hpp>

double LigthExperiment::energy()
{
  LightRS phY(Eigen::Vector3r(Gravity[0],Gravity[1],Gravity[2]));
  phY.set(ribbon.getSR());
  return phY.energy();
}

double LigthExperiment::gradientNorm()
{
  LightRS phY(Eigen::Vector3r(Gravity[0],Gravity[1],Gravity[2]));
  phY.set(ribbon.getSR());
  return phY.gradient().norm();
}

void LigthExperiment::step()
{
  LightRS phY(Eigen::Vector3r(Gravity[0],Gravity[1],Gravity[2]));
  phY.set(ribbon.getSR());
  Eigen::VectorXr g=phY.gradient();
  auto SR=ribbon.getSR();
  std::vector<double> pt = ribbon.getPoint();
  size_t t=pt.size();
  double length=0.;
  for(int i=0;i<SR.getNbSegments();i++)
    length+=SR.getLength(i);
  double factor = ribbon.getWidth()*ribbon.getAreaDensity()*length;
  for(size_t i=0;i<t;i++)
    pt.at(i)-=stepSize*g[i]/factor;
  ribbon.setPoint(pt);
}

void initLightExperimentClass(sol::state& lua)
{
  lua.new_usertype<LigthExperiment>(
    "FakeDyn", sol::constructors<LigthExperiment()>(),
    "gravity",        &LigthExperiment::Gravity,
    "name",           &LigthExperiment::name,
    "ribbon",         &LigthExperiment::ribbon,
    "energy",         &LigthExperiment::energy,
    "gradientNorm",   &LigthExperiment::gradientNorm,
    "stepSize",       &LigthExperiment::stepSize,
    "step",           &LigthExperiment::step
    );
}
