/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef LIGTHEXPERIMENT_H
#define LIGTHEXPERIMENT_H

#include "../sol.hpp"
#include "RibbonStdInterface.hpp"
#include <interface.hpp>
#include <vector>

class LigthExperiment
{
public:
  std::array<double,3> Gravity = {0., 0., -9.81};
  std::string name = "GiveMeAName";
  RibbonStdInterface ribbon;
  double stepSize;
  double energy();
  double gradientNorm();
  void step();
};

void initLightExperimentClass(sol::state &lua);

#endif // LIGTHEXPERIMENT_H
