/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
/* inclusion guard */
#ifndef __LUAIMAGE_H__
#define __LUAIMAGE_H__

#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <array>
#include <cmath>
#include <cstring>
#include "../sol.hpp"

class LuaImage
{
public:
  LuaImage(unsigned int width, unsigned int height);
  void setPixel(unsigned int x,unsigned int y,std::array<unsigned int,3> color);
  std::array<unsigned int,3> getPixel(unsigned int x,unsigned int y) const;
  unsigned int getWidth() const;
  unsigned int getHeight() const;
  void save(std::string filename) const;
private:
  unsigned int width;
  unsigned int height;
  std::vector<unsigned char> data;
};

void initImageClass(sol::state &lua);

#endif /* __LUAIMAGE_H__ */
