/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "MRFitter.hpp"
#include "MixedPhY.hpp"
#include "IpoptMixedRibbon.hpp"
#include <IpIpoptApplication.hpp>
#include <IpSolveStatistics.hpp>
#include "Constraints/ConstraintEndPos.hpp"
#include "Constraints/ConstraintEndFrame.hpp"

double MRFitter::energy()
{
  MixedPhY phy(ribbon.getSR());
  phy.setGravity(Eigen::Vector3r(Gravity[0],Gravity[1],Gravity[2]));
  return phy.getElasticEnergy()+phy.getGravityEnergy();
}

double MRFitter::gradientNorm()
{
  MixedPhY phy(ribbon.getSR());
  phy.setGravity(Eigen::Vector3r(Gravity[0],Gravity[1],Gravity[2]));
  return (phy.getElasticEnergyGradient()+phy.getGravityEnergyGradient()).norm();
}

void MRFitter::attachEnd(std::array<double, 3> posEnd, std::array<double, 9> frameEnd)
{
  fixEnd=true;
  this->posEnd=posEnd;
  this->frameEnd=frameEnd;
}

void MRFitter::detachEnd()
{
  fixEnd=false;
}

int MRFitter::addOffPlane(std::array<double, 3> point, std::array<double, 3> normale)
{
  offPlane.push_back({point, normale,-1,0});
  return offPlane.size()-1;
}

int MRFitter::addOffPlaneDoubleEnd(std::array<double, 3> point, std::array<double, 3> normale)
{
  offPlaneDoubleEnd.push_back({point, normale,-1,0});
  return offPlaneDoubleEnd.size()-1;
}

bool MRFitter::changeOffPlane(int at, std::array<double, 3> point, std::array<double, 3> normale)
{
  if(at>=0&&at<static_cast<int>(offPlane.size()))
  {
    offPlane.at(at)={point,normale,-1,0};
    return true;
  }
  return false;
}

bool MRFitter::changeOffPlaneDoubleEnd(int at, std::array<double, 3> point, std::array<double, 3> normale)
{
  if(at>=0&&at<static_cast<int>(offPlaneDoubleEnd.size()))
  {
    offPlaneDoubleEnd.at(at)={point,normale,-1,0};
    return true;
  }
  return false;
}

std::vector<double> MRFitter::extractLambdaOffPlane(int at)
{
  if(at<0||at>static_cast<int>(offPlane.size()))return {};
  int ids = std::get<2>(offPlane[at]);
  int idl = std::get<3>(offPlane[at]);
  if(ids<0)return{};
  if(static_cast<int>(ostats.lambda.size())>ids+idl)return{};
  std::vector<double>::const_iterator first = ostats.lambda.begin() + ids;
  std::vector<double>::const_iterator last = first + idl;
  return std::vector<double>(first,last);
}

std::vector<double> MRFitter::extractLambdaOffPlaneDoubleEnd(int at)
{
  if(at<0||at>static_cast<int>(offPlaneDoubleEnd.size()))return {};
  int ids = std::get<2>(offPlaneDoubleEnd[at]);
  int idl = std::get<3>(offPlaneDoubleEnd[at]);
  if(ids<0)return{};
  if(static_cast<int>(ostats.lambda.size())<ids+idl)return{};
  std::vector<double>::const_iterator first = ostats.lambda.begin() + ids;
  std::vector<double>::const_iterator last = first + idl;
  return std::vector<double>(first,last);
}

std::vector<double> MRFitter::extractLambdaPressure(int at)
{if(at<0||at>static_cast<int>(pressures.size()))return {};
  int ids = std::get<1>(pressures[at]);
  int idl = std::get<2>(pressures[at]);
  if(ids<0)return{};
  if(static_cast<int>(ostats.lambda.size())<ids+idl)return{};
  std::vector<double>::const_iterator first = ostats.lambda.begin() + ids;
  std::vector<double>::const_iterator last = first + idl;
  return std::vector<double>(first,last);
}


void MRFitter::clearOffPlanes()
{
  offPlane.clear();
  offPlaneDoubleEnd.clear();
}

void MRFitter::useLooseClamping(std::array<double,3> center, real radius)
{
  if(radius<0)
    clampOriInBoundingSphere = std::nullopt;
  else
  {
    Eigen::Vector3r vcenter;
    vcenter<<center[0],center[1],center[2];
    clampOriInBoundingSphere={vcenter,radius};
  }
}

void MRFitter::disableLooseClamping()
{
  clampOriInBoundingSphere = std::nullopt;
}

void MRFitter::clampRuling(bool start, bool end)
{
  startEtaFixed=start;
  endEtaFixed=end;
  etaCyclic=false;
}

void MRFitter::cyclicRulings(bool negate)
{
  etaCyclic=true;
  etaCyclicNegate=negate;
}

void MRFitter::detachOrClamping()
{
  oriClamp=false;
}

void MRFitter::reatachOrClamping()
{
  oriClamp=true;
}

void MRFitter::declareRibbonCyclic(bool val)
{
  cyclicRibbon=val;
}

void MRFitter::defineOriAsConstraint(bool val)
{
  oriIsConstraint=val;
}

bool MRFitter::getOriAsContraint()
{
  return oriIsConstraint;
}

int MRFitter::addPressure(PressureStdInterface pre)
{
  pressures.push_back({pre,-1,0});
  return pressures.size()-1;
}

bool MRFitter::changePressure(int at, PressureStdInterface pre)
{
  if(at>=0&&at<static_cast<int>(pressures.size()))
  {
    pressures.at(at)={pre,-1,0};
    return true;
  }
  return false;
}

int MRFitter::addRepulsionField(RepulsionFieldStdInterface rf)
{
  repulsionFields.push_back(rf);
  return repulsionFields.size()-1;
}

bool MRFitter::changeRepulsionField(int at, RepulsionFieldStdInterface rf)
{
  if(at>=0&&at<static_cast<int>(repulsionFields.size()))
  {
    repulsionFields.at(at)=rf;
    return true;
  }
  return false;
}

PressureStdInterface MRFitter::getPressure(int at)
{
  return std::get<0>(pressures.at(at));
}

void MRFitter::disableRibbonBounds()
{
  ribbonBoundsActive=false;
}

void MRFitter::reenableRibbonBounds()
{
  ribbonBoundsActive=true;
}

void MRFitter::alignBarycenterOnAxis(std::array<double, 3> position, std::array<double, 3> direction)
{
  Eigen::Vector3r p,d;
  p<<position[0],position[1],position[2];
  d<<direction[0],direction[1],direction[2];
  alignBarycenterOnAxisData={p,d};
}

void MRFitter::detachBarycenterFromAxis()
{
  alignBarycenterOnAxisData=std::nullopt;
}

void MRFitter::addOriFilter(Filter f)
{
  oriFilter=f;
  if(!oriIsConstraint)
    std::cout<<"Warning, filter won't be handeld, please set oriIsConstraint to true"<<std::endl;
}

void MRFitter::eraseOriFilter()
{
  oriFilter=std::nullopt;
}




void initMRFitterClass(sol::state& lua)
{
  
  lua.new_usertype<MRFitter>(
    "MRFitter", sol::constructors<MRFitter()>(),
    "gravity",                 &MRFitter::Gravity,
    "clampRuling",             &MRFitter::clampRuling,
    "name",                    &MRFitter::name,
    "ribbon",                  &MRFitter::ribbon,
    "options",                 &MRFitter::options,
    "findEquilibrium",         &MRFitter::findEquilibrium,
    "energy",                  &MRFitter::energy,
    "gradientNorm",            &MRFitter::gradientNorm,
    "attachEnd",               &MRFitter::attachEnd,
    "detachEnd",               &MRFitter::detachEnd,
    "addOffPlane",             &MRFitter::addOffPlane,
    "addOffPlaneDoubleEnd",    &MRFitter::addOffPlaneDoubleEnd,
    "changeOffPlane",          &MRFitter::changeOffPlane,
    "addPressure",             &MRFitter::addPressure,
    "changePressure",          &MRFitter::changePressure,
    "changeOffPlaneDoubleEnd", &MRFitter::changeOffPlaneDoubleEnd,
    "getPressure",             &MRFitter::getPressure,
    "addRepulsionField",       &MRFitter::addRepulsionField,
    "changeRepulsionField",    &MRFitter::changeRepulsionField,
    "clearOffPlanes",          &MRFitter::clearOffPlanes,
    "oriIsConstraint",         sol::property(&MRFitter::defineOriAsConstraint,&MRFitter::getOriAsContraint),
    "useLooseClamping",        &MRFitter::useLooseClamping,
    "disableLooseClamping",    &MRFitter::disableLooseClamping,
    "detachOrClamping",        &MRFitter::detachOrClamping,
    "reatachOrClamping",       &MRFitter::reatachOrClamping,
    "declareRibbonCyclic",     &MRFitter::declareRibbonCyclic,
    "declareEtaCyclic",        &MRFitter::cyclicRulings,
    "extractLambdaOffPlaneDoubleEnd", &MRFitter::extractLambdaOffPlaneDoubleEnd,
    "extractLambdaOffPlane",   &MRFitter::extractLambdaOffPlane,
    "extractLambdaPressure",   &MRFitter::extractLambdaPressure,
    "disableRibbonBounds",     &MRFitter::disableRibbonBounds,
    "reenableRibbonBounds",    &MRFitter::reenableRibbonBounds,
    "alignBarycenterOnAxis",   &MRFitter::alignBarycenterOnAxis,
    "detachBarycenterFromAxis",&MRFitter::detachBarycenterFromAxis,
    "stats",                   &MRFitter::ostats,
    "addOriFilter",            &MRFitter::addOriFilter,
    "eraseOriFilter",          &MRFitter::eraseOriFilter
    );
  lua.set_function("MRCoeffs",[](double a, double b){
    MixedPhY::alphaConstraintFrame=b;
    MixedPhY::alphaConstraintPos=a;
    MixedPhY::alphaConstraintEndFrame=b;
    MixedPhY::alphaConstraintEndPos=a;
  });
  lua.set_function("MRLinkCoeffs",[](double a, double b){
    MixedPhY::alphaConstraintFrame=b;
    MixedPhY::alphaConstraintPos=a;
  });
  lua.set_function("MREndCoeffs",[](double a, double b){
    MixedPhY::alphaConstraintEndFrame=b;
    MixedPhY::alphaConstraintEndPos=a;
  });
  lua.set_function("MRBarycenterCoeff",[](double a){
    MixedPhY::alphaConstraintBarycenter=a;
  });
}
