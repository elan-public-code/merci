/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef MRFITTER_H
#define MRFITTER_H

#include "../sol.hpp"
#include "MRStdInterface.hpp"
#include "IpoptOptions.hpp"
#include "PressureStdInterface.hpp"
#include "RepulsionFieldStdInterface.hpp"
#include "MixedPhyRestrainOri.hpp"
#include <vector>
#include <optional>
#include <tuple>

class MRFitter
{
public:
  std::array<double,3> Gravity = {0., 0., -9.81};
  std::string name = "GiveMeAName";
  MRStdInterface ribbon;
  IpoptOptions options;
  int findEquilibrium();
  double energy();
  double gradientNorm();
  void attachEnd(std::array<double,3> posEnd, std::array<double,9> frameEnd);
  void detachEnd();
  int addOffPlane(std::array<double,3> point, std::array<double,3> normale);
  int addOffPlaneDoubleEnd(std::array<double,3> point, std::array<double,3> normale);
  bool changeOffPlane(int at, std::array<double,3> point, std::array<double,3> normale);
  bool changeOffPlaneDoubleEnd(int at, std::array<double,3> point, std::array<double,3> normale);
  void clearOffPlanes();
  void clampRuling(bool start, bool end);
  void cyclicRulings(bool negate);
  void defineOriAsConstraint(bool val);
  bool getOriAsContraint();
  void useLooseClamping(std::array<double,3> center, real radius);
  void disableLooseClamping();
  void detachOrClamping();
  void reatachOrClamping();
  void declareRibbonCyclic(bool val);
  std::vector<double> extractLambdaOffPlane(int at);
  std::vector<double> extractLambdaOffPlaneDoubleEnd(int at);
  std::vector<double> extractLambdaPressure(int at);
  int addPressure(PressureStdInterface pre);
  bool changePressure(int at, PressureStdInterface pre);
  PressureStdInterface getPressure(int at);
  IpoptOutStats ostats;
  void disableRibbonBounds();
  void reenableRibbonBounds();
  int addRepulsionField(RepulsionFieldStdInterface rf);
  bool changeRepulsionField(int at, RepulsionFieldStdInterface rf);
  void alignBarycenterOnAxis(std::array<double,3> position, std::array<double,3> direction);
  void detachBarycenterFromAxis();
  void addOriFilter(Filter f);
  void eraseOriFilter();
private:
    bool oriIsConstraint=true;
    bool fixEnd=false;
    bool startEtaFixed=false;
    bool endEtaFixed=false;
    bool oriClamp=true;
    bool cyclicRibbon=false;
    bool etaCyclic=false;
    bool etaCyclicNegate=false;
    bool ribbonBoundsActive = true;
  std::optional<std::tuple<Eigen::Vector3r, real> > clampOriInBoundingSphere;
  //bool fixEndShortDOF=false;
  std::array<double,3> posEnd = {0.,0.,0.};
  std::array<double,9> frameEnd = {1.,0.,0.,0.,1.,0.,0.,0.,1.};
  std::vector<std::tuple<std::array<double,3>, std::array<double,3>, int, int > > offPlane;
  std::vector<std::tuple<std::array<double,3>, std::array<double,3>, int, int > > offPlaneDoubleEnd;
  std::vector<std::tuple<PressureStdInterface, int, int> > pressures;
  std::vector<RepulsionFieldStdInterface> repulsionFields;
  std::optional<std::pair<Eigen::Vector3r, Eigen::Vector3r>> alignBarycenterOnAxisData;
  std::optional<Filter> oriFilter;
};

void initMRFitterClass(sol::state &lua);

#endif // MRFITTER_H
