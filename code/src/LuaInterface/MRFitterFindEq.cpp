/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "MRFitter.hpp"
#include "MixedPhY.hpp"
#include "IpoptMixedRibbon.hpp"
#include <IpIpoptApplication.hpp>
#include <IpSolveStatistics.hpp>
#include "Constraints/ConstraintEndPos.hpp"
#include "Constraints/ConstraintEndFrame.hpp"

using namespace Ipopt;

int MRFitter::findEquilibrium()
{
  bool disableOri = false;
  if(oriFilter.has_value())disableOri=true;
  if(!oriClamp)
  {
    oriIsConstraint = true;
    disableOri = true;
  }
  MixedPhY phY(ribbon.getSR(), disableOri);
  phY.setGravity(Eigen::Vector3r(Gravity[0],Gravity[1],Gravity[2]));
  if(!ribbonBoundsActive)
    phY.setRibbonBoundActive(false);
  if(!oriClamp && clampOriInBoundingSphere.has_value()){
      phY.addConstraintClampOrBoundingSphere(std::get<0>(clampOriInBoundingSphere.value()),std::get<1>(clampOriInBoundingSphere.value()));
  }
  if(/*!oriClamp &&*/ cyclicRibbon)
      phY.addConstraintLoopRibbon();
  if(!oriClamp && alignBarycenterOnAxisData.has_value())
  {
    phY.addConstraintBarycenterOnAxis(alignBarycenterOnAxisData->first, alignBarycenterOnAxisData->second);
  }
  if (fixEnd){
    Eigen::Vector3r pos;
    pos<<posEnd[0],posEnd[1],posEnd[2];
    Eigen::Matrix3r frame;
    frame<<frameEnd[0],frameEnd[1],frameEnd[2],
        frameEnd[3],frameEnd[4],frameEnd[5],
        frameEnd[6],frameEnd[7],frameEnd[8];
    phY.addConstraintEndPos(pos);
    phY.addConstraintEndFrame(frame);
  }
  size_t i,t=pressures.size();
  //for(PressureStdInterface& pre:pressures)
  for (i=0; i<t; i++)
  {
    Pressure p=std::get<0>(pressures[i]).toPressure();
    int idx, ids,idl;
    if(std::get<0>(pressures[i]).doubleEnd) std::tie(idx, ids,idl) = phY.addPressureDoubleEnd(p);
    else std::tie(idx, ids,idl) = phY.addPressure(p);
    std::get<1>(pressures[i])=ids;
    std::get<2>(pressures[i])=idl;
  }
  for(const RepulsionFieldStdInterface& rp:repulsionFields)
  {
    phY.addRepulsionField(rp.getField());
  }
  if (etaCyclic)
  {
    phY.addConstraintLoopEta(etaCyclicNegate);
  }
  else
  {
    if (startEtaFixed)phY.addConstraintEtaStart();
    if (endEtaFixed)phY.addConstraintEtaEnd();
  }
  t=offPlane.size();
  for (i=0; i<t; i++){//auto[pt_a,nml_a]:offPlane){
    Eigen::Vector3r pt;
    Eigen::Vector3r nml;
    std::array<double,3> pt_a = std::get<0>(offPlane[i]);
    std::array<double,3> nml_a = std::get<1>(offPlane[i]);
    pt<<pt_a[0],pt_a[1],pt_a[2];
    nml<<nml_a[0],nml_a[1],nml_a[2];
    auto [ids,idl] = phY.addConstraintOffPlane(pt,nml);
    std::get<2>(offPlane[i])=ids;
    std::get<3>(offPlane[i])=idl;
    
  }
  t=offPlaneDoubleEnd.size();
  for (i=0; i<t; i++){ //for (auto[pt_a,nml_a]:offPlaneDoubleEnd){
    Eigen::Vector3r pt;
    Eigen::Vector3r nml;
    std::array<double,3> pt_a = std::get<0>(offPlaneDoubleEnd[i]);
    std::array<double,3> nml_a = std::get<1>(offPlaneDoubleEnd[i]);
    pt<<pt_a[0],pt_a[1],pt_a[2];
    nml<<nml_a[0],nml_a[1],nml_a[2];
    auto [ids,idl] = phY.addConstraintOffPlaneDoubleEnd(pt,nml);
    std::get<2>(offPlaneDoubleEnd[i])=ids;
    std::get<3>(offPlaneDoubleEnd[i])=idl;
  }
  // Create a new instance of your nlp
  //  (use a SmartPtr, not raw)
  IpoptMixedRibbonVirtualBase *mynlpr;
  if(oriFilter.has_value())
  {
    std::cout<<"----------------- USING FILTER"<<std::endl;
    mynlpr = new IpoptMixedRibbon<MixedPhyRestrainOri>(phY, oriFilter.value()); 
  }
  else if(oriIsConstraint)mynlpr = new IpoptMixedRibbon<MixedPhY>(phY);
  else mynlpr = new IpoptMixedRibbon<MixedPhyBoxOri>(phY);
  //IpoptMixedRibbon *mynlpr = new IpoptMixedRibbon(phY);
  SmartPtr<TNLP> mynlp = mynlpr;

  // Create a new instance of IpoptApplication
  //  (use a SmartPtr, not raw)
  // We are using the factory, since this allows us to compile this
  // example with an Ipopt Windows DLL
  SmartPtr<IpoptApplication> app = IpoptApplicationFactory();

  // Change some options
  // Note: The following choices are only examples, they might not be
  //       suitable for your optimization problem.
  app->Options()->SetNumericValue("tol", options.tol);
  //app->Options()->SetStringValue("mu_strategy", "adaptive");
  //app->Options()->SetStringValue("mu_strategy", "monotone");
  app->Options()->SetIntegerValue("print_level",options.logLevel);//12);
  app->Options()->SetStringValue("output_file", options.outputFile);
  if (options.checkDerivation)app->Options()->SetStringValue("derivative_test", "second-order");
  app->Options()->SetStringValue("fixed_variable_treatment","make_constraint");
  app->Options()->SetNumericValue("bound_relax_factor",0);
  app->Options()->SetIntegerValue("max_iter", options.maxIters);
  //app->Options()->SetIntegerValue("max_soc",0);
  if(options.warmstartFromPrevious){
    app->Options()->SetStringValue("warm_start_init_point","yes");
    mynlpr->setupWS(ostats.lambda, ostats.WSzU, ostats.WSzL);
  }

  // Intialize the IpoptApplication and process the options
  ApplicationReturnStatus status;
  status = app->Initialize();
  if (status != Solve_Succeeded) {
    std::cout<<std::endl<<std::endl<<"*** Error during initialization!"<<std::endl;
    return (int) status;
  }

  // Ask Ipopt to solve the problem
  status = app->OptimizeTNLP(mynlp);
  
  if(status == Ipopt::NonIpopt_Exception_Thrown)
  {
    std::cout<<std::endl<<std::endl<<"*** Fatal Error during minimisation!"<<std::endl;
    return (int) status;
  }

//   if (status == Solve_Succeeded) {
//     std::cout<<std::endl<<std::endl<<"*** The problem solved!"<<std::endl;
//   } else {
//     std::cout<<std::endl<<std::endl<<"*** The problem FAILED!"<<std::endl;
//   }

  // As the SmartPtrs go out of scope, the reference count
  // will be decremented and the objects will automatically
  // be deleted.
  ribbon.setSR(mynlpr->getSolution());
  t = pressures.size();
  auto pd = mynlpr->getPressureDistances();
  for(size_t i=0;i<t;i++)
    std::get<0>(pressures[i]).distance=pd[i];
    
  auto stats = app->Statistics ();
  ostats.iterationsCount=stats->IterationCount();
  ostats.totalTime=stats->TotalCPUTime();
  stats->NumberOfEvaluations(ostats.numerEvalObjective, ostats.numerEvalConstraints, ostats.numerEvalGradientObjective, ostats.numerEvalGradientConstraints, ostats.numerEvalHessian);
  stats->Infeasibilities(ostats.dual_inf, ostats.constr_viol, ostats.complementarity, ostats.ktt_error);
  stats->ScaledInfeasibilities(ostats.dual_inf_scaled, ostats.constr_viol_scaled, ostats.complementarity_scaled, ostats.ktt_error_scaled);
  ostats.lambda = mynlpr->getLambda();
  ostats.WSzU=mynlpr->getWSzU();
  ostats.WSzL=mynlpr->getWSzL();
  ostats.setStatus(status);
  MixedPhY tempPhY(mynlpr->getSolution());
  ostats.repulsionFields=mynlpr->getRepulsionEnergyPerField();
  return (int) status;
}
