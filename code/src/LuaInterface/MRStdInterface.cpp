/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "MRStdInterface.hpp"
#include "MixedPhY.hpp"
#include <RibbonPathGenerator.hpp>

MRStdInterface::MRStdInterface()
{
}

MRStdInterface::MRStdInterface(RibbonStdInterface from)
{
  msr.setThickness(from.getThickness());
  int t=from.getNbSegments();
  msr.setNbSegments(t);
  msr.setWidth(from.getWidth());
  msr.setAreaDensity(from.getAreaDensity());
  msr.setD(from.getD());
  msr.setPoissonRatio(from.getPoissonRatio());
  
  SuperRibbon sr=from.getSR();
  msr.setPosStart(0,sr.getPosOr());
  msr.setFrameStart(0,sr.getFrameOr());
  
  
  for(int i=0;i<t;i++)
  {
    msr.setLength(i,sr.getLength(i));
    msr.setOmega1(i,sr.getOmega1(i));
    msr.setNatOmega1(i,sr.getNatOmega1(i).value_or(LinearScalar(0.,0.)));
    msr.setEta(i,sr.getEta(i));
  }
  reChain();
}

MRStdInterface MRStdInterface::copy()
{
  MRStdInterface cpy;
  cpy.msr=msr;
  return cpy;
}

void MRStdInterface::setNbSegments(int n)
{
  msr.setNbSegments(n);
}

int MRStdInterface::getNbSegments()
{
  return msr.getNbSegments();
}

void MRStdInterface::setWidth(double w)
{
  msr.setWidth(w);
}

double MRStdInterface::getWidth()
{
  return msr.getWidth();
}

void MRStdInterface::setThickness(double h)
{
  msr.setThickness(h);
}

double MRStdInterface::getThickness()
{
  return msr.getThickness();
}

void MRStdInterface::setAreaDensity(double ad)
{
  msr.setAreaDensity(ad);
}

double MRStdInterface::getAreaDensity()
{
  return msr.getAreaDensity();
}

void MRStdInterface::setD(double d)
{
  msr.setD(d);
}

double MRStdInterface::getD()
{
  return msr.getD();
}

void MRStdInterface::setPoissonRatio(double nu)
{
  msr.setPoissonRatio(nu);
}

double MRStdInterface::getPoissonRatio()
{
  return msr.getPoissonRatio().value_or(0.);
}

void MRStdInterface::setPosOr(std::array<double, 3> pos)
{
  setPosStart(0,pos);
}

void MRStdInterface::setFrameOr(std::array<double, 9> f)
{
  setFrameStart(0,f);
}

std::array<double,3> MRStdInterface::getPosOr()
{
  return getPosStart(0);
}

std::array<double,9> MRStdInterface::getFrameOr()
{
  return getFrameStart(0);
}

void MRStdInterface::setOmega1(int segment, double a, double b)
{
  msr.setOmega1(segment,LinearScalar(a,b));
}

std::array<double, 2> MRStdInterface::getOmega1(int segment)
{
  auto w1=msr.getOmega1(segment);
  return{w1.A,w1.B};
}

void MRStdInterface::setNatOmega1(int segment, double anat, double bnat)
{
  msr.setNatOmega1(segment,LinearScalar(anat,bnat));
}

std::array<double, 2> MRStdInterface::getNatOmega1(int segment)
{
  auto w1=msr.getNatOmega1(segment);
  return{w1.A,w1.B};
}

void MRStdInterface::setEta(int segment, double n, double m)
{
  msr.setEta(segment,LinearScalar(n,m));
}

std::array<double, 2> MRStdInterface::getEta(int segment)
{
  auto n=msr.getEta(segment);
  return{n.A,n.B};
}

void MRStdInterface::setMiniSysd(int segment, double a, double b, double n, double m)
{
  setMiniSys(segment, {a,b,n,m});
}

void MRStdInterface::setMiniSys(int segment, std::array<double, 4> msys)
{
  msr.setOmega1(segment,LinearScalar(msys[0],msys[1]));
  msr.setEta(segment,LinearScalar(msys[2],msys[3]));
}

std::array<double, 4> MRStdInterface::getMiniSys(int segment)
{
  auto w1=msr.getOmega1(segment);
  auto n=msr.getEta(segment);
  return{w1.A,w1.B,n.A,n.B};
}

void MRStdInterface::setLength(int segment, double l)
{
  msr.setLength(segment,l);
}

double MRStdInterface::getLength(int segment)
{
  return msr.getLength(segment);
}

void MRStdInterface::setPosStart(int segment, std::array<double, 3> pos)
{
  Eigen::Vector3r p;p<<pos[0],pos[1],pos[2];
  msr.setPosStart(segment,p);
}

void MRStdInterface::setFrameStart(int segment, std::array<double, 9> frame)
{
  Eigen::Matrix3r f;
  f<<frame[0],frame[1],frame[2],
           frame[3],frame[4],frame[5],
           frame[6],frame[7],frame[8];
  msr.setFrameStart(segment,f);
}

std::array<double, 3> MRStdInterface::getPosStart(int segment)
{
  Eigen::Vector3r pos= msr.getPosStart(segment);
  return {pos[0],pos[1],pos[2]};
}

std::array<double, 9> MRStdInterface::getFrameStart(int segment)
{
  Eigen::Matrix3r frame = msr.getFrameStart(segment);
  return
  std::array<double, 9> {{frame(0,0),frame(0,1),frame(0,2),
                        frame(1,0),frame(1,1),frame(1,2),
                        frame(2,0),frame(2,1),frame(2,2)}};
}

void MRStdInterface::reChain()
{
  MixedPhY phy(msr);
  int t=msr.getNbSegments();
  Eigen::Vector3r pos;
  Eigen::Matrix3r frame;
  Eigen::VectorXr pt = phy.getPoint();
  for(int i=0;i<t-1;i++)
  {
    std::tie(pos, frame) = phy.posFrameAtEnd(i);
    msr.setPosStart(i+1,pos);
    msr.setFrameStart(i+1,frame);
    pt.segment<3>(MixedPhY::vpointIDPos(i+1))=pos;
    for(int j=0;j<9;j++) pt(MixedPhY::vpointIDFrame(i+1)+j)=frame(j/3,j%3);
    phy.setPoint(pt);
  }
}

std::array<double, 3> MRStdInterface::endPos(int segment)
{
  MixedPhY phy(msr);
  Eigen::Vector3r pos = phy.posAtEnd(segment);
  return {pos[0], pos[1], pos[2]};
}

std::array<double, 9> MRStdInterface::endFrame(int segment)
{
  MixedPhY phy(msr);
  Eigen::Matrix3r frame = phy.frameAtEnd(segment);
  return
  std::array<double, 9> {{frame(0,0),frame(0,1),frame(0,2),
                        frame(1,0),frame(1,1),frame(1,2),
                        frame(2,0),frame(2,1),frame(2,2)}};
}

MixedSuperRibbon MRStdInterface::getSR()
{
  return msr;
}

void MRStdInterface::setSR(MixedSuperRibbon sr)
{
  msr=sr;
}

void MRStdInterface::genDiscreteDescriptor(double prec, std::string filename)
{
  std::ofstream os(filename);
  RibbonPathGenerator pathGen;
  auto ribbon = getSR();
  double length=0.0;
  int t=ribbon.getNbSegments();
  for(int i=0;i<t;i++)length+=ribbon.getLength(i);
  std::vector<ObjRibbonVertex> path = pathGen.genShapeFrom(ribbon,prec);
  os<<"[";
  Eigen::IOFormat CommaInitFmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "", "");
  bool first=true;
  double s=0.;
  for(ObjRibbonVertex obj:path)
  {
    if(!first)os<<',';
    first=false;
    os<<std::endl<<" {"<<std::endl;
    os<<"  \"s\":"<<s<<','<<std::endl;
    os<<"  \"pos\":["<<obj.pos.format(CommaInitFmt)<<"],"<<std::endl;
    os<<"  \"frame\":["<<obj.frame.format(CommaInitFmt)<<"],"<<std::endl;
    os<<"  \"curvature\":["<<obj.omega1<<", 0, "<<obj.omega1*obj.eta<<"],"<<std::endl;
    os<<"  \"eta\":"<<obj.eta<<std::endl;
    os<<" }";
    s+=prec;
    if(s>length)s=length;
  }
  os<<std::endl<<"]"<<std::endl;
}

MRStdInterface::MRStdInterface(std::string fromFile)
{
  std::ifstream ifs( fromFile, std::ios::binary );
  std::function<double()> rdouble=[&ifs]()->double{
    double read;
    ifs.read(reinterpret_cast<char*>(&read), sizeof(double));
    return read;
  };
  std::function<int()> rint=[&ifs]()->int{
    int read;
    ifs.read(reinterpret_cast<char*>(&read), sizeof(int));
    return read;
  };
  std::function<std::array<double, 3>()> rad3=[&ifs]()->std::array<double, 3>{
    std::array<double, 3> read;
    ifs.read(reinterpret_cast<char*>(&read), sizeof(std::array<double, 3>));
    return read;
  };
  std::function<std::array<double, 4>()> rad4=[&ifs]()->std::array<double, 4>{
    std::array<double, 4> read;
    ifs.read(reinterpret_cast<char*>(&read), sizeof(std::array<double, 4>));
    return read;
  };
  std::function<std::array<double, 9>()> rad9=[&ifs]()->std::array<double, 9>{
    std::array<double, 9> read;
    ifs.read(reinterpret_cast<char*>(&read), sizeof(std::array<double, 9>));
    return read;
  };
  setThickness(rdouble());
  setWidth(rdouble());
  setAreaDensity(rdouble());
  setD(rdouble());
  setPoissonRatio(rdouble());
  int n = rint();
  setNbSegments(n);
  for(int i=0;i<n;i++)
  {
    //length, pos, frame, abnm
    setLength(i,rdouble());
    setPosStart(i,rad3());
    setFrameStart(i,rad9());
    setMiniSys(i,rad4());
  }
  ifs.close();
}

void MRStdInterface::saveToFile(std::string filename)
{
  std::ofstream ofs( filename, std::ios::binary );
  std::function<void(double)> wdouble=[&ofs](double arg){
    ofs.write(reinterpret_cast<char*>(&arg), sizeof(double));
  };
  std::function<void(int)> wint=[&ofs](int arg){
    ofs.write(reinterpret_cast<char*>(&arg), sizeof(int));
  };
  
  std::function<void(std::array<double, 3>)> wda3=[&ofs](std::array<double, 3> arg){
    ofs.write(reinterpret_cast<char*>(&arg), sizeof(std::array<double, 3>));
  };
  std::function<void(std::array<double, 4>)> wda4=[&ofs](std::array<double, 4> arg){
    ofs.write(reinterpret_cast<char*>(&arg), sizeof(std::array<double, 4>));
  };
  std::function<void(std::array<double, 9>)> wda9=[&ofs](std::array<double, 9> arg){
    ofs.write(reinterpret_cast<char*>(&arg), sizeof(std::array<double, 9>));
  };
  wdouble(getThickness());
  wdouble(getWidth());
  wdouble(getAreaDensity());
  wdouble(getD());
  wdouble(getPoissonRatio());
  int n=getNbSegments();
  wint(n);
  for(int i=0;i<n;i++)
  {
    //length, pos, frame, abnm
    wdouble(getLength(i));
    wda3(getPosStart(i));
    wda9(getFrameStart(i));
    wda4(getMiniSys(i));
  }
  ofs.close();
}

void initMixedSuperRibbonStdInterfaceClass(sol::state& lua)
{
  lua.new_usertype<MRStdInterface>(
    "MixedSuperRibbon",       sol::constructors<MRStdInterface(),MRStdInterface(RibbonStdInterface),MRStdInterface(std::string)>(),
    "width",                  sol::property(&MRStdInterface::getWidth,       &MRStdInterface::setWidth),
    "thickness",              sol::property(&MRStdInterface::getThickness,   &MRStdInterface::setThickness),
    "areaDensity",            sol::property(&MRStdInterface::setAreaDensity, &MRStdInterface::getAreaDensity),
    "D",                      sol::property(&MRStdInterface::setD,           &MRStdInterface::getD),
    "position",               sol::property(&MRStdInterface::setPosOr,       &MRStdInterface::getPosOr),
    "frame",                  sol::property(&MRStdInterface::setFrameOr,     &MRStdInterface::getFrameOr),
    "poissonRatio",           sol::property(&MRStdInterface::setPoissonRatio,&MRStdInterface::getPoissonRatio),
    "numberSegments",         sol::property(&MRStdInterface::setNbSegments,  &MRStdInterface::getNbSegments),
    "setOmega1",              &MRStdInterface::setOmega1,
    "setNatOmega1",           &MRStdInterface::setNatOmega1,
    "setEta",                 &MRStdInterface::setEta,
    "setMiniSys",             sol::overload(&MRStdInterface::setMiniSysd,&MRStdInterface::setMiniSys),
    "setLength",              &MRStdInterface::setLength,
    "setPosStart",            &MRStdInterface::setPosStart,
    "setFrameStart",          &MRStdInterface::setFrameStart,
    "getOmega1",              &MRStdInterface::getOmega1,
    "getNatOmega1",           &MRStdInterface::setNatOmega1,
    "getEta",                 &MRStdInterface::getEta,
    "getMiniSys",             &MRStdInterface::getMiniSys,
    "getLength",              &MRStdInterface::getLength,
    "getPosStart",            &MRStdInterface::getPosStart,
    "getFrameStart",          &MRStdInterface::getFrameStart,
    "copy",                   &MRStdInterface::copy,
    "reChain",                &MRStdInterface::reChain,
    "endPos",                 &MRStdInterface::endPos,
    "endFrame",               &MRStdInterface::endFrame,
    "genDiscreteDescriptor",  &MRStdInterface::genDiscreteDescriptor,
    "saveToFile",             &MRStdInterface::saveToFile
    );
}
