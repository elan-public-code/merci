/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef MRSTDINTERFACE_H
#define MRSTDINTERFACE_H

#include <BaseCore/LinearScalar.hpp>
#include <Eigen/Dense>
#include <vector>
#include <optional>
#include <MixedSuperRibbon.hpp>

#include "RibbonStdInterface.hpp"

class MRStdInterface
{
public:
  MRStdInterface();
  MRStdInterface(RibbonStdInterface from);
  MRStdInterface(std::string fromFile);
  void setNbSegments(int n);        int getNbSegments();
  void setWidth(double w);          double getWidth();
  void setThickness(double h);      double getThickness();
  void setAreaDensity(double ad);   double getAreaDensity();
  void setD(double d);              double getD();
  void setPoissonRatio(double nu);  double getPoissonRatio();
  
  void setPosOr(std::array<double, 3> pos); std::array<double,3> getPosOr();
  void setFrameOr(std::array<double,9> f); std::array<double,9> getFrameOr();
  
  void setOmega1(int segment, double a, double b);
  std::array<double, 2> getOmega1(int segment);
  void setNatOmega1(int segment, double anat, double bnat);
  std::array<double, 2> getNatOmega1(int segment);
  void setEta(int segment, double n, double m);
  std::array<double, 2> getEta(int segment);
  void setMiniSysd(int segment, double a, double b, double n, double m);
  void setMiniSys(int segment, std::array<double, 4> msys);
  std::array<double, 4> getMiniSys(int segment);
  void setLength(int segment, double l);
  double getLength(int segment);
  void setPosStart(int segment, std::array<double, 3> pos);
  void setFrameStart(int segment, std::array<double, 9> frame);
  std::array<double, 3> getPosStart(int segment);
  std::array<double, 9> getFrameStart(int segment);
  void genDiscreteDescriptor(double prec, std::string filename);

  MixedSuperRibbon getSR();
  void setSR(MixedSuperRibbon sr);
  void reChain();
  MRStdInterface copy();
  void saveToFile(std::string filename);
  
  std::array<double, 3> endPos(int segment);
  std::array<double, 9> endFrame(int segment);
private:
  MixedSuperRibbon msr;
};

void initMixedSuperRibbonStdInterfaceClass(sol::state& lua);

#endif // MRSTDINTERFACE_H
