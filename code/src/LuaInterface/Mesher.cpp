/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "Mesher.hpp"
#include <SuperRibbonMesher.hpp>
#include <FigureSuperRibbonMesher.hpp>
#include <FibreMesher.hpp>
#include <ArrowHDMesh.hpp>
#include <PlaneMesh.hpp>
#include <MRMesher.hpp>
#include <RibbonPathGenerator.hpp>
#include <CenterlineMesher.hpp>
#include <RectangleMesh.hpp>
#include <iomanip>
#include <SphereMesh.hpp>

Mesher::Mesher(std::string filename):
  mainColor(0,0,0),
  secondColor(0,0,0)
{
  writer.open(filename);
}

Mesher::~Mesher()
{
  writer.close();
}

void Mesher::setPrecision(double prec) { precision=prec; }
double Mesher::getPrecision() { return precision; }
void Mesher::setMainColor(std::array<int, 3> mc) { mainColor = Eigen::Vector3i(mc[0]%256, mc[1]%256, mc[2]%256); }
void Mesher::setSecondColor(std::array<int, 3> sc) { secondColor = Eigen::Vector3i(sc[0]%256, sc[1]%256, sc[2]%256); }
std::array<int, 3> Mesher::getMainColor() { return { mainColor[0], mainColor[1], mainColor[2]}; }
std::array<int, 3> Mesher::getSecondColor() { return { secondColor[0], secondColor[1], secondColor[2]}; }
void Mesher::setRibbonName(std::string n) { name=n; }
std::string Mesher::getRibbonName() { return name; }

void Mesher::setColors(std::array<int, 3> mc, std::array<int, 3> sc)
{
  setMainColor(mc);
  setSecondColor(sc);
}

void Mesher::add(RibbonStdInterface ribbon)
{
  addSS(ribbon, false);
}

void Mesher::addSS(RibbonStdInterface ribbon, bool showSubSegs)
{
  SuperRibbon rib = ribbon.getSR();
  SuperRibbonMesher mesher(mainColor, secondColor);
  mesher.setMeshDiscreteLength(precision);
  mesher.setRibbonThickness(rib.getThickness());
  mesher.setRibbonName(name);
  if(showSubSegs)mesher.showSubsegmentInsteadOfSegments();
  mesher.set(rib);
  mesher.save(writer);
}

void Mesher::addMR(MRStdInterface ribbon)
{
  MRMesher mesher(mainColor, secondColor);
  mesher.setMeshDiscreteLength(precision);
  mesher.setRibbonThickness(ribbon.getThickness());
  mesher.set(ribbon.getSR());

  mesher.save(writer);
}


void Mesher::addF(FiberStdInterface fiber)
{
  SuperClothoid sc = fiber.getSC();
  FibreMesher mesher(mainColor,secondColor);
  mesher.setMeshDiscreteLength(precision);
  mesher.setCrossSection(fiber.getCrossSection()[0],fiber.getCrossSection()[1]);
  mesher.setFibreName(name);
  mesher.set(sc);
  mesher.save(writer);
}

void Mesher::addA(ArrowForMesher arrow)
{
  Eigen::Vector3d ori(arrow.ori[0],arrow.ori[1],arrow.ori[2]);
  Eigen::Vector3d dir(arrow.dir[0],arrow.dir[1],arrow.dir[2]);
  ArrowHD mesh;
  mesh.setup(ori,dir);
  mesh.setLength(arrow.length,arrow.lengthHead,arrow.diameter,arrow.diameterHead);
  mesh.setResolution(arrow.resolution);
  
  std::stringstream ssmaincolor;
  ssmaincolor << "marr" << std::setw(2) << std::setfill('0') << std::hex << arrow.color[0] << std::setw(2) << std::setfill('0') << std::hex << arrow.color[1] << std::setw(2) << std::setfill('0') << std::hex << arrow.color[2];
  std::string namemaincolor = ssmaincolor.str();
  Eigen::Vector3f colmain(arrow.color[0] / 255., arrow.color[1] / 255., arrow.color[2] / 255.);
  libobj::Mtl matmain(namemaincolor, colmain);

  writer<<matmain<<mesh;
}

void Mesher::addP(PlaneForMesher plane)
{
  Eigen::Vector3d pos(plane.pos[0],plane.pos[1],plane.pos[2]);
  Eigen::Vector3d nml(plane.normal[0],plane.normal[1],plane.normal[2]);
  PlaneMesh mesh;
  mesh.place(pos,nml);
  mesh.setSize(plane.size);
  mesh.setThickness(plane.thickness);
  
  std::stringstream ssmaincolor;
  ssmaincolor << "mpln" << std::setw(2) << std::setfill('0') << std::hex << plane.color[0] << std::setw(2) << std::setfill('0') << std::hex << plane.color[1] << std::setw(2) << std::setfill('0') << std::hex << plane.color[2];
  std::string namemaincolor = ssmaincolor.str();
  Eigen::Vector3f colmain(plane.color[0] / 255., plane.color[1] / 255., plane.color[2] / 255.);
  libobj::Mtl matmain(namemaincolor, colmain);

  writer<<matmain<<mesh;
}

void Mesher::addR(RectangleForMesher rect)
{
  RectangleMesh rm;
  Eigen::Vector3d LL, d1, d2;
  LL << rect.LL[0], rect.LL[1], rect.LL[2];
  d1 << rect.d1[0], rect.d1[1], rect.d1[2];
  d2 << rect.d2[0], rect.d2[1], rect.d2[2];
  rm.setup(LL, d1, d2);
  
  std::stringstream ssmaincolor;
  ssmaincolor << "rectmat" << std::setw(2) << std::setfill('0') << std::hex << rect.color[0] << std::setw(2) << std::setfill('0') << std::hex << rect.color[1] << std::setw(2) << std::setfill('0') << std::hex << rect.color[2];
  std::string namemaincolor = ssmaincolor.str();
  Eigen::Vector3f colmain(rect.color[0] / 255., rect.color[1] / 255., rect.color[2] / 255.);
  libobj::Mtl matmain(namemaincolor, colmain);

  writer<<matmain<<rm;
}

void Mesher::addPressure(PressureStdInterface p, std::array<int, 3> color)
{
  std::array<double, 3> orP;
  for(int i:{0,1,2})
    orP[i]=p.position[i]-p.distance*p.getNormale()[i];
  PlaneForMesher pm(orP,p.getNormale());
  pm.color=color;
  addP(pm);
  ArrowForMesher arrow;
  arrow.dir=p.getNormale();
  arrow.ori=p.position;
  arrow.color=color;
  arrow.fastDirSetup(0.1,0.015,0.0025,0.005);
  addA(arrow);
}


void Mesher::experimentalAddFigure(RibbonStdInterface ribbon)
{
  SuperRibbon rib = ribbon.getSR();
  FigureSuperRibbonMesher mesher;
  mesher.setMeshDiscreteLength(precision);
  mesher.setRibbonName(name);
  mesher.setColors(Eigen::Vector3i(255,255,255),Eigen::Vector3i(0,0,0));
  mesher.genShapeFrom(rib);
  mesher.addFrames(rib.getLength(0),false,false);
  mesher.save(writer);
}

void Mesher::addFinalR(RibbonStdInterface ribbon)
{
  RibbonPathGenerator pathGen;
  SuperRibbon rib = ribbon.getSR();
  auto path=pathGen.genShapeFrom(rib, precision);
  ObjRibbonMesh theRibbonMesh(path, ribbon.getWidth(), ribbon.getThickness());
  std::stringstream ssmaincolor;
  ssmaincolor << "matFR" << std::setw(2) << std::setfill('0') << std::hex << mainColor[0] << std::setw(2) << std::setfill('0') << std::hex << mainColor[1] << std::setw(2) << std::setfill('0') << std::hex << mainColor[2];
  std::string namemaincolor = ssmaincolor.str();
  Eigen::Vector3f colmain(mainColor[0] / 255., mainColor[1] / 255., mainColor[2] / 255.);
  libobj::Mtl matmain(namemaincolor, colmain);
  writer<<matmain<<theRibbonMesh;
}

void Mesher::addSphere(std::array<double, 3> position, double radius, int prec, std::array<int, 3> color)
{
  Eigen::Vector3d pos;
  pos<<position[0],position[1],position[2];
  SphereMesh m;
  m.setFrom(pos,radius);
  m.setResolution(prec);
  m.generate();
  std::stringstream ssmaincolor;
  ssmaincolor << "matSph" << std::setw(2) << std::setfill('0') << std::hex << color[0] << std::setw(2) << std::setfill('0') << std::hex << color[1] << std::setw(2) << std::setfill('0') << std::hex << color[2];
  std::string namemaincolor = ssmaincolor.str();
  Eigen::Vector3f colmain(color[0] / 255., color[1] / 255., color[2] / 255.);
  libobj::Mtl matmain(namemaincolor, colmain);
  writer<<matmain<<m;
}


void Mesher::addCenterline(RibbonStdInterface ribbon)
{
  SuperRibbon rib = ribbon.getSR();
  CenterlineMesher mesher;
  mesher.setDimensions(rib.getWidth()/50, precision);
  mesher.setColor(mainColor);
  mesher.genShapeFrom(rib);
  mesher.save(writer);
}

void Mesher::addCenterlineMR(MRStdInterface ribbon)
{
  MixedSuperRibbon rib = ribbon.getSR();
  CenterlineMesher mesher;
  mesher.setDimensions(rib.getWidth()/50, precision);
  mesher.setColor(mainColor);
  mesher.genShapeFrom(rib);
  mesher.save(writer);
}


void Mesher::close()
{
  writer.close();
}

void initMesherClass(sol::state& lua)
{
  lua.new_usertype<Mesher>(
    "Mesher", sol::constructors<Mesher(std::string)>(),
    "precision", sol::property(&Mesher::setPrecision,&Mesher::getPrecision),
    "mainColor", sol::property(&Mesher::setMainColor,&Mesher::getMainColor),
    "secondColor", sol::property(&Mesher::setMainColor,&Mesher::getMainColor),
    "ribbonName", sol::property(&Mesher::setRibbonName,&Mesher::getRibbonName),
    "setColors", &Mesher::setColors,
    "add",sol::overload(&Mesher::add,&Mesher::addF,&Mesher::addA, &Mesher::addP,&Mesher::addSS,&Mesher::addMR,&Mesher::addR,&Mesher::addPressure),
    "addCenterline",sol::overload(&Mesher::addCenterline,&Mesher::addCenterlineMR),
    "addSphere", &Mesher::addSphere,
    "addAsFigure",&Mesher::experimentalAddFigure,
    "addFinal",&Mesher::addFinalR,
    "close",&Mesher::close
    );
}
