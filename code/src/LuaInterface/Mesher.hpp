/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef MESHER_H
#define MESHER_H
#include "../sol.hpp"
#include "RibbonStdInterface.hpp"
#include "MRStdInterface.hpp"
#include "FiberStdInterface.hpp"
#include "ArrowForMesher.hpp"
#include "PlaneForMesher.hpp"
#include "PressureStdInterface.hpp"
#include <ObjFileWriter.hpp>
#include <array>

class Mesher
{
public:
  Mesher(std::string filename);
  ~Mesher();
  void add(RibbonStdInterface ribbon);
  void addSS(RibbonStdInterface ribbon, bool showSubSegs);
  void addF(FiberStdInterface fiber);
  void addA(ArrowForMesher arrow);
  void addP(PlaneForMesher plane);
  void addPressure(PressureStdInterface p, std::array<int,3> color);
  void addMR(MRStdInterface ribbon); 
  void addCenterline(RibbonStdInterface ribbon);
  void addCenterlineMR(MRStdInterface ribbon);
  void addR(RectangleForMesher rect);
  void experimentalAddFigure(RibbonStdInterface ribbon);
  void addFinalR(RibbonStdInterface ribbon);
  void addSphere(std::array<double, 3> position, double radius, int prec, std::array<int,3> color);
  double getPrecision();
  std::array<int,3> getMainColor();
  std::array<int,3> getSecondColor();
  std::string getRibbonName();
  void setMainColor(std::array<int,3> mc);
  void setSecondColor(std::array<int,3> sc);
  void setColors(std::array<int,3> mc, std::array<int,3> sc);
  void setPrecision(double prec);
  void setRibbonName(std::string name);
  void close();
private:
  std::string name;
  double precision=0.001;
  Eigen::Vector3i mainColor;
  Eigen::Vector3i secondColor;
  libobj::ObjFileWriter writer;
};

void initMesherClass(sol::state &lua);
#endif // MESHER_H
