/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "OriBoxFilter.hpp"
#include <MixedPhyRestrainOri.hpp>

void initOriBoxFilterClass(sol::state& lua)
{
  lua.new_usertype<Filter>(
    "OriBoxFilter",  sol::constructors<Filter()>(),
    "reset",         &Filter::resetFilter,
    "filterOutPos",  &Filter::filterOutPos,
    "filterOutFrame",&Filter::filterOutFrame,
    "test",          &Filter::test
  );
}
