/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "PlaneForMesher.hpp"

PlaneForMesher::PlaneForMesher(std::array<double, 3> pos, std::array<double, 3> normal):
pos(pos),normal(normal)
{
}


void PlaneForMesher::place(std::array<double, 3> p, std::array<double, 3> nml)
{
  pos=p;
  normal=nml;
}

void initPlaneForMesherClass(sol::state& lua)
{
    lua.new_usertype<PlaneForMesher>(
    "PlaneMesh",      sol::constructors<PlaneForMesher(), PlaneForMesher(std::array<double, 3>, std::array<double, 3> )>(),
    "position",       &PlaneForMesher::pos,
    "normal",         &PlaneForMesher::normal,
    "place",          &PlaneForMesher::place,
    "size",           &PlaneForMesher::size,
    "thickness",      &PlaneForMesher::thickness,
    "color",          &PlaneForMesher::color
    );
}


RectangleForMesher::RectangleForMesher(std::array<double,3> LL, std::array<double,3> d1, std::array<double,3> d2):
  LL(LL),d1(d1),d2(d2)
{}

void RectangleForMesher::place(std::array<double,3> LL, std::array<double,3> d1, std::array<double,3> d2)
{
  this->LL=LL;
  this->d1=d1;
  this->d2=d2;
}

void initRectangleForMesherClass(sol::state &lua)
{
    lua.new_usertype<RectangleForMesher>(
    "RectangleMesh",      sol::constructors<RectangleForMesher(), RectangleForMesher(std::array<double, 3>, std::array<double, 3>, std::array<double, 3>)>(),
    "LL",       &RectangleForMesher::LL,
    "d1",       &RectangleForMesher::d1,
    "d2",       &RectangleForMesher::d2,
    "place",    &RectangleForMesher::place,
    "color",    &RectangleForMesher::color
    );
}
