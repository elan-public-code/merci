/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef PLANEFORMESHER_H
#define PLANEFORMESHER_H

#include <array>
#include "sol.hpp"

class PlaneForMesher
{
public:
  PlaneForMesher() = default;
  PlaneForMesher(std::array<double,3> pos, std::array<double,3> normal);
  std::array<double,3> pos={0,0,0};
  std::array<double,3> normal={0,0,0};
  std::array<int,3> color={0,0,0};
  double size = 1.;
  double thickness = 1e-6;
  void place( std::array<double,3> p, std::array<double,3> nml);
};

void initPlaneForMesherClass(sol::state &lua);

class RectangleForMesher
{
public:
  RectangleForMesher() = default;
  RectangleForMesher(std::array<double,3> LL, std::array<double,3> d1, std::array<double,3> d2);
  std::array<double,3> LL={0.,0.,0.};
  std::array<double,3> d1={1.,0.,0.};
  std::array<double,3> d2={0.,1.,0.};
  std::array<int,3> color={0,0,0};
  void place(std::array<double,3> LL, std::array<double,3> d1, std::array<double,3> d2);
};

void initRectangleForMesherClass(sol::state &lua);

#endif // PLANEFORMESHER_H
