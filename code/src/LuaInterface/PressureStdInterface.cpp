/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "PressureStdInterface.hpp"

PressureStdInterface::PressureStdInterface(std::array<double, 3> position, std::array<double, 3> normale, double k) :
  position(position),
  k(k),
  distance(0.),
  doubleEnd(true),
  normale(normale)
{
}

PressureStdInterface::PressureStdInterface(std::array<double, 3> position, std::array<double, 3> normale, double k, double distance) :
  position(position),
  k(k),
  distance(distance),
  doubleEnd(true),
  normale(normale)
{
}

void PressureStdInterface::setNormale(std::array<double, 3> N)
{
  Eigen::Vector3d nml;
  nml<<N[0],N[1],N[2];
  if(nml.squaredNorm()<1e-24)nml<<1,0,0;
  nml.normalize();
  for(int i:{0,1,2})normale[i]=nml[i];
}

std::array<double, 3> PressureStdInterface::getNormale()
{
  std::array<double, 3> N;
  for(int i:{0,1,2})N[i]=normale[i];
  return N;
}

Pressure PressureStdInterface::toPressure()
{
  Pressure res;
  res.distance=distance;
  res.k=k;
  for(int i:{0,1,2})res.normale[i]=normale[i];
  for(int i:{0,1,2})res.position[i]=position[i];
  return res;
}

void initPressureClass(sol::state& lua)
{
    lua.new_usertype<PressureStdInterface>(
    "Pressure", sol::constructors<PressureStdInterface(std::array<double, 3>, std::array<double, 3>, double),PressureStdInterface(std::array<double, 3>, std::array<double, 3>, double, double)>(),
    "position",&PressureStdInterface::position,
    "k",&PressureStdInterface::k,
    "distance", &PressureStdInterface::distance,
    "normale", sol::property(&PressureStdInterface::setNormale, &PressureStdInterface::getNormale),
    "doubleEnd", &PressureStdInterface::doubleEnd
    );
}
