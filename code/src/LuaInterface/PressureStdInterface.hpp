/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef PRESSURESTDINTERFACE_H
#define PRESSURESTDINTERFACE_H

#include <MixedPhY.hpp>
#include "../sol.hpp"

class PressureStdInterface
{
public:
  PressureStdInterface(std::array<double,3> position, std::array<double,3> normale,double k);
  PressureStdInterface(std::array<double,3> position, std::array<double,3> normale,double k, double distance);
  std::array<double,3> position;
  std::array<double,3> getNormale();
  void setNormale(std::array<double,3> N);
  double k;
  double distance;
  bool doubleEnd;
  Pressure toPressure();
  
private:
  std::array<double,3> normale;
};

void initPressureClass(sol::state &lua);

#endif // PRESSURESTDINTERFACE_H
