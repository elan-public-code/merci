/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "RepulsionFieldStdInterface.hpp"
#include "Log/Logger.hpp"


RepulsionFieldStdInterface::RepulsionFieldStdInterface(std::array<double, 3> position, std::array<double, 3> normale, double k)
  : position(position),
  normale(normale),
  k(k),
  model(RepulsionField::Model::Quadratic)
{
}

void RepulsionFieldStdInterface::setK(double newK)
{
  k=newK;
}

double RepulsionFieldStdInterface::getK()
{
  return k;
}

RepulsionField RepulsionFieldStdInterface::getField() const
{
  Eigen::Vector3r p,n;
  p<<position[0],position[1],position[2];
  n<<normale[0],normale[1],normale[2];
  return RepulsionField(p,n,k,model);
}

void RepulsionFieldStdInterface::printFieldChoices()
{
  LoggerMsg message("Field choices:");
  message.addChild("Linear", std::to_string(static_cast<int>(RepulsionField::Model::Linear)));
  message.addChild("SmoothExp", std::to_string(static_cast<int>(RepulsionField::Model::Exp)));
  message.addChild("Quadratic", std::to_string(static_cast<int>(RepulsionField::Model::Quadratic)));
  message.addChild("SoftWall", std::to_string(static_cast<int>(RepulsionField::Model::SoftWall)));
  getLogger()->Write(LoggerLevel::INFORMATION, "RepulsionField", message);
}

void RepulsionFieldStdInterface::setFieldFromId(int id)
{
  model=static_cast<RepulsionField::Model>(id);
}

void RepulsionFieldStdInterface::setFieldFromName(std::string name)
{
  if(name == "Linear") model= RepulsionField::Model::Linear;
  else if (name == "SmoothExp") model = RepulsionField::Model::Exp;
  else if (name == "Quadratic") model = RepulsionField::Model::Quadratic;
  else if (name == "SoftWall") model = RepulsionField::Model::SoftWall;
  else getLogger()->Write(LoggerLevel::WARNING, "RepulsionField", "Type "+name+" is not defined.");
}


void initRepulsionFieldStdInterfaceClass(sol::state& lua)
{
  lua.new_usertype<RepulsionFieldStdInterface>(
    "RepulsionField",    sol::constructors<RepulsionFieldStdInterface(std::array<double, 3>, std::array<double, 3> ), RepulsionFieldStdInterface(std::array<double, 3>, std::array<double, 3>, double )>(),
    "K",                 sol::property(&RepulsionFieldStdInterface::setK, &RepulsionFieldStdInterface::getK),
    "position",          &RepulsionFieldStdInterface::position,
    "normale",           &RepulsionFieldStdInterface::normale,
    "setField",          sol::overload(&RepulsionFieldStdInterface::setFieldFromName, &RepulsionFieldStdInterface::setFieldFromId), 
    "printFieldChoices", &RepulsionFieldStdInterface::printFieldChoices
  );
}
