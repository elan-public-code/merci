/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef REPULSIONFIELDSTDINTERFACE_H
#define REPULSIONFIELDSTDINTERFACE_H

#include "sol.hpp"
#include "RepulsionField.hpp"
#include <array>
#include <string>

class RepulsionFieldStdInterface
{
public:
  RepulsionFieldStdInterface(std::array<double, 3> position, std::array<double, 3> normale, double k = 1.);
  void setFieldFromName(std::string name);
  void setFieldFromId(int id);
  void printFieldChoices();
  double getK();
  void setK(double);
  RepulsionField getField() const;
  std::array<double, 3> position;
  std::array<double, 3> normale;
private:
  
  double k;
  RepulsionField::Model model;
};

void initRepulsionFieldStdInterfaceClass(sol::state& lua);

#endif // REPULSIONFIELDSTDINTERFACE_H
