/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "RibbonStdInterface.hpp"
#include <SuperRibbonCore/RibbonSystem.hpp>
#include <RibbonObjLib/RibbonPathGenerator.hpp>
#include <random>
#include <chrono> 
#include <Eigen/Dense>

RibbonStdInterface::RibbonStdInterface():
  ribbonWidth(0.01),
  ribbonThickness(0.00001),
  areaDensity(0.08),
  D(0.0002),
  poissonRatio(0.5),
  posOr(0.,0.,0.),
  omegaB(0.),
  etaM(0.)
{
  frameOr.setIdentity();
}


void RibbonStdInterface::setNbSegments(int n)
{
  pieces.resize(n);
  natCurvs.resize(n);
}

int RibbonStdInterface::getNbSegments()
{
  return pieces.size();
}

void RibbonStdInterface::setWidth(double w)
{
  ribbonWidth=w;
}

double RibbonStdInterface::getWidth()
{
  return ribbonWidth;
}

void RibbonStdInterface::setThickness(double h)
{
  ribbonThickness=h;
}

double RibbonStdInterface::getThickness()
{
  return ribbonThickness;
}

void RibbonStdInterface::setAreaDensity(double ad)
{
  areaDensity=ad;
}

double RibbonStdInterface::getAreaDensity()
{
  return areaDensity;
}

void RibbonStdInterface::setD(double d)
{
  D=d;
}

double RibbonStdInterface::getD()
{
  return D;
}

void RibbonStdInterface::setPoissonRatio(double nu)
{
  poissonRatio=nu;
}

double RibbonStdInterface::getPoissonRatio()
{
  return poissonRatio;
}

void RibbonStdInterface::setPosOr(std::array<double, 3> p)
{
  posOr<<p[0],p[1],p[2];
}

std::array<double, 3> RibbonStdInterface::getPosOr()
{
    return std::array<double, 3>{{posOr[0],posOr[1],posOr[2]}};
}

void RibbonStdInterface::setRibbonStart(double omegaB, double etaM)
{
  this->omegaB=omegaB;
  this->etaM=etaM;
}

void RibbonStdInterface::setRibbonPiece(int segmentID, double length, double omegaA, double etaN)
{
  pieces.at(segmentID)={{length,omegaA,etaN}};
}

void RibbonStdInterface::setRibbonCurv(int segmentID, double omegaA, double etaN)
{
  pieces.at(segmentID)={{pieces.at(segmentID)[0],omegaA,etaN}};
}

void RibbonStdInterface::setNaturalCurvature(int segmentID, double A, double B)
{
  natCurvs.at(segmentID)=LinearScalar(A,B);
}

std::vector<double> RibbonStdInterface::getPoint()
{
  std::vector<double> point;
  point.push_back(omegaB);
  point.push_back(etaM);
  for(auto [l,b,n]:pieces)
  {
    point.push_back(b);
    point.push_back(n);
  }
  return point;
}

void RibbonStdInterface::printPoint()
{
  auto vPt=getPoint();
  Eigen::VectorXd pt(vPt.size());
  for(int i=0;i<static_cast<int>(vPt.size());i++)
    pt(i)=vPt.at(i);
  std::cout<<pt.transpose()<<std::endl;
}


void RibbonStdInterface::setPoint(std::vector<double> data)
{
  omegaB=data.at(0);
  etaM=data.at(1);
  size_t t = pieces.size();
  for(size_t i=0;i<t;i++)
  {
    pieces.at(i)[1]=data.at(2+2*i);
    pieces.at(i)[2]=data.at(3+2*i);
  }
}


void RibbonStdInterface::setFrameOr(std::array<double, 9> f)
{
  frameOr<<f[0],f[1],f[2],
           f[3],f[4],f[5],
           f[6],f[7],f[8];
}

std::array<double, 9> RibbonStdInterface::getFrameOr()
{
  return
  std::array<double, 9> {{frameOr(0,0),frameOr(0,1),frameOr(0,2),
                        frameOr(1,0),frameOr(1,1),frameOr(1,2),
                        frameOr(2,0),frameOr(2,1),frameOr(2,2)}};
}

void RibbonStdInterface::randomizeShape()
{
  double ribbonLength=0.;
  for(auto [length,tmp1,tmp2]: pieces)
    ribbonLength+=length;
  std::default_random_engine generator;
  generator.seed(std::chrono::system_clock::now().time_since_epoch().count());
  double etaPrimeBound=2.0/ribbonWidth;
  std::uniform_real_distribution<double> distributionEtaPrime(-etaPrimeBound,etaPrimeBound);
  std::uniform_real_distribution<double> distributionStartEta(-1.0,1.0);
  double OmegaStartBound=2.*6.3/ribbonLength;
  std::uniform_real_distribution<double> distributionStartOmega(-OmegaStartBound,OmegaStartBound);
  omegaB=distributionStartOmega(generator);
  etaM=distributionStartEta(generator);
  double lastW=omegaB,lastN=etaM;
  size_t t=pieces.size();
  for(size_t i=0;i<t;i++)
  {
    double newW=distributionStartOmega(generator);
    double newN=distributionStartEta(generator);
    double newWP=(newW-lastW)/pieces[i][0];
    double newNP=(newN-lastN)/pieces[i][0];
    if(newNP<-etaPrimeBound)newNP=-etaPrimeBound;
    if(newNP>etaPrimeBound)newNP=etaPrimeBound;
    pieces[i][1]=newWP;
    pieces[i][2]=newNP;
    lastW=newW;
    lastN=newN;
  }
}

SuperRibbon RibbonStdInterface::getSR()
{
  SuperRibbon sr;
  //std::cout<<"Constructing ribbbon"<<std::endl;
  sr.setPosOr(posOr);
  sr.setFrameOr(frameOr);
  //std::cout<<"Pos: "<<std::endl<<posOr.transpose()<<std::endl;
  //std::cout<<"Frame: "<<std::endl<<frameOr<<std::endl;
  sr.setPoissonRatio(poissonRatio);
  //std::cout<<"Poisson ratio: "<<poissonRatio<<std::endl;
  sr.setWidth(ribbonWidth);
  sr.setThickness(ribbonThickness);
  //std::cout<<"Width: "<<ribbonWidth<<std::endl;
  //std::cout<<"Thickness: "<<ribbonThickness<<std::endl;
  sr.setD(D);
  //std::cout<<"D: "<<D<<std::endl;
  sr.setAreaDensity(areaDensity);
  //std::cout<<"Surfassic mass: "<<surfassicMass<<std::endl;
  int n=pieces.size();
  for(int i=0;i<n;i++)
  {
    sr.addSegmentAt(i);
    sr.setLength(i,pieces[i][0]);
    sr.setOmega1A(i,pieces[i][1]);
    sr.setEtaN(i,pieces[i][2]);
    sr.setNatOmega1(i,natCurvs[i]);
    //std::cout<<"Piece "<<i<<": "<<pieces[i][0]<<", "<<pieces[i][1]<<", "<<pieces[i][2]<<std::endl;
  }
  sr.setOmega1BOr(omegaB);
  sr.setEtaMOr(etaM);
  //std::cout<<"Start with"<<omegaB<<", "<<etaM<<std::endl;
  return sr;
}

RibbonStdInterface RibbonStdInterface::copy()
{
  return RibbonStdInterface(*this);
}

void RibbonStdInterface::getHealthRepport()
{
  RibbonSystem rs(Eigen::Vector3r(0.,0.,0.));
  rs.set(getSR());
  rs.logHealthReport();
}

std::array<double, 3> RibbonStdInterface::getEndPos()
{
  RibbonSystem phY(Eigen::Vector3r(0.,0.,0.));
  phY.set(getSR());
  Eigen::Vector3r p = phY.getEndPos();
  return  {p[0],p[1],p[2]};
}

std::array<double, 9> RibbonStdInterface::getEndFrame()
{
  RibbonSystem phY(Eigen::Vector3r(0.,0.,0.));
  phY.set(getSR());
  Eigen::Matrix3r f = phY.getEndFrame();
  return
  std::array<double, 9> {{f(0,0),f(0,1),f(0,2),
                        f(1,0),f(1,1),f(1,2),
                        f(2,0),f(2,1),f(2,2)}};
}

void RibbonStdInterface::genDiscreteDescriptor(double prec, std::string filename)
{
  std::ofstream os(filename);
  RibbonPathGenerator pathGen;
  auto ribbon = getSR();
  double length=0.0;
  int t=ribbon.getNbSegments();
  for(int i=0;i<t;i++)length+=ribbon.getLength(i);
  std::vector<ObjRibbonVertex> path = pathGen.genShapeFrom(ribbon,prec);
  os<<"[";
  Eigen::IOFormat CommaInitFmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", "", "");
  bool first=true;
  double s=0.;
  for(ObjRibbonVertex obj:path)
  {
    if(!first)os<<',';
    first=false;
    os<<std::endl<<" {"<<std::endl;
    os<<"  \"s\":"<<s<<','<<std::endl;
    os<<"  \"pos\":["<<obj.pos.format(CommaInitFmt)<<"],"<<std::endl;
    os<<"  \"frame\":["<<obj.frame.format(CommaInitFmt)<<"],"<<std::endl;
    os<<"  \"curvature\":["<<obj.omega1<<", 0, "<<obj.omega1*obj.eta<<"],"<<std::endl;
    os<<"  \"eta\":"<<obj.eta<<std::endl;
    os<<" }";
    s+=prec;
    if(s>length)s=length;
  }
  os<<std::endl<<"]"<<std::endl;
}


void initRibbonStdInterfaceClass(sol::state& lua)
{
  lua.new_usertype<RibbonStdInterface>(
    "SuperRibbon",            sol::constructors<RibbonStdInterface()>(),
    "width",                  sol::property(&RibbonStdInterface::getWidth,       &RibbonStdInterface::setWidth),
    "thickness",              sol::property(&RibbonStdInterface::getThickness,   &RibbonStdInterface::setThickness),
    "areaDensity",            sol::property(&RibbonStdInterface::setAreaDensity, &RibbonStdInterface::getAreaDensity),
    "D",                      sol::property(&RibbonStdInterface::setD,           &RibbonStdInterface::getD),
    "position",               sol::property(&RibbonStdInterface::setPosOr,       &RibbonStdInterface::getPosOr),
    "frame",                  sol::property(&RibbonStdInterface::setFrameOr,     &RibbonStdInterface::getFrameOr),
    "poissonRatio",           sol::property(&RibbonStdInterface::setPoissonRatio,&RibbonStdInterface::getPoissonRatio),
    "numberSegments",         sol::property(&RibbonStdInterface::setNbSegments,  &RibbonStdInterface::getNbSegments),
    "setRibbonStart",         &RibbonStdInterface::setRibbonStart,
    "setRibbonPiece",         &RibbonStdInterface::setRibbonPiece,
    "setNaturalCurvature",    &RibbonStdInterface::setNaturalCurvature,
    "copy",                   &RibbonStdInterface::copy,
    "randomizeShape",         &RibbonStdInterface::randomizeShape,
    "setRibbonPieceCurvature",&RibbonStdInterface::setRibbonCurv,
    "getPoint",               &RibbonStdInterface::getPoint,
    "setPoint",               &RibbonStdInterface::setPoint,
    "healthRepport",          &RibbonStdInterface::getHealthRepport,
    "endPos",                 &RibbonStdInterface::getEndPos,
    "endFrame",               &RibbonStdInterface::getEndFrame,
    "genDiscreteDescriptor",  &RibbonStdInterface::genDiscreteDescriptor,
    "printPoint",             &RibbonStdInterface::printPoint
    );
  lua.set_function("ExperimentalyToggleSubdivision", SuperRibbonGeometrieSerie::experimentalyToggleSubdivision);
  lua.set_function("ExperimentalySetSeriesOrder", sol::overload(SuperRibbonGeometrieSerie::experimentalySetNeededOrder,SuperRibbonGeometrieSerie::experimentalyResetNeededOrder));
}
