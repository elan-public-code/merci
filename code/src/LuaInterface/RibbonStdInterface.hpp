/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef RIBBONSTDINTERFACE_H
#define RIBBONSTDINTERFACE_H

#include "sol.hpp"
#include <SuperRibbonCore/SuperRibbon.hpp>
#include <BaseCore/LinearScalar.hpp>
#include <array>
#include <vector>
#include <optional>

class RibbonStdInterface
{
public:
  RibbonStdInterface();
  void setNbSegments(int);
  int getNbSegments();
  
  void setWidth(double w);                 double getWidth();
  void setThickness(double h);             double getThickness();
  void setAreaDensity(double ad);          double getAreaDensity();
  void setD(double d);                     double getD();
  void setPoissonRatio(double nu);         double getPoissonRatio();
  void setFrameOr(std::array<double,9> f); std::array<double,9> getFrameOr();
  void setPosOr(std::array<double,3> p);   std::array<double,3> getPosOr();
  
  void setRibbonStart(double omegaB, double etaM);
  void setRibbonPiece(int segmentID, double length, double omegaA, double etaN);
  void setRibbonCurv(int segmentID, double omegaA, double etaN);
  void setNaturalCurvature(int segmentID, double A, double B);
  
  std::vector<double> getPoint();
  void setPoint(std::vector<double> data);
  
  void randomizeShape();
  
  SuperRibbon getSR();
  RibbonStdInterface copy();
  void getHealthRepport();
  std::array<double,3> getEndPos();
  std::array<double,9> getEndFrame();
  void genDiscreteDescriptor(double prec, std::string filename);
  void printPoint();
private:
  double ribbonWidth;
  double ribbonThickness;
  double areaDensity;
  double D;
  double poissonRatio;
  Eigen::Vector3d posOr;
  Eigen::Matrix3d frameOr;
  double omegaB;
  double etaM;
  std::vector<std::array<double,3>> pieces;
  std::vector<std::optional<LinearScalar>> natCurvs;
};

void initRibbonStdInterfaceClass(sol::state &lua);

#endif // RIBBONSTDINTERFACE_H
