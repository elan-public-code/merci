/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "RibbonStdInterface.hpp"
#include "FiberStdInterface.hpp"
#include "IpoptOptions.hpp"
#include "Experiment.hpp"
#include "FExperiment.hpp"
#include "Mesher.hpp"
#include "LuaImage.hpp"
#include "ArrowForMesher.hpp"
#include "PlaneForMesher.hpp"
#include "MRStdInterface.hpp"
#include "MRFitter.hpp"
#include "RepulsionFieldStdInterface.hpp"
//#include "LigthExperiment.hpp"
#include "Extension/ExtensionCMinPack.hpp"
#include "OriBoxFilter.hpp"

int main(int argc, char** argv)
{
  sol::state lua;
  initRibbonStdInterfaceClass(lua);
  initFiberStdInterfaceClass(lua);
  initArrowForMesherClass(lua);
  initPlaneForMesherClass(lua);
  initIpoptOptionClass(lua);
  initPressureClass(lua);
  initExperimentClass(lua);
  initFExperimentClass(lua);
  initMixedSuperRibbonStdInterfaceClass(lua);
  initMRFitterClass(lua);
  initMesherClass(lua);
  initImageClass(lua);
  initRepulsionFieldStdInterfaceClass(lua);
  initOriBoxFilterClass(lua);
  initRectangleForMesherClass(lua);
  if(ExtensionCMinPack::Instance()->isLoaded())
    ExtensionCMinPack::Instance()->initHybridParamsClass(lua);
  //initLightExperimentClass(lua);
  lua.open_libraries(sol::lib::base, sol::lib::math, sol::lib::io, sol::lib::table, sol::lib::string, sol::lib::package, sol::lib::os);
  for(int i=1;i<argc;i++)
    lua.script_file(argv[i]);
  

  return 0;
  
  return 42;
}
