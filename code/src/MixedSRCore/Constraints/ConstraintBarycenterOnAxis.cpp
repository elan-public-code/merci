/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "ConstraintBarycenterOnAxis.hpp"
#include "MixedPhY.hpp"

ConstraintBarycenterOnAxis::ConstraintBarycenterOnAxis(int constraintId, Eigen::Vector3r posAxis, Eigen::Vector3r dirAxis, real alphaConstraint) :
  GenericExtraConstraint(constraintId),
  posAxis(posAxis),
  alphaConstraint(alphaConstraint)
{
  dirAxis.normalize();
  if(dirAxis.squaredNorm()==0.)throw "Constraint is incoherent";
  const Eigen::Vector3r u1(1.,0.,0.);
  const Eigen::Vector3r u2(1.,0.,0.);
  if(dirAxis.cross(u1).norm()>1.e-4)dirAxisO1=dirAxis.cross(u1).normalized();
  else dirAxisO1=dirAxis.cross(u2).normalized();
  dirAxisO2=dirAxis.cross(dirAxisO1);
}

void ConstraintBarycenterOnAxis::hessianStruct(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, int >& liste)
{
  if(!linearMode){
    int t= phy.getConst().getNbSegments();
    for(int i=0;i<t;i++) 
    {
      for(int j=0;j<3;j++) {
        liste[Point(constraintId, MixedPhY::vpointIDPos(i)+j)] ++;
        liste[Point(constraintId+1, MixedPhY::vpointIDPos(i)+j)] ++;
      }
    }
  }
}

void ConstraintBarycenterOnAxis::hessian(const MixedPhY& phy, const Eigen::VectorXr& scalar, std::map< GenericExtraConstraint::Point, real >& liste)
{
  if(!linearMode) {
    int t= phy.getConst().getNbSegments();
    Eigen::Vector3r bary;
    bary.setZero();
    for(int i=0; i<t; i++)
    {
      bary+=phy.getConst().getPosStart(i);
    }
    bary=(1./t)*bary;
    double c1 = (bary-posAxis).dot(dirAxisO1);
    double c2 = (bary-posAxis).dot(dirAxisO2);
    for(int i=0;i<t;i++) 
    {
      for(int j=0;j<3;j++) {
        liste[Point(constraintId, MixedPhY::vpointIDPos(i)+j)] += scalar[constraintId]*alphaConstraint*c1*c1;
        liste[Point(constraintId+1, MixedPhY::vpointIDPos(i)+j)] += scalar[constraintId+1]*alphaConstraint*c2*c2;
      }
    }
  }
}

void ConstraintBarycenterOnAxis::jacobianStruct(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, int >& liste)
{
  int t= phy.getConst().getNbSegments();
  for(int i=0;i<t;i++) 
  {
    for(int j=0;j<3;j++) {
      liste[Point(constraintId, MixedPhY::vpointIDPos(i)+j)] ++;
      liste[Point(constraintId+1, MixedPhY::vpointIDPos(i)+j)] ++;
    }
  }
}

void ConstraintBarycenterOnAxis::jacobian(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, real >& liste)
{
  int t= phy.getConst().getNbSegments();
  if(linearMode)
  {
    for(int i=0;i<t;i++) 
    {
      for(int j=0;j<3;j++) {
        liste[Point(constraintId, MixedPhY::vpointIDPos(i)+j)] += (alphaConstraint)*dirAxisO1[j];
        liste[Point(constraintId+1, MixedPhY::vpointIDPos(i)+j)] += (alphaConstraint)*dirAxisO2[j];
      }
    }
  }
  else
  {
    Eigen::Vector3r bary;
    bary.setZero();
    for(int i=0; i<t; i++)
    {
      bary+=phy.getConst().getPosStart(i);
    }
    bary=(1./t)*bary;
    double c1 = (bary-posAxis).dot(dirAxisO1);
    double c2 = (bary-posAxis).dot(dirAxisO2);
    for(int i=0;i<t;i++) 
    {
      for(int j=0;j<3;j++) {
        liste[Point(constraintId, MixedPhY::vpointIDPos(i)+j)] += (alphaConstraint*c1/t)*dirAxisO1[j];
        liste[Point(constraintId+1, MixedPhY::vpointIDPos(i)+j)] += (alphaConstraint*c2/t)*dirAxisO2[j];
      }
    }
  }
}

void ConstraintBarycenterOnAxis::eval(const MixedPhY& phy, Eigen::VectorXr& allConstraints)
{
  int t= phy.getConst().getNbSegments();
  Eigen::Vector3r bary;
  bary.setZero();
  for(int i=0; i<t; i++)
  {
    bary+=phy.getConst().getPosStart(i);
  }
  bary=(1./t)*bary;
  
  double c1 = (bary-posAxis).dot(dirAxisO1);
  double c2 = (bary-posAxis).dot(dirAxisO2);
  if(linearMode)
  {
    allConstraints[constraintId] = alphaConstraint*c1;
    allConstraints[constraintId+1] = alphaConstraint*c2;
  }
  else
  {
    allConstraints[constraintId] = alphaConstraint*0.5*c1*c1;
    allConstraints[constraintId+1] = alphaConstraint*0.5*c2*c2;
  }
}

int ConstraintBarycenterOnAxis::size()
{
  return 2;
}
