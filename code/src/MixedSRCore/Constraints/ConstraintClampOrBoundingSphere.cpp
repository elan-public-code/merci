/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "ConstraintClampOrBoundingSphere.hpp"
#include "MixedPhY.hpp"

ConstraintClampOrBoundingSphere::ConstraintClampOrBoundingSphere(int constraintId, Eigen::Vector3r center, real radius) :
  GenericExtraConstraint(constraintId),
  center(center),
  radius(radius*radius)
{
}


void ConstraintClampOrBoundingSphere::hessian(const MixedPhY& phy, const Eigen::VectorXr& scalar, std::map< GenericExtraConstraint::Point, real >& liste)
{
  for(int i=0;i<3;i++)
    liste.at(Point(MixedPhY::vpointIDPos(0)+i, MixedPhY::vpointIDPos(0)+i)) += 2.;
}

void ConstraintClampOrBoundingSphere::hessianStruct(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, int >& liste)
{
  for(int i=0;i<3;i++)
    liste[Point(MixedPhY::vpointIDPos(0)+i, MixedPhY::vpointIDPos(0)+i)] ++;
}

void ConstraintClampOrBoundingSphere::jacobianStruct(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, int >& liste)
{
  for(int i=0;i<3;i++)
    liste[Point(constraintId, MixedPhY::vpointIDPos(0)+i)]++;
}

void ConstraintClampOrBoundingSphere::jacobian(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, real >& liste)
{
  Eigen::Vector3r pos = phy.getConst().getPosStart(0);
  for(int i=0;i<3;i++)
    liste.at(Point(constraintId, MixedPhY::vpointIDPos(0)+i)) += 2.*(pos(i)-center(i));
}

void ConstraintClampOrBoundingSphere::eval(const MixedPhY& phy, Eigen::VectorXr& allConstraints)
{
  Eigen::Vector3r pos = phy.getConst().getPosStart(0);
  allConstraints(constraintId) = (pos-center).squaredNorm();
}

int ConstraintClampOrBoundingSphere::size()
{
  return 1;
}

void ConstraintClampOrBoundingSphere::getUpperBound(Eigen::VectorXr& inpV)
{
  for(int i=0;i<size();i++)
    inpV[constraintId+i]=radius;
}
