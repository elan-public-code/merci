/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "ConstraintClampingOr.hpp"
#include "MixedPhY.hpp"

ConstraintClampingOr::ConstraintClampingOr(int constraintId, Eigen::Vector3r posOr, Eigen::Matrix3r frameOr, real alphaConstraintPos, real alphaConstraintFrame) : 
  GenericExtraConstraint(constraintId),
  posOr(posOr),
  frameOr(frameOr),
  alphaConstraintPos(alphaConstraintPos),
  alphaConstraintFrame(alphaConstraintFrame)
{
}


void ConstraintClampingOr::hessian(const MixedPhY& phy, const Eigen::VectorXr& scalar, std::map< GenericExtraConstraint::Point, real >& liste)
{

}

void ConstraintClampingOr::hessianStruct(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, int >& liste)
{

}

void ConstraintClampingOr::jacobianStruct(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, int >& liste)
{
  for(int i=0;i<3;i++) 
    liste[Point(constraintId+i, MixedPhY::vpointIDPos(0)+i)] ++;
  
  //Eigen::Matrix3r diffF = frameOr - sr.getFrameStart(0);
  for (int i = 0; i < 9; i++)
    liste[Point(constraintId+3+i, MixedPhY::vpointIDFrame(0) + i)] ++;
}

void ConstraintClampingOr::jacobian(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, real >& liste)
{
  //Rappel Eigen::Vector3r diffP = posOr - sr.getPosStart(0);
  for(int i=0;i<3;i++) 
    liste.at(Point(constraintId+i, MixedPhY::vpointIDPos(0)+i)) += -1.*alphaConstraintPos;
  
  //Eigen::Matrix3r diffF = frameOr - sr.getFrameStart(0);
  for (int i = 0; i < 9; i++)
    liste.at(Point(constraintId+3+i, MixedPhY::vpointIDFrame(0) + i)) += -1.*alphaConstraintFrame;
  
}

void ConstraintClampingOr::eval(const MixedPhY& phy, Eigen::VectorXr& allConstraints)
{
  Eigen::Vector3r pos = phy.getConst().getPosStart(0);
  Eigen::Matrix3r frame = phy.getConst().getFrameStart(0);
  Eigen::Vector3r posdiff=posOr - pos;
  allConstraints.segment<3>(constraintId) = alphaConstraintPos * posdiff; 
  Eigen::MatrixXr framediff =frameOr - frame;
  for(int i=0;i<9;i++)
    allConstraints[constraintId+3+i] = alphaConstraintFrame * framediff(i/3,i%3);
}

int ConstraintClampingOr::size()
{
  return 12;
}
