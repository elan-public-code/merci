/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "ConstraintCyclicEta.hpp"
#include "MixedPhY.hpp"

ConstraintCyclicEta::ConstraintCyclicEta(int constraintId, bool negate) :
  GenericExtraConstraint(constraintId),
  negate(negate)
{
}


void ConstraintCyclicEta::hessian(const MixedPhY& phy, const Eigen::VectorXr& scalar, std::map< GenericExtraConstraint::Point, real >& liste)
{

}

void ConstraintCyclicEta::hessianStruct(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, int >& liste)
{

}

void ConstraintCyclicEta::jacobianStruct(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, int >& liste)
{
  size_t t = phy.getConst().getNbSegments();
  liste[Point(constraintId, MixedPhY::vpointIDEta(t-1))]++;
  liste[Point(constraintId, MixedPhY::vpointIDEta(t-1) + 1)]++;
  liste[Point(constraintId, MixedPhY::vpointIDEta(0) + 1)]++;
}

void ConstraintCyclicEta::jacobian(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, real >& liste)
{
  auto sr = phy.getConst();
  size_t t =sr.getNbSegments();
  real l = sr.getLength(t-1);
  liste.at(Point(constraintId, MixedPhY::vpointIDEta(t-1))) += l;
  liste.at(Point(constraintId, MixedPhY::vpointIDEta(t-1) + 1)) += 1.;
  if(negate)
    liste.at(Point(constraintId, MixedPhY::vpointIDEta(0) + 1)) += +1.;
  else
    liste.at(Point(constraintId, MixedPhY::vpointIDEta(0) + 1)) += -1.;
}

void ConstraintCyclicEta::eval(const MixedPhY& phy, Eigen::VectorXr& allConstraints)
{
  auto sr = phy.getConst();
  size_t t =sr.getNbSegments();
  real l = sr.getLength(t-1);
  if(negate)
    allConstraints[constraintId] = sr.getEta(t-1).A * l + sr.getEta(t-1).B + sr.getEta(0).B;
  else
    allConstraints[constraintId] = sr.getEta(t-1).A * l + sr.getEta(t-1).B - sr.getEta(0).B;
}

int ConstraintCyclicEta::size()
{
  return 1;
}
