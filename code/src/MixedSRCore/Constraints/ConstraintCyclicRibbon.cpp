/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "ConstraintCyclicRibbon.hpp"
#include "MixedPhY.hpp"
#include "SuperRibbonGeometrieSerie.hpp"
#include "MathOperations.hpp"

using namespace MathOperations;

ConstraintCyclicRibbon::ConstraintCyclicRibbon(int constraintId, real alphaConstraintPos, real alphaConstraintFrame) :
  GenericExtraConstraint(constraintId),
  alphaConstraintPos(alphaConstraintPos),
  alphaConstraintFrame(alphaConstraintFrame)
{
}

void ConstraintCyclicRibbon::hessianStruct(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, int >& liste)
{
  int segment = phy.getConst().getNbSegments()-1;
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 16; j++) 
      liste[Point(MixedPhY::vpointIDOmega1(segment) + j / 4, MixedPhY::vpointIDOmega1(segment) + (j % 4))]++;
  for (int i = 0; i < 9; i++)
  {
    for(int j=0;j<4;j++)
    {
      liste[Point(MixedPhY::vpointIDFrame(segment) + i, MixedPhY::vpointIDOmega1(segment) + j)]++;
      liste[Point(MixedPhY::vpointIDOmega1(segment) + j, MixedPhY::vpointIDFrame(segment) + i)]++;
    }
  }
  for(int i=0;i<3;i++)
    for (int j = 0; j < 16; j++) 
      liste[Point(MixedPhY::vpointIDOmega1(segment) + j / 4, MixedPhY::vpointIDOmega1(segment) + (j % 4))]++;
  for(int i=0;i<3;i++)
  {
    for(int j=0;j<4;j++)
    {
      for(int pi=0;pi<9;pi++)
      {
        liste[Point(MixedPhY::vpointIDFrame(segment) + pi, MixedPhY::vpointIDOmega1(segment) + j)]++;
        liste[Point(MixedPhY::vpointIDOmega1(segment) + j, MixedPhY::vpointIDFrame(segment) + pi)]++;
      }
    }
  }
  for(int i=0;i<3;i++)
  {
    for(int j=0;j<4;j++)
    {
      for(int k=0;k<3;k++)
      {
        liste[Point(MixedPhY::vpointIDOmega1(segment) + j, MixedPhY::vpointIDFrame(0) + ((i+1)%3)+3*k)]++;
        liste[Point( MixedPhY::vpointIDFrame(0) + ((i+1)%3)+3*k, MixedPhY::vpointIDOmega1(segment) + j)]++;
      }
    }
  }
  for(int i=0;i<3;i++)
  {
    for(int j=0;j<9;j++)
    {
      for(int k=0;k<3;k++)
      {
        
        liste[Point(MixedPhY::vpointIDFrame(0) + ((i+1)%3)+3*k, MixedPhY::vpointIDFrame(segment) + j)]++;
        liste[Point( MixedPhY::vpointIDFrame(segment) + j, MixedPhY::vpointIDFrame(0) + ((i+1)%3)+3*k)]++;
      }
    }
  }
}


void ConstraintCyclicRibbon::hessian(const MixedPhY& phy, const Eigen::VectorXr& scalar, std::map< GenericExtraConstraint::Point, real >& liste)
{
  Eigen::Matrix<Eigen::Matrix4r, 3, 1> hessianPreAltitudeEnergyOut;
  Eigen::Matrix<Eigen::Matrix4r, 3, 1> hessianPosOut;
  Eigen::Matrix<Eigen::Matrix4r, 3, 3> hessianFrameOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPreAltitudeEnergyOut;
  Eigen::Matrix<Eigen::Vector3r, 3, 1> dPreAltitudeEnergydPosOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPreAltitudeEnergydFrameOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 3> dFramedFrameOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPosdFrameOut;
  Eigen::Matrix<Eigen::Vector3r, 3, 1> dPosdPosOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 3> gradFrameOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPosOut;
  Eigen::Vector3r posOut;
  Eigen::Matrix3r frameOut;
  int segment = phy.getConst().getNbSegments()-1;
  phy.getSecondOrder(segment,
                 hessianPreAltitudeEnergyOut,
                 hessianPosOut,
                 hessianFrameOut,
                 gradPreAltitudeEnergyOut,
                 dPreAltitudeEnergydPosOut,
                 dPreAltitudeEnergydFrameOut,
                 dFramedFrameOut,
                 dPosdFrameOut,
                 dPosdPosOut,
                 gradFrameOut,
                 gradPosOut,
                 posOut,
                 frameOut);
  
  Eigen::Matrix<Eigen::Vector4r, 3, 3> gradPreFrameOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPreTranslationOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPreAltitudeRotationalEnergyOut;
  Eigen::Matrix3r preFrame;
  Eigen::Vector3r preTranslation;
  phy.getPres(segment,
          gradPreFrameOut,
          gradPreTranslationOut,
          gradPreAltitudeRotationalEnergyOut,
          preFrame,
          preTranslation
         );
  //Eigen::Vector3r endPos = sr.getPosStart(0);
  //Eigen::Vector3r dpos = posOut - endPos;

  //Pos
  //d2cdk2
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 16; j++) 
      liste.at(Point(MixedPhY::vpointIDOmega1(segment) + j / 4, MixedPhY::vpointIDOmega1(segment) + (j % 4))) += alphaConstraintPos * scalar[constraintId+i]*hessianPosOut[i](j/4,j%4);

  //d2cdP02
  //Zero

  //d2cdF02
  //Zero

  //d2cdP12
  //Zero

  //d2cdkdP0
  //Zero

  //d2cdkdF0
  for (int i = 0; i < 9; i++)
  {
    for(int j=0;j<4;j++)
    {
      liste.at(Point(MixedPhY::vpointIDFrame(segment) + i, MixedPhY::vpointIDOmega1(segment) + j)) += alphaConstraintPos * scalar[constraintId+i/3]*(gradPreTranslationOut)[i%3][j];
      liste.at(Point(MixedPhY::vpointIDOmega1(segment) + j, MixedPhY::vpointIDFrame(segment) + i)) += alphaConstraintPos * scalar[constraintId+i/3]*(gradPreTranslationOut)[i%3][j];
    }
  }

  //d2cdkdP1
  //Zero

  //d2cdP0dF0
  //Zero

  //d2cdP0dP1
  //Zero

  //d2cdF0dP1
  //Zero
  
  //Frame

  //Eigen::Matrix3r endFrame = sr.getFrameStart(0);
  //Eigen::Matrix3r dFrame = frameOut - endFrame;
  Eigen::MatrixXr frameNext = phy.getConst().getFrameStart(0);

  //d2cdk2
  for(int i=0;i<3;i++)
    for (int j = 0; j < 16; j++) 
    {
      double acc=0.;
      for(int k=0;k<3;k++)
      {
        acc+=hessianFrameOut(k,i)(j/4,j%4)*frameNext(k,(i+1)%3);
      }
      liste.at(Point(MixedPhY::vpointIDOmega1(segment) + j / 4, MixedPhY::vpointIDOmega1(segment) + (j % 4))) += alphaConstraintFrame * scalar[constraintId+3+i]*acc;
    }

  //d2cdP02
  //zero

  //d2cdF02
  //Zero

  //d2cdF12
  //zero

  //d2cdkdP0
  //zero

  //d2cdkdF0
  for(int i=0;i<3;i++)
  {
    for(int j=0;j<4;j++)
    {
      Eigen::Matrix3d dgradPreFramedj=extractDerivation(gradPreFrameOut,j);
      for(int pi=0;pi<9;pi++)
      {
        double acc=0.;
        for(int k=0;k<3;k++)
        {
          Eigen::Matrix3d Rotpre = Eigen::Matrix3d::Zero();
          Rotpre(pi/3,pi%3)=1.;
          acc+=(Rotpre*dgradPreFramedj)(k,i)*frameNext(k,(i+1)%3);
        }
        liste.at(Point(MixedPhY::vpointIDFrame(segment) + pi, MixedPhY::vpointIDOmega1(segment) + j)) += alphaConstraintFrame * scalar [constraintId+3+i]* acc;
        liste.at(Point(MixedPhY::vpointIDOmega1(segment) + j, MixedPhY::vpointIDFrame(segment) + pi)) += alphaConstraintFrame * scalar [constraintId+3+i]* acc;
      }
    }
  }
  //d2cdkdF1
  for(int i=0;i<3;i++)
  {
    for(int j=0;j<4;j++)
    {
      for(int k=0;k<3;k++)
      {
        liste.at(Point(MixedPhY::vpointIDOmega1(segment) + j, MixedPhY::vpointIDFrame(0) + ((i+1)%3)+3*k)) += alphaConstraintFrame * scalar [constraintId+3+i]* gradFrameOut(k,i)[j];
        liste.at(Point( MixedPhY::vpointIDFrame(0) + ((i+1)%3)+3*k, MixedPhY::vpointIDOmega1(segment) + j)) += alphaConstraintFrame * scalar [constraintId+3+i]* gradFrameOut(k,i)[j];
      }
    }
  }

  //d2cdP0dF0
  //zero

  //d2cdP0dF1
  //Zero

  //d2cdF0dF1
  for(int i=0;i<3;i++)
  {
    for(int j=0;j<9;j++)
    {
      for(int k=0;k<3;k++)
      {
        
        liste.at(Point(MixedPhY::vpointIDFrame(0) + ((i+1)%3)+3*k, MixedPhY::vpointIDFrame(segment) + j)) += alphaConstraintFrame * scalar [constraintId+3+i]* dFramedFrameOut(k,i)(j/3,j%3);
        liste.at(Point( MixedPhY::vpointIDFrame(segment) + j, MixedPhY::vpointIDFrame(0) + ((i+1)%3)+3*k)) += alphaConstraintFrame * scalar [constraintId+3+i]* dFramedFrameOut(k,i)(j/3,j%3);
      }
    }
  }
}

void ConstraintCyclicRibbon::jacobianStruct(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, int >& liste)
{
  int segment = phy.getConst().getNbSegments()-1;
  for(int i=0;i<3;i++)
    for (int j = 0; j < 3; j++) 
      liste[Point(constraintId+i, MixedPhY::vpointIDPos(segment) + j)]++;
  for(int i=0;i<3;i++)
    for (int j = 0; j < 9; j++)
      liste[Point(constraintId+i, MixedPhY::vpointIDFrame(segment) + j)]++;
  for (int j = 0; j < 3; j++) 
    liste[Point(constraintId+j, MixedPhY::vpointIDPos(0) + j)]++;
  for(int i=0;i<3;i++)
    for (int j = 0; j < 4; j++)
      liste[Point(constraintId+i, MixedPhY::vpointIDOmega1(segment) + j)]++;
  for(int i=0;i<3;i++)
    for (int j = 0; j < 9; j++)
      liste[Point(constraintId+3+i, MixedPhY::vpointIDFrame(segment) + j)]++;
  for(int i=0;i<3;i++)
    for (int j = 0; j < 3; j++)
    liste[Point(constraintId+3+i,MixedPhY::vpointIDFrame(0) + (3*j+((i+1)%3)))]++;
  for(int i=0;i<3;i++)
    for (int j = 0; j < 4; j++) 
      liste[Point(constraintId+3+i, MixedPhY::vpointIDOmega1(segment) + j)]++;
}

void ConstraintCyclicRibbon::jacobian(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, real >& liste)
{
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPreAltitudeEnergyOut;
  Eigen::Matrix<Eigen::Vector3r, 3, 1> dPreAltitudeEnergydPosOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPreAltitudeEnergydFrameOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 3> dFramedFrameOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPosdFrameOut;
  Eigen::Matrix<Eigen::Vector3r, 3, 1> dPosdPosOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 3> gradFrameOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPosOut;
  Eigen::Vector3r posOut;
  Eigen::Matrix3r frameOut;
  int segment = phy.getConst().getNbSegments()-1;
  phy.getGradsForGravityEnergy(segment, gradPreAltitudeEnergyOut, dPreAltitudeEnergydPosOut, dPreAltitudeEnergydFrameOut, dFramedFrameOut, dPosdFrameOut, dPosdPosOut, gradFrameOut, gradPosOut, posOut, frameOut);
  //Eigen::Vector3r dpos = posOut - sr.getPosStart(0);
  //Eigen::Matrix3r dframe = frameOut - sr.getFrameStart(0);

  //  Rappel posdiff=(pos - sr.getPosStart(0));
  for(int i=0;i<3;i++)
    for (int j = 0; j < 3; j++) 
      liste.at(Point(constraintId+i, MixedPhY::vpointIDPos(segment) + j))+= alphaConstraintPos * dPosdPosOut[i](j);
  for(int i=0;i<3;i++)
    for (int j = 0; j < 9; j++)
      liste.at(Point(constraintId+i, MixedPhY::vpointIDFrame(segment) + j)) += alphaConstraintPos * dPosdFrameOut[i](j / 3, j % 3);
  for (int j = 0; j < 3; j++) 
    liste.at(Point(constraintId+j, MixedPhY::vpointIDPos(0) + j))+= alphaConstraintPos * -1.;
  for(int i=0;i<3;i++)
    for (int j = 0; j < 4; j++)
      liste.at(Point(constraintId+i, MixedPhY::vpointIDOmega1(segment) + j))+= alphaConstraintPos * gradPosOut[i](j);

  
    
  Eigen::MatrixXr frameNext = phy.getConst().getFrameStart(0);
  //aligned
  //gradFrameOr
  for(int i=0;i<3;i++)
    for (int j = 0; j < 9; j++)
    {
      double acc=0.;
      for(int k=0;k<3;k++)
        acc+=dFramedFrameOut(k,i)(j / 3, j % 3)*frameNext(k,(i+1)%3);
      liste.at(Point(constraintId+3+i, MixedPhY::vpointIDFrame(segment) + j))+= alphaConstraintFrame * acc;
    }
  //gradFrameNext
  for(int i=0;i<3;i++)
    for (int j = 0; j < 3; j++)
    liste.at(Point(constraintId+3+i,MixedPhY::vpointIDFrame(0) + (3*j+((i+1)%3)))) += alphaConstraintFrame * frameOut(j,i);

  //gradK
  for(int i=0;i<3;i++)
    for (int j = 0; j < 4; j++) 
    {
      double acc=0.;
      for(int k=0;k<3;k++)
      {
        acc+=gradFrameOut(k,i)[j]*frameNext(k,(i+1)%3);
      }
      liste.at(Point(constraintId+3+i, MixedPhY::vpointIDOmega1(segment) + j))+= alphaConstraintFrame * acc;
    }
}

void ConstraintCyclicRibbon::eval(const MixedPhY& phy, Eigen::VectorXr& allConstraints)
{
  auto sr = phy.getConst();
  int segment = sr.getNbSegments()-1;
  real l = sr.getLength(segment);
  SuperRibbonGeometrieSerie ribbon;
  Eigen::Vector3r pos = sr.getPosStart(segment);
  Eigen::Matrix3r frame = sr.getFrameStart(segment);
  LinearScalar omega1 = sr.getOmega1(segment);
  LinearScalar eta = sr.getEta(segment);
  LinearScalar omega1Or = sr.getOmega1(segment);
  LinearScalar etaOr = sr.getEta(segment);
  real s0 = 0.;
  ribbon.Place(pos, frame);
  ribbon.Setup(omega1, eta);
  while (s0 < l) {
    real smax = ribbon.estimSMax();
    if (s0 + smax > l) {
      smax = l - s0;
      std::tie(pos, frame) = ribbon.PosFrame(smax);
      break;
    }
    std::tie(pos, frame) = ribbon.PosFrame(smax);
    s0 += smax;
    ribbon.Place(pos, frame);
    omega1.B = omega1Or.B + s0 * omega1Or.A;
    eta.B = etaOr.B + s0 * etaOr.A;
    ribbon.Setup(omega1, eta);
  }
  Eigen::Vector3r posdiff=(pos - sr.getPosStart(0));
  for(int i=0;i<3;i++)
    allConstraints[constraintId+i] = alphaConstraintPos * posdiff[i]; 
  Eigen::MatrixXr frameNext = sr.getFrameStart(0);
  for(int i=0;i<3;i++)//aligned
    allConstraints[constraintId+3+i] = alphaConstraintFrame * (frame.col(i).dot(frameNext.col((i+1)%3)));
}

int ConstraintCyclicRibbon::size()
{
  return 6;
}
