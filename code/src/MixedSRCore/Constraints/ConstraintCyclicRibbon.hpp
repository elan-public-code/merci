/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef CONSTRAINTCYCLICRIBBON_H
#define CONSTRAINTCYCLICRIBBON_H

#include "GenericExtraConstraint.hpp"

class ConstraintCyclicRibbon : public GenericExtraConstraint
{
public:
  ConstraintCyclicRibbon(int constraintId, real alphaConstraintPos = 1., real alphaConstraintFrame = 1.);
  virtual ~ConstraintCyclicRibbon() = default;
  virtual void hessian(const MixedPhY& phy, const Eigen::VectorXr& scalar, std::map< GenericExtraConstraint::Point, real >& liste);
  virtual void hessianStruct(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, int >& liste);
  virtual void jacobian(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, real >& liste);
  virtual void jacobianStruct(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, int >& liste);
  virtual void eval(const MixedPhY& phy, Eigen::VectorXr& allConstraints);
  virtual int size();
private:
  real alphaConstraintPos;
  real alphaConstraintFrame;

};

#endif // CONSTRAINTCYCLICRIBBON_H
