/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "ConstraintEndFrame.hpp"
#include "MathOperations.hpp"
#include "MixedPhY.hpp"

ConstraintEndFrame::ConstraintEndFrame(int constraintId, Eigen::Matrix3r endFrame, real alphaConstraintEndFrame) :
  GenericExtraConstraint(constraintId),
  endFrame(endFrame),
  alphaConstraintEndFrame(alphaConstraintEndFrame)
{
}


void ConstraintEndFrame::hessian(const MixedPhY& phy, const Eigen::VectorXr& scalar, std::map< GenericExtraConstraint::Point, real >& liste)
{
  int segment = phy.getConst().getNbSegments() - 1;
  Eigen::Matrix<Eigen::Matrix4r, 3, 1> hessianPreAltitudeEnergyOut;
  Eigen::Matrix<Eigen::Matrix4r, 3, 1> hessianPosOut;
  Eigen::Matrix<Eigen::Matrix4r, 3, 3> hessianFrameOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPreAltitudeEnergyOut;
  Eigen::Matrix<Eigen::Vector3r, 3, 1> dPreAltitudeEnergydPosOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPreAltitudeEnergydFrameOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 3> dFramedFrameOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPosdFrameOut;
  Eigen::Matrix<Eigen::Vector3r, 3, 1> dPosdPosOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 3> gradFrameOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPosOut;
  Eigen::Vector3r posOut;
  Eigen::Matrix3r frameOut;
  phy.getSecondOrder(segment,
                 hessianPreAltitudeEnergyOut,
                 hessianPosOut,
                 hessianFrameOut,
                 gradPreAltitudeEnergyOut,
                 dPreAltitudeEnergydPosOut,
                 dPreAltitudeEnergydFrameOut,
                 dFramedFrameOut,
                 dPosdFrameOut,
                 dPosdPosOut,
                 gradFrameOut,
                 gradPosOut,
                 posOut,
                 frameOut);
  Eigen::Matrix<Eigen::Vector4r, 3, 3> gradPreFrameOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPreTranslationOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPreAltitudeRotationalEnergyOut;
  Eigen::Matrix3r preFrame;
  Eigen::Vector3r preTranslation;
  phy.getPres(segment,
          gradPreFrameOut,
          gradPreTranslationOut,
          gradPreAltitudeRotationalEnergyOut,
          preFrame,
          preTranslation
  );
  //d2cdk2
  for(int i=0;i<3;i++)
    for (int j = 0; j < 16; j++) 
    {
      double acc=0.;
      for(int k=0;k<3;k++)
      {
        acc+=hessianFrameOut(k,i)(j/4,j%4)*endFrame(k,(i+1)%3);
      }
      liste.at(Point(MixedPhY::vpointIDOmega1(segment) + j / 4, MixedPhY::vpointIDOmega1(segment) + (j % 4))) += alphaConstraintEndFrame * scalar[constraintId+i]*acc;
    }

  //d2cdP02
  //zero

  //d2cdF02
  //Zero

  //d2cdkdP0
  //zero

  //d2cdkdF0
  for(int i=0;i<3;i++)
  {
    for(int j=0;j<4;j++)
    {
      Eigen::Matrix3d dgradPreFramedj = MathOperations::extractDerivation(gradPreFrameOut,j);
      for(int pi=0;pi<9;pi++)
      {
        double acc=0.;
        for(int k=0;k<3;k++)
        {
          Eigen::Matrix3d Rotpre = Eigen::Matrix3d::Zero();
          Rotpre(pi/3,pi%3)=1.;
          acc+=(Rotpre*dgradPreFramedj)(k,i)*endFrame(k,(i+1)%3);
        }
        liste.at(Point(MixedPhY::vpointIDFrame(segment) + pi, MixedPhY::vpointIDOmega1(segment) + j)) += alphaConstraintEndFrame * scalar [constraintId+i]* acc;
        liste.at(Point(MixedPhY::vpointIDOmega1(segment) + j, MixedPhY::vpointIDFrame(segment) + pi)) += alphaConstraintEndFrame * scalar [constraintId+i]* acc;
      }
    }
  }

  //d2cdP0dF0
  //zero
}

void ConstraintEndFrame::hessianStruct(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, int >& liste)
{
  int segment = phy.getConst().getNbSegments() - 1;
  for(int i=0;i<3;i++)
    for (int j = 0; j < 16; j++) 
    {
      liste[Point(MixedPhY::vpointIDOmega1(segment) + j / 4, MixedPhY::vpointIDOmega1(segment) + (j % 4))] += 1;
    }

  //d2cdkdF0
  for(int i=0;i<3;i++)
  {
    for(int j=0;j<4;j++)
    {
      for(int pi=0;pi<9;pi++)
      {
        liste[Point(MixedPhY::vpointIDFrame(segment) + pi, MixedPhY::vpointIDOmega1(segment) + j)] += 1;
        liste[Point(MixedPhY::vpointIDOmega1(segment) + j, MixedPhY::vpointIDFrame(segment) + pi)] += 1;
      }
    }
  }
}

void ConstraintEndFrame::jacobian(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, real >& liste)
{
  int t = phy.getConst().getNbSegments();
  int segment = t - 1;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPreAltitudeEnergyOut;
  Eigen::Matrix<Eigen::Vector3r, 3, 1> dPreAltitudeEnergydPosOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPreAltitudeEnergydFrameOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 3> dFramedFrameOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPosdFrameOut;
  Eigen::Matrix<Eigen::Vector3r, 3, 1> dPosdPosOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 3> gradFrameOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPosOut;
  Eigen::Vector3r posOut;
  Eigen::Matrix3r frameOut;
  phy.getGradsForGravityEnergy(segment, gradPreAltitudeEnergyOut, dPreAltitudeEnergydPosOut, dPreAltitudeEnergydFrameOut, dFramedFrameOut, dPosdFrameOut, dPosdPosOut, gradFrameOut, gradPosOut, posOut, frameOut);
    
    //aligned
  //gradFrameOr
  for(int i=0;i<3;i++)
    for (int j = 0; j < 9; j++)
    {
      double acc=0.;
      for(int k=0;k<3;k++)
        acc+=dFramedFrameOut(k,i)(j / 3, j % 3)*endFrame(k,(i+1)%3);
      liste.at(Point(constraintId+i, MixedPhY::vpointIDFrame(segment) + j))+= alphaConstraintEndFrame * acc;
    }
  //gradK
  for(int i=0;i<3;i++)
    for (int j = 0; j < 4; j++) 
    {
      double acc=0.;
      for(int k=0;k<3;k++)
      {
        acc+=gradFrameOut(k,i)[j]*endFrame(k,(i+1)%3);
      }
      liste.at(Point(constraintId+i, MixedPhY::vpointIDOmega1(segment) + j))+= alphaConstraintEndFrame * acc;
    }
}

void ConstraintEndFrame::jacobianStruct(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, int >& liste)
{
  int segment = phy.getConst().getNbSegments() - 1;
  //gradFrameOr
  for(int i=0;i<3;i++)
    for (int j = 0; j < 9; j++)
    {
      liste[Point(constraintId+i, MixedPhY::vpointIDFrame(segment) + j)]+=1;
    }
  //gradK
  for(int i=0;i<3;i++)
    for (int j = 0; j < 4; j++) 
    {
      liste[Point(constraintId+i, MixedPhY::vpointIDOmega1(segment) + j)]+=1;
    }
}

void ConstraintEndFrame::eval(const MixedPhY& phy, Eigen::VectorXr& allConstraints)
{
  
  Eigen::Vector3r res;
  auto frame = phy.frameAtEnd(phy.getConst().getNbSegments()-1);
  for(int i=0;i<3;i++)//aligned
    res[i] =  (frame.col(i).dot(endFrame.col((i+1)%3)));
  allConstraints.segment<3>(constraintId) = alphaConstraintEndFrame * res;
}

int ConstraintEndFrame::size()
{
  return 3;
}
