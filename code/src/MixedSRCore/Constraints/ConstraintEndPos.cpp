/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "ConstraintEndPos.hpp"
#include "MixedPhY.hpp"


ConstraintEndPos::ConstraintEndPos(int constraintId, Eigen::Vector3r endPos, real alphaConstraintEndPos) :
  GenericExtraConstraint(constraintId),
  endPos(endPos),
  alphaConstraintEndPos(alphaConstraintEndPos)
{
}


void ConstraintEndPos::hessian(const MixedPhY& phy, const Eigen::VectorXr& scalar, std::map< GenericExtraConstraint::Point, real >& liste)
{
  int segment = phy.getConst().getNbSegments() - 1;
  Eigen::Matrix<Eigen::Matrix4r, 3, 1> hessianPreAltitudeEnergyOut;
  Eigen::Matrix<Eigen::Matrix4r, 3, 1> hessianPosOut;
  Eigen::Matrix<Eigen::Matrix4r, 3, 3> hessianFrameOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPreAltitudeEnergyOut;
  Eigen::Matrix<Eigen::Vector3r, 3, 1> dPreAltitudeEnergydPosOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPreAltitudeEnergydFrameOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 3> dFramedFrameOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPosdFrameOut;
  Eigen::Matrix<Eigen::Vector3r, 3, 1> dPosdPosOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 3> gradFrameOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPosOut;
  Eigen::Vector3r posOut;
  Eigen::Matrix3r frameOut;
  phy.getSecondOrder(segment,
                 hessianPreAltitudeEnergyOut,
                 hessianPosOut,
                 hessianFrameOut,
                 gradPreAltitudeEnergyOut,
                 dPreAltitudeEnergydPosOut,
                 dPreAltitudeEnergydFrameOut,
                 dFramedFrameOut,
                 dPosdFrameOut,
                 dPosdPosOut,
                 gradFrameOut,
                 gradPosOut,
                 posOut,
                 frameOut);
  Eigen::Matrix<Eigen::Vector4r, 3, 3> gradPreFrameOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPreTranslationOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPreAltitudeRotationalEnergyOut;
  Eigen::Matrix3r preFrame;
  Eigen::Vector3r preTranslation;
  phy.getPres(segment,
          gradPreFrameOut,
          gradPreTranslationOut,
          gradPreAltitudeRotationalEnergyOut,
          preFrame,
          preTranslation
  );
  //Eigen::Vector3r dpos = posOut - endPos;
  
  //d2cdk2
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 16; j++) 
      liste.at(Point(MixedPhY::vpointIDOmega1(segment) + j / 4, MixedPhY::vpointIDOmega1(segment) + (j % 4))) += alphaConstraintEndPos * scalar[constraintId+i]*hessianPosOut[i](j/4,j%4);
  
  //d2cdP02
  //Zero

  //d2cdF02
  //Zero

  //d2cdkdP0
  //Zero

  //d2cdkdF0
  for (int i = 0; i < 9; i++) {
    {
      for(int j=0;j<4;j++)
      {
        liste.at(Point(MixedPhY::vpointIDFrame(segment) + i, MixedPhY::vpointIDOmega1(segment) + j))+=alphaConstraintEndPos * scalar[constraintId+(i/3)]*gradPreTranslationOut[i%3][j];
        liste.at(Point(MixedPhY::vpointIDOmega1(segment) + j, MixedPhY::vpointIDFrame(segment) + i))+=alphaConstraintEndPos * scalar[constraintId+(i/3)]*gradPreTranslationOut[i%3][j];
      }
    }
  }

  //d2cdP0dF0
  //Zero
}

void ConstraintEndPos::hessianStruct(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, int >& liste)
{
  int segment = phy.getConst().getNbSegments() - 1;
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 16; j++) 
    {
      liste[Point(MixedPhY::vpointIDOmega1(segment) + j / 4, MixedPhY::vpointIDOmega1(segment) + (j % 4))]++;
    }

  for (int i = 0; i < 9; i++) {
    for(int pi=0;pi<3;pi++)
    {
      for(int j=0;j<4;j++)
      {
        liste[Point(MixedPhY::vpointIDFrame(segment) + i, MixedPhY::vpointIDOmega1(segment) + j)]++;
        liste[Point(MixedPhY::vpointIDOmega1(segment) + j, MixedPhY::vpointIDFrame(segment) + i)]++;
      }
    }
  }
}

void ConstraintEndPos::jacobian(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, real >& liste)
{
  int t = phy.getConst().getNbSegments();
  int segment = t - 1;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPreAltitudeEnergyOut;
  Eigen::Matrix<Eigen::Vector3r, 3, 1> dPreAltitudeEnergydPosOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPreAltitudeEnergydFrameOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 3> dFramedFrameOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPosdFrameOut;
  Eigen::Matrix<Eigen::Vector3r, 3, 1> dPosdPosOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 3> gradFrameOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPosOut;
  Eigen::Vector3r posOut;
  Eigen::Matrix3r frameOut;
  phy.getGradsForGravityEnergy(segment, gradPreAltitudeEnergyOut, dPreAltitudeEnergydPosOut, dPreAltitudeEnergydFrameOut, dFramedFrameOut, dPosdFrameOut, dPosdPosOut, gradFrameOut, gradPosOut, posOut, frameOut);
  //Eigen::Vector3r dpos = posOut - endPos;

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++) 
      liste.at(Point(constraintId+i, MixedPhY::vpointIDPos(segment) + j)) += alphaConstraintEndPos * dPosdPosOut[i](j);
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 9; j++)
      liste.at(Point(constraintId+i, MixedPhY::vpointIDFrame(segment) + j)) += alphaConstraintEndPos * dPosdFrameOut[i](j / 3, j % 3);
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 4; j++) 
      liste.at(Point(constraintId +i , MixedPhY::vpointIDOmega1(segment) + j)) += alphaConstraintEndPos * gradPosOut[i](j);
}

void ConstraintEndPos::jacobianStruct(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, int >& liste)
{
  int segment = phy.getConst().getNbSegments() - 1;
  for (int i = 0; i < 3; i++) for (int j = 0; j < 3; j++) liste[Point(constraintId + i, MixedPhY::vpointIDPos(segment) + j)] += 1;
  for (int i = 0; i < 3; i++) for (int j = 0; j < 9; j++) liste[Point(constraintId + i, MixedPhY::vpointIDFrame(segment) + j)] += 1;
  for (int i = 0; i < 3; i++) for (int j = 0; j < 4; j++) liste[Point(constraintId + i, MixedPhY::vpointIDOmega1(segment) + j)] += 1;
}

void ConstraintEndPos::eval(const MixedPhY& phy, Eigen::VectorXr& allConstraints)
{
  allConstraints.segment<3>(constraintId) = alphaConstraintEndPos * (phy.posAtEnd(phy.getConst().getNbSegments()-1)- endPos);
}

int ConstraintEndPos::size()
{
  return 3;
}
