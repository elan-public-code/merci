/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef CONSTRAINTENDPOS_H
#define CONSTRAINTENDPOS_H

#include "GenericExtraConstraint.hpp"

class ConstraintEndPos : public GenericExtraConstraint
{
public:
  ConstraintEndPos(int constraintId, Eigen::Vector3r endPos, real alphaConstraintEndPos = 1.);
  virtual ~ConstraintEndPos() = default;
  virtual void hessian(const MixedPhY& phy, const Eigen::VectorXr& scalar, std::map< GenericExtraConstraint::Point, real >& liste);
  virtual void hessianStruct(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, int >& liste);
  virtual void jacobian(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, real >& liste);
  virtual void jacobianStruct(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, int >& liste);
  virtual void eval(const MixedPhY& phy, Eigen::VectorXr& allConstraints);
  virtual int size();
private:
  const Eigen::Vector3r endPos;
  const real alphaConstraintEndPos;
};

#endif // CONSTRAINTENDPOS_H
