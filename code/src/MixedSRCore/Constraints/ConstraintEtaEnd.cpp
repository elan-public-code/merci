/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "ConstraintEtaEnd.hpp"
#include "MixedPhY.hpp"

ConstraintEtaEnd::ConstraintEtaEnd(int constraintId) :
  GenericExtraConstraint(constraintId)
{
}


void ConstraintEtaEnd::hessian(const MixedPhY& phy, const Eigen::VectorXr& scalar, std::map< GenericExtraConstraint::Point, real >& liste)
{
//null
}

void ConstraintEtaEnd::hessianStruct(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, int >& liste)
{
//null
}

void ConstraintEtaEnd::jacobian(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, real >& liste)
{
  auto sr = phy.getConst();
  int t = sr.getNbSegments();
  real l = sr.getLength(t - 1);
  liste.at(Point(constraintId, MixedPhY::vpointIDEta(t - 1))) += l;
  liste.at(Point(constraintId, MixedPhY::vpointIDEta(t - 1) + 1)) += 1.;
}

void ConstraintEtaEnd::jacobianStruct(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, int >& liste)
{
  int t = phy.getConst().getNbSegments();
  liste[Point(constraintId, MixedPhY::vpointIDEta(t - 1))] = 42;
  liste[Point(constraintId, MixedPhY::vpointIDEta(t - 1) + 1)] = 42;
}

void ConstraintEtaEnd::eval(const MixedPhY& phy, Eigen::VectorXr& allConstraints)
{
  auto sr = phy.getConst();
  int t = sr.getNbSegments();
  LinearScalar ls = sr.getEta(t - 1);
  real l = sr.getLength(t - 1);
  allConstraints(constraintId) = ls.A * l + ls.B;
}

int ConstraintEtaEnd::size()
{
  return 1;
}
