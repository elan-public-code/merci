/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "ConstraintEtaStart.hpp"
#include "MixedPhY.hpp"

ConstraintEtaStart::ConstraintEtaStart(int constraintId) :
  GenericExtraConstraint(constraintId)
{
}


void ConstraintEtaStart::hessian(const MixedPhY& phy, const Eigen::VectorXr& scalar, std::map< GenericExtraConstraint::Point, real >& liste)
{
//null
}

void ConstraintEtaStart::hessianStruct(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, int >& liste)
{
//null
}

void ConstraintEtaStart::jacobian(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, real >& liste)
{
  liste.at(Point(constraintId, MixedPhY::vpointIDEta(0) + 1)) += 1.;
}

void ConstraintEtaStart::jacobianStruct(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, int >& liste)
{
  liste[Point(constraintId, MixedPhY::vpointIDEta(0) + 1)] = 23;
}

void ConstraintEtaStart::eval(const MixedPhY& phy, Eigen::VectorXr& allConstraints)
{
  allConstraints(constraintId) = phy.getConst().getEta(0).B;
}

int ConstraintEtaStart::size()
{
  return 1;
}
