/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "ConstraintFrameInSO3.hpp"
#include "MixedPhY.hpp"

ConstraintFrameInSO3::ConstraintFrameInSO3(int constraintId) :
  GenericExtraConstraint(constraintId)
{
}

void ConstraintFrameInSO3::hessian(const MixedPhY& phy, const Eigen::VectorXr& scalar, std::map< GenericExtraConstraint::Point, real >& liste)
{
  Eigen::MatrixXr frame = phy.getConst().getFrameStart(0);

  for(int i=0;i<3;i++)//normed
    for(int j=0;j<3;j++)
      liste.at(Point(MixedPhY::vpointIDFrame(0) + 3*i+j, MixedPhY::vpointIDFrame(0)   + 3*i+j)) += scalar [constraintId+j]* 2.;
  //ortho
  for(int i=0;i<3;i++)
    liste.at(Point(MixedPhY::vpointIDFrame(0) + 3*i+0, MixedPhY::vpointIDFrame(0)   + 3*i+1)) += scalar [constraintId+3];
  for(int i=0;i<3;i++)
    liste.at(Point(MixedPhY::vpointIDFrame(0) + 3*i+1, MixedPhY::vpointIDFrame(0)   + 3*i+0)) += scalar [constraintId+3];
  for(int i=0;i<3;i++)
    liste.at(Point(MixedPhY::vpointIDFrame(0) + 3*i+0, MixedPhY::vpointIDFrame(0)   + 3*i+2)) += scalar [constraintId+4];
  for(int i=0;i<3;i++)
    liste.at(Point(MixedPhY::vpointIDFrame(0) + 3*i+2, MixedPhY::vpointIDFrame(0)   + 3*i+0)) += scalar [constraintId+4];
  for(int i=0;i<3;i++)
    liste.at(Point(MixedPhY::vpointIDFrame(0) + 3*i+1, MixedPhY::vpointIDFrame(0)   + 3*i+2)) += scalar [constraintId+5];
  for(int i=0;i<3;i++)
    liste.at(Point(MixedPhY::vpointIDFrame(0) + 3*i+2, MixedPhY::vpointIDFrame(0)   + 3*i+1)) += scalar [constraintId+5];

}

void ConstraintFrameInSO3::hessianStruct(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, int >& liste)
{
  for(int i=0;i<3;i++)//normed
    for(int j=0;j<3;j++)
      liste[Point(MixedPhY::vpointIDFrame(0) + 3*i+j, MixedPhY::vpointIDFrame(0)   + 3*i+j)]++;
  //ortho
  for(int i=0;i<3;i++)
    liste[Point(MixedPhY::vpointIDFrame(0) + 3*i+0, MixedPhY::vpointIDFrame(0)   + 3*i+1)]++;
  for(int i=0;i<3;i++)
    liste[Point(MixedPhY::vpointIDFrame(0) + 3*i+1, MixedPhY::vpointIDFrame(0)   + 3*i+0)]++;
  for(int i=0;i<3;i++)
    liste[Point(MixedPhY::vpointIDFrame(0) + 3*i+0, MixedPhY::vpointIDFrame(0)   + 3*i+2)]++;
  for(int i=0;i<3;i++)
    liste[Point(MixedPhY::vpointIDFrame(0) + 3*i+2, MixedPhY::vpointIDFrame(0)   + 3*i+0)]++;
  for(int i=0;i<3;i++)
    liste[Point(MixedPhY::vpointIDFrame(0) + 3*i+1, MixedPhY::vpointIDFrame(0)   + 3*i+2)]++;
  for(int i=0;i<3;i++)
    liste[Point(MixedPhY::vpointIDFrame(0) + 3*i+2, MixedPhY::vpointIDFrame(0)   + 3*i+1)]++;
}

void ConstraintFrameInSO3::jacobianStruct(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, int >& liste)
{
  for(int i=0;i<3;i++)//normed
    for(int j=0;j<3;j++)
    liste[Point(constraintId+i, MixedPhY::vpointIDFrame(0) + (3*j+i))]++;
  //ortho
  for(int i=0;i<3;i++)
    liste[Point(constraintId+3, MixedPhY::vpointIDFrame(0) +3*i)]++;
  for(int i=0;i<3;i++)
    liste[Point(constraintId+3, MixedPhY::vpointIDFrame(0) +3*i+1)]++;
  for(int i=0;i<3;i++)
    liste[Point(constraintId+4, MixedPhY::vpointIDFrame(0) +3*i)]++;
  for(int i=0;i<3;i++)
    liste[Point(constraintId+4, MixedPhY::vpointIDFrame(0) +3*i+2)]++;
  for(int i=0;i<3;i++)
    liste[Point(constraintId+5, MixedPhY::vpointIDFrame(0) +3*i +1)]++;
  for(int i=0;i<3;i++)
    liste[Point(constraintId+5, MixedPhY::vpointIDFrame(0) +3*i+2)]++;
}

void ConstraintFrameInSO3::jacobian(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, real >& liste)
{
    
  Eigen::MatrixXr frame = phy.getConst().getFrameStart(0);
  for(int i=0;i<3;i++)//normed
    for(int j=0;j<3;j++)
    liste.at(Point(constraintId+i, MixedPhY::vpointIDFrame(0) + (3*j+i))) += 2.* frame(j,i);
  //ortho
  for(int i=0;i<3;i++)
    liste.at(Point(constraintId+3, MixedPhY::vpointIDFrame(0) +3*i)) +=  frame(i,1);
  for(int i=0;i<3;i++)
    liste.at(Point(constraintId+3, MixedPhY::vpointIDFrame(0) +3*i+1)) += frame(i,0);
  for(int i=0;i<3;i++)
    liste.at(Point(constraintId+4, MixedPhY::vpointIDFrame(0) +3*i)) += frame(i,2);
  for(int i=0;i<3;i++)
    liste.at(Point(constraintId+4, MixedPhY::vpointIDFrame(0) +3*i+2)) += frame(i,0);
  for(int i=0;i<3;i++)
    liste.at(Point(constraintId+5, MixedPhY::vpointIDFrame(0) +3*i +1)) += frame(i,2);
  for(int i=0;i<3;i++)
    liste.at(Point(constraintId+5, MixedPhY::vpointIDFrame(0) +3*i+2)) += frame(i,1);
}

void ConstraintFrameInSO3::eval(const MixedPhY& phy, Eigen::VectorXr& allConstraints)
{
  Eigen::MatrixXr frame = phy.getConst().getFrameStart(0);
  for(int i=0;i<3;i++)//normed
    allConstraints[constraintId+i] = (frame.col(i).squaredNorm())-1;
  //ortho
  allConstraints[constraintId+3] = (frame.col(0).dot(frame.col(1)));
  allConstraints[constraintId+4] = (frame.col(0).dot(frame.col(2)));
  allConstraints[constraintId+5] = (frame.col(1).dot(frame.col(2)));
}

int ConstraintFrameInSO3::size()
{
  return 6;
}
