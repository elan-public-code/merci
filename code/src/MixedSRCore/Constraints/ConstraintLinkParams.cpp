/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "ConstraintLinkParams.hpp"
#include "MixedPhY.hpp"

ConstraintLinkParams::ConstraintLinkParams(int constraintId, const int segment) :
  GenericExtraConstraint(constraintId),
  segment(segment)
{
}


void ConstraintLinkParams::hessian(const MixedPhY& phy, const Eigen::VectorXr& scalar, std::map< GenericExtraConstraint::Point, real >& liste)
{

}

void ConstraintLinkParams::hessianStruct(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, int >& liste)
{

}

void ConstraintLinkParams::jacobianStruct(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, int >& liste)
{
  liste[Point(constraintId,  MixedPhY::vpointIDOmega1(segment))]++;
  liste[Point(constraintId, MixedPhY::vpointIDOmega1(segment) + 1)]++;
  liste[Point(constraintId, MixedPhY::vpointIDOmega1(segment + 1) + 1)]++;
  liste[Point(constraintId + 1, MixedPhY::vpointIDEta(segment))]++;
  liste[Point(constraintId + 1, MixedPhY::vpointIDEta(segment) + 1)]++;
  liste[Point(constraintId + 1, MixedPhY::vpointIDEta(segment + 1) + 1)]++;
}

void ConstraintLinkParams::jacobian(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, real >& liste)
{
  real l = phy.getConst().getLength(segment);
  liste.at(Point(constraintId,  MixedPhY::vpointIDOmega1(segment))) += l;
  liste.at(Point(constraintId, MixedPhY::vpointIDOmega1(segment) + 1)) += 1.;
  liste.at(Point(constraintId, MixedPhY::vpointIDOmega1(segment + 1) + 1)) += -1.;
  liste.at(Point(constraintId + 1, MixedPhY::vpointIDEta(segment))) += l;
  liste.at(Point(constraintId + 1, MixedPhY::vpointIDEta(segment) + 1)) += 1.;
  liste.at(Point(constraintId + 1, MixedPhY::vpointIDEta(segment + 1) + 1)) += -1.;
}

void ConstraintLinkParams::eval(const MixedPhY& phy, Eigen::VectorXr& allConstraints)
{
  auto sr = phy.getConst();
  real l = sr.getLength(segment);
  allConstraints[constraintId] = sr.getOmega1(segment).A * l + sr.getOmega1(segment).B - sr.getOmega1(segment + 1).B;
  allConstraints[constraintId+1] = sr.getEta(segment).A * l + sr.getEta(segment).B - sr.getEta(segment + 1).B;
}

int ConstraintLinkParams::size()
{
  return 2;
}
