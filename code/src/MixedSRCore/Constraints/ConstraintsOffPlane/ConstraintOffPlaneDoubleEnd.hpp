/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef CONTRAINTOFFPLANEDOUBLEEND_H
#define CONTRAINTOFFPLANEDOUBLEEND_H

#include "../../GenericExtraConstraint.hpp"

class ConstraintOffPlaneDoubleEnd : public GenericExtraConstraint
{
public:
  ConstraintOffPlaneDoubleEnd(int constraintId, int segment, Eigen::Vector3r planePt, Eigen::Vector3r planeNml);
  virtual void hessian(const MixedPhY& phy, const Eigen::VectorXr& scalar, std::map< GenericExtraConstraint::Point, real >& liste) override;
  virtual void hessianStruct(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, int >& liste) override;
  virtual void jacobianStruct(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, int >& liste) override;
  virtual void jacobian(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, real >& liste) override;
  virtual void eval(const MixedPhY& phy, Eigen::VectorXr& allConstraints) override;
  virtual int size() override;
  virtual void getUpperBound(Eigen::VectorXr& inpV) override;
private:
  int segment;
  Eigen::Vector3r planePt;
  Eigen::Vector3r planeNml;

};

#endif // CONTRAINTOFFPLANEDOUBLEEND_H
