/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "ConstraintOffPlaneDoubleEndStartSegment.hpp"
#include "MixedPhY.hpp"

ConstraintOffPlaneDoubleEndStartSegment::ConstraintOffPlaneDoubleEndStartSegment(int constraintId, int segment, Eigen::Vector3r planePt, Eigen::Vector3r planeNml) :
  GenericExtraConstraint(constraintId),
  segment(segment),
  planePt(planePt),
  planeNml(planeNml)
{
}

void ConstraintOffPlaneDoubleEndStartSegment::hessianStruct(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, int >& liste)
{
  for (int j = 0; j < 3; j++)
  {
    liste[Point(MixedPhY::vpointIDFrame(segment) + 3*j+2, MixedPhY::vpointIDEta(segment)+1)]++;
    liste[Point(MixedPhY::vpointIDEta(segment)+1, MixedPhY::vpointIDFrame(segment) + 3*j+2)]++;
  }
  for (int j = 0; j < 3; j++)
  {
    liste[Point(MixedPhY::vpointIDFrame(segment) + 3*j+2, MixedPhY::vpointIDEta(segment)+1)]++;
    liste[Point(MixedPhY::vpointIDEta(segment)+1, MixedPhY::vpointIDFrame(segment) + 3*j+2)]++;
  }
}

void ConstraintOffPlaneDoubleEndStartSegment::hessian(const MixedPhY& phy, const Eigen::VectorXr& scalar, std::map< GenericExtraConstraint::Point, real >& liste)
{
  real w=phy.getConst().getWidth();
  for (int j = 0; j < 3; j++)
  {
    liste.at(Point(MixedPhY::vpointIDFrame(segment) + 3*j+2, MixedPhY::vpointIDEta(segment)+1)) += -scalar[constraintId]* 0.5*w*planeNml(j);
    liste.at(Point(MixedPhY::vpointIDEta(segment)+1, MixedPhY::vpointIDFrame(segment) + 3*j+2)) += -scalar[constraintId]* 0.5*w*planeNml(j);
  }
  for (int j = 0; j < 3; j++)
  {
    liste.at(Point(MixedPhY::vpointIDFrame(segment) + 3*j+2, MixedPhY::vpointIDEta(segment)+1)) += scalar[constraintId+1]*0.5*w*planeNml(j);
    liste.at(Point(MixedPhY::vpointIDEta(segment)+1, MixedPhY::vpointIDFrame(segment) + 3*j+2)) += scalar[constraintId+1]*0.5*w*planeNml(j);
  }
}

void ConstraintOffPlaneDoubleEndStartSegment::jacobianStruct(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, int >& liste)
{
  liste[Point(constraintId, MixedPhY::vpointIDEta(segment) + 1)]++;
  liste[Point(constraintId+1, MixedPhY::vpointIDEta(segment) + 1)]++;
  for (int j = 0; j < 3; j++) 
    liste[Point(constraintId, MixedPhY::vpointIDPos(segment) + j)]++;
  for (int j = 0; j < 3; j++) 
    liste[Point(constraintId+1, MixedPhY::vpointIDPos(segment) + j)]++;
  for (int j = 0; j < 3; j++)
    liste[Point(constraintId, MixedPhY::vpointIDFrame(segment) + 3*j)]++;
  for (int j = 0; j < 3; j++)
    liste[Point(constraintId+1, MixedPhY::vpointIDFrame(segment) + 3*j)]++;
  for (int j = 0; j < 3; j++)
    liste[Point(constraintId, MixedPhY::vpointIDFrame(segment) + 3*j+2)]++;
  for (int j = 0; j < 3; j++)
    liste[Point(constraintId+1, MixedPhY::vpointIDFrame(segment) + 3*j+2)]++;
}

void ConstraintOffPlaneDoubleEndStartSegment::jacobian(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, real >& liste)
{
  auto sr = phy.getConst();
  LinearScalar ls = sr.getEta(segment);
  real eta = ls.B;
  Eigen::Matrix3r frame = sr.getFrameStart(segment);
  real w=sr.getWidth();
  
  //gradC
  liste.at(Point(constraintId, MixedPhY::vpointIDEta(segment) + 1)) += -0.5*w*planeNml.dot(frame.col(2));
  liste.at(Point(constraintId+1, MixedPhY::vpointIDEta(segment) + 1)) += 0.5*w*planeNml.dot(frame.col(2));
  //dCdpos
  for (int j = 0; j < 3; j++) 
    liste.at(Point(constraintId, MixedPhY::vpointIDPos(segment) + j)) += planeNml(j);
  for (int j = 0; j < 3; j++) 
    liste.at(Point(constraintId+1, MixedPhY::vpointIDPos(segment) + j)) += planeNml(j);
  
  //dCdFrame
  for (int j = 0; j < 3; j++)
    liste.at(Point(constraintId, MixedPhY::vpointIDFrame(segment) + 3*j)) += -0.5*w*planeNml(j);
  for (int j = 0; j < 3; j++)
    liste.at(Point(constraintId+1, MixedPhY::vpointIDFrame(segment) + 3*j)) += 0.5*w*planeNml(j);
  for (int j = 0; j < 3; j++)
    liste.at(Point(constraintId, MixedPhY::vpointIDFrame(segment) + 3*j+2)) += -0.5*w*eta*planeNml(j);
  for (int j = 0; j < 3; j++)
    liste.at(Point(constraintId+1, MixedPhY::vpointIDFrame(segment) + 3*j+2)) += 0.5*w*eta*planeNml(j);
}

void ConstraintOffPlaneDoubleEndStartSegment::eval(const MixedPhY& phy, Eigen::VectorXr& allConstraints)
{
  auto sr = phy.getConst();
  LinearScalar ls = sr.getEta(segment);
  real eta = ls.B;
  Eigen::Vector3r pos = sr.getPosStart(segment);
  Eigen::Matrix3r frame = sr.getFrameStart(segment);
  Eigen::Vector3r g = frame.col(0)+eta*frame.col(2);
  real w=sr.getWidth();
  allConstraints(constraintId) = planeNml.dot(pos-planePt-0.5*w*g);
  allConstraints(constraintId+1) = planeNml.dot(pos-planePt+0.5*w*g);
}

int ConstraintOffPlaneDoubleEndStartSegment::size()
{
  return 2;
}

void ConstraintOffPlaneDoubleEndStartSegment::getUpperBound(Eigen::VectorXr& inpV)
{
  for(int i=0;i<size();i++)
    inpV[constraintId+i]=std::numeric_limits<real>::infinity();
}
