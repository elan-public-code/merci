/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "ConstraintOffPlaneStartSegment.hpp"
#include "MixedPhY.hpp"

ConstraintOffPlaneStartSegment::ConstraintOffPlaneStartSegment(int constraintId, int segment, Eigen::Vector3r planePt, Eigen::Vector3r planeNml) :
  GenericExtraConstraint(constraintId),
  segment(segment),
  planePt(planePt),
  planeNml(planeNml)
{
}


void ConstraintOffPlaneStartSegment::hessian(const MixedPhY& phy, const Eigen::VectorXr& scalar, std::map< GenericExtraConstraint::Point, real >& liste)
{

}

void ConstraintOffPlaneStartSegment::hessianStruct(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, int >& liste)
{

}

void ConstraintOffPlaneStartSegment::jacobianStruct(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, int >& liste)
{
  for (int j = 0; j < 3; j++) 
    liste[Point(constraintId, MixedPhY::vpointIDPos(segment) + j)]++;
}

void ConstraintOffPlaneStartSegment::jacobian(const MixedPhY& phy, std::map< GenericExtraConstraint::Point, real >& liste)
{
  for (int j = 0; j < 3; j++) 
    liste.at(Point(constraintId, MixedPhY::vpointIDPos(segment) + j)) += planeNml(j);
}

void ConstraintOffPlaneStartSegment::eval(const MixedPhY& phy, Eigen::VectorXr& allConstraints)
{
  auto pos = phy.getConst().getPosStart(segment);
  allConstraints(constraintId) = planeNml.dot(pos-planePt);
}

void ConstraintOffPlaneStartSegment::getUpperBound(Eigen::VectorXr& inpV)
{
  for(int i=0;i<size();i++)
    inpV[constraintId+i]=std::numeric_limits<real>::infinity();
}

int ConstraintOffPlaneStartSegment::size()
{
  return 1;
}
