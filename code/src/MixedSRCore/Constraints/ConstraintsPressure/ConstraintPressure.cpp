/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "ConstraintPressure.hpp"
#include "MixedPhY.hpp"
#include "MathOperations.hpp"

ConstraintPressure::ConstraintPressure(int constraintId, int segment, int pressureId) :
  GenericExtraConstraint(constraintId),
  segment(segment),
  pressureId(pressureId)
{
}

void ConstraintPressure::hessianStruct(const MixedPhY& phy, std::map<GenericExtraConstraint::Point, int>& liste)
{
  for (int j = 0; j < 16; j++) 
    liste[Point(MixedPhY::vpointIDOmega1(segment) + j / 4, MixedPhY::vpointIDOmega1(segment) + (j % 4))] ++;
  for (int i = 0; i < 9; i++) {
    {
      for(int j=0;j<4;j++)
      {
        liste[Point(MixedPhY::vpointIDFrame(segment) + i, MixedPhY::vpointIDOmega1(segment) + j)]++;
        liste[Point(MixedPhY::vpointIDOmega1(segment) + j, MixedPhY::vpointIDFrame(segment) + i)]++;
      }
    }
  }
}


void ConstraintPressure::hessian(const MixedPhY& phy, const Eigen::VectorXr& scalar, std::map<GenericExtraConstraint::Point, real>& liste)
{
  Eigen::Matrix<Eigen::Matrix4r, 3, 1> hessianPreAltitudeEnergyOut;
  Eigen::Matrix<Eigen::Matrix4r, 3, 1> hessianPosOut;
  Eigen::Matrix<Eigen::Matrix4r, 3, 3> hessianFrameOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPreAltitudeEnergyOut;
  Eigen::Matrix<Eigen::Vector3r, 3, 1> dPreAltitudeEnergydPosOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPreAltitudeEnergydFrameOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 3> dFramedFrameOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPosdFrameOut;
  Eigen::Matrix<Eigen::Vector3r, 3, 1> dPosdPosOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 3> gradFrameOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPosOut;
  Eigen::Vector3r posOut;
  Eigen::Matrix3r frameOut;
  phy.getSecondOrder(segment,
                 hessianPreAltitudeEnergyOut,
                 hessianPosOut,
                 hessianFrameOut,
                 gradPreAltitudeEnergyOut,
                 dPreAltitudeEnergydPosOut,
                 dPreAltitudeEnergydFrameOut,
                 dFramedFrameOut,
                 dPosdFrameOut,
                 dPosdPosOut,
                 gradFrameOut,
                 gradPosOut,
                 posOut,
                 frameOut);
  Eigen::Matrix<Eigen::Vector4r, 3, 3> gradPreFrameOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPreTranslationOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPreAltitudeRotationalEnergyOut;
  Eigen::Matrix3r preFrame;
  Eigen::Vector3r preTranslation;
  phy.getPres(segment,
          gradPreFrameOut,
          gradPreTranslationOut,
          gradPreAltitudeRotationalEnergyOut,
          preFrame,
          preTranslation
  );
  //Eigen::Vector3r dpos = posOut - endPos;
  Eigen::Vector3r planeNml=phy.getPressure(pressureId).normale;
  //d2cdk2
  Eigen::Matrix4r d2Cdk2 = MathOperations::contractGeneralDotP(planeNml, hessianPosOut);
  for (int j = 0; j < 16; j++) 
    liste.at(Point(MixedPhY::vpointIDOmega1(segment) + j / 4, MixedPhY::vpointIDOmega1(segment) + (j % 4))) += scalar[constraintId]*d2Cdk2(j/4,j%4);
  
  //d2cdP02
  //Zero

  //d2cdF02
  //Zero

  //d2cdkdP0
  //Zero

  //d2cdkdF0
  for (int i = 0; i < 9; i++) {
    {
      for(int j=0;j<4;j++)
      {
        liste.at(Point(MixedPhY::vpointIDFrame(segment) + i, MixedPhY::vpointIDOmega1(segment) + j))+=planeNml[i/3] * scalar[constraintId]*gradPreTranslationOut[i%3][j];
        liste.at(Point(MixedPhY::vpointIDOmega1(segment) + j, MixedPhY::vpointIDFrame(segment) + i))+=planeNml[i/3] * scalar[constraintId]*gradPreTranslationOut[i%3][j];
      }
    }
  }

  //d2cdP0dF0
  //Zero
}

void ConstraintPressure::jacobianStruct(const MixedPhY& phy, std::map<GenericExtraConstraint::Point, int>& liste)
{
  for (int j = 0; j < 3; j++) 
    liste[Point(constraintId, MixedPhY::vpointIDPos(segment) + j)] += 1;
  for (int j = 0; j < 9; j++)
    liste[Point(constraintId, MixedPhY::vpointIDFrame(segment) + j)] += 1;
  for (int j = 0; j < 4; j++) 
    liste[Point(constraintId, MixedPhY::vpointIDOmega1(segment) + j)] += 1;
  liste[Point(constraintId, MixedPhY::vpointIDPressures(phy.getConst().getNbSegments()) + pressureId)]+=1;
}

void ConstraintPressure::jacobian(const MixedPhY& phy, std::map<GenericExtraConstraint::Point, real>& liste)
{
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPreAltitudeEnergyOut;
  Eigen::Matrix<Eigen::Vector3r, 3, 1> dPreAltitudeEnergydPosOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPreAltitudeEnergydFrameOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 3> dFramedFrameOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPosdFrameOut;
  Eigen::Matrix<Eigen::Vector3r, 3, 1> dPosdPosOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 3> gradFrameOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPosOut;
  Eigen::Vector3r posOut;
  Eigen::Matrix3r frameOut;
  phy.getGradsForGravityEnergy(segment, gradPreAltitudeEnergyOut, dPreAltitudeEnergydPosOut, dPreAltitudeEnergydFrameOut, dFramedFrameOut, dPosdFrameOut, dPosdPosOut, gradFrameOut, gradPosOut, posOut, frameOut);
  
  auto pressure = phy.getPressure(pressureId);
  Eigen::Vector3r dCdpos = MathOperations::contractGeneralDotP(pressure.normale, dPosdPosOut);
  Eigen::Matrix3r dCdframe = MathOperations::contractGeneralDotP(pressure.normale,dPosdFrameOut);
  Eigen::Vector4r gradC = MathOperations::contractGeneralDotP(pressure.normale,gradPosOut);
  
  for (int j = 0; j < 3; j++) 
    liste.at(Point(constraintId, MixedPhY::vpointIDPos(segment) + j)) += dCdpos(j);
  for (int j = 0; j < 9; j++)
    liste.at(Point(constraintId, MixedPhY::vpointIDFrame(segment) + j)) += dCdframe(j / 3, j % 3);
  for (int j = 0; j < 4; j++) 
    liste.at(Point(constraintId, MixedPhY::vpointIDOmega1(segment) + j)) += gradC(j);
  liste.at(Point(constraintId, MixedPhY::vpointIDPressures(phy.getConst().getNbSegments()) + pressureId)) += pressure.normale.dot(pressure.normale);
}

void ConstraintPressure::eval(const MixedPhY& phy, Eigen::VectorXr& allConstraints)
{
  auto pos = phy.posAtEnd(segment);
  auto pressure = phy.getPressure(pressureId);
  Eigen::Vector3r pressurePos = pressure.position-pressure.distance*pressure.normale;
  allConstraints(constraintId) = pressure.normale.dot(pos-pressurePos);
}

void ConstraintPressure::getUpperBound(Eigen::VectorXr& inpV)
{
  for(int i=0;i<size();i++)
    inpV[constraintId+i]=std::numeric_limits<real>::infinity();
}

int ConstraintPressure::size()
{
  return 1;
}
