/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "ConstraintPressureDoubleEnd.hpp"
#include "MixedPhY.hpp"
#include "MathOperations.hpp"

ConstraintPressureDoubleEnd::ConstraintPressureDoubleEnd(int constraintId, int segment, int pressureId) :
  GenericExtraConstraint(constraintId),
  segment(segment),
  pressureId(pressureId)
{
}

void ConstraintPressureDoubleEnd::hessianStruct(const MixedPhY& phy, std::map<GenericExtraConstraint::Point, int>& liste)
{
  for (int j = 0; j < 16; j++) 
  {
    liste[Point(MixedPhY::vpointIDOmega1(segment) + j / 4, MixedPhY::vpointIDOmega1(segment) + (j % 4))]++;
    liste[Point(MixedPhY::vpointIDOmega1(segment) + j / 4, MixedPhY::vpointIDOmega1(segment) + (j % 4))]++;
  }
  for (int i = 0; i < 9; i++) 
  {
    for(int j=0;j<4;j++)
    {
      liste[Point(MixedPhY::vpointIDFrame(segment) + i, MixedPhY::vpointIDOmega1(segment) + j)]++;
      liste[Point(MixedPhY::vpointIDOmega1(segment) + j, MixedPhY::vpointIDFrame(segment) + i)]++;
      liste[Point(MixedPhY::vpointIDFrame(segment) + i, MixedPhY::vpointIDOmega1(segment) + j)]++;
      liste[Point(MixedPhY::vpointIDOmega1(segment) + j, MixedPhY::vpointIDFrame(segment) + i)]++;
    }
  }
}

void ConstraintPressureDoubleEnd::hessian(const MixedPhY& phy, const Eigen::VectorXr& scalar, std::map<GenericExtraConstraint::Point, real>& liste)
{
  Eigen::Matrix<Eigen::Matrix4r, 3, 1> hessianPreAltitudeEnergyOut;
  Eigen::Matrix<Eigen::Matrix4r, 3, 1> hessianPosOut;
  Eigen::Matrix<Eigen::Matrix4r, 3, 3> hessianFrameOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPreAltitudeEnergyOut;
  Eigen::Matrix<Eigen::Vector3r, 3, 1> dPreAltitudeEnergydPosOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPreAltitudeEnergydFrameOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 3> dFramedFrameOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPosdFrameOut;
  Eigen::Matrix<Eigen::Vector3r, 3, 1> dPosdPosOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 3> gradFrameOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPosOut;
  Eigen::Vector3r posOut;
  Eigen::Matrix3r frameOut;
  phy.getSecondOrder(segment,
                 hessianPreAltitudeEnergyOut,
                 hessianPosOut,
                 hessianFrameOut,
                 gradPreAltitudeEnergyOut,
                 dPreAltitudeEnergydPosOut,
                 dPreAltitudeEnergydFrameOut,
                 dFramedFrameOut,
                 dPosdFrameOut,
                 dPosdPosOut,
                 gradFrameOut,
                 gradPosOut,
                 posOut,
                 frameOut);
  Eigen::Matrix<Eigen::Vector4r, 3, 3> gradPreFrameOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPreTranslationOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPreAltitudeRotationalEnergyOut;
  Eigen::Matrix3r preFrame;
  Eigen::Vector3r preTranslation;
  phy.getPres(segment,
          gradPreFrameOut,
          gradPreTranslationOut,
          gradPreAltitudeRotationalEnergyOut,
          preFrame,
          preTranslation
  );
  auto sr = phy.getConst();
  LinearScalar ls = sr.getEta(segment);
  real l = sr.getLength(segment);
  real eta = ls.A * l + ls.B;
  //Eigen::Matrix3r frame = phy.frameAtEnd(segment);
  real w=sr.getWidth();
  Eigen::Vector3r planeNml=phy.getPressure(pressureId).normale;
  //d2cdk2
  Eigen::Matrix4r d2Cdk2 = MathOperations::contractGeneralDotP(planeNml, hessianPosOut);
  
  Eigen::Matrix<Eigen::Vector4r,3,1> gradFrameOut3 = gradFrameOut.col(2);
  //Eigen::Matrix<Eigen::Vector4r,3,1> gradFrameOut1 = gradFrameOut.col(0);
  
  
  Eigen::Matrix<Eigen::Matrix4r,3,1> hessFrameOut3 = hessianFrameOut.col(2);
  Eigen::Matrix<Eigen::Matrix4r,3,1> hessFrameOut1 = hessianFrameOut.col(0);
  Eigen::Matrix4r d2Cdk2_g = MathOperations::contractGeneralDotP(planeNml,hessFrameOut1)+eta*MathOperations::contractGeneralDotP(planeNml,hessFrameOut3);
  
  Eigen::Vector4r additif2 = l*MathOperations::contractGeneralDotP(planeNml,gradFrameOut3);
  Eigen::Vector4r additif3 = MathOperations::contractGeneralDotP(planeNml,gradFrameOut3);
  d2Cdk2_g.col(2)+=additif2;
  d2Cdk2_g.col(3)+=additif3;
  d2Cdk2_g.row(2)+=additif2;
  d2Cdk2_g.row(3)+=additif3;
  
  
  for (int j = 0; j < 16; j++) 
  {
    liste.at(Point(MixedPhY::vpointIDOmega1(segment) + j / 4, MixedPhY::vpointIDOmega1(segment) + (j % 4))) += scalar[constraintId]*(d2Cdk2(j/4,j%4)-0.5*w*d2Cdk2_g(j/4,j%4));
    liste.at(Point(MixedPhY::vpointIDOmega1(segment) + j / 4, MixedPhY::vpointIDOmega1(segment) + (j % 4))) += scalar[constraintId+1]*(d2Cdk2(j/4,j%4)+0.5*w*d2Cdk2_g(j/4,j%4));
  }
  
  //d2cdP02
  //Zero

  //d2cdF02
  //Zero

  //d2cdkdP0
  //Zero

  //d2cdkdF0
  for (int i = 0; i < 9; i++)
  {
    Eigen::Matrix3r pre=Eigen::Matrix3r::Zero();
    pre(i/3,i%3)=1.;
    //pos
    Eigen::Matrix<Eigen::Vector4r,3,1> temp1 = MathOperations::product( pre,gradPreTranslationOut);
    Eigen::Vector4r d2cdkdF0_1 = MathOperations::contractGeneralDotP(planeNml,temp1);
    //d1, d3
    Eigen::Matrix<Eigen::Vector4r,3,1> temp2 = MathOperations::product(pre, gradPreFrameOut.col(0).eval());
    Eigen::Matrix<Eigen::Vector4r,3,1> temp3 = MathOperations::product(pre, gradPreFrameOut.col(2).eval());
    //eta
    Eigen::Vector4r gradC2 = MathOperations::contractGeneralDotP(planeNml,temp2)+eta*MathOperations::contractGeneralDotP(planeNml,temp3);
    gradC2[2]+=l*planeNml.dot(pre*preFrame.col(2));
    gradC2[3]+=planeNml.dot(pre*preFrame.col(2));
    Eigen::Vector4r d2cdkdF0_2 = gradC2;
    for(int j=0;j<4;j++)
    {
      liste.at(Point(MixedPhY::vpointIDFrame(segment) + i, MixedPhY::vpointIDOmega1(segment) + j)) += scalar[constraintId]*(d2cdkdF0_1[j]-0.5*w*d2cdkdF0_2[j]);
      liste.at(Point(MixedPhY::vpointIDOmega1(segment) + j, MixedPhY::vpointIDFrame(segment) + i)) += scalar[constraintId]*(d2cdkdF0_1[j]-0.5*w*d2cdkdF0_2[j]);
      liste.at(Point(MixedPhY::vpointIDFrame(segment) + i, MixedPhY::vpointIDOmega1(segment) + j)) += scalar[constraintId+1]*(d2cdkdF0_1[j]+0.5*w*d2cdkdF0_2[j]);
      liste.at(Point(MixedPhY::vpointIDOmega1(segment) + j, MixedPhY::vpointIDFrame(segment) + i)) += scalar[constraintId+1]*(d2cdkdF0_1[j]+0.5*w*d2cdkdF0_2[j]);
    }
  }

  //d2cdP0dF0
  //Zero
}

void ConstraintPressureDoubleEnd::jacobianStruct(const MixedPhY& phy, std::map<GenericExtraConstraint::Point, int>& liste)
{
  for (int j = 0; j < 3; j++) 
    liste[Point(constraintId, MixedPhY::vpointIDPos(segment) + j)]++;
  for (int j = 0; j < 3; j++) 
    liste[Point(constraintId+1, MixedPhY::vpointIDPos(segment) + j)]++;
  for (int j = 0; j < 9; j++)
    liste[Point(constraintId, MixedPhY::vpointIDFrame(segment) + j)]++;
  for (int j = 0; j < 9; j++)
    liste[Point(constraintId+1, MixedPhY::vpointIDFrame(segment) + j)]++;
  for (int j = 0; j < 4; j++) 
    liste[Point(constraintId, MixedPhY::vpointIDOmega1(segment) + j)]++;
  for (int j = 0; j < 4; j++) 
    liste[Point(constraintId+1, MixedPhY::vpointIDOmega1(segment) + j)]++;
  liste[Point(constraintId, MixedPhY::vpointIDPressures(phy.getConst().getNbSegments()) + pressureId)]+=1;
  liste[Point(constraintId+1, MixedPhY::vpointIDPressures(phy.getConst().getNbSegments()) + pressureId)]+=1;
}

void ConstraintPressureDoubleEnd::jacobian(const MixedPhY& phy, std::map<GenericExtraConstraint::Point, real>& liste)
{
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPreAltitudeEnergyOut;
  Eigen::Matrix<Eigen::Vector3r, 3, 1> dPreAltitudeEnergydPosOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPreAltitudeEnergydFrameOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 3> dFramedFrameOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPosdFrameOut;
  Eigen::Matrix<Eigen::Vector3r, 3, 1> dPosdPosOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 3> gradFrameOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPosOut;
  Eigen::Vector3r posOut;
  Eigen::Matrix3r frameOut;
  phy.getGradsForGravityEnergy(segment, gradPreAltitudeEnergyOut, dPreAltitudeEnergydPosOut, dPreAltitudeEnergydFrameOut, dFramedFrameOut, dPosdFrameOut, dPosdPosOut, gradFrameOut, gradPosOut, posOut, frameOut);
  
  auto pressure = phy.getPressure(pressureId);
  Eigen::Vector3r dCdpos = MathOperations::contractGeneralDotP(pressure.normale, dPosdPosOut);
  Eigen::Matrix3r dCdframe1 = MathOperations::contractGeneralDotP(pressure.normale,dPosdFrameOut);
  Eigen::Vector4r gradC1 = MathOperations::contractGeneralDotP(pressure.normale,gradPosOut);
  
  
  auto sr = phy.getConst();
  LinearScalar ls = sr.getEta(segment);
  real l = sr.getLength(segment);
  real eta = ls.A * l + ls.B;
  Eigen::Matrix3r frame = phy.frameAtEnd(segment);
  real w=sr.getWidth();
  
  Eigen::Matrix<Eigen::Matrix3r,3,1> dFramedFrameOut3 = dFramedFrameOut.col(2);
  Eigen::Matrix<Eigen::Matrix3r,3,1> dFramedFrameOut1 = dFramedFrameOut.col(0);
  Eigen::Matrix3r dCdframe2 = MathOperations::contractGeneralDotP(pressure.normale,dFramedFrameOut1)+eta*MathOperations::contractGeneralDotP(pressure.normale,dFramedFrameOut3);
  
  Eigen::Matrix<Eigen::Vector4r,3,1> gradFrameOut3 = gradFrameOut.col(2);
  Eigen::Matrix<Eigen::Vector4r,3,1> gradFrameOut1 = gradFrameOut.col(0);
  Eigen::Vector4r gradC2 = MathOperations::contractGeneralDotP(pressure.normale,gradFrameOut1)+eta*MathOperations::contractGeneralDotP(pressure.normale,gradFrameOut3);
  gradC2[2]+=l*pressure.normale.dot(frame.col(2));
  gradC2[3]+=pressure.normale.dot(frame.col(2));
  
  Eigen::Matrix3r dCdframe_minus = dCdframe1-0.5*w*dCdframe2;
  Eigen::Vector4r gradC_minus = gradC1-0.5*w*gradC2;
  Eigen::Matrix3r dCdframe_plus = dCdframe1+0.5*w*dCdframe2;
  Eigen::Vector4r gradC_plus = gradC1+0.5*w*gradC2;
  
  for (int j = 0; j < 3; j++) 
    liste.at(Point(constraintId, MixedPhY::vpointIDPos(segment) + j)) += dCdpos(j);
  for (int j = 0; j < 3; j++) 
    liste.at(Point(constraintId+1, MixedPhY::vpointIDPos(segment) + j)) += dCdpos(j);
  for (int j = 0; j < 9; j++)
    liste.at(Point(constraintId, MixedPhY::vpointIDFrame(segment) + j)) += dCdframe_minus(j / 3, j % 3);
  for (int j = 0; j < 9; j++)
    liste.at(Point(constraintId+1, MixedPhY::vpointIDFrame(segment) + j)) += dCdframe_plus(j / 3, j % 3);
  for (int j = 0; j < 4; j++) 
    liste.at(Point(constraintId, MixedPhY::vpointIDOmega1(segment) + j)) += gradC_minus(j);
  for (int j = 0; j < 4; j++) 
    liste.at(Point(constraintId+1, MixedPhY::vpointIDOmega1(segment) + j)) += gradC_plus(j);
  liste.at(Point(constraintId, MixedPhY::vpointIDPressures(phy.getConst().getNbSegments()) + pressureId)) += pressure.normale.dot(pressure.normale);
  liste.at(Point(constraintId+1, MixedPhY::vpointIDPressures(phy.getConst().getNbSegments()) + pressureId)) += pressure.normale.dot(pressure.normale);
}

void ConstraintPressureDoubleEnd::eval(const MixedPhY& phy, Eigen::VectorXr& allConstraints)
{
  auto sr = phy.getConst();
  LinearScalar ls = sr.getEta(segment);
  real l = sr.getLength(segment);
  real eta = ls.A * l + ls.B;
  auto [pos,frame] = phy.posFrameAtEnd(segment);
  Eigen::Vector3r g = frame.col(0)+eta*frame.col(2);
  real w=sr.getWidth();
  auto pressure = phy.getPressure(pressureId);
  Eigen::Vector3r pressurePos = pressure.position-pressure.distance*pressure.normale;
  allConstraints(constraintId) = pressure.normale.dot(pos-pressurePos-0.5*w*g);
  allConstraints(constraintId+1) = pressure.normale.dot(pos-pressurePos+0.5*w*g);
}

void ConstraintPressureDoubleEnd::getUpperBound(Eigen::VectorXr& inpV)
{
  for(int i=0;i<size();i++)
    inpV[constraintId+i]=std::numeric_limits<real>::infinity();
}

int ConstraintPressureDoubleEnd::size()
{
  return 2;
}
