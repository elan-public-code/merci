/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "ConstraintPressureDoubleEndStartSegment.hpp"
#include "MixedPhY.hpp"

ConstraintPressureDoubleEndStartSegment::ConstraintPressureDoubleEndStartSegment(int constraintId, int segment, int pressureId) :
  GenericExtraConstraint(constraintId),
  segment(segment),
  pressureId(pressureId)
{
}

void ConstraintPressureDoubleEndStartSegment::hessianStruct(const MixedPhY& phy, std::map<GenericExtraConstraint::Point, int>& liste)
{
  for (int j = 0; j < 3; j++)
  {
    liste[Point(MixedPhY::vpointIDFrame(segment) + 3*j+2, MixedPhY::vpointIDEta(segment)+1)]++;
    liste[Point(MixedPhY::vpointIDEta(segment)+1, MixedPhY::vpointIDFrame(segment) + 3*j+2)]++;
  }
  for (int j = 0; j < 3; j++)
  {
    liste[Point(MixedPhY::vpointIDFrame(segment) + 3*j+2, MixedPhY::vpointIDEta(segment)+1)]++;
    liste[Point(MixedPhY::vpointIDEta(segment)+1, MixedPhY::vpointIDFrame(segment) + 3*j+2)]++;
  }
}

void ConstraintPressureDoubleEndStartSegment::hessian(const MixedPhY& phy, const Eigen::VectorXr& scalar, std::map<GenericExtraConstraint::Point, real>& liste)
{
  real w=phy.getConst().getWidth();
  Eigen::Vector3r planeNml=phy.getPressure(pressureId).normale;
  for (int j = 0; j < 3; j++)
  {
    liste.at(Point(MixedPhY::vpointIDFrame(segment) + 3*j+2, MixedPhY::vpointIDEta(segment)+1)) += -scalar[constraintId]* 0.5*w*planeNml(j);
    liste.at(Point(MixedPhY::vpointIDEta(segment)+1, MixedPhY::vpointIDFrame(segment) + 3*j+2)) += -scalar[constraintId]* 0.5*w*planeNml(j);
  }
  for (int j = 0; j < 3; j++)
  {
    liste.at(Point(MixedPhY::vpointIDFrame(segment) + 3*j+2, MixedPhY::vpointIDEta(segment)+1)) += scalar[constraintId+1]*0.5*w*planeNml(j);
    liste.at(Point(MixedPhY::vpointIDEta(segment)+1, MixedPhY::vpointIDFrame(segment) + 3*j+2)) += scalar[constraintId+1]*0.5*w*planeNml(j);
  }
}

void ConstraintPressureDoubleEndStartSegment::jacobianStruct(const MixedPhY& phy, std::map<GenericExtraConstraint::Point, int>& liste)
{
  liste[Point(constraintId, MixedPhY::vpointIDEta(segment) + 1)]++;
  liste[Point(constraintId+1, MixedPhY::vpointIDEta(segment) + 1)]++;
  for (int j = 0; j < 3; j++) 
    liste[Point(constraintId, MixedPhY::vpointIDPos(segment) + j)]++;
  for (int j = 0; j < 3; j++) 
    liste[Point(constraintId+1, MixedPhY::vpointIDPos(segment) + j)]++;
  for (int j = 0; j < 3; j++)
    liste[Point(constraintId, MixedPhY::vpointIDFrame(segment) + 3*j)]++;
  for (int j = 0; j < 3; j++)
    liste[Point(constraintId+1, MixedPhY::vpointIDFrame(segment) + 3*j)]++;
  for (int j = 0; j < 3; j++)
    liste[Point(constraintId, MixedPhY::vpointIDFrame(segment) + 3*j+2)]++;
  for (int j = 0; j < 3; j++)
    liste[Point(constraintId+1, MixedPhY::vpointIDFrame(segment) + 3*j+2)]++;
  liste[Point(constraintId, MixedPhY::vpointIDPressures(phy.getConst().getNbSegments()) + pressureId)]+=1;
  liste[Point(constraintId+1, MixedPhY::vpointIDPressures(phy.getConst().getNbSegments()) + pressureId)]+=1;
}

void ConstraintPressureDoubleEndStartSegment::jacobian(const MixedPhY& phy, std::map<GenericExtraConstraint::Point, real>& liste)
{
  auto sr = phy.getConst();
  LinearScalar ls = sr.getEta(segment);
  real eta = ls.B;
  Eigen::Matrix3r frame = sr.getFrameStart(segment);
  real w=sr.getWidth();
  auto pressure = phy.getPressure(pressureId);
  Eigen::Vector3r planeNml=pressure.normale;
  //gradC
  liste.at(Point(constraintId, MixedPhY::vpointIDEta(segment) + 1)) += -0.5*w*planeNml.dot(frame.col(2));
  liste.at(Point(constraintId+1, MixedPhY::vpointIDEta(segment) + 1)) += 0.5*w*planeNml.dot(frame.col(2));
  //dCdpos
  for (int j = 0; j < 3; j++) 
    liste.at(Point(constraintId, MixedPhY::vpointIDPos(segment) + j)) += planeNml(j);
  for (int j = 0; j < 3; j++) 
    liste.at(Point(constraintId+1, MixedPhY::vpointIDPos(segment) + j)) += planeNml(j);
  
  //dCdFrame
  for (int j = 0; j < 3; j++)
    liste.at(Point(constraintId, MixedPhY::vpointIDFrame(segment) + 3*j)) += -0.5*w*planeNml(j);
  for (int j = 0; j < 3; j++)
    liste.at(Point(constraintId+1, MixedPhY::vpointIDFrame(segment) + 3*j)) += 0.5*w*planeNml(j);
  for (int j = 0; j < 3; j++)
    liste.at(Point(constraintId, MixedPhY::vpointIDFrame(segment) + 3*j+2)) += -0.5*w*eta*planeNml(j);
  for (int j = 0; j < 3; j++)
    liste.at(Point(constraintId+1, MixedPhY::vpointIDFrame(segment) + 3*j+2)) += 0.5*w*eta*planeNml(j);
  liste.at(Point(constraintId, MixedPhY::vpointIDPressures(phy.getConst().getNbSegments()) + pressureId)) += pressure.normale.dot(pressure.normale);
  liste.at(Point(constraintId+1, MixedPhY::vpointIDPressures(phy.getConst().getNbSegments()) + pressureId)) += pressure.normale.dot(pressure.normale);
}


void ConstraintPressureDoubleEndStartSegment::eval(const MixedPhY& phy, Eigen::VectorXr& allConstraints)
{
  auto sr = phy.getConst();
  LinearScalar ls = sr.getEta(segment);
  real eta = ls.B;
  Eigen::Vector3r pos = sr.getPosStart(segment);
  Eigen::Matrix3r frame = sr.getFrameStart(segment);
  Eigen::Vector3r g = frame.col(0)+eta*frame.col(2);
  real w=sr.getWidth();
  auto pressure = phy.getPressure(pressureId);
  Eigen::Vector3r pressurePos = pressure.position-pressure.distance*pressure.normale;
  allConstraints(constraintId) = pressure.normale.dot(pos-pressurePos-0.5*w*g);
  allConstraints(constraintId+1) = pressure.normale.dot(pos-pressurePos+0.5*w*g);
}

void ConstraintPressureDoubleEndStartSegment::getUpperBound(Eigen::VectorXr& inpV)
{
  for(int i=0;i<size();i++)
    inpV[constraintId+i]=std::numeric_limits<real>::infinity();
}

int ConstraintPressureDoubleEndStartSegment::size()
{
  return 2;
}
