/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "ConstraintPressureStartSegment.hpp"
#include "MixedPhY.hpp"

ConstraintPressureStartSegment::ConstraintPressureStartSegment(int constraintId, int segment, int pressureId) :
  GenericExtraConstraint(constraintId),
  segment(segment),
  pressureId(pressureId)
{
}

void ConstraintPressureStartSegment::hessianStruct(const MixedPhY& phy, std::map<GenericExtraConstraint::Point, int>& liste)
{
}

void ConstraintPressureStartSegment::hessian(const MixedPhY& phy, const Eigen::VectorXr& scalar, std::map<GenericExtraConstraint::Point, real>& liste)
{
}

void ConstraintPressureStartSegment::jacobianStruct(const MixedPhY& phy, std::map<GenericExtraConstraint::Point, int>& liste)
{
  for (int j = 0; j < 3; j++) 
    liste[Point(constraintId, MixedPhY::vpointIDPos(segment) + j)]++;
  liste[Point(constraintId, MixedPhY::vpointIDPressures(phy.getConst().getNbSegments()) + pressureId)]+=1;
}

void ConstraintPressureStartSegment::jacobian(const MixedPhY& phy, std::map<GenericExtraConstraint::Point, real>& liste)
{
  auto pressure = phy.getPressure(pressureId);
  for (int j = 0; j < 3; j++) 
    liste.at(Point(constraintId, MixedPhY::vpointIDPos(segment) + j)) += pressure.normale(j);
  liste.at(Point(constraintId, MixedPhY::vpointIDPressures(phy.getConst().getNbSegments()) + pressureId)) += pressure.normale.dot(pressure.normale);
}

void ConstraintPressureStartSegment::eval(const MixedPhY& phy, Eigen::VectorXr& allConstraints)
{
  auto pos = phy.getConst().getPosStart(segment);
  auto pressure = phy.getPressure(pressureId);
  Eigen::Vector3r pressurePos = pressure.position-pressure.distance*pressure.normale;
  allConstraints(constraintId) = pressure.normale.dot(pos-pressurePos);
}

void ConstraintPressureStartSegment::getUpperBound(Eigen::VectorXr& inpV)
{
  for(int i=0;i<size();i++)
    inpV[constraintId+i]=std::numeric_limits<real>::infinity();
}

int ConstraintPressureStartSegment::size()
{
  return 1;
}
