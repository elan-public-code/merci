/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "ExtraConstraint.hpp"


ExtraConstraint::ExtraConstraint()
{
}

ExtraConstraint::ExtraConstraint(ExtraConstraintId id)
{
  actives=1<<static_cast<int>(id);
}

ExtraConstraint::ExtraConstraint(int actives):
  actives(actives)
{
}


ExtraConstraint::operator bool() const
{
  return actives>0;
}

ExtraConstraint ExtraConstraint::operator+(const ExtraConstraint& other) const
{
  return ExtraConstraint(actives|other.actives);
}

ExtraConstraint ExtraConstraint::operator*(const ExtraConstraint& other) const
{
  return ExtraConstraint(actives&other.actives);
}

int ExtraConstraint::getNbContraints()
{
  int extraSize=0; 
  if(actives&(1<<static_cast<int>(ExtraConstraintId::StartEta)))extraSize++;
  if(actives&(1<<static_cast<int>(ExtraConstraintId::EndEta)))extraSize++;
  if(actives&(1<<static_cast<int>(ExtraConstraintId::EndPos)))extraSize+=3;
  if(actives&(1<<static_cast<int>(ExtraConstraintId::EndFrame)))extraSize+=3;
  return extraSize;
}


const ExtraConstraint ExtraConstraint::StartEta=ExtraConstraint(ExtraConstraint::ExtraConstraintId::StartEta);
const ExtraConstraint ExtraConstraint::EndEta=ExtraConstraint(ExtraConstraint::ExtraConstraintId::EndEta);
const ExtraConstraint ExtraConstraint::EndPos=ExtraConstraint(ExtraConstraint::ExtraConstraintId::EndPos);
const ExtraConstraint ExtraConstraint::EndFrame=ExtraConstraint(ExtraConstraint::ExtraConstraintId::EndFrame);

