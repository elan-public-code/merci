/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef EXTRACONSTRAINT_H
#define EXTRACONSTRAINT_H

#include <MixedSRCore/MixedPhY.hpp>

class ExtraConstraint{
public:
  ExtraConstraint();
  static const ExtraConstraint StartEta;
  static const ExtraConstraint EndEta;
  static const ExtraConstraint EndPos;
  static const ExtraConstraint EndFrame;
  ExtraConstraint operator+(const ExtraConstraint& other) const;
  ExtraConstraint operator*(const ExtraConstraint& other) const;
  operator bool() const;
  int getNbContraints();
private:
  enum class ExtraConstraintId{StartEta=0, EndEta=1, EndPos=2, EndFrame=3};
  ExtraConstraint(ExtraConstraintId id);
  ExtraConstraint(int actives);
  int actives=0;
};

#endif // EXTRACONSTRAINT_H
