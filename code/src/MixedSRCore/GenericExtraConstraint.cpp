/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "GenericExtraConstraint.hpp"

GenericExtraConstraint::GenericExtraConstraint(int constraintId):
  constraintId(constraintId)
{
}

int GenericExtraConstraint::offset()
{
  return constraintId;
}

void GenericExtraConstraint::getLowerBound(Eigen::VectorXr& inpV)
{
  for(int i=0;i<size();i++)
    inpV[constraintId+i]=0.;
}

void GenericExtraConstraint::getUpperBound(Eigen::VectorXr& inpV)
{
  for(int i=0;i<size();i++)
    inpV[constraintId+i]=0.;
}
