/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef GENERIC_EXTRA_CONSTRAINT
#define GENERIC_EXTRA_CONSTRAINT

#include <BaseCore/real.hpp>
#include <map>
class MixedPhY;

class GenericExtraConstraint{
public:
  GenericExtraConstraint(int constraintId);
  virtual ~GenericExtraConstraint() = default;
  //using Point=MixedPhY::Point;
  using Point=std::pair<int,int>;
  virtual int size() = 0;
  int offset();//constraintId
  virtual void eval(const MixedPhY& phy, Eigen::VectorXr& allConstraints) = 0;
  virtual void jacobian(const MixedPhY& phy, std::map<Point, real> &liste) = 0;
  virtual void jacobianStruct(const MixedPhY& phy, std::map<Point, int> &liste) = 0;
  virtual void hessianStruct(const MixedPhY& phy, std::map<Point, int> &liste) = 0;
  virtual void hessian(const MixedPhY& phy, const Eigen::VectorXr& scalar, std::map<Point, real> &liste) = 0;
  virtual void getUpperBound(Eigen::VectorXr& inpV);
  virtual void getLowerBound(Eigen::VectorXr& inpV);
protected:
  const int constraintId;
};

#endif
