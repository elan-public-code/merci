﻿/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef MIXEDPHY_H
#define MIXEDPHY_H

#include <MixedSRCore/MixedSuperRibbon.hpp>
#include "ExtraConstraint.hpp"
#include "GenericExtraConstraint.hpp"
#include <BaseCore/RepulsionField.hpp>
#include <vector>
#include <map>
#include <unordered_map>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <memory>

struct Pressure{
  Eigen::Vector3r position;
  Eigen::Vector3r normale;
  real distance;
  real k;
};


class MixedPhY
{
public:
  MixedPhY(MixedSuperRibbon msr, bool disableClamping = false);
//   MixedPhY(MixedSuperRibbon msr, Eigen::Matrix<bool, 3, 1> keepPosClamp, Eigen::Matrix<bool, 3, 3> keepFrameClamp);
  ~MixedPhY();
  using Point=std::pair<int,int>;
  //void set(MixedSuperRibbon msr);
  MixedSuperRibbon get();
  const MixedSuperRibbon& getConst() const;
  void setGravity(Eigen::Vector3r grav);
  Eigen::Vector3r getGravity();
  void setPoint(Eigen::VectorXr x);
  Eigen::VectorXr  getPoint();
  Eigen::VectorXr  getUpperBound();
  Eigen::VectorXr  getLowerBound();
  Eigen::VectorXr  getConstraintsUpperBound();
  Eigen::VectorXr  getConstraintsLowerBound();
  
  void tryBetterInitPoint();
  
  real getGravityEnergy();
  real getElasticEnergy();
  real getPressureEnergy();
  real getRepulsionEnergy();
  Eigen::VectorXr getRepulsionEnergyPerField();
  Eigen::VectorXr getElasticEnergyGradient();
  Eigen::VectorXr getGravityEnergyGradient();
  Eigen::VectorXr getPressureEnergyGradient();
  Eigen::VectorXr getRepulsionEnergyGradient();
  
  Eigen::VectorXr constraints();
  void fillConstraintsJac(std::map<Point, real>& liste);//The list must be fill with 0 at existing points
  void fillConstraintsJacStruct(std::map<Point, int>& liste);
  
  void fillConstraintsHessian(std::map<Point, real>& liste, Eigen::VectorXr scalarV);//The list must be fill with 0 at existing points
  void fillConstraintsHessianStruct(std::map<Point, int>& liste);
  void fillGravityHessian(std::map<Point, real>& liste, real scalar = 1.);//The list must be fill with int at existing points
  void fillGravityHessianStruct(std::map<Point, int>& liste);
  void fillElasticHessian(std::map<Point, real>& liste, real scalar = 1.);//The list must be fill with int at existing points
  void fillElasticHessianStruct(std::map<Point, int>& liste);
  void fillPressureHessian(std::map<Point, real>& liste, real scalar = 1.);//The list must be fill with int at existing points
  void fillPressureHessianStruct(std::map<Point, int>& liste);
  void fillRepulsionHessian(std::map<Point, real>& liste, real scalar = 1.);//The list must be fill with int at existing points
  void fillRepulsionHessianStruct(std::map<Point, int>& liste);
  
  std::tuple<int, int, int> addPressure(Pressure pressure);
  std::tuple<int, int, int> addPressureDoubleEnd(Pressure pressure);
  Pressure getPressure(int id) const;
  Eigen::VectorXr getPressureDistances() const;
  
  int addRepulsionField(RepulsionField rp);
  RepulsionField getRepulsionField(int id);
  
  int getNbVars();
  int getNbConstraints();

  
  static int vpointIDPressures(int nbSegments);
  //static int vpointNbSegments(const Eigen::VectorXr &pt);
  static int vpointIDOmega1(int segmentID);
  static int vpointIDEta(int segmentID);
  static int vpointIDPos(int segmentID);
  static int vpointIDFrame(int segmentID);
  
  std::pair<int,int> addConstraintEtaStart();
  std::pair<int,int> addConstraintEtaEnd();
  std::pair<int,int> addConstraintEndPos(Eigen::Vector3r ePos);
  std::pair<int,int> addConstraintEndFrame(Eigen::Matrix3r eFrame);
  std::pair<int,int> addConstraintOffPlane(Eigen::Vector3r pt, Eigen::Vector3r nmle);
  std::pair<int,int> addConstraintOffPlaneDoubleEnd(Eigen::Vector3r pt, Eigen::Vector3r nmle);
  std::pair<int,int> addConstraintClampOrBoundingSphere(Eigen::Vector3r center, real radius);
  std::pair<int,int> addConstraintLoopRibbon();
  std::pair<int,int> addConstraintLoopEta(bool negate);
  std::pair<int,int> addConstraintBarycenterOnAxis(Eigen::Vector3r posAxis, Eigen::Vector3r dirAxis);
  
  std::pair<Eigen::Vector3r, Eigen::Matrix3r> posFrameAtEnd(int segment) const;
  Eigen::Vector3r posAtEnd(int segment) const;
  Eigen::Matrix3r frameAtEnd(int segment) const;
  
  void setRibbonBoundActive(bool value);
private:
  std::pair<int,int> addConstraintPressure(int pressureID);
  std::pair<int,int> addConstraintPressureDoubleEnd(int pressureID);
protected:
  struct RibbonPartSetup {
    uint partnb;
    real s0;
    real sout;
    real smax;
    real segmentLength;
    real ribbonWidth;
    bool lastPart = false;
    LinearScalar omega1;
    LinearScalar n;
    Eigen::Vector3r posOr;
    Eigen::Vector3r posF;
    Eigen::Matrix3r frameOr;
    Eigen::Matrix3r frameF;
  };
  real getElasticEnergy(int segment);
  real getGravityEnergy(int segment);
  Eigen::Vector3r getPreAltitudeEnergy(int segment);
  Eigen::Vector3r chainPreAltitudeEnergyRibbonPart(MixedPhY::RibbonPartSetup &setup);
  void addElasticEnergyGradient(int segment, Eigen::VectorXr &grad);
  void addGravityEnergyGradient(int segment, Eigen::VectorXr &grad);
  
  
Eigen::Matrix<Eigen::Matrix<Eigen::Vector4r, 3, 3>, 3, 1>  d2PreAltitudeEnergydFramedabnm(Eigen::Matrix<Eigen::Vector4r,3,1> gradPreAltitudeRotationalEnergyOut) const;
  
protected:
  void fillGravityHessianSegment(int segmentId, std::map<Point, real>& liste, real scalar = 1.);//The list must be fill with 0 at existing points
  void fillGravityHessianSegmentStruct(int segmentId, std::map<Point, int>& liste);
  void fillElasticHessianSegment(int segmentId, std::map<Point, real>& liste, real scalar = 1.);//The list must be fill with 0 at existing points
  void fillElasticHessianSegmentStruct(int segmentId, std::map<Point, int>& liste);
  
protected:
  MixedSuperRibbon sr;
  Eigen::Vector3r G = Eigen::Vector3r(0., 0., 10.);
  std::vector<std::shared_ptr<GenericExtraConstraint>> theConstraints;
  std::vector<Pressure> thePressures;
  std::vector<RepulsionField> theRepulsionFields;
  bool desactivateRibbonBound=false;
  //ExtraConstraint extraConstraints;
public:
  static real alphaConstraintFrame;
  static real alphaConstraintPos;
  static real alphaConstraintEndPos;
  static real alphaConstraintEndFrame;
  static real alphaConstraintBarycenter;
  
  
  
public://For computations
  
  void getPres(int segment,
               Eigen::Matrix<Eigen::Vector4r, 3, 3> &gradPreFrameOut,
               Eigen::Matrix<Eigen::Vector4r, 3, 1> &gradPreTranslationOut,
               Eigen::Matrix<Eigen::Vector4r, 3, 1> &gradPreAltitudeRotationalEnergyOut,
               Eigen::Matrix3r &preFrame,
               Eigen::Vector3r &preTranslation
  ) const;
  
  void getGradsForGravityEnergy(int segment,
                                Eigen::Matrix<Eigen::Vector4r, 3, 1> &gradPreAltitudeEnergyOut,
                                Eigen::Matrix<Eigen::Vector3r, 3, 1> &dPreAltitudeEnergydPosOut,
                                Eigen::Matrix<Eigen::Matrix3r, 3, 1> &dPreAltitudeEnergydFrameOut,
                                Eigen::Matrix<Eigen::Matrix3r, 3, 3> &dFramedFrameOut,
                                Eigen::Matrix<Eigen::Matrix3r, 3, 1> &dPosdFrameOut,
                                Eigen::Matrix<Eigen::Vector3r, 3, 1> &dPosdPosOut,
                                Eigen::Matrix<Eigen::Vector4r, 3, 3> &gradFrameOut,
                                Eigen::Matrix<Eigen::Vector4r, 3, 1> &gradPosOut,
                                Eigen::Vector3r& posOut,
                                Eigen::Matrix3r& frameOut
                               ) const;
  
  void getSecondOrder(int segment,
                                Eigen::Matrix<Eigen::Matrix4r, 3, 1> &hessianPreAltitudeEnergyOut,
                                Eigen::Matrix<Eigen::Matrix4r, 3, 1> &hessianPosOut,
                                Eigen::Matrix<Eigen::Matrix4r, 3, 3> &hessianFrameOut,
                                Eigen::Matrix<Eigen::Vector4r, 3, 1> &gradPreAltitudeEnergyOut,
                                Eigen::Matrix<Eigen::Vector3r, 3, 1> &dPreAltitudeEnergydPosOut,
                                Eigen::Matrix<Eigen::Matrix3r, 3, 1> &dPreAltitudeEnergydFrameOut,
                                Eigen::Matrix<Eigen::Matrix3r, 3, 3> &dFramedFrameOut,
                                Eigen::Matrix<Eigen::Matrix3r, 3, 1> &dPosdFrameOut,
                                Eigen::Matrix<Eigen::Vector3r, 3, 1> &dPosdPosOut,
                                Eigen::Matrix<Eigen::Vector4r, 3, 3> &gradFrameOut,
                                Eigen::Matrix<Eigen::Vector4r, 3, 1> &gradPosOut,
                                Eigen::Vector3r& posOut,
                                Eigen::Matrix3r& frameOut
                               ) const;
  Eigen::Matrix<Eigen::Matrix3r, 3, 3> getdFramedFrame(Eigen::Matrix3r preFrame) const;
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> getdPosdFrame(Eigen::Vector3r preTrans) const;
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> getdPreAltdFrame(Eigen::Vector3r preAltitudeRotationalEnergy) const;
};

#endif // MIXEDPHY_H
