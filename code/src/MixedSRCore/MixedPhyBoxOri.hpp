/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef MIXEDPHYBOXORI_H
#define MIXEDPHYBOXORI_H

#include <MixedSRCore/MixedPhY.hpp>

class MixedPhyBoxOri
{
public:
  using Point=MixedPhY::Point;
  MixedPhyBoxOri(MixedSuperRibbon msr);
  MixedPhyBoxOri(MixedPhY msr);
  //void set(MixedSuperRibbon msr);
  //void set(MixedPhY msr); 
  MixedSuperRibbon get();
  void setGravity(Eigen::Vector3r grav);
  Eigen::Vector3r getGravity();
  void setPoint(Eigen::VectorXr x);
  Eigen::VectorXr  getPoint();
  Eigen::VectorXr  getUpperBound();
  Eigen::VectorXr  getLowerBound();
  Eigen::VectorXr  getConstraintsUpperBound();
  Eigen::VectorXr  getConstraintsLowerBound();
  
  void tryBetterInitPoint();
  
  real getGravityEnergy();
  real getElasticEnergy();
  real getPressureEnergy();
  real getRepulsionEnergy();
  Eigen::VectorXr getElasticEnergyGradient();
  Eigen::VectorXr getGravityEnergyGradient();
  Eigen::VectorXr getPressureEnergyGradient();
  Eigen::VectorXr getRepulsionEnergyGradient();
  
  Eigen::VectorXr constraints();
  void fillConstraintsJac(std::map<Point, real>& liste);//The list must be fill with 0 at existing points
  void fillConstraintsJacStruct(std::map<Point, int>& liste);
  
  void fillConstraintsHessian(std::map<Point, real>& liste, Eigen::VectorXr scalarV);//The list must be fill with 0 at existing points
  void fillConstraintsHessianStruct(std::map<Point, int>& liste);
  void fillGravityHessian(std::map<Point, real>& liste, real scalar = 1.);//The list must be fill with int at existing points
  void fillGravityHessianStruct(std::map<Point, int>& liste);
  void fillElasticHessian(std::map<Point, real>& liste, real scalar = 1.);//The list must be fill with int at existing points
  void fillElasticHessianStruct(std::map<Point, int>& liste);
  void fillPressureHessian(std::map<Point, real>& liste, real scalar = 1.);//The list must be fill with int at existing points
  void fillPressureHessianStruct(std::map<Point, int>& liste);
  
  void fillRepulsionHessian(std::map<Point, real>& liste, real scalar = 1.);//The list must be fill with int at existing points
  void fillRepulsionHessianStruct(std::map<Point, int>& liste);
  
  int getNbVars();
  int getNbConstraints();
  
  Eigen::VectorXr getPressureDistances() const;
  Eigen::VectorXr getRepulsionEnergyPerField();
  
  std::pair<Eigen::Vector3r, Eigen::Matrix3r> posFrameAtEnd(int segment);
protected:
  inline Eigen::VectorXr cutClampingPt(Eigen::VectorXr ori);
  inline Eigen::VectorXr cutClampingConstr(Eigen::VectorXr ori);
private:
  MixedPhY phy;
  Eigen::VectorXr point;
};

#endif // MIXEDPHYBOXORI_H
