/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "../MixedPhY.hpp"
#include "ElasticEnergy.hpp"
#include "SuperRibbonGeometrieSerie.hpp"
#include "MathOperations.hpp"
#include "Constraints/ConstraintClampingOr.hpp"
#include "Constraints/ConstraintLinkParams.hpp"
#include "Constraints/ConstraintLinkPosFrame.hpp"
#include "Constraints/ConstraintFrameInSO3.hpp"

using namespace MathOperations;

int MixedPhY::getNbVars()
{
  return 16*sr.getNbSegments()+thePressures.size();
}

int MixedPhY::vpointIDPressures(int nbSegments)
{
  return 16*nbSegments;
}

int MixedPhY::vpointIDOmega1(int segmentID)
{
  return 16 * segmentID + 12;
}

int MixedPhY::vpointIDEta(int segmentID)
{
  return 16 * segmentID + 14;
}

int MixedPhY::vpointIDPos(int segmentID)
{
  return 16 * segmentID + 0;
}

int MixedPhY::vpointIDFrame(int segmentID)
{
  return 16 * segmentID + 3;
}

//void MixedPhY::set(MixedSuperRibbon msr)
MixedPhY::MixedPhY(MixedSuperRibbon msr, bool disableClamping)
{
  sr = msr;
  int constraintsOffset = 0;
  if(!disableClamping)
  {
    theConstraints.push_back(std::shared_ptr<GenericExtraConstraint>(new ConstraintClampingOr(constraintsOffset, sr.getPosStart(0), sr.getFrameStart(0), alphaConstraintPos, alphaConstraintFrame)));
    constraintsOffset += theConstraints.back()->size();
  }
  else
  {
    theConstraints.push_back(std::shared_ptr<GenericExtraConstraint>(new ConstraintFrameInSO3(constraintsOffset)));
    constraintsOffset += theConstraints.back()->size();
  }
  int t=sr.getNbSegments();
  for(int i=0;i<t-1;i++)
  {
    theConstraints.push_back( std::shared_ptr<GenericExtraConstraint>(new ConstraintLinkParams(constraintsOffset,i)));
    constraintsOffset += theConstraints.back()->size();
    theConstraints.push_back( std::shared_ptr<GenericExtraConstraint>(new ConstraintLinkPosFrame(constraintsOffset,i,alphaConstraintPos,alphaConstraintFrame)));
    constraintsOffset += theConstraints.back()->size();
  }
}

MixedSuperRibbon MixedPhY::get()
{
  return sr;
}

const MixedSuperRibbon & MixedPhY::getConst() const
{
  return sr;
}


void MixedPhY::setGravity(Eigen::Vector3r grav)
{
  G = -grav;
}

Eigen::Vector3r MixedPhY::getGravity()
{
  return G;
}

void MixedPhY::tryBetterInitPoint()
{
  int t = sr.getNbSegments() - 1;
  for (int segment = 0; segment < t; segment++) {
    real l = sr.getLength(segment);
    SuperRibbonGeometrieSerie ribbon;
    Eigen::Vector3r pos = sr.getPosStart(segment);
    Eigen::Matrix3r frame = sr.getFrameStart(segment);
    LinearScalar omega1 = sr.getOmega1(segment);
    LinearScalar eta = sr.getEta(segment);
    LinearScalar omega1Or = sr.getOmega1(segment);
    LinearScalar etaOr = sr.getEta(segment);
    real s0 = 0.;
    ribbon.Place(pos, frame);
    ribbon.Setup(omega1, eta);
    while (s0 < l) {
      real smax = ribbon.estimSMax();
      if (s0 + smax > l) {
        smax = l - s0;
        std::tie(pos, frame) = ribbon.PosFrame(smax);
        break;
      }
      std::tie(pos, frame) = ribbon.PosFrame(smax);
      s0 += smax;
      ribbon.Place(pos, frame);
      omega1.B = omega1Or.B + s0 * omega1Or.A;
      eta.B = etaOr.B + s0 * etaOr.A;
      ribbon.Setup(omega1, eta);
    }
    sr.setPosStart(segment + 1, pos);
    sr.setFrameStart(segment + 1, frame);
  }
}

Eigen::VectorXr MixedPhY::getPoint()
{
  int t = sr.getNbSegments();
  Eigen::VectorXr res(getNbVars());
  for (int i = 0; i < t; i++) {
    res[vpointIDOmega1(i)] = sr.getOmega1(i).A;
    res[vpointIDOmega1(i) + 1] = sr.getOmega1(i).B;
    res[vpointIDEta(i)] = sr.getEta(i).A;
    res[vpointIDEta(i) + 1] = sr.getEta(i).B;
    res.segment<3>(vpointIDPos(i)) = sr.getPosStart(i);
    Eigen::Matrix3r f = sr.getFrameStart(i);
    res.segment<9>(vpointIDFrame(i)) << f(0, 0), f(0, 1), f(0, 2), f(1, 0), f(1, 1), f(1, 2), f(2, 0), f(2, 1), f(2, 2);
  }
  int offset=vpointIDPressures(t);
  t=thePressures.size();
  for(int i=0;i<t;i++)
    res[offset+i]=thePressures[i].distance;
  return res;
}

void MixedPhY::setPoint(Eigen::VectorXr x)
{
  assert(x.size()==getNbVars());
  int t = sr.getNbSegments();
  for (int i = 0; i < t; i++) {
    sr.setOmega1(i, LinearScalar(x[vpointIDOmega1(i)], x[vpointIDOmega1(i) + 1]));
    sr.setEta(i, LinearScalar(x[vpointIDEta(i)], x[vpointIDEta(i) + 1]));
    sr.setPosStart(i, x.segment<3>(vpointIDPos(i)));
    Eigen::Matrix3r f;
    for (int j = 0; j < 9; j++)f(j / 3, j % 3) = x[vpointIDFrame(i) + j];
    sr.setFrameStart(i, f);
  }
  int offset=vpointIDPressures(t);
  t=thePressures.size();
  for(int i=0;i<t;i++)
    thePressures[i].distance=x[offset+i];
}

Eigen::VectorXr MixedPhY::getLowerBound()
{
  real lb = -2. / sr.getWidth();
  int t = sr.getNbSegments();
  Eigen::VectorXr pt(getNbVars());
  pt.setConstant(-std::numeric_limits<real>::infinity());
  if(!desactivateRibbonBound) {
    for (int i = 0; i < t; i++) {
      pt[vpointIDEta(i)] = lb;
    }
  }
  return pt;
}

Eigen::VectorXr MixedPhY::getUpperBound()
{
  real ub = 2. / sr.getWidth();
  int t = sr.getNbSegments();
  Eigen::VectorXr pt(getNbVars());
  pt.setConstant(std::numeric_limits<real>::infinity());
  if(!desactivateRibbonBound) {
    for (int i = 0; i < t; i++) {
      pt[vpointIDEta(i)] = ub;
    }
  }
  return pt;
}

void MixedPhY::setRibbonBoundActive(bool value)
{
  desactivateRibbonBound=!value;
}

real MixedPhY::getElasticEnergy()
{
  int t = sr.getNbSegments();
  real res = 0.;
  for (int i = 0; i < t; i++)
    res += getElasticEnergy(i);
  return res;
}

real MixedPhY::getGravityEnergy()
{
  int t = sr.getNbSegments();
  real res = 0.;
  for (int i = 0; i < t; i++)
    res += getGravityEnergy(i);
  return res;
}

real MixedPhY::getElasticEnergy(int segment)
{
  ElasticEnergy el;
  el.FixPhysics(sr.getD(), sr.getThickness(), sr.getWidth(), sr.getLength(segment), sr.getPoissonRatio().value_or(0.5));
  el.FixPt(sr.getOmega1(segment), sr.getEta(segment), sr.getNatOmega1(segment));
  return el.computeEnergy();
}

real MixedPhY::getGravityEnergy(int segment)
{
  real eg = getPreAltitudeEnergy(segment).dot(G);
  eg *= sr.getAreaDensity();
  return eg;
}

Eigen::Vector3r MixedPhY::getPreAltitudeEnergy(int segment)
{
  Eigen::Vector3r eg(0., 0., 0.);
  LinearScalar w = sr.getOmega1(segment);
  LinearScalar n = sr.getEta(segment);
  RibbonPartSetup rps;
  rps.frameOr = sr.getFrameStart(segment);
  rps.posOr = sr.getPosStart(segment);
  rps.omega1 = sr.getOmega1(segment);
  rps.n = sr.getEta(segment);

  //std::cout<<"w: "<<rps.omega1<<std::endl<<"n: "<<rps.n<<std::endl<<"f: "<<rps.frameOr<<std::endl<<"p: "<<rps.posOr.transpose()<<std::endl<<std::endl;

  rps.segmentLength = sr.getLength(segment);
  rps.ribbonWidth = sr.getWidth();
  rps.s0 = 0.;
  do {
    eg += chainPreAltitudeEnergyRibbonPart(rps);
    rps.frameOr = rps.frameF;
    rps.posOr = rps.posF;
    rps.s0 = rps.sout;
    rps.omega1.B = w.B + rps.s0 * w.A;
    rps.omega1.A = w.A;
    rps.n.B = n.B + rps.s0 * n.A;
    rps.n.A = n.A;
    rps.partnb++;
  } while (!rps.lastPart);
  return eg;
}

Eigen::Vector3r MixedPhY::chainPreAltitudeEnergyRibbonPart(MixedPhY::RibbonPartSetup &setup)
{
  SuperRibbonGeometrieSerie ribbon;
  ribbon.Place(setup.posOr, setup.frameOr);
  ribbon.Setup(setup.omega1, setup.n);
  ribbon.SetRibbonWidth(setup.ribbonWidth);
  setup.smax = ribbon.estimSMax();
  real s1 = setup.smax;
  if (s1 + setup.s0 >= setup.segmentLength) {
    s1 = setup.segmentLength - setup.s0;
    setup.lastPart = true;
  }
  setup.sout = setup.s0 + s1;
  std::tie(setup.posF, setup.frameF) = ribbon.PosFrame(s1);
  return ribbon.preAltitudeEnergy(s1);
}

void MixedPhY::addElasticEnergyGradient(int segment, Eigen::VectorXr &grad)
{
  ElasticEnergy el;
  el.FixPhysics(sr.getD(), sr.getThickness(), sr.getWidth(), sr.getLength(segment), sr.getPoissonRatio().value_or(0.5));
  el.FixPt(sr.getOmega1(segment), sr.getEta(segment), sr.getNatOmega1(segment));
  Eigen::Vector4r localVec = el.computeGrad();
  grad[vpointIDOmega1(segment)] += localVec[0];
  grad[vpointIDOmega1(segment) + 1] += localVec[1];
  grad[vpointIDEta(segment)] += localVec[2];
  grad[vpointIDEta(segment) + 1] += localVec[3];
}

Eigen::VectorXr MixedPhY::getElasticEnergyGradient()
{
  int t = sr.getNbSegments();
  Eigen::VectorXr grad(getNbVars());
  grad.setZero();
  for (int i = 0; i < t; i++)
    addElasticEnergyGradient(i, grad);
  return grad;
}

Eigen::VectorXr MixedPhY::getGravityEnergyGradient()
{
  int t = sr.getNbSegments();
  Eigen::VectorXr grad(getNbVars());
  grad.setZero();
  for (int i = 0; i < t; i++)
    addGravityEnergyGradient(i, grad);
  return grad;
}

void MixedPhY::addGravityEnergyGradient(int segment, Eigen::VectorXr &grad)
{
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPreAltitudeEnergyOut;
  Eigen::Matrix<Eigen::Vector3r, 3, 1> dPreAltitudeEnergydPosOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPreAltitudeEnergydFrameOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 3> dFramedFrameOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPosdFrameOut;
  Eigen::Matrix<Eigen::Vector3r, 3, 1> dPosdPosOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 3> gradFrameOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPosOut;
  Eigen::Vector3r posOut;
  Eigen::Matrix3r frameOut;
  getGradsForGravityEnergy(segment, gradPreAltitudeEnergyOut, dPreAltitudeEnergydPosOut, dPreAltitudeEnergydFrameOut, dFramedFrameOut, dPosdFrameOut, dPosdPosOut, gradFrameOut, gradPosOut, posOut, frameOut);
  real ad = sr.getAreaDensity();
  grad.segment<4>(vpointIDOmega1(segment)) = ad * contractGeneralDotP(G, gradPreAltitudeEnergyOut);
  grad.segment<3>(vpointIDPos(segment)) = ad * contractGeneralDotP(G, dPreAltitudeEnergydPosOut);
  Eigen::Matrix3r mfrtmp = ad * contractGeneralDotP(G, dPreAltitudeEnergydFrameOut);
  for (int j = 0; j < 9; j++)grad[vpointIDFrame(segment) + j] = mfrtmp(j / 3, j % 3);
}

void MixedPhY::getGradsForGravityEnergy(int segment,
                                        Eigen::Matrix<Eigen::Vector4r, 3, 1> &gradPreAltitudeEnergyOut,
                                        Eigen::Matrix<Eigen::Vector3r, 3, 1> &dPreAltitudeEnergydPosOut,
                                        Eigen::Matrix<Eigen::Matrix3r, 3, 1> &dPreAltitudeEnergydFrameOut,
                                        Eigen::Matrix<Eigen::Matrix3r, 3, 3> &dFramedFrameOut,
                                        Eigen::Matrix<Eigen::Matrix3r, 3, 1> &dPosdFrameOut,
                                        Eigen::Matrix<Eigen::Vector3r, 3, 1> &dPosdPosOut,
                                        Eigen::Matrix<Eigen::Vector4r, 3, 3> &gradFrameOut,
                                        Eigen::Matrix<Eigen::Vector4r, 3, 1> &gradPosOut,
                                        Eigen::Vector3r &posOut,
                                        Eigen::Matrix3r &frameOut) const
{
  Eigen::Matrix3r frame = sr.getFrameStart(segment);
  Eigen::Vector3r pos = sr.getPosStart(segment);
  Eigen::Vector3r preE(0., 0., 0.);
  gradPreAltitudeEnergyOut.setConstant(Eigen::Vector4r::Zero());
  dPreAltitudeEnergydPosOut.setConstant(Eigen::Vector3r::Zero());
  dPreAltitudeEnergydFrameOut.setConstant(Eigen::Matrix3r::Zero());
  {
    dFramedFrameOut.setConstant(Eigen::Matrix3r::Zero());
    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++) {
        dFramedFrameOut(i, j)(i, j) = 1;
      }
    }
  }
  dPosdFrameOut.setConstant(Eigen::Matrix3r::Zero());
  dPosdPosOut = Eigen::Matrix<Eigen::Vector3r, 3, 1>(Eigen::Vector3r(1., 0., 0.), Eigen::Vector3r(0., 1., 0.), Eigen::Vector3r(0., 0., 1.));
  gradFrameOut.setConstant(Eigen::Vector4r::Zero());
  gradPosOut.setConstant(Eigen::Vector4r::Zero());

  LinearScalar w = sr.getOmega1(segment);
  LinearScalar n = sr.getEta(segment);
  real s0 = 0;
  bool lastPart = false;
  real segmentLength = sr.getLength(segment);
  real ribbonWidth = sr.getWidth();
  Eigen::Vector3r posF;
  Eigen::Matrix3r frameF;
  Eigen::Matrix3r preframeF = Eigen::Matrix3r::Identity();
  Eigen::Vector3r preTransl = Eigen::Vector3r(0.,0.,0.);
  Eigen::Vector3r preAltitudeRotationalEnergy = Eigen::Vector3r(0.,0.,0.);
  int partCount = 0;
  while (!lastPart) {
    partCount++;
    SuperRibbonGeometrieSerie ribbon;
    ribbon.Place(pos, frame);
    LinearScalar ssw(w.A, w.B + s0 * w.A);
    LinearScalar ssn(n.A, n.B + s0 * n.A);
    ribbon.Setup(ssw, ssn);
    ribbon.SetRibbonWidth(sr.getWidth());
    real s1 = ribbon.estimSMax();
    if (s1 + s0 >= segmentLength) {
      s1 = segmentLength - s0;
      lastPart = true;
    }
    std::tie(posF, frameF) = ribbon.PosFrame(s1);
    Eigen::Matrix<real, 3, 4> gradPreAltitudeEnergySegment = ribbon.gradPreAltitudeEnergy(s1);
    Eigen::Matrix<Eigen::Vector3r, 3, 1> dPreAltitudeEnergydPosSegment = ribbon.dPreAltitudeEnergydPos(s1);
    Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPreAltitudeEnergydFrameSegment = ribbon.dPreAltitudeEnergydFrame(s1);
    Eigen::Matrix<Eigen::Matrix3r, 3, 3> dFramedFrameSegment = ribbon.dFramedFrame(s1);
    Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPosdFrameSegment = ribbon.dPosdFrame(s1);
    Eigen::Matrix<Eigen::Vector3r, 3, 1> dPosdPosSegment = ribbon.dPosdPos(s1);
    preE += ribbon.preAltitudeEnergy(s1);


    Eigen::Matrix3r dFdaSegment = ribbon.dFrameda(s1);
    Eigen::Matrix3r dFdbSegment = ribbon.dFramedb(s1);
    Eigen::Matrix3r dFdnSegment = ribbon.dFramedn(s1);
    Eigen::Matrix3r dFdmSegment = ribbon.dFramedm(s1);
    Eigen::Vector3r dPdaSegment = ribbon.dPosda(s1);
    Eigen::Vector3r dPdbSegment = ribbon.dPosdb(s1);
    Eigen::Vector3r dPdnSegment = ribbon.dPosdn(s1);
    Eigen::Vector3r dPdmSegment = ribbon.dPosdm(s1);

    dFdaSegment += s0 * dFdbSegment;
    dFdnSegment += s0 * dFdmSegment;
    dPdaSegment += s0 * dPdbSegment;
    dPdnSegment += s0 * dPdmSegment;
    gradPreAltitudeEnergySegment.col(0) += s0 * gradPreAltitudeEnergySegment.col(1);
    gradPreAltitudeEnergySegment.col(2) += s0 * gradPreAltitudeEnergySegment.col(3);

    for (int k = 0; k < 3; k++) gradPreAltitudeEnergyOut[k] += gradPreAltitudeEnergySegment.row(k);
    gradPreAltitudeEnergyOut += compDeriv(dPreAltitudeEnergydFrameSegment, gradFrameOut) + compDeriv(dPreAltitudeEnergydPosSegment, gradPosOut);

    gradPosOut = compDeriv(dPosdPosSegment, gradPosOut) + compDeriv(dPosdFrameSegment, gradFrameOut);
    gradFrameOut = compDeriv(dFramedFrameSegment, gradFrameOut);
    for (int a = 0; a < 3; a++) {
      for (int b = 0; b < 3; b++) {
        gradFrameOut(a, b)[0] += dFdaSegment(a, b);
        gradFrameOut(a, b)[1] += dFdbSegment(a, b);
        gradFrameOut(a, b)[2] += dFdnSegment(a, b);
        gradFrameOut(a, b)[3] += dFdmSegment(a, b);
      }
      gradPosOut(a)[0] += dPdaSegment(a);
      gradPosOut(a)[1] += dPdbSegment(a);
      gradPosOut(a)[2] += dPdnSegment(a);
      gradPosOut(a)[3] += dPdmSegment(a);
    }

    s0 += s1;
    real surface = s0 * ribbonWidth;
    preAltitudeRotationalEnergy+=s1*ribbonWidth*preTransl+ preframeF*ribbon.preAltitudeRotationalEnergy(s1);
    preTransl+=preframeF*ribbon.PreTranslation(s1);
    preframeF *= ribbon.PreFrame(s1);
    dPreAltitudeEnergydFrameOut = getdPreAltdFrame(preAltitudeRotationalEnergy);
    dPreAltitudeEnergydPosOut = Eigen::Matrix<Eigen::Vector3r, 3, 1>(Eigen::Vector3r(surface, 0., 0.), Eigen::Vector3r(0., surface, 0.), Eigen::Vector3r(0., 0., surface));
    frame = frameF;
    pos = posF;
    dFramedFrameOut = getdFramedFrame(preframeF);
    dPosdFrameOut = getdPosdFrame(preTransl);
  }
  real surface = segmentLength * ribbonWidth;
  dPreAltitudeEnergydPosOut = Eigen::Matrix<Eigen::Vector3r, 3, 1>(Eigen::Vector3r(surface, 0., 0.), Eigen::Vector3r(0., surface, 0.), Eigen::Vector3r(0., 0., surface));
  posOut = pos;
  frameOut = frame;
}

void MixedPhY::getSecondOrder(int segment,
                              Eigen::Matrix<Eigen::Matrix4r, 3, 1> &hessianPreAltitudeEnergyOut,
                              Eigen::Matrix<Eigen::Matrix4r, 3, 1> &hessianPosOut,
                              Eigen::Matrix<Eigen::Matrix4r, 3, 3> &hessianFrameOut,
                              Eigen::Matrix<Eigen::Vector4r, 3, 1> &gradPreAltitudeEnergyOut,
                              Eigen::Matrix<Eigen::Vector3r, 3, 1> &dPreAltitudeEnergydPosOut,
                              Eigen::Matrix<Eigen::Matrix3r, 3, 1> &dPreAltitudeEnergydFrameOut,
                              Eigen::Matrix<Eigen::Matrix3r, 3, 3> &dFramedFrameOut,
                              Eigen::Matrix<Eigen::Matrix3r, 3, 1> &dPosdFrameOut,
                              Eigen::Matrix<Eigen::Vector3r, 3, 1> &dPosdPosOut,
                              Eigen::Matrix<Eigen::Vector4r, 3, 3> &gradFrameOut,
                              Eigen::Matrix<Eigen::Vector4r, 3, 1> &gradPosOut,
                              Eigen::Vector3r &posOut,
                              Eigen::Matrix3r &frameOut) const
{
  Eigen::Matrix3r frame = sr.getFrameStart(segment);
  Eigen::Vector3r pos = sr.getPosStart(segment);
  Eigen::Vector3r preE(0., 0., 0.);
  gradPreAltitudeEnergyOut.setConstant(Eigen::Vector4r::Zero());
  dPreAltitudeEnergydPosOut.setConstant(Eigen::Vector3r::Zero());
  dPreAltitudeEnergydFrameOut.setConstant(Eigen::Matrix3r::Zero());
  {
    dFramedFrameOut.setConstant(Eigen::Matrix3r::Zero());
    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++) {
        dFramedFrameOut(i, j)(i, j) = 1;
      }
    }
  }
  dPosdFrameOut.setConstant(Eigen::Matrix3r::Zero());
  dPosdPosOut = Eigen::Matrix<Eigen::Vector3r, 3, 1>(Eigen::Vector3r(1., 0., 0.), Eigen::Vector3r(0., 1., 0.), Eigen::Vector3r(0., 0., 1.));
  gradFrameOut.setConstant(Eigen::Vector4r::Zero());
  gradPosOut.setConstant(Eigen::Vector4r::Zero());
  hessianPreAltitudeEnergyOut.setConstant(Eigen::Matrix4r::Zero());
  hessianPosOut.setConstant(Eigen::Matrix4r::Zero());
  hessianFrameOut.setConstant(Eigen::Matrix4r::Zero());

  LinearScalar w = sr.getOmega1(segment);
  LinearScalar n = sr.getEta(segment);
  real s0 = 0;
  bool lastPart = false;
  real segmentLength = sr.getLength(segment);
  real ribbonWidth = sr.getWidth();
  Eigen::Vector3r posF;
  Eigen::Matrix3r frameF;
  Eigen::Matrix3r preframeF = Eigen::Matrix3r::Identity();
  Eigen::Vector3r preTransl = Eigen::Vector3r(0.,0.,0.);
  Eigen::Vector3r preAltitudeRotationalEnergy = Eigen::Vector3r(0.,0.,0.);
  int partCount = 0;
  while (!lastPart) {
    partCount++;
    SuperRibbonGeometrieSerie ribbon;
    ribbon.Place(pos, frame);
    LinearScalar ssw(w.A, w.B + s0 * w.A);
    LinearScalar ssn(n.A, n.B + s0 * n.A);
    ribbon.Setup(ssw, ssn);
    ribbon.SetRibbonWidth(sr.getWidth());
    real s1 = ribbon.estimSMax();
    if (s1 + s0 >= segmentLength) {
      s1 = segmentLength - s0;
      lastPart = true;
    }
    std::tie(posF, frameF) = ribbon.PosFrame(s1);
    Eigen::Matrix<real, 3, 4> gradPreAltitudeEnergySegment = ribbon.gradPreAltitudeEnergy(s1);
    Eigen::Matrix<Eigen::Vector3r, 3, 1> dPreAltitudeEnergydPosSegment = ribbon.dPreAltitudeEnergydPos(s1);
    Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPreAltitudeEnergydFrameSegment = ribbon.dPreAltitudeEnergydFrame(s1);
    Eigen::Matrix<Eigen::Matrix3r, 3, 3> dFramedFrameSegment = ribbon.dFramedFrame(s1);
    Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPosdFrameSegment = ribbon.dPosdFrame(s1);
    Eigen::Matrix<Eigen::Vector3r, 3, 1> dPosdPosSegment = ribbon.dPosdPos(s1);
    preE += ribbon.preAltitudeEnergy(s1);


    Eigen::Matrix3r dFdaSegment = ribbon.dFrameda(s1);
    Eigen::Matrix3r dFdbSegment = ribbon.dFramedb(s1);
    Eigen::Matrix3r dFdnSegment = ribbon.dFramedn(s1);
    Eigen::Matrix3r dFdmSegment = ribbon.dFramedm(s1);
    Eigen::Vector3r dPdaSegment = ribbon.dPosda(s1);
    Eigen::Vector3r dPdbSegment = ribbon.dPosdb(s1);
    Eigen::Vector3r dPdnSegment = ribbon.dPosdn(s1);
    Eigen::Vector3r dPdmSegment = ribbon.dPosdm(s1);

    dFdaSegment += s0 * dFdbSegment;
    dFdnSegment += s0 * dFdmSegment;
    dPdaSegment += s0 * dPdbSegment;
    dPdnSegment += s0 * dPdmSegment;
    gradPreAltitudeEnergySegment.col(0) += s0 * gradPreAltitudeEnergySegment.col(1);
    gradPreAltitudeEnergySegment.col(2) += s0 * gradPreAltitudeEnergySegment.col(3);


    Eigen::Matrix<Eigen::Matrix4r, 3, 1> hessianPreAltitudeEnergy;
    Eigen::Matrix<Eigen::Matrix4r, 3, 1> hessianPos;
    Eigen::Matrix<Eigen::Matrix4r, 3, 3> hessianFrame;

    Eigen::Matrix<Eigen::Matrix<Eigen::Vector4r, 3, 3>, 3, 1> d2PosdFramedabnm = ribbon.d2PosdFramedabnm(s1);
    Eigen::Matrix<Eigen::Matrix<Eigen::Vector4r, 3, 3>, 3, 3> d2FramedFramedabnm = ribbon.d2FramedFramedabnm(s1);
    Eigen::Matrix<Eigen::Matrix<Eigen::Vector4r, 3, 3>, 3, 1> d2PreAltitudeEnergydFramedabnm = ribbon.d2PreAltitudeEnergydFramedabnm(s1);
    Eigen::Matrix<Eigen::Matrix4r, 3, 1> hessianPosSegment = ribbon.hessianPos(s1);
    Eigen::Matrix<Eigen::Matrix4r, 3, 3> hessianFrameSegment = ribbon.hessianFrame(s1);
    Eigen::Matrix<Eigen::Matrix4r, 3, 1> hessianPreAltitudeEnergySegment = ribbon.hessianPreAltitudeEnergy(s1);

    for (int i = 0; i < 3; i++)hessianPos(i) = addTransposeToItself(compDeriv2(d2PosdFramedabnm(i), gradFrameOut));
    for (int i = 0; i < 3; i++)hessianPos(i) += contractGeneralDotP(hessianFrameOut, dPosdFrameSegment(i));
    for (int i = 0; i < 3; i++)hessianPos(i) += contractGeneralDotP(hessianPosOut, dPosdPosSegment(i));
    for (int i = 0; i < 3; i++)hessianPos(i) += hessianPosSegment(i);
    //aa
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 0) += 2. * s0 * hessianPosSegment(i)(0, 1);
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 0) += 2. * s0 * contractGeneralDotP(extractDerivation(d2PosdFramedabnm(i), 1), extractDerivation(gradFrameOut, 0));
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 0) += s0 * s0 * hessianPosSegment(i)(1, 1);
    //ab
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 1) += s0 * hessianPosSegment(i)(1, 1);
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 1) += s0 * contractGeneralDotP(extractDerivation(d2PosdFramedabnm(i), 1), extractDerivation(gradFrameOut, 1));
    //an
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 2) += s0 * (hessianPosSegment(i)(1, 2) + hessianPosSegment(i)(0, 3));
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 2) += s0 * (contractGeneralDotP(extractDerivation(d2PosdFramedabnm(i), 1), extractDerivation(gradFrameOut, 2))
          + contractGeneralDotP(extractDerivation(d2PosdFramedabnm(i), 3), extractDerivation(gradFrameOut, 0)));
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 2) += s0 * s0 * hessianPosSegment(i)(1, 3);
    //na
    for (int i = 0; i < 3; i++)hessianPos(i)(2, 0) = hessianPos(i)(0, 2);
    //am
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 3) += s0 * hessianPosSegment(i)(1, 3);
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 3) += s0 * contractGeneralDotP(extractDerivation(d2PosdFramedabnm(i), 1), extractDerivation(gradFrameOut, 3));
    //ma
    for (int i = 0; i < 3; i++)hessianPos(i)(3, 0) = hessianPos(i)(0, 3);
    //nb
    for (int i = 0; i < 3; i++)hessianPos(i)(2, 1) += s0 * hessianPosSegment(i)(3, 1);
    for (int i = 0; i < 3; i++)hessianPos(i)(2, 1) += s0 * contractGeneralDotP(extractDerivation(d2PosdFramedabnm(i), 3), extractDerivation(gradFrameOut, 1));
    //bn
    for (int i = 0; i < 3; i++)hessianPos(i)(1, 2) = hessianPos(i)(2, 1);
    //nn
    for (int i = 0; i < 3; i++)hessianPos(i)(2, 2) += 2. * s0 * hessianPosSegment(i)(2, 3);
    for (int i = 0; i < 3; i++)hessianPos(i)(2, 2) += 2. * s0 * contractGeneralDotP(extractDerivation(d2PosdFramedabnm(i), 3), extractDerivation(gradFrameOut, 2));
    for (int i = 0; i < 3; i++)hessianPos(i)(2, 2) += s0 * s0 * hessianPosSegment(i)(3, 3);
    //nm
    for (int i = 0; i < 3; i++)hessianPos(i)(2, 3) += s0 * hessianPosSegment(i)(3, 3);
    for (int i = 0; i < 3; i++)hessianPos(i)(2, 3) += s0 * contractGeneralDotP(extractDerivation(d2PosdFramedabnm(i), 3), extractDerivation(gradFrameOut, 3));
    //ba
    for (int i = 0; i < 3; i++)hessianPos(i)(1, 0) = hessianPos(i)(0, 1);
    //mn
    for (int i = 0; i < 3; i++)hessianPos(i)(3, 2) = hessianPos(i)(2, 3);




    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j) = addTransposeToItself(compDeriv2(d2FramedFramedabnm(i, j), gradFrameOut));
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j) += contractGeneralDotP(hessianFrameOut, dFramedFrameSegment(i, j));
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j) += hessianFrameSegment(i, j);
    //aa
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 0) += 2 * s0 * hessianFrameSegment(i, j)(0, 1);
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 0) += 2 * s0 * contractGeneralDotP(extractDerivation(d2FramedFramedabnm(i, j), 1), extractDerivation(gradFrameOut, 0));
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 0) += s0 * s0 * hessianFrameSegment(i, j)(1, 1);
    //ab
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 1) += s0 * hessianFrameSegment(i, j)(1, 1);
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 1) += s0 * contractGeneralDotP(extractDerivation(d2FramedFramedabnm(i, j), 1), extractDerivation(gradFrameOut, 1));
    //an
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 2) += s0 * (hessianFrameSegment(i, j)(1, 2) + hessianFrameSegment(i, j)(0, 3));
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 2) += s0 * (contractGeneralDotP(extractDerivation(d2FramedFramedabnm(i, j), 1), extractDerivation(gradFrameOut, 2))
            + contractGeneralDotP(extractDerivation(d2FramedFramedabnm(i, j), 3), extractDerivation(gradFrameOut, 0))
                                                                                               );
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 2) += s0 * s0 * hessianFrameSegment(i, j)(1, 3);
    //na
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(2, 0) = hessianFrame(i, j)(0, 2);
    //am
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 3) += s0 * hessianFrameSegment(i, j)(1, 3);
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 3) += s0 * contractGeneralDotP(extractDerivation(d2FramedFramedabnm(i, j), 1), extractDerivation(gradFrameOut, 3));
    //ma
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(3, 0) = hessianFrame(i, j)(0, 3);
    //nb
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(2, 1) += s0 * hessianFrameSegment(i, j)(3, 1);
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(2, 1) += s0 * contractGeneralDotP(extractDerivation(d2FramedFramedabnm(i, j), 3), extractDerivation(gradFrameOut, 1));
    //bn
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(1, 2) = hessianFrame(i, j)(2, 1);
    //nn
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(2, 2) += 2 * s0 * hessianFrameSegment(i, j)(2, 3);
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(2, 2) += 2 * s0 * contractGeneralDotP(extractDerivation(d2FramedFramedabnm(i, j), 3), extractDerivation(gradFrameOut, 2));
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(2, 2) += s0 * s0 * hessianFrameSegment(i, j)(3, 3);
    //nm
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(2, 3) += s0 * hessianFrameSegment(i, j)(3, 3);
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(2, 3) += s0 * contractGeneralDotP(extractDerivation(d2FramedFramedabnm(i, j), 3), extractDerivation(gradFrameOut, 3));
    //ba
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(1, 0) = hessianFrame(i, j)(0, 1);
    //mn
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(3, 2) = hessianFrame(i, j)(2, 3);


    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i) = addTransposeToItself(compDeriv2(d2PreAltitudeEnergydFramedabnm(i), gradFrameOut));
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i) += contractGeneralDotP(hessianFrameOut, dPreAltitudeEnergydFrameSegment(i));
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i) += contractGeneralDotP(hessianPosOut, dPreAltitudeEnergydPosSegment(i));
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i) += hessianPreAltitudeEnergySegment(i);
    //aa
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 0) += 2. * s0 * hessianPreAltitudeEnergySegment(i)(0, 1);
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 0) += 2. * s0 * contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedabnm(i), 1), extractDerivation(gradFrameOut, 0));
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 0) += s0 * s0 * hessianPreAltitudeEnergySegment(i)(1, 1);
    //ab
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 1) += s0 * hessianPreAltitudeEnergySegment(i)(1, 1);
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 1) += s0 * contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedabnm(i), 1), extractDerivation(gradFrameOut, 1));
    //an
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 2) += s0 * (hessianPreAltitudeEnergySegment(i)(1, 2) + hessianPreAltitudeEnergySegment(i)(0, 3));
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 2) += s0 * (contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedabnm(i), 1), extractDerivation(gradFrameOut, 2))
          + contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedabnm(i), 3), extractDerivation(gradFrameOut, 0)));
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 2) += s0 * s0 * hessianPreAltitudeEnergySegment(i)(1, 3);
    //na
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(2, 0) = hessianPreAltitudeEnergy(i)(0, 2);
    //am
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 3) += s0 * hessianPreAltitudeEnergySegment(i)(1, 3);
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 3) += s0 * contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedabnm(i), 1), extractDerivation(gradFrameOut, 3));
    //ma
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(3, 0) = hessianPreAltitudeEnergy(i)(0, 3);
    //nb
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(2, 1) += s0 * hessianPreAltitudeEnergySegment(i)(3, 1);
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(2, 1) += s0 * contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedabnm(i), 3), extractDerivation(gradFrameOut, 1));
    //bn
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(1, 2) = hessianPreAltitudeEnergy(i)(2, 1);
    //nn
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(2, 2) += 2. * s0 * hessianPreAltitudeEnergySegment(i)(2, 3);
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(2, 2) += 2. * s0 * contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedabnm(i), 3), extractDerivation(gradFrameOut, 2));
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(2, 2) += s0 * s0 * hessianPreAltitudeEnergySegment(i)(3, 3);
    //nm
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(2, 3) += s0 * hessianPreAltitudeEnergySegment(i)(3, 3);
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(2, 3) += s0 * contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedabnm(i), 3), extractDerivation(gradFrameOut, 3));
    //ba
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(1, 0) = hessianPreAltitudeEnergy(i)(0, 1);
    //mn
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(3, 2) = hessianPreAltitudeEnergy(i)(2, 3);


    hessianPreAltitudeEnergyOut += hessianPreAltitudeEnergy;
    hessianFrameOut = hessianFrame;
    hessianPosOut = hessianPos;


    for (int k = 0; k < 3; k++) gradPreAltitudeEnergyOut[k] += gradPreAltitudeEnergySegment.row(k);
    gradPreAltitudeEnergyOut += compDeriv(dPreAltitudeEnergydFrameSegment, gradFrameOut) + compDeriv(dPreAltitudeEnergydPosSegment, gradPosOut);

    gradPosOut = compDeriv(dPosdPosSegment, gradPosOut) + compDeriv(dPosdFrameSegment, gradFrameOut);
    gradFrameOut = compDeriv(dFramedFrameSegment, gradFrameOut);
    for (int a = 0; a < 3; a++) {
      for (int b = 0; b < 3; b++) {
        gradFrameOut(a, b)[0] += dFdaSegment(a, b);
        gradFrameOut(a, b)[1] += dFdbSegment(a, b);
        gradFrameOut(a, b)[2] += dFdnSegment(a, b);
        gradFrameOut(a, b)[3] += dFdmSegment(a, b);
      }
      gradPosOut(a)[0] += dPdaSegment(a);
      gradPosOut(a)[1] += dPdbSegment(a);
      gradPosOut(a)[2] += dPdnSegment(a);
      gradPosOut(a)[3] += dPdmSegment(a);
    }

    s0 += s1;
    real surface = s0 * ribbonWidth;
    preAltitudeRotationalEnergy+=s1*ribbonWidth*preTransl+ preframeF*ribbon.preAltitudeRotationalEnergy(s1);
    preTransl+=preframeF*ribbon.PreTranslation(s1);
    preframeF *= ribbon.PreFrame(s1);
    dPreAltitudeEnergydFrameOut = getdPreAltdFrame(preAltitudeRotationalEnergy);
    dPreAltitudeEnergydPosOut = Eigen::Matrix<Eigen::Vector3r, 3, 1>(Eigen::Vector3r(surface, 0., 0.), Eigen::Vector3r(0., surface, 0.), Eigen::Vector3r(0., 0., surface));
    frame = frameF;
    pos = posF;
    dFramedFrameOut = getdFramedFrame(preframeF);
    dPosdFrameOut = getdPosdFrame(preTransl);
  }
  real surface = segmentLength * ribbonWidth;
  dPreAltitudeEnergydPosOut = Eigen::Matrix<Eigen::Vector3r, 3, 1>(Eigen::Vector3r(surface, 0., 0.), Eigen::Vector3r(0., surface, 0.), Eigen::Vector3r(0., 0., surface));
  posOut = pos;
  frameOut = frame;
}

void MixedPhY::getPres(int segment, Eigen::Matrix<Eigen::Vector4r, 3, 3>& gradPreFrameOut, Eigen::Matrix<Eigen::Vector4r, 3, 1>& gradPreTranslationOut, Eigen::Matrix<Eigen::Vector4r, 3, 1>& gradPreAltitudeRotationalEnergyOut,
               Eigen::Matrix3r &preFrame,
               Eigen::Vector3r &preTranslation) const
{
  Eigen::Matrix3r frame = Eigen::Matrix3r::Identity();
  Eigen::Vector3r pos(0.,0.,0.);
  Eigen::Vector3r preE(0., 0., 0.);
  gradPreAltitudeRotationalEnergyOut.setConstant(Eigen::Vector4r::Zero());
  gradPreFrameOut.setConstant(Eigen::Vector4r::Zero());
  gradPreTranslationOut.setConstant(Eigen::Vector4r::Zero());

  LinearScalar w = sr.getOmega1(segment);
  LinearScalar n = sr.getEta(segment);
  real s0 = 0;
  bool lastPart = false;
  real segmentLength = sr.getLength(segment);
  Eigen::Vector3r posF;
  Eigen::Matrix3r frameF;
  Eigen::Matrix3r preframeF = Eigen::Matrix3r::Identity();
  Eigen::Vector3r preTransl = Eigen::Vector3r(0.,0.,0.);
  Eigen::Vector3r preAltitudeRotationalEnergy = Eigen::Vector3r(0.,0.,0.);
  int partCount = 0;
  while (!lastPart) {
    partCount++;
    SuperRibbonGeometrieSerie ribbon;
    ribbon.Place(pos, frame);
    LinearScalar ssw(w.A, w.B + s0 * w.A);
    LinearScalar ssn(n.A, n.B + s0 * n.A);
    ribbon.Setup(ssw, ssn);
    ribbon.SetRibbonWidth(sr.getWidth());
    real s1 = ribbon.estimSMax();
    if (s1 + s0 >= segmentLength) {
      s1 = segmentLength - s0;
      lastPart = true;
    }
    std::tie(posF, frameF) = ribbon.PosFrame(s1);
    Eigen::Matrix<real, 3, 4> gradPreAltitudeEnergySegment = ribbon.gradPreAltitudeEnergy(s1);
    Eigen::Matrix<Eigen::Vector3r, 3, 1> dPreAltitudeEnergydPosSegment = ribbon.dPreAltitudeEnergydPos(s1);
    Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPreAltitudeEnergydFrameSegment = ribbon.dPreAltitudeEnergydFrame(s1);
    Eigen::Matrix<Eigen::Matrix3r, 3, 3> dFramedFrameSegment = ribbon.dFramedFrame(s1);
    Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPosdFrameSegment = ribbon.dPosdFrame(s1);
    Eigen::Matrix<Eigen::Vector3r, 3, 1> dPosdPosSegment = ribbon.dPosdPos(s1);
    preE += ribbon.preAltitudeEnergy(s1);


    Eigen::Matrix3r dFdaSegment = ribbon.dFrameda(s1);
    Eigen::Matrix3r dFdbSegment = ribbon.dFramedb(s1);
    Eigen::Matrix3r dFdnSegment = ribbon.dFramedn(s1);
    Eigen::Matrix3r dFdmSegment = ribbon.dFramedm(s1);
    Eigen::Vector3r dPdaSegment = ribbon.dPosda(s1);
    Eigen::Vector3r dPdbSegment = ribbon.dPosdb(s1);
    Eigen::Vector3r dPdnSegment = ribbon.dPosdn(s1);
    Eigen::Vector3r dPdmSegment = ribbon.dPosdm(s1);

    dFdaSegment += s0 * dFdbSegment;
    dFdnSegment += s0 * dFdmSegment;
    dPdaSegment += s0 * dPdbSegment;
    dPdnSegment += s0 * dPdmSegment;
    gradPreAltitudeEnergySegment.col(0) += s0 * gradPreAltitudeEnergySegment.col(1);
    gradPreAltitudeEnergySegment.col(2) += s0 * gradPreAltitudeEnergySegment.col(3);

    for (int k = 0; k < 3; k++) gradPreAltitudeRotationalEnergyOut[k] += gradPreAltitudeEnergySegment.row(k);
    gradPreAltitudeRotationalEnergyOut += compDeriv(dPreAltitudeEnergydFrameSegment, gradPreFrameOut) + compDeriv(dPreAltitudeEnergydPosSegment, gradPreTranslationOut);

    gradPreTranslationOut = compDeriv(dPosdPosSegment, gradPreTranslationOut) + compDeriv(dPosdFrameSegment, gradPreFrameOut);
    gradPreFrameOut = compDeriv(dFramedFrameSegment, gradPreFrameOut);
    for (int a = 0; a < 3; a++) {
      for (int b = 0; b < 3; b++) {
        gradPreFrameOut(a, b)[0] += dFdaSegment(a, b);
        gradPreFrameOut(a, b)[1] += dFdbSegment(a, b);
        gradPreFrameOut(a, b)[2] += dFdnSegment(a, b);
        gradPreFrameOut(a, b)[3] += dFdmSegment(a, b);
      }
      gradPreTranslationOut(a)[0] += dPdaSegment(a);
      gradPreTranslationOut(a)[1] += dPdbSegment(a);
      gradPreTranslationOut(a)[2] += dPdnSegment(a);
      gradPreTranslationOut(a)[3] += dPdmSegment(a);
    }

    s0 += s1;
    preTransl+=preframeF*ribbon.PreTranslation(s1);
    preAltitudeRotationalEnergy+=preframeF*ribbon.preAltitudeRotationalEnergy(s1);
    preframeF *= ribbon.PreFrame(s1);
    frame = frameF;
    pos = posF;
  }
  preFrame=frame;
  preTranslation=pos;
}

Eigen::Matrix<Eigen::Matrix3r, 3, 3> MixedPhY::getdFramedFrame(Eigen::Matrix3r preFrame) const
{
  Eigen::Matrix<Eigen::Matrix3r, 3, 3> out;

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++) {
      Eigen::Matrix3r tM;
      tM.setZero();
      tM(i, j) = 1.;
      tM = tM * preFrame;
      for (int k = 0; k < 3; k++)
        for (int l = 0; l < 3; l++)
          out(k, l)(i, j) = tM(k, l);
    }
  return out;
}

Eigen::Matrix<Eigen::Matrix3r, 3, 1> MixedPhY::getdPosdFrame(Eigen::Vector3r preTrans) const
{
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> out;

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++) {
      Eigen::Matrix3r tM;
      Eigen::Vector3r tMv;
      tM.setZero();
      tM(i, j) = 1.;
      tMv = tM * preTrans;
      for (int k = 0; k < 3; k++)
        out[k](i, j) = tMv(k);
    }
  return out;
}

Eigen::Matrix<Eigen::Matrix3r, 3, 1> MixedPhY::getdPreAltdFrame(Eigen::Vector3r preAltitudeRotationalEnergy) const
{
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> out;

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++) {
      Eigen::Matrix3r tM;
      Eigen::Vector3r tMv;
      tM.setZero();
      tM(i, j) = 1.;
      tMv = tM * preAltitudeRotationalEnergy;
      for (int k = 0; k < 3; k++)
        out[k](i, j) = tMv(k);
    }
  return out;
}

Eigen::Matrix<Eigen::Matrix<Eigen::Vector4r, 3, 3>, 3, 1> MixedPhY::d2PreAltitudeEnergydFramedabnm(Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPreAltitudeRotationalEnergyOut) const
{
  Eigen::Vector3r La = extractDerivation(gradPreAltitudeRotationalEnergyOut, 0);
  Eigen::Vector3r Lb = extractDerivation(gradPreAltitudeRotationalEnergyOut, 1);
  Eigen::Vector3r Ln = extractDerivation(gradPreAltitudeRotationalEnergyOut, 2);
  Eigen::Vector3r Lm = extractDerivation(gradPreAltitudeRotationalEnergyOut, 3);
  Eigen::Matrix<Eigen::Matrix<Eigen::Vector4r, 3, 3>, 3, 1> res;
  Eigen::Matrix3r temp;
  Eigen::Vector3r va, vb, vn, vm;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      temp.setZero();
      temp(i, j) = 1.;
      va = temp * La;
      vb = temp * Lb;
      vn = temp * Ln;
      vm = temp * Lm;
      for (int a = 0; a < 3; a++) {
        res[a](i, j)[0] = va[a];
        res[a](i, j)[1] = vb[a];
        res[a](i, j)[2] = vn[a];
        res[a](i, j)[3] = vm[a];
      }
    }
  }
  return res;
}


std::pair<Eigen::Vector3r, Eigen::Matrix3r> MixedPhY::posFrameAtEnd(int segment) const
{
  assert( segment < sr.getNbSegments() && segment>=0);
  real l = sr.getLength(segment);
  SuperRibbonGeometrieSerie ribbon;
  Eigen::Vector3r pos = sr.getPosStart(segment);
  Eigen::Matrix3r frame = sr.getFrameStart(segment);
  LinearScalar omega1 = sr.getOmega1(segment);
  LinearScalar eta = sr.getEta(segment);
  LinearScalar omega1Or = sr.getOmega1(segment);
  LinearScalar etaOr = sr.getEta(segment);
  real s0 = 0.;
  ribbon.Place(pos, frame);
  ribbon.Setup(omega1, eta);
  while (s0 < l) {
    real smax = ribbon.estimSMax();
    if (s0 + smax > l) {
      smax = l - s0;
      std::tie(pos, frame) = ribbon.PosFrame(smax);
      break;
    }
    std::tie(pos, frame) = ribbon.PosFrame(smax);
    s0 += smax;
    ribbon.Place(pos, frame);
    omega1.B = omega1Or.B + s0 * omega1Or.A;
    eta.B = etaOr.B + s0 * etaOr.A;
    ribbon.Setup(omega1, eta);
  }
  return {pos,frame};
}

Eigen::Vector3r MixedPhY::posAtEnd(int segment) const
{
  return posFrameAtEnd(segment).first;
}

Eigen::Matrix3r MixedPhY::frameAtEnd(int segment) const
{
  return posFrameAtEnd(segment).second;
}

void MixedPhY::fillElasticHessianStruct(std::map<Point, int> &liste)
{
  int t = sr.getNbSegments();
  for (int i = 0; i < t; i++) {
    fillElasticHessianSegmentStruct(i, liste);
  }
}

void MixedPhY::fillElasticHessian(std::map<Point, real> &liste, real scalar)
{
  int t = sr.getNbSegments();
  for (int i = 0; i < t; i++) {
    fillElasticHessianSegment(i, liste, scalar);
  }
}

void MixedPhY::fillGravityHessianStruct(std::map<Point, int> &liste)
{
  int t = sr.getNbSegments();
  for (int i = 0; i < t; i++) {
    fillGravityHessianSegmentStruct(i, liste);
  }
}

void MixedPhY::fillGravityHessian(std::map<Point, real> &liste, real scalar)
{
  int t = sr.getNbSegments();
  for (int i = 0; i < t; i++) {
    fillGravityHessianSegment(i, liste, scalar);
  }
}

void MixedPhY::fillElasticHessianSegmentStruct(int segmentId, std::map<Point, int> &liste)
{
  int offset = vpointIDOmega1(segmentId);
  for (int i = 0; i < 16; i++) {
    liste[Point((i / 4) + offset, (i % 4) + offset)]++;
  }
}

void MixedPhY::fillElasticHessianSegment(int segmentId, std::map<Point, real> &liste, real scalar)
{
  int offset = vpointIDOmega1(segmentId);
  ElasticEnergy el;
  el.FixPhysics(sr.getD(), sr.getThickness(), sr.getWidth(), sr.getLength(segmentId), sr.getPoissonRatio().value_or(0.5));
  el.FixPt(sr.getOmega1(segmentId), sr.getEta(segmentId), sr.getNatOmega1(segmentId));
  Eigen::Matrix4r res = el.computeHessian();
  for (int i = 0; i < 16; i++) {
    liste.at(Point((i / 4) + offset, (i % 4) + offset)) += scalar * res(i / 4, i % 4);
  }
}

void MixedPhY::fillGravityHessianSegmentStruct(int segmentId, std::map<Point, int> &liste)
{
  int offset = vpointIDOmega1(segmentId);
  for (int i = 0; i < 16; i++) {
    liste[Point((i / 4) + offset, (i % 4) + offset)]++;
    for (int j = 0; j < 4; j++) {
      for (int k = 0; k < 9; k++) {
        liste[Point(vpointIDOmega1(segmentId) + j, vpointIDFrame(segmentId) + k)]++;
        liste[Point(vpointIDFrame(segmentId) + k, vpointIDOmega1(segmentId) + j)]++;
      }
    }
  }
}

void MixedPhY::fillGravityHessianSegment(int segmentId, std::map<Point, real> &liste, real scalar)
{
  Eigen::Matrix<Eigen::Matrix4r, 3, 1> hessianPreAltitudeEnergyOut;
  Eigen::Matrix<Eigen::Matrix4r, 3, 1> hessianPosOut;
  Eigen::Matrix<Eigen::Matrix4r, 3, 3> hessianFrameOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPreAltitudeEnergyOut;
  Eigen::Matrix<Eigen::Vector3r, 3, 1> dPreAltitudeEnergydPosOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPreAltitudeEnergydFrameOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 3> dFramedFrameOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPosdFrameOut;
  Eigen::Matrix<Eigen::Vector3r, 3, 1> dPosdPosOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 3> gradFrameOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPosOut;
  Eigen::Vector3r posOut;
  Eigen::Matrix3r frameOut;

  getSecondOrder(segmentId,
                 hessianPreAltitudeEnergyOut,
                 hessianPosOut,
                 hessianFrameOut,
                 gradPreAltitudeEnergyOut,
                 dPreAltitudeEnergydPosOut,
                 dPreAltitudeEnergydFrameOut,
                 dFramedFrameOut,
                 dPosdFrameOut,
                 dPosdPosOut,
                 gradFrameOut,
                 gradPosOut,
                 posOut,
                 frameOut);
  Eigen::Matrix<Eigen::Vector4r, 3, 3> gradPreFrameOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPreTranslationOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPreAltitudeRotationalEnergyOut;
  Eigen::Matrix3r preFrame;
  Eigen::Vector3r preTranslation;
  getPres(segmentId,
          gradPreFrameOut,
          gradPreTranslationOut,
          gradPreAltitudeRotationalEnergyOut,
          preFrame,
          preTranslation
  );

  Eigen::Matrix4r res = sr.getAreaDensity() * contractGeneralDotP(hessianPreAltitudeEnergyOut, G);
  Eigen::Matrix<Eigen::Matrix<Eigen::Vector4r, 3, 3>, 3, 1> d2 = d2PreAltitudeEnergydFramedabnm(gradPreAltitudeRotationalEnergyOut);
  int offset = vpointIDOmega1(segmentId);
  for (int i = 0; i < 16; i++) {
    liste.at(Point((i / 4) + offset, (i % 4) + offset)) += scalar * res((i / 4), (i % 4));
  }
  real ad = sr.getAreaDensity();
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 4; j++) {
      for (int k = 0; k < 9; k++) {
        liste.at(Point(vpointIDOmega1(segmentId) + j, vpointIDFrame(segmentId) + k))
        += scalar * ad * G[i] * d2[i](k / 3, k % 3)[j];
        liste.at(Point(vpointIDFrame(segmentId) + k, vpointIDOmega1(segmentId) + j))
        += scalar * ad * G[i] * d2[i](k / 3, k % 3)[j];
      }
    }
  }
}

