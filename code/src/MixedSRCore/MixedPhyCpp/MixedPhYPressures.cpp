/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "../MixedPhY.hpp"


std::tuple<int, int, int> MixedPhY::addPressure(Pressure pressure)
{
  int index = thePressures.size();
  thePressures.push_back(pressure);
  auto [idl,ids] = addConstraintPressure(index);
  return {index,idl,ids};
}

std::tuple< int, int, int > MixedPhY::addPressureDoubleEnd(Pressure pressure)
{
  int index = thePressures.size();
  thePressures.push_back(pressure);
  auto [idl,ids] = addConstraintPressureDoubleEnd(index);
 return {index,idl,ids};
}

Pressure MixedPhY::getPressure(int id) const
{
  return thePressures.at(id);
}

Eigen::VectorXr MixedPhY::getPressureDistances() const
{
  size_t t=thePressures.size();
  Eigen::VectorXr res(t);
  for(size_t i=0;i<t;i++)
    res(i)=thePressures[i].distance;
  return res;
}

real MixedPhY::getPressureEnergy()
{
  real sum=0.;
  for(const auto& pr:thePressures)
    sum+=0.5*pr.k*pr.distance*pr.distance;
  return sum;
}

Eigen::VectorXr MixedPhY::getPressureEnergyGradient()
{
  size_t t = thePressures.size();
  Eigen::VectorXr grad(getNbVars());
  int offset=vpointIDPressures(sr.getNbSegments());
  grad.setZero();
  for(size_t i=0;i<t;i++)
    grad[offset+i]=thePressures.at(i).k*thePressures.at(i).distance;
  return grad;
}

void MixedPhY::fillPressureHessianStruct(std::map<Point, int>& liste)
{
  size_t t = thePressures.size();
  int offset=vpointIDPressures(sr.getNbSegments());
  for(size_t i=0;i<t;i++)
    liste[Point(offset+i,offset+i)]++;
}

void MixedPhY::fillPressureHessian(std::map<Point, real>& liste, real scalar)
{
  size_t t = thePressures.size();
  int offset=vpointIDPressures(sr.getNbSegments());
  for(size_t i=0;i<t;i++)
    liste.at(Point(offset+i,offset+i))+=scalar*thePressures.at(i).k;
}
