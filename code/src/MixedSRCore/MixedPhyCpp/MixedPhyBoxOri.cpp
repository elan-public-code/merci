/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "../MixedPhyBoxOri.hpp"

// void MixedPhyBoxOri::set(MixedSuperRibbon msr)
// {
//   phy.set(msr);
//   point=phy.getPoint();
// }
// 
// void MixedPhyBoxOri::set(MixedPhY msr)
// {
//   phy=msr;
//   point=phy.getPoint();
// }

MixedPhyBoxOri::MixedPhyBoxOri(MixedPhY msr) :
  phy(msr)
{
  point = phy.getPoint();
}
MixedPhyBoxOri::MixedPhyBoxOri(MixedSuperRibbon msr) :
  phy(msr)
{
  point = phy.getPoint();
}



void MixedPhyBoxOri::setGravity(Eigen::Vector3r grav)
{
  phy.setGravity(grav);
}

Eigen::Vector3r MixedPhyBoxOri::getGravity()
{
  return phy.getGravity();
}

void MixedPhyBoxOri::setPoint(Eigen::VectorXr x)
{
  assert(x.size()+12==point.size());
  point.tail(x.size())=x;
  phy.setPoint(point);
}

Eigen::VectorXr MixedPhyBoxOri::getPoint()
{
  point=phy.getPoint();
  return cutClampingPt(point);
}

Eigen::VectorXr MixedPhyBoxOri::getLowerBound()
{
  return cutClampingPt(phy.getLowerBound());
}

Eigen::VectorXr MixedPhyBoxOri::getUpperBound()
{
  return cutClampingPt(phy.getUpperBound());
}

void MixedPhyBoxOri::tryBetterInitPoint()
{
  phy.tryBetterInitPoint();
}

real MixedPhyBoxOri::getGravityEnergy()
{
  return phy.getGravityEnergy();
}

real MixedPhyBoxOri::getElasticEnergy()
{
  return phy.getElasticEnergy();
}

real MixedPhyBoxOri::getPressureEnergy()
{
  return phy.getPressureEnergy();
}

real MixedPhyBoxOri::getRepulsionEnergy()
{
  return phy.getRepulsionEnergy();
}

Eigen::VectorXr MixedPhyBoxOri::getElasticEnergyGradient()
{
  return cutClampingPt(phy.getElasticEnergyGradient());
}

Eigen::VectorXr MixedPhyBoxOri::getGravityEnergyGradient()
{
  return cutClampingPt(phy.getGravityEnergyGradient());
}

Eigen::VectorXr MixedPhyBoxOri::getPressureEnergyGradient()
{
  return cutClampingPt(phy.getPressureEnergyGradient());
}

Eigen::VectorXr MixedPhyBoxOri::getRepulsionEnergyGradient()
{
  return cutClampingPt(phy.getRepulsionEnergyGradient());
}

Eigen::VectorXr MixedPhyBoxOri::constraints()
{
  return cutClampingConstr(phy.constraints());
}

Eigen::VectorXr MixedPhyBoxOri::getConstraintsLowerBound()
{
  return cutClampingConstr(phy.getConstraintsLowerBound());
}

Eigen::VectorXr MixedPhyBoxOri::getConstraintsUpperBound()
{
  return cutClampingConstr(phy.getConstraintsUpperBound());
}


void MixedPhyBoxOri::fillConstraintsJac(std::map<Point, real>& liste)
{
  std::map<Point, real> listeOri;
  std::map<Point, int> listeStruct;
  phy.fillConstraintsJacStruct(listeStruct);
  for (auto p : listeStruct) 
    listeOri[p.first] = 0.;
  phy.fillConstraintsJac(listeOri);
  for(auto [p,val]:listeOri)
  {
    if(p.first<12||p.second<12)continue;
    liste[Point(p.first-12,p.second-12)]+=val;
  }
}

void MixedPhyBoxOri::fillConstraintsJacStruct(std::map<Point, int>& liste)
{
  std::map<Point, int> listeStruct;
  phy.fillConstraintsJacStruct(listeStruct);
  for(auto [p,val]:listeStruct)
  {
    if(p.first<12||p.second<12)continue;
    liste[Point(p.first-12,p.second-12)]+=val;
  }
}

void MixedPhyBoxOri::fillElasticHessian(std::map<Point, real>& liste, real scalar)
{
  std::map<Point, real> listeOri;
  std::map<Point, int> listeStruct;
  phy.fillElasticHessianStruct(listeStruct);
  for (auto p : listeStruct) 
    listeOri[p.first] = 0.;
  phy.fillElasticHessian(listeOri, scalar);
  for(auto [p,val]:listeOri)
  {
    if(p.first<12||p.second<12)continue;
    liste[Point(p.first-12,p.second-12)]+=val;
  }
}

void MixedPhyBoxOri::fillElasticHessianStruct(std::map<Point, int>& liste)
{
  std::map<Point, int> listeStruct;
  phy.fillElasticHessianStruct(listeStruct);
  for(auto [p,val]:listeStruct)
  {
    if(p.first<12||p.second<12)continue;
    liste[Point(p.first-12,p.second-12)]+=val;
  }
}

void MixedPhyBoxOri::fillGravityHessian(std::map<Point, real>& liste, real scalar)
{
  std::map<Point, real> listeOri;
  std::map<Point, int> listeStruct;
  phy.fillGravityHessianStruct(listeStruct);
  for (auto p : listeStruct) 
    listeOri[p.first] = 0.;
  phy.fillGravityHessian(listeOri, scalar);
  for(auto [p,val]:listeOri)
  {
    if(p.first<12||p.second<12)continue;
    liste[Point(p.first-12,p.second-12)]+=val;
  }
}

void MixedPhyBoxOri::fillGravityHessianStruct(std::map<Point, int>& liste)
{
  std::map<Point, int> listeStruct;
  phy.fillGravityHessianStruct(listeStruct);
  for(auto [p,val]:listeStruct)
  {
    if(p.first<12||p.second<12)continue;
    liste[Point(p.first-12,p.second-12)]+=val;
  }
}

void MixedPhyBoxOri::fillPressureHessian(std::map<Point, real>& liste, real scalar)
{
  std::map<Point, real> listeOri;
  std::map<Point, int> listeStruct;
  phy.fillPressureHessianStruct(listeStruct);
  for (auto p : listeStruct) 
    listeOri[p.first] = 0.;
  phy.fillPressureHessian(listeOri, scalar);
  for(auto [p,val]:listeOri)
  {
    if(p.first<12||p.second<12)continue;
    liste[Point(p.first-12,p.second-12)]+=val;
  }
}

void MixedPhyBoxOri::fillPressureHessianStruct(std::map<Point, int>& liste)
{
  std::map<Point, int> listeStruct;
  phy.fillPressureHessianStruct(listeStruct);
  for(auto [p,val]:listeStruct)
  {
    if(p.first<12||p.second<12)continue;
    liste[Point(p.first-12,p.second-12)]+=val;
  }
}

void MixedPhyBoxOri::fillRepulsionHessian(std::map<Point, real>& liste, real scalar)
{
  std::map<Point, real> listeOri;
  std::map<Point, int> listeStruct;
  phy.fillRepulsionHessianStruct(listeStruct);
  for (auto p : listeStruct) 
    listeOri[p.first] = 0.;
  phy.fillRepulsionHessian(listeOri, scalar);
  for(auto [p,val]:listeOri)
  {
    if(p.first<12||p.second<12)continue;
    liste[Point(p.first-12,p.second-12)]+=val;
  }
}

void MixedPhyBoxOri::fillRepulsionHessianStruct(std::map<Point, int>& liste)
{
  std::map<Point, int> listeStruct;
  phy.fillRepulsionHessianStruct(listeStruct);
  for(auto [p,val]:listeStruct)
  {
    if(p.first<12||p.second<12)continue;
    liste[Point(p.first-12,p.second-12)]+=val;
  }
}


void MixedPhyBoxOri::fillConstraintsHessian(std::map<Point, real>& liste, Eigen::VectorXr scalarV)
{
  std::map<Point, real> listeOri;
  std::map<Point, int> listeStruct;
  Eigen::VectorXr scalarExt(scalarV.size()+12);
  scalarExt.head<12>().setZero();
  scalarExt.tail(scalarV.size())=scalarV;
  phy.fillConstraintsHessianStruct(listeStruct);
  for (auto p : listeStruct) 
    listeOri[p.first] = 0.;
  phy.fillConstraintsHessian(listeOri,scalarExt);
  for(auto [p,val]:listeOri)
  {
    if(p.first<12||p.second<12)continue;
    liste[Point(p.first-12,p.second-12)]+=val;
  }
}


void MixedPhyBoxOri::fillConstraintsHessianStruct(std::map<Point, int>& liste)
{
  std::map<Point, int> listeStruct;
  phy.fillConstraintsHessianStruct(listeStruct);
  for(auto [p,val]:listeStruct)
  {
    if(p.first<12||p.second<12)continue;
    liste[Point(p.first-12,p.second-12)]+=val;
  }
}

int MixedPhyBoxOri::getNbVars()
{
  return phy.getNbVars()-12;
}

int MixedPhyBoxOri::getNbConstraints()
{
  return phy.getNbConstraints()-12;
}

std::pair<Eigen::Vector3r, Eigen::Matrix3r> MixedPhyBoxOri::posFrameAtEnd(int segment)
{
  return phy.posFrameAtEnd(segment);
}

Eigen::VectorXr MixedPhyBoxOri::getPressureDistances() const
{
  return phy.getPressureDistances();
}

Eigen::VectorXr MixedPhyBoxOri::getRepulsionEnergyPerField()
{
  return phy.getRepulsionEnergyPerField();
}

MixedSuperRibbon MixedPhyBoxOri::get()
{
  return phy.get();
}

Eigen::VectorXr MixedPhyBoxOri::cutClampingPt(Eigen::VectorXr ori)
{
  return ori.tail(ori.size()-12);
}

Eigen::VectorXr MixedPhyBoxOri::cutClampingConstr(Eigen::VectorXr ori)
{
  return ori.tail(ori.size()-12);
}
