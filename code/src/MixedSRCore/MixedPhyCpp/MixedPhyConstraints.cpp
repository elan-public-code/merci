/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "../MixedPhY.hpp"
#include "ElasticEnergy.hpp"
#include "SuperRibbonGeometrieSerie.hpp"
#include "MathOperations.hpp"
#include "Constraints/ConstraintEtaStart.hpp"
#include "Constraints/ConstraintEtaEnd.hpp"
#include "Constraints/ConstraintEndPos.hpp"
#include "Constraints/ConstraintEndFrame.hpp"
#include "Constraints/ConstraintsOffPlane/ConstraintOffPlane.hpp"
#include "Constraints/ConstraintsOffPlane/ConstraintOffPlaneDoubleEnd.hpp"
#include "Constraints/ConstraintsOffPlane/ConstraintOffPlaneStartSegment.hpp"
#include "Constraints/ConstraintsOffPlane/ConstraintOffPlaneDoubleEndStartSegment.hpp"
#include "Constraints/ConstraintsPressure/ConstraintPressure.hpp"
#include "Constraints/ConstraintsPressure/ConstraintPressureDoubleEnd.hpp"
#include "Constraints/ConstraintsPressure/ConstraintPressureStartSegment.hpp"
#include "Constraints/ConstraintsPressure/ConstraintPressureDoubleEndStartSegment.hpp"
#include "Constraints/ConstraintBarycenterOnAxis.hpp"
#include "Constraints/ConstraintClampOrBoundingSphere.hpp"
#include "Constraints/ConstraintCyclicRibbon.hpp"
#include "Constraints/ConstraintCyclicEta.hpp"

MixedPhY::~MixedPhY()
{
}

std::pair<int,int> MixedPhY::addConstraintEtaStart()
{
  theConstraints.push_back(std::shared_ptr<GenericExtraConstraint>(new ConstraintEtaStart(getNbConstraints())));
  return {theConstraints.back()->offset(),theConstraints.back()->size()};
}

std::pair<int,int> MixedPhY::addConstraintEtaEnd()
{
  theConstraints.push_back(std::shared_ptr<GenericExtraConstraint>(new ConstraintEtaEnd(getNbConstraints())));
  return {theConstraints.back()->offset(),theConstraints.back()->size()};
}

std::pair<int,int> MixedPhY::addConstraintEndPos(Eigen::Vector3r ePos)
{
  theConstraints.push_back(std::shared_ptr<GenericExtraConstraint>(new ConstraintEndPos(getNbConstraints(), ePos, alphaConstraintEndPos)));
  return {theConstraints.back()->offset(),theConstraints.back()->size()};
}

std::pair<int,int> MixedPhY::addConstraintEndFrame(Eigen::Matrix3r eFrame)
{
  theConstraints.push_back(std::shared_ptr<GenericExtraConstraint>(new ConstraintEndFrame(getNbConstraints(), eFrame, alphaConstraintEndFrame)));
  return {theConstraints.back()->offset(),theConstraints.back()->size()};
}

std::pair<int,int> MixedPhY::addConstraintOffPlane(Eigen::Vector3r pt, Eigen::Vector3r nmle)
{
  int t=sr.getNbSegments();
  int firstConstID=getNbConstraints();
  for(int i=0;i<t;i++)
    theConstraints.push_back( std::shared_ptr<GenericExtraConstraint>(new ConstraintOffPlaneStartSegment(getNbConstraints(),i,pt,nmle)));
  theConstraints.push_back( std::shared_ptr<GenericExtraConstraint>(new ConstraintOffPlane(getNbConstraints(),t-1,pt,nmle)));
  int lastConstID=getNbConstraints();
  return {firstConstID,lastConstID-firstConstID};
}

std::pair<int,int> MixedPhY::addConstraintOffPlaneDoubleEnd(Eigen::Vector3r pt, Eigen::Vector3r nmle)
{
  int t=sr.getNbSegments();
  int firstConstID=getNbConstraints();
  for(int i=0;i<t;i++)
    theConstraints.push_back( std::shared_ptr<GenericExtraConstraint>(new ConstraintOffPlaneDoubleEndStartSegment(getNbConstraints(),i,pt,nmle)));
  theConstraints.push_back( std::shared_ptr<GenericExtraConstraint>(new ConstraintOffPlaneDoubleEnd(getNbConstraints(),t-1,pt,nmle)));
  int lastConstID=getNbConstraints();
  return {firstConstID,lastConstID-firstConstID};
}

std::pair<int, int> MixedPhY::addConstraintPressure(int pressureID)
{
  int t=sr.getNbSegments();
  int firstConstID=getNbConstraints();
  for(int i=0;i<t;i++)
    theConstraints.push_back( std::shared_ptr<GenericExtraConstraint>(new ConstraintPressureStartSegment(getNbConstraints(),i,pressureID)));
  theConstraints.push_back( std::shared_ptr<GenericExtraConstraint>(new ConstraintPressure(getNbConstraints(),t-1,pressureID)));
  int lastConstID=getNbConstraints();
  return {firstConstID,lastConstID-firstConstID};
}

std::pair<int, int> MixedPhY::addConstraintPressureDoubleEnd(int pressureID)
{
  int t=sr.getNbSegments();
  int firstConstID=getNbConstraints();
  for(int i=0;i<t;i++)
    theConstraints.push_back( std::shared_ptr<GenericExtraConstraint>(new ConstraintPressureDoubleEndStartSegment(getNbConstraints(),i,pressureID)));
  theConstraints.push_back( std::shared_ptr<GenericExtraConstraint>(new ConstraintPressureDoubleEnd(getNbConstraints(),t-1,pressureID)));
  int lastConstID=getNbConstraints();
  return {firstConstID,lastConstID-firstConstID};
}

std::pair<int,int> MixedPhY::addConstraintClampOrBoundingSphere(Eigen::Vector3r center, real radius)
{
  theConstraints.push_back(std::shared_ptr<GenericExtraConstraint>(new ConstraintClampOrBoundingSphere(getNbConstraints(), center, radius)));
  return {theConstraints.back()->offset(),theConstraints.back()->size()};
}

std::pair<int,int> MixedPhY::addConstraintLoopRibbon()
{
  theConstraints.push_back(std::shared_ptr<GenericExtraConstraint>(new ConstraintCyclicRibbon(getNbConstraints(), alphaConstraintEndPos, alphaConstraintFrame)));
  return {theConstraints.back()->offset(),theConstraints.back()->size()};
}

std::pair<int,int> MixedPhY::addConstraintLoopEta(bool negate)
{
  theConstraints.push_back(std::shared_ptr<GenericExtraConstraint>(new ConstraintCyclicEta(getNbConstraints(), negate)));
  return {theConstraints.back()->offset(),theConstraints.back()->size()};
}

std::pair<int, int> MixedPhY::addConstraintBarycenterOnAxis(Eigen::Vector3r posAxis, Eigen::Vector3r dirAxis)
{
  theConstraints.push_back(std::shared_ptr<GenericExtraConstraint>(new ConstraintBarycenterOnAxis(getNbConstraints(), posAxis, dirAxis, alphaConstraintBarycenter)));
  return {theConstraints.back()->offset(),theConstraints.back()->size()};
}

// real MixedPhY::alphaConstraintFrame=1./9.;
// real MixedPhY::alphaConstraintPos=1./3.;
real MixedPhY::alphaConstraintFrame=1.;
real MixedPhY::alphaConstraintPos=1.;
real MixedPhY::alphaConstraintEndFrame=1.;
real MixedPhY::alphaConstraintEndPos=1.;
real MixedPhY::alphaConstraintBarycenter=1.;



using namespace MathOperations;


int MixedPhY::getNbConstraints()
{
  if(!theConstraints.empty())
  {
    std::shared_ptr<GenericExtraConstraint> gec=theConstraints.back();
    return gec->offset()+gec->size();
  }
  return 0;
}

Eigen::VectorXr MixedPhY::getConstraintsLowerBound()
{
  Eigen::VectorXr res(getNbConstraints());
  res.setZero();
  for(std::shared_ptr<GenericExtraConstraint> ge: theConstraints)
    ge->getLowerBound(res);
  return res;
}

Eigen::VectorXr MixedPhY::getConstraintsUpperBound()
{
  Eigen::VectorXr res(getNbConstraints());
  res.setZero();
  for(std::shared_ptr<GenericExtraConstraint> ge: theConstraints)
    ge->getUpperBound(res);
  return res;
}

Eigen::VectorXr MixedPhY::constraints()
{
  Eigen::VectorXr res(getNbConstraints());
  for(std::shared_ptr<GenericExtraConstraint> ge: theConstraints)
    ge->eval(*this, res);
  return res;
}


void MixedPhY::fillConstraintsJac(std::map<MixedPhY::Point, real> &liste)
{
  for(std::shared_ptr<GenericExtraConstraint> ge: theConstraints)
    ge->jacobian(*this, liste);
}

void MixedPhY::fillConstraintsJacStruct(std::map<Point, int> &liste)
{
  for(std::shared_ptr<GenericExtraConstraint> ge: theConstraints)
    ge->jacobianStruct(*this, liste);
}

void MixedPhY::fillConstraintsHessian(std::map<Point, real> &liste, Eigen::VectorXr scalarV)
{
  for(std::shared_ptr<GenericExtraConstraint> ge: theConstraints)
    ge->hessian(*this, scalarV, liste);
}

void MixedPhY::fillConstraintsHessianStruct(std::map<Point, int> &liste)
{
  for(std::shared_ptr<GenericExtraConstraint> ge: theConstraints)
    ge->hessianStruct(*this, liste);
}
