/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "../MixedPhY.hpp"

int MixedPhY::addRepulsionField(RepulsionField rp)
{
  int index = theRepulsionFields.size();
  theRepulsionFields.push_back(rp);
  return index;
}

RepulsionField MixedPhY::getRepulsionField(int id)
{
  return theRepulsionFields.at(id);
}

real MixedPhY::getRepulsionEnergy()
{
  double res=0;
  for(const RepulsionField& rf:theRepulsionFields)
  {
    double sres=0.;
    size_t t = sr.getNbSegments();
    for(size_t i=1;i<t;i++)
    {
      sres+=rf.energy(sr.getPosStart(i));
    }
    res+=sres;
  }
  return res;
}

Eigen::VectorXr MixedPhY::getRepulsionEnergyPerField()
{
  int t=theRepulsionFields.size();
  Eigen::VectorXr res(t);
  for(int i=0;i<t;i++)
  {
    double sres=0.;
    size_t ts = sr.getNbSegments();
    for(size_t is=1;is<ts;is++)
    {
      sres+=theRepulsionFields[i].energy(sr.getPosStart(is));
    }
    res[i]=sres;
  }
  return res;
}

Eigen::VectorXr MixedPhY::getRepulsionEnergyGradient()
{
  size_t t = sr.getNbSegments();
  Eigen::VectorXr res(getNbVars());
  res.setZero();
  for(const RepulsionField& rf:theRepulsionFields)
  {
    for(size_t i=1;i<t;i++)
    {
      res.segment<3>(vpointIDPos(i))+=rf.gradient(sr.getPosStart(i));
    }
  }
  return res;
}

void MixedPhY::fillRepulsionHessianStruct(std::map<Point, int>& liste)
{
  size_t t = sr.getNbSegments();
  if(!theRepulsionFields.empty())
  {
    for(size_t i=1;i<t;i++)
    {
      for(int ix=0;ix<3;ix++)
        for(int iy=0;iy<3;iy++)
          liste[Point(vpointIDPos(i)+ix,vpointIDPos(i)+iy)]++;
    }
  }
}

void MixedPhY::fillRepulsionHessian(std::map<Point, real>& liste, real scalar)
{
  
  size_t t = sr.getNbSegments();
  for(const RepulsionField& rf:theRepulsionFields)
  {
    for(size_t i=1;i<t;i++)
    {
      auto h = rf.hessian(sr.getPosStart(i));
      for(int ix=0;ix<3;ix++)
        for(int iy=0;iy<3;iy++)
          liste.at(Point(vpointIDPos(i)+ix,vpointIDPos(i)+iy))+=h(ix,iy);
    }
  }
}
