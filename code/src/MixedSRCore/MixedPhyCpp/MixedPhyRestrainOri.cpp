/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "../MixedPhyRestrainOri.hpp"

Filter::Filter() :
  shortToLong([](int i){return i;})
{
  positionRemove.setConstant(false);
  frameRemove.setConstant(false);
}

void Filter::reGenerateFilter()
{
  size=0;
  std::vector<int> remap(12);
  std::vector<std::optional<int>> remaprev(12);
  for(int i=0;i<3;i++)
  {
    if(!positionRemove[i])
    {
      remaprev[i]=size;
      remap[size]=i;
      size++;
    }
    else
      remaprev[i]=std::nullopt;
  }
  for(int i=0;i<9;i++)
  {
    if(!frameRemove(i/3, i%3))
    {
      remaprev[3+i]=size;
      remap[size]=3+i;
      size++;
    }
    else
      remaprev[3+i]=std::nullopt;
  }
  shortToLong=[=](int i){
    if (i<size){
      return remap[i];
    } 
    else
      return i-size+12;
  };
  longToShort=[=](int i)->std::optional<int>{
    if(i>=12)
      return i-12+size;
    else
      return remaprev[i];
  };
}

int Filter::eval(int i)
{
  return shortToLong(i);
}

int Filter::removeSize()
{
  return size;
}

int Filter::operator()(int i)
{
  return shortToLong(i);
}

std::optional<int> Filter::reverseFilter(int i)
{
  return longToShort(i);
}

void Filter::resetFilter()
{
  positionRemove.setConstant(false);
  frameRemove.setConstant(false);
  reGenerateFilter();
}

void Filter::filterOutPos(int index)
{
  positionRemove[index] = true;
  reGenerateFilter();
}

void Filter::filterOutFrame(int i, int j)
{
  frameRemove(i, j) = true;
  reGenerateFilter();
}

void Filter::test()
{
  std::cout<< "Testing filter"<<std::endl;
  std::cout << "Position out : "<<positionRemove.transpose()<<std::endl;
  std::cout << "Frame out : "<<std::endl<<frameRemove.transpose()<<std::endl;
  std::cout<<"Printing shortToLong: ";
  for(int i=0;i<=12;i++) std::cout<<eval(i)<<' ';
  std::cout<<std::endl;
  std::cout<<"Printing longToShort: ";
  for(int i=0;i<=12;i++) 
    if(reverseFilter(i).has_value())
      std::cout<<reverseFilter(i).value()<<' ';
    else
      std::cout<<"? ";
  std::cout<<std::endl;
}


MixedPhyRestrainOri::MixedPhyRestrainOri(MixedPhY msr) :
  phy(msr)
{
  point = phy.getPoint();
}
MixedPhyRestrainOri::MixedPhyRestrainOri(MixedSuperRibbon msr) :
  phy(msr)
{
  point = phy.getPoint();
}

MixedPhyRestrainOri::MixedPhyRestrainOri(MixedPhY msr, Filter filter) :
  phy(msr), filter(filter)
{
  point = phy.getPoint();
}
MixedPhyRestrainOri::MixedPhyRestrainOri(MixedSuperRibbon msr, Filter filter) :
  phy(msr), filter(filter)
{
  point = phy.getPoint();
}


void MixedPhyRestrainOri::setGravity(Eigen::Vector3r grav)
{
  phy.setGravity(grav);
}

Eigen::Vector3r MixedPhyRestrainOri::getGravity()
{
  return phy.getGravity();
}

void MixedPhyRestrainOri::setPoint(Eigen::VectorXr x)
{
  assert(x.size()-filter.removeSize()+12==point.size());
  int t=x.size();
  for(int i=0;i<t;i++)
    point[filter(i)]=x[i];
  phy.setPoint(point);
}

Eigen::VectorXr MixedPhyRestrainOri::getPoint()
{
  point=phy.getPoint();
  return cutClampingPt(point);
}

Eigen::VectorXr MixedPhyRestrainOri::getLowerBound()
{
  return cutClampingPt(phy.getLowerBound());
}

Eigen::VectorXr MixedPhyRestrainOri::getUpperBound()
{
  return cutClampingPt(phy.getUpperBound());
}

void MixedPhyRestrainOri::tryBetterInitPoint()
{
  phy.tryBetterInitPoint();
}

real MixedPhyRestrainOri::getGravityEnergy()
{
  return phy.getGravityEnergy();
}

real MixedPhyRestrainOri::getElasticEnergy()
{
  return phy.getElasticEnergy();
}

real MixedPhyRestrainOri::getPressureEnergy()
{
  return phy.getPressureEnergy();
}

real MixedPhyRestrainOri::getRepulsionEnergy()
{
  return phy.getRepulsionEnergy();
}

Eigen::VectorXr MixedPhyRestrainOri::getElasticEnergyGradient()
{
  return cutClampingPt(phy.getElasticEnergyGradient());
}

Eigen::VectorXr MixedPhyRestrainOri::getGravityEnergyGradient()
{
  return cutClampingPt(phy.getGravityEnergyGradient());
}

Eigen::VectorXr MixedPhyRestrainOri::getPressureEnergyGradient()
{
  return cutClampingPt(phy.getPressureEnergyGradient());
}

Eigen::VectorXr MixedPhyRestrainOri::getRepulsionEnergyGradient()
{
  return cutClampingPt(phy.getRepulsionEnergyGradient());
}

Eigen::VectorXr MixedPhyRestrainOri::constraints()
{
  return phy.constraints();
}

Eigen::VectorXr MixedPhyRestrainOri::getConstraintsLowerBound()
{
  return phy.getConstraintsLowerBound();
}

Eigen::VectorXr MixedPhyRestrainOri::getConstraintsUpperBound()
{
  return phy.getConstraintsUpperBound();
}


void MixedPhyRestrainOri::fillConstraintsJac(std::map<Point, real>& liste)
{
  std::map<Point, real> listeOri;
  std::map<Point, int> listeStruct;
  phy.fillConstraintsJacStruct(listeStruct);
  for (auto p : listeStruct) 
    listeOri[p.first] = 0.;
  phy.fillConstraintsJac(listeOri);
  for(auto [p,val]:listeOri)
  {
    auto iy = filter.reverseFilter(p.second);
    if(iy.has_value())
      liste[Point(p.first,iy.value())]+=val;
  }
}

void MixedPhyRestrainOri::fillConstraintsJacStruct(std::map<Point, int>& liste)
{
  std::map<Point, int> listeStruct;
  phy.fillConstraintsJacStruct(listeStruct);
  for(auto [p,val]:listeStruct)
  {
    auto iy = filter.reverseFilter(p.second);
    if(iy.has_value())
      liste[Point(p.first,iy.value())]+=val;
  }
}

void MixedPhyRestrainOri::fillElasticHessian(std::map<Point, real>& liste, real scalar)
{
  genericFillHessian(
    std::bind(&MixedPhY::fillElasticHessianStruct, &phy,std::placeholders::_1),
    std::bind(&MixedPhY::fillElasticHessian, &phy,std::placeholders::_1,std::placeholders::_2),
    liste, scalar
  );
}

void MixedPhyRestrainOri::fillElasticHessianStruct(std::map<Point, int>& liste)
{
  genericFillHessianStruct(
    std::bind(&MixedPhY::fillElasticHessianStruct, &phy,std::placeholders::_1),
    liste
  );
}

void MixedPhyRestrainOri::fillGravityHessian(std::map<Point, real>& liste, real scalar)
{
  genericFillHessian(
    std::bind(&MixedPhY::fillGravityHessianStruct, &phy,std::placeholders::_1),
    std::bind(&MixedPhY::fillGravityHessian, &phy,std::placeholders::_1,std::placeholders::_2),
    liste, scalar
  );
}

void MixedPhyRestrainOri::fillGravityHessianStruct(std::map<Point, int>& liste)
{
    genericFillHessianStruct(
    std::bind(&MixedPhY::fillGravityHessianStruct, &phy,std::placeholders::_1),
    liste
  );
}

void MixedPhyRestrainOri::fillPressureHessian(std::map<Point, real>& liste, real scalar)
{
  genericFillHessian(
    std::bind(&MixedPhY::fillPressureHessianStruct, &phy,std::placeholders::_1),
    std::bind(&MixedPhY::fillPressureHessian, &phy,std::placeholders::_1,std::placeholders::_2),
    liste, scalar
  );
}

void MixedPhyRestrainOri::fillPressureHessianStruct(std::map<Point, int>& liste)
{
  genericFillHessianStruct(
    std::bind(&MixedPhY::fillPressureHessianStruct, &phy,std::placeholders::_1),
    liste
  );
}

void MixedPhyRestrainOri::fillRepulsionHessian(std::map<Point, real>& liste, real scalar)
{
  genericFillHessian(
    std::bind(&MixedPhY::fillRepulsionHessianStruct, &phy,std::placeholders::_1),
    std::bind(&MixedPhY::fillRepulsionHessian, &phy,std::placeholders::_1,std::placeholders::_2),
    liste, scalar
  );
}

void MixedPhyRestrainOri::fillRepulsionHessianStruct(std::map<Point, int>& liste)
{
  genericFillHessianStruct(
    std::bind(&MixedPhY::fillRepulsionHessianStruct, &phy,std::placeholders::_1),
    liste
  );
}

void MixedPhyRestrainOri::genericFillHessian(std::function<void (std::map<Point, int> &)> fillHessianStruct, 
                                             std::function<void (std::map<Point, real> &, real)> fillHessian,
                                             std::map<Point, real>& liste, real scalar)
{
  std::map<Point, real> listeOri;
  std::map<Point, int> listeStruct;
  fillHessianStruct(listeStruct);
  for (auto p : listeStruct) 
    listeOri[p.first] = 0.;
  fillHessian(listeOri, scalar);
  for(auto [p,val]:listeOri)
  {
    auto ix = filter.reverseFilter(p.first);
    auto iy = filter.reverseFilter(p.second);
    if(ix.has_value()&&iy.has_value())
      liste[Point(ix.value(),iy.value())]+=val;
  }
}

void MixedPhyRestrainOri::genericFillHessianStruct(std::function<void (std::map<Point, int> &)> fillHessianStruct, std::map<Point, int>& liste)
{
  std::map<Point, int> listeStruct;
  fillHessianStruct(listeStruct);
  for(auto [p,val]:listeStruct)
  {
    auto ix = filter.reverseFilter(p.first);
    auto iy = filter.reverseFilter(p.second);
    if(ix.has_value()&&iy.has_value())
      liste[Point(ix.value(),iy.value())]+=val;
  }
}



void MixedPhyRestrainOri::fillConstraintsHessian(std::map<Point, real>& liste, Eigen::VectorXr scalarV)
{
  
  
  std::map<Point, real> listeOri;
  std::map<Point, int> listeStruct;
  phy.fillConstraintsHessianStruct(listeStruct);
  for (auto p : listeStruct) 
    listeOri[p.first] = 0.;
  phy.fillConstraintsHessian(listeOri, scalarV);
  for(auto [p,val]:listeOri)
  {
    auto ix = filter.reverseFilter(p.first);
    auto iy = filter.reverseFilter(p.second);
    if(ix.has_value()&&iy.has_value())
      liste[Point(ix.value(),iy.value())]+=val;
  }
  
//   phy.fillConstraintsHessian(liste,scalarV);
}


void MixedPhyRestrainOri::fillConstraintsHessianStruct(std::map<Point, int>& liste)
{
  genericFillHessianStruct(
    std::bind(&MixedPhY::fillConstraintsHessianStruct, &phy,std::placeholders::_1),
    liste);
//   phy.fillConstraintsHessianStruct(liste);
}

int MixedPhyRestrainOri::getNbVars()
{
  //std::cout<<"Get "<<phy.getNbVars()-12+filter.removeSize()<<" variables"<<std::endl;
  return phy.getNbVars()-12+filter.removeSize();
}

int MixedPhyRestrainOri::getNbConstraints()
{
  return phy.getNbConstraints();
}

std::pair<Eigen::Vector3r, Eigen::Matrix3r> MixedPhyRestrainOri::posFrameAtEnd(int segment)
{
  return phy.posFrameAtEnd(segment);
}

Eigen::VectorXr MixedPhyRestrainOri::getPressureDistances() const
{
  return phy.getPressureDistances();
}

Eigen::VectorXr MixedPhyRestrainOri::getRepulsionEnergyPerField()
{
  return phy.getRepulsionEnergyPerField();
}

MixedSuperRibbon MixedPhyRestrainOri::get()
{
  return phy.get();
}

Eigen::VectorXr MixedPhyRestrainOri::cutClampingPt(Eigen::VectorXr ori)
{
  int t=ori.size()-12+filter.removeSize();
  Eigen::VectorXr res(ori.size()-12+filter.removeSize());
  for(int i=0; i<t; i++)
    res[i]=ori[filter(i)];
  return res;
}

Filter & MixedPhyRestrainOri::refFilter()
{
  return filter;
}
