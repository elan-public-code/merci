/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef MIXEDPHY_RESTRAINORI_H
#define MIXEDPHY_RESTRAINORI_H

#include <functional>
#include <optional>
#include <MixedSRCore/MixedPhY.hpp>


class Filter{
public:
  Filter();
  int eval(int);
  int removeSize();
  int operator()(int);
  std::optional<int> reverseFilter(int);
  void resetFilter();
  void filterOutPos(int index);
  void filterOutFrame(int i, int j);
  
  void test();
private:
  void reGenerateFilter();
  Eigen::Matrix<bool, 3, 1> positionRemove;
  Eigen::Matrix<bool, 3, 3> frameRemove;
  std::function<int(int)> shortToLong;
  std::function<std::optional<int>(int)> longToShort;
  int size;
};

class MixedPhyRestrainOri
{
public:
  using Point=MixedPhY::Point;
  MixedPhyRestrainOri(MixedSuperRibbon msr);
  MixedPhyRestrainOri(MixedPhY msr);
  MixedPhyRestrainOri(MixedSuperRibbon msr, Filter filter);
  MixedPhyRestrainOri(MixedPhY msr, Filter filter);
  //void set(MixedSuperRibbon msr);
  //void set(MixedPhY msr); 
  MixedSuperRibbon get();
  void setGravity(Eigen::Vector3r grav);
  Eigen::Vector3r getGravity();
  void setPoint(Eigen::VectorXr x);
  Eigen::VectorXr  getPoint();
  Eigen::VectorXr  getUpperBound();
  Eigen::VectorXr  getLowerBound();
  Eigen::VectorXr  getConstraintsUpperBound();
  Eigen::VectorXr  getConstraintsLowerBound();
  
  void tryBetterInitPoint();
  
  real getGravityEnergy();
  real getElasticEnergy();
  real getPressureEnergy();
  real getRepulsionEnergy();
  Eigen::VectorXr getElasticEnergyGradient();
  Eigen::VectorXr getGravityEnergyGradient();
  Eigen::VectorXr getPressureEnergyGradient();
  Eigen::VectorXr getRepulsionEnergyGradient();
  
  Eigen::VectorXr constraints();
  void fillConstraintsJac(std::map<Point, real>& liste);//The list must be fill with 0 at existing points
  void fillConstraintsJacStruct(std::map<Point, int>& liste);
  
  void fillConstraintsHessian(std::map<Point, real>& liste, Eigen::VectorXr scalarV);//The list must be fill with 0 at existing points
  void fillConstraintsHessianStruct(std::map<Point, int>& liste);
  void fillGravityHessian(std::map<Point, real>& liste, real scalar = 1.);//The list must be fill with int at existing points
  void fillGravityHessianStruct(std::map<Point, int>& liste);
  void fillElasticHessian(std::map<Point, real>& liste, real scalar = 1.);//The list must be fill with int at existing points
  void fillElasticHessianStruct(std::map<Point, int>& liste);
  void fillPressureHessian(std::map<Point, real>& liste, real scalar = 1.);//The list must be fill with int at existing points
  void fillPressureHessianStruct(std::map<Point, int>& liste);
  
  void fillRepulsionHessian(std::map<Point, real>& liste, real scalar = 1.);//The list must be fill with int at existing points
  void fillRepulsionHessianStruct(std::map<Point, int>& liste);
  
  int getNbVars();
  int getNbConstraints();
  
  Eigen::VectorXr getPressureDistances() const;
  Eigen::VectorXr getRepulsionEnergyPerField();
  
  std::pair<Eigen::Vector3r, Eigen::Matrix3r> posFrameAtEnd(int segment);
  Filter& refFilter();
protected:
  inline Eigen::VectorXr cutClampingPt(Eigen::VectorXr ori);
private:
  void genericFillHessian(std::function<void(std::map<Point, int> &liste)> fillHessianStruct, std::function<void(std::map<Point, real> &liste, real scalar)> fillHessian, std::map<Point, real>& liste, real scalar = 1.);
  void genericFillHessianStruct(std::function<void(std::map<Point, int> &liste)> fillHessianStruct, std::map<Point, int>& liste);
  MixedPhY phy;
  Eigen::VectorXr point;
  Filter filter;
};

#endif // MIXEDPHYBOXORI_H
