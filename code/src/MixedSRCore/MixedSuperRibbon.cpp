/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "MixedSuperRibbon.hpp"

MixedSuperRibbon::SuperRibbonSegment::SuperRibbonSegment()
{
  posStart.setZero();
  frameStart.setIdentity();
}

MixedSuperRibbon::MixedSuperRibbon()
{
}

void MixedSuperRibbon::setPosStart(int segment, Eigen::Vector3r pos)
{
  segments[segment].posStart = pos;
}

void MixedSuperRibbon::setFrameStart(int segment, Eigen::Matrix3r frame)
{
  segments[segment].frameStart = frame;
}

void MixedSuperRibbon::setWidth(real w)
{
  width = w;
}

void MixedSuperRibbon::setAreaDensity(real ad)
{
  areaDensity = ad;
}

void MixedSuperRibbon::setPoissonRatio(std::optional<real> nu)
{
  poissonRatio = nu;
}

void MixedSuperRibbon::setD(real d)
{
  D=d;
}

void MixedSuperRibbon::setNbSegments(int n)
{
  segments.resize(n);
}

void MixedSuperRibbon::setOmega1(int segment, LinearScalar omega1)
{
  segments[segment].omega1 = omega1;
}

void MixedSuperRibbon::setNatOmega1(int segment, LinearScalar natomega1)
{
  segments[segment].natOmega1 = natomega1;
}

void MixedSuperRibbon::setEta(int segment, LinearScalar eta)
{
  segments[segment].eta = eta;
}

void MixedSuperRibbon::setLength(int sid, real l)
{
  segments[sid].length=l;
}

size_t MixedSuperRibbon::getNbSegments() const
{
  return segments.size();
}

real MixedSuperRibbon::getD() const
{
  return D;
}

real MixedSuperRibbon::getWidth() const
{
  return width;
}

std::optional<real> MixedSuperRibbon::getPoissonRatio() const
{
  return poissonRatio;
}

real MixedSuperRibbon::getAreaDensity() const
{
  return areaDensity;
}

Eigen::Vector3r MixedSuperRibbon::getPosStart(int segment) const
{
  return segments[segment].posStart;
}

Eigen::Matrix3r MixedSuperRibbon::getFrameStart(int segment) const
{
  return segments[segment].frameStart;
}

real MixedSuperRibbon::getLength(int segment) const
{
  return segments[segment].length;
}

LinearScalar MixedSuperRibbon::getOmega1(int segment) const
{
  return segments[segment].omega1;
}

LinearScalar MixedSuperRibbon::getEta(int segment) const
{
  return segments[segment].eta;
}

LinearScalar MixedSuperRibbon::getNatOmega1(int segment) const
{
  return segments[segment].natOmega1;
}

void MixedSuperRibbon::setThickness(real thickness)
{
  this->thickness = thickness;
}

real MixedSuperRibbon::getThickness() const
{
  return thickness;
}
