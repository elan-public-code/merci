/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef MIXEDSUPERRIBBON_H
#define MIXEDSUPERRIBBON_H

#include <BaseCore/LinearScalar.hpp>
#include <Eigen/Dense>
#include <vector>
#include <optional>

class MixedSuperRibbon
{
public:
  MixedSuperRibbon();
  void setNbSegments(int n);
  void setOmega1(int segment, LinearScalar omega1);
  void setNatOmega1(int segment, LinearScalar natomega1);
  void setEta(int segment, LinearScalar eta);
  void setLength(int segment, real l);
  void setPosStart(int segment, Eigen::Vector3r pos);
  void setFrameStart(int segment, Eigen::Matrix3r frame);
  void setWidth(real w);
  void setAreaDensity(real ad);
  void setD(real d);
  void setPoissonRatio(std::optional<real> nu);
  void setThickness(real thickness);

  LinearScalar getOmega1(int segment) const;
  LinearScalar getNatOmega1(int segment) const;
  LinearScalar getEta(int segment) const;
  real getLength(int segment) const;

  size_t getNbSegments() const;
  Eigen::Vector3r getPosStart(int segment) const;
  Eigen::Matrix3r getFrameStart(int segment) const;
  real getWidth() const;
  real getAreaDensity() const;
  real getD() const;
  std::optional<real> getPoissonRatio() const;
  real getThickness() const;
private:
  struct SuperRibbonSegment {
    SuperRibbonSegment();
    LinearScalar eta;
    LinearScalar omega1;
    LinearScalar natOmega1;
    real length = 0.;
    Eigen::Vector3r posStart;
    Eigen::Matrix3r frameStart;
  };
  real width = 0.01;
  real thickness =0.0001;
  real D = 1.e-4;
  std::optional<real> poissonRatio = std::nullopt; 
  real areaDensity = 0.1;
  std::vector<SuperRibbonSegment> segments;
};

#endif // MIXEDSUPERRIBBON_H
