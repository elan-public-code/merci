/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "Mesh.hpp"

namespace libobj{

void Mesh::write(ObjFileWriter& os)
{
    if(isGroup())os<<ObjGroup(getObjName());
    else os<<ObjObject(getObjName());
    size_t t = getNbVertex();
    for(size_t i=0; i<t;i++)
        os<<getVertexAt(i);
    t = getNbFaces();
    for(size_t i=0; i<t;i++)
        os<<getFaceAt(i);
}

ObjFileWriter& operator<<(ObjFileWriter&os, Mesh& mesh)
{
  mesh.write(os);
  return os;
}

};
