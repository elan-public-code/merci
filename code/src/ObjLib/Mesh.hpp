/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef MESH_H
#define MESH_H
#include <string>
#include "ObjFileWriter.hpp"

namespace libobj{

/**
 * This class is an abstract representation of a mesh. If you have a triangular mesh what you need for is TriMesh. Otherwise you should derive a new class from this.
 */
class Mesh
{
public:
    void write(ObjFileWriter& os);
private:
    virtual size_t getNbVertex() = 0;
    virtual size_t getNbFaces() = 0;
    virtual std::string getObjName() = 0;
    virtual ObjVertex getVertexAt(size_t i) = 0;
    virtual ObjFace getFaceAt(size_t i) = 0;
    virtual bool isGroup() = 0;
};

ObjFileWriter& operator<<(ObjFileWriter&os, Mesh& mesh);

};

#endif // MESH_H
