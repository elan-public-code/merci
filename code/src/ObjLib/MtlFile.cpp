/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "MtlFile.hpp"
#include <fstream>

namespace libobj
{
  
Mtl::Mtl()
{
}

Mtl::Mtl(std::string name, Eigen::Vector3f basecolor):
 ambient(basecolor),
 diffuse(basecolor),
 name(name)
{
}

std::ostream& operator<<(std::ostream& os, const Mtl& mtl)
{
  os<<"newmtl "<<mtl.name<<std::endl;
  os<<"Ka "<<mtl.ambient[0]<<' '<<mtl.ambient[1]<<' '<<mtl.ambient[2]<<std::endl;
  os<<"Kd "<<mtl.diffuse[0]<<' '<<mtl.diffuse[1]<<' '<<mtl.diffuse[2]<<std::endl;
  os<<"Ks "<<mtl.specular[0]<<' '<<mtl.specular[1]<<' '<<mtl.specular[2]<<std::endl;
  os<<"Ns "<<mtl.specularExp<<std::endl;
  os<<std::endl;
  return os;
}

bool MtlFile::mtlDefined(std::string name)
{
  return !(theMaterials.find(name)==theMaterials.end());
}

void MtlFile::addMtl(Mtl material, bool overide)
{
  bool add=overide||(!mtlDefined(material.name));
  if(add) theMaterials[material.name]=material;
}

void MtlFile::saveFile(std::string filename)
{
  std::ofstream os(filename);
  for(auto m:theMaterials)
    os<<m.second;
}

};
