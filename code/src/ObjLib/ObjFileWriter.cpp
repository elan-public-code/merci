/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "ObjFileWriter.hpp"

namespace libobj
{

ObjObject::ObjObject(const std::string &name): name(name)
{}

ObjGroup::ObjGroup(const std::string &name): name(name)
{}

ObjFace::ObjFace(const Eigen::Vector3i &tface)
{
  for (int i = 0; i < 3; i++)face.push_back(tface(i));
}

ObjFace::ObjFace(const Eigen::Vector4i &tface)
{
  for (int i = 0; i < 4; i++)face.push_back(tface(i));
}

ObjFace::ObjFace(const Eigen::VectorXi &tface)
{
  for (int i = 0; i < tface.size(); i++)face.push_back(tface(i));
}

ObjVertex::ObjVertex(Eigen::Vector3d pos, Eigen::Vector3d normale):
  pos(pos),
  normale(normale)
{}

ObjFileWriter::ObjFileWriter()
{
}

ObjFileWriter::~ObjFileWriter()
{
  if(os)
    close();
}

void ObjFileWriter::open(std::string filename)
{
  os.open(filename);
  this->matfilename = filename + ".mtl";
  if (filename.size() > 4) {
    auto t = filename.size();
    if (filename[t - 4] == '.' && filename[t - 3] == 'o' && filename[t - 2] == 'b' && filename[t - 1] == 'j') {
      filename[t - 3] = 'm';
      filename[t - 2] = 't';
      filename[t - 1] = 'l';
      matfilename = filename;
    }
  }
  os<<"mtllib "<<matfilename<<std::endl;
}

void ObjFileWriter::close()
{
  os.close();
  matFile.saveFile(matfilename);
  objectVertexOffset = 1;
  objectNbVertices = 0;
}

void ObjFileWriter::addVertex(ObjVertex vertex)
{
  if (!os)return;
  os << "v " << vertex.pos.x() << ' ' << vertex.pos.y() << ' ' << vertex.pos.z() << std::endl;
  os << "vn " << vertex.normale.x() << ' ' << vertex.normale.y() << ' ' << vertex.normale.z() << std::endl;
  objectNbVertices++;
}

void ObjFileWriter::addFace(ObjFace face)
{
  if (!os)return;
  os << "f ";
  for (int i : face.face)
    os << i + objectVertexOffset << "//" << i + objectVertexOffset << ' ';
  os << std::endl;
}


void ObjFileWriter::newObject(ObjObject name)
{
  if (!os)return;
  objectVertexOffset += objectNbVertices;
  objectNbVertices = 0;
  os << "o " << name.name << std::endl;
}

void ObjFileWriter::newGroup(ObjGroup name)
{
  if (!os)return;
  objectVertexOffset += objectNbVertices;
  objectNbVertices = 0;
  os << "g " << name.name << std::endl;
}

void ObjFileWriter::useMaterial(Mtl mat)
{
  if (!os)return;
  matFile.addMtl(mat, false);
  os << "usemtl " << mat.name << std::endl;
}

ObjFileWriter &ObjFileWriter::operator<<(const ObjVertex &v)
{
  addVertex(v);
  return *this;
}

ObjFileWriter &ObjFileWriter::operator<<(const ObjFace &f)
{
  addFace(f);
  return *this;
}

ObjFileWriter &ObjFileWriter::operator<<(const ObjGroup &g)
{
  newGroup(g);
  return *this;
}

ObjFileWriter &ObjFileWriter::operator<<(const ObjObject &o)
{
  newObject(o);
  return *this;
}

ObjFileWriter &ObjFileWriter::operator<<(const Mtl &m)
{
  useMaterial(m);
  return *this;
}

};
