/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef OBJFILEWRITER_H
#define OBJFILEWRITER_H

#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <Eigen/Dense>
#include "MtlFile.hpp"

namespace libobj{

  /**
   * In this code, a mesh vertice always has a position and a normal. If you need other stuff, better learn OBJ format before modifing this.
   */
struct ObjVertex{
    ObjVertex(Eigen::Vector3d pos, Eigen::Vector3d normale);
    Eigen::Vector3d pos;
    Eigen::Vector3d normale;
};

/**
* A face is defined by the indices of the vertices. In general there are 3 vertices per face, but OBJ format alows for more
*/
struct ObjFace{
    ObjFace(const Eigen::Vector3i& face);
    ObjFace(const Eigen::Vector4i& face);
    ObjFace(const Eigen::VectorXi& face);
    std::vector<int> face;
};

/**
 * OBJ files can contains many object, you should name them smartly to distinguish them in your favorite viewer (Blender I guess)
 */
struct ObjObject{
    ObjObject(const std::string& name);
    std::string name;
};

/**
 * OBJ files can contains groups, in case an object has several parts
 */
struct ObjGroup{
    ObjGroup(const std::string& name);
    std::string name;
};

/**
 * Similar to std::ofstream, but specialiazed for meshes files, written with obj format.
 * This class is quite low level. To save meshes, you should consider Mesh or preferably TriMesh class.
 */
class ObjFileWriter
{
public:
    ObjFileWriter();
    ~ObjFileWriter();
    ObjFileWriter(const ObjFileWriter&) = delete;
    ObjFileWriter& operator =(const ObjFileWriter&) = delete;
    void open(std::string filename);
    void close();
    void addVertex(ObjVertex vertex);
    void addFace(ObjFace face);
    void newObject(ObjObject name);
    void newGroup(ObjGroup name);
    void useMaterial(Mtl mat);
    ObjFileWriter& operator<<(const ObjVertex& v);///< Shortcut to addVertex
    ObjFileWriter& operator<<(const ObjFace& f);///< Shortcut to addFace
    ObjFileWriter& operator<<(const ObjGroup& g);///< Shortcut to newGroup
    ObjFileWriter& operator<<(const ObjObject& o);///< Shortcut to newObject
    ObjFileWriter& operator<<(const Mtl& m);///< Shortcut to useMaterial
private:
    std::ofstream os;
    std::string matfilename;
    MtlFile matFile;
    int objectVertexOffset = 1;
    int objectNbVertices = 0;
};

};
#endif // OBJFILEWRITER_H
