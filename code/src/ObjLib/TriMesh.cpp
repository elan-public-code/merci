/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "TriMesh.hpp"

namespace libobj{

TriMesh::TriMesh()
{

}

void TriMesh::defineAsGroup(bool group)
{
  this->group = group;
}

void TriMesh::resize(size_t nbVertices, size_t nbFaces)
{
    pos.resize(nbVertices);
    nml.resize(nbVertices);
    faces.resize(nbFaces);
}

void TriMesh::setVertex(size_t at, Eigen::Vector3d position, Eigen::Vector3d normale)
{
    pos.at(at) = position;
    nml.at(at) = normale;
}

void TriMesh::setFace(size_t at, int idx1, int idx2, int idx3)
{
    faces.at(at) = Eigen::Vector3i(idx1,idx2,idx3);
}

size_t TriMesh::getNbVertex()
{
    return pos.size();
}

size_t TriMesh::getNbFaces()
{
    return faces.size();
}

ObjVertex TriMesh::getVertexAt(size_t i)
{
    return ObjVertex(pos.at(i),nml.at(i));
}

ObjFace TriMesh::getFaceAt(size_t i)
{
    return ObjFace(faces.at(i));
}

bool TriMesh::isGroup()
{
  return group;
}

};
