/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef TRIMESH_H
#define TRIMESH_H
#include <vector>
#include <Eigen/Dense>
#include "Mesh.hpp"

namespace libobj{

  /**
   * A triangular Mesh should be build with this class. Please be reasonnable and start indexing at 0.
   * Please call resize before specifing vertices or faces (just like for the std::vector class).
   * Also note that for convinience, some special meshes are implemented in the RibbonObjLib library like ArrowHD or TubeMesh
   * */
class TriMesh : public Mesh
{
public:
    TriMesh();
protected:
    void resize(size_t nbVertices, size_t nbFaces);
    void setVertex(size_t at, Eigen::Vector3d position, Eigen::Vector3d normale);
    void setFace(size_t at, int idx1, int idx2, int idx3);
    void defineAsGroup(bool group);///<Obj files contains groups. If you don't know what this is, do not call this function.
private:
    virtual size_t getNbVertex() override final;
    virtual size_t getNbFaces() override final;
    virtual ObjVertex getVertexAt(size_t i) override final;
    virtual ObjFace getFaceAt(size_t i) override final;
    virtual bool isGroup() override final;
    std::vector<Eigen::Vector3d> pos;
    std::vector<Eigen::Vector3d> nml;
    std::vector<Eigen::Vector3i> faces;
    bool group = false;
};

};

#endif // TRIMESH_H
