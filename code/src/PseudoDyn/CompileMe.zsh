if [[ ! -a PartExact.o ]]; then
  clang++-8 -std=c++17 -g -c -I/usr/include/eigen3 PartExact.cpp
fi
  
if [[ ! -a PartApprox.o ]]; then
  clang++-8 -std=c++17 -g -c -I/usr/include/eigen3 PartApprox.cpp
fi

llvm-ar-8 rc libGenPseudoDyn.a Part*.o 
