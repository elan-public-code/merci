/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "LightRS.hpp"
#include "MathOperations.hpp"
#include <Logger.hpp>

using namespace MathOperations;

size_t LightRS::vpointSize() const { return 2*(1+sr.getNbSegments()); }
size_t LightRS::vpointIDb0() const { return 0; }
size_t LightRS::vpointIDm0() const { return 1; }
size_t LightRS::vpointIDa ( const size_t& segmentID ) const { return 2+2*segmentID; }
size_t LightRS::vpointIDn ( const size_t& segmentID ) const { return 2+2*segmentID+1; }

LightRS::LightRS(Eigen::Vector3r gravity):
  G(gravity)
{
}

void LightRS::set( const SuperRibbon& sr ) 
{
  this->sr = sr; 
  size_t t=sr.getNbSegments();
  parts.clear();
  parts.reserve(t);
  for(size_t i=0;i<t;i++)
    parts.push_back(SegmentSystem(G, sr.getAreaDensity(), sr.getD(), sr.getWidth(), sr.getLength(i), sr.getPoissonRatio()));
  updateParts();
}

SuperRibbon LightRS::get() const { return sr; }

void LightRS::setPoint ( const Eigen::VectorXr& x )
{
  int t = sr.getNbSegments();
  assert(x.size() == vpointSize());
  sr.setOmega1BOr(x[vpointIDb0()]);
  sr.setEtaMOr(x[vpointIDm0()]);
  for (int i = 0; i < t; i++) {
    sr.setOmega1A(i, x[vpointIDa(i)]);
    sr.setEtaN(i, x[vpointIDn(i)]);
  }
  updateParts();
}

void LightRS::updateParts()
{
  gradPos.clear();
  gradFrames.clear();
  size_t t=sr.getNbSegments();
  Eigen::Vector3r p=sr.getPosOr();
  Eigen::Matrix3r f=sr.getFrameOr();
  for(size_t i=0;i<t;i++)
  {
    parts.at(i).setPoint(p,f, sr.getOmega1(i), sr.getEta(i), sr.getNatOmega1(i));
    std::tie(p,f)=parts.at(i).endPosFrame();
  }
}

Eigen::Vector3r LightRS::getEndPos()
{
  return parts.back().endPosFrame().first;
}

Eigen::Matrix3r LightRS::getEndFrame()
{
  return parts.back().endPosFrame().second;
}

Eigen::VectorXr LightRS::getPoint() const
{
  int t = sr.getNbSegments();
  Eigen::VectorXr pt(vpointSize());
  pt[vpointIDb0()] = sr.getOmega1(0).B;
  pt[vpointIDm0()] = sr.getEta(0).B;
  for (int i = 0; i < t; i++) {
    pt[vpointIDa(i)] = sr.getOmega1(i).A;
    pt[vpointIDn(i)] = sr.getEta(i).A;
  }
  return pt;
}

Eigen::VectorXr LightRS::getLowerBound() const
{
  real lb = -2. / sr.getWidth();
  int t = sr.getNbSegments();
  Eigen::VectorXr pt(vpointSize());
  pt[vpointIDb0()] = -std::numeric_limits<real>::infinity();
  pt[vpointIDm0()] = -std::numeric_limits<real>::infinity();
  for (int i = 0; i < t; i++) {
    pt[vpointIDa(i)] = -std::numeric_limits<real>::infinity();
    pt[vpointIDn(i)] = lb;
  }
  return pt;
}

Eigen::VectorXr LightRS::getUpperBound() const
{
  real ub = 2. / sr.getWidth();
  int t = sr.getNbSegments();
  Eigen::VectorXr pt(vpointSize());
  pt[vpointIDb0()] = std::numeric_limits<real>::infinity();
  pt[vpointIDm0()] = std::numeric_limits<real>::infinity();
  for (int i = 0; i < t; i++) {
    pt[vpointIDa(i)] = std::numeric_limits<real>::infinity();
    pt[vpointIDn(i)] = ub;
  }
  return pt;
}

void LightRS::logHealthReport()
{
  LoggerMsg msg("Health repport");
  Eigen::VectorXr pt=getPoint();
  Eigen::VectorXr ub=getUpperBound();
  Eigen::VectorXr lb=getLowerBound();
  bool noMesg=true;
  for(int i=0;i<pt.size();i++)
  {
    double lerr=pt[i]-lb[i];
    if(lerr<0.)
    {
      std::stringstream ptVal,lVal;
      ptVal<<pt[i];
      lVal<<lb[i];
      auto ssmsg=msg.addChild("CRITICAL","Lower bound violation");
      ssmsg->addChild("segment",std::to_string(i));
      ssmsg->addChild("lower bound",lVal.str());
      ssmsg->addChild("value",ptVal.str());
      noMesg=false;
      continue;
    }
    if(lerr<1.e-5)
    {
      std::stringstream errVal;
      errVal<<lerr;
      auto ssmsg=msg.addChild("Touching","Lower bound");
      ssmsg->addChild("segment",std::to_string(i));
      ssmsg->addChild("distance",errVal.str());
      noMesg=false;
      continue;
    }
      
    double uerr=ub[i]-pt[i];
    if(uerr<0.)
    {
      std::stringstream ptVal,uVal;
      ptVal<<pt[i];
      uVal<<ub[i];
      auto ssmsg=msg.addChild("CRITICAL","Upper bound violation");
      ssmsg->addChild("segment",std::to_string(i));
      ssmsg->addChild("upper bound",uVal.str());
      ssmsg->addChild("value",ptVal.str());
      noMesg=false;
      continue;
    }
    if(uerr<1.e-5)
    {
      std::stringstream errVal;
      errVal<<uerr;
      auto ssmsg=msg.addChild("Touching","Upper bound");
      ssmsg->addChild("segment",std::to_string(i));
      ssmsg->addChild("distance",errVal.str());
      noMesg=false;
      continue;
    }
  }
  if(noMesg)msg.addChild("Sound","Nothing to repport");
  getLogger()->Write(LoggerLevel::INFORMATION,"LightRS",msg);
}

real LightRS::energy()
{
  real E=0;
  size_t t = static_cast<size_t>(sr.getNbSegments());
  for(size_t i=0;i<t;i++)
  {
    E+=parts.at(i).energy();
    auto [eP,eF]=parts.at(i).endPosFrame();
    auto eta = sr.getEta(i);
    Eigen::Vector3d q=0.5 * sr.getWidth() * (eF.col(0) + (eta.B+sr.getLength(i)*eta.A) * eF.col(2));
    Eigen::Vector3d end1 = eP+q;
    Eigen::Vector3d end2 = eP-q;
    double y1=end1[1],y2=end2[1];
    if(y1<=0) E+=sr.getLength(i)*(exp(y1*y1)-1);
    if(y2<=0) E+=sr.getLength(i)*(exp(y2*y2)-1);
  }
  return E;
}

Eigen::VectorXr LightRS::gradient()
{
  Eigen::VectorXr grad(vpointSize());
  grad.setZero();
  if(gradPos.empty())
    computeGradPosFrames();
  addGradient(grad);
  return grad;
}

Eigen::VectorXr LightRS::gradPosX()
{
  Eigen::VectorXr grad(vpointSize());
  grad.setZero();
  gradPos.clear();
  gradFrames.clear();
  computeGradPosFrames();
  size_t t=sr.getNbSegments();
  size_t n=vpointSize();
  for(size_t i=0;i<n;i++)
    grad[i] = gradPos.at({t,i})[2];
  return grad;
}

Eigen::VectorXr LightRS::gradFrameX()
{
  Eigen::VectorXr grad(vpointSize());
  grad.setZero();
  gradPos.clear();
  gradFrames.clear();
  computeGradPosFrames();
  size_t t=sr.getNbSegments();
  size_t n=vpointSize();
  for(size_t i=0;i<n;i++)
    grad[i]=gradFrames.at({t,i})(2,0);
  return grad;
}

real LightRS::posX()
{
  return parts.back().endPosFrame().first[2];
}

Eigen::Matrix<Eigen::VectorXr, 3, 1> LightRS::gradEndPos()
{
  Eigen::Matrix<Eigen::VectorXr, 3, 1> grad;
  Eigen::VectorXr myZero(vpointSize());
  myZero.setZero();
  grad.setConstant(myZero);
  gradPos.clear();
  gradFrames.clear();
  computeGradPosFrames();
  size_t t=sr.getNbSegments();
  size_t n=vpointSize();
  for(size_t i=0;i<n;i++)
  {
    grad[0][i] = gradPos.at({t,i})[0];
    grad[1][i] = gradPos.at({t,i})[1];
    grad[2][i] = gradPos.at({t,i})[2];
  }
  return grad;
}

Eigen::Matrix<Eigen::VectorXr, 3, 3> LightRS::gradEndFrame()
{
  Eigen::Matrix<Eigen::VectorXr, 3, 3> grad;
  Eigen::VectorXr myZero(vpointSize());
  myZero.setZero();
  grad.setConstant(myZero);
  gradPos.clear();
  gradFrames.clear();
  computeGradPosFrames();
  size_t t=sr.getNbSegments();
  size_t n=vpointSize();
  for(size_t i=0;i<n;i++)
    for(size_t idx=0;idx<9;idx++) grad(idx/3,idx%3)[i]=gradFrames.at({t,i})(idx/3,idx%3);
  return grad;
}

void LightRS::computeGradPosFrames()
{
  Eigen::Matrix<Eigen::Matrix<real,3,1>,3,1> dPosdPos;
  dPosdPos<<Eigen::Vector3r(1.,0.,0.),Eigen::Vector3r(0.,1.,0.),Eigen::Vector3r(0.,0.,1.);
  
  size_t t = sr.getNbSegments();
  for(size_t segmentID = 1; segmentID<=t;segmentID++)
  {
    const auto partSegmentGradPos = parts.at(segmentID-1).gradPos();
    const auto partSegmentGradFrame = parts.at(segmentID-1).gradFrame();
    gradPos[{segmentID,vpointIDa(segmentID-1)}]=extractDerivation(partSegmentGradPos,0);
    gradPos[{segmentID,vpointIDn(segmentID-1)}]=extractDerivation(partSegmentGradPos,2);
    gradFrames[{segmentID,vpointIDa(segmentID-1)}]=extractDerivation(partSegmentGradFrame,0);
    gradFrames[{segmentID,vpointIDn(segmentID-1)}]=extractDerivation(partSegmentGradFrame,2);
    
    
    for(size_t j=0; j<segmentID-1;j++)
    {
      gradPos[{segmentID,vpointIDa(j)}] = sr.getLength(j) * extractDerivation(partSegmentGradPos,1)
        + compDeriv(parts.at(segmentID-1).getdPosdFrame(), gradFrames[{segmentID-1,vpointIDa(j)}]) 
        + compDeriv(dPosdPos, gradPos[{segmentID-1,vpointIDa(j)}]);
      gradPos[{segmentID,vpointIDn(j)}]=sr.getLength(j) * extractDerivation(partSegmentGradPos,3)
        + compDeriv(parts.at(segmentID-1).getdPosdFrame(), gradFrames[{segmentID-1,vpointIDn(j)}]) 
        + compDeriv(dPosdPos, gradPos[{segmentID-1,vpointIDn(j)}]);
      gradFrames[{segmentID,vpointIDa(j)}]=sr.getLength(j) * extractDerivation(partSegmentGradFrame,1) 
        + compDeriv(parts.at(segmentID-1) .getdFramedFrame(), gradFrames[{segmentID-1,vpointIDa(j)}]);
      gradFrames[{segmentID,vpointIDn(j)}]=sr.getLength(j) * extractDerivation(partSegmentGradFrame,3) 
        + compDeriv(parts.at(segmentID-1).getdFramedFrame(), gradFrames[{segmentID-1,vpointIDn(j)}]);
    }
    
    
    gradPos[{segmentID,vpointIDb0()}]=extractDerivation(partSegmentGradPos,1);
    gradPos[{segmentID,vpointIDm0()}]=extractDerivation(partSegmentGradPos,3);
    gradFrames[{segmentID,vpointIDb0()}]=extractDerivation(partSegmentGradFrame,1);
    gradFrames[{segmentID,vpointIDm0()}]=extractDerivation(partSegmentGradFrame,3);

    if(segmentID>1)
    {
      gradPos[{segmentID,vpointIDb0()}] +=
        compDeriv(parts.at(segmentID-1).getdPosdFrame(), gradFrames.at({segmentID-1,vpointIDb0()})) + compDeriv(dPosdPos, gradPos[{segmentID-1,vpointIDb0()}]);
      gradPos[{segmentID,vpointIDm0()}] +=
        compDeriv(parts.at(segmentID-1).getdPosdFrame(), gradFrames[{segmentID-1,vpointIDm0()}]) + compDeriv(dPosdPos, gradPos[{segmentID-1,vpointIDm0()}]);
      gradFrames[{segmentID,vpointIDb0()}] +=
        compDeriv(parts.at(segmentID-1).getdFramedFrame(), gradFrames[{segmentID-1,vpointIDb0()}]);
      gradFrames[{segmentID,vpointIDm0()}] +=
        compDeriv(parts.at(segmentID-1).getdFramedFrame(), gradFrames[{segmentID-1,vpointIDm0()}]);
    }
  }
}


void LightRS::addGradient( Eigen::VectorXr& grad)
{
  size_t t=sr.getNbSegments();
  for(size_t segmentID=0;segmentID<t;segmentID++)
  {
    Eigen::Vector4r tmp = parts.at(segmentID).gradient();
    grad[vpointIDa(segmentID)]+=tmp[0];
    grad[vpointIDn(segmentID)]+=tmp[2];
    
    const Eigen::Matrix3r dEnergydFrameSegment = parts.at(segmentID).getdEnergydFrameOut();
    const Eigen::Vector3r dEnergydPosSegment = parts.at(segmentID).getdEnergydPosOut();
    for(size_t j=0;j<segmentID;j++)
    {
      grad[vpointIDa(j)]+=sr.getLength(j)*tmp[1];
      grad[vpointIDn(j)]+=sr.getLength(j)*tmp[3];
      
      grad[vpointIDn(j)] += 
        contractGeneralDotP(dEnergydFrameSegment, gradFrames.at({segmentID,vpointIDn(j)})) 
        + dEnergydPosSegment.dot(gradPos.at({segmentID,vpointIDn(j)}));
      grad[vpointIDa(j)] += 
        contractGeneralDotP(dEnergydFrameSegment, gradFrames.at({segmentID,vpointIDa(j)})) 
        + dEnergydPosSegment.dot(gradPos.at({segmentID,vpointIDa(j)}));
    }
  }
  
  
  {
    int t = sr.getNbSegments();
    grad[vpointIDb0()]=0.;
    grad[vpointIDm0()]=0.;
    for (int i = t - 1; i > 0; i--) {
      real dpreEdbSegment = parts.at(i).gradient()[1];
      real dpreEdmSegment = parts.at(i).gradient()[3];
      real gradIb = 
        dpreEdbSegment 
        + contractGeneralDotP(parts.at(i).getdEnergydFrameOut(), gradFrames.at({i,vpointIDb0()})) 
        + parts.at(i).getdEnergydPosOut().dot(gradPos.at({i,vpointIDb0()}));
      grad[vpointIDb0()] += gradIb;
      real gradIm = 
        dpreEdmSegment 
        + contractGeneralDotP(parts.at(i).getdEnergydFrameOut(), gradFrames.at({i,vpointIDm0()})) 
        + parts.at(i).getdEnergydPosOut().dot(gradPos.at({i,vpointIDm0()}));
      grad[vpointIDm0()] += gradIm;
    }
    grad[vpointIDb0()] += parts.at(0).gradient()[1];
    grad[vpointIDm0()] += parts.at(0).gradient()[3];
      
  }
  
  double ribbonWidth=sr.getWidth();
  for(size_t segmentID=0;segmentID<t;segmentID++)
  {
    auto [eP,eF]=parts.at(segmentID).endPosFrame();
    auto eta = sr.getEta(segmentID);
    Eigen::Vector3d q=0.5 * ribbonWidth * (eF.col(0) + (eta.B+sr.getLength(segmentID)*eta.A) * eF.col(2));
    Eigen::Vector3d end1 = eP+q;
    Eigen::Vector3d end2 = eP-q;
    double y1=end1[1],y2=end2[1];
    double yd,qd;
    double ribbonLength = sr.getLength(segmentID);
    if(y1<=0) 
    {
      yd=gradPos.at({segmentID+1,vpointIDb0()})(1);
      qd = 0.5 * ribbonWidth * (gradFrames.at({segmentID+1,vpointIDb0()})(1,0) + (eta.B+ribbonLength*eta.A) * gradFrames.at({segmentID+1,vpointIDb0()})(1,2));
      grad[vpointIDb0()] += ribbonLength*(yd+qd)*2.*y1*exp(y1*y1);
      
      yd=gradPos.at({segmentID+1,vpointIDm0()})(1);
      qd = 0.5 * ribbonWidth * (gradFrames.at({segmentID+1,vpointIDm0()})(1,0) + (eta.B+ribbonLength*eta.A) * gradFrames.at({segmentID+1,vpointIDm0()})(1,2));
      grad[vpointIDm0()] += ribbonLength*(yd+qd)*2.*y1*exp(y1*y1);
      
      for(int j=0;j<=segmentID;j++)
      {
        yd=gradPos.at({segmentID+1,vpointIDa(j)})(1);
        qd = 0.5 * ribbonWidth * (gradFrames.at({segmentID+1,vpointIDa(j)})(1,0) + (eta.B+ribbonLength*eta.A) * gradFrames.at({segmentID+1,vpointIDa(j)})(1,2)) ;
        grad[vpointIDa(j)] += ribbonLength*(yd+qd)*2.*y1*exp(y1*y1);
        
        yd=gradPos.at({segmentID+1,vpointIDn(j)})(1);
        qd = 0.5 * ribbonWidth * (gradFrames.at({segmentID+1,vpointIDn(j)})(1,0) + (eta.B+ribbonLength*eta.A) * gradFrames.at({segmentID+1,vpointIDn(j)})(1,2)) ;
        grad[vpointIDn(j)] += ribbonLength*(yd+qd)*2.*y1*exp(y1*y1);
      }
    }
    if(y2<=0)
    {
      yd=gradPos.at({segmentID+1,vpointIDb0()})(1);
      qd = 0.5 * ribbonWidth * (gradFrames.at({segmentID+1,vpointIDb0()})(1,0) + (eta.B+ribbonLength*eta.A) * gradFrames.at({segmentID+1,vpointIDb0()})(1,2));
      grad[vpointIDb0()] += ribbonLength*(yd-qd)*2.*y2*exp(y2*y2);
      
      yd=gradPos.at({segmentID+1,vpointIDm0()})(1);
      qd = 0.5 * ribbonWidth * (gradFrames.at({segmentID+1,vpointIDm0()})(1,0) + (eta.B+ribbonLength*eta.A) * gradFrames.at({segmentID+1,vpointIDm0()})(1,2));
      grad[vpointIDm0()] += ribbonLength*(yd-qd)*2.*y2*exp(y2*y2);
      
      for(int j=0;j<=segmentID;j++)
      {
        yd=gradPos.at({segmentID+1,vpointIDa(j)})(1);
        qd = 0.5 * ribbonWidth * (gradFrames.at({segmentID+1,vpointIDa(j)})(1,0) + (eta.B+ribbonLength*eta.A) * gradFrames.at({segmentID+1,vpointIDa(j)})(1,2)) ;
        grad[vpointIDa(j)] += ribbonLength*(yd-qd)*2.*y2*exp(y2*y2);
        
        yd=gradPos.at({segmentID+1,vpointIDn(j)})(1);
        qd = 0.5 * ribbonWidth * (gradFrames.at({segmentID+1,vpointIDn(j)})(1,0) + (eta.B+ribbonLength*eta.A) * gradFrames.at({segmentID+1,vpointIDn(j)})(1,2)) ;
        grad[vpointIDn(j)] += ribbonLength*(yd-qd)*2.*y2*exp(y2*y2);
      }
    }
  }
}

