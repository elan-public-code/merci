/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef PART_EXACT_HPP
#define PART_EXACT_HPP

#include <Eigen/Dense>
#include <cmath>

class FunctionPartExact{
public:
 //Parameters
  Eigen::Vector3d G;
  double D;
  double areaDensity;
  double soffset;
  double width;

 //In arguments
  double a;
  double anat;
  double b;
  double bnat;
  double length;
  double m;
  double n;
  double nu;

 //In functions
  Eigen::Matrix3d R;
  Eigen::Matrix3d d1Rda;
  Eigen::Matrix3d d1Rdb;
  Eigen::Matrix3d d1Rdm;
  Eigen::Matrix3d d1Rdn;
  Eigen::Matrix3d d2Rdada;
  Eigen::Matrix3d d2Rdadb;
  Eigen::Matrix3d d2Rdadm;
  Eigen::Matrix3d d2Rdadn;
  Eigen::Matrix3d d2Rdbda;
  Eigen::Matrix3d d2Rdbdb;
  Eigen::Matrix3d d2Rdbdm;
  Eigen::Matrix3d d2Rdbdn;
  Eigen::Matrix3d d2Rdmda;
  Eigen::Matrix3d d2Rdmdb;
  Eigen::Matrix3d d2Rdmdm;
  Eigen::Matrix3d d2Rdmdn;
  Eigen::Matrix3d d2Rdnda;
  Eigen::Matrix3d d2Rdndb;
  Eigen::Matrix3d d2Rdndm;
  Eigen::Matrix3d d2Rdndn;
  Eigen::Vector3d d1rda;
  Eigen::Vector3d d1rdb;
  Eigen::Vector3d d1rdm;
  Eigen::Vector3d d1rdn;
  Eigen::Vector3d d2rdada;
  Eigen::Vector3d d2rdadb;
  Eigen::Vector3d d2rdadm;
  Eigen::Vector3d d2rdadn;
  Eigen::Vector3d d2rdbda;
  Eigen::Vector3d d2rdbdb;
  Eigen::Vector3d d2rdbdm;
  Eigen::Vector3d d2rdbdn;
  Eigen::Vector3d d2rdmda;
  Eigen::Vector3d d2rdmdb;
  Eigen::Vector3d d2rdmdm;
  Eigen::Vector3d d2rdmdn;
  Eigen::Vector3d d2rdnda;
  Eigen::Vector3d d2rdndb;
  Eigen::Vector3d d2rdndm;
  Eigen::Vector3d d2rdndn;
  Eigen::Vector3d r;

 //Out arguments
  Eigen::Matrix3d dEgdFrame;
  Eigen::Matrix3d dEwdFrame;
  Eigen::Matrix3d dFrameda;
  Eigen::Matrix3d dFramedada;
  Eigen::Matrix3d dFramedadb;
  Eigen::Matrix3d dFramedadm;
  Eigen::Matrix3d dFramedadn;
  Eigen::Matrix3d dFramedb;
  Eigen::Matrix3d dFramedbda;
  Eigen::Matrix3d dFramedbdb;
  Eigen::Matrix3d dFramedbdm;
  Eigen::Matrix3d dFramedbdn;
  Eigen::Matrix3d dFramedm;
  Eigen::Matrix3d dFramedmda;
  Eigen::Matrix3d dFramedmdb;
  Eigen::Matrix3d dFramedmdm;
  Eigen::Matrix3d dFramedmdn;
  Eigen::Matrix3d dFramedn;
  Eigen::Matrix3d dFramednda;
  Eigen::Matrix3d dFramedndb;
  Eigen::Matrix3d dFramedndm;
  Eigen::Matrix3d dFramedndn;
  Eigen::Matrix3d endFrame;
  Eigen::Matrix4d hessEg;
  Eigen::Matrix4d hessEw;
  Eigen::Vector3d dEgdPos;
  Eigen::Vector3d dEwdPos;
  Eigen::Vector3d dPosda;
  Eigen::Vector3d dPosdada;
  Eigen::Vector3d dPosdadb;
  Eigen::Vector3d dPosdadm;
  Eigen::Vector3d dPosdadn;
  Eigen::Vector3d dPosdb;
  Eigen::Vector3d dPosdbda;
  Eigen::Vector3d dPosdbdb;
  Eigen::Vector3d dPosdbdm;
  Eigen::Vector3d dPosdbdn;
  Eigen::Vector3d dPosdm;
  Eigen::Vector3d dPosdmda;
  Eigen::Vector3d dPosdmdb;
  Eigen::Vector3d dPosdmdm;
  Eigen::Vector3d dPosdmdn;
  Eigen::Vector3d dPosdn;
  Eigen::Vector3d dPosdnda;
  Eigen::Vector3d dPosdndb;
  Eigen::Vector3d dPosdndm;
  Eigen::Vector3d dPosdndn;
  Eigen::Vector3d endPos;
  Eigen::Vector4d gradEg;
  Eigen::Vector4d gradEw;
  double Eg;
  double Ew;
  double H;
  double K;
  double P;

  void operator()();
  };

#endif
