/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "PartSegment.hpp"
#include "PartExact.hpp"
#include "PartApprox.hpp"
#include <cmath>
#include <iostream>



double PartSegment::estimSMax(double vA, double vB, double vN, double vM)
{
  double T_2 = (52. / 2.) * std::log(2) / 2.;
  double l0 = std::max(abs(vB), abs(vM * vB));
  double l1 = std::max(abs(vA), abs(vM * vA + vN * vB));
  double l2 = abs(vN * vA);
  l0 = std::max(static_cast<double>(1.), l0);
  l1 = std::max(static_cast<double>(1.), l1);
  l2 = std::max(static_cast<double>(1.), l2);

  if (std::isnan(l0) || std::isnan(l1) || std::isnan(l2)) {
    return std::numeric_limits<double>::infinity();
  }

  double res = std::numeric_limits<double>::infinity();
  //TODO search a better solution
  if (l0 + l1 + l2 < T_2) { //Smax>1
    res = std::cbrt(T_2 / (l0 + l1 + l2));
  } else { //Smax<1
    res = T_2 / (l0 + l1 + l2);
  }

  if (res < 1.e-8) {
//     LoggerMsg message("Very small smax");
//     message.addChild("smax", std::to_string(res));
//     message.addChild("l0", std::to_string(l0));
//     message.addChild("l1", std::to_string(l1));
//     message.addChild("l2", std::to_string(l2));
//     getLogger()->Write(LoggerLevel::WARNING, "Serie", message);
    res = std::numeric_limits<double>::infinity();
  }

  return res / 2.;
}

void PartSegment::operator()()
{
  PartSubSegmentIn in;
  PartSubSegmentOut out;
  in.G=G;
  in.D=D;
  in.areaDensity=areaDensity;
  in.soffset=0.;
  in.width=width;
  in.anat=anat;
  in.bnat=bnat;
  in.nu=nu;
  in.a=a;
  in.b=b;
  in.m=m;
  in.n=n;
  
  in.R=R;
  in.d1Rda=d1Rda;
  in.d1Rdb=d1Rdb;
  in.d1Rdm=d1Rdm;
  in.d1Rdn=d1Rdn;
  in.d2Rdada=d2Rdada;
  in.d2Rdadb=d2Rdadb;
  in.d2Rdadm=d2Rdadm;
  in.d2Rdadn=d2Rdadn;
  in.d2Rdbda=d2Rdbda;
  in.d2Rdbdb=d2Rdbdb;
  in.d2Rdbdm=d2Rdbdm;
  in.d2Rdbdn=d2Rdbdn;
  in.d2Rdmda=d2Rdmda;
  in.d2Rdmdb=d2Rdmdb;
  in.d2Rdmdm=d2Rdmdm;
  in.d2Rdmdn=d2Rdmdn;
  in.d2Rdnda=d2Rdnda;
  in.d2Rdndb=d2Rdndb;
  in.d2Rdndm=d2Rdndm;
  in.d2Rdndn=d2Rdndn;
  in.d1rda=d1rda;
  in.d1rdb=d1rdb;
  in.d1rdm=d1rdm;
  in.d1rdn=d1rdn;
  in.d2rdada=d2rdada;
  in.d2rdadb=d2rdadb;
  in.d2rdadm=d2rdadm;
  in.d2rdadn=d2rdadn;
  in.d2rdbda=d2rdbda;
  in.d2rdbdb=d2rdbdb;
  in.d2rdbdm=d2rdbdm;
  in.d2rdbdn=d2rdbdn;
  in.d2rdmda=d2rdmda;
  in.d2rdmdb=d2rdmdb;
  in.d2rdmdm=d2rdmdm;
  in.d2rdmdn=d2rdmdn;
  in.d2rdnda=d2rdnda;
  in.d2rdndb=d2rdndb;
  in.d2rdndm=d2rdndm;
  in.d2rdndn=d2rdndn;
  in.r=r;
  
  Ew=0.;
  Eg=0.;
  gradEw.setZero();
  gradEg.setZero();
  hessEw.setZero();
  hessEg.setZero();
  int nbParts=0;
  double localsStart=0.;
  while(localsStart<length)
  {
    double localLength =  estimSMax(a,b+localsStart*a,n,m+localsStart*n);
    nbParts++;
    if(localsStart+localLength>length)
      localLength=length-localsStart;
    in.soffset=localsStart;
    in.length=localLength;
    computeSub(in, out);
    Eg+=out.Eg;
    Ew+=out.Ew;
    std::cout<<"--->Eg="<<out.Eg<<std::endl;
    std::cout<<"--->Ew="<<out.Ew<<std::endl;
    gradEg+=out.gradEg;
    gradEw+=out.gradEw;
    hessEg+=out.hessEg;
    hessEw+=out.hessEw;
    
    in.r=out.endPos;
    in.R=out.endFrame;
    in.d1rda=out.dPosda;
    in.d1Rda=out.dFrameda;
    in.d1rdb=out.dPosdb;
    in.d1Rdb=out.dFramedb;
    in.d1rdn=out.dPosdn;
    in.d1Rdn=out.dFramedn;
    in.d1rdm=out.dPosdm;
    in.d1Rdm=out.dFramedm;
    
    in.d2rdada=out.dPosdada;
    in.d2Rdada=out.dFramedada;
    in.d2rdbda=out.dPosdbda;
    in.d2Rdbda=out.dFramedbda;
    in.d2rdnda=out.dPosdnda;
    in.d2Rdnda=out.dFramednda;
    in.d2rdmda=out.dPosdmda;
    in.d2Rdmda=out.dFramedmda;
    
    in.d2rdadb=out.dPosdadb;
    in.d2Rdadb=out.dFramedadb;
    in.d2rdbdb=out.dPosdbdb;
    in.d2Rdbdb=out.dFramedbdb;
    in.d2rdndb=out.dPosdndb;
    in.d2Rdndb=out.dFramedndb;
    in.d2rdmdb=out.dPosdmdb;
    in.d2Rdmdb=out.dFramedmdb;
    
    in.d2rdadn=out.dPosdadn;
    in.d2Rdadn=out.dFramedadn;
    in.d2rdbdn=out.dPosdbdn;
    in.d2Rdbdn=out.dFramedbdn;
    in.d2rdndn=out.dPosdndn;
    in.d2Rdndn=out.dFramedndn;
    in.d2rdmdn=out.dPosdmdn;
    in.d2Rdmdn=out.dFramedmdn;
    
    in.d2rdadm=out.dPosdadm;
    in.d2Rdadm=out.dFramedadm;
    in.d2rdbdm=out.dPosdbdm;
    in.d2Rdbdm=out.dFramedbdm;
    in.d2rdndm=out.dPosdndm;
    in.d2Rdndm=out.dFramedndm;
    in.d2rdmdm=out.dPosdmdm;
    in.d2Rdmdm=out.dFramedmdm;
    
    
    localsStart += localLength;
  }
  endPos=out.endPos;
  endFrame=out.endFrame;
  
  dPosda=out.dPosda;
  dFrameda=out.dFrameda;
  dPosdb=out.dPosdb;
  dFramedb=out.dFramedb;
  dPosdn=out.dPosdn;
  dFramedn=out.dFramedn;
  dPosdm=out.dPosdm;
  dFramedm=out.dFramedm;
  
  dPosdada=out.dPosdada;
  dFramedada=out.dFramedada;
  dPosdbda=out.dPosdbda;
  dFramedbda=out.dFramedbda;
  dPosdnda=out.dPosdnda;
  dFramednda=out.dFramednda;
  dPosdmda=out.dPosdmda;
  dFramedmda=out.dFramedmda;
  
  dPosdadb=out.dPosdadb;
  dFramedadb=out.dFramedadb;
  dPosdbdb=out.dPosdbdb;
  dFramedbdb=out.dFramedbdb;
  dPosdndb=out.dPosdndb;
  dFramedndb=out.dFramedndb;
  dPosdmdb=out.dPosdmdb;
  dFramedmdb=out.dFramedmdb;
  
  dPosdadn=out.dPosdadn;
  dFramedadn=out.dFramedadn;
  dPosdbdn=out.dPosdbdn;
  dFramedbdn=out.dFramedbdn;
  dPosdndn=out.dPosdndn;
  dFramedndn=out.dFramedndn;
  dPosdmdn=out.dPosdmdn;
  dFramedmdn=out.dFramedmdn;
  
  dPosdadm=out.dPosdadm;
  dFramedadm=out.dFramedadm;
  dPosdbdm=out.dPosdbdm;
  dFramedbdm=out.dFramedbdm;
  dPosdndm=out.dPosdndm;
  dFramedndm=out.dFramedndm;
  dPosdmdm=out.dPosdmdm;
  dFramedmdm=out.dFramedmdm;
}


void PartSegment::computeSub(PartSubSegmentIn& in, PartSubSegmentOut& out)
{
  double x = in.n * in.width / 2.;
  if (abs(x) < 1e-5)
    computeSubT<FunctionPartApprox>(in, out);
  else
    computeSubT<FunctionPartExact>(in,out);
}

template<class T> void PartSegment::computeSubT(PartSubSegmentIn& in, PartSubSegmentOut& out)
{
  T part;
  part.G=in.G;
  part.D=in.D;
  part.areaDensity=in.areaDensity;
  part.soffset=in.soffset;
  part.width=in.width;

 //In arguments
  part.a=in.a;
  part.anat=in.anat;
  part.b=in.b;
  part.bnat=in.bnat;
  part.length=in.length;
  part.m=in.m;
  part.n=in.n;
  part.nu=in.nu;

 //In functions
  part.R=in.R;
  part.d1Rda=in.d1Rda;
  part.d1Rdb=in.d1Rdb;
  part.d1Rdm=in.d1Rdm;
  part.d1Rdn=in.d1Rdn;
  part.d2Rdada=in.d2Rdada;
  part.d2Rdadb=in.d2Rdadb;
  part.d2Rdadm=in.d2Rdadm;
  part.d2Rdadn=in.d2Rdadn;
  part.d2Rdbda=in.d2Rdbda;
  part.d2Rdbdb=in.d2Rdbdb;
  part.d2Rdbdm=in.d2Rdbdm;
  part.d2Rdbdn=in.d2Rdbdn;
  part.d2Rdmda=in.d2Rdmda;
  part.d2Rdmdb=in.d2Rdmdb;
  part.d2Rdmdm=in.d2Rdmdm;
  part.d2Rdmdn=in.d2Rdmdn;
  part.d2Rdnda=in.d2Rdnda;
  part.d2Rdndb=in.d2Rdndb;
  part.d2Rdndm=in.d2Rdndm;
  part.d2Rdndn=in.d2Rdndn;
  part.d1rda=in.d1rda;
  part.d1rdb=in.d1rdb;
  part.d1rdm=in.d1rdm;
  part.d1rdn=in.d1rdn;
  part.d2rdada=in.d2rdada;
  part.d2rdadb=in.d2rdadb;
  part.d2rdadm=in.d2rdadm;
  part.d2rdadn=in.d2rdadn;
  part.d2rdbda=in.d2rdbda;
  part.d2rdbdb=in.d2rdbdb;
  part.d2rdbdm=in.d2rdbdm;
  part.d2rdbdn=in.d2rdbdn;
  part.d2rdmda=in.d2rdmda;
  part.d2rdmdb=in.d2rdmdb;
  part.d2rdmdm=in.d2rdmdm;
  part.d2rdmdn=in.d2rdmdn;
  part.d2rdnda=in.d2rdnda;
  part.d2rdndb=in.d2rdndb;
  part.d2rdndm=in.d2rdndm;
  part.d2rdndn=in.d2rdndn;
  part.r=in.r;
  
  std::cout<<"+Part "<<std::endl;
  std::cout<<"+pt=("<<part.a<<','<<part.b<<','<<part.n<<','<<part.m<<')'<<std::endl;
  std::cout<<"+length="<<part.length<<std::endl;
  std::cout<<"+width="<<part.width<<std::endl;
  std::cout<<"+D="<<part.D<<std::endl; 
  std::cout<<"+ad="<<part.areaDensity<<std::endl;
  std::cout<<"+G="<<part.G.transpose()<<std::endl;
  
  part();
  
  std::cout<<"++Eg="<<part.Eg<<std::endl;
  std::cout<<"++Ew="<<part.Ew<<std::endl;
//   std::cout<<"++P="<<part.P<<std::endl;
//   std::cout<<"++H="<<part.H<<std::endl;
//   std::cout<<"++K="<<part.K<<std::endl;
  std::cout<<"++endPos="<<part.endPos.transpose()<<std::endl;
  
  out.dFrameda=part.dFrameda;
  out.dFramedada=part.dFramedada;
  out.dFramedadb=part.dFramedadb;
  out.dFramedadm=part.dFramedadm;
  out.dFramedadn=part.dFramedadn;
  out.dFramedb=part.dFramedb;
  out.dFramedbda=part.dFramedbda;
  out.dFramedbdb=part.dFramedbdb;
  out.dFramedbdm=part.dFramedbdm;
  out.dFramedbdn=part.dFramedbdn;
  out.dFramedm=part.dFramedm;
  out.dFramedmda=part.dFramedmda;
  out.dFramedmdb=part.dFramedmdb;
  out.dFramedmdm=part.dFramedmdm;
  out.dFramedmdn=part.dFramedmdn;
  out.dFramedn=part.dFramedn;
  out.dFramednda=part.dFramednda;
  out.dFramedndb=part.dFramedndb;
  out.dFramedndm=part.dFramedndm;
  out.dFramedndn=part.dFramedndn;
  out.endFrame=part.endFrame;
  out.hessEg=part.hessEg;
  out.hessEw=part.hessEw;
  out.dPosda=part.dPosda;
  out.dPosdada=part.dPosdada;
  out.dPosdadb=part.dPosdadb;
  out.dPosdadm=part.dPosdadm;
  out.dPosdadn=part.dPosdadn;
  out.dPosdb=part.dPosdb;
  out.dPosdbda=part.dPosdbda;
  out.dPosdbdb=part.dPosdbdb;
  out.dPosdbdm=part.dPosdbdm;
  out.dPosdbdn=part.dPosdbdn;
  out.dPosdm=part.dPosdm;
  out.dPosdmda=part.dPosdmda;
  out.dPosdmdb=part.dPosdmdb;
  out.dPosdmdm=part.dPosdmdm;
  out.dPosdmdn=part.dPosdmdn;
  out.dPosdn=part.dPosdn;
  out.dPosdnda=part.dPosdnda;
  out.dPosdndb=part.dPosdndb;
  out.dPosdndm=part.dPosdndm;
  out.dPosdndn=part.dPosdndn;
  out.endPos=part.endPos;
  out.gradEg=part.gradEg;
  out.gradEw=part.gradEw;
  out.Eg=part.Eg;
  out.Ew=part.Ew;
}

template void PartSegment::computeSubT<FunctionPartApprox>(PartSubSegmentIn& in, PartSubSegmentOut& out);
template void PartSegment::computeSubT<FunctionPartExact>(PartSubSegmentIn& in, PartSubSegmentOut& out);
