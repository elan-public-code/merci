/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "RibbonFunction.hpp"
#include <iostream>


void RibbonFunction::operator()()
{
  PartSegment part;
  part.G=G;
  part.D=D;
  part.areaDensity=areaDensity;
  part.width=width;
  part.nu=nu;
  part.r=r;
  part.R=R;
  part.d1Rda=d1Rda;
  part.d1Rdb=d1Rdb;
  part.d1Rdm=d1Rdm;
  part.d1Rdn=d1Rdn;
  part.d2Rdada=d2Rdada;
  part.d2Rdadb=d2Rdadb;
  part.d2Rdadm=d2Rdadm;
  part.d2Rdadn=d2Rdadn;
  part.d2Rdbda=d2Rdbda;
  part.d2Rdbdb=d2Rdbdb;
  part.d2Rdbdm=d2Rdbdm;
  part.d2Rdbdn=d2Rdbdn;
  part.d2Rdmda=d2Rdmda;
  part.d2Rdmdb=d2Rdmdb;
  part.d2Rdmdm=d2Rdmdm;
  part.d2Rdmdn=d2Rdmdn;
  part.d2Rdnda=d2Rdnda;
  part.d2Rdndb=d2Rdndb;
  part.d2Rdndm=d2Rdndm;
  part.d2Rdndn=d2Rdndn;
  part.d1rda=d1rda;
  part.d1rdb=d1rdb;
  part.d1rdm=d1rdm;
  part.d1rdn=d1rdn;
  part.d2rdada=d2rdada;
  part.d2rdadb=d2rdadb;
  part.d2rdadm=d2rdadm;
  part.d2rdadn=d2rdadn;
  part.d2rdbda=d2rdbda;
  part.d2rdbdb=d2rdbdb;
  part.d2rdbdm=d2rdbdm;
  part.d2rdbdn=d2rdbdn;
  part.d2rdmda=d2rdmda;
  part.d2rdmdb=d2rdmdb;
  part.d2rdmdm=d2rdmdm;
  part.d2rdmdn=d2rdmdn;
  part.d2rdnda=d2rdnda;
  part.d2rdndb=d2rdndb;
  part.d2rdndm=d2rdndm;
  part.d2rdndn=d2rdndn;
  
  int syssize=(a.size()+1)*2;
  if(a.size()!=n.size())throw std::logic_error("System inconsistant");
  if(a.size()!=anat.size())throw std::logic_error("System inconsistant");
  if(a.size()!=length.size())throw std::logic_error("System inconsistant");
  Eg=0;Ew=0.;
  gradEg=Eigen::VectorXd(syssize);
  gradEg.setZero();
  gradEw=gradEg;
  hessEg=Eigen::MatrixXd(syssize,syssize);
  hessEg.setZero();
  hessEw=hessEg;
  std::cout<<"Pieces: "<<a.size()<<std::endl;
  for(int i=0;i<a.size();i++)
    handlePart(part,i);
  endPos=part.endPos;
  endFrame=part.endFrame;
}

void RibbonFunction::handlePart(PartSegment& part, int segID)
{
  part.a=a[segID];
  part.n=n[segID];
  part.anat=anat[segID];
  part.length=length[segID];
  
  part.b=b;part.bnat=bnat;part.m=m;
  for(int i=0;i<segID;i++)
  {
    part.b+=a[i]*length[i];
    part.bnat+=anat[i]*length[i];
    part.m+=n[i]*length[i];
  }
  
  std::cout<<"Segment "<<segID<<std::endl;
  std::cout<<"pt=("<<part.a<<','<<part.b<<','<<part.n<<','<<part.m<<')'<<std::endl;
  std::cout<<"length="<<part.length<<std::endl;
  std::cout<<"width="<<part.width<<std::endl;
  std::cout<<"D="<<part.D<<std::endl; 
  std::cout<<"ad="<<part.areaDensity<<std::endl;
  std::cout<<"G="<<part.G.transpose()<<std::endl;
  
  part();
  Eg+=part.Eg;
  Ew+=part.Ew;
  addGradient(gradEg, part.gradEg, segID);
  addGradient(gradEw, part.gradEw, segID);
  
  addHessian(hessEg, part.hessEg, segID);
  addHessian(hessEw, part.hessEw, segID);
  
  part.r=part.endPos;
  part.R=part.endFrame;
  
  //part.d1Rda=part.dFrameda;
  part.d1Rda.setZero();
  part.d1Rdb=part.dFramedb;
  //part.d1Rdb.setZero();
  //part.d1Rdn=part.dFramedn;
  part.d1Rdn.setZero();
  part.d1Rdm=part.dFramedm;
  //part.d1Rdm.setZero();
  part.d2Rdada=part.dFramedada;
  part.d2Rdadb=part.dFramedadb;
  part.d2Rdadn=part.dFramedadn;
  part.d2Rdadm=part.dFramedadm;
  part.d2Rdbda=part.dFramedbda;
  part.d2Rdbdb=part.dFramedbdb;
  part.d2Rdbdn=part.dFramedbdn;
  part.d2Rdbdm=part.dFramedbdm;
  part.d2Rdnda=part.dFramednda;
  part.d2Rdndb=part.dFramedndb;
  part.d2Rdndn=part.dFramedndn;
  part.d2Rdndm=part.dFramedndm;
  part.d2Rdmda=part.dFramedmda;
  part.d2Rdmdb=part.dFramedmdb;
  part.d2Rdmdn=part.dFramedmdn;
  part.d2Rdmdm=part.dFramedmdm;
//   part.d1rda=part.dPosda;
  part.d1rdb=part.dPosdb;
//   part.d1rdn=part.dPosdn;
  part.d1rdm=part.dPosdm;
  part.d1rda.setZero();
//  part.d1rdb.setZero();
  part.d1rdn.setZero();
//  part.d1rdm.setZero();
  part.d2rdada=part.dPosdada;
  part.d2rdadb=part.dPosdadb;
  part.d2rdadn=part.dPosdadn;
  part.d2rdadm=part.dPosdadm;
  part.d2rdbda=part.dPosdbda;
  part.d2rdbdb=part.dPosdbdb;
  part.d2rdbdn=part.dPosdbdn;
  part.d2rdbdm=part.dPosdbdm;
  part.d2rdnda=part.dPosdnda;
  part.d2rdndb=part.dPosdndb;
  part.d2rdndn=part.dPosdndn;
  part.d2rdndm=part.dPosdndm;
  part.d2rdmda=part.dPosdmda;
  part.d2rdmdb=part.dPosdmdb;
  part.d2rdmdn=part.dPosdmdn;
  part.d2rdmdm=part.dPosdmdm;
  
}

void RibbonFunction::addGradient(Eigen::VectorXd& grad, const Eigen::Vector4d& base, int segID)
{
  grad[2+segID*2]+=base[0];
  grad[2+segID*2+1]+=base[2];
  grad[0]+=base[1];
  grad[1]+=base[3];
  for(int i=0;i<segID;i++)
  {
    grad[2+i*2]+=length[i]*base[1];
    grad[2+i*2+1]+=length[i]*base[3];
  }
}


void RibbonFunction::addHessian(Eigen::MatrixXd& hess, const Eigen::Matrix4d& base, int segID)
{
  hess(2+segID*2,2+segID*2)+=base(0,0);
  hess(2+segID*2+1,2+segID*2)+=base(2,0);
  hess(2+segID*2,2+segID*2+1)+=base(0,2);
  hess(2+segID*2+1,2+segID*2+1)+=base(2,2);
  
  hess(0,0)+=base(1,1);
  hess(0,1)+=base(1,3);
  hess(1,0)+=base(3,1);
  hess(1,1)+=base(3,3);
  
  hess(0,2+segID*2)+=base(1,0);
  hess(1,2+segID*2)+=base(3,0);
  hess(0,2+segID*2+1)+=base(1,2);
  hess(1,2+segID*2+1)+=base(3,2);
  
  hess(2+segID*2,0)+=base(0,1);
  hess(2+segID*2+1,0)+=base(2,1);
  hess(2+segID*2,1)+=base(0,3);
  hess(2+segID*2+1,1)+=base(2,3);
  
  for(int i=0;i<segID;i++)
  {
    hess(2+i*2,2+segID*2)+=length[i]*base(1,0);
    hess(2+i*2,2+segID*2+1)+=length[i]*base(1,2);
    hess(2+i*2+1,2+segID*2)+=length[i]*base(3,0);
    hess(2+i*2+1,2+segID*2+1)+=length[i]*base(3,2);
    
    hess(2+segID*2,2+i*2)+=length[i]*base(0,1);
    hess(2+segID*2+1,2+i*2)+=length[i]*base(2,1);
    hess(2+segID*2,2+i*2+1)+=length[i]*base(0,3);
    hess(2+segID*2+1,2+i*2+1)+=length[i]*base(2,3);
    
    hess(2+i*2,0)+=length[i]*base(1,1);
    hess(2+i*2,1)+=length[i]*base(1,3);
    hess(2+i*2+1,0)+=length[i]*base(3,1);
    hess(2+i*2+1,1)+=length[i]*base(3,3);
    
    hess(0,2+i*2)+=length[i]*base(1,1);
    hess(1,2+i*2)+=length[i]*base(3,1);
    hess(0,2+i*2+1)+=length[i]*base(1,3);
    hess(1,2+i*2+1)+=length[i]*base(3,3);
  }
  
  for(int i=0;i<segID;i++)
  {
    for(int j=0;j<segID;j++)
    {
      hess(2+i*2,2+j*2)+=length[i]*length[j]*base(1,1);
      hess(2+i*2,2+j*2+1)+=length[i]*length[j]*base(1,3);
      hess(2+i*2+1,2+j*2)+=length[i]*length[j]*base(3,1);
      hess(2+i*2+1,2+j*2+1)+=length[i]*length[j]*base(3,3);
    }
  }
}

