/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef RIBBONFUNCTION_H
#define RIBBONFUNCTION_H

#include <Eigen/Dense>
#include <vector>
#include "PartSegment.hpp"

class RibbonFunction
{
public:
  //Parameters
  Eigen::Vector3d G;
  double D;
  double areaDensity;
  double width;
  double nu;

 //In arguments
  std::vector<double> a;
  std::vector<double> anat;
  double b;
  double bnat;
  std::vector<double> length;
  std::vector<double> n;
  double m;

 //In functions
  Eigen::Matrix3d R;
  Eigen::Matrix3d d1Rda;
  Eigen::Matrix3d d1Rdb;
  Eigen::Matrix3d d1Rdm;
  Eigen::Matrix3d d1Rdn;
  Eigen::Matrix3d d2Rdada;
  Eigen::Matrix3d d2Rdadb;
  Eigen::Matrix3d d2Rdadm;
  Eigen::Matrix3d d2Rdadn;
  Eigen::Matrix3d d2Rdbda;
  Eigen::Matrix3d d2Rdbdb;
  Eigen::Matrix3d d2Rdbdm;
  Eigen::Matrix3d d2Rdbdn;
  Eigen::Matrix3d d2Rdmda;
  Eigen::Matrix3d d2Rdmdb;
  Eigen::Matrix3d d2Rdmdm;
  Eigen::Matrix3d d2Rdmdn;
  Eigen::Matrix3d d2Rdnda;
  Eigen::Matrix3d d2Rdndb;
  Eigen::Matrix3d d2Rdndm;
  Eigen::Matrix3d d2Rdndn;
  Eigen::Vector3d d1rda;
  Eigen::Vector3d d1rdb;
  Eigen::Vector3d d1rdm;
  Eigen::Vector3d d1rdn;
  Eigen::Vector3d d2rdada;
  Eigen::Vector3d d2rdadb;
  Eigen::Vector3d d2rdadm;
  Eigen::Vector3d d2rdadn;
  Eigen::Vector3d d2rdbda;
  Eigen::Vector3d d2rdbdb;
  Eigen::Vector3d d2rdbdm;
  Eigen::Vector3d d2rdbdn;
  Eigen::Vector3d d2rdmda;
  Eigen::Vector3d d2rdmdb;
  Eigen::Vector3d d2rdmdm;
  Eigen::Vector3d d2rdmdn;
  Eigen::Vector3d d2rdnda;
  Eigen::Vector3d d2rdndb;
  Eigen::Vector3d d2rdndm;
  Eigen::Vector3d d2rdndn;
  Eigen::Vector3d r;
  
  void operator()();
  
  //Out arguments
  double Eg;
  double Ew;
  Eigen::VectorXd gradEg;
  Eigen::VectorXd gradEw;
  Eigen::MatrixXd hessEg;
  Eigen::MatrixXd hessEw;
  
  Eigen::Vector3d endPos;
  Eigen::Matrix3d endFrame;
private:
  void handlePart(PartSegment& part, int segID);
  void addHessian(Eigen::MatrixXd& hess, const Eigen::Matrix4d& base, int segID);
  void addGradient(Eigen::VectorXd& grad, const Eigen::Vector4d& base, int segID);
};

#endif // RIBBONFUNCTION_H
