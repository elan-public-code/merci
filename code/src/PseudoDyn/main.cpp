/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "RibbonFunction.hpp"
#include "PartApprox.hpp"
#include <SuperRibbon.hpp>
#include <RibbonSystem.hpp>

void fillZeroM(RibbonFunction& rf)
{
  rf.d1Rda.setZero();
  rf.d1Rdb.setZero();
  rf.d1Rdm.setZero();
  rf.d1Rdn.setZero();
  rf.d2Rdada.setZero();
  rf.d2Rdadb.setZero();
  rf.d2Rdadm.setZero();
  rf.d2Rdadn.setZero();
  rf.d2Rdbda.setZero();
  rf.d2Rdbdb.setZero();
  rf.d2Rdbdm.setZero();
  rf.d2Rdbdn.setZero();
  rf.d2Rdmda.setZero();
  rf.d2Rdmdb.setZero();
  rf.d2Rdmdm.setZero();
  rf.d2Rdmdn.setZero();
  rf.d2Rdnda.setZero();
  rf.d2Rdndb.setZero();
  rf.d2Rdndm.setZero();
  rf.d2Rdndn.setZero();
  rf.d1rda.setZero();
  rf.d1rdb.setZero();
  rf.d1rdm.setZero();
  rf.d1rdn.setZero();
  rf.d2rdada.setZero();
  rf.d2rdadb.setZero();
  rf.d2rdadm.setZero();
  rf.d2rdadn.setZero();
  rf.d2rdbda.setZero();
  rf.d2rdbdb.setZero();
  rf.d2rdbdm.setZero();
  rf.d2rdbdn.setZero();
  rf.d2rdmda.setZero();
  rf.d2rdmdb.setZero();
  rf.d2rdmdm.setZero();
  rf.d2rdmdn.setZero();
  rf.d2rdnda.setZero();
  rf.d2rdndb.setZero();
  rf.d2rdndm.setZero();
  rf.d2rdndn.setZero();
}

void TestNew()
{
  RibbonFunction rf;
  
  rf.G={0.,9.81,0.};
  rf.D=1;
  rf.areaDensity=10;
  rf.width=0.01;
  rf.nu=0.5;
  
  rf.a={0.1};
  rf.b=-0.1;
  rf.n={-0.05};
  rf.m=0.5;
  rf.length={0.1};
  rf.anat={0.};
  rf.bnat=0.;
  
  rf.R.setIdentity();
  rf.r<<0.,0.,0.;
  fillZeroM(rf);
  rf();
  
  std::cout<<"E = "<<(rf.Ew+rf.Eg)<<std::endl;
  std::cout<<"G = "<<(rf.gradEw+rf.gradEg).transpose()<<std::endl;
  std::cout<<"H = "<<(rf.hessEw+rf.hessEg)<<std::endl;
  std::cout<<"endPos = "<<rf.endPos.transpose()<<std::endl;
}

void TestNew2()
{
  RibbonFunction rf;
  
  rf.G={0.,9.81,0.};
  rf.D=1;
  rf.areaDensity=10;
  rf.width=0.01;
  rf.nu=0.5;
  
  rf.a={0.1,0.2};
  rf.b=-0.1;
  rf.n={-0.05,0.01};
  rf.m=0.5;
  rf.length={0.1,0.1};
  rf.anat={0.,0.};
  rf.bnat=0.;
  
  rf.R.setIdentity();
  rf.r<<0.,0.,0.;
  fillZeroM(rf);
  rf();
  
  std::cout<<"E = "<<(rf.Ew+rf.Eg)<<std::endl;
  std::cout<<"G = "<<(rf.gradEw+rf.gradEg).transpose()<<std::endl;
  std::cout<<"H = "<<(rf.hessEw+rf.hessEg)<<std::endl;
  std::cout<<"endPos = "<<rf.endPos.transpose()<<std::endl;
}

void TestOld()
{
  SuperRibbon sr;
  sr.setD(1);
  sr.setAreaDensity(10);
  sr.setWidth(0.01);
  sr.setPoissonRatio(0.5);
  sr.addSegmentAt(0);
  sr.setOmega1A(0,0.1);
  sr.setOmega1BOr(-0.1);
  sr.setEtaN(0,-0.05);
  sr.setEtaMOr(0.5);
  sr.setLength(0,0.1);
  sr.setNatOmega1(0,LinearScalar(0.,0.));
  
  RibbonSystem sys(Eigen::Vector3d(0.,-9.81,0.));
  sys.set(sr);
  std::cout<<"E = "<<sys.energy()<<std::endl;
  std::cout<<"G = "<<sys.gradient().transpose()<<std::endl;
  std::cout<<"H = "<<sys.hessian()<<std::endl;
  std::cout<<"endPos = "<<sys.getEndPos().transpose()<<std::endl;
}

void TestOld2()
{
  SuperRibbon sr;
  sr.setD(1);
  sr.setAreaDensity(10);
  sr.setWidth(0.01);
  sr.setPoissonRatio(0.5);
  sr.addSegmentAt(0);
  sr.addSegmentAt(1);
  sr.setOmega1A(0,0.1);
  sr.setOmega1A(1,0.2);
  sr.setOmega1BOr(-0.1);
  sr.setEtaN(0,-0.05);
  sr.setEtaN(1,0.01);
  sr.setEtaMOr(0.5);
  sr.setLength(0,0.1);
  sr.setLength(1,0.1);
  sr.setNatOmega1(0,LinearScalar(0.,0.));
  sr.setNatOmega1(1,LinearScalar(0.,0.));
  
  RibbonSystem sys(Eigen::Vector3d(0.,-9.81,0.));
  sys.set(sr);
  std::cout<<"E = "<<sys.energy()<<std::endl;
  std::cout<<"G = "<<sys.gradient().transpose()<<std::endl;
  std::cout<<"H = "<<sys.hessian()<<std::endl;
  std::cout<<"endPos = "<<sys.getEndPos().transpose()<<std::endl;
}

int main()
{
  TestNew2();
  std::cout<<"======================================"<<std::endl;
  TestOld2();
  
  
  return 0;
}

