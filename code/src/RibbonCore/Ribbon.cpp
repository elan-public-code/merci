/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "Ribbon.hpp"

Ribbon::Ribbon()
{
  posOr.setZero();
  frameOr.setIdentity();
}

void Ribbon::setPosOr(Eigen::Vector3d pos)
{
  posOr = pos;
}

void Ribbon::setFrameOr(Eigen::Matrix3d frame)
{
  frameOr = frame;
}

Eigen::Vector3d Ribbon::getPosOr()
{
  return posOr;
}

Eigen::Matrix3d Ribbon::getFrameOr()
{
  return frameOr;
}

size_t Ribbon::getNbSegments()
{
  return segments.size();
}

void Ribbon::setEta(int segment, double etaN)
{
  segments.at(segment).eta = etaN;
}

void Ribbon::setLength(int segment, double l)
{
  segments.at(segment).length = l;
}

void Ribbon::setOmega1(int segment, double omega1A)
{
  segments.at(segment).omega1 = omega1A;
}

void Ribbon::setNatOmega1(int segment, std::optional<double> natomega1)
{
  segments.at(segment).natOmega1 = natomega1;
}

double Ribbon::getLength(int segment)
{
  return segments.at(segment).length;
}

double Ribbon::getOmega1(int segment)
{
  return segments.at(segment).omega1;
}

std::optional<double> Ribbon::getNatOmega1(int segment)
{
  return segments.at(segment).natOmega1;
}


double Ribbon::getEta(int segment)
{
  return segments.at(segment).eta;
}

void Ribbon::addSegmentAt(int idx)
{
  segments.insert(segments.begin() + idx, RibbonSegment());
}

void Ribbon::deleteSegment(int idx)
{
  segments.erase(segments.begin() + idx);
}

double Ribbon::getD()
{
  return D;
}

std::optional<double> Ribbon::getPoissonRatio()
{
  return poissonRatio;
}

double Ribbon::getAreaDensity()
{
  return areaDensity;
}

double Ribbon::getWidth()
{
  return width;
}

void Ribbon::setD(double d)
{
  D = d;
}

void Ribbon::setPoissonRatio(std::optional<double> nu)
{
  poissonRatio = nu;
}

void Ribbon::setAreaDensity(double ad)
{
  areaDensity = ad;
}

void Ribbon::setWidth(double w)
{
  width = w;
}
