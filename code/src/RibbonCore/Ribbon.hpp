/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef RIBBON_H
#define RIBBON_H

#include <Eigen/Dense>
#include <vector>
#include <optional>

class Ribbon
{
public:
  Ribbon();
  void deleteSegment(int idx);
  void addSegmentAt(int idx);
  void setOmega1(int segment, double omega1A);
  
  void setNatOmega1(int segment, std::optional<double> natomega1);
  void setEta(int segment, double etaN);
  void setLength(int segment, double l);
  void setPosOr(Eigen::Vector3d pos);
  void setFrameOr(Eigen::Matrix3d frame);
  void setWidth(double w);
  void setAreaDensity(double ad);
  void setD(double d);
  void setPoissonRatio(std::optional<double> nu);

  double getOmega1(int segment);
  std::optional<double> getNatOmega1(int segment);
  double getEta(int segment);
  double getLength(int segment);

  size_t getNbSegments();
  Eigen::Vector3d getPosOr();
  Eigen::Matrix3d getFrameOr();
  double getWidth();
  double getAreaDensity();
  double getD();
  std::optional<double> getPoissonRatio();
private:
  struct RibbonSegment {
    double eta =0.;
    double omega1=0.;
    std::optional<double> natOmega1;
    double length = 0.;
  };
  Eigen::Vector3d posOr;
  Eigen::Matrix3d frameOr;
  double width = 0.01;
  double D = 1.e-4;
  std::optional<double> poissonRatio = std::nullopt; 
  double areaDensity = 0.1;
  std::vector<RibbonSegment> segments;
};

#endif // RIBBON_H
