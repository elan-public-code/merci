/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "RibbonGeometrie.hpp"
#include "Log/Logger.hpp"

void RibbonGeometrie::Place(Eigen::Vector3d pos, Eigen::Matrix3d frame)
{
    posOr = pos;
    frameOr = frame;
}

void RibbonGeometrie::Setup(double omega1, double eta)
{
    if (false) {
        LoggerMsg message("Setup");
        message.addChild("omega1", std::to_string(omega1));
        message.addChild("eta", std::to_string(eta));
        getLogger()->Write(LoggerLevel::INFORMATION, "Serie", message);
    }
    this->omega1 = omega1;
    this->eta = eta;
}

bool RibbonGeometrie::testLittle(double l) const
{
    return (l*sqrt(eta*eta*omega1*omega1+omega1*omega1)<1e-16);
}

std::pair<Eigen::Vector3d, Eigen::Matrix3d> RibbonGeometrie::PosFrame(double l) const
{
    Eigen::Matrix3d frame;
    Eigen::Vector3d pos;
    if(testLittle(l))
    {
        frame(0,0) = frameOr(0,0);
        frame(0,1) = frameOr(0,1);
        frame(0,2) = frameOr(0,2);
        pos(0) = frameOr(0,2)*l+posOr(0);
        frame(1,0) = frameOr(1,0);
        frame(1,1) = frameOr(1,1);
        frame(1,2) = frameOr(1,2);
        pos(1) = frameOr(1,2)*l+posOr(1);
        frame(2,0) = frameOr(2,0);
        frame(2,1) = frameOr(2,1);
        frame(2,2) = frameOr(2,2);
        pos(2) = frameOr(2,2)*l+posOr(2);
    }
    else
    {
        const double t1 = eta*eta;
        const double t2 = omega1*omega1;
        const double t3 = t1*t2;
        const double t4 = t3+t2;
        const double t5 = sqrt(t4);
        const double t6 = l*t5;
        const double t7 = cos(t6);
        const double t8 = 1.0-t7;
        const double t9 = 1/t4;
        const double t10 = t8*t9;
        const double t12 = -t10*t3+1.0;
        const double t14 = sin(t6);
        const double t15 = frameOr(0,1)*t14;
        const double t16 = 1/t5;
        const double t18 = t16*eta*omega1;
        const double t22 = t9*eta*t2;
        const double t27 = l*l;
        const double t36 = t8/t27*t9*(-t27*t1*t2-t27*t2)+1.0;
        const double t39 = t16*omega1;
        const double t46 = -t10*t2+1.0;
        const double t52 = 1.0-t14/l*t16;
        const double t56 = eta*t2*l;
        const double t59 = t9*omega1;
        const double t63 = -t52*t9*t2+1.0;
        const double t68 = frameOr(1,1)*t14;
        const double t93 = frameOr(2,1)*t14;
        frame(0,0) = frameOr(0,2)*t8*t22+frameOr(0,0)*t12+t15*t18;
        frame(0,1) = -frameOr(0,0)*t14*t18+frameOr(0,2)*t14*t39+frameOr(0,1)*t36;
        frame(0,2) = frameOr(0,0)*t8*t22-t15*t39+frameOr(0,2)*t46;
        pos(0) = frameOr(0,0)*t52*t9*t56+frameOr(0,2)*t63*l-frameOr(0,1)*t8*t59+posOr(0);
        frame(1,0) = frameOr(1,2)*t8*t22+frameOr(1,0)*t12+t68*t18;
        frame(1,1) = -frameOr(1,0)*t14*t18+frameOr(1,2)*t14*t39+frameOr(1,1)*t36;
        frame(1,2) = frameOr(1,0)*t8*t22-t68*t39+frameOr(1,2)*t46;
        pos(1) = frameOr(1,0)*t52*t9*t56+frameOr(1,2)*t63*l-frameOr(1,1)*t8*t59+posOr(1);
        frame(2,0) = frameOr(2,2)*t8*t22+frameOr(2,0)*t12+t93*t18;
        frame(2,1) = -frameOr(2,0)*t14*t18+frameOr(2,2)*t14*t39+frameOr(2,1)*t36;
        frame(2,2) = frameOr(2,0)*t8*t22-t93*t39+frameOr(2,2)*t46;
        pos(2) = frameOr(2,0)*t52*t9*t56+frameOr(2,2)*t63*l-frameOr(2,1)*t8*t59+posOr(2);
    }
    return std::pair<Eigen::Vector3d, Eigen::Matrix3d>(pos,frame);
}

std::pair<Eigen::Vector3d, Eigen::Matrix3d> RibbonGeometrie::dPosFramedOmega1(double l) const
{
    Eigen::Matrix3d frame;
    Eigen::Vector3d pos;
    if(testLittle(l))
    {
        frame.setZero();
        pos.setZero();
    }
    else
    {
        const double t1 = eta*eta;
        const double t2 = omega1*omega1;
        const double t3 = t1*t2;
        const double t4 = t3+t2;
        const double t5 = sqrt(t4);
        const double t7 = 1/t5/t4;
        const double t8 = l*t7;
        const double t9 = t1*omega1;
        const double t10 = t9+omega1;
        const double t12 = l*t5;
        const double t13 = sin(t12);
        const double t18 = cos(t12);
        const double t19 = 1.0-t18;
        const double t20 = t4*t4;
        const double t21 = 1/t20;
        const double t22 = t19*t21;
        const double t25 = 1/t4;
        const double t26 = t19*t25;
        const double t29 = -t8*t10*t13*t1*t2+2.0*t22*t10*t3-2.0*t26*t9;
        const double t31 = frameOr(0,1)*l;
        const double t32 = t31*t25;
        const double t33 = 2.0*t10*t18;
        const double t34 = eta*omega1;
        const double t35 = t33*t34;
        const double t38 = frameOr(0,1)*t13;
        const double t40 = 2.0*t34*t10;
        const double t43 = 1/t5;
        const double t44 = t43*eta;
        const double t46 = frameOr(0,2)*l;
        const double t48 = 2.0*t10*t13;
        const double t49 = eta*t2;
        const double t50 = t48*t49;
        const double t53 = frameOr(0,2)*t19;
        const double t55 = 2.0*t49*t10;
        const double t58 = t25*eta*omega1;
        const double t62 = frameOr(0,0)*l;
        const double t66 = frameOr(0,0)*t13;
        const double t71 = 1/l;
        const double t73 = l*l;
        const double t74 = t73*t1;
        const double t77 = -t73*t2-t74*t2;
        const double t82 = t19/t73;
        const double t91 = t71*t7*t48*t77/2.0-2.0*t82*t21*t77*t10+2.0*t82*t25*(-t73*omega1-t74*omega1);
        const double t94 = t33*omega1;
        const double t97 = frameOr(0,2)*t13;
        const double t99 = 2.0*t7*omega1*t10;
        const double t107 = frameOr(0,0)*t19;
        const double t120 = 2.0*t10*t2;
        const double t124 = -t8*t48*t2/2.0+t22*t120-2.0*t26*omega1;
        const double t129 = t13*t71;
        const double t132 = 2.0*t129*t7*t10-2.0*t25*t10*t18;
        const double t135 = t49*l;
        const double t138 = -t129*t43+1.0;
        const double t139 = frameOr(0,0)*t138;
        const double t142 = 2.0*t49*l*t10;
        const double t145 = t34*l;
        const double t149 = t48*omega1;
        const double t152 = frameOr(0,1)*t19;
        const double t154 = 2.0*t21*omega1*t10;
        const double t164 = -t132*t25*t2/2.0+t138*t21*t120-2.0*t138*t25*omega1;
        const double t169 = frameOr(1,1)*l;
        const double t170 = t169*t25;
        const double t173 = frameOr(1,1)*t13;
        const double t178 = frameOr(1,2)*l;
        const double t182 = frameOr(1,2)*t19;
        const double t188 = frameOr(1,0)*l;
        const double t192 = frameOr(1,0)*t13;
        const double t201 = frameOr(1,2)*t13;
        const double t209 = frameOr(1,0)*t19;
        const double t224 = frameOr(1,0)*t138;
        const double t233 = frameOr(1,1)*t19;
        const double t240 = frameOr(2,1)*l;
        const double t241 = t240*t25;
        const double t244 = frameOr(2,1)*t13;
        const double t249 = frameOr(2,2)*l;
        const double t253 = frameOr(2,2)*t19;
        const double t259 = frameOr(2,0)*l;
        const double t263 = frameOr(2,0)*t13;
        const double t272 = frameOr(2,2)*t13;
        const double t280 = frameOr(2,0)*t19;
        const double t295 = frameOr(2,0)*t138;
        const double t304 = frameOr(2,1)*t19;
        frame(0,0) = frameOr(0,0)*t29+t32*t35/2.0-t38*t7*t40/2.0+t38*t44+t46*t7*t50/2.0-t53*t21*t55+2.0*t53*t58;
        frame(0,1) = -t62*t25*t35/2.0+t66*t7*t40/2.0-t66*t44+frameOr(0,1)*t91+t46*t25*t94/2.0-t97*t99/2.0+t97*t43;
        frame(0,2) = t62*t7*t50/2.0-t107*t21*t55+2.0*t107*t58-t32*t94/2.0+t38*t99/2.0-t38*t43+frameOr(0,2)*t124;
        pos(0) = frameOr(0,0)*t132*t25*t135/2.0-t139*t21*t142+2.0*t139*t25*t145-t31*t7*t149/2.0+t152*t154-t152*t25+frameOr(0,2)*t164*l;
        frame(1,0) = frameOr(1,0)*t29+t170*t35/2.0-t173*t7*t40/2.0+t173*t44+t178*t7*t50/2.0-t182*t21*t55+2.0*t182*t58;
        frame(1,1) = -t188*t25*t35/2.0+t192*t7*t40/2.0-t192*t44+frameOr(1,1)*t91+t178*t25*t94/2.0-t201*t99/2.0+t201*t43;
        frame(1,2) = t188*t7*t50/2.0-t209*t21*t55+2.0*t209*t58-t170*t94/2.0+t173*t99/2.0-t173*t43+frameOr(1,2)*t124;
        pos(1) = frameOr(1,0)*t132*t25*t135/2.0-t224*t21*t142+2.0*t224*t25*t145-t169*t7*t149/2.0+t233*t154-t233*t25+frameOr(1,2)*t164*l;
        frame(2,0) = frameOr(2,0)*t29+t241*t35/2.0-t244*t7*t40/2.0+t244*t44+t249*t7*t50/2.0-t253*t21*t55+2.0*t253*t58;
        frame(2,1) = -t259*t25*t35/2.0+t263*t7*t40/2.0-t263*t44+frameOr(2,1)*t91+t249*t25*t94/2.0-t272*t99/2.0+t272*t43;
        frame(2,2) = t259*t7*t50/2.0-t280*t21*t55+2.0*t280*t58-t241*t94/2.0+t244*t99/2.0-t244*t43+frameOr(2,2)*t124;
        pos(2) = frameOr(2,0)*t132*t25*t135/2.0-t295*t21*t142+2.0*t295*t25*t145-t240*t7*t149/2.0+t304*t154-t304*t25+frameOr(2,2)*t164*l;

    }
    return std::pair<Eigen::Vector3d, Eigen::Matrix3d>(pos,frame);
}

std::pair<Eigen::Vector3d, Eigen::Matrix3d> RibbonGeometrie::dPosFramedEta(double l) const
{
    Eigen::Matrix3d frame;
    Eigen::Vector3d pos;
    if(testLittle(l))
    {
        frame.setZero();
        pos.setZero();
    }
    else
    {
        const double t1 = eta*eta;
        const double t2 = omega1*omega1;
        const double t4 = t1*t2+t2;
        const double t5 = sqrt(t4);
        const double t7 = 1/t5/t4;
        const double t8 = l*t7;
        const double t10 = t2*t2;
        const double t11 = t1*eta*t10;
        const double t12 = l*t5;
        const double t13 = sin(t12);
        const double t16 = cos(t12);
        const double t17 = 1.0-t16;
        const double t18 = t4*t4;
        const double t19 = 1/t18;
        const double t20 = t17*t19;
        const double t23 = 1/t4;
        const double t25 = eta*t2;
        const double t27 = 2.0*t17*t23*t25;
        const double t28 = -t8*t11*t13+2.0*t20*t11-t27;
        const double t30 = frameOr(0,1)*l;
        const double t31 = t30*t23;
        const double t32 = t2*omega1;
        const double t34 = t1*t32*t16;
        const double t36 = frameOr(0,1)*t13;
        const double t38 = t7*t1*t32;
        const double t40 = 1/t5;
        const double t41 = t40*omega1;
        const double t43 = frameOr(0,2)*l;
        const double t45 = t1*t10;
        const double t46 = t45*t13;
        const double t48 = frameOr(0,2)*t17;
        const double t50 = t19*t1*t10;
        const double t53 = t23*t2;
        const double t56 = frameOr(0,0)*l;
        const double t59 = frameOr(0,0)*t13;
        const double t62 = 1/l;
        const double t66 = l*l;
        const double t70 = -t66*t1*t2-t66*t2;
        const double t80 = t62*t7*eta*t2*t13*t70-2.0*t17/t66*t19*t70*eta*t2-t27;
        const double t83 = eta*t32;
        const double t84 = t83*t16;
        const double t88 = t7*t32*eta;
        const double t93 = frameOr(0,0)*t17;
        const double t99 = eta*t10;
        const double t104 = -t8*t99*t13+2.0*t20*t99;
        const double t110 = t13*t62;
        const double t114 = t110*t7*eta*t2-t23*eta*t2*t16;
        const double t117 = t25*l;
        const double t120 = -t110*t40+1.0;
        const double t121 = frameOr(0,0)*t120;
        const double t123 = t45*l;
        const double t126 = t53*l;
        const double t129 = t83*t13;
        const double t133 = t19*t32*eta;
        const double t141 = -t114*t23*t2+2.0*t120*t19*t99;
        const double t146 = frameOr(1,1)*l;
        const double t147 = t146*t23;
        const double t149 = frameOr(1,1)*t13;
        const double t152 = frameOr(1,2)*l;
        const double t155 = frameOr(1,2)*t17;
        const double t160 = frameOr(1,0)*l;
        const double t163 = frameOr(1,0)*t13;
        const double t174 = frameOr(1,0)*t17;
        const double t185 = frameOr(1,0)*t120;
        const double t199 = frameOr(2,1)*l;
        const double t200 = t199*t23;
        const double t202 = frameOr(2,1)*t13;
        const double t205 = frameOr(2,2)*l;
        const double t208 = frameOr(2,2)*t17;
        const double t213 = frameOr(2,0)*l;
        const double t216 = frameOr(2,0)*t13;
        const double t227 = frameOr(2,0)*t17;
        const double t238 = frameOr(2,0)*t120;
        frame(0,0) = t43*t7*t46+frameOr(0,0)*t28+t31*t34-t36*t38+t36*t41-2.0*t48*t50+t48*t53;
        frame(0,1) = -frameOr(0,2)*t13*t88-t56*t23*t34+t43*t23*t84+t59*t38-t59*t41+frameOr(0,1)*t80;
        frame(0,2) = t56*t7*t46+frameOr(0,2)*t104-t31*t84+t36*t88-2.0*t93*t50+t93*t53;
        pos(0) = frameOr(0,0)*t114*t23*t117+frameOr(0,2)*t141*l-2.0*t121*t19*t123-t30*t7*t129+2.0*frameOr(0,1)*t17*t133+t121*t126;
        frame(1,0) = t152*t7*t46+t147*t34-t149*t38+t149*t41-2.0*t155*t50+t155*t53+frameOr(1,0)*t28;
        frame(1,1) = -frameOr(1,2)*t13*t88+t152*t23*t84-t160*t23*t34+t163*t38-t163*t41+frameOr(1,1)*t80;
        frame(1,2) = t160*t7*t46+frameOr(1,2)*t104-t147*t84+t149*t88-2.0*t174*t50+t174*t53;
        pos(1) = frameOr(1,0)*t114*t23*t117+frameOr(1,2)*t141*l-2.0*t185*t19*t123-t146*t7*t129+2.0*frameOr(1,1)*t17*t133+t185*t126;
        frame(2,0) = t205*t7*t46+t200*t34-t202*t38+t202*t41-2.0*t208*t50+t208*t53+frameOr(2,0)*t28;
        frame(2,1) = -frameOr(2,2)*t13*t88+t205*t23*t84-t213*t23*t34+t216*t38-t216*t41+frameOr(2,1)*t80;
        frame(2,2) = t213*t7*t46+frameOr(2,2)*t104-t200*t84+t202*t88-2.0*t227*t50+t227*t53;
        pos(2) = frameOr(2,0)*t114*t23*t117+frameOr(2,2)*t141*l-2.0*t238*t19*t123-t199*t7*t129+2.0*frameOr(2,1)*t17*t133+t238*t126;
    }
    return std::pair<Eigen::Vector3d, Eigen::Matrix3d>(pos,frame);
}

Eigen::Matrix<Eigen::Vector3d, 3, 1> RibbonGeometrie::dPosdPos(double l) const
{
    Eigen::Matrix<Eigen::Vector3d, 3, 1>res;
    res << Eigen::Vector3d(1., 0., 0.), Eigen::Vector3d(0., 1., 0.), Eigen::Vector3d(0., 0., 1.);
    return res;
}

Eigen::Matrix<Eigen::Matrix3d, 3, 3> RibbonGeometrie::dFramedFrame(double l) const
{
    auto[pos,frame]=PosFrame(l);
    Eigen::Matrix3d K =frameOr.inverse()*frame;
    Eigen::Matrix<Eigen::Matrix3d, 3, 3> res;
    res.setConstant(Eigen::Matrix3d::Zero());
    Eigen::Matrix3d temp;
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            temp.setZero();
            temp(i, j) = 1.;
            temp = temp * K;
            for (int a = 0; a < 3; a++) {
                for (int b = 0; b < 3; b++) {
                    res(a, b)(i, j) = temp(a, b);
                }
            }
        }
    }
    return res;
}

Eigen::Matrix<Eigen::Matrix3d, 3, 1> RibbonGeometrie::dPosdFrame(double l) const
{
    auto[pos,frame]=PosFrame(l);
    Eigen::Vector3d K =frameOr.inverse()*(pos-posOr);
    Eigen::Matrix<Eigen::Matrix3d, 3, 1> res;
    Eigen::Matrix3d temp;
    Eigen::Vector3d vtemp;
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            temp.setZero();
            temp(i, j) = 1.;
            vtemp = temp * K;
            for (int b = 0; b < 3; b++) {
                res[b](i, j) = vtemp(b);
            }
        }
    }
    return res;
}

Eigen::Matrix<Eigen::Matrix3d, 3, 3> RibbonGeometrie::d2FramedFramedOmega1(double l) const
{
  auto[pos,frame]=dPosFramedOmega1(l);
    Eigen::Matrix3d K =frameOr.inverse()*frame;
    Eigen::Matrix<Eigen::Matrix3d, 3, 3> res;
    res.setConstant(Eigen::Matrix3d::Zero());
    Eigen::Matrix3d temp;
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            temp.setZero();
            temp(i, j) = 1.;
            temp = temp * K;
            for (int a = 0; a < 3; a++) {
                for (int b = 0; b < 3; b++) {
                    res(a, b)(i, j) = temp(a, b);
                }
            }
        }
    }
    return res;
}

Eigen::Matrix<Eigen::Matrix3d, 3, 3> RibbonGeometrie::d2FramedFramedEta(double l) const
{
  auto[pos,frame]=dPosFramedEta(l);
    Eigen::Matrix3d K =frameOr.inverse()*frame;
    Eigen::Matrix<Eigen::Matrix3d, 3, 3> res;
    res.setConstant(Eigen::Matrix3d::Zero());
    Eigen::Matrix3d temp;
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            temp.setZero();
            temp(i, j) = 1.;
            temp = temp * K;
            for (int a = 0; a < 3; a++) {
                for (int b = 0; b < 3; b++) {
                    res(a, b)(i, j) = temp(a, b);
                }
            }
        }
    }
    return res;
}

Eigen::Matrix<Eigen::Matrix3d, 3, 1> RibbonGeometrie::d2PosdFramedOmega1(double l) const
{
    auto[pos,frame]=dPosFramedOmega1(l);
    Eigen::Vector3d K =frameOr.inverse()*(pos);
    Eigen::Matrix<Eigen::Matrix3d, 3, 1> res;
    Eigen::Matrix3d temp;
    Eigen::Vector3d vtemp;
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            temp.setZero();
            temp(i, j) = 1.;
            vtemp = temp * K;
            for (int b = 0; b < 3; b++) {
                res[b](i, j) = vtemp(b);
            }
        }
    }
    return res;
}

Eigen::Matrix<Eigen::Matrix3d, 3, 1> RibbonGeometrie::d2PosdFramedEta(double l) const
{
    auto[pos,frame]=dPosFramedEta(l);
    Eigen::Vector3d K =frameOr.inverse()*(pos);
    Eigen::Matrix<Eigen::Matrix3d, 3, 1> res;
    Eigen::Matrix3d temp;
    Eigen::Vector3d vtemp;
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            temp.setZero();
            temp(i, j) = 1.;
            vtemp = temp * K;
            for (int b = 0; b < 3; b++) {
                res[b](i, j) = vtemp(b);
            }
        }
    }
    return res;
}



std::pair<Eigen::Matrix<Eigen::Matrix2d, 3, 1>, Eigen::Matrix<Eigen::Matrix2d, 3, 3> > RibbonGeometrie::hessianPosFrame(double l) const
{
    Eigen::Matrix<Eigen::Matrix2d, 3, 1> pos;
    Eigen::Matrix<Eigen::Matrix2d, 3, 3> frame;
    if(testLittle(l))
    {
        frame.setConstant(Eigen::Matrix2d::Zero());
        pos.setConstant(Eigen::Matrix2d::Zero());
    }
    else
    {
        {
            const double t1 = eta*eta;
            const double t2 = omega1*omega1;
            const double t3 = t1*t2;
            const double t4 = t3+t2;
            const double t5 = t4*t4;
            const double t6 = sqrt(t4);
            const double t8 = 1/t6/t5;
            const double t9 = l*t8;
            const double t10 = t1*omega1;
            const double t11 = t10+omega1;
            const double t12 = 4.0*t11*t11;
            const double t14 = l*t6;
            const double t15 = sin(t14);
            const double t16 = t15*t1;
            const double t17 = t2*t16;
            const double t21 = 1/t6/t4;
            const double t22 = l*t21;
            const double t23 = t1+1.0;
            const double t27 = l*l;
            const double t28 = 1/t5;
            const double t29 = t27*t28;
            const double t31 = cos(t14);
            const double t40 = 1.0-t31;
            const double t42 = 1/t5/t4;
            const double t43 = t40*t42;
            const double t47 = t40*t28;
            const double t53 = 1/t4;
            const double t54 = t40*t53;
            const double t57 = 5.0/4.0*t9*t12*t17-t22*t23*t17-t29*t12*t31*t1*t2/4.0-4.0*t22*t11*t16*omega1-2.0*t43*t3*t12+8.0*t47*t10*t11+2.0*t47*t3*t23-2.0*t54*t1;
            const double t59 = frameOr(0,1)*l;
            const double t60 = t59*t28;
            const double t61 = t12*t31;
            const double t62 = eta*omega1;
            const double t63 = t61*t62;
            const double t66 = t59*t53;
            const double t67 = 2.0*t23*t31;
            const double t68 = t67*t62;
            const double t71 = frameOr(0,1)*t27;
            const double t72 = t71*t21;
            const double t73 = t12*t15;
            const double t74 = t73*t62;
            const double t78 = 2.0*t11*t31*eta;
            const double t80 = frameOr(0,1)*t15;
            const double t82 = t62*t12;
            const double t86 = 2.0*t21*eta*t11;
            const double t89 = 2.0*t62*t23;
            const double t92 = frameOr(0,2)*l;
            const double t94 = eta*t2;
            const double t95 = t73*t94;
            const double t98 = t92*t21;
            const double t99 = 2.0*t23*t15;
            const double t100 = t99*t94;
            const double t103 = frameOr(0,2)*t27;
            const double t105 = t61*t94;
            const double t108 = 2.0*t11*t15;
            const double t109 = t108*t62;
            const double t112 = frameOr(0,2)*t40;
            const double t114 = t94*t12;
            const double t117 = t112*t28;
            const double t118 = 2.0*t62*t11;
            const double t121 = 2.0*t94*t23;
            const double t123 = t53*eta;
            const double t126 = frameOr(0,0)*t57-3.0/4.0*t60*t63+t66*t68/2.0-t72*t74/4.0+t66*t78+3.0/4.0*t80*t8*t82-t80*t86-t80*t21*t89/2.0-5.0/4.0*t92*t8*t95+t98*t100/2.0+t103*t28*t105/4.0+2.0*t98*t109+2.0*t112*t42*t114-4.0*t117*t118-t117*t121+2.0*t112*t123;
            const double t127 = frameOr(0,0)*l;
            const double t131 = t127*t53;
            const double t134 = frameOr(0,0)*t27;
            const double t139 = frameOr(0,0)*t15;
            const double t147 = 1/l;
            const double t149 = t27*t1;
            const double t152 = -t149*t2-t27*t2;
            const double t156 = t147*t21;
            const double t160 = t28*t12;
            const double t166 = -t149*omega1-t27*omega1;
            const double t170 = t40/t27;
            const double t185 = -5.0/4.0*t147*t8*t73*t152+t156*t99*t152/2.0+t160*t31*t152/4.0+2.0*t156*t108*t166+2.0*t170*t42*t152*t12-8.0*t170*t28*t166*t11-2.0*t170*t28*t152*t23+2.0*t170*t53*(-t149-t27);
            const double t188 = t61*omega1;
            const double t192 = t67*omega1;
            const double t196 = t73*omega1;
            const double t200 = 2.0*t53*t11*t31;
            const double t202 = frameOr(0,2)*t15;
            const double t204 = t8*omega1*t12;
            const double t207 = 2.0*t21*t11;
            const double t210 = 2.0*t21*omega1*t23;
            const double t213 = 3.0/4.0*t127*t28*t63-t131*t68/2.0+t134*t21*t74/4.0-t131*t78-3.0/4.0*t139*t8*t82+t139*t86+t139*t21*t89/2.0+frameOr(0,1)*t185-3.0/4.0*t92*t28*t188+t92*t53*t192/2.0-t103*t21*t196/4.0+t92*t200+3.0/4.0*t202*t204-t202*t207-t202*t210/2.0;
            const double t217 = t127*t21;
            const double t225 = frameOr(0,0)*t40;
            const double t229 = t225*t28;
            const double t259 = t2*t12;
            const double t262 = 2.0*omega1*t11;
            const double t265 = 2.0*t23*t2;
            const double t268 = 5.0/4.0*t9*t73*t2-t22*t99*t2/2.0-t29*t61*t2/4.0-2.0*t22*t108*omega1-2.0*t43*t259+4.0*t47*t262+t47*t265-2.0*t54;
            const double t270 = -5.0/4.0*t127*t8*t95+t217*t100/2.0+t134*t28*t105/4.0+2.0*t217*t109+2.0*t225*t42*t114-4.0*t229*t118-t229*t121+2.0*t225*t123+3.0/4.0*t60*t188-t66*t192/2.0+t72*t196/4.0-t59*t200-3.0/4.0*t80*t204+t80*t207+t80*t210/2.0+frameOr(0,2)*t268;
            const double t280 = t15*t147;
            const double t287 = 3.0/4.0*t160*t31-t53*t23*t31+t21*t12*l*t15/4.0-3.0/4.0*t280*t8*t12+t280*t21*t23;
            const double t290 = t94*l;
            const double t293 = t280*t207-t200;
            const double t294 = frameOr(0,0)*t293/2.0;
            const double t296 = 2.0*l*t11;
            const double t297 = t94*t296;
            const double t301 = t62*l;
            const double t306 = 1.0-t280/t6;
            const double t307 = frameOr(0,0)*t306;
            const double t310 = t94*l*t12;
            const double t313 = t307*t28;
            const double t314 = t62*t296;
            const double t318 = 2.0*t94*l*t23;
            const double t320 = t123*l;
            const double t327 = t99*omega1;
            const double t333 = t207*t15;
            const double t335 = frameOr(0,1)*t40;
            const double t337 = t42*omega1*t12;
            const double t340 = 2.0*t28*t11;
            const double t344 = 2.0*t28*omega1*t23;
            const double t358 = t306*t28;
            const double t364 = 2.0*t293*t28*t2*t11-2.0*t293*t53*omega1-t287*t53*t2-2.0*t306*t42*t259+4.0*t358*t262+t358*t265-2.0*t306*t53;
            const double t367 = frameOr(0,0)*t287*t53*t290-2.0*t294*t28*t297+4.0*t294*t53*t301+2.0*t307*t42*t310-4.0*t313*t314-t313*t318+2.0*t307*t320+5.0/4.0*t59*t8*t196-t59*t21*t327/2.0-t71*t28*t188/4.0-t59*t333-2.0*t335*t337+2.0*t335*t340+t335*t344+frameOr(0,2)*t364*l;
            const double t369 = frameOr(1,1)*l;
            const double t370 = t369*t28;
            const double t373 = t369*t53;
            const double t376 = frameOr(1,1)*t27;
            const double t377 = t376*t21;
            const double t381 = frameOr(1,1)*t15;
            const double t389 = frameOr(1,2)*l;
            const double t393 = t389*t21;
            const double t396 = frameOr(1,2)*t27;
            const double t402 = frameOr(1,2)*t40;
            const double t406 = t402*t28;
            const double t412 = frameOr(1,0)*t57-3.0/4.0*t370*t63+t373*t68/2.0-t377*t74/4.0+t373*t78+3.0/4.0*t381*t8*t82-t381*t86-t381*t21*t89/2.0-5.0/4.0*t389*t8*t95+t393*t100/2.0+t396*t28*t105/4.0+2.0*t393*t109+2.0*t402*t42*t114-4.0*t406*t118-t406*t121+2.0*t402*t123;
            const double t413 = frameOr(1,0)*l;
            const double t417 = t413*t53;
            const double t420 = frameOr(1,0)*t27;
            const double t425 = frameOr(1,0)*t15;
            const double t444 = frameOr(1,2)*t15;
            const double t450 = 3.0/4.0*t413*t28*t63-t417*t68/2.0+t420*t21*t74/4.0-t417*t78-3.0/4.0*t425*t8*t82+t425*t86+t425*t21*t89/2.0+frameOr(1,1)*t185-3.0/4.0*t389*t28*t188+t389*t53*t192/2.0-t396*t21*t196/4.0+t389*t200+3.0/4.0*t444*t204-t444*t207-t444*t210/2.0;
            const double t454 = t413*t21;
            const double t462 = frameOr(1,0)*t40;
            const double t466 = t462*t28;
            const double t485 = -5.0/4.0*t413*t8*t95+t454*t100/2.0+t420*t28*t105/4.0+2.0*t454*t109+2.0*t462*t42*t114-4.0*t466*t118-t466*t121+2.0*t462*t123+3.0/4.0*t370*t188-t373*t192/2.0+t377*t196/4.0-t369*t200-3.0/4.0*t381*t204+t381*t207+t381*t210/2.0+frameOr(1,2)*t268;
            const double t489 = frameOr(1,0)*t293/2.0;
            const double t496 = frameOr(1,0)*t306;
            const double t500 = t496*t28;
            const double t516 = frameOr(1,1)*t40;
            const double t524 = frameOr(1,0)*t287*t53*t290-2.0*t489*t28*t297+4.0*t489*t53*t301+2.0*t496*t42*t310-4.0*t500*t314-t500*t318+2.0*t496*t320+5.0/4.0*t369*t8*t196-t369*t21*t327/2.0-t376*t28*t188/4.0-t369*t333-2.0*t516*t337+2.0*t516*t340+t516*t344+frameOr(1,2)*t364*l;
            const double t526 = frameOr(2,1)*l;
            const double t527 = t526*t28;
            const double t530 = t526*t53;
            const double t533 = frameOr(2,1)*t27;
            const double t534 = t533*t21;
            const double t538 = frameOr(2,1)*t15;
            const double t546 = frameOr(2,2)*l;
            const double t550 = t546*t21;
            const double t553 = frameOr(2,2)*t27;
            const double t559 = frameOr(2,2)*t40;
            const double t563 = t559*t28;
            const double t569 = frameOr(2,0)*t57-3.0/4.0*t527*t63+t530*t68/2.0-t534*t74/4.0+t530*t78+3.0/4.0*t538*t8*t82-t538*t86-t538*t21*t89/2.0-5.0/4.0*t546*t8*t95+t550*t100/2.0+t553*t28*t105/4.0+2.0*t550*t109+2.0*t559*t42*t114-4.0*t563*t118-t563*t121+2.0*t559*t123;
            const double t570 = frameOr(2,0)*l;
            const double t574 = t570*t53;
            const double t577 = frameOr(2,0)*t27;
            const double t582 = frameOr(2,0)*t15;
            const double t601 = frameOr(2,2)*t15;
            const double t607 = 3.0/4.0*t570*t28*t63-t574*t68/2.0+t577*t21*t74/4.0-t574*t78-3.0/4.0*t582*t8*t82+t582*t86+t582*t21*t89/2.0+frameOr(2,1)*t185-3.0/4.0*t546*t28*t188+t546*t53*t192/2.0-t553*t21*t196/4.0+t546*t200+3.0/4.0*t601*t204-t601*t207-t601*t210/2.0;
            const double t611 = t570*t21;
            const double t619 = frameOr(2,0)*t40;
            const double t623 = t619*t28;
            const double t642 = -5.0/4.0*t570*t8*t95+t611*t100/2.0+t577*t28*t105/4.0+2.0*t611*t109+2.0*t619*t42*t114-4.0*t623*t118-t623*t121+2.0*t619*t123+3.0/4.0*t527*t188-t530*t192/2.0+t534*t196/4.0-t526*t200-3.0/4.0*t538*t204+t538*t207+t538*t210/2.0+frameOr(2,2)*t268;
            const double t646 = frameOr(2,0)*t293/2.0;
            const double t653 = frameOr(2,0)*t306;
            const double t657 = t653*t28;
            const double t673 = frameOr(2,1)*t40;
            const double t681 = frameOr(2,0)*t287*t53*t290-2.0*t646*t28*t297+4.0*t646*t53*t301+2.0*t653*t42*t310-4.0*t657*t314-t657*t318+2.0*t653*t320+5.0/4.0*t526*t8*t196-t526*t21*t327/2.0-t533*t28*t188/4.0-t526*t333-2.0*t673*t337+2.0*t673*t340+t673*t344+frameOr(2,2)*t364*l;
            frame(0,0)(0,0) = t126;
            frame(0,1)(0,0) = t213;
            frame(0,2)(0,0) = t270;
            pos(0)(0,0) = t367;
            frame(1,0)(0,0) = t412;
            frame(1,1)(0,0) = t450;
            frame(1,2)(0,0) = t485;
            pos(1)(0,0) = t524;
            frame(2,0)(0,0) = t569;
            frame(2,1)(0,0) = t607;
            frame(2,2)(0,0) = t642;
            pos(2)(0,0) = t681;
        }
        {
            const double t1 = eta*eta;
            const double t2 = omega1*omega1;
            const double t4 = t1*t2+t2;
            const double t5 = t4*t4;
            const double t6 = sqrt(t4);
            const double t8 = 1/t6/t5;
            const double t9 = l*t8;
            const double t10 = t1*t1;
            const double t11 = t2*t2;
            const double t12 = t2*t11;
            const double t13 = t10*t12;
            const double t14 = l*t6;
            const double t15 = sin(t14);
            const double t20 = 1/t6/t4;
            const double t24 = t20*t1*t11*l*t15;
            const double t26 = l*l;
            const double t27 = 1/t5;
            const double t28 = t26*t27;
            const double t29 = cos(t14);
            const double t32 = 1.0-t29;
            const double t34 = 1/t5/t4;
            const double t35 = t32*t34;
            const double t38 = t32*t27;
            const double t39 = t1*t11;
            const double t40 = t38*t39;
            const double t42 = 1/t4;
            const double t45 = 2.0*t32*t42*t2;
            const double t46 = 5.0*t9*t13*t15-t28*t13*t29-8.0*t35*t13-5.0*t24+10.0*t40-t45;
            const double t48 = frameOr(0,1)*l;
            const double t49 = t48*t27;
            const double t50 = t1*eta;
            const double t51 = omega1*t11;
            const double t52 = t50*t51;
            const double t53 = t52*t29;
            const double t57 = t2*omega1;
            const double t59 = eta*t57*t29;
            const double t62 = frameOr(0,1)*t26;
            const double t63 = t62*t20;
            const double t64 = t52*t15;
            const double t66 = frameOr(0,1)*t15;
            const double t68 = t8*t50*t51;
            const double t71 = t20*t57;
            const double t72 = t71*eta;
            const double t75 = frameOr(0,2)*l;
            const double t77 = t50*t12;
            const double t78 = t77*t15;
            const double t82 = eta*t11;
            const double t83 = t82*t15;
            const double t86 = frameOr(0,2)*t26;
            const double t88 = t77*t29;
            const double t90 = frameOr(0,2)*t32;
            const double t92 = t34*t50*t12;
            const double t96 = t27*eta*t11;
            const double t99 = 3.0*t75*t20*t83+t86*t27*t88+3.0*t48*t42*t59-5.0*t75*t8*t78+frameOr(0,0)*t46-3.0*t49*t53-t63*t64+3.0*t66*t68-3.0*t66*t72+8.0*t90*t92-6.0*t90*t96;
            const double t100 = frameOr(0,0)*l;
            const double t107 = frameOr(0,0)*t26;
            const double t110 = frameOr(0,0)*t15;
            const double t115 = 1/l;
            const double t118 = t11*t15;
            const double t122 = -t26*t1*t2-t26*t2;
            const double t130 = t27*t1;
            const double t131 = t11*t29;
            const double t136 = t32/t26;
            const double t147 = 8.0*t136*t34*t122*t1*t11-5.0*t115*t8*t1*t118*t122+t115*t20*t2*t15*t122-2.0*t136*t27*t122*t2+t130*t131*t122-4.0*t24+8.0*t40-t45;
            const double t150 = t1*t51;
            const double t151 = t150*t29;
            const double t155 = t42*t57*t29;
            const double t158 = t150*t15;
            const double t160 = frameOr(0,2)*t15;
            const double t162 = t8*t51*t1;
            const double t166 = 3.0*t100*t27*t53-3.0*t100*t42*t59+t107*t20*t64-3.0*t75*t27*t151-t86*t20*t158-3.0*t110*t68+3.0*t110*t72+frameOr(0,1)*t147+t75*t155+3.0*t160*t162-t160*t71;
            const double t175 = frameOr(0,0)*t32;
            const double t187 = t1*t12;
            const double t199 = -l*t20*t118+5.0*t9*t187*t15-t28*t187*t29+2.0*t38*t11-8.0*t35*t187;
            const double t201 = 3.0*t100*t20*t83-5.0*t8*t100*t78+t107*t27*t88+3.0*t49*t151-t48*t155+t63*t158-3.0*t66*t162+8.0*t175*t92-6.0*t175*t96+frameOr(0,2)*t199+t66*t71;
            const double t204 = t42*t2;
            const double t206 = t15*t115;
            const double t213 = -3.0*t206*t8*t1*t11+t206*t20*t2+3.0*t130*t131-t204*t29+t24;
            const double t217 = eta*t2*l;
            const double t225 = t206*t20*eta*t2-t42*eta*t2*t29;
            const double t226 = frameOr(0,0)*t225;
            const double t228 = t39*l;
            const double t231 = t204*l;
            const double t236 = 1.0-t206/t6;
            const double t237 = frameOr(0,0)*t236;
            const double t239 = t77*l;
            const double t243 = t82*l;
            const double t249 = t71*t15;
            const double t253 = frameOr(0,1)*t32;
            const double t255 = t34*t51*t1;
            const double t258 = t27*t57;
            const double t272 = 2.0*t236*t27*t11-8.0*t236*t34*t187-t213*t42*t2+4.0*t225*t27*t82;
            const double t275 = frameOr(0,0)*t213*t42*t217+frameOr(0,2)*t272*l-t62*t27*t151+5.0*t48*t8*t158-4.0*t226*t27*t228+8.0*t237*t34*t239-6.0*t237*t27*t243+2.0*t226*t231-t48*t249-8.0*t253*t255+2.0*t253*t258;
            const double t277 = frameOr(1,1)*l;
            const double t278 = t277*t27;
            const double t284 = frameOr(1,1)*t26;
            const double t285 = t284*t20;
            const double t287 = frameOr(1,1)*t15;
            const double t292 = frameOr(1,2)*l;
            const double t299 = frameOr(1,2)*t26;
            const double t302 = frameOr(1,2)*t32;
            const double t307 = 3.0*t292*t20*t83+t299*t27*t88+3.0*t277*t42*t59-5.0*t292*t8*t78-3.0*t278*t53-t285*t64+3.0*t287*t68-3.0*t287*t72+8.0*t302*t92-6.0*t302*t96+frameOr(1,0)*t46;
            const double t308 = frameOr(1,0)*l;
            const double t315 = frameOr(1,0)*t26;
            const double t318 = frameOr(1,0)*t15;
            const double t330 = frameOr(1,2)*t15;
            const double t334 = -3.0*t292*t27*t151-t299*t20*t158+t315*t20*t64+3.0*t308*t27*t53-3.0*t308*t42*t59+frameOr(1,1)*t147+t292*t155+3.0*t330*t162-3.0*t318*t68+3.0*t318*t72-t330*t71;
            const double t343 = frameOr(1,0)*t32;
            const double t356 = 3.0*t308*t20*t83+t315*t27*t88-5.0*t308*t8*t78+3.0*t278*t151-t277*t155+t285*t158-3.0*t287*t162+frameOr(1,2)*t199+t287*t71+8.0*t343*t92-6.0*t343*t96;
            const double t360 = frameOr(1,0)*t225;
            const double t366 = frameOr(1,0)*t236;
            const double t379 = frameOr(1,1)*t32;
            const double t386 = frameOr(1,0)*t213*t42*t217+frameOr(1,2)*t272*l-t284*t27*t151+5.0*t277*t8*t158-4.0*t360*t27*t228+8.0*t366*t34*t239-6.0*t366*t27*t243+2.0*t360*t231-t277*t249-8.0*t379*t255+2.0*t379*t258;
            const double t388 = frameOr(2,1)*l;
            const double t389 = t388*t27;
            const double t395 = frameOr(2,1)*t26;
            const double t396 = t395*t20;
            const double t398 = frameOr(2,1)*t15;
            const double t403 = frameOr(2,2)*l;
            const double t410 = frameOr(2,2)*t26;
            const double t413 = frameOr(2,2)*t32;
            const double t418 = 3.0*t403*t20*t83+t410*t27*t88+3.0*t388*t42*t59-5.0*t403*t8*t78-3.0*t389*t53-t396*t64+3.0*t398*t68-3.0*t398*t72+8.0*t413*t92-6.0*t413*t96+t46*frameOr(2,0);
            const double t419 = frameOr(2,0)*l;
            const double t426 = frameOr(2,0)*t26;
            const double t429 = frameOr(2,0)*t15;
            const double t441 = frameOr(2,2)*t15;
            const double t445 = -3.0*t151*t27*t403-t158*t20*t410+t20*t426*t64+3.0*t27*t419*t53-3.0*t419*t42*t59+t147*frameOr(2,1)+t155*t403+3.0*t162*t441-3.0*t429*t68+3.0*t429*t72-t441*t71;
            const double t454 = frameOr(2,0)*t32;
            const double t467 = 3.0*t20*t419*t83+t27*t426*t88-5.0*t419*t78*t8+3.0*t151*t389-t155*t388+t158*t396-3.0*t162*t398+t199*frameOr(2,2)+t398*t71+8.0*t454*t92-6.0*t454*t96;
            const double t471 = frameOr(2,0)*t225;
            const double t477 = frameOr(2,0)*t236;
            const double t490 = frameOr(2,1)*t32;
            const double t497 = t213*t217*t42*frameOr(2,0)+l*t272*frameOr(2,2)-t151*t27*t395+5.0*t158*t388*t8-4.0*t228*t27*t471+8.0*t239*t34*t477-6.0*t243*t27*t477+2.0*t231*t471-t249*t388-8.0*t255*t490+2.0*t258*t490;
            frame(0,0)(1,1) = t99;
            frame(0,1)(1,1) = t166;
            frame(0,2)(1,1) = t201;
            pos(0)(1,1) = t275;
            frame(1,0)(1,1) = t307;
            frame(1,1)(1,1) = t334;
            frame(1,2)(1,1) = t356;
            pos(1)(1,1) = t386;
            frame(2,0)(1,1) = t418;
            frame(2,1)(1,1) = t445;
            frame(2,2)(1,1) = t467;
            pos(2)(1,1) = t497;
        }
        {
            const double t1 = eta*eta;
            const double t2 = omega1*omega1;
            const double t3 = t1*t2;
            const double t4 = t3+t2;
            const double t5 = t4*t4;
            const double t6 = sqrt(t4);
            const double t8 = 1/t6/t5;
            const double t11 = t1*omega1+omega1;
            const double t12 = 2.0*l*t8*t11;
            const double t13 = l*t6;
            const double t14 = sin(t13);
            const double t15 = t1*eta;
            const double t17 = t2*t2;
            const double t22 = 1/t6/t4;
            const double t23 = l*t22;
            const double t24 = t2*omega1;
            const double t25 = t15*t24;
            const double t29 = l*l;
            const double t30 = 1/t5;
            const double t32 = 2.0*t29*t30*t11;
            const double t33 = t15*t17;
            const double t34 = cos(t13);
            const double t38 = 2.0*t22*t11;
            const double t40 = eta*t2;
            const double t41 = t40*t14;
            const double t42 = t38*l*t41;
            const double t43 = 1.0-t34;
            const double t45 = 1/t5/t4;
            const double t46 = t43*t45;
            const double t50 = t43*t30;
            const double t51 = 2.0*t40*t11;
            const double t53 = 2.0*t50*t51;
            const double t56 = 1/t4;
            const double t58 = eta*omega1;
            const double t60 = 4.0*t43*t56*t58;
            const double t61 = 5.0/2.0*t12*t14*t15*t17-4.0*t23*t25*t14-t32*t33*t34/2.0-t42-8.0*t46*t33*t11+t53+8.0*t50*t25-t60;
            const double t63 = frameOr(0,1)*l;
            const double t64 = t63*t30;
            const double t65 = 2.0*t11*t34;
            const double t66 = t1*t24;
            const double t67 = t65*t66;
            const double t70 = t63*t56;
            const double t71 = t3*t34;
            const double t74 = frameOr(0,1)*t29;
            const double t75 = t74*t22;
            const double t76 = 2.0*t1*t11;
            const double t77 = t24*t14;
            const double t78 = t76*t77;
            const double t81 = t65*omega1;
            const double t84 = frameOr(0,1)*t14;
            const double t85 = t84*t8;
            const double t86 = 2.0*t66*t11;
            const double t90 = 2.0*t22*omega1*t11;
            const double t94 = t22*t1*t2;
            const double t97 = 1/t6;
            const double t99 = frameOr(0,2)*l;
            const double t101 = 2.0*t11*t14;
            const double t102 = t1*t17;
            const double t103 = t101*t102;
            const double t106 = t99*t22;
            const double t107 = t66*t14;
            const double t110 = frameOr(0,2)*t29;
            const double t113 = t76*t17*t34;
            const double t116 = t101*t2;
            const double t119 = frameOr(0,2)*t43;
            const double t121 = 2.0*t102*t11;
            const double t124 = t30*t2;
            const double t125 = 2.0*t124*t11;
            const double t128 = t30*t1*t24;
            const double t131 = t56*omega1;
            const double t134 = frameOr(0,0)*t61-3.0/2.0*t64*t67+3.0*t70*t71-t75*t78/2.0+t70*t81/2.0+3.0/2.0*t85*t86-t84*t90/2.0-3.0*t84*t94+t84*t97-5.0/2.0*t99*t8*t103+4.0*t106*t107+t110*t30*t113/2.0+t106*t116/2.0+4.0*t119*t45*t121-t119*t125-8.0*t119*t128+2.0*t119*t131;
            const double t135 = frameOr(0,0)*l;
            const double t139 = t135*t56;
            const double t142 = frameOr(0,0)*t29;
            const double t148 = frameOr(0,0)*t14;
            const double t157 = 1/l;
            const double t160 = t29*t1;
            const double t163 = -t160*t2-t29*t2;
            const double t169 = t157*t22*eta;
            const double t174 = 2.0*t30*t11;
            const double t176 = t2*t34;
            const double t181 = t43/t29;
            const double t187 = t181*t30;
            const double t195 = -t160*omega1-t29*omega1;
            const double t202 = -5.0*t157*t8*t11*t14*t163*t40+2.0*t169*omega1*t14*t163+t174*eta*t176*t163/2.0-t42+8.0*t181*t45*t163*t11*t40+t53-4.0*t187*t163*eta*omega1+2.0*t169*t2*t14*t195-4.0*t187*t195*eta*t2-t60;
            const double t205 = t24*eta;
            const double t206 = t65*t205;
            const double t210 = t40*t34;
            const double t215 = 2.0*t11*eta*t77;
            const double t218 = frameOr(0,2)*t14;
            const double t221 = 2.0*t24*t11*eta;
            const double t225 = t22*t2*eta;
            const double t228 = 3.0/2.0*t135*t30*t67-3.0*t139*t71+t142*t22*t78/2.0-t139*t81/2.0-3.0/2.0*t148*t8*t86+t148*t90/2.0+3.0*t148*t94-t148*t97+frameOr(0,1)*t202-3.0/2.0*t99*t30*t206+3.0*t99*t56*t210-t110*t22*t215/2.0+3.0/2.0*t218*t8*t221-3.0*t218*t225;
            const double t232 = t135*t22;
            const double t240 = frameOr(0,0)*t43;
            const double t266 = eta*t17;
            const double t271 = 2.0*t17*t11*eta;
            const double t276 = 5.0/2.0*t12*t14*t17*eta-4.0*t23*t205*t14-t32*t266*t34/2.0-4.0*t46*t271+8.0*t50*t205;
            const double t278 = -5.0/2.0*t135*t8*t103+4.0*t232*t107+t142*t30*t113/2.0+t232*t116/2.0+4.0*t240*t45*t121-t240*t125-8.0*t240*t128+2.0*t240*t131+3.0/2.0*t64*t206-3.0*t70*t210+t75*t215/2.0-3.0/2.0*t85*t221+3.0*t84*t225+frameOr(0,2)*t276;
            const double t281 = t56*eta;
            const double t286 = t14*t157;
            const double t294 = 3.0/2.0*t174*t210-2.0*t281*omega1*t34+t42/2.0-3.0/2.0*t286*t8*t51+2.0*t286*t22*eta*omega1;
            const double t297 = t40*l;
            const double t302 = -2.0*t56*t11*t34+t286*t38;
            const double t303 = frameOr(0,0)*t302/2.0;
            const double t305 = t102*l;
            const double t309 = t56*t2*l;
            const double t313 = -t281*t176+t286*t225;
            const double t314 = frameOr(0,0)*t313;
            const double t316 = 2.0*l*t11;
            const double t317 = t40*t316;
            const double t320 = -t286*t97+1.0;
            const double t321 = frameOr(0,0)*t320;
            const double t323 = t102*t316;
            const double t326 = t321*t30;
            const double t328 = 2.0*t2*l*t11;
            const double t330 = t66*l;
            const double t334 = t58*l;
            const double t337 = t131*l;
            const double t349 = frameOr(0,1)*t43;
            const double t353 = t124*eta;
            const double t373 = 2.0*t313*t30*t2*t11-2.0*t313*t56*omega1-t294*t56*t2+8.0*t320*t30*t205+t302*t30*t266-4.0*t320*t45*t271;
            const double t376 = frameOr(0,0)*t294*t56*t297-2.0*t303*t30*t305+t303*t309-t314*t30*t317+4.0*t321*t45*t323-t326*t328-8.0*t326*t330+2.0*t314*t56*t334+2.0*t321*t337+5.0/2.0*t63*t8*t215-3.0*t63*t22*t41-t74*t30*t206/2.0-4.0*t349*t45*t221+6.0*t349*t353+frameOr(0,2)*t373*l;
            const double t378 = frameOr(1,1)*l;
            const double t379 = t378*t30;
            const double t382 = t378*t56;
            const double t385 = frameOr(1,1)*t29;
            const double t386 = t385*t22;
            const double t391 = frameOr(1,1)*t14;
            const double t392 = t391*t8;
            const double t400 = frameOr(1,2)*l;
            const double t404 = t400*t22;
            const double t407 = frameOr(1,2)*t29;
            const double t413 = frameOr(1,2)*t43;
            const double t422 = frameOr(1,0)*t61-3.0/2.0*t379*t67+3.0*t382*t71-t386*t78/2.0+t382*t81/2.0+3.0/2.0*t392*t86-t391*t90/2.0-3.0*t391*t94+t391*t97-5.0/2.0*t400*t8*t103+4.0*t404*t107+t407*t30*t113/2.0+t404*t116/2.0+4.0*t413*t45*t121-t413*t125-8.0*t413*t128+2.0*t413*t131;
            const double t423 = frameOr(1,0)*l;
            const double t427 = t423*t56;
            const double t430 = frameOr(1,0)*t29;
            const double t436 = frameOr(1,0)*t14;
            const double t455 = frameOr(1,2)*t14;
            const double t461 = 3.0/2.0*t423*t30*t67-3.0*t427*t71+t430*t22*t78/2.0-t427*t81/2.0-3.0/2.0*t436*t8*t86+t436*t90/2.0+3.0*t436*t94-t436*t97+frameOr(1,1)*t202-3.0/2.0*t400*t30*t206+3.0*t400*t56*t210-t407*t22*t215/2.0+3.0/2.0*t455*t8*t221-3.0*t455*t225;
            const double t465 = t423*t22;
            const double t473 = frameOr(1,0)*t43;
            const double t493 = -5.0/2.0*t423*t8*t103+4.0*t465*t107+t430*t30*t113/2.0+t465*t116/2.0+4.0*t473*t45*t121-t473*t125-8.0*t473*t128+2.0*t473*t131+3.0/2.0*t379*t206-3.0*t382*t210+t386*t215/2.0-3.0/2.0*t392*t221+3.0*t391*t225+frameOr(1,2)*t276;
            const double t497 = frameOr(1,0)*t302/2.0;
            const double t502 = frameOr(1,0)*t313;
            const double t505 = frameOr(1,0)*t320;
            const double t509 = t505*t30;
            const double t527 = frameOr(1,1)*t43;
            const double t535 = frameOr(1,0)*t294*t56*t297-2.0*t497*t30*t305+t497*t309-t502*t30*t317+4.0*t505*t45*t323-t509*t328-8.0*t509*t330+2.0*t502*t56*t334+2.0*t505*t337+5.0/2.0*t378*t8*t215-3.0*t378*t22*t41-t385*t30*t206/2.0-4.0*t527*t45*t221+6.0*t527*t353+frameOr(1,2)*t373*l;
            const double t537 = frameOr(2,1)*l;
            const double t538 = t537*t30;
            const double t541 = t537*t56;
            const double t544 = frameOr(2,1)*t29;
            const double t545 = t544*t22;
            const double t550 = frameOr(2,1)*t14;
            const double t551 = t550*t8;
            const double t559 = frameOr(2,2)*l;
            const double t563 = t559*t22;
            const double t566 = frameOr(2,2)*t29;
            const double t572 = frameOr(2,2)*t43;
            const double t581 = frameOr(2,0)*t61-3.0/2.0*t538*t67+3.0*t541*t71-t545*t78/2.0+t541*t81/2.0+3.0/2.0*t551*t86-t550*t90/2.0-3.0*t550*t94+t550*t97-5.0/2.0*t559*t8*t103+4.0*t563*t107+t566*t30*t113/2.0+t563*t116/2.0+4.0*t572*t45*t121-t572*t125-8.0*t572*t128+2.0*t572*t131;
            const double t582 = frameOr(2,0)*l;
            const double t586 = t582*t56;
            const double t589 = frameOr(2,0)*t29;
            const double t595 = frameOr(2,0)*t14;
            const double t614 = frameOr(2,2)*t14;
            const double t620 = 3.0/2.0*t582*t30*t67-3.0*t586*t71+t589*t22*t78/2.0-t586*t81/2.0-3.0/2.0*t595*t8*t86+t595*t90/2.0+3.0*t595*t94-t595*t97+frameOr(2,1)*t202-3.0/2.0*t559*t30*t206+3.0*t559*t56*t210-t566*t22*t215/2.0+3.0/2.0*t614*t8*t221-3.0*t614*t225;
            const double t624 = t582*t22;
            const double t632 = frameOr(2,0)*t43;
            const double t652 = -5.0/2.0*t582*t8*t103+4.0*t624*t107+t589*t30*t113/2.0+t624*t116/2.0+4.0*t632*t45*t121-t632*t125-8.0*t632*t128+2.0*t632*t131+3.0/2.0*t538*t206-3.0*t541*t210+t545*t215/2.0-3.0/2.0*t551*t221+3.0*t550*t225+frameOr(2,2)*t276;
            const double t656 = frameOr(2,0)*t302/2.0;
            const double t661 = frameOr(2,0)*t313;
            const double t664 = frameOr(2,0)*t320;
            const double t668 = t664*t30;
            const double t686 = frameOr(2,1)*t43;
            const double t694 = frameOr(2,0)*t294*t56*t297-2.0*t656*t30*t305+t656*t309-t661*t30*t317+4.0*t664*t45*t323-t668*t328-8.0*t668*t330+2.0*t661*t56*t334+2.0*t664*t337+5.0/2.0*t537*t8*t215-3.0*t537*t22*t41-t544*t30*t206/2.0-4.0*t686*t45*t221+6.0*t686*t353+frameOr(2,2)*t373*l;
            frame(0,0)(1,0) = frame(0,0)(0,1) = t134;
            frame(0,1)(1,0) = frame(0,1)(0,1) = t228;
            frame(0,2)(1,0) = frame(0,2)(0,1) = t278;
            pos(0)(1,0) = pos(0)(0,1)  = t376;
            frame(1,0)(1,0) = frame(1,0)(0,1) = t422;
            frame(1,1)(1,0) = frame(1,1)(0,1) = t461;
            frame(1,2)(1,0) = frame(1,2)(0,1) = t493;
            pos(1)(1,0) = pos(1)(0,1)  = t535;
            frame(2,0)(1,0) = frame(2,0)(0,1) = t581;
            frame(2,1)(1,0) = frame(2,1)(0,1) = t620;
            frame(2,2)(1,0) = frame(2,2)(0,1) = t652;
            pos(2)(1,0) = pos(2)(0,1)  = t694;
        }
    }
    return std::pair<Eigen::Matrix<Eigen::Matrix2d, 3, 1>, Eigen::Matrix<Eigen::Matrix2d, 3, 3> >(pos,frame);
}


//
// Eigen::Matrix<Eigen::Matrix<Eigen::Vector2d, 3, 3>, 3, 3> RibbonGeometrie::d2FramedFramedOmega1Eta(double s)
// {
//   if (!called["computeSeriePreTerms"])computeSeriePreTerms();
//   if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
//   int order = PosPreTerms.size();
//   computeSPowers(s);
//   Eigen::Matrix3d Komega1 = Eigen::Matrix3d::Zero();
//   Eigen::Matrix3d Keta = Eigen::Matrix3d::Zero();
//   for (int i = 1; i < order; i++) {
//     Komega1 += spowers[i] * dFramePreTermsdomega1[i];
//     Keta += spowers[i] * dFramePreTermsdeta[i];
//   }
//
//   Eigen::Matrix<Eigen::Matrix<Eigen::Vector2d, 3, 3>, 3, 3> res;
//   Eigen::Matrix<Eigen::Vector2d, 3, 3> tmp1;
//   tmp1.setConstant(Eigen::Vector2d(0., 0.));
//   res.setConstant(tmp1);
//   Eigen::Matrix3d tmpomega1;
//   Eigen::Matrix3d tmpeta;
//   Eigen::Matrix3d temp;
//   for (int i = 0; i < 3; i++) {
//     for (int j = 0; j < 3; j++) {
//       temp.setZero();
//       temp(i, j) = 1.;
//       tmpomega1 = temp * Komega1;
//       tmpeta = temp * Keta;
//       for (int a = 0; a < 3; a++) {
//         for (int b = 0; b < 3; b++) {
//           res(a, b)(i, j)[0] = tmpomega1(a, b);
//           res(a, b)(i, j)[1] = tmpeta(a, b);
//         }
//       }
//     }
//   }
//   return res;
// }
//
// Eigen::Matrix<Eigen::Matrix<Eigen::Vector2d, 3, 3>, 3, 1> RibbonGeometrie::d2PosdFramedOmega1Eta(double s)
// {
//   if (!called["computeSeriePreTerms"])computeSeriePreTerms();
//   if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
//   int order = PosPreTerms.size();
//   computeSPowers(s);
//   Eigen::Vector3d Komega1 = Eigen::Vector3d::Zero();
//   Eigen::Vector3d Keta = Eigen::Vector3d::Zero();
//   for (int i = 1; i < order; i++) {
//     Komega1 += spowers[i] * dPosPreTermsdomega1[i];
//     Keta += spowers[i] * dPosPreTermsdeta[i];
//   }
//
//   Eigen::Matrix<Eigen::Matrix<Eigen::Vector2d, 3, 3>, 3, 1> res;
//   Eigen::Matrix<Eigen::Vector2d, 3, 3> tmp1;
//   tmp1.setConstant(Eigen::Vector2d(0., 0.));
//   res.setConstant(tmp1);
//   Eigen::Vector3d tmpomega1;
//   Eigen::Vector3d tmpeta;
//   Eigen::Matrix3d temp;
//   for (int i = 0; i < 3; i++) {
//     for (int j = 0; j < 3; j++) {
//       temp.setZero();
//       temp(i, j) = 1.;
//       tmpomega1 = temp * Komega1;
//       tmpeta = temp * Keta;
//       for (int b = 0; b < 3; b++) {
//         res(b)(i, j)[0] = tmpomega1(b);
//         res(b)(i, j)[1] = tmpeta(b);
//       }
//     }
//   }
//   return res;
// }
//
//
