/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef RIBBON_GEOMETRIE_H
#define RIBBON_GEOMETRIE_H

#include <Eigen/Dense>
#include <vector>
#include <utility>
#include <iostream>
#include <map>

class RibbonGeometrie
{
public:
    void Place(Eigen::Vector3d pos, Eigen::Matrix3d frame);
    void Setup(double omega1, double eta);

    std::pair<Eigen::Vector3d, Eigen::Matrix3d> PosFrame(double s) const;
    std::pair<Eigen::Vector3d, Eigen::Matrix3d> dPosFramedOmega1(double s) const;
    std::pair<Eigen::Vector3d, Eigen::Matrix3d> dPosFramedEta(double s) const;
    Eigen::Matrix<Eigen::Vector3d, 3, 1> dPosdPos(double s) const;
    Eigen::Matrix<Eigen::Matrix3d, 3, 3> dFramedFrame(double s) const;
    Eigen::Matrix<Eigen::Matrix3d, 3, 1> dPosdFrame(double s) const;
    
    Eigen::Matrix<Eigen::Matrix3d, 3, 3> d2FramedFramedOmega1(double s) const;
    Eigen::Matrix<Eigen::Matrix3d, 3, 3> d2FramedFramedEta(double s) const;
    Eigen::Matrix<Eigen::Matrix3d, 3, 1> d2PosdFramedOmega1(double s) const;
    Eigen::Matrix<Eigen::Matrix3d, 3, 1> d2PosdFramedEta(double s) const;

    std::pair<Eigen::Matrix<Eigen::Matrix2d, 3, 1>,Eigen::Matrix<Eigen::Matrix2d, 3, 3>> hessianPosFrame(double s) const;

//     Eigen::Matrix<Eigen::Matrix<Eigen::Vector2d, 3, 3>, 3, 3> d2FramedFramedOmega1Eta(double s);
//     Eigen::Matrix<Eigen::Matrix<Eigen::Vector2d, 3, 3>, 3, 1> d2PosdFramedOmega1Eta(double s);
// 
//     bool checkDerivations();
private:
     Eigen::Vector3d posOr;
     Eigen::Matrix3d frameOr;
     double omega1;
     double eta;
     
     bool testLittle(double l) const;
};
#endif // RIBBON_GEOMETRIE_H

