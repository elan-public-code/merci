/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "RibbonPhysique.hpp"
#include "SimpleElasticEnergy.hpp"
#include "RibbonGeometrie.hpp"
#include "MathOperations.hpp"

using namespace MathOperations;


void RibbonPhysique::set(Ribbon sr)
{
  this->sr = sr;
  precomputeStuff();
}

Ribbon RibbonPhysique::get()
{
  return sr;
}

void RibbonPhysique::setup(Eigen::Vector3d posOr, Eigen::Matrix3d frameOr, double width, double D, double areaDensity, std::vector<double> segmentsLength)
{
  sr = Ribbon();
  sr.setPosOr(posOr);
  sr.setFrameOr(frameOr);
  sr.setWidth(width);
  sr.setD(D);
  sr.setAreaDensity(areaDensity);
  int t = segmentsLength.size();
  assert(t > 0);
  for (int i = 0; i < t; i++) {
    sr.addSegmentAt(i);
    sr.setLength(i, segmentsLength[i]);
  }
  precomputeStuff();
}

void RibbonPhysique::setGravity(Eigen::Vector3d grav)
{
  G = grav;
}

Eigen::Vector3d RibbonPhysique::getGravity()
{
  return G;
}

Eigen::VectorXd RibbonPhysique::getPoint()
{
  int t = sr.getNbSegments();
  Eigen::VectorXd pt(2*t);
  for (int i = 0; i < t; i++) {
    pt[2*i] = sr.getOmega1(i);
    pt[2*i+1] = sr.getEta(i);
  }
  return pt;
}

Eigen::VectorXd RibbonPhysique::getUpperBound()
{
  int t = sr.getNbSegments();
  Eigen::VectorXd pt(2*t);
  pt.setConstant(std::numeric_limits<double>::infinity());
  return pt;
}

Eigen::VectorXd RibbonPhysique::getLowerBound()
{
  int t = sr.getNbSegments();
  Eigen::VectorXd pt(2*t);
  pt.setConstant(-std::numeric_limits<double>::infinity());
  return pt;
}

void RibbonPhysique::setPoint(Eigen::VectorXd pt)
{
  int t = sr.getNbSegments();
  assert(pt.size() == 2*t);
  for (int i = 0; i < t; i++) {
    sr.setOmega1(i, pt[2*i]);
    sr.setEta(i, pt[2*i+1]);
  }
}

double RibbonPhysique::getEnergy()
{
  int t = sr.getNbSegments();
  Eigen::Vector3d pos = sr.getPosOr();
  Eigen::Matrix3d frame = sr.getFrameOr();
  double res = 0.;
  double reselastic = 0.;
  double resgravity = 0.;
  Eigen::VectorXd eta(t);
  Eigen::VectorXd omega1(t);
  for(int i=0;i<t;i++)
  {
    eta(i)=sr.getEta(i);
    omega1(i)=sr.getOmega1(i);
  }
  Eigen::VectorXd etaPrime = etaPrimeMatrix*eta;
  SimpleElasticEnergy sel;
  
  for (int i = 0; i < t; i++)
  {
    sel.FixPhysics(sr.getD(),sr.getWidth(),sr.getLength(i),sr.getPoissonRatio());
    sel.FixPt(omega1(i),eta(i),etaPrime(i),sr.getNatOmega1(i));
    reselastic+=sel.computeEnergy();
    resgravity+=(sr.getAreaDensity()*sr.getWidth()*sr.getLength(i))*pos.dot(G);
    RibbonGeometrie geo;
    geo.Place(pos,frame);
    geo.Setup(omega1(i),eta(i));
    std::tie(pos,frame)=geo.PosFrame(sr.getLength(i));
  }
  res=reselastic+resgravity;
  return res;
}

Eigen::VectorXd RibbonPhysique::getEnergyGradient()
{
  int t = sr.getNbSegments();
  Eigen::Vector3d pos = sr.getPosOr();
  Eigen::Matrix3d frame = sr.getFrameOr();
  
  std::vector<std::vector<Eigen::Vector3d>> posGrad;
  std::vector<std::vector<Eigen::Matrix3d>> frameGrad;
  
  frameGrad.resize(t);
  posGrad.resize(t);
  for(int i=0;i<t;i++)
  {
    frameGrad[i].resize(2*t, Eigen::Matrix3d::Zero());
    posGrad[i].resize(2*t,Eigen::Vector3d::Zero());
  }
  //index (pos, dof)
  Eigen::VectorXd eta(t);
  Eigen::VectorXd omega1(t);
  for(int i=0;i<t;i++)
  {
    eta(i)=sr.getEta(i);
    omega1(i)=sr.getOmega1(i);
  }
  Eigen::VectorXd etaPrime = etaPrimeMatrix*eta;
  
  
  Eigen::VectorXd gradEta(t);
  Eigen::VectorXd gradOmega1(t);
  gradEta.setZero();
  gradOmega1.setZero();
  SimpleElasticEnergy sel;
  for(int currentIndex =0; currentIndex <t; currentIndex++)
  {
    sel.FixPhysics(sr.getD(),sr.getWidth(),sr.getLength(currentIndex),sr.getPoissonRatio());
    sel.FixPt(omega1(currentIndex),eta(currentIndex),etaPrime(currentIndex),sr.getNatOmega1(currentIndex));
    auto selGrad = sel.computeGrad();
    gradEta+=selGrad[2]*etaPrimeMatrix.row(currentIndex);
    gradEta[currentIndex]+=selGrad[1];
    gradOmega1[currentIndex]+=selGrad[0];
    RibbonGeometrie geo;
    geo.Place(pos,frame);
    geo.Setup(omega1(currentIndex),eta(currentIndex));
    double l=sr.getLength(currentIndex);
    auto [dPosdEta,dFramedEta] = geo.dPosFramedEta(l);
    auto [dPosdOmega1,dFramedOmega1] = geo.dPosFramedOmega1(l);
    posGrad[currentIndex][1+2*currentIndex] = dPosdEta;
    posGrad[currentIndex][2*currentIndex] = dPosdOmega1;
    frameGrad[currentIndex][1+2*currentIndex] = dFramedEta;
    frameGrad[currentIndex][2*currentIndex] = dFramedOmega1;
    Eigen::Matrix<Eigen::Vector3d, 3, 1> dPosdPos=geo.dPosdPos(l);
    Eigen::Matrix<Eigen::Matrix3d, 3, 1> dPosdFrame=geo.dPosdFrame(l);
    Eigen::Matrix<Eigen::Matrix3d, 3, 3> dFramedFrame=geo.dFramedFrame(l);
    for(int j=0;j< currentIndex;j++)
    {
      posGrad[currentIndex][1+2*j]+=compDerivfg(posGrad[currentIndex -1][1+2*j],dPosdPos);
      posGrad[currentIndex][1+2*j]+=compDerivfg(frameGrad[currentIndex -1][1+2*j],dPosdFrame);
      posGrad[currentIndex][2*j]+=compDerivfg(posGrad[currentIndex -1][2*j],dPosdPos);
      posGrad[currentIndex][2*j]+=compDerivfg(frameGrad[currentIndex -1][2*j],dPosdFrame);
      frameGrad[currentIndex][1+2*j]+=compDerivfg(frameGrad[currentIndex -1][1+2*j],dFramedFrame);
      frameGrad[currentIndex][2*j]+=compDerivfg(frameGrad[currentIndex -1][2*j],dFramedFrame);
    }
    double M = sr.getWidth()*l*sr.getAreaDensity();
    
    for(int k=0;k< currentIndex;k++)
    {
      gradEta(k)+=M*posGrad[currentIndex-1][1+2*k].dot(G);
      gradOmega1(k)+=M*posGrad[currentIndex-1][2*k].dot(G);
    }
    
    std::tie(pos,frame)=geo.PosFrame(sr.getLength(currentIndex));
  }
  Eigen::VectorXd res(2*t);
  for(int i=0;i<t;i++)
  {
    res(2*i)=gradOmega1(i);
    res(2*i+1)=gradEta(i);
  }
  return res;
}

template < typename K, typename V>
V& GetWithDef(std::map<K,V>& m, K const& key, const V & defval)
{
    typename std::map<K,V>::iterator it = m.find( key );
    if (it == m.end())
    {
      m[key]=defval;
      return m[key];
    }
    return it->second;
}

Eigen::MatrixXd RibbonPhysique::getEnergyHessian()
{
  class listTriMat{
  public:
    listTriMat():idx(-1),entryX(0),entryY(0){}
    listTriMat(int i,int x,int y):idx(i),entryX(std::min(x,y)),entryY(std::max(x,y)){}
    bool operator<=(const listTriMat t) const{
      if(idx<t.idx)return true;
      if(idx>t.idx)return false;
      if(entryX<t.entryX)return true;
      if(entryX>t.entryX)return false;
      return entryY <=t.entryY;
    }
    bool operator<(const listTriMat t) const{
      if(idx<t.idx)return true;
      if(idx>t.idx)return false;
      if(entryX<t.entryX)return true;
      if(entryX>t.entryX)return false;
      return entryY <t.entryY;
    }
    bool operator ==(const listTriMat t) const{
      return (idx==t.idx)&&(entryX==t.entryX)&&(entryY==t.entryY);
    }
    int index() const {return idx;}
    int coordonateX() const {return entryX;}
    int coordonateY() const {return entryY;}
  private:
    int idx;
    int entryX;
    int entryY;
  };
  int t = sr.getNbSegments();
  Eigen::Vector3d pos = sr.getPosOr();
  Eigen::Matrix3d frame = sr.getFrameOr();
  Eigen::Vector3d v3_0;v3_0.setZero();
  Eigen::Matrix3d m33_0;m33_0.setZero();
  
  std::vector<std::vector<Eigen::Vector3d>> posGrad;
  std::vector<std::vector<Eigen::Matrix3d>> frameGrad;
  std::map<listTriMat, Eigen::Vector3d> posHessian;
  std::map<listTriMat, Eigen::Matrix3d> frameHessian;
  posGrad.resize(t);
  frameGrad.resize(t);
  for(int i=0;i<t;i++)
  {
    posGrad[i].resize(2*t, Eigen::Vector3d::Zero());
    frameGrad[i].resize(2*t, Eigen::Matrix3d::Zero());
  }
  //index (pos, dof)
  Eigen::VectorXd eta(t);
  Eigen::VectorXd omega1(t);
  for(int i=0;i<t;i++)
  {
    eta(i)=sr.getEta(i);
    omega1(i)=sr.getOmega1(i);
  }
  Eigen::VectorXd etaPrime = etaPrimeMatrix*eta;
  
  Eigen::MatrixXd res(2*t,2*t);
  res.setZero();
  SimpleElasticEnergy sel;
  for(int currentIndex =0; currentIndex <t; currentIndex++)
  {
    sel.FixPhysics(sr.getD(),sr.getWidth(),sr.getLength(currentIndex),sr.getPoissonRatio());
    sel.FixPt(omega1(currentIndex),eta(currentIndex),etaPrime(currentIndex),sr.getNatOmega1(currentIndex));
    auto selHess = sel.computeHessian();
    Eigen::SparseMatrix<double> transfo(3,2*t);
    using T=Eigen::Triplet<double>;
    std::vector<T > terms;
    terms.push_back(T(0,2*currentIndex,1.));
    terms.push_back(T(1,2*currentIndex+1,1.));
    auto list=wholeetaPrimesGradientLine(currentIndex);
    for(auto e: list)
      terms.push_back(T(2,e.col(),e.value()));
    transfo.setFromTriplets(terms.begin(),terms.end());
    res+=transfo.transpose()*selHess*transfo;
    
    RibbonGeometrie geo;
    geo.Place(pos,frame);
    geo.Setup(omega1(currentIndex),eta(currentIndex));
    double l=sr.getLength(currentIndex);
    auto [dPosdEta,dFramedEta] = geo.dPosFramedEta(l);
    auto [dPosdOmega1,dFramedOmega1] = geo.dPosFramedOmega1(l);
    auto [PosHessLoc,FrameHessLoc] = geo.hessianPosFrame(l);
    posGrad[currentIndex][1+2*currentIndex] = dPosdEta;
    posGrad[currentIndex][2*currentIndex] = dPosdOmega1;
    frameGrad[currentIndex][1+2*currentIndex] = dFramedEta;
    frameGrad[currentIndex][2*currentIndex] = dFramedOmega1;
    
    Eigen::Matrix<Eigen::Vector3d, 3, 1> dPosdPos=geo.dPosdPos(l);
    Eigen::Matrix<Eigen::Matrix3d, 3, 1> dPosdFrame=geo.dPosdFrame(l);
    Eigen::Matrix<Eigen::Matrix3d, 3, 3> dFramedFrame=geo.dFramedFrame(l);
    Eigen::Matrix<Eigen::Matrix3d, 3, 3> d2FramedFramedOmega1=geo.d2FramedFramedOmega1(l);
    Eigen::Matrix<Eigen::Matrix3d, 3, 3> d2FramedFramedEta=geo.d2FramedFramedEta(l);
    Eigen::Matrix<Eigen::Matrix3d, 3, 1> d2PosdFramedOmega1=geo.d2PosdFramedOmega1(l);
    Eigen::Matrix<Eigen::Matrix3d, 3, 1> d2PosdFramedEta=geo.d2PosdFramedEta(l);
    for(int j=0;j< currentIndex;j++)
    {
      posGrad[currentIndex][1+2*j]+=compDerivfg(posGrad[currentIndex -1][1+2*j],dPosdPos);
      posGrad[currentIndex][1+2*j]+=compDerivfg(frameGrad[currentIndex -1][1+2*j],dPosdFrame);
      posGrad[currentIndex][2*j]+=compDerivfg(posGrad[currentIndex -1][2*j],dPosdPos);
      posGrad[currentIndex][2*j]+=compDerivfg(frameGrad[currentIndex -1][2*j],dPosdFrame);
      frameGrad[currentIndex][1+2*j]+=compDerivfg(frameGrad[currentIndex -1][1+2*j],dFramedFrame);
      frameGrad[currentIndex][2*j]+=compDerivfg(frameGrad[currentIndex -1][2*j],dFramedFrame);
    }
    std::vector<std::pair<int,int>> coeffs={{0,0},{1,1},{1,0}};
    for(auto [i,j]: coeffs)
    {
      GetWithDef(posHessian,listTriMat(currentIndex,2*currentIndex+i,2*currentIndex+j),v3_0) += extractDerivation(PosHessLoc,i,j);
      GetWithDef(frameHessian,listTriMat(currentIndex,2*currentIndex+i,2*currentIndex+j),m33_0) += extractDerivation(FrameHessLoc,i,j);
    }
    for(int j=0; j < 2*currentIndex; j++)
    {
      for(int k=0; k <= j; k++)
      {
        GetWithDef(posHessian,listTriMat(currentIndex,j,k),v3_0)+=compDerivfg(GetWithDef(posHessian,listTriMat(currentIndex-1,j,k),v3_0),dPosdPos);
        GetWithDef(posHessian,listTriMat(currentIndex,j,k),v3_0)+=compDerivfg(GetWithDef(frameHessian,listTriMat(currentIndex-1,j,k),m33_0),dPosdFrame);
          
        GetWithDef(frameHessian,listTriMat(currentIndex,j,k),m33_0)+=compDerivfg(GetWithDef(frameHessian,listTriMat(currentIndex-1,j,k),m33_0),dFramedFrame);
      }
    }
      
    for(int j=0; j < currentIndex; j++)
    {
      
      GetWithDef(posHessian,listTriMat(currentIndex,2*currentIndex,   2*j),  v3_0)+=compDerivfg(frameGrad[currentIndex-1][2*j],  d2PosdFramedOmega1);
      GetWithDef(posHessian,listTriMat(currentIndex,2*currentIndex+1, 2*j+1),v3_0)+=compDerivfg(frameGrad[currentIndex-1][2*j+1],d2PosdFramedEta);
      GetWithDef(posHessian,listTriMat(currentIndex,2*currentIndex+1, 2*j),  v3_0)+=compDerivfg(frameGrad[currentIndex-1][2*j],  d2PosdFramedEta);
      GetWithDef(posHessian,listTriMat(currentIndex,2*currentIndex,   2*j+1),v3_0)+=compDerivfg(frameGrad[currentIndex-1][2*j+1],d2PosdFramedOmega1);
      
      GetWithDef(frameHessian,listTriMat(currentIndex,2*currentIndex,2*j),m33_0)+=compDerivfg(frameGrad[currentIndex-1][2*j],d2FramedFramedOmega1);
      GetWithDef(frameHessian,listTriMat(currentIndex,2*currentIndex+1,2*j+1),m33_0)+=compDerivfg(frameGrad[currentIndex-1][2*j+1],d2FramedFramedEta);
      GetWithDef(frameHessian,listTriMat(currentIndex,2*currentIndex+1,2*j),m33_0)+=compDerivfg(frameGrad[currentIndex-1][2*j],d2FramedFramedEta);
      GetWithDef(frameHessian,listTriMat(currentIndex,2*currentIndex,2*j+1),m33_0)+=compDerivfg(frameGrad[currentIndex-1][2*j+1],d2FramedFramedOmega1);
    }
    double M = sr.getWidth()*l*sr.getAreaDensity();
    for(int k=0;k<= 2*currentIndex-1;k++)
    {
      for(int l=0;l<= 2*currentIndex-1;l++)
      {
        res(k,l)+=M*GetWithDef(posHessian,listTriMat(currentIndex-1,k,l),v3_0).dot(G);
      }
    }
    std::tie(pos,frame)=geo.PosFrame(l);
  }
  return res;
}


double RibbonPhysique::targetEnergy(Eigen::Vector3d tPos, Eigen::Matrix3d tFrame)
{
  int t = sr.getNbSegments();
  Eigen::Vector3d pos = sr.getPosOr();
  Eigen::Matrix3d frame = sr.getFrameOr();
  
  for (int i = 0; i < t; i++)
  {
    RibbonGeometrie geo;
    geo.Place(pos,frame);
    geo.Setup(sr.getOmega1(i),sr.getEta(i));
    std::tie(pos,frame)=geo.PosFrame(sr.getLength(i));
  }
  Eigen::Matrix3d frameDiff=(tFrame-frame);
  return (tPos-pos).dot((tPos-pos)) + contractGeneralDotP(frameDiff,frameDiff);
}

Eigen::VectorXd RibbonPhysique::targetEnergyGradient(Eigen::Vector3d tPos, Eigen::Matrix3d tFrame)
{
  int t = sr.getNbSegments();
  Eigen::Vector3d pos = sr.getPosOr();
  Eigen::Matrix3d frame = sr.getFrameOr();
  
  std::vector<std::vector<Eigen::Vector3d>> posGradEta;
  std::vector<std::vector<Eigen::Matrix3d>> frameGradEta;
  std::vector<std::vector<Eigen::Vector3d>> posGradOmega1;
  std::vector<std::vector<Eigen::Matrix3d>> frameGradOmega1;
  posGradEta.resize(t);
  frameGradEta.resize(t);
  posGradOmega1.resize(t);
  frameGradOmega1.resize(t);
  for(int i=0;i<t;i++)
  {
    posGradEta[i].resize(t, Eigen::Vector3d::Zero());
    frameGradEta[i].resize(t, Eigen::Matrix3d::Zero());
    posGradOmega1[i].resize(t, Eigen::Vector3d::Zero());
    frameGradOmega1[i].resize(t, Eigen::Matrix3d::Zero());
  }
  //index (pos, dof)
  Eigen::VectorXd eta(t);
  Eigen::VectorXd omega1(t);
  for(int i=0;i<t;i++)
  {
    eta(i)=sr.getEta(i);
    omega1(i)=sr.getOmega1(i);
  }
  
  
  for(int currentIndex =0; currentIndex <t; currentIndex++)
  {
    RibbonGeometrie geo;
    geo.Place(pos,frame);
    geo.Setup(omega1(currentIndex),eta(currentIndex));
    double l=sr.getLength(currentIndex);
    auto [dPosdEta,dFramedEta] = geo.dPosFramedEta(l);
    auto [dPosdOmega1,dFramedOmega1] = geo.dPosFramedOmega1(l);
    std::vector<Eigen::Vector3d>& gradPosEtaCurrent = posGradEta[currentIndex];
    std::vector<Eigen::Matrix3d>& gradFrameEtaCurrent = frameGradEta[currentIndex];
    std::vector<Eigen::Vector3d>& gradPosOmega1Current = posGradOmega1[currentIndex];
    std::vector<Eigen::Matrix3d>& gradFrameOmega1Current = frameGradOmega1[currentIndex];
    posGradEta[currentIndex][currentIndex] = dPosdEta;
    posGradOmega1[currentIndex][currentIndex] = dPosdOmega1;
    frameGradEta[currentIndex][currentIndex] = dFramedEta;
    frameGradOmega1[currentIndex][currentIndex] = dFramedOmega1;
    
    Eigen::Matrix<Eigen::Vector3d, 3, 1> dPosdPos=geo.dPosdPos(l);
    Eigen::Matrix<Eigen::Matrix3d, 3, 1> dPosdFrame=geo.dPosdFrame(l);
    Eigen::Matrix<Eigen::Matrix3d, 3, 3> dFramedFrame=geo.dFramedFrame(l);
    for(int j=0;j< currentIndex;j++)
    {
      gradPosEtaCurrent[j]+=compDerivfg(posGradEta[currentIndex - 1][j],dPosdPos);
      gradPosEtaCurrent[j]+=compDerivfg(frameGradEta[currentIndex - 1][j],dPosdFrame);
      gradPosOmega1Current[j]+=compDerivfg(posGradOmega1[currentIndex - 1][j],dPosdPos);
      gradPosOmega1Current[j]+=compDerivfg(frameGradOmega1[currentIndex - 1][j],dPosdFrame);
      gradFrameEtaCurrent[j]+=compDerivfg(frameGradEta[currentIndex - 1][j],dFramedFrame);
      gradFrameOmega1Current[j]+=compDerivfg(frameGradOmega1[currentIndex - 1][j],dFramedFrame);
    }
    
    std::tie(pos,frame)=geo.PosFrame(l);
  }
  Eigen::VectorXd res(2*t);
  res.setZero();
  Eigen::Vector3d posDiff=-2.0*(tPos-pos);
  Eigen::Matrix3d frameDiff=-2.0*(tFrame-frame);
  for(int i=0;i<t;i++)
  {
    res(2*i)=posDiff.dot(posGradOmega1[t-1][i]);
    res(2*i+1)=posDiff.dot(posGradEta[t-1][i]);
    res(2*i)+=contractGeneralDotP(frameDiff,frameGradOmega1[t-1][i]);
    res(2*i+1)+=contractGeneralDotP(frameDiff,frameGradEta[t-1][i]);
  }
  return res;
}

Eigen::MatrixXd RibbonPhysique::targetEnergyHessian(Eigen::Vector3d tPos, Eigen::Matrix3d tFrame)
{
  class listTriMat{
  public:
    listTriMat():idx(-1),entryX(0),entryY(0){}
    listTriMat(int i,int x,int y):idx(i),entryX(std::min(x,y)),entryY(std::max(x,y)){}
    bool operator<=(const listTriMat t) const{
      if(idx<t.idx)return true;
      if(idx>t.idx)return false;
      if(entryX<t.entryX)return true;
      if(entryX>t.entryX)return false;
      return entryY <=t.entryY;
    }
    bool operator<(const listTriMat t) const{
      if(idx<t.idx)return true;
      if(idx>t.idx)return false;
      if(entryX<t.entryX)return true;
      if(entryX>t.entryX)return false;
      return entryY <t.entryY;
    }
    bool operator ==(const listTriMat t) const{
      return (idx==t.idx)&&(entryX==t.entryX)&&(entryY==t.entryY);
    }
    int index() const {return idx;}
    int coordonateX() const {return entryX;}
    int coordonateY() const {return entryY;}
  private:
    int idx;
    int entryX;
    int entryY;
  };
  int t = sr.getNbSegments();
  Eigen::Vector3d pos = sr.getPosOr();
  Eigen::Matrix3d frame = sr.getFrameOr();
  Eigen::Vector3d v3_0;v3_0.setZero();
  Eigen::Matrix3d m33_0;m33_0.setZero();
  
  std::vector<std::vector<Eigen::Vector3d>> posGrad;
  std::vector<std::vector<Eigen::Matrix3d>> frameGrad;
  std::map<listTriMat, Eigen::Vector3d> posHessian;
  std::map<listTriMat, Eigen::Matrix3d> frameHessian;
  posGrad.resize(t);
  frameGrad.resize(t);
  for(int i=0;i<t;i++)
  {
    posGrad[i].resize(2*t, Eigen::Vector3d::Zero());
    frameGrad[i].resize(2*t, Eigen::Matrix3d::Zero());
  }
  //index (pos, dof)
  Eigen::VectorXd eta(t);
  Eigen::VectorXd omega1(t);
  for(int i=0;i<t;i++)
  {
    eta(i)=sr.getEta(i);
    omega1(i)=sr.getOmega1(i);
  }
  Eigen::VectorXd etaPrime = etaPrimeMatrix*eta;
  
  std::vector<std::pair<int,int>> coeffs={{0,0},{1,1},{1,0}};
  for(int currentIndex =0; currentIndex <t; currentIndex++)
  {
    RibbonGeometrie geo;
    geo.Place(pos,frame);
    geo.Setup(omega1(currentIndex),eta(currentIndex));
    double l=sr.getLength(currentIndex);
    auto [dPosdEta,dFramedEta] = geo.dPosFramedEta(l);
    auto [dPosdOmega1,dFramedOmega1] = geo.dPosFramedOmega1(l);
    auto [PosHessLoc,FrameHessLoc] = geo.hessianPosFrame(l);
    posGrad[currentIndex][1+2*currentIndex] = dPosdEta;
    posGrad[currentIndex][2*currentIndex] = dPosdOmega1;
    frameGrad[currentIndex][1+2*currentIndex] = dFramedEta;
    frameGrad[currentIndex][2*currentIndex] = dFramedOmega1;
    
    Eigen::Matrix<Eigen::Vector3d, 3, 1> dPosdPos=geo.dPosdPos(l);
    Eigen::Matrix<Eigen::Matrix3d, 3, 1> dPosdFrame=geo.dPosdFrame(l);
    Eigen::Matrix<Eigen::Matrix3d, 3, 3> dFramedFrame=geo.dFramedFrame(l);
    Eigen::Matrix<Eigen::Matrix3d, 3, 3> d2FramedFramedOmega1=geo.d2FramedFramedOmega1(l);
    Eigen::Matrix<Eigen::Matrix3d, 3, 3> d2FramedFramedEta=geo.d2FramedFramedEta(l);
    Eigen::Matrix<Eigen::Matrix3d, 3, 1> d2PosdFramedOmega1=geo.d2PosdFramedOmega1(l);
    Eigen::Matrix<Eigen::Matrix3d, 3, 1> d2PosdFramedEta=geo.d2PosdFramedEta(l);
    for(int j=0;j< currentIndex;j++)
    {
      posGrad[currentIndex][1+2*j]+=compDerivfg(posGrad[currentIndex -1][1+2*j],dPosdPos);
      posGrad[currentIndex][1+2*j]+=compDerivfg(frameGrad[currentIndex -1][1+2*j],dPosdFrame);
      posGrad[currentIndex][2*j]+=compDerivfg(posGrad[currentIndex -1][2*j],dPosdPos);
      posGrad[currentIndex][2*j]+=compDerivfg(frameGrad[currentIndex -1][2*j],dPosdFrame);
      frameGrad[currentIndex][1+2*j]+=compDerivfg(frameGrad[currentIndex -1][1+2*j],dFramedFrame);
      frameGrad[currentIndex][2*j]+=compDerivfg(frameGrad[currentIndex -1][2*j],dFramedFrame);
    }
    
    for(auto [i,j]: coeffs)
    {
      GetWithDef(posHessian,listTriMat(currentIndex,2*currentIndex+i,2*currentIndex+j),v3_0) += extractDerivation(PosHessLoc,i,j);
      GetWithDef(frameHessian,listTriMat(currentIndex,2*currentIndex+i,2*currentIndex+j),m33_0) += extractDerivation(FrameHessLoc,i,j);
    }
    for(int j=0; j < 2*currentIndex; j++)
    {
      for(int k=0; k <= j; k++)
      {
        GetWithDef(posHessian,listTriMat(currentIndex,j,k),v3_0)+=compDerivfg(GetWithDef(posHessian,listTriMat(currentIndex-1,j,k),v3_0),dPosdPos);
        GetWithDef(posHessian,listTriMat(currentIndex,j,k),v3_0)+=compDerivfg(GetWithDef(frameHessian,listTriMat(currentIndex-1,j,k),m33_0),dPosdFrame);
          
        GetWithDef(frameHessian,listTriMat(currentIndex,j,k),m33_0)+=compDerivfg(GetWithDef(frameHessian,listTriMat(currentIndex-1,j,k),m33_0),dFramedFrame);
      }
    }
      
    for(int j=0; j < currentIndex; j++)
    {
      
      GetWithDef(posHessian,listTriMat(currentIndex,2*currentIndex,   2*j),  v3_0)+=compDerivfg(frameGrad[currentIndex-1][2*j],  d2PosdFramedOmega1);
      GetWithDef(posHessian,listTriMat(currentIndex,2*currentIndex+1, 2*j+1),v3_0)+=compDerivfg(frameGrad[currentIndex-1][2*j+1],d2PosdFramedEta);
      GetWithDef(posHessian,listTriMat(currentIndex,2*currentIndex+1, 2*j),  v3_0)+=compDerivfg(frameGrad[currentIndex-1][2*j],  d2PosdFramedEta);
      GetWithDef(posHessian,listTriMat(currentIndex,2*currentIndex,   2*j+1),v3_0)+=compDerivfg(frameGrad[currentIndex-1][2*j+1],d2PosdFramedOmega1);
      
      GetWithDef(frameHessian,listTriMat(currentIndex,2*currentIndex,2*j),m33_0)+=compDerivfg(frameGrad[currentIndex-1][2*j],d2FramedFramedOmega1);
      GetWithDef(frameHessian,listTriMat(currentIndex,2*currentIndex+1,2*j+1),m33_0)+=compDerivfg(frameGrad[currentIndex-1][2*j+1],d2FramedFramedEta);
      GetWithDef(frameHessian,listTriMat(currentIndex,2*currentIndex+1,2*j),m33_0)+=compDerivfg(frameGrad[currentIndex-1][2*j],d2FramedFramedEta);
      GetWithDef(frameHessian,listTriMat(currentIndex,2*currentIndex,2*j+1),m33_0)+=compDerivfg(frameGrad[currentIndex-1][2*j+1],d2FramedFramedOmega1);
    }
    std::tie(pos,frame)=geo.PosFrame(l);
  }
  Eigen::MatrixXd res(2*t,2*t);
  res.setZero();
  Eigen::Vector3d posDiff=(tPos-pos);
  Eigen::Matrix3d frameDiff=(tFrame-frame);
  for(int k=0;k< 2*t;k++)
  {
    for(int l=0;l< 2*t;l++)
    {
      res(k,l)=2.0*(posGrad[t-1][k].dot(posGrad[t-1][l])-posDiff.dot(posHessian[listTriMat(t-1,k,l)]));
      res(k,l)+=2.0*(contractGeneralDotP(frameGrad[t-1][k],frameGrad[t-1][l])-contractGeneralDotP( frameDiff,frameHessian[listTriMat(t-1,k,l)]));
    }
  }
  return res;
}


Eigen::VectorXd RibbonPhysique::etaPrimes()
{
  int n = sr.getNbSegments();
  Eigen::VectorXd eta(n);
  for(int i=0;i<n;i++)
  {
    eta(i)=sr.getEta(i);
  }
  Eigen::VectorXd etaPrime = etaPrimeMatrix*eta;
  return etaPrime;
}

void RibbonPhysique::precomputeStuff()
{
  int n = sr.getNbSegments();
  if(n<2)throw std::string("Two segments at least are needed");
  etaPrimeMatrix=Eigen::SparseMatrix<double>(n,n);
  using T=Eigen::Triplet<double>;
  std::vector<T> terms;
  double sl;
  for(int i=0;i<n-1;i++)
  {
    sl=sr.getLength(i);
    terms.push_back(T(i,1+i,1./sl));
    terms.push_back(T(i,0+i,-1./sl));
  }
  sl=sr.getLength(n-1);
  terms.push_back(T(n-1,n-1,-1./sl));
  
  etaPrimeMatrix.setFromTriplets(terms.begin(),terms.end());
}

std::vector<Eigen::Triplet<double>> RibbonPhysique::etaPrimesGradient()
{
  int n = sr.getNbSegments();
  using T=Eigen::Triplet<double>;
  std::vector<T> terms;
  double sl;
  for(int i=0;i<n-1;i++)
  {
    sl=sr.getLength(i);
    terms.push_back(T(i,2*(i+1)+1,1./sl));
    terms.push_back(T(i,2*(i)+1,-1./sl));
  }
  sl=sr.getLength(n-1);
  terms.push_back(T(n-1,2*(n-1)+1,-1./sl));
  
  return terms;
}

std::vector<Eigen::Triplet<double> > RibbonPhysique::wholeetaPrimesGradientLine(int line)
{
  int n = sr.getNbSegments();
  using T=Eigen::Triplet<double>;
  std::vector<T> terms;
  if(line<n-1)
  {
    double sl=sr.getLength(line);
    terms.push_back(T(line,2*(line+1)+1,1./sl));
    terms.push_back(T(line,2*line+1,-1./sl));
  }
  else
  {
    double sl=sr.getLength(n-1);    
    terms.push_back(T(n-1,2*(n-1)+1,-1./sl));
  }
  return terms;
}

int RibbonPhysique::getNbVars()
{
  return sr.getNbSegments()*2;
}

