/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef RIBBONPHYSIQUE_H
#define RIBBONPHYSIQUE_H

#include "Ribbon.hpp"
#include <vector>
#include <map>
#include <Eigen/Sparse>

class RibbonPhysique
{
public:
    void set(Ribbon sr);
    Ribbon get();

    /**
     * \note if set has been called, setup is useless
     */
    void setup(Eigen::Vector3d posOr, Eigen::Matrix3d frameOr, double width, double D, double areaDensity, std::vector<double> segmentsLength);
    void setPoint(Eigen::VectorXd x);
    void setGravity(Eigen::Vector3d grav);
    Eigen::Vector3d getGravity();
    Eigen::VectorXd getPoint();
    Eigen::VectorXd getUpperBound();
    Eigen::VectorXd getLowerBound();
    
    double getEnergy();
    Eigen::VectorXd getEnergyGradient();
    Eigen::MatrixXd getEnergyHessian();
    
    double targetEnergy(Eigen::Vector3d tPos, Eigen::Matrix3d tFrame);
    Eigen::VectorXd targetEnergyGradient(Eigen::Vector3d tPos, Eigen::Matrix3d tFrame);
    Eigen::MatrixXd targetEnergyHessian(Eigen::Vector3d tPos, Eigen::Matrix3d tFrame);
    
    Eigen::VectorXd etaPrimes();
    std::vector<Eigen::Triplet<double>> etaPrimesGradient();
    std::vector<Eigen::Triplet<double>> wholeetaPrimesGradientLine(int line);
    
    int getNbVars();

protected:
    Eigen::Matrix3d endFrame = Eigen::Matrix3d::Identity();
    Eigen::Vector3d endPos = Eigen::Vector3d::Zero();
    Ribbon sr;
    Eigen::Vector3d G = Eigen::Vector3d(0., 0., 10.);
    
    void precomputeStuff();
    Eigen::SparseMatrix<double> etaPrimeMatrix;
};


#endif // RIBBONPHYSIQUE_H
