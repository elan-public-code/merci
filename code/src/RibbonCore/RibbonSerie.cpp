/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "RibbonSerie.hpp"
#include "Log/Logger.hpp"

Eigen::Matrix3d RibbonSerie::skewOf(Eigen::Vector3d w)
{
  Eigen::Matrix3d res;
  res << 0,    -w[2], w[1],
      w[2],  0,    -w[0],
      -w[1], w[0], 0;
  return res;
}

void RibbonSerie::Place(Eigen::Vector3d pos, Eigen::Matrix3d frame)
{
  called.clear();
  posOr = pos;
  frameOr = frame;
}

void RibbonSerie::Setup(double omega1, double eta)
{
  if (false) {
    LoggerMsg message("Setup");
    message.addChild("omega1", std::to_string(omega1));
    message.addChild("eta", std::to_string(eta));
    getLogger()->Write(LoggerLevel::INFORMATION, "Serie", message);
  }
  called.clear();
  this->omega1 = omega1;
  this->eta = eta;
  lambda0Sq = skewOf(Eigen::Vector3d(omega1, 0.0, eta * omega1));
  lambda0 = Eigen::Vector3d(omega1, 0.0, eta * omega1);
  lambda = lambda0.lpNorm<Eigen::Infinity>();
}

void RibbonSerie::SetRibbonWidth(double w)
{
  called.clear();
  ribbonWidth = w;
}

double RibbonSerie::estimRemainderOfTheSerie(int order, double s)
{
  assert(s > 0);
  assert(order > 3);
  return std::exp(C(2 * s)) / pow(2., order - 1);
}

double RibbonSerie::factorial(int k)
{
  assert(k >= 1);
  double res = 1.;
  for (int i = 2; i <= k; i++)
    res *= static_cast<double>(i);
  return res;
}

int RibbonSerie::estimNeededOrder(double prec)
{
  int a = static_cast<int>(std::log2(prec) - 0.95);
  assert(a < 0);
  return 52 - a;
}

int RibbonSerie::estimNeededOrder()
{
  return (52 * 3 / 2);
}

double RibbonSerie::C(double s)
{
  return 2.*std::abs(s) * lambda;
}

double RibbonSerie::estimSMax()
{
  double T_2 = (52. / 2.) * std::log(2) / 2.;
  double l0 = lambda0.lpNorm<Eigen::Infinity>();
  l0 = std::max(1., l0);

  if (std::isnan(l0)) {
    return std::numeric_limits<double>::infinity();
  }

  double res = std::numeric_limits<double>::infinity();
  //TODO search a better solution
  if (l0 < T_2) { //Smax>1
    res = std::cbrt(T_2 / l0);
  } else { //Smax<1
    res = T_2 / l0;
  }

  if (res < 1.e-8) {
    LoggerMsg message("Very small smax");
    message.addChild("smax", std::to_string(res));
    message.addChild("l0", std::to_string(l0));
    getLogger()->Write(LoggerLevel::WARNING, "Serie", message);
    res = std::numeric_limits<double>::infinity();
  }

  return res / 2.;
}

void RibbonSerie::computeSeriePreTerms()
{
  computeSeriePreTerms(estimNeededOrder());
}

void RibbonSerie::computeSeriePreTerms(int order)
{
  if (called["computeSeriePreTerms"])return;
  assert(order > 3);
  FramePreTerms.clear();
  FramePreTerms.push_back(Eigen::Matrix3d::Identity());
  FramePreTerms.push_back(lambda0Sq);
  for (int n = 2; n <= order; n++) {
    FramePreTerms.push_back((1. / n) *(FramePreTerms[n - 1]*lambda0Sq));
  }
  PosPreTerms.clear();
  PosPreTerms.push_back(Eigen::Vector3d(NAN, NAN, NAN));
  for (int n = 1; n <= order; n++) {
    PosPreTerms.push_back((1. / n) * (
                            FramePreTerms[n - 1].col(2)));
  }
  called["computeSeriePreTerms"] = true;
}

Eigen::Matrix3d RibbonSerie::Frame_s(double s)
{
  assert(s <= estimSMax());
  return Frame(s);
}

Eigen::Vector3d RibbonSerie::Pos_s(double s)
{
  assert(s <= estimSMax());
  return Pos(s);
}

Eigen::Matrix3d RibbonSerie::Frame(double s)
{
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  int order = FramePreTerms.size();
  computeSPowers(s);
  Eigen::Matrix3d res = FramePreTerms[0];
  for (int i = 1; i < order; i++)
    res += spowers[i] * FramePreTerms[i];
  return frameOr * res;
}

Eigen::Matrix3d RibbonSerie::PreFrame(double s)
{
    if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  int order = FramePreTerms.size();
  computeSPowers(s);
  Eigen::Matrix3d res = FramePreTerms[0];
  for (int i = 1; i < order; i++)
    res += spowers[i] * FramePreTerms[i];
  return res;
}


Eigen::Vector3d RibbonSerie::Pos(double s)
{
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  int order = PosPreTerms.size();
  computeSPowers(s);
  Eigen::Vector3d res(0., 0., 0.);
  for (int i = 1; i < order; i++)
    res += spowers[i] * PosPreTerms[i];
  res = frameOr * res;
  res += posOr;
  return res;
}

Eigen::Vector3d RibbonSerie::PreTranslation(double s)
{
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  int order = PosPreTerms.size();
  computeSPowers(s);
  Eigen::Vector3d res(0., 0., 0.);
  for (int i = 1; i < order; i++)
    res += spowers[i] * PosPreTerms[i];
  return res;
}

std::pair<Eigen::Vector3d, Eigen::Matrix3d> RibbonSerie::PosFrame(double s)
{
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  int order = FramePreTerms.size();
  computeSPowers(s);
  Eigen::Vector3d resPos(0., 0., 0.);
  for (int i = 1; i < order; i++)
    resPos += spowers[i] * PosPreTerms[i];
  resPos = frameOr * resPos;
  resPos += posOr;
  Eigen::Matrix3d resFrame = FramePreTerms[0];
  for (int i = 1; i < order; i++) {
    resFrame += spowers[i] * FramePreTerms[i];
  }
  return std::make_pair(resPos, frameOr * resFrame);
}

void RibbonSerie::computeDerivationsPreTerms()
{
  if (called["computeDerivationsPreTerms"])return;
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  dLambda0domega1 << 1., 0., eta;
  dLambda0deta << 0., 0., omega1;

  dLambda0domega1Sq = skewOf(dLambda0domega1);
  dLambda0detaSq = skewOf(dLambda0deta);

  int order = FramePreTerms.size();
  assert(order > 0);

  //Derivation of the frame
  {
    dFramePreTermsdomega1.clear();
    dFramePreTermsdeta.clear();
    Eigen::Matrix3d zero;
    zero.setZero();
    //order 0
    dFramePreTermsdomega1.push_back(zero);
    dFramePreTermsdeta.push_back(zero);
    //order 1
    dFramePreTermsdomega1.push_back(dLambda0domega1Sq);
    dFramePreTermsdeta.push_back(dLambda0detaSq);
    //order n
    for (int n = 2; n < order; n++) {
      dFramePreTermsdomega1.push_back((1. / static_cast<double>(n)) * (
                                   FramePreTerms[n - 1]*dLambda0domega1Sq
                                   + dFramePreTermsdomega1[n - 1]*lambda0Sq));
      dFramePreTermsdeta.push_back((1. / static_cast<double>(n)) * (
                                   FramePreTerms[n - 1]*dLambda0detaSq
                                   + dFramePreTermsdeta[n - 1]*lambda0Sq));
    }
  }

  //Derivation of the position
  {
    dPosPreTermsdomega1.clear();
    dPosPreTermsdeta.clear();

    Eigen::Vector3d zero;
    zero.setZero();
    Eigen::Vector3d vnan(NAN, NAN, NAN);

    //order 0
    dPosPreTermsdomega1.push_back(vnan);
    dPosPreTermsdeta.push_back(vnan);
    //order 1
    dPosPreTermsdomega1.push_back(zero);
    dPosPreTermsdeta.push_back(zero);
    //order n
    for (int n = 2; n < order; n++) {
      dPosPreTermsdomega1.push_back((1. / static_cast<double>(n))*dFramePreTermsdomega1[n - 1].col(2));
      dPosPreTermsdeta.push_back((1. / static_cast<double>(n))*dFramePreTermsdeta[n - 1].col(2));

    }
  }
  called["computeDerivationsPreTerms"] = true;
}

void RibbonSerie::computeDerivationsPreTermsSecondOrder()
{
  if (called["computeDerivationsPreTermsSecondOrder"])return;
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  dLambda0dOmega1Eta << 0., 0., 1.;
  dLambda0dEtaOmega1 << 0., 0., 1.;
  dLambda0dOmega1EtaSq = skewOf(dLambda0dOmega1Eta);
  dLambda0dEtaOmega1Sq = skewOf(dLambda0dEtaOmega1);

  int order = FramePreTerms.size();
  assert(order > 0);

  //Derivation of the frame
  {
    dFramePreTermsdOmega1Omega1.clear();
    dFramePreTermsdEtaOmega1.clear();
    dFramePreTermsdOmega1Eta.clear();
    dFramePreTermsdEtaEta.clear();
    Eigen::Matrix3d zero;
    zero.setZero();
    //order 0
    dFramePreTermsdOmega1Omega1.push_back(zero);
    dFramePreTermsdEtaOmega1.push_back(zero);
    dFramePreTermsdOmega1Eta.push_back(zero);
    dFramePreTermsdEtaEta.push_back(zero);
    //order 1
    dFramePreTermsdOmega1Omega1.push_back(zero);
    dFramePreTermsdEtaOmega1.push_back(dLambda0dEtaOmega1Sq);
    dFramePreTermsdOmega1Eta.push_back(dLambda0dOmega1EtaSq);
    dFramePreTermsdEtaEta.push_back(zero);
    //order n
    for (int n = 2; n < order; n++) {
      dFramePreTermsdOmega1Omega1.push_back((1. / static_cast<double>(n)) * (
                                    2.0*dFramePreTermsdomega1[n - 1]*dLambda0domega1Sq
                                    + dFramePreTermsdOmega1Omega1[n - 1]*lambda0Sq ));
      dFramePreTermsdEtaOmega1.push_back((1. / static_cast<double>(n)) * (
                                    dFramePreTermsdomega1[n - 1]*dLambda0detaSq
                                    + dFramePreTermsdeta[n - 1]*dLambda0domega1Sq
                                    + FramePreTerms[n - 1]*dLambda0dEtaOmega1Sq
                                    + dFramePreTermsdEtaOmega1[n - 1]*lambda0Sq ));
      dFramePreTermsdOmega1Eta.push_back((1. / static_cast<double>(n)) * (
                                    dFramePreTermsdomega1[n - 1]*dLambda0detaSq
                                    + dFramePreTermsdeta[n - 1]*dLambda0domega1Sq
                                    + FramePreTerms[n - 1]*dLambda0dEtaOmega1Sq
                                    + dFramePreTermsdEtaOmega1[n - 1]*lambda0Sq));
      dFramePreTermsdEtaEta.push_back((1. / static_cast<double>(n)) * (
                                    2.0*dFramePreTermsdeta[n - 1]*dLambda0detaSq
                                    + dFramePreTermsdEtaEta[n - 1]*lambda0Sq));
    }
  }

  //Derivation of the position
  {
    dPosPreTermsdOmega1Omega1.clear();
    dPosPreTermsdEtaOmega1.clear();
    dPosPreTermsdOmega1Eta.clear();
    dPosPreTermsdEtaEta.clear();
    
    Eigen::Vector3d zero;
    zero.setZero();
    Eigen::Vector3d vnan(NAN, NAN, NAN);

    //order 0
    dPosPreTermsdOmega1Omega1.push_back(vnan);
    dPosPreTermsdEtaOmega1.push_back(vnan);
    dPosPreTermsdOmega1Eta.push_back(vnan);
    dPosPreTermsdEtaEta.push_back(vnan);
    //order 1
    dPosPreTermsdOmega1Omega1.push_back(zero);
    dPosPreTermsdEtaOmega1.push_back(zero);
    dPosPreTermsdOmega1Eta.push_back(zero);
    dPosPreTermsdEtaEta.push_back(zero);
    //order n
    for (int n = 2; n < order; n++) {
      dPosPreTermsdOmega1Omega1.push_back((1. / static_cast<double>(n))*dFramePreTermsdOmega1Omega1[n - 1].col(2));
      dPosPreTermsdEtaOmega1.push_back((1. / static_cast<double>(n))*dFramePreTermsdEtaOmega1[n - 1].col(2));
      dPosPreTermsdOmega1Eta.push_back((1. / static_cast<double>(n))*dFramePreTermsdOmega1Eta[n - 1].col(2));
      dPosPreTermsdEtaEta.push_back((1. / static_cast<double>(n))*dFramePreTermsdEtaEta[n - 1].col(2));
    }
  }

  called["computeDerivationsPreTermsSecondOrder"] = true;
}

Eigen::Matrix3d RibbonSerie::dFramedOmega1(double s)
{
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  int order = dFramePreTermsdomega1.size();
  computeSPowers(s);
  Eigen::Matrix3d res;
  res.setZero();
  for (int i = 1; i < order; i++)
    res += spowers[i] * dFramePreTermsdomega1[i];
  res = frameOr * res;

  return res;
}

Eigen::Matrix3d RibbonSerie::dFramedEta(double s)
{
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  int order = dFramePreTermsdeta.size();
  computeSPowers(s);
  Eigen::Matrix3d res;
  res.setZero();
  for (int i = 1; i < order; i++)
    res += spowers[i] * dFramePreTermsdeta[i];
  res = frameOr * res;

  return res;
}


Eigen::Vector3d RibbonSerie::dPosdOmega1(double s)
{
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  int order = dPosPreTermsdomega1.size();
  computeSPowers(s);
  Eigen::Vector3d res(0., 0., 0.);
  for (int i = 1; i < order; i++)
    res += spowers[i] * dPosPreTermsdomega1[i];
  res = frameOr * res;

  return res;
}

Eigen::Vector3d RibbonSerie::dPosdEta(double s)
{
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  int order = dPosPreTermsdeta.size();
  computeSPowers(s);
  Eigen::Vector3d res(0., 0., 0.);
  for (int i = 1; i < order; i++)
    res += spowers[i] * dPosPreTermsdeta[i];
  res = frameOr * res;

  return res;
}

Eigen::Matrix<Eigen::Matrix2d, 3, 3> RibbonSerie::hessianFrame(double s)
{
  if (!called["computeDerivationsPreTermsSecondOrder"])computeDerivationsPreTermsSecondOrder();
  int order = dFramePreTermsdOmega1Omega1.size();
  computeSPowers(s);
  Eigen::Matrix3d resOmega1Omega1;
  Eigen::Matrix3d resOmega1Eta;
  Eigen::Matrix3d resEtaOmega1;
  Eigen::Matrix3d resEtaEta;
  resOmega1Omega1.setZero();
  resOmega1Eta.setZero();
  resEtaOmega1.setZero();
  resEtaEta.setZero();

  for (int i = 1; i < order; i++) {
    resOmega1Omega1 += spowers[i] * dFramePreTermsdOmega1Omega1[i];
    resOmega1Eta += spowers[i] * dFramePreTermsdOmega1Eta[i];
    resEtaOmega1 += spowers[i] * dFramePreTermsdEtaOmega1[i];
    resEtaEta += spowers[i] * dFramePreTermsdEtaEta[i];
  }
  resOmega1Omega1 = frameOr * resOmega1Omega1;
  resOmega1Eta = frameOr * resOmega1Eta;
  resEtaOmega1 = frameOr * resEtaOmega1;
  resEtaEta = frameOr * resEtaEta;
  Eigen::Matrix<Eigen::Matrix2d, 3, 3> res;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      res(i, j)(0, 0) = resOmega1Omega1(i, j);
      res(i, j)(0, 1) = resOmega1Eta(i, j);
      res(i, j)(1, 0) = resEtaOmega1(i, j);
      res(i, j)(1, 1) = resEtaEta(i, j);
    }
  }
  return res;
}

Eigen::Matrix<Eigen::Matrix2d, 3, 1> RibbonSerie::hessianPos(double s)
{
  if (!called["computeDerivationsPreTermsSecondOrder"])computeDerivationsPreTermsSecondOrder();
  int order = dPosPreTermsdOmega1Omega1.size();
  computeSPowers(s);
  Eigen::Vector3d resOmega1Omega1;
  Eigen::Vector3d resOmega1Eta;
  Eigen::Vector3d resEtaOmega1;
  Eigen::Vector3d resEtaEta;
  resOmega1Omega1.setZero();
  resOmega1Eta.setZero();
  resEtaOmega1.setZero();
  resEtaEta.setZero();

  for (int i = 1; i < order; i++) {
    resOmega1Omega1 += spowers[i] * dPosPreTermsdOmega1Omega1[i];
    resOmega1Eta += spowers[i] * dPosPreTermsdOmega1Eta[i];
    resEtaOmega1 += spowers[i] * dPosPreTermsdEtaOmega1[i];
    resEtaEta += spowers[i] * dPosPreTermsdEtaEta[i];
  }
  resOmega1Omega1 = frameOr * resOmega1Omega1;
  resOmega1Eta = frameOr * resOmega1Eta;
  resEtaOmega1 = frameOr * resEtaOmega1;
  resEtaEta = frameOr * resEtaEta;
  Eigen::Matrix<Eigen::Matrix2d, 3, 1> res;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      res(i, j)(0, 0) = resOmega1Omega1(i, j);
      res(i, j)(0, 1) = resOmega1Eta(i, j);
      res(i, j)(1, 0) = resEtaOmega1(i, j);
      res(i, j)(1, 1) = resEtaEta(i, j);
    }
  }
  return res;
}

bool RibbonSerie::checkDerivations()
{
  std::cout << "=== test SuperRibbonGeometrieSerie ===" << std::endl;
  double omega1Or = omega1;
  double etaOr = eta;

  auto initvars = [&](bool varomega, double offset) {
    omega1 = omega1Or;
    eta = etaOr;
    if (varomega) {
      omega1 += offset;
    } else {
      eta += offset;
    }
    Setup(omega1, eta);
  };
  auto Test = [&](bool varomega, double offset)->std::vector<double> {
    std::vector<Eigen::Matrix3d> estimV;

    initvars(varomega, offset);
    computeSeriePreTerms(10);
    estimV = FramePreTerms;
    initvars(varomega, -offset);
    computeSeriePreTerms(10);
    size_t t = estimV.size();
    for (size_t i = 0; i < t; i++)
      estimV[i] -= FramePreTerms[i];
    double varlen = 2.*offset;
    for (size_t i = 0; i < t; i++)
      estimV[i] = (1. / varlen) * estimV[i].eval();
    Setup(omega1Or, etaOr);
    computeSeriePreTerms(10);
    computeDerivationsPreTerms();
    std::vector<double> res(t);
    std::vector<Eigen::Matrix3d> &dFrameSeriedRef = std::ref(dFramePreTermsdomega1);
    if (varomega)
    {
      dFrameSeriedRef = std::ref(dFramePreTermsdomega1);
    } else
    {
      dFrameSeriedRef = std::ref(dFramePreTermsdeta);
    }
    for (size_t i = 0; i < t; i++)
    {
      res[i] = (estimV[i] - dFrameSeriedRef[i]).lpNorm<Eigen::Infinity>();
    }
    return res;
  };
  auto Validate = [&](std::vector<double> &V, double tolerance)->bool {
    for (auto x : V)if (x > tolerance)return false;
    return true;
  };
  auto res = Test(true, 0.5e-5);
  if (!Validate(res, 1.e-10))return false;
  std::cout << "[1/2]PASSED" << std::endl;
  res = Test(false, 0.5e-5);
  if (!Validate(res, 1.e-10))return false;
  std::cout << "[2/2]PASSED" << std::endl;
  return true;
}

void RibbonSerie::computePreAltitudeEnergyPreTerms()
{
  if (called["computePreAltitudeEnergyPreTerms"])return;
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  //Approx carotte
  called["computePreAltitudeEnergyPreTerms"] = true;
}

void RibbonSerie::computePreAltitudeEnergyPreTermsDerivations()
{
  if (called["computePreAltitudeEnergyPreTermsDerivations"])return;
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  //Approx carotte
  called["computePreAltitudeEnergyPreTermsDerivations"] = true;
}



void RibbonSerie::computePreAltitudeEnergyPreTermsDerivationsSecondOrder()
{
  if (called["computePreAltitudeEnergyPreTermsDerivationsSecondOrder"])return;
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  if (!called["computeDerivationsPreTermsSecondOrder"])computeDerivationsPreTermsSecondOrder();
  if (!called["computePreAltitudeEnergyPreTermsDerivations"])computePreAltitudeEnergyPreTermsDerivations();
  //Approx carotte
  called["computePreAltitudeEnergyPreTermsDerivationsSecondOrder"] = true;
}


Eigen::Vector3d RibbonSerie::preAltitudeEnergy(double s)
{
  //Approx carotte
  return ribbonWidth*s*posOr;
}

Eigen::Vector3d RibbonSerie::preAltitudeRotationalEnergy(double s)
{
  //Approx carotte
  return Eigen::Vector3d::Zero();
}


Eigen::Matrix<double, 3, 2> RibbonSerie::gradPreAltitudeEnergy(double s)
{
  //Approx carotte
  return Eigen::Matrix<double, 3, 2>::Zero();
}

Eigen::Matrix<Eigen::Vector3d, 3, 1> RibbonSerie::dPreAltitudeEnergydPos(double s)
{
  Eigen::Matrix<Eigen::Vector3d, 3, 1> res;
  res << Eigen::Vector3d(s * ribbonWidth, 0., 0.), Eigen::Vector3d(0., s * ribbonWidth, 0.), Eigen::Vector3d(0., 0., s * ribbonWidth);
  return res;
}

Eigen::Matrix<Eigen::Matrix3d, 3, 1> RibbonSerie::dPreAltitudeEnergydFrame(double s)
{
  //Approx carotte
  Eigen::Matrix<Eigen::Matrix3d, 3, 1> res;
  for(int i=0;i<3;i++)res[i].setZero();
  return res;
}

Eigen::Matrix<Eigen::Matrix<Eigen::Vector2d, 3, 3>, 3, 1> RibbonSerie::d2PreAltitudeEnergydFramedabnm(double s)
{
  Eigen::Matrix<Eigen::Matrix<Eigen::Vector2d, 3, 3>, 3, 1> res;
  for(int i=0;i<3;i++)res[i].setConstant(Eigen::Vector2d(0.,0.));
  return res;
}


Eigen::Matrix<Eigen::Matrix2d, 3, 1> RibbonSerie::hessianPreAltitudeEnergy(double s)
{
  Eigen::Matrix<Eigen::Matrix2d, 3, 1> res;
  for(int i=0;i<3;i++)res[i].setZero();
  return res;
}

Eigen::Matrix<Eigen::Vector3d, 3, 1> RibbonSerie::dPosdPos(double s)
{
  Eigen::Matrix<Eigen::Vector3d, 3, 1>res;
  res << Eigen::Vector3d(1., 0., 0.), Eigen::Vector3d(0., 1., 0.), Eigen::Vector3d(0., 0., 1.);
  return res;
}

Eigen::Matrix<Eigen::Matrix3d, 3, 3> RibbonSerie::dFramedFrame(double s)
{
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  int order = FramePreTerms.size();
  computeSPowers(s);
  Eigen::Matrix3d K = FramePreTerms[0];
  for (int i = 1; i < order; i++)
    K += spowers[i] * FramePreTerms[i];
  Eigen::Matrix<Eigen::Matrix3d, 3, 3> res;
  res.setConstant(Eigen::Matrix3d::Zero());
  Eigen::Matrix3d temp;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      temp.setZero();
      temp(i, j) = 1.;
      temp = temp * K;
      for (int a = 0; a < 3; a++) {
        for (int b = 0; b < 3; b++) {
          res(a, b)(i, j) = temp(a, b);
        }
      }
    }
  }
  return res;
}

Eigen::Matrix<Eigen::Matrix3d, 3, 1> RibbonSerie::dPosdFrame(double s)
{
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  int order = PosPreTerms.size();
  computeSPowers(s);
  Eigen::Vector3d K(0., 0., 0.);
  for (int i = 1; i < order; i++)
    K += spowers[i] * PosPreTerms[i];
  Eigen::Matrix<Eigen::Matrix3d, 3, 1> res;
  Eigen::Matrix3d temp;
  Eigen::Vector3d vtemp;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      temp.setZero();
      temp(i, j) = 1.;
      vtemp = temp * K;
      for (int b = 0; b < 3; b++) {
        res[b](i, j) = vtemp(b);
      }
    }
  }
  return res;
}

Eigen::Matrix<Eigen::Matrix<Eigen::Vector2d, 3, 3>, 3, 3> RibbonSerie::d2FramedFramedOmega1Eta(double s)
{
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  int order = PosPreTerms.size();
  computeSPowers(s);
  Eigen::Matrix3d Komega1 = Eigen::Matrix3d::Zero();
  Eigen::Matrix3d Keta = Eigen::Matrix3d::Zero();
  for (int i = 1; i < order; i++) {
    Komega1 += spowers[i] * dFramePreTermsdomega1[i];
    Keta += spowers[i] * dFramePreTermsdeta[i];
  }

  Eigen::Matrix<Eigen::Matrix<Eigen::Vector2d, 3, 3>, 3, 3> res;
  Eigen::Matrix<Eigen::Vector2d, 3, 3> tmp1;
  tmp1.setConstant(Eigen::Vector2d(0., 0.));
  res.setConstant(tmp1);
  Eigen::Matrix3d tmpomega1;
  Eigen::Matrix3d tmpeta;
  Eigen::Matrix3d temp;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      temp.setZero();
      temp(i, j) = 1.;
      tmpomega1 = temp * Komega1;
      tmpeta = temp * Keta;
      for (int a = 0; a < 3; a++) {
        for (int b = 0; b < 3; b++) {
          res(a, b)(i, j)[0] = tmpomega1(a, b);
          res(a, b)(i, j)[1] = tmpeta(a, b);
        }
      }
    }
  }
  return res;
}

Eigen::Matrix<Eigen::Matrix<Eigen::Vector2d, 3, 3>, 3, 1> RibbonSerie::d2PosdFramedOmega1Eta(double s)
{
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  int order = PosPreTerms.size();
  computeSPowers(s);
  Eigen::Vector3d Komega1 = Eigen::Vector3d::Zero();
  Eigen::Vector3d Keta = Eigen::Vector3d::Zero();
  for (int i = 1; i < order; i++) {
    Komega1 += spowers[i] * dPosPreTermsdomega1[i];
    Keta += spowers[i] * dPosPreTermsdeta[i];
  }

  Eigen::Matrix<Eigen::Matrix<Eigen::Vector2d, 3, 3>, 3, 1> res;
  Eigen::Matrix<Eigen::Vector2d, 3, 3> tmp1;
  tmp1.setConstant(Eigen::Vector2d(0., 0.));
  res.setConstant(tmp1);
  Eigen::Vector3d tmpomega1;
  Eigen::Vector3d tmpeta;
  Eigen::Matrix3d temp;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      temp.setZero();
      temp(i, j) = 1.;
      tmpomega1 = temp * Komega1;
      tmpeta = temp * Keta;
      for (int b = 0; b < 3; b++) {
        res(b)(i, j)[0] = tmpomega1(b);
        res(b)(i, j)[1] = tmpeta(b);
      }
    }
  }
  return res;
}




void RibbonSerie::computeSPowers(double s)
{
  int order = FramePreTerms.size();
  assert(order > 0);
  if ((PosPreTerms.size() == spowers.size()) && (sPowersCurrentS == s))
    return;
  spowers.clear();
  spowers.resize(order);
  spowers[0] = 1;
  spowers[1] = s;
  for (int i = 2; i < order; i++) {
    if (i % 2)
      spowers[i] = spowers[i / 2] * spowers[i / 2 + 1];
    else
      spowers[i] = spowers[i / 2] * spowers[i / 2];
  }
}
