/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef RIBBON_SERIE_H
#define RIBBON_SERIE_H

#include <Eigen/Dense>
#include <vector>
#include <utility>
#include <iostream>
#include <map>

/**
 * \file RibbonSerie.hpp
 * This file implement the serie to represent the geometrie of the ribbon.
 * It is based on the Ph.D. of Romain Casati, chapter 4, and even it is adapted,
 * the theoritical basis remainds the same.
 */

class RibbonSerie
{
public:
    void Place(Eigen::Vector3d pos, Eigen::Matrix3d frame);
    void Setup(double omega1, double eta);
    void SetRibbonWidth(double w);

    /**
     * \brief Upper bound of the rest of the serie at order \c order
     */
    double estimRemainderOfTheSerie(int order, double s = 1.);

    /**
     * \brief Upper bound on the order of the serie to get the precision \c prec
     */
    int estimNeededOrder(double prec);

    /**
     * \brief Upper bound on the order of the serie to get a reasonnable precision
     */
    int estimNeededOrder();

    /**
     * \brief Upper bound on the maximal abscissa to use to avoid computation issues with the serie
     */
    double estimSMax();


    Eigen::Matrix3d Frame(double s);
    Eigen::Matrix3d PreFrame(double s);//Frame = FrameOr*PreFrame
    Eigen::Vector3d Pos(double s);
    Eigen::Vector3d PreTranslation(double s);//Pos = PosOr+FrameOr*PreTranslation

    Eigen::Matrix3d Frame_s(double s);
    Eigen::Vector3d Pos_s(double s);

    std::pair<Eigen::Vector3d, Eigen::Matrix3d> PosFrame(double s);

    Eigen::Vector3d preAltitudeEnergy(double s);
    Eigen::Vector3d preAltitudeRotationalEnergy(double s);//preAltitudeEnergy=surface*pos+frameOr*preAltitudeRotationalEnergy
    Eigen::Matrix<double, 3, 2> gradPreAltitudeEnergy(double s);
    Eigen::Matrix<Eigen::Vector3d, 3, 1> dPreAltitudeEnergydPos(double s);
    Eigen::Matrix<Eigen::Matrix3d, 3, 1> dPreAltitudeEnergydFrame(double s);

    Eigen::Matrix<Eigen::Matrix2d, 3, 1> hessianPreAltitudeEnergy(double s);
    Eigen::Matrix<Eigen::Matrix<Eigen::Vector2d, 3, 3>, 3, 1> d2PreAltitudeEnergydFramedabnm(double s);

    Eigen::Matrix<Eigen::Matrix3d, 3, 3> dFramedFrame(double s);
    Eigen::Matrix<Eigen::Matrix3d, 3, 1> dPosdFrame(double s);

    Eigen::Matrix3d dFramedOmega1(double s);
    Eigen::Matrix3d dFramedEta(double s);
    Eigen::Vector3d dPosdOmega1(double s);
    Eigen::Vector3d dPosdEta(double s);

    Eigen::Matrix<Eigen::Matrix2d, 3, 3> hessianFrame(double s);
    Eigen::Matrix<Eigen::Matrix2d, 3, 1> hessianPos(double s);
    Eigen::Matrix<Eigen::Matrix<Eigen::Vector2d, 3, 3>, 3, 3> d2FramedFramedOmega1Eta(double s);
    Eigen::Matrix<Eigen::Matrix<Eigen::Vector2d, 3, 3>, 3, 1> d2PosdFramedOmega1Eta(double s);

    Eigen::Matrix<Eigen::Vector3d, 3, 1> dPosdPos(double s);
    bool checkDerivations();
    void computeSeriePreTerms(int order);
private:
    std::map<std::string, bool> called;
    void computeSeriePreTerms();
    void computeDerivationsPreTerms();
    void computePreAltitudeEnergySerieDerivations();
    void computePreAltitudeEnergyPreTerms();
    void computePreAltitudeEnergyPreTermsDerivations();
    void computePreAltitudeEnergyPreTermsDerivationsSecondOrder();

    void computeDerivationsPreTermsSecondOrder();

    double factorial(int k);

    double C(double s);
    Eigen::Matrix3d skewOf(Eigen::Vector3d);
    Eigen::Vector3d posOr;
    Eigen::Matrix3d frameOr;
    Eigen::Matrix3d lambda0Sq;
    Eigen::Vector3d lambda0;
    double lambda;

    double omega1;                                      // (A, B)
    double eta;                                         // (N, M)
    double ribbonWidth;

    std::vector<Eigen::Matrix3d> FramePreTerms;
    std::vector<Eigen::Vector3d> PosPreTerms;

    // Derivations

    Eigen::Vector3d dLambda0domega1;
    Eigen::Vector3d dLambda0deta;
    Eigen::Matrix3d dLambda0domega1Sq;
    Eigen::Matrix3d dLambda0detaSq;

    
    std::vector<Eigen::Matrix3d> dFramePreTermsdomega1;
    std::vector<Eigen::Matrix3d> dFramePreTermsdeta;
    std::vector<Eigen::Vector3d> dPosPreTermsdomega1;
    std::vector<Eigen::Vector3d> dPosPreTermsdeta;

    //Second order derivations

    Eigen::Vector3d dLambda0dOmega1Eta;
    Eigen::Vector3d dLambda0dEtaOmega1;
    Eigen::Matrix3d dLambda0dOmega1EtaSq;
    Eigen::Matrix3d dLambda0dEtaOmega1Sq;

    std::vector<Eigen::Matrix3d> dFramePreTermsdOmega1Omega1;
    std::vector<Eigen::Matrix3d> dFramePreTermsdEtaOmega1;
    std::vector<Eigen::Matrix3d> dFramePreTermsdOmega1Eta;
    std::vector<Eigen::Matrix3d> dFramePreTermsdEtaEta;
    std::vector<Eigen::Vector3d> dPosPreTermsdOmega1Omega1;
    std::vector<Eigen::Vector3d> dPosPreTermsdEtaOmega1;
    std::vector<Eigen::Vector3d> dPosPreTermsdOmega1Eta;
    std::vector<Eigen::Vector3d> dPosPreTermsdEtaEta;

    std::vector<double> spowers;
    double sPowersCurrentS = 0.;
    void computeSPowers(double s);
};

#endif // RIBBON_SERIE_H

