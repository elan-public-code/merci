/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "SimpleElasticEnergy.hpp"
#include "Log/Logger.hpp"
#include <string>

void SimpleElasticEnergy::FixPhysics(double D, double ribbonWidth, double ribbonLength, std::optional<double> poissonRatio)
{
  this->D = D;
  this->ribbonWidth = ribbonWidth;
  this->ribbonLength = ribbonLength;
  this->poissonRatio = poissonRatio;
}

void SimpleElasticEnergy::FixPt(double omega1, double eta, double etaPrime, std::optional<double> naturalOmega1)
{
  this->omega1 = omega1;
  this->eta = eta;
  this->etaPrime = etaPrime;
  this->naturalOmega1 = naturalOmega1;
}

double SimpleElasticEnergy::computeEnergy()
{
  const double L = ribbonLength;
  const double w = ribbonWidth;

  const double K = D * w / 2;
  
  const double t1 = w/(1.-etaPrime*0.5*w);
  const double t2 = etaPrime*t1*t1;
  const double t3 = etaPrime*t2*t1;
  
  const double tn = 1.+eta*eta;
  const double E = K*L*omega1*omega1*tn*tn*(t1+0.5*t2+(1./3.)*t3);
  
  if(K<0)
    std::cout<<"Problem K<0"<<std::endl;
  const double x = etaPrime * ribbonWidth / 2.;
  if (x < -1. || x > 1.) {
    LoggerMsg msg("Point out of constraints");
    msg.addChild("eta'", std::to_string(etaPrime));
    msg.addChild("w", std::to_string(ribbonWidth));
    getLogger()->Write(LoggerLevel::ERROR, "Model", msg);
  }
  
  if (poissonRatio && naturalOmega1) 
  {
    const double Enat =  - D * w * L * naturalOmega1.value() *omega1*(1.+poissonRatio.value()*eta*eta);
    return E+Enat;
  }
  return E;
}

Eigen::Vector3d SimpleElasticEnergy::computeGrad()
{
  Eigen::Vector3d res;
  const double L = ribbonLength;
  const double w = ribbonWidth;
  {
    const double t5 = D*w*L;
    const double t6 = eta*eta;
    const double t7 = t6+1.0;
    const double t8 = t7*t7;
    const double t12 = 1.0-etaPrime*w/2.0;
    const double t15 = w*w;
    const double t17 = t12*t12;
    const double t18 = 1/t17;
    const double t21 = etaPrime*etaPrime;
    const double t22 = t15*w;
    const double t25 = 1/t17/t12;
    const double t28 = w/t12+etaPrime*t15*t18/2.0+t21*t22*t25/3.0;
    const double t31 = omega1*omega1;
    const double t42 = t15*t15;
    const double t44 = t17*t17;
    res[0] = t5*omega1*t8*t28;
    res[1] = 2.0*t5*t31*t7*t28*eta;
    res[2] = t5*t31*t8*(t15*t18+7.0/6.0*etaPrime*t22*t25+t21*t42/t44/2.0)/2.0;
  }
  if (poissonRatio && naturalOmega1) 
  {
    const double t4 = D*w;
    const double t6 = eta*eta;
    res[0] += -t4*L*naturalOmega1.value()*(poissonRatio.value()*t6+1.0);
    res[1] += -2.0*t4*L*naturalOmega1.value()*omega1*eta*poissonRatio.value();
  }
  return res;
}

Eigen::Matrix3d SimpleElasticEnergy::computeHessian()
{
  Eigen::Matrix3d res;
  const double L = ribbonLength;
  const double w = ribbonWidth;  
  {
    const double t4 = D*w;
    const double t5 = eta*eta;
    const double t6 = t5+1.0;
    const double t7 = t6*t6;
    const double t11 = 1.0-etaPrime*w/2.0;
    const double t14 = w*w;
    const double t16 = t11*t11;
    const double t17 = 1/t16;
    const double t20 = etaPrime*etaPrime;
    const double t21 = t14*w;
    const double t24 = 1/t16/t11;
    const double t27 = w/t11+etaPrime*t14*t17/2.0+t20*t21*t24/3.0;
    const double t30 = t4*L;
    const double t35 = 4.0*t30*omega1*t6*t27*eta;
    const double t41 = t14*t14;
    const double t43 = t16*t16;
    const double t44 = 1/t43;
    const double t47 = t14*t17+7.0/6.0*etaPrime*t21*t24+t20*t41*t44/2.0;
    const double t49 = t30*omega1*t7*t47;
    const double t50 = omega1*omega1;
    const double t55 = t50*t6;
    const double t63 = 2.0*t30*t55*t47*eta;
    res(0,0) = t4*L*t7*t27;
    res(0,1) = t35;
    res(0,2) = t49;
    res(1,0) = t35;
    res(1,1) = 4.0*t30*t50*t5*t27+2.0*t30*t55*t27;
    res(1,2) = t63;
    res(2,0) = t49;
    res(2,1) = t63;
    res(2,2) = t30*t50*t7*(13.0/6.0*t21*t24+11.0/4.0*etaPrime*t41*t44+t20*t41*w/t43/t11)/2.0;
  }
  if (poissonRatio && naturalOmega1) 
  {
    const double t5 = D*w*L;
    const double t9 = 2.0*t5*naturalOmega1.value()*eta*poissonRatio.value();
    res(0,1) += -t9;
    res(1,0) += -t9;
    res(1,1) += -2.0*t5*naturalOmega1.value()*omega1*poissonRatio.value();
  }
  return res;
}


