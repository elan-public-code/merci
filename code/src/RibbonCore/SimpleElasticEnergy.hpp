/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef SIMPLEELASTICENERGY_H
#define SIMPLEELASTICENERGY_H

#include <Eigen/Dense>
#include <optional>

class SimpleElasticEnergy
{
public:
  void FixPhysics(double D, double ribbonWidth, double ribbonLength, std::optional<double> poissonRatio/* = std::nullopt*/);
  void FixPt(double omega1, double eta, double etaPrime, std::optional<double> naturalOmega1/* = std::nullopt*/);
  double computeEnergy();
  Eigen::Vector3d computeGrad();
  Eigen::Matrix3d computeHessian();
private:
  double omega1;
  double eta;
  double etaPrime;
  std::optional<double> naturalOmega1;
  double D;
  double ribbonWidth;
  double ribbonLength;
  std::optional<double> poissonRatio;
};

#endif // SIMPLEELASTICENERGY_H


