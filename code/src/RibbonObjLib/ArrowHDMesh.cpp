/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "ArrowHDMesh.hpp"

unsigned int ArrowHD::idx = 0;

ArrowHD::ArrowHD():
  ori(0.,0.,0.),
  dir(1.,0.,0.)
{
  update();
  name="arrow_"+std::to_string(idx++);
}

void ArrowHD::setLength(double body, double head, double diam, double headDiam)
{
  length=body;
  headlength=head;
  diameter=diam;
  headDiameter=headDiam;
  update();
}

void ArrowHD::setup(Eigen::Vector3d ori, Eigen::Vector3d dir)
{
  this->ori=ori;
  this->dir=dir.normalized();
  update();
}

void ArrowHD::setResolution(int res)
{
  if(res>=3)cylinderRes=res;
}

std::string ArrowHD::getObjName()
{
  return name;
}


void ArrowHD::update()
{
  resize(6*cylinderRes,5*cylinderRes);
  Eigen::Vector3d orthy(0., 1., 0.), orthz(0., 0., 1.);
  double oy = dir.dot(orthy);
  double oz = dir.dot(orthz);
  Eigen::Vector3d orth = (std::abs(oy)<std::abs(oz)) ? orthy : orthz;
  orth -= dir.dot(orth)*dir;
  orth.normalize();
  Eigen::Vector3d orth2 = dir.cross(orth);

    Eigen::Vector3d vpos, vnorm;
    for(int i=0;i<cylinderRes;i++)
    {
      vnorm = cos(i*2*M_PI/cylinderRes)*orth + sin(i*2*M_PI/cylinderRes)*orth2;
      vpos = ori + diameter*vnorm;
      setVertex(i, vpos, vnorm);
    }
    for(int i=0;i<cylinderRes;i++)
    {
      vnorm = cos(i*2*M_PI/cylinderRes)*orth + sin(i*2*M_PI/cylinderRes)*orth2;
      vpos = ori + length*dir + diameter*vnorm;
      setVertex(cylinderRes+i, vpos, vnorm);
      setVertex(2*cylinderRes+i, vpos, -dir);
    }
    for(int i=0;i<cylinderRes;i++)
    {
      vnorm = cos(i*2*M_PI/cylinderRes)*orth + sin(i*2*M_PI/cylinderRes)*orth2;
      vpos = ori + length*dir + headDiameter*vnorm;
      setVertex(3*cylinderRes+i, vpos, -dir);
      vnorm=(headDiameter*dir+headlength*vnorm).eval().normalized();
      setVertex(4*cylinderRes+i, vpos, vnorm);
      setVertex(5*cylinderRes+i, ori+(length+headlength)*dir, vnorm);
    }

    std::function<void(int, size_t, size_t, size_t)> setFaceInv = [&](int tri, size_t id1, size_t id2, size_t id3) {
        setFace(tri,id3,id2,id1);
      };
      
    for(int i=0;i<cylinderRes;i++)
    {
      setFace(5*i, i, (i+1)%cylinderRes, cylinderRes+i);
      setFaceInv(5*i+1, (i+1)%cylinderRes, cylinderRes+i, cylinderRes+(i+1)%cylinderRes);
      setFace(5*i+2, 2*cylinderRes +i, 2*cylinderRes + (i+1)%cylinderRes, 3*cylinderRes+i);
      setFaceInv(5*i+3, 2*cylinderRes +(i+1)%cylinderRes, 3*cylinderRes+i, 3*cylinderRes+(i+1)%cylinderRes);
      setFace(5*i+4, 4*cylinderRes +i, 4*cylinderRes + (i+1)%cylinderRes, 5*cylinderRes+i);
    }
}
