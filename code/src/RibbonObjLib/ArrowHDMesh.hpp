/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef ARROWHDMESH_H
#define ARROWHDMESH_H
#include <ObjLib/TriMesh.hpp>

class ArrowHD : public libobj::TriMesh
{
public:
  ArrowHD();
  void setup(Eigen::Vector3d ori, Eigen::Vector3d dir);
  void setLength(double body, double head, double diam, double headDiam);
  void setResolution(int res);
  virtual std::string getObjName() override;
private:
  void update();
  Eigen::Vector3d ori;
  Eigen::Vector3d dir;
  double length = 1.;
  double diameter = 0.05;
  double headlength = 0.1;
  double headDiameter = 0.1;
  int cylinderRes=10;
  
  std::string name;
  static unsigned int idx;
};

#endif // ARROWHDMESH_H
