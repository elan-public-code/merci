/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "CenterlineMesher.hpp"
#include "TubeMesh.hpp"
#include <iomanip>

CenterlineMesher::CenterlineMesher()
{
  centerLineSize=0.01;
  centerLineRes=0.0001;
  centerLineColor=Eigen::Vector3i(0,0,0);
}

void CenterlineMesher::setColor(Eigen::Vector3i c)
{
  centerLineColor=c;
}

void CenterlineMesher::setDimensions(double clSize, double clRes)
{
  centerLineSize=clSize;
  centerLineRes=clRes;
}


void CenterlineMesher::genShapeFrom(SuperRibbon ribbon)
{
  RibbonPathGenerator pathGen;
  path=pathGen.genShapeFrom(ribbon, centerLineRes);
}

void CenterlineMesher::genShapeFrom(MixedSuperRibbon ribbon)
{
  RibbonPathGenerator pathGen;
  path=pathGen.genShapeFrom(ribbon, centerLineRes);
}

void CenterlineMesher::save(libobj::ObjFileWriter& os)
{
  TubeMesh theCenterLineMesh;
  theCenterLineMesh.setDiameter(centerLineSize);
  theCenterLineMesh.setResolution(centerLineRes);
  for(auto& pt:path)
  {
    theCenterLineMesh.addPoint(pt.pos);
  }
  theCenterLineMesh.generate();
  
  std::stringstream ssmaincolor;
  ssmaincolor << "mcl" << std::setw(2) << std::setfill('0') << std::hex << centerLineColor.x() << std::setw(2) << std::setfill('0') << std::hex << centerLineColor.y() << std::setw(2) << std::setfill('0') << std::hex << centerLineColor.z();
  std::string namemaincolor = ssmaincolor.str();
  Eigen::Vector3f colsecond(centerLineColor.x() / 255., centerLineColor.y() / 255., centerLineColor.z() / 255.);
  libobj::Mtl matcl(namemaincolor, colsecond);
  
  static size_t clIdx = 0;
  std::stringstream ss;
  ss << "centerline" << clIdx++;
  os << libobj::ObjObject(ss.str());
  os << matcl<<theCenterLineMesh;
}
