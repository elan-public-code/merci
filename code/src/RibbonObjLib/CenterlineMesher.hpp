/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef CENTERLINEMESHER_H
#define CENTERLINEMESHER_H

#include <ObjLib/TriMesh.hpp>
#include "RibbonPathGenerator.hpp"

class CenterlineMesher
{
public:
  CenterlineMesher();
  void save(libobj::ObjFileWriter& os);
  void genShapeFrom(SuperRibbon ribbon);
  void genShapeFrom(MixedSuperRibbon ribbon);
  void setColor(Eigen::Vector3i c);
  void setDimensions(double centerLineSize, double centerLineRes);
private:
  
  double centerLineSize;
  double centerLineRes;
  Eigen::Vector3i centerLineColor;
  std::vector<ObjRibbonVertex> path;
};

#endif // CENTERLINEMESHER_H
