/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "FigureSuperRibbonMesher.hpp"
#include "ArrowHDMesh.hpp"
#include "TubeMesh.hpp"
#include <SuperRibbonGeometrieSerie.hpp>
#include <iomanip>


FigureSuperRibbonMesher::FigureSuperRibbonMesher()
{
  ribbonColor = Eigen::Vector3i(200, 0, 0);
  centerLineColor = Eigen::Vector3i(255, 150, 0);
  frameXColor=Eigen::Vector3i(0, 0, 255);
  frameYColor=Eigen::Vector3i(255, 0, 0);
  frameZColor=Eigen::Vector3i(0, 255, 0);
  generatrixColor=Eigen::Vector3i(0, 125, 255);
  centerLineRatio=8.0;
  resolution = 20;
}

void FigureSuperRibbonMesher::setColors(Eigen::Vector3i ribbon, Eigen::Vector3i centerLine)
{
  ribbonColor=ribbon;
  centerLineColor=centerLine;
}

void FigureSuperRibbonMesher::setFrameColors(Eigen::Vector3i X, Eigen::Vector3i Y, Eigen::Vector3i Z)
{
  frameXColor=X;
  frameYColor=Y;
  frameZColor=Z;
}

void FigureSuperRibbonMesher::setMeshDiscreteLength(std::optional<double> mdl)
{
  meshDiscreteLength = mdl;
}

void FigureSuperRibbonMesher::setRibbonName(std::string name)
{
  ribbonName=name;
}

void FigureSuperRibbonMesher::genShapeFrom(SuperRibbon ribbon)
{
  path.clear();
  ribbonWidth=ribbon.getWidth();
  ribbonThickness = ribbon.getThickness();
  this->ribbon = ribbon;
  RibbonPathGenerator pathGen;
  path=pathGen.genShapeFrom(ribbon, meshDiscreteLength);
}

void FigureSuperRibbonMesher::addFrames(double spacing, bool removeFirst, bool removeLast)
{
  RibbonPathGenerator pathGen;
  pathFrames=pathGen.genShapeFrom(ribbon, spacing);
  if(removeFirst)pathFrames.erase(pathFrames.begin());
  if(removeLast)pathFrames.erase(pathFrames.begin()+(pathFrames.size()-1));
}

void FigureSuperRibbonMesher::save(libobj::ObjFileWriter& os)
{
  ObjRibbonMesh theRibbonMesh(path, ribbonWidth,ribbonThickness);
  TubeMesh theCenterLineMesh;
  theCenterLineMesh.setDiameter(centerLineRatio*ribbonThickness);
  theCenterLineMesh.setResolution(resolution);
  for(auto& pt:path)
  {
    theCenterLineMesh.addPoint(pt.pos);
  }
  theCenterLineMesh.generate();
  
  static size_t ribbonIdx = 0;
  std::string usedRibbonName;
  if(!ribbonName.empty())
  {
    usedRibbonName=ribbonName;
  }
  else
  {
    std::stringstream ss;
    ss << "ribbonFigure" << ribbonIdx++;
    usedRibbonName=ss.str();
  }
  
  std::stringstream ssmaincolor;
  ssmaincolor << "matFig_ribbon_" << usedRibbonName;
  std::string namemaincolor = ssmaincolor.str();
  Eigen::Vector3f colmain(ribbonColor.x() / 255., ribbonColor.y() / 255., ribbonColor.z() / 255.);
  libobj::Mtl matmain(namemaincolor, colmain);

  std::stringstream sssecondcolor;
  sssecondcolor << "matFig_centerLine_" << usedRibbonName;
  std::string namesecondcolor = sssecondcolor.str();
  Eigen::Vector3f colsecond(centerLineColor.x() / 255., centerLineColor.y() / 255., centerLineColor.z() / 255.);
  libobj::Mtl matsecond(namesecondcolor, colsecond);
  
  std::stringstream ssframexcolor;
  ssframexcolor << "matFig_framex_" << usedRibbonName;
  std::string nameframexcolor = ssframexcolor.str();
  Eigen::Vector3f colframeX(frameXColor.x() / 255., frameXColor.y() / 255., frameXColor.z() / 255.);
  libobj::Mtl matframex(nameframexcolor, colframeX);

  std::stringstream ssframeycolor;
  ssframeycolor << "matFig_framey_" << usedRibbonName;
  std::string nameframeycolor = ssframeycolor.str();
  Eigen::Vector3f colframeY(frameYColor.x() / 255., frameYColor.y() / 255., frameYColor.z() / 255.);
  libobj::Mtl matframey(nameframeycolor, colframeY);
  
  std::stringstream ssframezcolor;
  ssframezcolor << "matFig_framez_" << usedRibbonName;
  std::string nameframezcolor = ssframezcolor.str();
  Eigen::Vector3f colframeZ(frameZColor.x() / 255., frameZColor.y() / 255., frameZColor.z() / 255.);
  libobj::Mtl matframez(nameframezcolor, colframeZ);
  
  std::stringstream ssgeneratrixcolor;
  ssgeneratrixcolor << "matFig_generatrix_" << usedRibbonName;
  std::string namegeneratrixcolor = ssgeneratrixcolor.str();
  Eigen::Vector3f colgeneratrix(generatrixColor.x() / 255., generatrixColor.y() / 255., generatrixColor.z() / 255.);
  libobj::Mtl matgeneratrix(namegeneratrixcolor, colgeneratrix);
  
  os << libobj::ObjObject(usedRibbonName);
  os << matmain<<theRibbonMesh;
  os << matsecond<<theCenterLineMesh;
  
  double arrowLength=ribbonWidth*3.0/8.0;
  double arrowHeadLength=arrowLength/4.0;
  double arrowDiameter=centerLineRatio*ribbonThickness*1.5*1.05;
  double arrowHeadDiameter=2.0*arrowDiameter;
  
  if(!pathFrames.empty())
  {
    os<<matframex;
    for(auto& f:pathFrames)
    {
      ArrowHD arrow;
      arrow.setResolution(resolution);
      arrow.setup(f.pos,f.frame.col(0));
      arrow.setLength(arrowLength, arrowHeadLength, arrowDiameter, arrowHeadDiameter);
      os<<arrow;
    }
    os<<matframey;
    for(auto& f:pathFrames)
    {
      ArrowHD arrow;
      arrow.setResolution(resolution);
      arrow.setup(f.pos,f.frame.col(1));
      arrow.setLength(arrowLength, arrowHeadLength, arrowDiameter, arrowHeadDiameter);
      os<<arrow;
    }
    os<<matframez;
    for(auto& f:pathFrames)
    {
      ArrowHD arrow;
      arrow.setResolution(resolution);
      arrow.setup(f.pos,f.frame.col(2));
      arrow.setLength(arrowLength, arrowHeadLength, arrowDiameter, arrowHeadDiameter);
      os<<arrow;
    }
    os<<matgeneratrix;
    for(auto& f:pathFrames)
    {
      TubeMesh tm;
      tm.setDiameter(arrowDiameter/2.0);
      tm.setResolution(resolution);
      Eigen::Vector3d g = f.frame.col(0)+f.eta*f.frame.col(2);
      tm.addPoint(f.pos+0.5*ribbonWidth*g);
      tm.addPoint(f.pos);
      tm.addPoint(f.pos-0.5*ribbonWidth*g);
      tm.generate();
      os<<tm;
      /*ArrowHD arrow;
      arrow.setResolution(resolution);
      arrow.setup(f.pos,f.frame.col(0)+f.eta*f.frame.col(2));
      double lengthratio=(f.frame.col(0)+f.eta*f.frame.col(2)).norm();
      arrow.setLength(lengthratio*arrowLength, lengthratio*arrowHeadLength, arrowDiameter/2.0, arrowHeadDiameter/2.0);
      os<<arrow;*/
    }
  }
}
