/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef FIGURESUPERRIBBONMESHER_H
#define FIGURESUPERRIBBONMESHER_H

#include <vector>
#include <SuperRibbon.hpp>
#include <optional>
#include "ObjRibbonMesh.hpp"
#include <ObjLib/TriMesh.hpp>
#include "RibbonPathGenerator.hpp"

class FigureSuperRibbonMesher
{
public:
  FigureSuperRibbonMesher();
  void genShapeFrom(SuperRibbon ribbon);
  void setMeshDiscreteLength(std::optional<double> mdl);
  void setRibbonName(std::string name);
  void setColors(Eigen::Vector3i ribbon, Eigen::Vector3i centerLine);
  void setFrameColors(Eigen::Vector3i X, Eigen::Vector3i Y, Eigen::Vector3i Z);
  void save(libobj::ObjFileWriter& os);
  void addFrames(double spacing, bool removeFirst, bool removeLast);
private:
  SuperRibbon ribbon;
  std::string ribbonName;
  Eigen::Vector3i ribbonColor;
  Eigen::Vector3i centerLineColor;
  Eigen::Vector3i frameXColor;
  Eigen::Vector3i frameYColor;
  Eigen::Vector3i frameZColor;
  Eigen::Vector3i generatrixColor;
  std::optional<double> meshDiscreteLength;
  double ribbonThickness;
  double ribbonWidth;
  double centerLineRatio;
  std::vector<ObjRibbonVertex> path;
  std::vector<ObjRibbonVertex> pathFrames;
  int resolution;
};

#endif // FIGURESUPERRIBBONMESHER_H
