/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "MRMesher.hpp"
#include <SuperRibbonGeometrieSerie.hpp>
#include <iomanip>

MRMesher::MRMesher()
{
  meshMainColor = Eigen::Vector3i(200, 0, 0);
  meshSecondColor = Eigen::Vector3i(255, 150, 0);
}

MRMesher::MRMesher(Eigen::Vector3i maincolor, Eigen::Vector3i secondcolor):
  meshMainColor(maincolor),
  meshSecondColor(secondcolor)
{
}

std::vector<ObjRibbonVertex> MRMesher::setupRibbonPart(MRMesher::RibbonPartSetup &setup)
{
  SuperRibbonGeometrieSerie ribbon;
  ribbon.Place(setup.posOr, setup.frameOr);
  ribbon.Setup(setup.omega1, setup.n);
  setup.smax = ribbon.estimSMax();
  real s1 = setup.smax;
  if (s1 + setup.s0 >= setup.segmentLength) {
    s1 = setup.segmentLength - setup.s0;
    setup.lastPart = true;
  }
  std::vector<ObjRibbonVertex> path;
  int segment = static_cast<int>(s1 / meshDiscreteLength.value_or(setup.ribbonWidth) + 1.);
  for (int i = 0; i <= segment; i++) {
    real s = (static_cast<real>(i) / segment) * s1;
    auto [pos, frame] = ribbon.PosFrame(s);
    ObjRibbonVertex v;
    for (int j = 0; j < 9; j++)v.frame(j % 3, j / 3) = frame(j % 3, j / 3);
    for (int j = 0; j < 3; j++)v.pos[j] = pos(j);
    v.eta = setup.n.A * s + setup.n.B;
    path.push_back(v);
  }
  setup.sout = setup.s0 + s1;
  std::tie(setup.posF, setup.frameF) = ribbon.PosFrame(s1);
  return path;
}

void MRMesher::computeSegment(MixedSuperRibbon &data, int segmentNumber, int &MeshOffset)
{
  LinearScalar omega1 = data.getOmega1(segmentNumber);
  LinearScalar n = data.getEta(segmentNumber);
  std::vector<std::vector<ObjRibbonVertex> > preMeshes;
  RibbonPartSetup rps;
  rps.frameOr = data.getFrameStart(segmentNumber);
  rps.posOr = data.getPosStart(segmentNumber);
  rps.omega1 = omega1;
  rps.n = n;
  rps.s0 = 0.;
  rps.segmentLength = data.getLength(segmentNumber);
  rps.ribbonWidth = data.getWidth();
  do {
    preMeshes.push_back(setupRibbonPart(rps));
    rps.frameOr = rps.frameF;
    rps.posOr = rps.posF;
    rps.s0 = rps.sout;
    rps.omega1.B = omega1.B + rps.s0 * omega1.A;
    rps.omega1.A = omega1.A;
    rps.n.B = n.B + rps.s0 * n.A;
    rps.n.A = n.A;
    rps.partnb++;
  } while (!rps.lastPart);

  uint t = preMeshes.size();
  for (uint i = 0; i < t; i++) {
    std::vector<ObjRibbonVertex> path = preMeshes[i];
    if(path.size()==0)continue;
    theRibbon.push_back(RibbonPart(ObjRibbonMesh(path, rps.ribbonWidth,ribbonThickness.value_or(rps.ribbonWidth/20.)), (segmentNumber + 1) % 2));
  }
  MeshOffset += t;
}

void MRMesher::setColors(Eigen::Vector3i main, Eigen::Vector3i second)
{
  meshMainColor = main;
  meshSecondColor = second;
}

void MRMesher::set(MixedSuperRibbon ribbon)
{
  theRibbon.clear();
  int t = ribbon.getNbSegments();
  int Mo = 0;
  for (int i = 0; i < t; i++)
    computeSegment(ribbon, i, Mo);
}

void MRMesher::setMeshDiscreteLength(std::optional<double> mdl)
{
  meshDiscreteLength = mdl;
}

void MRMesher::setRibbonThickness(std::optional<double> rt)
{
  ribbonThickness=rt;
}

void MRMesher::clear()
{
  theRibbon.clear();
}

void MRMesher::save(libobj::ObjFileWriter &os)
{
  static size_t ribbonIdx = 0;
  std::stringstream ss;
  ss << "mixedribbon" << ribbonIdx++;
  save(os, ss.str());
}

void MRMesher::save(libobj::ObjFileWriter& os, std::string ribbonName)
{
  std::stringstream ssmaincolor;
  ssmaincolor << "m" << std::setw(2) << std::setfill('0') << std::hex << meshMainColor.x() << std::setw(2) << std::setfill('0') << std::hex << meshMainColor.y() << std::setw(2) << std::setfill('0') << std::hex << meshMainColor.z();
  std::string namemaincolor = ssmaincolor.str();
  Eigen::Vector3f colmain(meshMainColor.x() / 255., meshMainColor.y() / 255., meshMainColor.z() / 255.);
  libobj::Mtl matmain(namemaincolor, colmain);

  std::stringstream sssecondcolor;
  sssecondcolor << "m" << std::setw(2) << std::setfill('0') << std::hex << meshSecondColor.x() << std::setw(2) << std::setfill('0') << std::hex << meshSecondColor.y() << std::setw(2) << std::setfill('0') << std::hex << meshSecondColor.z();
  std::string namesecondcolor = sssecondcolor.str();
  Eigen::Vector3f colsecond(meshSecondColor.x() / 255., meshSecondColor.y() / 255., meshSecondColor.z() / 255.);
  libobj::Mtl matsecond(namesecondcolor, colsecond);
  
  os << libobj::ObjObject(ribbonName);
  for (auto ent : theRibbon) {
    if (ent.maincolor) os << matmain;
    else os << matsecond;
    os << ent.mesh;
  }
}
