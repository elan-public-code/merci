/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef MRMESHER_H
#define MRMESHER_H

#include <vector>
#include <MixedSRCore/MixedSuperRibbon.hpp>
#include <optional>
#include "ObjRibbonMesh.hpp"


class MRMesher
{
public:
  MRMesher();
  MRMesher(Eigen::Vector3i maincolor, Eigen::Vector3i secondcolor);
  void set(MixedSuperRibbon ribbon);
  void setMeshDiscreteLength(std::optional<double> mdl);
  void setRibbonThickness(std::optional<double> rt);
  void clear();
  void setColors(Eigen::Vector3i main, Eigen::Vector3i second);
  void save(libobj::ObjFileWriter& os);
  void save(libobj::ObjFileWriter& os, std::string ribbonName);
private:
  struct RibbonPart {
    RibbonPart(ObjRibbonMesh m,bool mc):mesh(m),maincolor(mc){}
    ObjRibbonMesh mesh;
    bool maincolor;
  };

  struct RibbonPartSetup {
    uint partnb;
    real s0;
    real sout;
    real smax;
    real segmentLength;
    real ribbonWidth;
    bool lastPart = false;
    LinearScalar omega1;
    LinearScalar n;
    Eigen::Vector3r posOr;
    Eigen::Vector3r posF;
    Eigen::Matrix3r frameOr;
    Eigen::Matrix3r frameF;
  };
  std::optional<double> meshDiscreteLength;
  std::optional<double> ribbonThickness;
  Eigen::Vector3i meshMainColor;
  Eigen::Vector3i meshSecondColor;
  std::vector<RibbonPart> theRibbon;
  std::vector<ObjRibbonVertex> setupRibbonPart(RibbonPartSetup &);
  void computeSegment(MixedSuperRibbon& data, int segmentNumber, int& MeshOffset);
};

#endif // MRMESHER_H
