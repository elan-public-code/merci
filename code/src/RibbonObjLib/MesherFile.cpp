/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "MesherFile.hpp"
#include <sstream>
#include <iomanip>
#include <RibbonObjLib/MRMesher.hpp>
#include <RibbonObjLib/PlaneMesh.hpp>
#include <RibbonObjLib/ArrowHDMesh.hpp>

AutoMat::AutoMat(libobj::Mtl mtl) :
  content(mtl)
{
}

AutoMat::AutoMat(std::array<int, 3> c) :
  content(colToName(c), colToC(c))
{
}

AutoMat::AutoMat(std::string n, std::array<int, 3> c) :
  content(n, colToC(c))
{
}

AutoMat::operator libobj::Mtl()  const
{
  return content;
}



std::string AutoMat::colToName(std::array<int, 3> c)
{
  std::stringstream sscolor;
  sscolor << "mat" << std::setw(2) << std::setfill('0') << std::hex << c[0] << std::setw(2) << std::setfill('0') << std::hex << c[1] << std::setw(2) << std::setfill('0') << std::hex << c[2];
  return sscolor.str();
}

Eigen::Vector3f AutoMat::colToC(std::array<int, 3> c)
{
  return Eigen::Vector3f(c[0] / 255., c[1] / 255., c[2] / 255.);
}

void FastMesherFile::open(std::string filename)
{
  meshWriter.open(filename);
}

void FastMesherFile::close()
{
  meshWriter.close();
}

void FastMesherFile::addRibbon(MixedSuperRibbon msr, std::optional<std::string> name)
{
  addRibbon(msr, lastMRfirstCol, lastMRsecondCol, name);
}


void FastMesherFile::addRibbon(MixedSuperRibbon msr, AutoMat firstCol, AutoMat secondCol, std::optional<std::string> name)
{
  lastMRfirstCol=firstCol;
  lastMRsecondCol=secondCol;
  libobj::Mtl mtl1= firstCol;
  libobj::Mtl mtl2= secondCol;
  Eigen::Vector3i c1; c1 << 255*mtl1.ambient[0], 255*mtl1.ambient[1], 255*mtl1.ambient[2];
  Eigen::Vector3i c2; c2 << 255*mtl2.ambient[0], 255*mtl2.ambient[1], 255*mtl2.ambient[2];
  MRMesher mesher(c1, c2);
  mesher.setMeshDiscreteLength(MRdiscreteLength);
  mesher.setRibbonThickness(msr.getThickness());
  mesher.set(msr);
  if(name.has_value())
    mesher.save(meshWriter, name.value());
  else
    mesher.save(meshWriter);
}

void FastMesherFile::addPlane(Eigen::Vector3r planePos, Eigen::Vector3r planeNml)
{
  addPlane(planePos, planeNml, lastPlaneMat);
}

void FastMesherFile::addPlane(Eigen::Vector3r planePos, Eigen::Vector3r planeNml, AutoMat color)
{
  lastPlaneMat = color;
  PlaneMesh planemesh;
  planemesh.place(planePos, planeNml);
  meshWriter<<color<<planemesh;
}

void FastMesherFile::addArrow(Eigen::Vector3r pos, Eigen::Vector3r dir, double scale)
{
  addArrow(pos, dir, lastArrowMat, scale);
}

void FastMesherFile::addArrow(Eigen::Vector3r pos, Eigen::Vector3r dir, AutoMat color, double scale)
{
  lastArrowMat = color;
  ArrowHD arrowmesh;
  arrowmesh.setup(Eigen::Vector3d(0.,0.,0.), Eigen::Vector3d(0.,-1.,0.));
  arrowmesh.setLength(scale, scale*0.15, scale*0.025, scale*0.05);
  arrowmesh.setResolution(10);
  meshWriter<<color<<arrowmesh;
}
