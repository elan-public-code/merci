/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef MESHERFILE_H
#define MESHERFILE_H

#include <ObjLib/ObjFileWriter.hpp>
#include <ObjLib/MtlFile.hpp>
#include <MixedSRCore/MixedSuperRibbon.hpp>
#include <array>
#include <optional>

class AutoMat{
public: 
  AutoMat(libobj::Mtl);
  AutoMat(std::array<int,3>);
  AutoMat(std::string, std::array<int,3>);
  operator libobj::Mtl() const;
private:
  static std::string colToName(std::array<int,3>);
  static Eigen::Vector3f colToC(std::array<int,3>);
  libobj::Mtl content;
};

class FastMesherFile
{
public:
  void open(std::string filename);
  void close();
  void addRibbon(MixedSuperRibbon msr, AutoMat firstCol, AutoMat secondCol, std::optional<std::string> name = std::nullopt);
  void addRibbon(MixedSuperRibbon msr, std::optional<std::string> name = std::nullopt);
  void addPlane(Eigen::Vector3r planePos, Eigen::Vector3r planeNml, AutoMat color);
  void addArrow(Eigen::Vector3r pos, Eigen::Vector3r dir, AutoMat color, double scale=0.1);
  void addPlane(Eigen::Vector3r planePos, Eigen::Vector3r planeNml);
  void addArrow(Eigen::Vector3r pos, Eigen::Vector3r dir, double scale=0.1);
private:
  libobj::ObjFileWriter meshWriter;
  double MRdiscreteLength = 0.005;
  AutoMat lastMRfirstCol={{0,0,0}};
  AutoMat lastMRsecondCol={{255,255,255}};
  AutoMat lastArrowMat={{255,255,255}};
  AutoMat lastPlaneMat={{255,255,255}};
};

#endif // MESHERFILE_H
