/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "ObjRibbonMesh.hpp"

/*ObjRibbonMesh::ObjRibbonMesh(std::vector<ObjRibbonVertex> path, double width): libobj::TriMesh()
{
  size_t subdivision = path.size()-1;
#ifdef RIBBON_MESH_SINGLE_SIDED
  resize(3 * subdivision + 3, 4 * subdivision);
#else
  resize(3 * subdivision + 3, 8 * subdivision);
#endif
//INDICES
  int t = subdivision;
#ifdef RIBBON_MESH_SINGLE_SIDED
  data->setTrianglesCount(4 * t);
  auto tri = data->triangles();
  for (int i = 0; i < t; i++) {
    setFace(4 * i, 3 * i, 3 * i + 1, 3 * (i + 1));
    setFace(4 * i + 1, 3 * i + 1, 3 * (i + 1) + 1, 3 * (i + 1));
    setFace(4 * i + 2, 3 * i + 1, 3 * i + 2, 3 * (i + 1) + 1);
    setFace(4 * i + 3, 3 * i + 2, 3 * (i + 1) + 2, 3 * (i + 1) + 1);
  }
#else //#ifdef RIBBON_MESH_SINGLE_SIDED
  //Double sided
  for (int i = 0; i < t; i++) {
    setFace(8 * i, 3 * i, 3 * i + 1, 3 * (i + 1));
    setFace(8 * i + 1, 3 * i + 1, 3 * (i + 1) + 1, 3 * (i + 1));
    setFace(8 * i + 2, 3 * i + 1, 3 * i + 2, 3 * (i + 1) + 1);
    setFace(8 * i + 3, 3 * i + 2, 3 * (i + 1) + 2, 3 * (i + 1) + 1);
    setFace(8 * i + 4, 3 * (i + 1), 3 * i + 1, 3 * i);
    setFace(8 * i + 5, 3 * (i + 1), 3 * (i + 1) + 1, 3 * i + 1);
    setFace(8 * i + 6, 3 * (i + 1) + 1, 3 * i + 2, 3 * i + 1);
    setFace(8 * i + 7, 3 * (i + 1) + 1, 3 * (i + 1) + 2, 3 * i + 2);
  }
#endif //#ifdef RIBBON_MESH_SINGLE_SIDED


//VERTICES
  t = path.size();
  for (int i = 0; i < t; i++) {
    Eigen::Vector3d midpos = path.at(i).pos;
    Eigen::Matrix3d frame = path.at(i).frame;
    Eigen::Vector3d frameX(frame(0, 0), frame(1, 0), frame(2, 0)), frameY(frame(0, 1), frame(1, 1), frame(2, 1)), frameZ(frame(0, 2), frame(1, 2), frame(2, 2));
    double eta = path.at(i).eta;
    Eigen::Vector3d border1, border2;
    border1 = midpos - 0.5 * width * (frameX + eta * frameZ);
    border2 = midpos + 0.5 * width * (frameX + eta * frameZ);
    setVertex(3 * i, border1, frameY);
    setVertex(3 * i + 1, midpos, frameY);
    setVertex(3 * i + 2, border2, frameY);
  }
  defineAsGroup(true);
}*/

ObjRibbonMesh::ObjRibbonMesh(std::vector<ObjRibbonVertex> path, double width, double height): libobj::TriMesh()
{
  size_t subdivision = path.size()-1;
  resize(10*(1 * subdivision + 1 ) + 8, 12 * subdivision+4);
  auto idxOf=[](int triid, bool side, int vertex){return 10*triid+(side?5:0)+vertex+1;};
//INDICES
  int t = subdivision;
  for (int i = 0; i < t; i++) {
    setFace(8 * i + 0, idxOf(i,true,0), idxOf(i,true,1), idxOf(i+1,true,0));
    setFace(8 * i + 1, idxOf(i,true,1), idxOf(i+1,true,1), idxOf(i+1,true,0));
    setFace(8 * i + 2, idxOf(i,true,1), idxOf(i,true,2), idxOf(i+1,true,1));
    setFace(8 * i + 3, idxOf(i,true,2), idxOf(i+1,true,2), idxOf(i+1,true,1));
    setFace(8 * i + 4, idxOf(i+1,false,0), idxOf(i,false,1), idxOf(i,false,0));
    setFace(8 * i + 5, idxOf(i+1,false,0), idxOf(i+1,false,1), idxOf(i,false,1));
    setFace(8 * i + 6, idxOf(i+1,false,1), idxOf(i,false,2), idxOf(i,false,1));
    setFace(8 * i + 7, idxOf(i+1,false,1), idxOf(i+1,false,2), idxOf(i,false,2));
  }

  int offset = 8*t;
  
  auto idxOfSide=[](int triid, bool side, int vertex){return 10*triid+(side?5:0)+vertex*4;};
  
  for (int i = 0; i < t; i++) {
    setFace(offset + 4 * i + 0, idxOfSide(i,true,0), idxOfSide(i+1,true,0), idxOfSide(i,false,0));
    setFace(offset + 4 * i + 1, idxOfSide(i+1,true,0), idxOfSide(i+1,false,0), idxOfSide(i,false,0));
    setFace(offset + 4 * i + 2, idxOfSide(i,false,1), idxOfSide(i+1,true,1), idxOfSide(i,true,1));
    setFace(offset + 4 * i + 3, idxOfSide(i,false,1), idxOfSide(i+1,false,1), idxOfSide(i+1,true,1));
  }
  offset = 12*t;
  //setFace(offset + 0, idxOf(0,true,2), idxOf(0,true,0), idxOf(0,false,0));
  //setFace(offset + 1, idxOf(0,true,2), idxOf(0,false,0), idxOf(0,false,2));
  //setFace(offset + 2, idxOf(t,true,0), idxOf(t,true,2), idxOf(t,false,0));
  //setFace(offset + 3, idxOf(t,true,2), idxOf(t,false,2), idxOf(t,false,0));
  setFace(offset + 0, 10*(t+1), 10*(t+1)+1, 10*(t+1)+3);
  setFace(offset + 1, 10*(t+1)+1, 10*(t+1)+2, 10*(t+1)+3);
  setFace(offset + 2, 10*(t+1)+7, 10*(t+1)+5, 10*(t+1)+4);
  setFace(offset + 3, 10*(t+1)+7, 10*(t+1)+6, 10*(t+1)+5);

//VERTICES
  t = path.size();
  for (int i = 0; i < t; i++) {
    Eigen::Vector3d midpos = path.at(i).pos;
    Eigen::Matrix3d frame = path.at(i).frame;
    Eigen::Vector3d frameX(frame(0, 0), frame(1, 0), frame(2, 0)), frameY(frame(0, 1), frame(1, 1), frame(2, 1)), frameZ(frame(0, 2), frame(1, 2), frame(2, 2));
    double eta = path.at(i).eta;
    Eigen::Vector3d border1, border2;
    border1 = midpos - 0.5 * width * (frameX + eta * frameZ);
    border2 = midpos + 0.5 * width * (frameX + eta * frameZ);
    Eigen::Vector3d thickVar=0.5*height*frameY;
    Eigen::Vector3d nml=(frameX + eta * frameZ).normalized();
    setVertex(10 * i + 0, border1+thickVar, -nml);
    setVertex(10 * i + 1, border1+thickVar, frameY);
    setVertex(10 * i + 2, midpos+thickVar, frameY);
    setVertex(10 * i + 3, border2+thickVar, frameY);
    setVertex(10 * i + 4, border2+thickVar, nml);
    setVertex(10 * i + 5, border1-thickVar, -nml);
    setVertex(10 * i + 6, border1-thickVar, -frameY);
    setVertex(10 * i + 7, midpos-thickVar, -frameY);
    setVertex(10 * i + 8, border2-thickVar, -frameY);
    setVertex(10 * i + 9, border2-thickVar, nml);
  }
  {
    Eigen::Vector3d midpos = path.at(0).pos;
    Eigen::Matrix3d frame = path.at(0).frame;
    Eigen::Vector3d frameX(frame(0, 0), frame(1, 0), frame(2, 0)), frameY(frame(0, 1), frame(1, 1), frame(2, 1)), frameZ(frame(0, 2), frame(1, 2), frame(2, 2));
    double eta = path.at(0).eta;
    Eigen::Vector3d border1, border2;
    border1 = midpos - 0.5 * width * (frameX + eta * frameZ);
    border2 = midpos + 0.5 * width * (frameX + eta * frameZ);
    Eigen::Vector3d thickVar=0.5*height*frameY;
    setVertex(10 * t + 0, border1+thickVar, -frameZ);
    setVertex(10 * t + 1, border2+thickVar, -frameZ);
    setVertex(10 * t + 2, border2-thickVar, -frameZ);
    setVertex(10 * t + 3, border1-thickVar, -frameZ);
  }
  {
    Eigen::Vector3d midpos = path.at(t-1).pos;
    Eigen::Matrix3d frame = path.at(t-1).frame;
    Eigen::Vector3d frameX(frame(0, 0), frame(1, 0), frame(2, 0)), frameY(frame(0, 1), frame(1, 1), frame(2, 1)), frameZ(frame(0, 2), frame(1, 2), frame(2, 2));
    double eta = path.at(t-1).eta;
    Eigen::Vector3d border1, border2;
    border1 = midpos - 0.5 * width * (frameX + eta * frameZ);
    border2 = midpos + 0.5 * width * (frameX + eta * frameZ);
    Eigen::Vector3d thickVar=0.5*height*frameY;
    setVertex(10 * t + 4, border1+thickVar, frameZ);
    setVertex(10 * t + 5, border2+thickVar, frameZ);
    setVertex(10 * t + 6, border2-thickVar, frameZ);
    setVertex(10 * t + 7, border1-thickVar, frameZ);
  }
  defineAsGroup(true);
}

std::string ObjRibbonMesh::getObjName()
{
  static int idx = 0;
  return "ribbon_part_" + std::to_string(idx++);
}
