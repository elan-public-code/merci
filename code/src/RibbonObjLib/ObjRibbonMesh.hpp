/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef OBJRIBBONMESH_H
#define OBJRIBBONMESH_H

#include <ObjLib/TriMesh.hpp>
#include <vector>
#include <BaseCore/real.hpp>

class ObjRibbonVertex
{
public:
  Eigen::Vector3d pos;
  float eta;
  float omega1;
  Eigen::Matrix3d frame;
};

/**
 * The Ribbon Mesh object himself
 */
class ObjRibbonMesh : public libobj::TriMesh
{
public:
  //[[deprecated]]ObjRibbonMesh(std::vector<ObjRibbonVertex> path, double width);
  
  ObjRibbonMesh(std::vector<ObjRibbonVertex> path, double width, double height);
  virtual std::string getObjName() override;
};

#endif // OBJRIBBONMESH_H
