/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "PlaneMesh.hpp"

unsigned int PlaneMesh::idx = 0;

PlaneMesh::PlaneMesh()
{
  thickness=1.e-6;
  size=1;
  pos<<0,0,0;
  normal<<0,0,1;
  name="plan_"+std::to_string(idx++);
  update();
}

std::string PlaneMesh::getObjName()
{
  return name;
}

void PlaneMesh::place(Eigen::Vector3d p, Eigen::Vector3d nml)
{
  pos=p;
  normal=nml;
  update();
}

void PlaneMesh::setNormal(Eigen::Vector3d nml)
{
  normal=nml;
  update();
}

void PlaneMesh::setPos(Eigen::Vector3d p)
{
  pos=p;
  update();
}

void PlaneMesh::setSize(double s)
{
  size=s; 
  update();
}

void PlaneMesh::setThickness(double th)
{
  thickness=th;
  update();
}

void PlaneMesh::update()
{
  //resize(24,12);
  resize(8,4);
//   Eigen::Matrix3d m1,m2;
//   m1<<0,1,0,
//      -1,0,0,
//       0,0,1;
//   m2<<1,0,0,
//       0,0,1,
//       0,-1,0;
//   Eigen::Vector3d planeD1=m1*m2*normal;
  Eigen::Vector3d planeD1; planeD1<<-normal(1),normal(0),0.;
  if(planeD1.isZero())
    planeD1<<1.,0.,0.;
  planeD1.normalize();
  Eigen::Vector3d planeD2=normal.cross(planeD1);
  //upper face
  setVertex(0, pos+0.5*size*planeD1+0.5*size*planeD2, normal);
  setVertex(1, pos+0.5*size*planeD1-0.5*size*planeD2, normal);
  setVertex(2, pos-0.5*size*planeD1-0.5*size*planeD2, normal);
  setVertex(3, pos-0.5*size*planeD1+0.5*size*planeD2, normal);
  setFace(0,2,1,0);
  setFace(1,0,3,2);
  //lower face
  setVertex(4, pos+0.5*size*planeD1+0.5*size*planeD2-thickness*normal, -normal);
  setVertex(5, pos+0.5*size*planeD1-0.5*size*planeD2-thickness*normal, -normal);
  setVertex(6, pos-0.5*size*planeD1-0.5*size*planeD2-thickness*normal, -normal);
  setVertex(7, pos-0.5*size*planeD1+0.5*size*planeD2-thickness*normal, -normal);
  setFace(2,4,5,6);
  setFace(3,6,7,4);
}
