/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef PLANEMESH_H
#define PLANEMESH_H

#include <ObjLib/TriMesh.hpp>

class PlaneMesh : public libobj::TriMesh
{
public:
    PlaneMesh();
    virtual std::string getObjName() override;
    void setPos(Eigen::Vector3d p);
    void setNormal(Eigen::Vector3d nml);
    void setSize(double s);
    void setThickness(double th);
    void place(Eigen::Vector3d p, Eigen::Vector3d nml);
private:
  void update();
  Eigen::Vector3d pos;
  Eigen::Vector3d normal;
  double thickness;
  double size;
  std::string name;
  static unsigned int idx;
};

#endif // PLANEMESH_H
