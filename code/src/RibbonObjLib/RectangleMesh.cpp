/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "RectangleMesh.hpp"

unsigned int RectangleMesh::idx=0;

RectangleMesh::RectangleMesh()
{
  LLCorner<<0.,0.,0.;
  dir1<<1.,0.,0.;
  dir2<<0.,1.,0.;
  name="rect"+std::to_string(idx++);
  update();
}

std::string RectangleMesh::getObjName()
{
  return name;
}

void RectangleMesh::setup(Eigen::Vector3d LLCorner, Eigen::Vector3d dir1, Eigen::Vector3d dir2)
{
  this->LLCorner=LLCorner;
  this->dir1=dir1;
  this->dir2=dir2;
  update();
}

void RectangleMesh::update()
{
  resize(4,2);
  Eigen::Vector3d n= dir1.cross(dir2).normalized();
  setVertex(0,LLCorner,n);
  setVertex(1,LLCorner+dir1,n);
  setVertex(2,LLCorner+dir1+dir2,n);
  setVertex(3,LLCorner+dir2,n);
  setFace(0,0,1,2);
  setFace(1,0,2,3);
}
