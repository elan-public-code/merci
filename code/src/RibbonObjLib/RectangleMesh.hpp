/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef RECTANGLEMESH_H
#define RECTANGLEMESH_H

#include <ObjLib/TriMesh.hpp>

class RectangleMesh : public libobj::TriMesh
{
public:
    RectangleMesh();
    virtual std::string getObjName() override;
    void setup(Eigen::Vector3d LLCorner, Eigen::Vector3d dir1, Eigen::Vector3d dir2);
private:
  void update();
  Eigen::Vector3d LLCorner;
  Eigen::Vector3d dir1;
  Eigen::Vector3d dir2;
  std::string name;
  static unsigned int idx;
};

#endif // RECTANGLEMESH_H
