/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "RibbonPathGenerator.hpp"
#include <SuperRibbonGeometrieSerie.hpp>

std::vector<ObjRibbonVertex> RibbonPathGenerator::genShapeFrom(SuperRibbon ribbon, std::optional<double> meshDiscretisationLength)
{
  meshDiscreteLength=meshDiscretisationLength.value_or(ribbon.getWidth());
  path.clear();
  
  int t = ribbon.getNbSegments();
  Eigen::Vector3r pos = ribbon.getPosOr();
  Eigen::Matrix3r frame = ribbon.getFrameOr();
  double lastS = -meshDiscreteLength;
  for (int i = 0; i < t; i++)
    computeSegment(ribbon, i, pos, frame, lastS, (t-i)<=1);
  
  return path;
}

std::vector<ObjRibbonVertex> RibbonPathGenerator::genShapeFrom(MixedSuperRibbon ribbon, std::optional<double> meshDiscretisationLength)
{
  meshDiscreteLength=meshDiscretisationLength.value_or(ribbon.getWidth());
  path.clear();
  
  int t = ribbon.getNbSegments();
  Eigen::Vector3r pos;
  Eigen::Matrix3r frame;
  LinearScalar omega1,eta;
  double length;
  double w=ribbon.getWidth();
  double lastS = -meshDiscreteLength;
  for (int i = 0; i < t; i++)
  {
    omega1=ribbon.getOmega1(i);
    eta=ribbon.getEta(i);
    pos=ribbon.getPosStart(i);
    frame=ribbon.getFrameStart(i);
    length=ribbon.getLength(i);
    computeSegment(omega1, eta, length, w, pos, frame, lastS, (t-i)<=1);
  }
  return path;
}

void RibbonPathGenerator::computeSegment(SuperRibbon& data, int segmentNumber, Eigen::Vector3r& pos, Eigen::Matrix3r& frame, double& lastS, bool lastPart)
{
  computeSegment(data.getOmega1(segmentNumber), data.getEta(segmentNumber), data.getLength(segmentNumber), data.getWidth(), pos, frame, lastS, lastPart);
}

void RibbonPathGenerator::computeSegment(const LinearScalar& omega1, const LinearScalar& n, const double& length, const double& width, Eigen::Vector3r& pos, Eigen::Matrix3r& frame, double& lastS, bool lastPart)
{
  RibbonPartSetup rps;
  rps.frameOr = frame;
  rps.posOr = pos;
  rps.omega1 = omega1;
  rps.n = n;
  rps.s0 = 0.;
  rps.segmentLength = length;
  rps.ribbonWidth = width;
  do {
    setupRibbonPart(rps, lastS, lastPart);
    rps.frameOr = rps.frameF;
    rps.posOr = rps.posF;
    rps.s0 = rps.sout;
    rps.omega1.B = omega1.B + rps.s0 * omega1.A;
    rps.omega1.A = omega1.A;
    rps.n.B = n.B + rps.s0 * n.A;
    rps.n.A = n.A;
    rps.partnb++;
  } while (!rps.lastPart);
  frame = rps.frameF;
  pos = rps.posF;
}


void RibbonPathGenerator::setupRibbonPart(RibbonPathGenerator::RibbonPartSetup& setup, double& lastS, bool lastPart)
{
  SuperRibbonGeometrieSerie ribbon;
  ribbon.Place(setup.posOr, setup.frameOr);
  ribbon.Setup(setup.omega1, setup.n);
  setup.smax = ribbon.estimSMax();
  real s1 = setup.smax;
  if (s1 + setup.s0 >= setup.segmentLength) {
    s1 = setup.segmentLength - setup.s0;
    setup.lastPart = true;
  }
  while (lastS+meshDiscreteLength<=s1)
  {
    lastS+=meshDiscreteLength;
    auto [pos, frame] = ribbon.PosFrame(lastS);
    ObjRibbonVertex v;
    for (int j = 0; j < 9; j++)v.frame(j % 3, j / 3) = frame(j % 3, j / 3);
    for (int j = 0; j < 3; j++)v.pos[j] = pos(j);
    v.eta = setup.n.A * lastS + setup.n.B;
    v.omega1 = setup.omega1.A * lastS + setup.omega1.B;
    path.push_back(v);
  }
  
  if(lastPart&&setup.lastPart)
  {
    auto [pos, frame] = ribbon.PosFrame(s1);
    ObjRibbonVertex v;
    for (int j = 0; j < 9; j++)v.frame(j % 3, j / 3) = frame(j % 3, j / 3);
    for (int j = 0; j < 3; j++)v.pos[j] = pos(j);
    v.eta = setup.n.A * s1 + setup.n.B;
    v.omega1 = setup.omega1.A * s1 + setup.omega1.B;
    path.push_back(v);
  }
  lastS-=s1;
  setup.sout = setup.s0 + s1;
  std::tie(setup.posF, setup.frameF) = ribbon.PosFrame(s1);
}
