/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef RIBBONPATHGENERATOR_H
#define RIBBONPATHGENERATOR_H

#include <vector>
#include <SuperRibbonCore/SuperRibbon.hpp>
#include <MixedSRCore/MixedSuperRibbon.hpp>
#include <optional>
#include "ObjRibbonMesh.hpp"

/**
 * Convinient class to extract a path from a ribbon. A path is a set of point (position, frame) along the centerline
 */
class RibbonPathGenerator
{
public:
  std::vector<ObjRibbonVertex> genShapeFrom(SuperRibbon ribbon, std::optional<double> meshDiscretisationLength);///< meshDiscreteLength is the sampling distance along the centerline
  std::vector<ObjRibbonVertex> genShapeFrom(MixedSuperRibbon ribbon, std::optional<double> meshDiscretisationLength);///< meshDiscreteLength is the sampling distance along the centerline
private:
  std::vector<ObjRibbonVertex> path;
  double meshDiscreteLength;
  
  struct RibbonPartSetup {
    uint partnb;
    real s0;
    real sout;
    real smax;
    real segmentLength;
    real ribbonWidth;
    bool lastPart = false;
    LinearScalar omega1;
    LinearScalar n;
    Eigen::Vector3r posOr;
    Eigen::Vector3r posF;
    Eigen::Matrix3r frameOr;
    Eigen::Matrix3r frameF;
  };
  
  void setupRibbonPart(RibbonPartSetup &part, double& lastS, bool lastPart);
  void computeSegment(SuperRibbon& data, int segmentNumber, Eigen::Vector3r& pos, Eigen::Matrix3r& frame, double& lastS, bool lastPart);
  void computeSegment(const LinearScalar& omega1, const LinearScalar& eta, const double& length, const double& width, Eigen::Vector3r& pos, Eigen::Matrix3r& frame, double& lastS, bool lastPart);
};

#endif // RIBBONPATHGENERATOR_H
