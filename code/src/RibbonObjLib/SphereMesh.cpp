/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "SphereMesh.hpp"

SphereMesh::SphereMesh()
{
  name="sphere_"+std::to_string(idx++);
}

std::string SphereMesh::getObjName()
{
  return name;
}

void SphereMesh::setResolution(int res)
{
  sphRes=res;
}

void SphereMesh::setFrom(Eigen::Vector3d center, double radius)
{
  this->center=center;
  this->radius=radius;
}

void SphereMesh::generate()
{
  std::function<void(int, size_t, size_t, size_t)> setFaceInv = [&](int tri, size_t id1, size_t id2, size_t id3) {
        setFace(tri,id3,id2,id1);
      };
  std::function<void(int, int, size_t, size_t, size_t)> setFaceOffset = [&](int tri, int offset, size_t id1, size_t id2, size_t id3) {
        setFace(tri, id1+offset, id2+offset, id3+offset);
      };
  std::function<void(int, int, size_t, size_t, size_t)> setFaceInvOffset = [&](int tri, int offset, size_t id1, size_t id2, size_t id3) {
        setFace(tri, id3+offset, id2+offset, id1+offset);
      };
  std::function<Eigen::Vector3d(const Eigen::Vector3d&)> getOneOrthDir = [](const Eigen::Vector3d& dir)->Eigen::Vector3d {
        Eigen::Vector3d orthy(0., 1., 0.), orthz(0., 0., 1.);
        double oy = dir.dot(orthy);
        double oz = dir.dot(orthz);
        Eigen::Vector3d orth = (std::abs(oy)<std::abs(oz)) ? orthy : orthz;
        orth -= dir.dot(orth)*dir;
        orth.normalize();
        return orth;
      };    
  size_t t=sphRes-1;
  if(t<2)return;
  int vtubeBeginOffset=t*sphRes;
  int vtubeBeginMiddleID=vtubeBeginOffset;
  int vtubeEndMiddleID=vtubeBeginOffset+1;
  int ftubeBeginOffset=(2*t-2)*sphRes;
  int ftubeEndOffset=(2*t-1)*sphRes;
  resize(vtubeEndMiddleID+1,2*t*sphRes);
  
    
  for(size_t i=0;i<sphRes-1;i++)
  {
    double theta=((i+1)*M_PI)/sphRes;
//     double locRad=radius*sin(theta);
//     double locH=radius*cos(theta)
    Eigen::Vector3d vpos, vnorm;
    const int voffset=i*sphRes;
    for(int j=0;j<sphRes;j++)
    {
      vnorm << cos(j*2*M_PI/sphRes)*sin(theta),sin(j*2*M_PI/sphRes)*sin(theta),cos(theta);
      
      vpos = center + radius*vnorm;
      setVertex(voffset+j, vpos, vnorm);
      setFaceInvOffset((2*i)*sphRes+j, voffset, j, (j+1)%sphRes, sphRes+j);
      setFaceOffset((2*i+1)*sphRes+j, voffset, (j+1)%sphRes, sphRes+j, sphRes+(j+1)%sphRes);
    }
  }
  //begin tube
  {
    Eigen::Vector3d vpos, vnorm;
    vnorm=Eigen::Vector3d(0.,0.,1.);
    vpos=center+Eigen::Vector3d(0.,0.,radius);
    setVertex(vtubeBeginMiddleID, vpos, vnorm);
    for(int j=0;j<sphRes;j++)
    {
      setFace(ftubeBeginOffset+j, j, (j+1)%sphRes, vtubeBeginMiddleID);
    }
  }
  //end tube
  {
    Eigen::Vector3d vpos, vnorm;
    vnorm=Eigen::Vector3d(0.,0.,-1.);
    vpos=center+Eigen::Vector3d(0.,0.,-radius);
    setVertex(vtubeEndMiddleID, vpos, vnorm);
    const int voffset=(sphRes-2)*sphRes;
    for(int j=0;j<sphRes;j++)
    {
      setFaceInv(ftubeEndOffset+j, voffset+j, voffset+(j+1)%sphRes, vtubeEndMiddleID);
    }
  }
}

unsigned int SphereMesh::idx = 0;
