/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef SPHEREMESH_H
#define SPHEREMESH_H

#include <ObjLib/TriMesh.hpp>

class SphereMesh : public libobj::TriMesh
{
public:
  SphereMesh();
  virtual std::string getObjName() override;
  void setFrom(Eigen::Vector3d center, double radius);
  void setResolution(int res);
  void generate();
private:
  std::string name;
  static unsigned int idx;
  Eigen::Vector3d center;
  double radius;
  int sphRes=10;
};

#endif // SPHEREMESH_H
