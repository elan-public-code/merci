/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef SUPERRIBBONMESHER_H
#define SUPERRIBBONMESHER_H

#include <vector>
#include <SuperRibbonCore/SuperRibbon.hpp>
#include <optional>
#include "ObjRibbonMesh.hpp"

/**
 * The class you will use to help saving ribbon as mesh.
 * Two colors are used to show the segments in different colors
 * You do not need to worry about materials… 
 * */
class SuperRibbonMesher
{
public:
  SuperRibbonMesher();
  SuperRibbonMesher(Eigen::Vector3i maincolor, Eigen::Vector3i secondcolor);///<Colors in RGB format [0-255] interval
  void setRibbonName(std::string name);///<Identify your ribbon with a name, or get a ribbonXXX name 
  void set(SuperRibbon ribbon);///< This is important to do, call this fonction with a non empty ribbon
  void setMeshDiscreteLength(std::optional<double> mdl);///< distance of abscissa between discret point
  void setRibbonThickness(std::optional<double> rt); ///< Thickness is a visual story
  void clear();///< Only free the shape of the ribbon, not the name and colors
  void setColors(Eigen::Vector3i main, Eigen::Vector3i second);///<Colors in RGB format [0-255] interval
  void save(libobj::ObjFileWriter& os);///< If you do not understand this, go to the doctor ! Please be sure to call set before to call this.
  void showSubsegmentInsteadOfSegments();///< Should not use in general, this show when the power serie is cut, see PhD or CMAME
  void showAsUsual();///<Reverse of showSubsegmentInsteadOfSegments
private:
  struct RibbonPart {
    RibbonPart(ObjRibbonMesh m,bool mc):mesh(m),maincolor(mc){}
    ObjRibbonMesh mesh;
    bool maincolor;
  };
    bool showSubseg=false;

  struct RibbonPartSetup {
    uint partnb;
    real s0;
    real sout;
    real smax;
    real segmentLength;
    real ribbonWidth;
    bool lastPart = false;
    LinearScalar omega1;
    LinearScalar n;
    Eigen::Vector3r posOr;
    Eigen::Vector3r posF;
    Eigen::Matrix3r frameOr;
    Eigen::Matrix3r frameF;
  };
  std::optional<double> meshDiscreteLength;
  std::optional<double> ribbonThickness;
  Eigen::Vector3i meshMainColor;
  Eigen::Vector3i meshSecondColor;
  std::string ribbonName;
  std::vector<RibbonPart> theRibbon;
  std::vector<ObjRibbonVertex> setupRibbonPart(RibbonPartSetup &);
  void computeSegment(SuperRibbon& data, int segmentNumber, Eigen::Vector3r& pos, Eigen::Matrix3r& frame, int& MeshOffset);
};

#endif // SUPERRIBBONMESHER_H
