/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "TubeMesh.hpp"

TubeMesh::TubeMesh()
{
  name="tube_"+std::to_string(idx++);
}

std::string TubeMesh::getObjName()
{
  return name;
}

void TubeMesh::setDiameter(double d)
{
  diameter=d;
}

void TubeMesh::addPoint(Eigen::Vector3d pt)
{
  path.push_back(pt);
}

void TubeMesh::setResolution(int res)
{
  if(res>2)cylinderRes=res;
}

void TubeMesh::generate()
{
  std::function<void(int, size_t, size_t, size_t)> setFaceInv = [&](int tri, size_t id1, size_t id2, size_t id3) {
        setFace(tri,id3,id2,id1);
      };
  std::function<void(int, int, size_t, size_t, size_t)> setFaceOffset = [&](int tri, int offset, size_t id1, size_t id2, size_t id3) {
        setFace(tri, id1+offset, id2+offset, id3+offset);
      };
  std::function<void(int, int, size_t, size_t, size_t)> setFaceInvOffset = [&](int tri, int offset, size_t id1, size_t id2, size_t id3) {
        setFace(tri, id3+offset, id2+offset, id1+offset);
      };
  std::function<Eigen::Vector3d(const Eigen::Vector3d&)> getOneOrthDir = [](const Eigen::Vector3d& dir)->Eigen::Vector3d {
        Eigen::Vector3d orthy(0., 1., 0.), orthz(0., 0., 1.);
        double oy = dir.dot(orthy);
        double oz = dir.dot(orthz);
        Eigen::Vector3d orth = (std::abs(oy)<std::abs(oz)) ? orthy : orthz;
        orth -= dir.dot(orth)*dir;
        orth.normalize();
        return orth;
      };    
  size_t t=path.size();
  if(t<2)return;
  int vtubeBeginOffset=t*cylinderRes;
  int vtubeBeginMiddleID=(t+2)*cylinderRes;
  int vtubeEndOffset=(t+1)*cylinderRes;
  int vtubeEndMiddleID=(t+2)*cylinderRes+1;
  int ftubeBeginOffset=(2*t-2)*cylinderRes;
  int ftubeEndOffset=(2*t-1)*cylinderRes;
  resize((t+2)*cylinderRes+2,2*t*cylinderRes);
  
  Eigen::Vector3d ori=path.at(0);
  Eigen::Vector3d dir=(path.at(1)-ori).normalized();
  Eigen::Vector3d orth = getOneOrthDir(dir);
  Eigen::Vector3d orth2 = dir.cross(orth);
    
  for(size_t i=0;i<t-1;i++)
  {
    ori=path.at(i);
    dir=(path.at(i+1)-ori).normalized();
    orth-=dir.dot(orth)*dir;
    orth.normalize();
    orth2=dir.cross(orth);
    Eigen::Vector3d vpos, vnorm;
    const int voffset=i*cylinderRes;
    for(int j=0;j<cylinderRes;j++)
    {
      vnorm = cos(j*2*M_PI/cylinderRes)*orth + sin(j*2*M_PI/cylinderRes)*orth2;
      vpos = ori + diameter*vnorm;
      setVertex(voffset+j, vpos, vnorm);
      setFaceOffset((2*i)*cylinderRes+j, voffset, j, (j+1)%cylinderRes, cylinderRes+j);
      setFaceInvOffset((2*i+1)*cylinderRes+j, voffset, (j+1)%cylinderRes, cylinderRes+j, cylinderRes+(j+1)%cylinderRes);
    }
  }
  //last ring
  {
    ori=path.at(t-1);
    dir=(ori-path.at(t-2)).normalized();
    Eigen::Vector3d orth = getOneOrthDir(dir);
    Eigen::Vector3d orth2 = dir.cross(orth);
    Eigen::Vector3d vpos, vnorm;
    for(int j=0;j<cylinderRes;j++)
    {
      vnorm = cos(j*2*M_PI/cylinderRes)*orth + sin(j*2*M_PI/cylinderRes)*orth2;
      vpos = ori + diameter*vnorm;
      setVertex((t-1)*cylinderRes+j, vpos, vnorm);
    }
  }
  //begin tube
  {
    const Eigen::Vector3d ori=path.at(0);
    const Eigen::Vector3d dir=(path.at(1)-ori).normalized();
    Eigen::Vector3d orth = getOneOrthDir(dir);
    Eigen::Vector3d orth2 = dir.cross(orth);
    Eigen::Vector3d vpos, vnorm;
    for(int j=0;j<cylinderRes;j++)
    {
      vnorm = cos(j*2*M_PI/cylinderRes)*orth + sin(j*2*M_PI/cylinderRes)*orth2;
      vpos = ori + diameter*vnorm;
      setVertex(vtubeBeginOffset+j, vpos, dir);
      setFace(ftubeBeginOffset+j, vtubeBeginOffset+j, vtubeBeginOffset+(j+1)%cylinderRes, vtubeBeginMiddleID);
    }
    setVertex(vtubeBeginMiddleID, vpos, dir);
  }
  //end tube
  {
    const Eigen::Vector3d ori=path.at(t-1);
    const Eigen::Vector3d dir=(path.at(t-2)-ori).normalized();
    Eigen::Vector3d orth = getOneOrthDir(dir);
    Eigen::Vector3d orth2 = dir.cross(orth);
    Eigen::Vector3d vpos, vnorm;
    for(int j=0;j<cylinderRes;j++)
    {
      vnorm = cos(j*2*M_PI/cylinderRes)*orth + sin(j*2*M_PI/cylinderRes)*orth2;
      vpos = ori + diameter*vnorm;
      setVertex(vtubeEndOffset+j, vpos, dir);
      setFaceInv(ftubeEndOffset+j, vtubeEndOffset+j, vtubeEndOffset+(j+1)%cylinderRes, vtubeEndMiddleID);
    }
    setVertex(vtubeEndMiddleID, vpos, dir);
  }
}

unsigned int TubeMesh::idx = 0;
