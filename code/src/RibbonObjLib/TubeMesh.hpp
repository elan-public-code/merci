/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef TUBEMESH_H
#define TUBEMESH_H
#include <ObjLib/TriMesh.hpp>

/**
 * Important, call generate before to send it to the ObjFileWriter
 * No, the diameter is constant.
 */
class TubeMesh : public libobj::TriMesh
{
public:
  TubeMesh();
  virtual std::string getObjName() override;
  void addPoint(Eigen::Vector3d pt);
  void setDiameter(double d);
  void setResolution(int res);
  void generate();
private:
  std::string name;
  static unsigned int idx;
  std::vector<Eigen::Vector3d> path;
  double diameter=1.;
  int cylinderRes=10;
};

#endif // TUBEMESH_H
