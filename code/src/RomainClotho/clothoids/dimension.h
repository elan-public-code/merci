//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#ifndef CLOC_CLOTHOIDS_DIMENSION_H_INCLUDED
#define CLOC_CLOTHOIDS_DIMENSION_H_INCLUDED

/**
 * \file dimension.h
 * \brief This file defines (and check) macros defining the dimension
 * of ambiant space.
 *
 * \author Romain Casati
 * \date   24th Jan. 2013
 */

/**
 * \def CLOC_CLOTHOIDS_SPACE_DIM
 * \brief If not already defined, dimension is set to 3 (by default).
 */
#ifndef CLOC_CLOTHOIDS_SPACE_DIM
#define CLOC_CLOTHOIDS_SPACE_DIM  3
#endif

// Testing if the dimension is 2 or 3
#define DIM  CLOC_CLOTHOIDS_SPACE_DIM
#if DIM != 2 && DIM != 3               //(2*DIM-5)*(2*DIM-5) != 1
#error Dimension of ambiant space must be 2 or 3 !
#endif
#undef DIM

/**
 * \def CLOC_CLOTHOIDS_DOF_DIM
 * \brief Dimension of degrees of freedom (kappa) (1 for DIM=1, 3 for DIM=3).
 */
#define CLOC_CLOTHOIDS_DOF_DIM \
     ((CLOC_CLOTHOIDS_SPACE_DIM*(CLOC_CLOTHOIDS_SPACE_DIM-1))/2)

#endif // ! CLOC_CLOTHOIDS_DIMENSION_H_INCLUDED
