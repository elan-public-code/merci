//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#include "kinematical_params.h"

#include "core/mem.h"
#include "core/algorithms.h"

#define DIM CLOC_CLOTHOIDS_SPACE_DIM

#define DEF_FILE "kinematical_params.def"

void cloc_kineparams_init(cloc_kineparams_t* params)
{
#define EXPAND_KINE_PARAM(n,t,v)                \
     params->n = v;
#include DEF_FILE
#undef EXPAND_KINE_PARAM
     
     params->clamping_pos = cloc_calloc(DIM, cloc_scalar_t);
     cloc_fillzeros(params->clamping_pos, DIM);
     params->clamping_frame = cloc_calloc(DIM*DIM, cloc_scalar_t);
     cloc_fillzeros(params->clamping_frame, DIM*DIM);

     params->curvature = cloc_calloc(1, cloc_pacurvature_t);
     params->curvature_dot = cloc_calloc(1, cloc_pacurvature_t);
     
     const cloc_scalar_t lengths[] = {0.21};
#if DIM == 2
     const cloc_scalar_t curvatures[] = {0., 0.};
#else // 3d
     const cloc_scalar_t curvatures[] = {0., 0., 0., 0., 0., 0.};
#endif

     cloc_pacurvature_init(params->curvature, lengths, curvatures, 1);
     cloc_pacurvature_init(params->curvature_dot, lengths, curvatures, 1);
}

void cloc_kineparams_destroy(cloc_kineparams_t* params)
{
     cloc_free(params->clamping_pos);
     cloc_free(params->clamping_frame);
     cloc_pacurvature_destroy(params->curvature);
     cloc_free(params->curvature);
     cloc_pacurvature_destroy(params->curvature_dot);
     cloc_free(params->curvature_dot);
}

#define EXPAND_KINE_PARAM(n,t,v)                                        \
     t cloc_kineparams_get_##n(const cloc_kineparams_t* params) { return params->n; }
#include DEF_FILE
#undef EXPAND_KINE_PARAM

#define EXPAND_KINE_PARAM(n,t,v)                                        \
     void cloc_kineparams_set_##n(cloc_kineparams_t* params, t val) { params->n = val; }
#include DEF_FILE
#undef EXPAND_KINE_PARAM

void cloc_kineparams_change_curvature(cloc_kineparams_t* params,
                                      const cloc_pacurvature_t* pac)
{
     cloc_pacurvature_destroy(params->curvature);
     cloc_pacurvature_cpy(params->curvature, pac);
}

void cloc_kineparams_change_curvaturedot(cloc_kineparams_t* params,
                                         const cloc_pacurvature_t* pac_dot)
{
     cloc_pacurvature_destroy(params->curvature_dot);
     cloc_pacurvature_cpy(params->curvature_dot, pac_dot);
}

#undef DEF_FILE
