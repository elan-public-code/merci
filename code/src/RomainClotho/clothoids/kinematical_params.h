//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#ifndef CLOC_CLOTHOIDS_KINEMATICAL_PARAMS_H_INCLUDED
#define CLOC_CLOTHOIDS_KINEMATICAL_PARAMS_H_INCLUDED

/**
 * \file kinematical_params.h
 * \brief 
 *
 * \author Romain Casati
 * \date   11th March. 2013
 */

#include "pacurvature.h"

#define DEF_FILE "kinematical_params.def"

/**
 * \struct cloc_kineparams_t
 * \brief Definition of kinematical parameters
 */
typedef struct
{
#define EXPAND_KINE_PARAM(n,t,v)                \
     t n;
#include DEF_FILE
#undef EXPAND_KINE_PARAM
} cloc_kineparams_t;

/**
 * Init parameters.
 */
void cloc_kineparams_init(cloc_kineparams_t* params);

/**
 * Destroy parameters.
 */
void cloc_kineparams_destroy(cloc_kineparams_t* params);

/**
 * Get the value of a specific kinematical parameter.
 */
#define EXPAND_KINE_PARAM(n,t,v)                                        \
     t cloc_kineparams_get_##n(const cloc_kineparams_t* params);
#include DEF_FILE
#undef EXPAND_KINE_PARAM

/**
 * Set the value of a specific kinematical parameter.
 */
#define EXPAND_KINE_PARAM(n,t,v)                                         \
     void cloc_kineparams_set_##n(cloc_kineparams_t* params, t val);
#include DEF_FILE
#undef EXPAND_KINE_PARAM

/**
 * \brief Change curavture.
 */
void cloc_kineparams_change_curvature(cloc_kineparams_t* params,
                                      const cloc_pacurvature_t* pac);

/**
 * \brief Change curavture dot.
 */
void cloc_kineparams_change_curvaturedot(cloc_kineparams_t* params,
                                         const cloc_pacurvature_t* pac_dot);

#undef DEF_FILE

#endif // ! CLOC_CLOTHOIDS_KINEMATICAL_PARAMS_H_INCLUDED
