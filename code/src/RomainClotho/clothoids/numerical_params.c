//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#include "numerical_params.h"

#define DEF_FILE "numerical_params.def"

void cloc_numparams_init(cloc_numparams_t* params)
{
#define EXPAND_NUM_PARAM(n,t,v)                 \
     params->n = v;
#include DEF_FILE
#undef EXPAND_NUM_PARAM
}

void cloc_numparams_destroy(cloc_numparams_t* params)
{
     (void) params;
}

#define EXPAND_NUM_PARAM(n,t,v)                                         \
     t cloc_numparams_get_##n(const cloc_numparams_t* params) { return params->n; }
#include DEF_FILE
#undef EXPAND_NUM_PARAM

#define EXPAND_NUM_PARAM(n,t,v)                                         \
     void cloc_numparams_set_##n(cloc_numparams_t* params, t val) { params->n = val; }
#include DEF_FILE
#undef EXPAND_NUM_PARAM

#undef DEF_FILE
