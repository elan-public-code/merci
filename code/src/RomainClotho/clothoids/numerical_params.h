//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#ifndef CLOC_CLOTHOIDS_NUMERICAL_PARAMS_H_INCLUDED
#define CLOC_CLOTHOIDS_NUMERICAL_PARAMS_H_INCLUDED

/**
 * \file numerical_params.h
 * \brief Partameters relative to discretization.
 *
 * \author Romain Casati
 * \date   11th March. 2013
 */

#include "core/definitions.h"

#define DEF_FILE "numerical_params.def"

/**
 * \struct cloc_numparams_t
 * \brief Definition of numerical parameters
 */
typedef struct
{
#define EXPAND_NUM_PARAM(n,t,v)                 \
     t n;
#include DEF_FILE
#undef EXPAND_NUM_PARAM
} cloc_numparams_t;

/**
 * Init parameters.
 */
void cloc_numparams_init(cloc_numparams_t* params);

/**
 * Destroy parameters.
 */
void cloc_numparams_destroy(cloc_numparams_t* params);

/**
 * Get the value of a specific numerical parameter.
 */
#define EXPAND_NUM_PARAM(n,t,v)                                         \
     t cloc_numparams_get_##n(const cloc_numparams_t* params);
#include DEF_FILE
#undef EXPAND_NUM_PARAM

/**
 * Set the value of a specific numerical parameter.
 */
#define EXPAND_NUM_PARAM(n,t,v)                                         \
     void cloc_numparams_set_##n(cloc_numparams_t* params, t val);
#include DEF_FILE
#undef EXPAND_NUM_PARAM

#undef DEF_FILE

#endif // ! CLOC_CLOTHOIDS_NUMERICAL_PARAMS_H_INCLUDED
