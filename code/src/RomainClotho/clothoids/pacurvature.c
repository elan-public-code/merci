//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#include "pacurvature.h"

#include "core/algorithms.h"

#include "core/mem.h"
#include <assert.h>

#define DOF_DIM CLOC_CLOTHOIDS_DOF_DIM

void cloc_pacurvature_init(cloc_pacurvature_t* pac,
                           const cloc_scalar_t* lengths,
                           const cloc_scalar_t* curvatures,
                           unsigned npieces)
{
     pac->nnodes = npieces+1;
     // curvatures
     pac->curvatures = cloc_calloc(DOF_DIM*(npieces+1),cloc_scalar_t);
     cloc_cpy(pac->curvatures, curvatures, DOF_DIM*(npieces+1), cloc_scalar_t);
     // Nodes ticks
     pac->sis = cloc_calloc(npieces+1,cloc_scalar_t);
     pac->sis[0] = 0.;
     for(unsigned i=0; i<npieces; ++i)
     {
          assert( lengths[i] > 0. );
          pac->sis[i+1] = pac->sis[i] + lengths[i];
     }
}

void cloc_pacurvature_destroy(cloc_pacurvature_t* pac)
{
     cloc_free(pac->curvatures);
     cloc_free(pac->sis);
}

void cloc_pacurvature_cpy(cloc_pacurvature_t* dst, const cloc_pacurvature_t* src)
{
     const unsigned nnodes = src->nnodes;
     dst->nnodes = nnodes;
     dst->curvatures = cloc_calloc(nnodes*DOF_DIM, cloc_scalar_t);
     cloc_cpy(dst->curvatures, src->curvatures, nnodes*DOF_DIM, cloc_scalar_t);
     dst->sis = cloc_calloc(nnodes, cloc_scalar_t);
     cloc_cpy(dst->sis, src->sis, nnodes, cloc_scalar_t);
}

cloc_scalar_t cloc_pacurvature_get(const cloc_pacurvature_t* pac,
                                   unsigned inode, cloc_curvindex_t k)
{
     return pac->curvatures[DOF_DIM*inode+k];
}

cloc_scalar_t cloc_pacurvature_getat(const cloc_pacurvature_t* pac,
                                     cloc_scalar_t s, cloc_curvindex_t k)
{
     const unsigned index = cloc_index_of_segment(pac->sis, pac->nnodes, s);
     const cloc_scalar_t u = (s - pac->sis[index]) / cloc_pacurvature_length(pac,index);
     return cloc_pacurvature_get(pac, index+1, k)*u
          + cloc_pacurvature_get(pac, index, k)*(1.-u);
}

unsigned cloc_pacurvature_nsegments(const cloc_pacurvature_t* pac)
{
     return pac->nnodes-1;
}

unsigned cloc_pacurvature_nnodes(const cloc_pacurvature_t* pac)
{
     return pac->nnodes;
}

cloc_scalar_t cloc_pacurvature_length(const cloc_pacurvature_t* pac,
                                      unsigned inode)
{
     return pac->sis[inode+1]-pac->sis[inode];
}

cloc_scalar_t cloc_pacurvature_totallength(const cloc_pacurvature_t* pac)
{
     return pac->sis[pac->nnodes-1];
}

void cloc_pacurvature_set(cloc_pacurvature_t* pac,
                          unsigned inode, cloc_curvindex_t k,
                          cloc_scalar_t val)
{
     pac->curvatures[DOF_DIM*inode+k] = val;
}

cloc_scalar_t cloc_pacurvature_slope(const cloc_pacurvature_t* pac,
                                     unsigned isegment, cloc_curvindex_t k)
{
     const cloc_scalar_t length = pac->sis[isegment+1]-pac->sis[isegment];
     const cloc_scalar_t diff = pac->curvatures[DOF_DIM*(isegment+1)+k]
                                 - pac->curvatures[DOF_DIM*isegment+k];
     return diff/length;
}
