//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#ifndef CLOC_CLOTHOIDS_PACURVATURE_H_INCLUDED
#define CLOC_CLOTHOIDS_PACURVATURE_H_INCLUDED

#include "core/definitions.h"
#include "dimension.h"

/**
 * \file pacurvature.h
 * \brief Implementation of piecewise affine curvature profile.
 * 
 * \author Romain Casati
 * \date   29th Jan. 2013
 */

/**
 * \enum cloc_curvindex_t
 * \brief Index of curvature(s).
 */
typedef enum
{
#if CLOC_CLOTHOIDS_SPACE_DIM == 3 // 3D case
     CLOC_TWIST=0, CLOC_CURVATURE1=1, CLOC_CURVATURE2=2
#else // 2D case
     CLOC_CURVATURE=0
#endif
} cloc_curvindex_t;

/**
 * \struct cloc_pacurvature_t
 * \brief The structure representing piecewise affine curvature.
 */
typedef struct
{
     cloc_scalar_t* restrict curvatures;
     cloc_scalar_t* restrict sis;
     unsigned nnodes;
} cloc_pacurvature_t;

/**
 * \brief Constructor.
 *
 * \param pac The (struct-allocated) pointer to the piecewise affine curvature.
 * \param lengths Lengths of each segments (copied).
 * \param curvatures Curvatures at nodes (copied).
 * \param npieces Number of segments (size of lengths).
 *
 * \warning In the 3D case, curvatures must be strored consecutively (curvatures[3*i+j] is the curvature j at node i).
 */
void cloc_pacurvature_init(cloc_pacurvature_t* pac,
                           const cloc_scalar_t* lengths,
                           const cloc_scalar_t* curvatures,
                           unsigned npieces);

/**
 * \brief Destructor.
 */
void cloc_pacurvature_destroy(cloc_pacurvature_t* pac);

/**
 * \brief Make a copy of the given curvature.
 *
 * \warning dst must not be init'ed (but destroyed).
 */
void cloc_pacurvature_cpy(cloc_pacurvature_t* dst,
                          const cloc_pacurvature_t* src);

/**
 * \brief Get a curvature.
 * \param pac The piecewise affine curvature.
 * \param inode The node you look for the curvature at.
 * \param k The curvature index (not really usefull in the 2D case...).
 */
cloc_scalar_t cloc_pacurvature_get(const cloc_pacurvature_t* pac,
                                   unsigned inode, cloc_curvindex_t k);

/**
 * \brief Get a curvature at given s.
 * \param pac The piecewise affine curvature.
 * \param s Where to compute the curvature.
 * \param k The curvature index (not really usefull in the 2D case...).
 */
cloc_scalar_t cloc_pacurvature_getat(const cloc_pacurvature_t* pac,
                                     cloc_scalar_t s, cloc_curvindex_t k);

/**
 * \brief Get number of segments.
 */
unsigned cloc_pacurvature_nsegments(const cloc_pacurvature_t* pac);

/**
 * \brief Get number of nodes.
 */
unsigned cloc_pacurvature_nnodes(const cloc_pacurvature_t* pac);

/**
 * \brief Get the ith length.
 */
cloc_scalar_t cloc_pacurvature_length(const cloc_pacurvature_t* pac,
                                      unsigned inode);

/**
 * \brief Get the total length.
 */
cloc_scalar_t cloc_pacurvature_totallength(const cloc_pacurvature_t* pac);

/**
 * \brief Set a curvature.
 * \param pac The piecewise affine curvature.
 * \param inode The node you look for the curvature at.
 * \param k The curvature index (not really usefull in the 2D case...).
 * \param val The new curvature value.
 */
void cloc_pacurvature_set(cloc_pacurvature_t* pac,
                          unsigned inode, cloc_curvindex_t k,
                          cloc_scalar_t val);

/**
 * \brief Get curvature slope of given segment.
 * \param pac The piecewise affine curvature.
 * \param isegment The index of the segment you look for the slope at.
 * \param k The curvature index (not really usefull in the 2D case...).
 *
 * \warning This is the index of the segment that is requested here,
 * not the node one.
 */
cloc_scalar_t cloc_pacurvature_slope(const cloc_pacurvature_t* pac,
                                     unsigned isegment, cloc_curvindex_t k);

#endif // ! CLOC_CLOTHOIDS_PACURVATURE_H_INCLUDED
