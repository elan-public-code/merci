//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#include "physical_params.h"

#include "core/mem.h"
#include "core/algorithms.h"

#define DIM CLOC_CLOTHOIDS_SPACE_DIM

#define DEF_FILE "physical_params.def"

void cloc_phyparams_init(cloc_phyparams_t* params)
{
#define EXPAND_PHY_PARAM(n,t,v)                 \
     params->n = v;
#include DEF_FILE
#undef EXPAND_PHY_PARAM

     params->gravity = cloc_calloc(DIM, cloc_scalar_t);
     cloc_fillzeros(params->gravity, DIM);
     params->natural_curvature = cloc_calloc(1, cloc_pacurvature_t);
     const cloc_scalar_t lengths[] = {0.21};
#if DIM == 2
     const cloc_scalar_t curvatures[] = {0., 0.};
#else // 3d
     const cloc_scalar_t curvatures[] = {0., 0., 0., 0., 0., 0.};
#endif

     cloc_pacurvature_init(params->natural_curvature, lengths, curvatures, 1);
}

void cloc_phyparams_destroy(cloc_phyparams_t* params)
{
     cloc_free(params->gravity);
     cloc_pacurvature_destroy(params->natural_curvature);
     cloc_free(params->natural_curvature);
}

#define EXPAND_PHY_PARAM(n,t,v)                 \
     t cloc_phyparams_get_##n(const cloc_phyparams_t* params) { return params->n; }
#include DEF_FILE
#undef EXPAND_PHY_PARAM

#define EXPAND_PHY_PARAM(n,t,v)                 \
     void cloc_phyparams_set_##n(cloc_phyparams_t* params, t val) { params->n = val; }
#include DEF_FILE
#undef EXPAND_PHY_PARAM

void cloc_phyparams_change_natural_curvature(cloc_phyparams_t* params,
                                             const cloc_pacurvature_t* nat_pac)
{
     cloc_pacurvature_destroy(params->natural_curvature);
     cloc_pacurvature_cpy(params->natural_curvature, nat_pac);
}

#undef DEF_FILE
