//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#ifndef CLOC_CLOTHOIDS_PHYSICAL_PARAMS_H_INCLUDED
#define CLOC_CLOTHOIDS_PHYSICAL_PARAMS_H_INCLUDED

/**
 * \file physical_params.h
 * \brief 
 *
 * \author Romain Casati
 * \date   11th March. 2013
 */

#include "pacurvature.h"

#define DEF_FILE "physical_params.def"

/**
 * \struct cloc_phyparams_t
 * \brief Definition of physical parameters
 */
typedef struct
{
#define EXPAND_PHY_PARAM(n,t,v)                 \
     t n;
#include DEF_FILE
#undef EXPAND_PHY_PARAM
} cloc_phyparams_t;

/**
 * Init parameters.
 */
void cloc_phyparams_init(cloc_phyparams_t* params);

/**
 * Destroy parameters.
 */
void cloc_phyparams_destroy(cloc_phyparams_t* params);

/**
 * Get the value of a specific physical parameter.
 */
#define EXPAND_PHY_PARAM(n,t,v)                 \
     t cloc_phyparams_get_##n(const cloc_phyparams_t* params);
#include DEF_FILE
#undef EXPAND_PHY_PARAM

/**
 * Set the value of a specific physical parameter.
 */
#define EXPAND_PHY_PARAM(n,t,v)                 \
     void cloc_phyparams_set_##n(cloc_phyparams_t* params, t val);
#include DEF_FILE
#undef EXPAND_PHY_PARAM

/**
 * \brief Change natural curvature.
 */
void cloc_phyparams_change_natural_curvature(cloc_phyparams_t* params,
                                             const cloc_pacurvature_t* nat_pac);

#undef DEF_FILE

#endif // ! CLOC_CLOTHOIDS_PHYSICAL_PARAMS_H_INCLUDED
