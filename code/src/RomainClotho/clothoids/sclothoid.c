//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#include "sclothoid.h"

#include "core/mem.h"
#include "core/algorithms.h"

#include <cblas.h>
#include <clapack.h>
#include <assert.h>
#include <math.h>

#ifndef M_PI
#define M_PI           3.14159265358979323846
#endif

#define DIM     CLOC_CLOTHOIDS_SPACE_DIM
#define DOF_DIM CLOC_CLOTHOIDS_DOF_DIM

static void set_pse_not_init(cloc_sclothoid_t* ssc)
{
     ssc->pse.nnodes = 0; // trick
}

static bool is_pse_init(const cloc_sclothoid_t* ssc)
{
     return ssc->pse.nnodes != 0;
}

void cloc_sclothoid_init(cloc_sclothoid_t* ssc)
{
     cloc_kineparams_init(&(ssc->kparams));
     cloc_phyparams_init(&(ssc->pparams));
     cloc_numparams_init(&(ssc->nparams));
     set_pse_not_init(ssc);
     ssc->durty_pse = true;
}

void cloc_sclothoid_destroy(cloc_sclothoid_t* ssc)
{
     cloc_kineparams_destroy(&(ssc->kparams));
     cloc_phyparams_destroy(&(ssc->pparams));
     cloc_numparams_destroy(&(ssc->nparams));
     if(is_pse_init(ssc)) cloc_clopse_destroy(&(ssc->pse));
}

#define EXPAND_KINE_PARAM(n,t,v)                                \
     t cloc_sclothoid_get_##n(const cloc_sclothoid_t* ssc)      \
     {                                                          \
          return cloc_kineparams_get_##n(&(ssc->kparams));      \
     }
#include "kinematical_params.def"
#undef EXPAND_KINE_PARAM
#define EXPAND_PHY_PARAM(n,t,v)                                 \
     t cloc_sclothoid_get_##n(const cloc_sclothoid_t* ssc)      \
     {                                                          \
          return cloc_phyparams_get_##n(&(ssc->pparams));       \
     }
#include "physical_params.def"
#undef EXPAND_PHY_PARAM
#define EXPAND_NUM_PARAM(n,t,v)                                 \
     t cloc_sclothoid_get_##n(const cloc_sclothoid_t* ssc)      \
     {                                                          \
          return cloc_numparams_get_##n(&(ssc->nparams));       \
     }
#include "numerical_params.def"
#undef EXPAND_NUM_PARAM

cloc_scalar_t cloc_sclothoid_get_length(const cloc_sclothoid_t* ssc)
{
     return cloc_pacurvature_totallength(cloc_sclothoid_get_curvature(ssc));
}

#define EXPAND_KINE_PARAM(n,t,v)                                \
     void cloc_sclothoid_set_##n(cloc_sclothoid_t* ssc, t val)  \
     {                                                          \
          cloc_kineparams_set_##n(&(ssc->kparams), val);        \
     }
#include "kinematical_params.def"
#undef EXPAND_KINE_PARAM
#define EXPAND_PHY_PARAM(n,t,v)                                 \
     void cloc_sclothoid_set_##n(cloc_sclothoid_t* ssc, t val)  \
     {                                                          \
          cloc_phyparams_set_##n(&(ssc->pparams), val);         \
     }
#include "physical_params.def"
#undef EXPAND_PHY_PARAM
#define EXPAND_NUM_PARAM(n,t,v)                                 \
     void cloc_sclothoid_set_##n(cloc_sclothoid_t* ssc, t val)  \
     {                                                          \
          cloc_numparams_set_##n(&(ssc->nparams), val);         \
     }
#include "numerical_params.def"
#undef EXPAND_NUM_PARAM

void cloc_sclothoid_change_curvature(cloc_sclothoid_t* ssc,
                                     const cloc_pacurvature_t* pac)
{
     cloc_kineparams_change_curvature(&(ssc->kparams), pac);
     ssc->durty_pse = true;
}


void cloc_sclothoid_change_curvature_dot(cloc_sclothoid_t* ssc,
                                         const cloc_pacurvature_t* pac_dot)
{
     cloc_kineparams_change_curvaturedot(&(ssc->kparams), pac_dot);
     ssc->durty_pse = true;
}

void cloc_sclothoid_change_natural_curvature(cloc_sclothoid_t* ssc,
                                             const cloc_pacurvature_t* nat_pac)
{
     cloc_phyparams_change_natural_curvature(&(ssc->pparams), nat_pac);
}

typedef struct
{
     unsigned ndof;
     cloc_scalar_t* restrict diag;
     cloc_scalar_t* restrict lowerdiag;
} stiffness_mat_t;

static void stiffness_mat_init(stiffness_mat_t* stiffness,
                               const cloc_sclothoid_t* ssc)
{
     const cloc_pacurvature_t* curvature = cloc_sclothoid_get_curvature(ssc);
     const unsigned nnodes = cloc_pacurvature_nnodes(curvature);
     const unsigned ndof = nnodes*DOF_DIM;

     cloc_scalar_t* restrict diag = cloc_calloc(ndof, cloc_scalar_t);
     cloc_scalar_t* restrict lowerdiag = cloc_calloc(ndof-DOF_DIM, cloc_scalar_t);
     const double a1 = cloc_sclothoid_get_radius1(ssc);
     const double a2 = cloc_sclothoid_get_radius2(ssc);
     const double E = cloc_sclothoid_get_young_modulus(ssc);
     const double sigma = cloc_sclothoid_get_poisson_ratio(ssc);
     const double a12 = a1*a1;
     const double a13 = a12*a1;
     const double a22 = a2*a2;
     const double a23 = a22*a2;
     const double muJ = E/(2.*(1.+sigma))*M_PI*a13*a23/(a12+a22);
     const double EI1 = E*M_PI/4.*a1*a23;
     const double EI2 = E*M_PI/4.*a13*a2;
     const double stiffcoefs[3] = { muJ, EI1, EI2 };

     for(unsigned i=0; i<ndof; ++i)
     {
          const unsigned n = i/DOF_DIM;
          const cloc_scalar_t length1 = (n==0)?0.:cloc_pacurvature_length(curvature, n-1);
          const cloc_scalar_t length2 = (n==(nnodes-1))?0.:cloc_pacurvature_length(curvature, n);
          diag[i] = stiffcoefs[((i%DOF_DIM)+2*DOF_DIM)%3]*(length1+length2)/3.;
     }

     for(unsigned i=0; i<ndof-DOF_DIM; ++i)
     {
          const cloc_scalar_t length = cloc_pacurvature_length(curvature, i/DOF_DIM);
          lowerdiag[i] = stiffcoefs[((i%DOF_DIM)+2*DOF_DIM)%3]*length/6.;
     }

     stiffness->ndof = ndof;
     stiffness->diag = diag;
     stiffness->lowerdiag = lowerdiag;
}

static void stiffness_mat_destroy(stiffness_mat_t* stiffness)
{
     cloc_free(stiffness->diag);
     cloc_free(stiffness->lowerdiag);
}

static void stiffness_mat_addtomat(const stiffness_mat_t* stiffness,
                                   cloc_scalar_t coef,
                                   cloc_scalar_t* restrict m)
{
     const unsigned ndof = stiffness->ndof;
     for(unsigned i=0; i<ndof; ++i)
          m[i*(ndof+1)] += coef*stiffness->diag[i];
     for(unsigned i=0; i<ndof-DOF_DIM; ++i)
     {
          m[i*(ndof+1)+DOF_DIM] += coef*stiffness->lowerdiag[i];
          m[(i+DOF_DIM)*(ndof+1)-DOF_DIM] += coef*stiffness->lowerdiag[i];
     }
}

static void stiffness_mat_addtovec(const stiffness_mat_t* stiffness,
                                   const cloc_scalar_t* restrict vprod,
                                   cloc_scalar_t coef,
                                   cloc_scalar_t* restrict v)
{
     const unsigned ndof = stiffness->ndof;
     for(unsigned i=0; i<ndof; ++i)
          v[i] += coef*stiffness->diag[i]*vprod[i];
     for(unsigned i=0; i<ndof-DOF_DIM; ++i)
     {
          v[i] += coef*stiffness->lowerdiag[i]*vprod[i+DOF_DIM];
          v[i+DOF_DIM] += coef*stiffness->lowerdiag[i]*vprod[i];
     }
}

static void fill_linear_system(cloc_sclothoid_t* ssc,
                               cloc_scalar_t dt,
                               cloc_scalar_t* restrict mata,
                               cloc_scalar_t* restrict vecb)
{
     const cloc_pacurvature_t* curvature = cloc_sclothoid_get_curvature(ssc);
     const cloc_pacurvature_t* curvaturedot = cloc_sclothoid_get_curvature_dot(ssc);
     const cloc_pacurvature_t* curvaturenat = cloc_sclothoid_get_natural_curvature(ssc);
     const unsigned nnodes = cloc_pacurvature_nnodes(curvature);
     const unsigned ndof = nnodes * DOF_DIM;
     const cloc_scalar_t length =cloc_pacurvature_totallength(curvature);
     cloc_clopse_compute(&(ssc->pse), length);
     
     const cloc_scalar_t rhos = cloc_sclothoid_get_volumic_mass(ssc)
          * M_PI * cloc_sclothoid_get_radius1(ssc)
          * cloc_sclothoid_get_radius2(ssc);
     const cloc_scalar_t internalfric = cloc_sclothoid_get_internal_friction(ssc);
     const cloc_scalar_t airdamping = cloc_sclothoid_get_air_damping(ssc);
     // put mass matrix in mata
     const cloc_scalar_t* ptr;
     ptr = cloc_clopse_get(&(ssc->pse), CLOC_INT_CENTER_LINE_DIFF_SQUARE);
     for(unsigned i=0; i<ndof; ++i)
     {
          for(unsigned j=0; j<i; ++j)
          {
               mata[i*ndof+j] = ptr[CLOC_SYMINDEX(i,j)];
               mata[j*ndof+i] = ptr[CLOC_SYMINDEX(i,j)];
          }
          mata[i*(ndof+1)] = ptr[CLOC_SYMINDEX(i,i)];
     }

     // compute M(q)*qdot*(1-nu/rhos*dt) in vecb
     assert( sizeof(cloc_scalar_t) == 8);
     cblas_dsymv(CblasRowMajor, CblasUpper, (int) ndof, 1.0 - dt*airdamping/rhos,
                 mata, (int) ndof, curvaturedot->curvatures, 1, 0.0, vecb, 1);

     // adding inertia terms to b
     ptr = cloc_clopse_get(&(ssc->pse), CLOC_INT_CL_DIFF_CL_DIFF2);
     for(unsigned i=0; i<ndof; ++i)
     {
          vecb[i] -= dt*ptr[i];
     }
     // adding gravitational terms to b
     ptr = cloc_clopse_get(&(ssc->pse), CLOC_INT_CENTER_LINE_DIFF);
     const cloc_scalar_t* gravity = cloc_sclothoid_get_gravity(ssc);
     for(unsigned i=0; i<ndof; ++i)
          for(unsigned k=0; k<DIM; ++k)
               vecb[i] += dt*ptr[DIM*i+k]*gravity[k];
     
     // compute matrix K
     stiffness_mat_t matk;
     stiffness_mat_init(&matk, ssc);
     // filling a matrix
     stiffness_mat_addtomat(&matk, dt*(dt+internalfric)/rhos, mata);
     // filling b vector
     // trick to avoid allocation: store diff in curvature
     for(unsigned i=0; i<ndof; ++i)
          curvature->curvatures[i] -= curvaturenat->curvatures[i];
     stiffness_mat_addtovec(&matk, curvature->curvatures, -dt/rhos, vecb);
     // and restore
     for(unsigned i=0; i<ndof; ++i)
          curvature->curvatures[i] += curvaturenat->curvatures[i];
     stiffness_mat_destroy(&matk);
}

static void solve_linear_system(cloc_scalar_t* restrict mata,
                                cloc_scalar_t* restrict vecxb,
                                unsigned size)
{
     assert( sizeof(cloc_scalar_t) == 8);
     clapack_dposv(CblasRowMajor, CblasLower, (int) size, 1, mata,
                   (int) size, vecxb, (int) size);
}

static void update_after_timestep(cloc_sclothoid_t* ssc,
                                  const cloc_scalar_t* vecx,
                                  cloc_scalar_t dt)
{
     cloc_pacurvature_t* curvature = cloc_kineparams_get_curvature(&(ssc->kparams));
     cloc_pacurvature_t* curvaturedot = cloc_kineparams_get_curvature_dot(&(ssc->kparams));
     const unsigned nnodes = cloc_pacurvature_nnodes(curvature);
     for(unsigned i=0; i<nnodes; ++i)
          for(unsigned k=0; k<DOF_DIM; ++k)
          {
               cloc_pacurvature_set(curvaturedot, i, k, vecx[i*DOF_DIM+k]);
               const cloc_scalar_t oldcurv = cloc_pacurvature_get(curvature, i, k);
               cloc_pacurvature_set(curvature, i, k, oldcurv+dt*vecx[i*DOF_DIM+k]);
          }
     ssc->durty_pse = true;
}

void cloc_sclothoid_stepforward(cloc_sclothoid_t* ssc, cloc_scalar_t dt)
{
     if(ssc->durty_pse)
     {
          if(is_pse_init(ssc)) cloc_clopse_destroy(&(ssc->pse));
          cloc_clopse_init(&(ssc->pse), cloc_kineparams_get_curvature(&(ssc->kparams)),
                           cloc_kineparams_get_curvature_dot(&(ssc->kparams))->curvatures);
          ssc->durty_pse = false;
     }
     
     const unsigned nnodes = cloc_pacurvature_nnodes(
          cloc_kineparams_get_curvature(&(ssc->kparams)));
     const unsigned ndof = nnodes * DOF_DIM;
     cloc_scalar_t* restrict mata = cloc_calloc(ndof*ndof, cloc_scalar_t);
     cloc_scalar_t* restrict vecxb = cloc_calloc(ndof, cloc_scalar_t);
     fill_linear_system(ssc, dt, mata, vecxb);
     solve_linear_system(mata, vecxb, ndof);
     update_after_timestep(ssc, vecxb, dt);
     cloc_free(mata);
     cloc_free(vecxb);
}

const cloc_scalar_t* cloc_sclothoid_posat(cloc_sclothoid_t* ssc, cloc_scalar_t s)
{
     if(ssc->durty_pse)
     {
          if(is_pse_init(ssc)) cloc_clopse_destroy(&(ssc->pse));
          cloc_clopse_init(&(ssc->pse), cloc_kineparams_get_curvature(&(ssc->kparams)),
                           cloc_kineparams_get_curvature_dot(&(ssc->kparams))->curvatures);
          ssc->durty_pse = false;
     }

     cloc_clopse_compute(&(ssc->pse), s);
     return cloc_clopse_get(&(ssc->pse), CLOC_CENTER_LINE);
}


const cloc_scalar_t* cloc_sclothoid_frameat(cloc_sclothoid_t* ssc, cloc_scalar_t s)
{
     if(ssc->durty_pse)
     {
          if(is_pse_init(ssc)) cloc_clopse_destroy(&(ssc->pse));
          cloc_clopse_init(&(ssc->pse), cloc_kineparams_get_curvature(&(ssc->kparams)),
                           cloc_kineparams_get_curvature_dot(&(ssc->kparams))->curvatures);
          ssc->durty_pse = false;
     }

     cloc_clopse_compute(&(ssc->pse), s);
     return cloc_clopse_get(&(ssc->pse), CLOC_FRAME);
}
