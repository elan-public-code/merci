//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#ifndef CLOC_CLOTHOIDS_SCLOTHOID_H_INCLUDED
#define CLOC_CLOTHOIDS_SCLOTHOID_H_INCLUDED

/**
 * \file sclothoid.h
 * \brief Interface of a Super-Space-Clothoid.
 * 
 * \author  Romain Casati
 * \date    11th March 2013
 */

#include "kinematical_params.h"
#include "physical_params.h"
#include "numerical_params.h"
#include "series.h"

/**
 * \struct cloc_sclothoid_t
 * \brief Structure defining a ssc.
 */
typedef struct
{
     cloc_kineparams_t kparams;
     cloc_phyparams_t pparams;
     cloc_numparams_t nparams;
     cloc_clopse_t pse;
     bool durty_pse;
} cloc_sclothoid_t;

/**
 * \brief Init.
 */
void cloc_sclothoid_init(cloc_sclothoid_t* ssc);

/**
 * \brief Destroy.
 */
void cloc_sclothoid_destroy(cloc_sclothoid_t* ssc);

/**
 * \brief Get.
 */
#define EXPAND_KINE_PARAM(n,t,v)                        \
     t cloc_sclothoid_get_##n(const cloc_sclothoid_t* ssc);
#include "kinematical_params.def"
#undef EXPAND_KINE_PARAM
#define EXPAND_PHY_PARAM(n,t,v)                         \
     t cloc_sclothoid_get_##n(const cloc_sclothoid_t* ssc);
#include "physical_params.def"
#undef EXPAND_PHY_PARAM
#define EXPAND_NUM_PARAM(n,t,v)                         \
     t cloc_sclothoid_get_##n(const cloc_sclothoid_t* ssc);
#include "numerical_params.def"
#undef EXPAND_NUM_PARAM

/**
 * Get the rod length.
 */
cloc_scalar_t cloc_sclothoid_get_length(const cloc_sclothoid_t* ssc);

/**
 * \brief Set.
 */
#define EXPAND_KINE_PARAM(n,t,v)                                \
     void cloc_sclothoid_set_##n(cloc_sclothoid_t* ssc, t val);
#include "kinematical_params.def"
#undef EXPAND_KINE_PARAM
#define EXPAND_PHY_PARAM(n,t,v)                                 \
     void cloc_sclothoid_set_##n(cloc_sclothoid_t* ssc, t val);
#include "physical_params.def"
#undef EXPAND_PHY_PARAM
#define EXPAND_NUM_PARAM(n,t,v)                                 \
     void cloc_sclothoid_set_##n(cloc_sclothoid_t* ssc, t val);
#include "numerical_params.def"
#undef EXPAND_NUM_PARAM

/**
 * \brief Change curvature.
 */
void cloc_sclothoid_change_curvature(cloc_sclothoid_t* ssc,
                                     const cloc_pacurvature_t* pac);

/**
 * \brief Change curvature dot.
 */
void cloc_sclothoid_change_curvature_dot(cloc_sclothoid_t* ssc,
                                         const cloc_pacurvature_t* pac_dot);

/**
 * \brief Change natural curvature.
 */
void cloc_sclothoid_change_natural_curvature(cloc_sclothoid_t* ssc,
                                             const cloc_pacurvature_t* nat_pac);

/**
 * \brief 
 */
void cloc_sclothoid_stepforward(cloc_sclothoid_t* ssc, cloc_scalar_t dt);

/**
 * \brief
 */
const cloc_scalar_t* cloc_sclothoid_posat(cloc_sclothoid_t* ssc, cloc_scalar_t s);

/**
 * \brief
 */
const cloc_scalar_t* cloc_sclothoid_frameat(cloc_sclothoid_t* ssc, cloc_scalar_t s);

#endif // ! CLOC_CLOTHOIDS_SCLOTHOID_H_INCLUDED
