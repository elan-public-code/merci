//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#include "sclothoid_interface.h"

#include "core/mem.h"
#include "core/algorithms.h"

#define DIM     CLOC_CLOTHOIDS_SPACE_DIM
#define DOF_DIM CLOC_CLOTHOIDS_DOF_DIM


void cloc_sclothoid_i_init(cloc_sclothoid_i_t* ssci,
                           cloc_scalar_t radius1, cloc_scalar_t radius2,
                           cloc_scalar_t young_modulus,
                           cloc_scalar_t poisson_ratio,
                           cloc_scalar_t density, cloc_scalar_t gravity,
                           cloc_scalar_t dt, cloc_scalar_t length,
                           cloc_scalar_t* curvatures, unsigned nnodes,
                           cloc_scalar_t internal_friction,
                           cloc_scalar_t air_damping)
{
     const unsigned npieces = nnodes-1;

     cloc_sclothoid_t* ssc = &(ssci->ssc);
     cloc_sclothoid_init(ssc);
     cloc_pacurvature_t pac, pac_null;
     cloc_scalar_t* lengths = cloc_calloc(npieces, cloc_scalar_t);
     for(unsigned i=0; i<npieces; ++i) lengths[i] = length/npieces;
     cloc_scalar_t* curvatures_null = cloc_calloc(nnodes*DOF_DIM, cloc_scalar_t);
     cloc_fillzeros(curvatures_null, nnodes*DOF_DIM);
     cloc_pacurvature_init(&pac, lengths, curvatures, npieces);
     cloc_pacurvature_init(&pac_null, lengths, curvatures_null, npieces);
     cloc_sclothoid_change_curvature(ssc, &pac);
     cloc_sclothoid_change_curvature_dot(ssc, &pac_null);
     cloc_sclothoid_change_natural_curvature(ssc, &pac);
     cloc_pacurvature_destroy(&pac);
     cloc_pacurvature_destroy(&pac_null);
     cloc_free(lengths);
     cloc_free(curvatures_null);

     cloc_sclothoid_set_radius1(ssc, radius1);
     cloc_sclothoid_set_radius2(ssc, radius2);
     cloc_sclothoid_set_young_modulus(ssc, young_modulus);
     cloc_sclothoid_set_poisson_ratio(ssc, poisson_ratio);
     cloc_sclothoid_set_volumic_mass(ssc, density);
     cloc_sclothoid_get_gravity(ssc)[DIM-1] = -gravity;
     ssci->dt = dt;
     cloc_sclothoid_set_internal_friction(ssc, internal_friction);
     cloc_sclothoid_set_air_damping(ssc, air_damping);
}

void cloc_sclothoid_i_destroy(cloc_sclothoid_i_t* ssci)
{
     cloc_sclothoid_destroy(&(ssci->ssc));
}

void cloc_sclothoid_i_stepsforward(cloc_sclothoid_i_t* ssci, unsigned n)
{
     cloc_sclothoid_t* ssc = &(ssci->ssc);
     for(unsigned i=0; i<n; ++i) cloc_sclothoid_stepforward(ssc, ssci->dt);
}

void cloc_sclothoid_i_posat(cloc_sclothoid_i_t* ssci, cloc_scalar_t t, // in [0,1]
                            cloc_scalar_t* x, cloc_scalar_t* y, cloc_scalar_t* z)
{
     cloc_sclothoid_t* ssc = &(ssci->ssc);
     const cloc_scalar_t* pos =
          cloc_sclothoid_posat(ssc, t*cloc_sclothoid_get_length(ssc));
     *x = pos[0];
     *y = pos[1];
     *z = pos[2];
}

void cloc_sclothoid_i_frameat(cloc_sclothoid_i_t* ssci, cloc_scalar_t t, // in [0,1]
                              cloc_scalar_t* frame)
{
     cloc_sclothoid_t* ssc = &(ssci->ssc);
     const cloc_scalar_t* framec = cloc_sclothoid_frameat(ssc, t*cloc_sclothoid_get_length(ssc));
     cloc_cpy(frame,framec,9,cloc_scalar_t);
}
