//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#ifndef CLOC_CLOTHOIDS_SCLOTHOID_INTERFACE_H_INCLUDED
#define CLOC_CLOTHOIDS_SCLOTHOID_INTERFACE_H_INCLUDED

/**
 * \file sclothoid_interface.h
 * \brief Interface to sclothoid (usefull for python binding).
 *
 * \author Romain Casati
 * \date   March 22th 2013
 */

#include "sclothoid.h"

/**
 * \struct cloc_sclothoid_i_t
 * \brief
 */
typedef struct
{
     cloc_sclothoid_t ssc;
     cloc_scalar_t dt;
} cloc_sclothoid_i_t;

void cloc_sclothoid_i_init(cloc_sclothoid_i_t* ssci,
                           cloc_scalar_t radius1, cloc_scalar_t radius2,
                           cloc_scalar_t young_modulus,
                           cloc_scalar_t poisson_ratio,
                           cloc_scalar_t density, cloc_scalar_t gravity,
                           cloc_scalar_t dt, cloc_scalar_t length,
                           cloc_scalar_t* curvatures,
                           unsigned nnodes,
                           cloc_scalar_t internal_friction,
                           cloc_scalar_t air_damping);

void cloc_sclothoid_i_destroy(cloc_sclothoid_i_t* ssci);

void cloc_sclothoid_i_stepsforward(cloc_sclothoid_i_t* ssci, unsigned n);

void cloc_sclothoid_i_posat(cloc_sclothoid_i_t* ssci, cloc_scalar_t t, // in [0,1]
                            cloc_scalar_t* x, cloc_scalar_t* y, cloc_scalar_t* z);

void cloc_sclothoid_i_frameat(cloc_sclothoid_i_t* ssci, cloc_scalar_t t, // in [0,1]
                              cloc_scalar_t* frame);

#endif // ! CLOC_CLOTHOIDS_SCLOTHOID_INTERFACE_H_INCLUDED
