//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#include "series.h"

#include "dimension.h"

#define DIM     CLOC_CLOTHOIDS_SPACE_DIM
#define DOF_DIM CLOC_CLOTHOIDS_DOF_DIM

#define INTrN_SIZE(ndof) (DIM)
#define rN_SIZE(ndof) (DIM)
#define RN_SIZE(ndof) (DIM*DIM)
#define RNM1_SIZE(ndof) (DIM*DIM)
#if CLOC_CLOPSE_INNER_LEVEL == 0
#define INTDrNDQD2rNDQ2_SIZE(ndof) (ndof*((ndof*(ndof+1))/2))
#elif CLOC_CLOPSE_INNER_LEVEL == 1
#define INTDrNDQD2rNDQ2_SIZE(ndof) (ndof*ndof)
#else
#define INTDrNDQD2rNDQ2_SIZE(ndof) (ndof)
#endif
#if CLOC_CLOPSE_INNER_LEVEL == 3
#define INTDrNDQS_SIZE(ndof) (ndof)
#else
#define INTDrNDQS_SIZE(ndof) ((ndof*(ndof+1))/2)
#endif
#define INTDrNDQ_SIZE(ndof) (DIM*ndof)
#define DrNDQ_SIZE(ndof) (DIM*ndof)
#define DRNDQ_SIZE(ndof) (DIM*DIM*ndof)
#define DRNM1DQ_SIZE(ndof) (DIM*DIM*ndof)
#if CLOC_CLOPSE_INNER_LEVEL == 0
#define D2rNDQ2_SIZE(ndof) (DIM*ndof*((ndof*(ndof+1))/2))
#elif CLOC_CLOPSE_INNER_LEVEL == 1 || CLOC_CLOPSE_INNER_LEVEL == 3
#define D2rNDQ2_SIZE(ndof) (DIM*ndof)
#else // level 2
#define D2rNDQ2_SIZE(ndof) (DIM)
#endif
#define D2RNDQ2_SIZE(ndof) (DIM*DIM*((ndof*(ndof+1))/2))
#define D2RNM1DQ2_SIZE(ndof) (DIM*DIM*((ndof*(ndof+1))/2))

#define TERM_SIZE(ndof)                                                 \
     (INTrN_SIZE(ndof) + rN_SIZE(ndof)                                  \
      + RN_SIZE(ndof) + RNM1_SIZE(ndof)                                 \
      + INTDrNDQD2rNDQ2_SIZE(ndof) + INTDrNDQS_SIZE(ndof)               \
      + INTDrNDQ_SIZE(ndof) + DrNDQ_SIZE(ndof)                          \
      + DRNDQ_SIZE(ndof) + DRNM1DQ_SIZE(ndof)                           \
      + D2rNDQ2_SIZE(ndof)                                              \
      + D2RNDQ2_SIZE(ndof) + D2RNM1DQ2_SIZE(ndof))

#define INTrN_OFFSET(ndof) (0)
#define rN_OFFSET(ndof) (INTrN_OFFSET(ndof) + INTrN_SIZE(ndof))
#define RN_OFFSET(ndof) (rN_OFFSET(ndof) + rN_SIZE(ndof))
#define RNM1_OFFSET(ndof) (RN_OFFSET(ndof) + RN_SIZE(ndof))
#define INTDrNDQD2rNDQ2_OFFSET(ndof) (RNM1_OFFSET(ndof) + RNM1_SIZE(ndof))
#define INTDrNDQS_OFFSET(ndof) (INTDrNDQD2rNDQ2_OFFSET(ndof) + INTDrNDQD2rNDQ2_SIZE(ndof))
#define INTDrNDQ_OFFSET(ndof) (INTDrNDQS_OFFSET(ndof) + INTDrNDQS_SIZE(ndof))
#define DrNDQ_OFFSET(ndof) (INTDrNDQ_OFFSET(ndof) + INTDrNDQ_SIZE(ndof))
#define DRNDQ_OFFSET(ndof) (DrNDQ_OFFSET(ndof) + DrNDQ_SIZE(ndof))
#define DRNM1DQ_OFFSET(ndof) (DRNDQ_OFFSET(ndof) + DRNDQ_SIZE(ndof))
#define D2rNDQ2_OFFSET(ndof) (DRNM1DQ_OFFSET(ndof) + DRNM1DQ_SIZE(ndof))
#define D2RNDQ2_OFFSET(ndof) (D2rNDQ2_OFFSET(ndof) + D2rNDQ2_SIZE(ndof))
#define D2RNM1DQ2_OFFSET(ndof) (D2RNDQ2_OFFSET(ndof) + D2RNDQ2_SIZE(ndof))

#include <math.h>
#include <assert.h>
#include "series_subroutines.cimpl"

static inline cloc_scalar_t min(cloc_scalar_t a, cloc_scalar_t b) { return a<b?a:b; }

typedef cloc_scalar_t term_t;

typedef struct
{
     const cloc_pacurvature_t* curvature;
     const cloc_scalar_t* restrict inner;
     cloc_scalar_t gamma[DOF_DIM]; // current slope
     cloc_scalar_t k0[DOF_DIM];    // current curvature at 0
     cloc_scalar_t gammadiff[2]; // current slope diff
     cloc_scalar_t k0diff[2];    // current curvature at 0 diff
     term_t* tmp_sum;
     unsigned size_term;
     unsigned index;
     unsigned ndof;
     cloc_array_t drndqarray;
     cloc_array_t d2rndq2array;
#if CLOC_CLOPSE_INNER_LEVEL == 3
     cloc_array_t drndqinnerarray;
#endif
} data_t;

inline static void compute_gamma_and_k0(const data_t* data,
                                        unsigned index,
                                        cloc_scalar_t x0,
                                        cloc_scalar_t* restrict gamma,
                                        cloc_scalar_t* restrict k0)
{
     for(unsigned k=0; k<DOF_DIM; ++k)
     {
          gamma[k] = cloc_pacurvature_slope(data->curvature, index, (cloc_curvindex_t) k);
          k0[k] = cloc_pacurvature_get(data->curvature, index, (cloc_curvindex_t) k)
               + x0*gamma[k];
     }
}

inline static void compute_gamma_and_k0_diff(const data_t* data,
                                             unsigned index,
                                             cloc_scalar_t x0,
                                             cloc_scalar_t* restrict gammadiff,
                                             cloc_scalar_t* restrict k0diff)
{
     const cloc_scalar_t invli = 1./ cloc_pacurvature_length(data->curvature, index);
     
     gammadiff[0] = -invli;
     gammadiff[1] = invli;
     k0diff[0] = 1-x0*invli;
     k0diff[1] = x0*invli;
}

inline static void empty_array(cloc_array_t* array)
{
     const unsigned size = cloc_array_size(array);
     for(unsigned i=0; i<size; ++i)
          cloc_free(*((void**)cloc_array_get(array, i)));
     cloc_array_set_size(array, 0);
}

static void next(const void* restrict term, void* restrict next_term,
                 unsigned n, cloc_scalar_t x, void* data)
{
     data_t* const cdata = (data_t*) ((cloc_ppse_data_t*) data)->data;
     const cloc_scalar_t* restrict pterm = (const term_t*) term;
     cloc_scalar_t* restrict pnterm = (term_t*) next_term;
     const unsigned index = cdata->index;
     const unsigned ndof = cdata->ndof;
     assert( cdata->size_term == TERM_SIZE(ndof) );

     //const cloc_scalar_t* restrict intrn = pterm + INTrN_OFFSET(ndof);
     const cloc_scalar_t* restrict rn = pterm + rN_OFFSET(ndof);
     const cloc_scalar_t* restrict Rn = pterm + RN_OFFSET(ndof);
     const cloc_scalar_t* restrict Rnm1 = pterm + RNM1_OFFSET(ndof);
     //const cloc_scalar_t* restrict intdrndqs = pterm + INTDrNDQS_OFFSET(ndof);
     //const cloc_scalar_t* restrict intdrndq = pterm + INTDrNDQ_OFFSET(ndof);
     const cloc_scalar_t* restrict drndq = pterm + DrNDQ_OFFSET(ndof);
     const cloc_scalar_t* restrict dRndq = pterm + DRNDQ_OFFSET(ndof);
     const cloc_scalar_t* restrict dRnm1dq = pterm + DRNM1DQ_OFFSET(ndof);
     const cloc_scalar_t* restrict d2rndq2 = pterm + D2rNDQ2_OFFSET(ndof);
     const cloc_scalar_t* restrict d2Rndq2 = pterm + D2RNDQ2_OFFSET(ndof);
     const cloc_scalar_t* restrict d2Rnm1dq2 = pterm + D2RNM1DQ2_OFFSET(ndof);
     cloc_scalar_t* restrict nintrn = pnterm + INTrN_OFFSET(ndof);
     cloc_scalar_t* restrict nrn = pnterm + rN_OFFSET(ndof);
     cloc_scalar_t* restrict nRn = pnterm + RN_OFFSET(ndof);
     cloc_scalar_t* restrict nRnm1 = pnterm + RNM1_OFFSET(ndof);
     cloc_scalar_t* restrict nintdrndqd2rndq2 = pnterm + INTDrNDQD2rNDQ2_OFFSET(ndof);
     cloc_scalar_t* restrict nintdrndqs = pnterm + INTDrNDQS_OFFSET(ndof);
     cloc_scalar_t* restrict nintdrndq = pnterm + INTDrNDQ_OFFSET(ndof);
     cloc_scalar_t* restrict ndrndq = pnterm + DrNDQ_OFFSET(ndof);
     cloc_scalar_t* restrict ndRndq = pnterm + DRNDQ_OFFSET(ndof);
     cloc_scalar_t* restrict ndRnm1dq = pnterm + DRNM1DQ_OFFSET(ndof);
     cloc_scalar_t* restrict nd2rndq2 = pnterm + D2rNDQ2_OFFSET(ndof);
     cloc_scalar_t* restrict nd2Rndq2 = pnterm + D2RNDQ2_OFFSET(ndof);
     cloc_scalar_t* restrict nd2Rnm1dq2 = pnterm + D2RNM1DQ2_OFFSET(ndof);

     // add last term to drndqarray
     assert( n == cloc_array_size(&(cdata->drndqarray)) );
     cloc_scalar_t* drndqcpy = cloc_calloc(DrNDQ_SIZE(ndof), cloc_scalar_t);
     cloc_cpy(drndqcpy, drndq, DrNDQ_SIZE(ndof), cloc_scalar_t);
     cloc_array_push_back(&(cdata->drndqarray), &drndqcpy);
     // add last term to d2rndq2 array
     assert( n == cloc_array_size(&(cdata->d2rndq2array)) );
     cloc_scalar_t* d2rndq2cpy = cloc_calloc(D2rNDQ2_SIZE(ndof), cloc_scalar_t);
     cloc_cpy(d2rndq2cpy, d2rndq2, D2rNDQ2_SIZE(ndof), cloc_scalar_t);
     cloc_array_push_back(&(cdata->d2rndq2array), &d2rndq2cpy);
#if CLOC_CLOPSE_INNER_LEVEL == 3
     // add to drndqinnerarray
     assert( n == cloc_array_size(&(cdata->drndqinnerarray)) );
     cloc_scalar_t* drndqinner = cloc_calloc(DIM, cloc_scalar_t);
     clinner(drndq, inner, ndof, drndqinner);
     cloc_array_push_back(&(cdata->drndqinnerarray), &drndqinner);
#endif
     const cloc_scalar_t intfactor = x/(n+1);
     const cloc_scalar_t gammafactor[DOF_DIM] =
          {
               x*intfactor*cdata->gamma[0],
#if DIM == 3
               x*intfactor*cdata->gamma[1],
               x*intfactor*cdata->gamma[2]
#endif
          };
     const cloc_scalar_t k0factor[DOF_DIM] =
          {
               intfactor*cdata->k0[0],
#if DIM == 3
               intfactor*cdata->k0[1],
               intfactor*cdata->k0[2]
#endif
          };
     const cloc_scalar_t gammadifffactor[2] = {x*intfactor*cdata->gammadiff[0],
                                               x*intfactor*cdata->gammadiff[1]};
     const cloc_scalar_t k0difffactor[2] = {intfactor*cdata->k0diff[0],
                                            intfactor*cdata->k0diff[1]};
     
     next_intcl(rn, nintrn, intfactor);
     next_cl(Rn, nrn, intfactor);
     next_frame(Rn, Rnm1, nRn, nRnm1, gammafactor, k0factor, n==0);
#if CLOC_CLOPSE_INNER_LEVEL == 3
     next_intcldiffcldiff2(&(cdata->drndqinnerarray), &(cdata->d2rndq2array),
                           nintdrndqd2rndq2, intfactor, ndof);
     next_intcldiffsquare(&(cdata->drndqarray), &(cdata->drndqinnerarray),
                          nintdrndqs, intfactor, ndof);
#else
     next_intcldiffcldiff2(&(cdata->drndqarray), &(cdata->d2rndq2array),
                           nintdrndqd2rndq2, intfactor, ndof);
     next_intcldiffsquare(&(cdata->drndqarray), &(cdata->drndqarray),
                          nintdrndqs, intfactor, ndof);
#endif
     next_intcldiff(drndq, nintdrndq, intfactor, ndof);
     next_cldiff(dRndq, ndrndq, intfactor, ndof);
     next_framediff(dRndq, dRnm1dq, Rn, Rnm1, ndRndq, ndRnm1dq,
                    gammafactor, k0factor, gammadifffactor, k0difffactor,
                    index, ndof, n==0);
     next_cldiff2(d2Rndq2, nd2rndq2, intfactor,
                  cdata->inner, ndof);
     next_framediff2(d2Rndq2, d2Rnm1dq2, dRndq, dRnm1dq,
                     nd2Rndq2, nd2Rnm1dq2, gammafactor, k0factor,
                     gammadifffactor, k0difffactor, index, ndof, n==0);
}

static void add(const void* restrict term, void* restrict addto,
                void* data)
{
     const data_t* const cdata = (data_t*) ((cloc_ppse_data_t*) data)->data;
     const unsigned size = cdata->size_term;
     for(unsigned i=0; i<size; ++i)
     {
          ((term_t*)addto)[i] += ((const term_t*)term)[i];
     }
}

static cloc_scalar_t norm(const void* restrict term, void* data)
{
     const data_t* const cdata = (data_t*) ((cloc_ppse_data_t*) data)->data;
     const unsigned size = cdata->size_term;
     return cloc_maxnorm((const term_t*)term, size);
}

static cloc_scalar_t uptox(cloc_scalar_t x, void* data)
{
     // Get index
     const data_t* const cdata = (data_t*) ((cloc_ppse_data_t*) data)->data;
     const unsigned index = ((cloc_ppse_data_t*) data)->piece_index;
     cloc_scalar_t gamma[DOF_DIM];
     cloc_scalar_t k0[DOF_DIM];
     compute_gamma_and_k0(cdata, index, x, gamma, k0);
     const cloc_scalar_t aslope = cloc_maxnorm(gamma, DOF_DIM);
     const cloc_scalar_t astart = cloc_maxnorm(k0, DOF_DIM);
     if(aslope != 0.)
          return x + (sqrt(astart*astart+2.*aslope*CLOC_UPTOX_FOR_EXP)-astart)/(2.*aslope);
     // 2. is used here instead of 4. (as the calculus gives) but it seem
     // 4. is OK (4. gives longer steps than 2.).
     else if(astart != 0.)
          return x + CLOC_UPTOX_FOR_EXP/(2.*astart);
     else
          return x + CLOC_UPTOX_MAX_LENGTH;
}

static void prepare(void* restrict term0, cloc_scalar_t x0, void* data)
{
     (void) term0;
     const unsigned index = ((cloc_ppse_data_t*) data)->piece_index;
     data_t* const cdata = (data_t*)(((cloc_ppse_data_t*) data)->data);
     compute_gamma_and_k0(cdata, index, x0, cdata->gamma, cdata->k0);
     compute_gamma_and_k0_diff(cdata, index, x0, cdata->gammadiff, cdata->k0diff);
     cdata->index = index;
     empty_array(&(cdata->drndqarray));
     empty_array(&(cdata->d2rndq2array));
}

void cloc_clopse_init(cloc_clopse_t* pse,
                      const cloc_pacurvature_t* curvature,
                      const cloc_scalar_t* restrict inner)
{
     const unsigned nnodes = cloc_pacurvature_nnodes(curvature);
     const unsigned ndof = nnodes*DOF_DIM;
     const unsigned size_term = TERM_SIZE(ndof);
     const unsigned byte_size_term = size_term*(unsigned)sizeof(term_t);
     // Data
     data_t* data = cloc_calloc(1,data_t);
     data->curvature = curvature;
     data->inner = inner;
     data->ndof = ndof;
     data->size_term = size_term;
     data->tmp_sum = cloc_calloc(size_term,term_t);
     cloc_array_init(&(data->drndqarray), 0, sizeof(cloc_scalar_t*));
     cloc_array_init(&(data->d2rndq2array), 0, sizeof(cloc_scalar_t*));
     // Term0
     term_t* restrict term0 = cloc_calloc(size_term,term_t);
     init_term0(term0, size_term);
     // PPSE init
     cloc_pse_funcs_t funcs = {next, add, norm, uptox, prepare};
     cloc_ppse_init(pse, curvature->sis, cloc_pacurvature_nnodes(curvature),
                    term0, byte_size_term, &funcs, CLOC_DEFAULT_EPS_TRUNC, data);
}

void cloc_clopse_compute(cloc_clopse_t* pse,
                         cloc_scalar_t s)
{
     cloc_ppse_compute(pse, s, ((data_t*) (pse->data.data))->tmp_sum);
}

void cloc_clopse_destroy(cloc_clopse_t* pse)
{
     data_t* cdata = (data_t*) (pse->data.data);
     // drndqarray
     empty_array(&(cdata->drndqarray));
     cloc_array_destroy(&(cdata->drndqarray));
     // d2rndq2array
     empty_array(&(cdata->d2rndq2array));
     cloc_array_destroy(&(cdata->d2rndq2array));
     cloc_free(cdata->tmp_sum);
     cloc_ppse_destroy(pse, true, true);
}

const cloc_scalar_t* cloc_clopse_get(const cloc_clopse_t* pse,
                                     cloc_clopse_quantity_t q)
{
     const data_t* const cdata = (data_t*) pse->data.data;
     const cloc_scalar_t* const term = cdata->tmp_sum;
     const unsigned ndof = cdata->ndof;
     switch(q)
     {
     case CLOC_INT_CENTER_LINE:
          return term + INTrN_OFFSET(ndof);
          break;
     case CLOC_CENTER_LINE:
          return term + rN_OFFSET(ndof);
          break;
     case CLOC_FRAME:
          return term + RN_OFFSET(ndof);
          break;
     case CLOC_INT_CL_DIFF_CL_DIFF2:
          return term + INTDrNDQD2rNDQ2_OFFSET(ndof);
          break;
     case CLOC_INT_CENTER_LINE_DIFF_SQUARE:
          return term + INTDrNDQS_OFFSET(ndof);
          break;
     case CLOC_INT_CENTER_LINE_DIFF:
          return term + INTDrNDQ_OFFSET(ndof);
          break;
     case CLOC_CENTER_LINE_DIFF:
          return term + DrNDQ_OFFSET(ndof);
          break;
     case CLOC_FRAME_DIFF:
          return term + DRNDQ_OFFSET(ndof);
          break;
     case CLOC_CENTER_LINE_DIFF2:
          return term + D2rNDQ2_OFFSET(ndof);
          break;
     case CLOC_FRAME_DIFF2:
          return term + D2RNDQ2_OFFSET(ndof);
          break;
     }
     assert( false );
     return NULL;
}
