//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#ifndef CLOC_CLOTHOIDS_SERIES_H_INCLUDED
#define CLOC_CLOTHOIDS_SERIES_H_INCLUDED

#include "series/ppse.h"
#include "pacurvature.h"

/**
 * \file series.h
 * \brief API for the *global* series of geometrics and dynamics terms
 * of Super Space Clothoids.
 *
 * \author Romain Casati
 * \date   24th Jan. 2013
 */

/**
 * \typedef cloc_clopse_t
 * \brief The strucure of the global clothoid pse.
 */
typedef cloc_ppse_t cloc_clopse_t;

/**
 * \def CLOC_CLOPSE_INNER_LEVEL
 * \brief Defines the level of inner product.
 * 
 * This level affect the values of quantities accessed via
 * CLOC_INT_CL_DIFF_CL_DIFF2 and CLOC_INT_CENTER_LINE_DIFF_SQUARE.
 * If this level is 0, CLOC_INT_CL_DIFF_CL_DIFF2 gives access to
 * \f[ \int_0^L \frac{\partial r}{\partial q}
 * \frac{\partial^2 r}{\partial q^2} \ ds \f]
 * and CLOC_INT_CENTER_LINE_DIFF_SQUARE gives acces to
 * \f[ \int_0^L \frac{\partial r}{\partial q}
 * \frac{\partial r}{\partial q} \ ds. \f]
 * If the level is 1, only CLOC_INT_CL_DIFF_CL_DIFF2 is changed to
 * \f[ \int_0^L \frac{\partial r}{\partial q} \textrm{inner}_i
 * \frac{\partial^2 r}{\partial q_i\partial q} \ ds. \f]
 * If the level is equal to 2, CLOC_INT_CL_DIFF_CL_DIFF2 is changed to
 * \f[ \int_0^L \frac{\partial r}{\partial q} \textrm{inner}_i
 * \frac{\partial^2 r}{\partial q_i\partial q_j} \textrm{inner}_j \ ds. \f]
 * Then if the level is 3, LOC_INT_CL_DIFF_CL_DIFF2 is changed to
 * \f[ \int_0^L \frac{\partial r}{\partial q_i} \textrm{inner}_i
 * \frac{\partial^2 r}{\partial q^2} \textrm{inner} \ ds \f] and
 * CLOC_INT_CENTER_LINE_DIFF_SQUARE gives acces to
 * \f[ \int_0^L \frac{\partial r}{\partial q} 
 * \frac{\partial r}{\partial q} \textrm{inner} \ ds. \f]
 * All this actually allows to speed-up the computation if some
 * quantities are useless.
 */
#ifndef CLOC_CLOPSE_INNER_LEVEL
#define CLOC_CLOPSE_INNER_LEVEL 2
#endif
#if (2*(CLOC_CLOPSE_INNER_LEVEL)-3)*(2*(CLOC_CLOPSE_INNER_LEVEL)-3) > 9
#error "CLOC_CLOPSE_INNER_LEVEL must be between 0 and 3"
#endif

/**
 * \brief Constructor.
 * \param pse The clothoid series.
 * \param curvature The curvatures data.
 * \param inner The vector of the same size as curvature for inner formulas.
 * \warning curvature and inner are not copied so they are
 * supposed to be in life and remains constant during their use by clopse.
 */
void cloc_clopse_init(cloc_clopse_t* pse,
                      const cloc_pacurvature_t* curvature,
                      const cloc_scalar_t* restrict inner);

/**
 * \brief Destructor.
 */
void cloc_clopse_destroy(cloc_clopse_t* pse);

/**
 * \brief Computes all terms at given s.
 * Use the get function to access computed quantities.
 */
void cloc_clopse_compute(cloc_clopse_t* pse, cloc_scalar_t s);

/**
 * \enum cloc_clopse_quantity_t
 * \brief Quantity id to use with the get function to access
 * computed quantities.
 * 
 * \warning Symetric quantities uses the symetric storage of cloc:
 * use the CLOC_SYMINDEX(i,j) macro to access data.
 */
typedef enum
{
     CLOC_INT_CENTER_LINE, /**< Used by the gravitational energy */
     CLOC_CENTER_LINE,
     CLOC_FRAME,
     CLOC_INT_CL_DIFF_CL_DIFF2,
     CLOC_INT_CENTER_LINE_DIFF_SQUARE, /**< Used by the mass matrix */
     CLOC_INT_CENTER_LINE_DIFF, /**< Used by the force-field term */
     CLOC_CENTER_LINE_DIFF,
     CLOC_FRAME_DIFF,
     CLOC_CENTER_LINE_DIFF2,
     CLOC_FRAME_DIFF2
} cloc_clopse_quantity_t;

/**
 * \brief Acces computed quantities by the compute function.
 */
const cloc_scalar_t* cloc_clopse_get(const cloc_clopse_t* pse,
                                     cloc_clopse_quantity_t q);

#endif // ! CLOC_CLOTHOIDS_SERIES_H_INCLUDED
