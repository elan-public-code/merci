//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#include "algorithms.h"

#include "core/mem.h"
#include <math.h>

#define SWAP_PTR(a,b) do { void* const tmp = a; a = b; b = tmp; } while(0)

unsigned cloc_index_of_segment(const cloc_scalar_t* restrict pts,
                               unsigned npts,
                               cloc_scalar_t x)
{
     unsigned imin = 0, imax = npts-1, imid;
     while(imax > imin+1)
     {
          imid = (imax+imin)/2;
          if(pts[imid] > x) imax = imid;
          else imin = imid;
     }
     return imin;
}

void cloc_fillzeros(cloc_scalar_t* restrict array, unsigned size)
{
#ifdef __STDC_IEC_559__
     cloc_memset(array, 0, size, cloc_scalar_t);
     return;
#endif
     for(unsigned i=0; i<size; ++i)
          array[i] = 0.;
}

cloc_scalar_t cloc_maxnorm(const cloc_scalar_t* restrict array, unsigned size)
{
     cloc_scalar_t maxnorm = fabs(array[0]);
     for(unsigned i=1; i<size; ++i)
     {
          const cloc_scalar_t currnorm = fabs(array[i]);
          if(currnorm>maxnorm) maxnorm = currnorm;
     }
     return maxnorm;
}

cloc_scalar_t cloc_maxdiff(const cloc_scalar_t* restrict array1,
                           const cloc_scalar_t* restrict array2,
                           unsigned size)
{
     cloc_scalar_t maxnorm = fabs(array1[0]-array2[0]);
     for(unsigned i=1; i<size; ++i)
     {
          const cloc_scalar_t currnorm = fabs(array1[i]-array2[i]);
          if(currnorm>maxnorm) maxnorm = currnorm;
     }
     return maxnorm;
}

void cloc_matmult(const cloc_scalar_t* restrict a,
                  unsigned n, unsigned m,
                  const cloc_scalar_t* restrict b,
                  unsigned p,
                  cloc_scalar_t* restrict c)
{
#ifdef CLOC_COLUMN_MAJOR
     for(unsigned i=0; i<n; ++i)
          for(unsigned j=0; j<p; ++j)
#else // Row-major
     for(unsigned j=0; j<p; ++j)
          for(unsigned i=0; i<n; ++i)
#endif
          {
               c[CLOC_INDEX(i,j,n,p)] =
                    a[CLOC_INDEX(i,0,n,m)] * b[CLOC_INDEX(0,j,m,p)];
               for(unsigned k=1; k<m; ++k)
                    c[CLOC_INDEX(i,j,n,p)] +=
                         a[CLOC_INDEX(i,k,n,m)] * b[CLOC_INDEX(k,j,m,p)];
          }
}

unsigned cloc_broyden(cloc_scalar_t* restrict x, cloc_broyden_func_t f,
                      unsigned size, void* data,
                      unsigned itermax, cloc_scalar_t ftol, cloc_scalar_t rtol)
{
     // B matrix
     cloc_scalar_t* restrict matb = cloc_calloc(size*size, cloc_scalar_t);
     cloc_fillzeros(matb, size*size);
     for(unsigned i=0; i<size; ++i) matb[i*(size+1)] = 1.;
     // value of f at x
     cloc_scalar_t* restrict valf = cloc_calloc(size, cloc_scalar_t);
     f(x, valf, data);
     cloc_scalar_t* restrict valfnext = cloc_calloc(size, cloc_scalar_t);
     cloc_scalar_t* restrict xnext = cloc_calloc(size, cloc_scalar_t);
     cloc_scalar_t tmp;
     unsigned n;
     for(n=0; n<itermax; ++n)
     {
          // compute xnext
          cloc_cpy(xnext, x, size, cloc_scalar_t);
          for(unsigned i=0; i<size; ++i)
               for(unsigned j=0; j<size; ++j)
                    xnext[i] -= matb[CLOC_INDEX(i,j,size,size)]*valf[j];
          // compute f(xnext)
          f(xnext, valfnext, data);
          // store dx in xnext
          for(unsigned i=0; i<size; ++i) xnext[i] -= x[i];
          // under tolerances ?
          if(cloc_maxnorm(xnext, size) < rtol
             || cloc_maxdiff(valfnext, valf, size) < ftol)
          {
               // put xnext in x
               for(unsigned i=0; i<size; ++i) x[i] += xnext[i];
               break;
          }
          // store the norm in tmp
          tmp = 0.;
          for(unsigned j=0; j<size; ++j)
          {
               const cloc_scalar_t difff = valfnext[j] - valf[j];
               for(unsigned i=0; i<size; ++i)
                    tmp += xnext[i]*matb[CLOC_INDEX(i,j,size,size)]*difff;
          }
          // compute dx^T * B/norm inside valf
          for(unsigned i=0; i<size; ++i)
          {
               valf[i] = xnext[0]*matb[CLOC_INDEX(0,i,size,size)];
               for(unsigned j=1; j<size; ++j)
                    valf[i] += xnext[j]*matb[CLOC_INDEX(j,i,size,size)];
               valf[i] /= tmp; // minus is taken into account here
          }
          // store xnext in x
          for(unsigned i=0; i<size; ++i) x[i] += xnext[i];
          // store B*valfnext in xnext
          for(unsigned i=0; i<size; ++i)
          {
               xnext[i] = valfnext[0]*matb[CLOC_INDEX(i,0,size,size)];
               for(unsigned j=1; j<size; ++j)
                    xnext[i] += valfnext[j]*matb[CLOC_INDEX(i,j,size,size)];
          }
          // update B
          for(unsigned i=0; i<size; ++i)
               for(unsigned j=0; j<size; ++j)
                    matb[CLOC_INDEX(i,j,size,size)] -= xnext[i]*valf[j];
          SWAP_PTR(valf, valfnext);
     }
     cloc_free(matb);
     cloc_free(valf);
     cloc_free(valfnext);
     cloc_free(xnext);
     return n;
}
