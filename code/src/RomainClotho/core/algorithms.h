//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#ifndef CLOC_CORE_ALGORITHMS_H_INCLUDED
#define CLOC_CORE_ALGORITHMS_H_INCLUDED

#include "definitions.h"

/**
 * \file algorithms.h
 * \brief Implementation of useful algorithms.
 */

/**
 * \brief Returns the index of the segment containing x.
 * \param pts Array containing abscisae of segment points.
 * \param npts Size of pts.
 * \param x The point which you look for the segment index.
 */
unsigned cloc_index_of_segment(const cloc_scalar_t* restrict pts,
                               unsigned npts,
                               cloc_scalar_t x);

/**
 * \brief Fill the array with zeros.
 */
void cloc_fillzeros(cloc_scalar_t* restrict array, unsigned size);

/**
 * \brief Returns the max norm of an array.
 */
cloc_scalar_t cloc_maxnorm(const cloc_scalar_t* restrict array, unsigned size);

/**
 * \brief Return the maximum difference (absolute) between two arrays.
 */
cloc_scalar_t cloc_maxdiff(const cloc_scalar_t* restrict array1,
                           const cloc_scalar_t* restrict array2,
                           unsigned size);

/**
 * \brief Fill c(n,p) with the matrix product a(n,m) by b(m,p)
 */
void cloc_matmult(const cloc_scalar_t* restrict a,
                  unsigned n, unsigned m,
                  const cloc_scalar_t* restrict b,
                  unsigned p,
                  cloc_scalar_t* restrict c);

/**
 * \typedef cloc_broyden_func_t
 */
typedef void (*cloc_broyden_func_t)(const cloc_scalar_t* restrict x,
                                    cloc_scalar_t* restrict res, void* data);

/**
 * \brief 
 */
unsigned cloc_broyden(cloc_scalar_t* restrict x, cloc_broyden_func_t f,
                      unsigned size, void* data,
                      unsigned itermax, cloc_scalar_t ftol, cloc_scalar_t rtol);

#endif // ! CLOC_CORE_ALGORITHMS_H_INCLUDED
