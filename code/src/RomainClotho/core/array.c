//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#include "array.h"

#include "core/mem.h"

#define CLOC_ARRAY_DEFAULT_SIZE 100

int cloc_array_init(cloc_array_t* array, unsigned size, unsigned elem_size)
{
     array->size = size;
     array->allocated_size = (size == 0) ? CLOC_ARRAY_DEFAULT_SIZE : (2*size+1);
     array->elem_size = elem_size;
     array->data = cloc_calloc(array->allocated_size*elem_size, unsigned char);
     return array->data == NULL;
}

int cloc_array_destroy(cloc_array_t* array)
{
     cloc_free(array->data);
     array->data = NULL;
     return 0;
}

unsigned cloc_array_size(const cloc_array_t* array)
{
     return array->size;
}

unsigned cloc_array_elem_size(const cloc_array_t* array)
{
     return array->elem_size;
}

int cloc_array_set_size(cloc_array_t* array, unsigned size)
{
     if(size > array->allocated_size)
     {
          unsigned char* tmp = array->data;
          array->allocated_size = 2*size+1;
          array->data = cloc_calloc((array->allocated_size)*array->elem_size,unsigned char);
          memcpy(array->data, tmp, array->size*array->elem_size);
          cloc_free(tmp);
     }
     array->size = size;
     return array->data == NULL;
}

void* cloc_array_get(const cloc_array_t* array, unsigned i)
{
     return (void*) (array->data + i*array->elem_size);
}

void* cloc_array_first(const cloc_array_t* array)
{
     return cloc_array_get(array, 0);
}

void* cloc_array_last(const cloc_array_t* array)
{
     return cloc_array_get(array, cloc_array_size(array)-1);
}

int cloc_array_set(cloc_array_t* array, unsigned i,
                   const void* restrict elem)
{
     memcpy(array->data + i*array->elem_size, elem, array->elem_size);
     return 0;
}

int cloc_array_push_back(cloc_array_t* array, const void* restrict elem)
{
     if(cloc_array_set_size(array, array->size+1))
     {
          return -1;
     }
     cloc_array_set(array, array->size-1, elem);
     return 0;
}

void* cloc_array_pop_back(cloc_array_t* array)
{
     return cloc_array_get(array, --(array->size));
}
