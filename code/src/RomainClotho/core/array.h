//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#ifndef CLOC_CORE_ARRAY_H_INCLUDED
#define CLOC_CORE_ARRAY_H_INCLUDED

#include "definitions.h"

/**
 * \file array.h
 * \brief Implementation of dynamic array.
 * 
 * \author Romain Casati
 * \date 23rd Jan. 2013
 */

/**
 * \struct cloc_array_t
 * \brief Dynamic array type.
 */
typedef struct
{
     unsigned char* restrict data;
     unsigned size;
     unsigned elem_size;
     unsigned allocated_size;
} cloc_array_t;

/**
 * \brief Constructor.
 * \param array Pointer to the (structure-allocated) array.
 * \param size Initial size (number of elements) of the array.
 * \param elem_size Size of elements in the array (supposed to be constant).
 */
int cloc_array_init(cloc_array_t* array, unsigned size, unsigned elem_size);
/**
 * \brief Destructor.
 */
int cloc_array_destroy(cloc_array_t* array);

/**
 * \brief Returns the current number of elements in the array.
 */
unsigned cloc_array_size(const cloc_array_t* array);

/**
 * Returns the size of elements.
 */
unsigned cloc_array_elem_size(const cloc_array_t* array);

/**
 * \brief Change the size of the array.
 */
int cloc_array_set_size(cloc_array_t* array, unsigned size);

/**
 * \brief Get the ith element in the array.
 */
void* cloc_array_get(const cloc_array_t* array, unsigned i);

/**
 * \brief Get the first element in the array.
 */
void* cloc_array_first(const cloc_array_t* array);

/**
 * \brief Get the last element in the array.
 */
void* cloc_array_last(const cloc_array_t* array);

/**
 * \brief Set the ith element of the array.
 */
int cloc_array_set(cloc_array_t* array, unsigned i,
                   const void* restrict elem);

/**
 * \brief Push an element at the end of the array (incresing its size by 1).
 */
int cloc_array_push_back(cloc_array_t* array, const void* restrict elem);

/**
 * \brief Pop the last element of the array (decreasing its size by 1).
 * \warning Be careful, if you, for example, push back right before
 * poping, the returned pointer will then point to the pushed data.
 * It is your responsability to copy it before.
 */
void* cloc_array_pop_back(cloc_array_t* array);

#endif // ! CLOC_CORE_ARRAY_H_INCLUDED
