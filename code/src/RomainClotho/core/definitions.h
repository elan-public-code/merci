//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#ifndef CLOC_CORE_DEFINITIONS_H_INCLUDED
#define CLOC_CORE_DEFINITIONS_H_INCLUDED

/**
 * \file definitions.h
 * \brief Definitions of core types.
 */

/**
 * \typedef cloc_scalar_t
 * \brief   Default scalar type in cloc.
 * 
 * At least double precision is supposed to be used in the PSE computer.
 */
typedef double cloc_scalar_t;

/**
 * \def CLOC_COLUMN_MAJOR
 * \brief Default storing order is column-major.
 */
#define CLOC_COLUMN_MAJOR
//#define CLOC_ROW_MAJOR

/**
 * \def CLOC_INDEX
 * \brief Unified (in terms of row/column major acces to matrix entry.
 */
#ifdef  CLOC_COLUMN_MAJOR
#undef  CLOC_ROW_MAJOR
#define CLOC_INDEX(i,j,n,p) (j*n+i)
#else
#ifndef  CLOC_ROW_MAJOR
#error   "You have to define CLOC_COLUMN_MAJOR or CLOC_ROW_MAJOR."
#endif
#undef  CLOC_COLUMN_MAJOR
#define CLOC_INDEX(i,j,n,p) (i*p+j)
#endif

/**
 * \def CLOC_SYMINDEX
 * \brief Access to the (i,j) entry of a symetric matrix (i<=j).
 * \warning j must be lower or equal to i.
 */
#define CLOC_SYMINDEX(i,j) ((i*(i+1))/2+j)

// restrict keyword is not standard in C++ but often implemented as __restrict__
#ifdef __cplusplus
#ifndef __restrict__
#ifndef restrict
#define restrict
//#error "restrict feature not implemented with this compiler."
#endif
#else
#define restrict __restrict__
#endif
#endif

#endif // ! CLOC_CORE_DEFINITIONS_H_INCLUDED
