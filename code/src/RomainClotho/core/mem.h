//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#ifndef CLOC_CORE_MEM_H_INCLUDED
#define CLOC_CORE_MEM_H_INCLUDED

/**
 * \file mem.h
 * \brief Memory definitions (alloc, free, cpy).
 */

#include <stdlib.h>
#include <string.h>

/**
 * \def cloc_malloc
 * \brief Allocation definition (better for memory pool wraping).
 */
#define cloc_malloc(size) malloc(size)

/**
 * \def cloc_calloc
 * \brief Allocation definition (better for memory pool wraping).
 * \warning calloc stands for casted-alloc (no 0-init here).
 */
#define cloc_calloc(num,type) ((type*)cloc_malloc((num)*sizeof(type)))

/**
 * \def cloc_free
 * \brief Deallocation definition (better for memory pool).
 */
#define cloc_free(ptr) (free(ptr))

/**
 * \def cloc_cpy
 * \brief Copy of memory.
 */
#define cloc_cpy(dst,src,num,type) (memcpy(dst,src,(num)*sizeof(type)))

/**
 * \def cloc_memset
 * \brief Set memmory to value.
 */
#define cloc_memset(dst,val,num,type) (memset(dst,val,(num)*sizeof(type)))

#endif // ! CLOC_CORE_MEM_H_INCLUDED
