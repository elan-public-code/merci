//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#include "airy.h"

#include "core/mem.h"
#include <math.h>

static inline cloc_scalar_t
max(cloc_scalar_t a, cloc_scalar_t b) { return a>b?a:b; }

#define MAX4(a,b,c,d) max(max(a,b),max(c,d))

typedef struct
{
     cloc_scalar_t a;
     cloc_scalar_t da;
} semi_term_t;

typedef struct
{
     semi_term_t fn;
     semi_term_t fnm1;
} term_t;


static void next(const void* restrict term, void* restrict next_term,
                 unsigned n, cloc_scalar_t x, void* data)
{
     const cloc_scalar_t x0 = *((cloc_scalar_t*) data);
     term_t* const cnterm = (term_t*) next_term;
     const term_t* const cterm = (const term_t*) term;

     // Copy old curr in new prev
     cnterm->fnm1.a = cterm->fn.a;
     cnterm->fnm1.da = cterm->fn.da;
     // Next
     cnterm->fn.da = x/(n+1) * -(x0*cterm->fn.a + cterm->fnm1.a*x);
     cnterm->fn.a = x/(n+1) * cterm->fn.da;
}

static void add(const void* restrict term, void* restrict addto,
                void* data)
{
     (void) data;
     ((term_t*) addto)->fn.a += ((const term_t*) term)->fn.a;
     ((term_t*) addto)->fn.da += ((const term_t*) term)->fn.da;
     ((term_t*) addto)->fnm1.a += ((const term_t*) term)->fnm1.a;
     ((term_t*) addto)->fnm1.da += ((const term_t*) term)->fnm1.da;
}

static cloc_scalar_t norm(const void* restrict term, void* data)
{
     (void) data;
     const term_t* const cterm = (const term_t*) term;
     return MAX4(fabs(cterm->fn.a), fabs(cterm->fn.da),
                 fabs(cterm->fnm1.a), fabs(cterm->fnm1.da));
}

static cloc_scalar_t uptox(cloc_scalar_t x, void* data)
{
     (void) data;
     return x + (sqrt(x*x+4.*CLOC_UPTOX_FOR_EXP)-x)/2.;
}

static void prepare(void* restrict term0, cloc_scalar_t x0,
                    void* data)
{
     // Previous term is null (otherwise special make a case in next()).
     ((term_t*) term0)->fnm1.a = 0.;
     ((term_t*) term0)->fnm1.da = 0.;
     // Data contains starting point
     *((cloc_scalar_t*) data) = x0;
}

void cloc_airy_init(cloc_airy_t* a,
                    cloc_scalar_t f0, cloc_scalar_t df0)
{
     // f(0), df(0)
     term_t* term0 = cloc_calloc(1,term_t);
     term0->fn.a = f0;
     term0->fn.da = df0;
     term0->fnm1.a = 0.;
     term0->fnm1.da = 0.;
     
     // Data contains current starting point
     cloc_scalar_t* data = cloc_calloc(1,cloc_scalar_t);
     *data = 0.;

     cloc_pse_funcs_t funcs = {next, add, norm, uptox, prepare};
     cloc_pse_init(a, term0, sizeof(term_t),
                   &funcs, CLOC_DEFAULT_EPS_TRUNC, data);
}

void cloc_airy_destroy(cloc_airy_t* a)
{
     cloc_pse_destroy(a,true,true);
}

cloc_scalar_t cloc_airy_compute(cloc_airy_t* a, cloc_scalar_t x)
{
     term_t sum;
     cloc_pse_compute(a, x, &sum);
     return sum.fn.a;
}

void cloc_airy_compute_wd(cloc_airy_t* a, cloc_scalar_t x,
                          cloc_scalar_t* f, cloc_scalar_t* df)
{
     term_t sum;
     cloc_pse_compute(a, x, &sum);
     *f = sum.fn.a;
     *df = sum.fn.da;
}
