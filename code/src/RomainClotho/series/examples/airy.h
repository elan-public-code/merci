//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#ifndef CLOC_SERIES_AIRY_H_INCLUDED
#define CLOC_SERIES_AIRY_H_INCLUDED

#include "series/pse.h"

/**
 * \typedef cloc_airy_t
 * \brief Type defining the power series to compute the airy
 * function ai(-x).
 */
typedef cloc_pse_t cloc_airy_t;

/**
 * \brief Constructor.
 * \param a The series.
 * \param f0 Function value at x=0.
 * \param df0 Slope value at x=0.
 */
void cloc_airy_init(cloc_airy_t* a, 
                    cloc_scalar_t f0, cloc_scalar_t df0);

/**
 * \brief Destructor.
 */
void cloc_airy_destroy(cloc_airy_t* a);

/**
 * \brief Compute the airy function at given x (ai(-x)).
 * \param a The series.
 * \param x Where to compute the series.
 */
cloc_scalar_t cloc_airy_compute(cloc_airy_t* a, cloc_scalar_t x);

/**
 * \brief Compute the airy function and its derivative at
 * given x (ai(-x)).
 * \param a The series.
 * \param x Where to compute the series.
 * \param f Value of the function at x.
 * \param df Value of its derivative at x.
 */
void cloc_airy_compute_wd(cloc_airy_t* a, cloc_scalar_t x,
                          cloc_scalar_t* f, cloc_scalar_t* df);

#endif // ! CLOC_SERIES_AIRY_H_INCLUDED
