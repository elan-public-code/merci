//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#include "cauchy_product.h"

#include "core/mem.h"
#include <math.h>
#include <assert.h>


static inline cloc_scalar_t min(cloc_scalar_t a, cloc_scalar_t b) { return a<b?a:b; }
static inline cloc_scalar_t max(cloc_scalar_t a, cloc_scalar_t b) { return a>b?a:b; }

#define MAX3(a,b,c) max(max(a,b),c)

typedef struct
{
     cloc_scalar_t xn;
     cloc_scalar_t en;
     cloc_scalar_t emn;
} term_t;

// Data contains the list of previous terms
typedef struct
{
     cloc_array_t ens;
     cloc_array_t emns;
} data_t;


static void next(const void* restrict term, void* restrict next_term,
                 unsigned n, cloc_scalar_t x, void* data)
{
     data_t* cdata = (data_t*) data;
     term_t* const cnterm = (term_t*) next_term;
     const term_t* const cterm = (const term_t*) term;

     // data contains only all previous terms up to n-1
     cloc_array_push_back(&(cdata->ens), &(cterm->en));
     cloc_array_push_back(&(cdata->emns), &(cterm->emn));
     assert(cloc_array_size(&(cdata->ens)) == n+1);
     assert(cloc_array_size(&(cdata->emns)) == n+1);

     // Cauchy product
     cnterm->xn = 0.;
     for(unsigned i=0; i<=n; ++i)
     {
          cnterm->xn += *((cloc_scalar_t*)cloc_array_get(&(cdata->ens), i))
               * *((cloc_scalar_t*)cloc_array_get(&(cdata->emns), n-i));
     }
     cnterm->xn *= x/(n+1);

     // Induction
     cnterm->en = x/(n+1) * cterm->en;
     cnterm->emn = -x/(n+1) * cterm->emn;
}

static void add(const void* restrict term, void* restrict addto,
                void* data)
{
     (void) data;
     ((term_t*) addto)->xn += ((const term_t*) term)->xn;
     ((term_t*) addto)->en += ((const term_t*) term)->en;
     ((term_t*) addto)->emn += ((const term_t*) term)->emn;
}

static cloc_scalar_t norm(const void* restrict term, void* data)
{
     (void) data;
     const term_t* cterm = (const term_t*) term;
     return MAX3(fabs(cterm->xn), fabs(cterm->en), fabs(cterm->emn));
}

static cloc_scalar_t uptox(cloc_scalar_t x, void* data)
{
     (void) data;
     return x + min(1.,CLOC_UPTOX_FOR_EXP/2.);
}

static void prepare(void* restrict term0, cloc_scalar_t x0,
                    void* data)
{
     (void) term0, (void) x0;
     data_t* cdata = (data_t*) data;
     cloc_array_set_size(&(cdata->ens), 0);
     cloc_array_set_size(&(cdata->emns), 0);
}

void cloc_cauchy_product_init(cloc_cauchy_product_t* cp)
{
     term_t* term0 = cloc_calloc(1,term_t);
     term0->xn = 0.;
     term0->en = 1.;
     term0->emn = 1.;
     
     // Data contains 
     data_t* data = cloc_calloc(1,data_t);
     cloc_array_init(&(data->ens), 0, sizeof(cloc_scalar_t));
     cloc_array_init(&(data->emns), 0, sizeof(cloc_scalar_t));

     cloc_pse_funcs_t funcs = {next, add, norm, uptox, prepare};
     cloc_pse_init(cp, term0, sizeof(term_t),
                   &funcs, CLOC_DEFAULT_EPS_TRUNC, data);
}

void cloc_cauchy_product_destroy(cloc_cauchy_product_t* cp)
{
     data_t* cdata = (data_t*) cp->data;
     cloc_array_destroy(&(cdata->ens));
     cloc_array_destroy(&(cdata->emns));
     cloc_pse_destroy(cp,true,true);
}

cloc_scalar_t cloc_cauchy_product_compute(cloc_cauchy_product_t* cp,
                                          cloc_scalar_t x)
{
     term_t sum;
     cloc_pse_compute(cp, x, &sum);
     return sum.xn;
}
