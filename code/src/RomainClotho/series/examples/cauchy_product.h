//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#ifndef CLOC_SERIES_CAUCHY_PRODUCT_H_INCLUDED
#define CLOC_SERIES_CAUCHY_PRODUCT_H_INCLUDED

#include "series/pse.h"

/**
 * \typedef cloc_cauchy_product_t
 * \brief Type defining the power series to compute
 * \f$\int_0^x e^t e^{-t} dt = x\f$.
 */
typedef cloc_pse_t cloc_cauchy_product_t;

/**
 * \brief Constructor.
 */
void cloc_cauchy_product_init(cloc_cauchy_product_t* cp);

/**
 * \brief Destructor.
 */
void cloc_cauchy_product_destroy(cloc_cauchy_product_t* cp);

/**
 * \brief Compute the integral of the cauchy product of \f$ e^x \f$ and
 * \f$ e^{-x} \f$ (wich is equal to \f$ x \f$) at given x.
 * \param cp The series.
 * \param x Where to compute the series.
 */
cloc_scalar_t cloc_cauchy_product_compute(cloc_cauchy_product_t* cp,
                                          cloc_scalar_t x);

#endif // ! CLOC_SERIES_CAUCHY_PRODUCT_H_INCLUDED
