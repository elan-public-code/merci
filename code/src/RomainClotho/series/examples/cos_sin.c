//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#include "cos_sin.h"

#include "core/mem.h"
#include <math.h>

typedef struct
{
     cloc_scalar_t cos;
     cloc_scalar_t sin;
} term_t;

static void next(const void* restrict term, void* restrict next_term,
                 unsigned n, cloc_scalar_t x, void* data)
{
     (void) data;
     ((term_t*) next_term)->cos = ((const term_t*) term)->sin * -x/(n+1);
     ((term_t*) next_term)->sin = ((const term_t*) term)->cos * x/(n+1);
}

static void add(const void* restrict term, void* restrict addto,
                void* data)
{
     (void) data;
     ((term_t*) addto)->cos += ((const term_t*) term)->cos;
     ((term_t*) addto)->sin += ((const term_t*) term)->sin;
}

static cloc_scalar_t norm(const void* restrict term, void* data)
{
     (void) data;
     const cloc_scalar_t c = fabs(((const term_t*) term)->cos);
     const cloc_scalar_t s = fabs(((const term_t*) term)->sin);
     return (c>s)?c:s;
}

static cloc_scalar_t uptox(cloc_scalar_t x, void* data)
{
     (void) data;
     return x + CLOC_UPTOX_FOR_EXP;
}

void cloc_cos_sin_init(cloc_cos_sin_t* cs)
{
     // cos(0), sin(0)
     term_t* term0 = cloc_calloc(1,term_t);
     term0->cos = 1.;
     term0->sin = 0.;
     
     cloc_pse_funcs_t funcs = {next, add, norm, uptox, NULL};
     cloc_pse_init(cs, term0, sizeof(term_t),
                   &funcs, CLOC_DEFAULT_EPS_TRUNC, NULL);
}

void cloc_cos_sin_destroy(cloc_cos_sin_t* cs)
{
     cloc_pse_destroy(cs,true,false);
}

void cloc_cos_sin_compute(cloc_cos_sin_t* cs, cloc_scalar_t x,
                          cloc_scalar_t* c, cloc_scalar_t* s)
{
     term_t sum;
     cloc_pse_compute(cs, x, &sum);
     *c = sum.cos;
     *s = sum.sin;
}
