//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#ifndef CLOC_SERIES_COS_SIN_H_INCLUDED
#define CLOC_SERIES_COS_SIN_H_INCLUDED

#include "series/pse.h"

/**
 * \typedef cloc_cos_sin_t
 * \brief Type defining the power series to compute cos and sin.
 */
typedef cloc_pse_t cloc_cos_sin_t;

/**
 * \brief Constructor.
 */
void cloc_cos_sin_init(cloc_cos_sin_t* cs);

/**
 * \brief Destructor.
 */
void cloc_cos_sin_destroy(cloc_cos_sin_t* cs);

/**
 * \brief Compute cos and sin at given x.
 * \param cs The series.
 * \param x Where to compute the series.
 * \param c \f$\cos(x)\f$ is returned via this variable.
 * \param s \f$\sin(x)\f$ is returned via this variable.
 */
void cloc_cos_sin_compute(cloc_cos_sin_t* cs, cloc_scalar_t x,
                          cloc_scalar_t* c, cloc_scalar_t* s);

#endif // ! CLOC_SERIES_COS_SIN_H_INCLUDED
