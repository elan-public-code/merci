//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#include "cs_product.h"

#include "core/mem.h"
#include <math.h>
#include <assert.h>


static inline cloc_scalar_t min(cloc_scalar_t a, cloc_scalar_t b) { return a<b?a:b; }
static inline cloc_scalar_t max(cloc_scalar_t a, cloc_scalar_t b) { return a>b?a:b; }

#define MAX3(a,b,c) max(max(a,b),c)

typedef struct
{
     cloc_scalar_t int_prod;
     cloc_scalar_t cos;
     cloc_scalar_t sin;
} term_t;

// Data contains the list of previous terms
typedef struct
{
     cloc_array_t coss;
     cloc_array_t sins;
} data_t;


static void next(const void* restrict term, void* restrict next_term,
                 unsigned n, cloc_scalar_t x, void* data)
{
     data_t* cdata = (data_t*) data;
     term_t* const cnterm = (term_t*) next_term;
     const term_t* const cterm = (const term_t*) term;

     // data contains only all previous terms up to n-1
     cloc_array_push_back(&(cdata->coss), &(cterm->cos));
     cloc_array_push_back(&(cdata->sins), &(cterm->sin));
     assert(cloc_array_size(&(cdata->coss)) == n+1);
     assert(cloc_array_size(&(cdata->sins)) == n+1);

     // Cs product
     cnterm->int_prod = 0.;
     for(unsigned i=0; i<=n; ++i)
     {
          cnterm->int_prod += 
               *((cloc_scalar_t*)cloc_array_get(&(cdata->coss), i))
               * *((cloc_scalar_t*)cloc_array_get(&(cdata->sins), n-i));
     }
     cnterm->int_prod *= x/(n+1);

     // Induction
     cnterm->cos = -x/(n+1) * cterm->sin;
     cnterm->sin = x/(n+1) * cterm->cos;
}

static void add(const void* restrict term, void* restrict addto,
                void* data)
{
     (void) data;
     ((term_t*) addto)->int_prod += ((const term_t*) term)->int_prod;
     ((term_t*) addto)->cos += ((const term_t*) term)->cos;
     ((term_t*) addto)->sin += ((const term_t*) term)->sin;
}

static cloc_scalar_t norm(const void* restrict term, void* data)
{
     (void) data;
     const term_t* const cterm = (const term_t*) term;
     return MAX3(fabs(cterm->int_prod), fabs(cterm->cos), fabs(cterm->sin));
}

static cloc_scalar_t uptox(cloc_scalar_t x, void* data)
{
     (void) data;
     return x + min(1,(CLOC_UPTOX_FOR_EXP)/2.);
}

static void prepare(void* restrict term0, cloc_scalar_t x0,
                    void* data)
{
     (void) term0, (void) x0;
     data_t* cdata = (data_t*) data;
     cloc_array_set_size(&(cdata->coss), 0);
     cloc_array_set_size(&(cdata->sins), 0);
}

void cloc_cs_product_init(cloc_cs_product_t* csp)
{
     term_t* term0 = cloc_calloc(1,term_t);
     term0->int_prod = 0.;
     term0->cos = 1.;
     term0->sin = 0.;
     
     // Data contains 
     data_t* data = cloc_calloc(1,data_t);
     cloc_array_init(&(data->coss), 0, sizeof(cloc_scalar_t));
     cloc_array_init(&(data->sins), 0, sizeof(cloc_scalar_t));

     cloc_pse_funcs_t funcs = {next, add, norm, uptox, prepare};
     cloc_pse_init(csp, term0, sizeof(term_t),
                   &funcs, CLOC_DEFAULT_EPS_TRUNC, data);
}

void cloc_cs_product_destroy(cloc_cs_product_t* csp)
{
     data_t* cdata = (data_t*) csp->data;
     cloc_array_destroy(&(cdata->coss));
     cloc_array_destroy(&(cdata->sins));
     cloc_pse_destroy(csp,true,true);
}

cloc_scalar_t cloc_cs_product_compute(cloc_cs_product_t* csp,
                                      cloc_scalar_t x)
{
     term_t sum;
     cloc_pse_compute(csp, x, &sum);
     return sum.int_prod;
}
