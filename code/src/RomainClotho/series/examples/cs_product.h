//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#ifndef CLOC_SERIES_CS_PRODUCT_H_INCLUDED
#define CLOC_SERIES_CS_PRODUCT_H_INCLUDED

#include "series/pse.h"

/**
 * \typedef cloc_cs_product_t
 * \brief Type defining the power series to compute
 * \f$\int_0^x \cos(u)\sin(u) du = \frac{1-\cos(2u)}{4}\f$.
 */
typedef cloc_pse_t cloc_cs_product_t;

/**
 * \brief Constructor.
 */
void cloc_cs_product_init(cloc_cs_product_t* csp);

/**
 * \brief Destructor.
 */
void cloc_cs_product_destroy(cloc_cs_product_t* csp);

/**
 * \brief Compute the integral of the product of cos and sin
 * at given x.
 * \param csp The series.
 * \param x Where to compute the series.
 */
cloc_scalar_t cloc_cs_product_compute(cloc_cs_product_t* csp,
                                      cloc_scalar_t x);

#endif // ! CLOC_SERIES_CS_PRODUCT_H_INCLUDED
