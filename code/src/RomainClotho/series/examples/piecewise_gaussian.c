//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#include "piecewise_gaussian.h"

#include "core/mem.h"
#include <math.h>

static inline cloc_scalar_t max(cloc_scalar_t a, cloc_scalar_t b) { return a>b?a:b; }

typedef struct
{
     cloc_scalar_t gn;
     cloc_scalar_t gnm1;
} term_t;

typedef struct
{
     cloc_scalar_t bcurr;
     cloc_scalar_t acurr;
     cloc_scalar_t* restrict vpts; // values at pts
     const cloc_scalar_t* restrict pts;
} data_t;

static void next(const void* restrict term,
                 void* restrict next_term,
                 unsigned n, cloc_scalar_t x, void* data)
{
     //const unsigned index = ((cloc_ppse_data_t*) data)->piece_index;
     data_t* cdata = (data_t*)(((cloc_ppse_data_t*) data)->data);
     term_t* restrict cnterm = (term_t*) next_term;
     const term_t* restrict cterm = (const term_t*) term;
     if(n==0)
          cnterm->gn = -x/(n+1) * cdata->bcurr * cterm->gn;
     else
          cnterm->gn = -x/(n+1)* (x*cdata->acurr*cterm->gnm1
                                  + cdata->bcurr*cterm->gn);
     cnterm->gnm1 = cterm->gn;
}

static void add(const void* restrict term, void* restrict addto,
                void* data)
{
     (void) data;
     ((term_t*) addto)->gn += ((const term_t*) term)->gn;
     ((term_t*) addto)->gnm1 += ((const term_t*) term)->gnm1;
}

static cloc_scalar_t uptox(cloc_scalar_t x, void* data)
{
     // Get index
     const unsigned i = ((cloc_ppse_data_t*) data)->piece_index;
     data_t* cdata = (data_t*)(((cloc_ppse_data_t*) data)->data);
     const cloc_scalar_t aacurr = fabs((cdata->vpts[i+1]-cdata->vpts[i])
                                       / (cdata->pts[i+1]-cdata->pts[i]));
     const cloc_scalar_t abcurr = fabs(cdata->vpts[i] + x*cdata->acurr);
     if(aacurr != 0.)
          return x + (sqrt(abcurr*abcurr+4.*aacurr*CLOC_UPTOX_FOR_EXP)-abcurr)/(2.*aacurr);
     else if(abcurr != 0.)
          return x + CLOC_UPTOX_FOR_EXP/(2.*abcurr);
     else
          return x + CLOC_UPTOX_MAX_LENGTH;
}

static cloc_scalar_t norm(const void* restrict term, void* data)
{
     (void) data;
     return max(fabs(((const term_t*) term)->gn),
                fabs(((const term_t*) term)->gnm1));
}

static void prepare(void* restrict term0, cloc_scalar_t x0,
                    void* data)
{
     (void) term0;
     // ((term_t*) term0)->gnm1 = 0.; //useless since if(n==0) case is
     // tested in next().
     // Get index
     const unsigned i = ((cloc_ppse_data_t*) data)->piece_index;
     data_t* cdata = (data_t*)(((cloc_ppse_data_t*) data)->data);
     cdata->acurr = (cdata->vpts[i+1]-cdata->vpts[i])
          / (cdata->pts[i+1]-cdata->pts[i]);
     cdata->bcurr = cdata->vpts[i] + x0*cdata->acurr;
}

void cloc_pgauss_init(cloc_pgauss_t* pg,
                      const cloc_scalar_t* pts,
                      const cloc_scalar_t* values_at_pts,
                      unsigned npts)
{
     // Term0
     term_t* restrict term0 = cloc_calloc(1,term_t);
     //term0->gnm1 = 0.;
     term0->gn = 1.;
     // Data init
     data_t* data = cloc_calloc(1,data_t);
     data->vpts = cloc_calloc(npts,cloc_scalar_t);
     cloc_cpy(data->vpts, values_at_pts, npts, cloc_scalar_t);
     data->pts = (const cloc_scalar_t*) pts; // Temporarly
     cloc_ppse_data_t index_data = {0, data};
     prepare(term0, 0., &index_data);

     // PPSE init
     cloc_pse_funcs_t funcs = {next, add, norm, uptox, prepare};
     cloc_ppse_init(pg, pts, npts, term0, sizeof(term_t),
                    &funcs, CLOC_DEFAULT_EPS_TRUNC, data);
     // It's useless to compute points again, use ones in ppse
     data->pts = pg->xs;
}

void cloc_pgauss_destroy(cloc_pgauss_t* pg)
{
     cloc_free(((data_t*) pg->data.data)->vpts);
     cloc_ppse_destroy(pg, true, true);
}

cloc_scalar_t cloc_pgauss_compute(cloc_pgauss_t* pg, cloc_scalar_t x)
{
     term_t sum;
     cloc_ppse_compute(pg, x, &sum);
     return sum.gn;
}
