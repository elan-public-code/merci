//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#ifndef CLOC_SERIES_PIECEWISE_GAUSSIAN_H_INCLUDED
#define CLOC_SERIES_PIECEWISE_GAUSSIAN_H_INCLUDED

#include "series/ppse.h"

/**
 * \typedef cloc_pgauss_t
 * \brief Type defining the power series to compute the piecewise gaussian.
 * \f$ y' = -\lambda(x)y\f$ where \f$ \lambda\f$ is piecewise affine (C1).
 */
typedef cloc_ppse_t cloc_pgauss_t;

/**
 * \brief Constructor.
 */
void cloc_pgauss_init(cloc_pgauss_t* pg,
                      const cloc_scalar_t* pts,
                      const cloc_scalar_t* values_at_pts,
                      unsigned npts);

/**
 * \brief Destructor.
 */
void cloc_pgauss_destroy(cloc_pgauss_t* pg);

/**
 * \brief Compute piecewise gaussian at given x.
 * \param pg The piecewise power series.
 * \param x Where to compute the series.
 */
cloc_scalar_t cloc_pgauss_compute(cloc_pgauss_t* pg, cloc_scalar_t x);

#endif // ! CLOC_SERIES_PIECEWISE_GAUSSIAN_H_INCLUDED
