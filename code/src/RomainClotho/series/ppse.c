//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#include "ppse.h"
#include "core/algorithms.h"

#include "core/mem.h"
#include <assert.h>

void cloc_ppse_init(cloc_ppse_t* ppse,
                    const cloc_scalar_t* nodes, unsigned nnodes,
                    const void* term0, unsigned size_term,
                    const cloc_pse_funcs_t* funcs,
                    cloc_scalar_t eps_trunc,
                    void* data)
{
     const unsigned npieces = nnodes-1;
     ppse->nnodes = nnodes;
     ppse->data.data = data;
     ppse->data.piece_index = 0;
     // Init x ticks
     assert( nodes[0] == 0. );
     ppse->xs = cloc_calloc(nnodes,cloc_scalar_t);
     cloc_cpy(ppse->xs, nodes, nnodes, cloc_scalar_t);
     // Init all piece-series
     ppse->pses = cloc_calloc(npieces,cloc_pse_t);
     cloc_pse_init(ppse->pses+0, term0, size_term,
                   funcs, eps_trunc, &(ppse->data));
     void* restrict sum;
     for(unsigned i=1; i<npieces; ++i)
     {
          // Compute new term0 (end of previous segment)
          sum = cloc_malloc(size_term);
          cloc_pse_compute(ppse->pses+(i-1), nodes[i]-nodes[i-1], sum);
          // It have to be prepared as a term0 of segment i !
          ppse->data.piece_index = i;
          funcs->prepare(sum, 0., &(ppse->data));
          // Init next series
          cloc_pse_init(ppse->pses+i, sum, size_term,
                        funcs, eps_trunc, &(ppse->data));
     }
}

void cloc_ppse_compute(cloc_ppse_t* ppse, cloc_scalar_t x,
                       void* restrict sum)
{
     const unsigned index = cloc_index_of_segment(ppse->xs, ppse->nnodes, x);
     ppse->data.piece_index = index;
     cloc_pse_compute(ppse->pses+index, x-ppse->xs[index], sum);
}

void cloc_ppse_destroy(cloc_ppse_t* ppse, bool destroy_term0,
                       bool destroy_data)
{
     // Destroying first pse (with term0?)
     cloc_pse_destroy(ppse->pses+0, destroy_term0, false);
     // Destroying all other ppse with their term0
     for(unsigned i=1; i<ppse->nnodes-1; ++i)
     {
          cloc_pse_destroy(ppse->pses+i, true, false);
     }
     // Destroying data
     if(destroy_data) cloc_free(ppse->data.data);
     // It remains ticks and pse arrays
     cloc_free(ppse->xs);
     cloc_free(ppse->pses);
}
