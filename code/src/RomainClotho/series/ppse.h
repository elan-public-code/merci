//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#ifndef CLOC_SERIES_PPSE_H_INCLUDED
#define CLOC_SERIES_PPSE_H_INCLUDED

#include "pse.h"

/**
 * \file  ppse.h
 * \brief Piecewise Power Series Expension computer using cutting algorithm
 * to avoid losing precision.
 * 
 * 
 * 
 * \author Romain Casati
 * \date   28rd Jan. 2013
 */

/**
 * \struct cloc_ppse_data_t
 * \brief The ppse data type, it stores the pse data plus the current index.
 */
typedef struct
{
     unsigned piece_index;
     void* data;
} cloc_ppse_data_t;

/**
 * \struct cloc_ppse_t
 * \brief the structure of a piecewise series expansion.
 */
typedef struct
{
     unsigned nnodes;
     cloc_scalar_t* restrict xs;
     cloc_pse_t* restrict pses;
     cloc_ppse_data_t data;
} cloc_ppse_t;

/**
 * Initialize a new piecewise power series.
 * 
 * \param ppse The pointer to the (allocated) series
 * \param nodes Array of size nnodes containing x values for pieces,
 * starting at 0. in increasing order.
 * \param nnodes Number of nodes of the series.
 * of each segment (copied).
 * \param term0 The initial point of the ppse. It is not modified by the
 * API but must live until the destruction of ppse since it is not copied.
 * term0 is supposed to be "prepared".
 * \param size_term The (supposed constant) size of the terms.
 * \param funcs Strucutre containing carateristics functions (copied).
 * \param data Extra data passed at each call to funcs API.
 * \param eps_trunc Error under wich terms will not be computed
 * (truncation of the sum).
 * 
 * \warning term0 must live until the destruction of pse since it is not
 * copied.
 * \warning term0 is supposed to be prepared
 * (funcs->prepare(term0) already called).
 * \warning nodes[0] must be 0.
 */
void cloc_ppse_init(cloc_ppse_t* ppse,
                    const cloc_scalar_t* nodes, unsigned nnodes,
                    const void* term0, unsigned size_term,
                    const cloc_pse_funcs_t* funcs,
                    cloc_scalar_t eps_trunc,
                    void* data);

/**
 * Compute the (patial) sum of the piecewise power series at given x.
 * 
 * \param ppse The power series
 * \param x Where to compute the series (global)
 * \param sum Returned value (must be allocated): partial sum at x.
 * \warning The sum pointer must be allocated.
 */
void cloc_ppse_compute(cloc_ppse_t* ppse, cloc_scalar_t x,
                       void* restrict sum);

/**
 * Destroy the *content* of the computer, not the computer itself.
 * It's your job to free ppse (only in case of heap allocation (malloc))
 * 
 * \param ppse The piecewise power series
 * \param destroy_term0 The first term (term0) passed in the computer
 * at init can be freed by this call (or not in case you need it).
 * \param destroy_data The data passed in the computer at init can be
 * freed by this call (or not in case you need it).
 */
void cloc_ppse_destroy(cloc_ppse_t* ppse, bool destroy_term0,
                       bool destroy_data);

#endif // ! CLOC_SERIES_PPSE_H_INCLUDED
