//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#include "pse.h"

#include "core/mem.h"
#include <assert.h>

#define SWAP_PTR(a,b) do { void* const tmp = a; a = b; b = tmp; } while(0)

/**
 * \typedef cloc_xterm_t
 * \brief Basic struct for associating a term and a position.
 */
typedef struct
{
     const cloc_scalar_t x;
     const void* restrict term;
} cloc_xterm_t;

static void dummy_prepare(void* restrict term0, cloc_scalar_t x0,
                          void* data)
{
     (void) term0, (void) x0, (void) data;
}

void cloc_pse_init(cloc_pse_t* pse,
                   const void* term0, unsigned size_term,
                   const cloc_pse_funcs_t* funcs,
                   cloc_scalar_t eps_trunc,
                   void* data)
{
     // Callbacks
     pse->funcs = *funcs;
     if(funcs->prepare == NULL) pse->funcs.prepare = dummy_prepare;
     //
     pse->size_term = size_term;
     pse->data = data;
     pse->eps_trunc = eps_trunc;
     pse->xmax = funcs->uptox(0., data);
     // Init terms array
     cloc_xterm_t xterm = {0., term0};
     cloc_array_init(&(pse->start_pts), 0, sizeof(cloc_xterm_t));
     cloc_array_push_back(&(pse->start_pts), &xterm);
     // temporary terms
     pse->tmp_terma = cloc_malloc(size_term);
     pse->tmp_termb = cloc_malloc(size_term);
}

// Note: x is relative here (==0 for term0).
static void naiv_compute(cloc_scalar_t x, cloc_scalar_t x0,
                         const void* restrict term0,
                         void* restrict tmp_terma, void* restrict tmp_termb,
                         void* restrict sum,
                         unsigned size_term, const cloc_pse_funcs_t* funcs,
                         cloc_scalar_t eps_trunc, void* data)
{
     assert( x >= 0. && x0 >= 0. );

     // Copy first term in sum
     cloc_cpy(sum, term0, size_term, unsigned char);
     
     // Prepare term0 (*and data*).
     funcs->prepare(sum, x0, data);

     // Case where there is nothing to compute
     if(x == 0.) return;

     // Computation
     unsigned n = 0;
     funcs->next(sum, tmp_terma, n++, x, data);
     cloc_scalar_t norm;
#ifndef NDEBUG
     const cloc_scalar_t norm0 = funcs->norm(tmp_terma, data);
#endif
     while((norm = funcs->norm(tmp_terma, data)) > eps_trunc)
     {
          assert(norm < CLOC_HILLOCK_EXP*norm0);
          funcs->add(tmp_terma, sum, data);
          funcs->next(tmp_terma, tmp_termb, n++, x, data);
          SWAP_PTR(tmp_terma, tmp_termb);
     }
}

static void compute_next_point(cloc_pse_t* pse)
{
     // Last term used as starting point
     const cloc_xterm_t* xterm = (cloc_xterm_t*) cloc_array_last(&(pse->start_pts));
     const void* restrict term0 = xterm->term;
     const cloc_scalar_t x0 = xterm->x, x = pse->xmax;
     void* restrict sum = cloc_malloc(pse->size_term);
     
     // Computing
     naiv_compute(x-x0, x0, term0, pse->tmp_terma, pse->tmp_termb,
                  sum, pse->size_term, &(pse->funcs),
                  pse->eps_trunc, pse->data);

     // Storing result
     cloc_xterm_t xsum = {x, sum};
     cloc_array_push_back(&(pse->start_pts), &xsum);

     // Computing new xmax
     pse->xmax = pse->funcs.uptox(x, pse->data);
}

static unsigned closest_start_pts(const cloc_array_t* pts,
                                  cloc_scalar_t x)
{
     const cloc_xterm_t* xterm = (cloc_xterm_t*) cloc_array_last(pts);
     unsigned imax = cloc_array_size(pts)-1;
     if(xterm->x <= x) return imax;

     xterm = (cloc_xterm_t*) cloc_array_first(pts);
     unsigned imid, imin = 0;
     
     while(imax > imin+1)
     {
          imid = (imax+imin)/2;
          xterm = (cloc_xterm_t*) cloc_array_get(pts, imid);
          if(xterm->x > x) imax = imid;
          else imin = imid;
     }
     return imin;
}

void cloc_pse_compute(cloc_pse_t* pse, cloc_scalar_t x, void* restrict sum)
{
     assert( x >= 0. );
     // Case where x is outside already computed range
     while(pse->xmax < x)
     {
          compute_next_point(pse);
     }
     
     // Get the start of segment containing x
     const unsigned segment_index = closest_start_pts(&(pse->start_pts), x);

     const cloc_xterm_t* xterm = (cloc_xterm_t*) cloc_array_get(&(pse->start_pts), segment_index);
     const void* restrict term0 = xterm->term;
     const cloc_scalar_t x0 = xterm->x;
     
     // Computing
     naiv_compute(x-x0, x0, term0, pse->tmp_terma, pse->tmp_termb,
                  sum, pse->size_term, &(pse->funcs),
                  pse->eps_trunc, pse->data);
}

void cloc_pse_destroy(cloc_pse_t* pse, bool destroy_term0,
                      bool destroy_data)
{
     // Destoying terms
     const unsigned terms_size = cloc_array_size(&(pse->start_pts));
     const void* restrict term;
     
     for(unsigned i=destroy_term0?0:1; i<terms_size; ++i)
     {
          term = ((cloc_xterm_t*) cloc_array_get(&(pse->start_pts),i))->term;
          // Little trick to avoid cast-qual warning.
          union { const void* cp; void* ncp;} ptr = {term};
          cloc_free(ptr.ncp);
     }
     cloc_array_destroy(&(pse->start_pts));

     // data destroy ?
     if(destroy_data) cloc_free(pse->data);

     // tmp terms
     cloc_free(pse->tmp_terma);
     cloc_free(pse->tmp_termb);
}
