//    Copyright 2012 Romain Casati
//
//    This file is part of Cloc.
//
//    Cloc is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Cloc is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Cloc.  If not, see <http://www.gnu.org/licenses/>.
//
#ifndef CLOC_SERIES_PSE_H_INCLUDED
#define CLOC_SERIES_PSE_H_INCLUDED

#include <stdbool.h>
#include "core/definitions.h"
#include "core/array.h"

/**
 * \file  pse.h
 * \brief Power Series Expension computer using cutting algorithm
 * to avoid losing precision.
 * 
 * 
 * 
 * \author Romain Casati
 * \date   23rd Jan. 2013
 */

/**
 * \def CLOC_UPTOX_FOR_EXP
 * \brief Maximal segment length for computing exp as Power Series
 * without too much cancellation.
 */
#define CLOC_UPTOX_FOR_EXP    6.8

/**
 * \def CLOC_UPTOX_MAX_LENGTH
 * \brief Maximal segment length returned by uptox functions.
 */
#define CLOC_UPTOX_MAX_LENGTH 1e10

/**
 * \def CLOC_HILLOCK_EXP
 * \brief Maximal value for the coefficient of the exponential series
 * when cuted with CLO_UPTOX_FOR_EXP.
 */
#define CLOC_HILLOCK_EXP      3e1

/**
 * \def CLOC_DEFAULT_EPS_TRUNC
 * \brief Default trucation threshold.
 */
#define CLOC_DEFAULT_EPS_TRUNC    1e-18

/**
 * \typedef cloc_func_next_t
 * \brief Induction function.
 *
 * This function implements the induction formula (containing x) and is
 * called for *local* x i.e. on sub-intervals.
 */
typedef void (*cloc_func_next_t)(const void* restrict term,
                                 void* restrict next_term,
                                 unsigned n, cloc_scalar_t x, void* data);
/**
 * \typedef cloc_func_add_t
 * \brief Summation function.
 * 
 * Add term to addto
 */
typedef void (*cloc_func_add_t)(const void* restrict term,
                                void* restrict addto,
                                void* data);

/**
 * \typedef cloc_func_uptox_t
 * \brief Get the maximal point > x where one can compute the sum without
 * cancellation problems.
 *
 * Returns the norm of the term.
 */
typedef cloc_scalar_t (*cloc_func_uptox_t)(cloc_scalar_t x, void* data);

/**
 * \typedef cloc_func_norm_t
 * \brief Norm function.
 *
 * Returns the norm of the term.
 */
typedef cloc_scalar_t (*cloc_func_norm_t)(const void* restrict term,
                                          void* data);

/**
 * \typedef cloc_func_prepare_t
 * \brief Called on each first term of each segment.
 * term0 contains the sum at x0, it must be prepared as the first
 * term of the sum computed for x>x0. Note that you have to use
 * this function to prepare your data for a new segment.
 */
typedef void (*cloc_func_prepare_t)(void* restrict term0, cloc_scalar_t x0,
                                    void* data);

/**
 * \struct cloc_pse_funcs_t
 * \brief Structure containing functions caracterizing the Power Series.
 */
typedef struct
{
     cloc_func_next_t next;
     cloc_func_add_t add;
     cloc_func_norm_t norm;
     cloc_func_uptox_t uptox;
     cloc_func_prepare_t prepare;
} cloc_pse_funcs_t;

/**
 * \struct cloc_pse_t
 * 
 * The structure of a power series.
 */
typedef struct
{
     unsigned size_term;       // sizeof(*term)
     cloc_pse_funcs_t funcs;   // user-defined functions
     void* data;               // extra user data
     cloc_scalar_t eps_trunc;  // truncation threshold
     cloc_array_t start_pts;   // dynamic array of starting points
     cloc_scalar_t xmax;       // current maximum x where the PS can be computed
     void* restrict tmp_terma; // temporary term used for computation
     void* restrict tmp_termb; // temporary term used for computation
} cloc_pse_t;

/**
 * Initialize a new power series.
 * 
 * \param pse The pointer to the (allocated) series
 * \param term0 The initial point of the pse. It is not modified by the
 * API but must live until the destruction of pse since it is not copied.
 * term0 is supposed to be "prepared".
 * \param size_term The (supposed constant) size of the terms.
 * \param funcs Strucutre containing carateristics functions (copied).
 * Notes that the "prepare" function can be NULL.
 * \param data Extra data passed to each call to funcs API.
 * \param eps_trunc Error under wich terms will not be computed
 * (truncation of the sum).
 * 
 * \warning term0 must live until the destruction of pse since it is not
 * copied.
 * \warning term0 is supposed to be prepared
 * (funcs->prepare(term0) already called).
 */
void cloc_pse_init(cloc_pse_t* pse,
                   const void* term0, unsigned size_term,
                   const cloc_pse_funcs_t* funcs,
                   cloc_scalar_t eps_trunc,
                   void* data);

/**
 * Compute the (patial) sum of the power series at given x.
 * 
 * \param pse The power series
 * \param x Where to compute the series (global)
 * \param sum Returned value (must be allocated): partial sum at x.
 * \warning The sum pointer must be allocated.
 */
void cloc_pse_compute(cloc_pse_t* pse, cloc_scalar_t x, void* restrict sum);

/**
 * Destroy the *content* of the computer, not the computer itself.
 * It's your job to free pse (only in case of heap allocation (malloc))
 * 
 * \param pse The power series
 * \param destroy_term0 The first term (term0) passed in the computer
 * at init can be freed by this call (or not in case you need it).
 * \param destroy_data The data passed in the computer at init can be
 * freed by this call (or not in case you need it).
 */
void cloc_pse_destroy(cloc_pse_t* pse, bool destroy_term0,
                      bool destroy_data);

#endif // ! CLOC_SERIES_PSE_H_INCLUDED
