/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "CElasticEnergy.hpp"
#include "Log/Logger.hpp"
#include <string>
#include <cmath>

void CElasticEnergy::FixPhysics(Eigen::Matrix3r K, double ribbonLength)
{
  this->K=K;
  this->ribbonLength=ribbonLength;
}

void CElasticEnergy::FixPhysics(double YoungModulus, double poissonRatio, double width, double thickness, double ribbonLength)
{
  FixPhysics(ParamsToK(YoungModulus,poissonRatio,width,thickness),ribbonLength);
}

void CElasticEnergy::FixPt(LinearScalar omega1, LinearScalar omega2, LinearScalar omega3)
{
  FixPt(omega1,omega2,omega3,LinearScalar(),LinearScalar(),LinearScalar());
}

void CElasticEnergy::FixPt(LinearScalar w1, LinearScalar w2, LinearScalar w3, LinearScalar nw1, LinearScalar nw2, LinearScalar nw3)
{
  omega1=w1;
  omega2=w2;
  omega3=w3;
  naturalOmega1=nw1;
  naturalOmega2=nw2;
  naturalOmega3=nw3;
}



real CElasticEnergy::computeEnergy()
{
  Eigen::Vector3r lambdaA, lambdaB, lambdaNA,lambdaNB;
  lambdaA<<omega1.A,omega2.A,omega3.A;
  lambdaB<<omega1.B,omega2.B,omega3.B;
  lambdaNA<<naturalOmega1.A,naturalOmega2.A,naturalOmega3.A;
  lambdaNB<<naturalOmega1.B,naturalOmega2.B,naturalOmega3.B;
  Eigen::Vector3r lambdaDA,lambdaDB;
  lambdaDA=lambdaA-lambdaNA;
  lambdaDB=lambdaB-lambdaNB;
  
  double CC0=0.5*lambdaDB.transpose()*K*lambdaDB;
  double CC1=0.5*(static_cast<double>(lambdaDA.transpose()*K*lambdaDB)+static_cast<double>(lambdaDB.transpose()*K*lambdaDA));
  double CC2=0.5*lambdaDA.transpose()*K*lambdaDA;
  
  const real L = ribbonLength;
  
  return CC0*L+CC1*L*L*0.5+CC2*L*L*L/3.;
  
}

Eigen::Vector6r CElasticEnergy::computeGrad()
{
  Eigen::Vector6r res;  
  Eigen::Vector3r lambdaA, lambdaB, lambdaNA,lambdaNB;
  lambdaA<<omega1.A,omega2.A,omega3.A;
  lambdaB<<omega1.B,omega2.B,omega3.B;
  lambdaNA<<naturalOmega1.A,naturalOmega2.A,naturalOmega3.A;
  lambdaNB<<naturalOmega1.B,naturalOmega2.B,naturalOmega3.B;
  Eigen::Vector3r lambdaDA,lambdaDB;
  lambdaDA=lambdaA-lambdaNA;
  lambdaDB=lambdaB-lambdaNB;
  Eigen::Vector3r ex,ey,ez;
  ex<<1.,0.,0.;
  ey<<0.,1.,0.;
  ez<<0.,0.,1.;
  
  const real L = ribbonLength;
  
  double CC1Ax=0.5*(static_cast<double>(ex.transpose()*K*lambdaDB)+static_cast<double>(lambdaDB.transpose()*K*ex));
  double CC1Ay=0.5*(static_cast<double>(ey.transpose()*K*lambdaDB)+static_cast<double>(lambdaDB.transpose()*K*ey));
  double CC1Az=0.5*(static_cast<double>(ez.transpose()*K*lambdaDB)+static_cast<double>(lambdaDB.transpose()*K*ez));
  double CC2Ax=0.5*(static_cast<double>(ex.transpose()*K*lambdaDA)+static_cast<double>(lambdaDA.transpose()*K*ex));
  double CC2Ay=0.5*(static_cast<double>(ey.transpose()*K*lambdaDA)+static_cast<double>(lambdaDA.transpose()*K*ey));
  double CC2Az=0.5*(static_cast<double>(ez.transpose()*K*lambdaDA)+static_cast<double>(lambdaDA.transpose()*K*ez));
  
  double CC0Bx=0.5*(static_cast<double>(ex.transpose()*K*lambdaDB)+static_cast<double>(lambdaDB.transpose()*K*ex));
  double CC0By=0.5*(static_cast<double>(ey.transpose()*K*lambdaDB)+static_cast<double>(lambdaDB.transpose()*K*ey));
  double CC0Bz=0.5*(static_cast<double>(ez.transpose()*K*lambdaDB)+static_cast<double>(lambdaDB.transpose()*K*ez));
  double CC1Bx=0.5*(static_cast<double>(ex.transpose()*K*lambdaDA)+static_cast<double>(lambdaDA.transpose()*K*ex));
  double CC1By=0.5*(static_cast<double>(ey.transpose()*K*lambdaDA)+static_cast<double>(lambdaDA.transpose()*K*ey));
  double CC1Bz=0.5*(static_cast<double>(ez.transpose()*K*lambdaDA)+static_cast<double>(lambdaDA.transpose()*K*ez));
  
  res[0]=CC1Ax*L*L*0.5+CC2Ax*L*L*L/3.;
  res[2]=CC1Ay*L*L*0.5+CC2Ay*L*L*L/3.;
  res[4]=CC1Az*L*L*0.5+CC2Az*L*L*L/3.;
  res[1]=CC0Bx*L+CC1Bx*L*L*0.5;
  res[3]=CC0By*L+CC1By*L*L*0.5;
  res[5]=CC0Bz*L+CC1Bz*L*L*0.5;
  return res;
  
}

Eigen::Matrix6r CElasticEnergy::computeHessian()
{
  Eigen::Matrix6r res;
  Eigen::Vector3r lambdaA, lambdaB, lambdaNA,lambdaNB;
  lambdaA<<omega1.A,omega2.A,omega3.A;
  lambdaB<<omega1.B,omega2.B,omega3.B;
  lambdaNA<<naturalOmega1.A,naturalOmega2.A,naturalOmega3.A;
  lambdaNB<<naturalOmega1.B,naturalOmega2.B,naturalOmega3.B;
  Eigen::Vector3r lambdaDA,lambdaDB;
  lambdaDA=lambdaA-lambdaNA;
  lambdaDB=lambdaB-lambdaNB;
  Eigen::Vector3r ex,ey,ez;
  ex<<1.,0.,0.;
  ey<<0.,1.,0.;
  ez<<0.,0.,1.;
  
  const real L = ribbonLength;
  
  double CCxx=ex.transpose()*K*ex;
  double CCyx=0.5*(static_cast<double>(ey.transpose()*K*ex)+static_cast<double>(ex.transpose()*K*ey));
  double CCzx=0.5*(static_cast<double>(ez.transpose()*K*ex)+static_cast<double>(ex.transpose()*K*ez));
  double CCyy=ey.transpose()*K*ey;
  double CCzy=0.5*(static_cast<double>(ez.transpose()*K*ey)+static_cast<double>(ey.transpose()*K*ez));
  double CCzz=ez.transpose()*K*ez;
  
  res(0,0)=CCxx*L*L*L/3.;
  res(0,1)=CCxx*L*L*0.5;
  res(0,2)=CCyx*L*L*L/3.;
  res(0,3)=CCyx*L*L*0.5;
  res(0,4)=CCzx*L*L*L/3.;
  res(0,5)=CCzx*L*L*0.5;
  
  res(1,1)=CCxx*L;
  res(1,2)=CCyx*L*L*0.5;
  res(1,3)=CCyx*L;
  res(1,4)=CCzx*L*L*0.5;
  res(1,5)=CCzx*L;
  
  res(2,2)=CCyy*L*L*L/3.;
  res(2,3)=CCyy*L*L*0.5;
  res(2,4)=CCzy*L*L*L/3.;
  res(2,5)=CCzy*L*L*0.5;
  
  res(3,3)=CCyy*L;
  res(3,4)=CCzy*L*L*0.5;
  res(3,5)=CCzy*L;
  
  res(4,4)=CCzz*L*L*L/3.;
  res(4,5)=CCzz*L*L*0.5;
  
  res(5,5)=CCzz*L;
  
  for(int i=1;i<6;i++)for(int j=0;j<i;j++)res(i,j)=res(j,i);
  return res;
}

Eigen::Matrix3r CElasticEnergy::ParamsToK(double YoungModulus, double poissonRatio, double width, double thickness) 
{
  
  Eigen::Matrix3r K;
  double mu=YoungModulus/(2.0*(1.+poissonRatio));
  double th2=thickness*thickness;
  double wh2=width*width;
  double I1= M_PI_4 * width*thickness*th2;
  double I2=M_PI_4 * width*wh2*thickness;
  double J=M_PI*(width*thickness*wh2*th2)/(wh2+th2);
  K<<(YoungModulus*I1),0,0,
     0,(YoungModulus*I2),0,
     0,0,(mu*J);
  return K;
}
