/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef CELASTICENERGY_H
#define CELASTICENERGY_H

#include <BaseCore/real.hpp>
#include <BaseCore/LinearScalar.hpp>
#include <Eigen/Dense>
#include <optional>

class CElasticEnergy
{
public:
  static Eigen::Matrix3r ParamsToK(double YoungModulus, double poissonRatio, double width, double thickness);
  void FixPhysics(Eigen::Matrix3r K, double ribbonLength);
  void FixPhysics(double YoungModulus, double poissonRatio, double width, double thickness, double ribbonLength);
  void FixPt(LinearScalar omega1, LinearScalar omega2, LinearScalar omega3);
  void FixPt(LinearScalar omega1, LinearScalar omega2, LinearScalar omega3, LinearScalar naturalOmega1, LinearScalar naturalOmega2, LinearScalar naturalOmega3);
  real computeEnergy();
  Eigen::Vector6r computeGrad();
  Eigen::Matrix6r computeHessian();
private:
  real ribbonLength;
  Eigen::Matrix3r K;
  LinearScalar omega1;
  LinearScalar omega2; 
  LinearScalar omega3; 
  LinearScalar naturalOmega1; 
  LinearScalar naturalOmega2; 
  LinearScalar naturalOmega3;
};

#endif // CELASTICENERGY_H


