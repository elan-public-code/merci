/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "CSegmentSystem.hpp"
#include <MathOperations.hpp>

using namespace MathOperations;

CSegmentSystem::CSegmentSystem(const Eigen::Vector3r& gravity, const real& linearDensity, const Eigen::Matrix3r& K, const real& ribbonLength) :
  G(gravity),
  linearDensity(linearDensity),
  K(K),
  ribbonLength(ribbonLength),
  computationStatus({false,false,false})
{
  elasticEnergy.FixPhysics(K, ribbonLength);
}

CSegmentSystem::CSegmentSystem(const Eigen::Vector3r& gravity, const real& linearDensity, const double& YoungModulus, const double& poissonRatio, const double& width, const double& thickness, const real& ribbonLength) :
  G(gravity),
  linearDensity(linearDensity),
  K(CElasticEnergy::ParamsToK(YoungModulus,poissonRatio,width,thickness)),
  ribbonLength(ribbonLength),
  computationStatus({false,false,false})
{
  elasticEnergy.FixPhysics(YoungModulus, poissonRatio, width, thickness, ribbonLength);
}

void CSegmentSystem::setPoint(const Eigen::Vector3r& pos, const Eigen::Matrix3r& frame, const LinearScalar& omega1, const LinearScalar& omega2, const LinearScalar& omega3)
{
  this->pos=pos;
  this->frame=frame;
  this->omega1=omega1;
  this->omega2=omega2;
  this->omega3=omega3;
  elasticEnergy.FixPt(omega1, omega2, omega3);
  computationStatus[0]=computationStatus[1]=computationStatus[2]=false;
  parts.clear();

}

void CSegmentSystem::setPoint(const Eigen::Vector3r& pos, const Eigen::Matrix3r& frame, const LinearScalar& omega1, const LinearScalar& omega2, const LinearScalar& omega3, const LinearScalar& naturalOmega1, const LinearScalar& naturalOmega2, const LinearScalar& naturalOmega3)
{
  this->pos=pos;
  this->frame=frame;
  this->omega1=omega1;
  this->omega2=omega2;
  this->omega3=omega3;
  elasticEnergy.FixPt(omega1, omega2, omega3, naturalOmega1, naturalOmega2, naturalOmega3);
  computationStatus[0]=computationStatus[1]=computationStatus[2]=false;
  parts.clear();
}

void CSegmentSystem::energyPreCalc()
{
  Eigen::Vector3r eg(0., 0., 0.);
  LinearScalar w1 = omega1;
  LinearScalar w2 = omega2;
  LinearScalar w3 = omega3;
  Eigen::Vector3r p=pos;
  Eigen::Matrix3r f=frame;
  real sStart=0.;
  do{
    parts.push_back(SegmentPart());
    SegmentPart& part=parts.back();
    part.s0=sStart;
    part.serie.Place(p,f);
    part.serie.Setup(w1,w2,w3);
    part.lenght=part.serie.estimSMax();
    if(part.lenght+sStart>ribbonLength)
    {
      part.lenght=ribbonLength-sStart;
      std::tie(endPos, endFrame)=part.serie.PosFrame(part.lenght);
      eg+=part.serie.preAltitudeEnergy(part.lenght);
      break;
    }
    sStart+=part.lenght;
    w1=LinearScalar(omega1.A,omega1.B+sStart*omega1.A);
    w2=LinearScalar(omega2.A,omega2.B+sStart*omega2.A);
    w3=LinearScalar(omega3.A,omega3.B+sStart*omega3.A);
    std::tie(p, f)=part.serie.PosFrame(part.lenght);
    eg+=part.serie.preAltitudeEnergy(part.lenght);
  }
  while(true);
  energyValue = -linearDensity* eg.dot(G) + elasticEnergy.computeEnergy();
  computationStatus[0]=true;
}

void CSegmentSystem::gradientPreCalc()
{
  if(!computationStatus[0])energyPreCalc();
  Eigen::Matrix<Eigen::Vector6r, 3, 3> gradFrameOut;
  Eigen::Matrix<Eigen::Vector6r, 3, 1> gradPosOut;
  
  gradPreAltitudeEnergyOut.setConstant(Eigen::Vector6r::Zero());
  gradFrameOut.setConstant(Eigen::Vector6r::Zero());
  gradPosOut.setConstant(Eigen::Vector6r::Zero());
  
  Eigen::Matrix3r preframeF = Eigen::Matrix3r::Identity();
  Eigen::Vector3r preTransl = Eigen::Vector3r(0.,0.,0.);
  Eigen::Vector3r preAltitudeRotationalEnergy = Eigen::Vector3r(0.,0.,0.);
  for(SegmentPart& part:parts)
  {
    Eigen::Matrix<real, 3, 6> gradPreAltitudeEnergySegment = part.serie.gradPreAltitudeEnergy(part.lenght);
    Eigen::Matrix<Eigen::Vector3r, 3, 1> dPreAltitudeEnergydPosSegment = part.serie.dPreAltitudeEnergydPos(part.lenght);
    Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPreAltitudeEnergydFrameSegment = part.serie.dPreAltitudeEnergydFrame(part.lenght);
    Eigen::Matrix<Eigen::Matrix3r, 3, 3> dFramedFrameSegment = part.serie.dFramedFrame(part.lenght);
    Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPosdFrameSegment = part.serie.dPosdFrame(part.lenght);
    Eigen::Matrix<Eigen::Vector3r, 3, 1> dPosdPosSegment = part.serie.dPosdPos(part.lenght);

    Eigen::Matrix<Eigen::Matrix3r, 6 ,1> dFdvSegment = part.serie.dFramedvars(part.lenght);
    Eigen::Matrix<Eigen::Vector3r, 6 ,1> dPdvSegment = part.serie.dPosdvars(part.lenght);
    for(int i=0;i<3;i++)
    {
      dFdvSegment[2*i]+=part.s0 * dFdvSegment[2*i+1];
      dPdvSegment[2*i]+=part.s0 * dPdvSegment[2*i+1];
    }
    for(int i=0;i<3;i++)
      gradPreAltitudeEnergySegment.col(2*i) += part.s0 * gradPreAltitudeEnergySegment.col(2*i+1);

    for (int k = 0; k < 3; k++) gradPreAltitudeEnergyOut[k] += gradPreAltitudeEnergySegment.row(k);
    gradPreAltitudeEnergyOut += compDeriv(dPreAltitudeEnergydFrameSegment, gradFrameOut) + compDeriv(dPreAltitudeEnergydPosSegment, gradPosOut);

    gradPosOut = compDeriv(dPosdPosSegment, gradPosOut) + compDeriv(dPosdFrameSegment, gradFrameOut);
    gradFrameOut = compDeriv(dFramedFrameSegment, gradFrameOut);
    for (int a = 0; a < 3; a++) {
      for (int b = 0; b < 3; b++) {
        for(int v=0; v<6;v++)
        gradFrameOut(a, b)[v] += dFdvSegment(v)(a, b);
      }
      for(int v=0; v<6;v++)
        gradPosOut(a)[v] += dPdvSegment(v)(a);
    }
    preAltitudeRotationalEnergy+=part.lenght*preTransl+ preframeF*part.serie.preAltitudeRotationalEnergy(part.lenght);
    preTransl+=preframeF*part.serie.PreTranslation(part.lenght);
    preframeF *= part.serie.PreFrame(part.lenght);
    part.gradFrameOut=gradFrameOut;
    part.gradPosOut=gradPosOut;
  }
  
  Eigen::Matrix<Eigen::Vector3r, 3, 1> dPreAltitudeEnergydPosOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPreAltitudeEnergydFrameOut;
  dPreAltitudeEnergydPosOut = Eigen::Matrix<Eigen::Vector3r, 3, 1>(Eigen::Vector3r(ribbonLength, 0., 0.), Eigen::Vector3r(0., ribbonLength, 0.), Eigen::Vector3r(0., 0., ribbonLength));
  dPreAltitudeEnergydFrameOut = getdPreAltdFrame(preAltitudeRotationalEnergy);
  dFramedFrame = getdFramedFrame(preframeF);
  dPosdFrame = getdPosdFrame(preTransl);
  dEnergydFrameOut = -linearDensity* contractGeneralDotP(dPreAltitudeEnergydFrameOut,G);
  dEnergydPosOut = -linearDensity* contractGeneralDotP(dPreAltitudeEnergydPosOut,G);
  gradientValue = -linearDensity*contractGeneralDotP(gradPreAltitudeEnergyOut,G)+elasticEnergy.computeGrad();
  computationStatus[1]=true;
}

Eigen::Matrix<Eigen::Matrix3r, 3, 1> CSegmentSystem::getdPreAltdFrame(const Eigen::Vector3r& preAltitudeRotationalEnergy) const
{
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> out;

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++) {
      Eigen::Matrix3r tM;
      Eigen::Vector3r tMv;
      tM.setZero();
      tM(i, j) = 1.;
      tMv = tM * preAltitudeRotationalEnergy;
      for (int k = 0; k < 3; k++)
        out[k](i, j) = tMv(k);
    }
  return out;
}

Eigen::Matrix<Eigen::Matrix3r, 3, 1> CSegmentSystem::getdPosdFrame(const Eigen::Vector3r& preTrans) const
{
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> out;

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++) {
      Eigen::Matrix3r tM;
      Eigen::Vector3r tMv;
      tM.setZero();
      tM(i, j) = 1.;
      tMv = tM * preTrans;
      for (int k = 0; k < 3; k++)
        out[k](i, j) = tMv(k);
    }
  return out;
}

Eigen::Matrix<Eigen::Matrix3r, 3, 3> CSegmentSystem::getdFramedFrame(const Eigen::Matrix3r& preFrame) const
{
  Eigen::Matrix<Eigen::Matrix3r, 3, 3> out;

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++) {
      Eigen::Matrix3r tM;
      tM.setZero();
      tM(i, j) = 1.;
      tM = tM * preFrame;
      for (int k = 0; k < 3; k++)
        for (int l = 0; l < 3; l++)
          out(k, l)(i, j) = tM(k, l);
    }
  return out;
}


void CSegmentSystem::hessianPreCalc()
{
  if(!computationStatus[1])gradientPreCalc();
  Eigen::Matrix<Eigen::Vector6r, 3, 3> gradFrameOutPrev;
  Eigen::Matrix<Eigen::Vector6r, 3, 1> gradPosOutPrev;
  
  gradFrameOutPrev.setConstant(Eigen::Vector6r::Zero());
  gradPosOutPrev.setConstant(Eigen::Vector6r::Zero());
  
  hessianPreAltitudeEnergyOut.setConstant(Eigen::Matrix6r::Zero());
  hessianPosOut.setConstant(Eigen::Matrix6r::Zero());
  hessianFrameOut.setConstant(Eigen::Matrix6r::Zero());
  
  for(auto& part:parts)
  {
    Eigen::Matrix<Eigen::Matrix6r, 3, 1> hessianPreAltitudeEnergy;
    Eigen::Matrix<Eigen::Matrix6r, 3, 1> hessianPos;
    Eigen::Matrix<Eigen::Matrix6r, 3, 3> hessianFrame;

    Eigen::Matrix<Eigen::Matrix<Eigen::Vector6r, 3, 3>, 3, 1> d2PosdFramedvalues = part.serie.d2PosdFramedvalues(part.lenght);
    Eigen::Matrix<Eigen::Matrix<Eigen::Vector6r, 3, 3>, 3, 3> d2FramedFramedvalues = part.serie.d2FramedFramedvalues(part.lenght);
    Eigen::Matrix<Eigen::Matrix<Eigen::Vector6r, 3, 3>, 3, 1> d2PreAltitudeEnergydFramedvalues = part.serie.d2PreAltitudeEnergydFramedvalues(part.lenght);
    Eigen::Matrix<Eigen::Matrix6r, 3, 1> hessianPosSegment = part.serie.hessianPos(part.lenght);
    Eigen::Matrix<Eigen::Matrix6r, 3, 3> hessianFrameSegment = part.serie.hessianFrame(part.lenght);
    Eigen::Matrix<Eigen::Matrix6r, 3, 1> hessianPreAltitudeEnergySegment = part.serie.hessianPreAltitudeEnergy(part.lenght);
    Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPosdFrameSegment = part.serie.dPosdFrame(part.lenght);
    Eigen::Matrix<Eigen::Vector3r, 3, 1> dPosdPosSegment = part.serie.dPosdPos(part.lenght);
    Eigen::Matrix<Eigen::Vector3r, 3, 1> dPreAltitudeEnergydPosSegment = part.serie.dPreAltitudeEnergydPos(part.lenght);
    Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPreAltitudeEnergydFrameSegment = part.serie.dPreAltitudeEnergydFrame(part.lenght);
    Eigen::Matrix<Eigen::Matrix3r, 3, 3> dFramedFrameSegment = part.serie.dFramedFrame(part.lenght);

    for (int i = 0; i < 3; i++)hessianPos(i) = addTransposeToItself(compDeriv2(d2PosdFramedvalues(i), gradFrameOutPrev));
    for (int i = 0; i < 3; i++)hessianPos(i) += contractGeneralDotP(hessianFrameOut, dPosdFrameSegment(i));
    for (int i = 0; i < 3; i++)hessianPos(i) += contractGeneralDotP(hessianPosOut, dPosdPosSegment(i));
    for (int i = 0; i < 3; i++)hessianPos(i) += hessianPosSegment(i);
    //aa
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 0) += 2. * part.s0 * hessianPosSegment(i)(0, 1);
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 0) += 2. * part.s0 * contractGeneralDotP(extractDerivation(d2PosdFramedvalues(i), 1), extractDerivation(gradFrameOutPrev, 0));
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 0) += part.s0 * part.s0 * hessianPosSegment(i)(1, 1);
    //ab
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 1) += part.s0 * hessianPosSegment(i)(1, 1);
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 1) += part.s0 * contractGeneralDotP(extractDerivation(d2PosdFramedvalues(i), 1), extractDerivation(gradFrameOutPrev, 1));
    //an
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 2) += part.s0 * (hessianPosSegment(i)(1, 2) + hessianPosSegment(i)(0, 3));
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 2) += part.s0 * (contractGeneralDotP(extractDerivation(d2PosdFramedvalues(i), 1), extractDerivation(gradFrameOutPrev, 2))
          + contractGeneralDotP(extractDerivation(d2PosdFramedvalues(i), 3), extractDerivation(gradFrameOutPrev, 0)));
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 2) += part.s0 * part.s0 * hessianPosSegment(i)(1, 3);
    //am
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 3) += part.s0 * hessianPosSegment(i)(1, 3);
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 3) += part.s0 * contractGeneralDotP(extractDerivation(d2PosdFramedvalues(i), 1), extractDerivation(gradFrameOutPrev, 3));
    //ay
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 4) += part.s0 * (hessianPosSegment(i)(1, 4) + hessianPosSegment(i)(0, 5));
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 4) += part.s0 * (contractGeneralDotP(extractDerivation(d2PosdFramedvalues(i), 1), extractDerivation(gradFrameOutPrev, 4))
          + contractGeneralDotP(extractDerivation(d2PosdFramedvalues(i), 5), extractDerivation(gradFrameOutPrev, 0)));
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 4) += part.s0 * part.s0 * hessianPosSegment(i)(1, 5);
    //az
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 5) += part.s0 * hessianPosSegment(i)(1, 5);
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 5) += part.s0 * contractGeneralDotP(extractDerivation(d2PosdFramedvalues(i), 1), extractDerivation(gradFrameOutPrev, 5));
    //nb
    for (int i = 0; i < 3; i++)hessianPos(i)(2, 1) += part.s0 * hessianPosSegment(i)(3, 1);
    for (int i = 0; i < 3; i++)hessianPos(i)(2, 1) += part.s0 * contractGeneralDotP(extractDerivation(d2PosdFramedvalues(i), 3), extractDerivation(gradFrameOutPrev, 1));
    //nn
    for (int i = 0; i < 3; i++)hessianPos(i)(2, 2) += 2. * part.s0 * hessianPosSegment(i)(2, 3);
    for (int i = 0; i < 3; i++)hessianPos(i)(2, 2) += 2. * part.s0 * contractGeneralDotP(extractDerivation(d2PosdFramedvalues(i), 3), extractDerivation(gradFrameOutPrev, 2));
    for (int i = 0; i < 3; i++)hessianPos(i)(2, 2) += part.s0 * part.s0 * hessianPosSegment(i)(3, 3);
    //nm
    for (int i = 0; i < 3; i++)hessianPos(i)(2, 3) += part.s0 * hessianPosSegment(i)(3, 3);
    for (int i = 0; i < 3; i++)hessianPos(i)(2, 3) += part.s0 * contractGeneralDotP(extractDerivation(d2PosdFramedvalues(i), 3), extractDerivation(gradFrameOutPrev, 3));
    //ny
    for (int i = 0; i < 3; i++)hessianPos(i)(2, 4) += part.s0 * (hessianPosSegment(i)(3, 4) + hessianPosSegment(i)(2, 5));
    for (int i = 0; i < 3; i++)hessianPos(i)(2, 4) += part.s0 * (contractGeneralDotP(extractDerivation(d2PosdFramedvalues(i), 3), extractDerivation(gradFrameOutPrev, 4))
          + contractGeneralDotP(extractDerivation(d2PosdFramedvalues(i), 5), extractDerivation(gradFrameOutPrev, 2)));
    for (int i = 0; i < 3; i++)hessianPos(i)(2, 4) += part.s0 * part.s0 * hessianPosSegment(i)(3, 5);
    //nz
    for (int i = 0; i < 3; i++)hessianPos(i)(2, 5) += part.s0 * hessianPosSegment(i)(3, 5);
    for (int i = 0; i < 3; i++)hessianPos(i)(2, 5) += part.s0 * contractGeneralDotP(extractDerivation(d2PosdFramedvalues(i), 3), extractDerivation(gradFrameOutPrev, 5));
    //yb
    for (int i = 0; i < 3; i++)hessianPos(i)(4, 1) += part.s0 * hessianPosSegment(i)(5, 1);
    for (int i = 0; i < 3; i++)hessianPos(i)(4, 1) += part.s0 * contractGeneralDotP(extractDerivation(d2PosdFramedvalues(i), 5), extractDerivation(gradFrameOutPrev, 1));
    //ym
    for (int i = 0; i < 3; i++)hessianPos(i)(4, 3) += part.s0 * hessianPosSegment(i)(5, 3);
    for (int i = 0; i < 3; i++)hessianPos(i)(4, 3) += part.s0 * contractGeneralDotP(extractDerivation(d2PosdFramedvalues(i), 5), extractDerivation(gradFrameOutPrev, 3));
    //yy
    for (int i = 0; i < 3; i++)hessianPos(i)(4, 4) += 2. * part.s0 * hessianPosSegment(i)(4, 5);
    for (int i = 0; i < 3; i++)hessianPos(i)(4, 4) += 2. * part.s0 * contractGeneralDotP(extractDerivation(d2PosdFramedvalues(i), 5), extractDerivation(gradFrameOutPrev, 4));
    for (int i = 0; i < 3; i++)hessianPos(i)(4, 4) += part.s0 * part.s0 * hessianPosSegment(i)(5, 5);
    //yz
    for (int i = 0; i < 3; i++)hessianPos(i)(4, 5) += part.s0 * hessianPosSegment(i)(5, 5);
    for (int i = 0; i < 3; i++)hessianPos(i)(4, 5) += part.s0 * contractGeneralDotP(extractDerivation(d2PosdFramedvalues(i), 5), extractDerivation(gradFrameOutPrev, 5));
    
    //ba
    for (int i = 0; i < 3; i++)hessianPos(i)(1, 0) = hessianPos(i)(0, 1);
    //na
    for (int i = 0; i < 3; i++)hessianPos(i)(2, 0) = hessianPos(i)(0, 2);
    //ma
    for (int i = 0; i < 3; i++)hessianPos(i)(3, 0) = hessianPos(i)(0, 3);
    //ya
    for (int i = 0; i < 3; i++)hessianPos(i)(4, 0) = hessianPos(i)(0, 4);
    //za
    for (int i = 0; i < 3; i++)hessianPos(i)(5, 0) = hessianPos(i)(0, 5);
    //bn
    for (int i = 0; i < 3; i++)hessianPos(i)(1, 2) = hessianPos(i)(2, 1);
    //mn
    for (int i = 0; i < 3; i++)hessianPos(i)(3, 2) = hessianPos(i)(2, 3);
    //yn
    for (int i = 0; i < 3; i++)hessianPos(i)(4, 2) = hessianPos(i)(2, 4);
    //zn
    for (int i = 0; i < 3; i++)hessianPos(i)(5, 2) = hessianPos(i)(2, 5);
    //by
    for (int i = 0; i < 3; i++)hessianPos(i)(1, 4) = hessianPos(i)(4, 1);
    //my
    for (int i = 0; i < 3; i++)hessianPos(i)(3, 4) = hessianPos(i)(4, 3);
    //yz
    for (int i = 0; i < 3; i++)hessianPos(i)(5, 4) = hessianPos(i)(4, 5);


    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j) = addTransposeToItself(compDeriv2(d2FramedFramedvalues(i, j), gradFrameOutPrev));
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j) += contractGeneralDotP(hessianFrameOut, dFramedFrameSegment(i, j));
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j) += hessianFrameSegment(i, j);
    //aa
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 0) += 2 * part.s0 * hessianFrameSegment(i, j)(0, 1);
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 0) += 2 * part.s0 * contractGeneralDotP(extractDerivation(d2FramedFramedvalues(i, j), 1), extractDerivation(gradFrameOutPrev, 0));
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 0) += part.s0 * part.s0 * hessianFrameSegment(i, j)(1, 1);
    //ab
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 1) += part.s0 * hessianFrameSegment(i, j)(1, 1);
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 1) += part.s0 * contractGeneralDotP(extractDerivation(d2FramedFramedvalues(i, j), 1), extractDerivation(gradFrameOutPrev, 1));
    //an
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 2) += part.s0 * (hessianFrameSegment(i, j)(1, 2) + hessianFrameSegment(i, j)(0, 3));
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 2) += part.s0 * (contractGeneralDotP(extractDerivation(d2FramedFramedvalues(i, j), 1), extractDerivation(gradFrameOutPrev, 2))
            + contractGeneralDotP(extractDerivation(d2FramedFramedvalues(i, j), 3), extractDerivation(gradFrameOutPrev, 0))
                                                                                               );
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 2) += part.s0 * part.s0 * hessianFrameSegment(i, j)(1, 3);
    //am
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 3) += part.s0 * hessianFrameSegment(i, j)(1, 3);
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 3) += part.s0 * contractGeneralDotP(extractDerivation(d2FramedFramedvalues(i, j), 1), extractDerivation(gradFrameOutPrev, 3));
    //ay
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 4) += part.s0 * (hessianFrameSegment(i, j)(1, 4) + hessianFrameSegment(i, j)(0, 5));
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 4) += part.s0 * (contractGeneralDotP(extractDerivation(d2FramedFramedvalues(i, j), 1), extractDerivation(gradFrameOutPrev, 4))
            + contractGeneralDotP(extractDerivation(d2FramedFramedvalues(i, j), 5), extractDerivation(gradFrameOutPrev, 0))
                                                                                               );
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 4) += part.s0 * part.s0 * hessianFrameSegment(i, j)(1, 5);
    //az
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 5) += part.s0 * hessianFrameSegment(i, j)(1, 5);
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 5) += part.s0 * contractGeneralDotP(extractDerivation(d2FramedFramedvalues(i, j), 1), extractDerivation(gradFrameOutPrev, 5));
    //nb
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(2, 1) += part.s0 * hessianFrameSegment(i, j)(3, 1);
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(2, 1) += part.s0 * contractGeneralDotP(extractDerivation(d2FramedFramedvalues(i, j), 3), extractDerivation(gradFrameOutPrev, 1));
    //nn
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(2, 2) += 2 * part.s0 * hessianFrameSegment(i, j)(2, 3);
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(2, 2) += 2 * part.s0 * contractGeneralDotP(extractDerivation(d2FramedFramedvalues(i, j), 3), extractDerivation(gradFrameOutPrev, 2));
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(2, 2) += part.s0 * part.s0 * hessianFrameSegment(i, j)(3, 3);
    //nm
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(2, 3) += part.s0 * hessianFrameSegment(i, j)(3, 3);
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(2, 3) += part.s0 * contractGeneralDotP(extractDerivation(d2FramedFramedvalues(i, j), 3), extractDerivation(gradFrameOutPrev, 3));
    //ny
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(2, 4) += part.s0 * (hessianFrameSegment(i, j)(3, 4) + hessianFrameSegment(i, j)(2, 5));
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(2, 4) += part.s0 * (contractGeneralDotP(extractDerivation(d2FramedFramedvalues(i, j), 3), extractDerivation(gradFrameOutPrev, 4))
            + contractGeneralDotP(extractDerivation(d2FramedFramedvalues(i, j), 5), extractDerivation(gradFrameOutPrev, 2))
                                                                                               );
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(2, 4) += part.s0 * part.s0 * hessianFrameSegment(i, j)(3, 5);
    //nz
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(2, 5) += part.s0 * hessianFrameSegment(i, j)(3, 5);
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(2, 5) += part.s0 * contractGeneralDotP(extractDerivation(d2FramedFramedvalues(i, j), 3), extractDerivation(gradFrameOutPrev, 5));
    
    //yb
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(4, 1) += part.s0 * hessianFrameSegment(i, j)(5, 1);
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(4, 1) += part.s0 * contractGeneralDotP(extractDerivation(d2FramedFramedvalues(i, j), 5), extractDerivation(gradFrameOutPrev, 1));
    //ym
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(4, 3) += part.s0 * hessianFrameSegment(i, j)(5, 3);
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(4, 3) += part.s0 * contractGeneralDotP(extractDerivation(d2FramedFramedvalues(i, j), 5), extractDerivation(gradFrameOutPrev, 3));
    //yy
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(4, 4) += 2 * part.s0 * hessianFrameSegment(i, j)(4, 5);
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(4, 4) += 2 * part.s0 * contractGeneralDotP(extractDerivation(d2FramedFramedvalues(i, j), 5), extractDerivation(gradFrameOutPrev, 4));
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(4, 4) += part.s0 * part.s0 * hessianFrameSegment(i, j)(5, 5);
    //yz
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(4, 5) += part.s0 * hessianFrameSegment(i, j)(5, 5);
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(4, 5) += part.s0 * contractGeneralDotP(extractDerivation(d2FramedFramedvalues(i, j), 5), extractDerivation(gradFrameOutPrev, 5));
    
    //ba
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(1, 0) = hessianFrame(i, j)(0, 1);
    //na
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(2, 0) = hessianFrame(i, j)(0, 2);
    //ma
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(3, 0) = hessianFrame(i, j)(0, 3);
    //ya
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(4, 0) = hessianFrame(i, j)(0, 4);
    //za
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(5, 0) = hessianFrame(i, j)(0, 5);
    //bn
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(1, 2) = hessianFrame(i, j)(2, 1);
    //mn
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(3, 2) = hessianFrame(i, j)(2, 3);
    //yn
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(4, 2) = hessianFrame(i, j)(2, 4);
    //zn
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(5, 2) = hessianFrame(i, j)(2, 5);
    //by
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(1, 4) = hessianFrame(i, j)(4, 1);
    //my
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(3, 4) = hessianFrame(i, j)(4, 3);
    //yz
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(5, 4) = hessianFrame(i, j)(4, 5);


    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i) = addTransposeToItself(compDeriv2(d2PreAltitudeEnergydFramedvalues(i), gradFrameOutPrev));
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i) += contractGeneralDotP(hessianFrameOut, dPreAltitudeEnergydFrameSegment(i));
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i) += contractGeneralDotP(hessianPosOut, dPreAltitudeEnergydPosSegment(i));
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i) += hessianPreAltitudeEnergySegment(i);
    //aa
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 0) += 2. * part.s0 * hessianPreAltitudeEnergySegment(i)(0, 1);
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 0) += 2. * part.s0 * contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedvalues(i), 1), extractDerivation(gradFrameOutPrev, 0));
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 0) += part.s0 * part.s0 * hessianPreAltitudeEnergySegment(i)(1, 1);
    //ab
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 1) += part.s0 * hessianPreAltitudeEnergySegment(i)(1, 1);
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 1) += part.s0 * contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedvalues(i), 1), extractDerivation(gradFrameOutPrev, 1));
    //an
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 2) += part.s0 * (hessianPreAltitudeEnergySegment(i)(1, 2) + hessianPreAltitudeEnergySegment(i)(0, 3));
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 2) += part.s0 * (contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedvalues(i), 1), extractDerivation(gradFrameOutPrev, 2))
          + contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedvalues(i), 3), extractDerivation(gradFrameOutPrev, 0)));
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 2) += part.s0 * part.s0 * hessianPreAltitudeEnergySegment(i)(1, 3);
    //am
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 3) += part.s0 * hessianPreAltitudeEnergySegment(i)(1, 3);
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 3) += part.s0 * contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedvalues(i), 1), extractDerivation(gradFrameOutPrev, 3));
    //ay
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 4) += part.s0 * (hessianPreAltitudeEnergySegment(i)(1, 4) + hessianPreAltitudeEnergySegment(i)(0, 5));
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 4) += part.s0 * (contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedvalues(i), 1), extractDerivation(gradFrameOutPrev, 4))
          + contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedvalues(i), 5), extractDerivation(gradFrameOutPrev, 0)));
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 4) += part.s0 * part.s0 * hessianPreAltitudeEnergySegment(i)(1, 5);
    //az
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 5) += part.s0 * hessianPreAltitudeEnergySegment(i)(1, 5);
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 5) += part.s0 * contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedvalues(i), 1), extractDerivation(gradFrameOutPrev, 5));
    //nb
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(2, 1) += part.s0 * hessianPreAltitudeEnergySegment(i)(3, 1);
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(2, 1) += part.s0 * contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedvalues(i), 3), extractDerivation(gradFrameOutPrev, 1));
    //nn
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(2, 2) += 2. * part.s0 * hessianPreAltitudeEnergySegment(i)(2, 3);
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(2, 2) += 2. * part.s0 * contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedvalues(i), 3), extractDerivation(gradFrameOutPrev, 2));
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(2, 2) += part.s0 * part.s0 * hessianPreAltitudeEnergySegment(i)(3, 3);
    //nm
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(2, 3) += part.s0 * hessianPreAltitudeEnergySegment(i)(3, 3);
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(2, 3) += part.s0 * contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedvalues(i), 3), extractDerivation(gradFrameOutPrev, 3));
    //ny
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(2, 4) += part.s0 * (hessianPreAltitudeEnergySegment(i)(3, 4) + hessianPreAltitudeEnergySegment(i)(2, 5));
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(2, 4) += part.s0 * (contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedvalues(i), 3), extractDerivation(gradFrameOutPrev, 4))
          + contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedvalues(i), 5), extractDerivation(gradFrameOutPrev, 2)));
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(2, 4) += part.s0 * part.s0 * hessianPreAltitudeEnergySegment(i)(3, 5);
    //nz
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(2, 5) += part.s0 * hessianPreAltitudeEnergySegment(i)(3, 5);
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(2, 5) += part.s0 * contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedvalues(i), 3), extractDerivation(gradFrameOutPrev, 5));
    //yb
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(4, 1) += part.s0 * hessianPreAltitudeEnergySegment(i)(5, 1);
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(4, 1) += part.s0 * contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedvalues(i), 5), extractDerivation(gradFrameOutPrev, 1));
    //ym
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(4, 3) += part.s0 * hessianPreAltitudeEnergySegment(i)(5, 3);
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(4, 3) += part.s0 * contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedvalues(i), 5), extractDerivation(gradFrameOutPrev, 3));
    //yy
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(4, 4) += 2. * part.s0 * hessianPreAltitudeEnergySegment(i)(4, 5);
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(4, 4) += 2. * part.s0 * contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedvalues(i), 5), extractDerivation(gradFrameOutPrev, 4));
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(4, 4) += part.s0 * part.s0 * hessianPreAltitudeEnergySegment(i)(5, 5);
    //yz
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(4, 5) += part.s0 * hessianPreAltitudeEnergySegment(i)(5, 5);
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(4, 5) += part.s0 * contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedvalues(i), 5), extractDerivation(gradFrameOutPrev, 5));
    
    //ba
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(1, 0) = hessianPreAltitudeEnergy(i)(0, 1);
    //na
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(2, 0) = hessianPreAltitudeEnergy(i)(0, 2);
    //ma
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(3, 0) = hessianPreAltitudeEnergy(i)(0, 3);
    //ya
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(4, 0) = hessianPreAltitudeEnergy(i)(0, 4);
    //za
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(5, 0) = hessianPreAltitudeEnergy(i)(0, 5);
    //bn
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(1, 2) = hessianPreAltitudeEnergy(i)(2, 1);
    //mn
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(3, 2) = hessianPreAltitudeEnergy(i)(2, 3);
    //yn
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(4, 2) = hessianPreAltitudeEnergy(i)(2, 4);
    //zn
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(5, 2) = hessianPreAltitudeEnergy(i)(2, 5);
    //by
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(1, 4) = hessianPreAltitudeEnergy(i)(4, 1);
    //my
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(3, 4) = hessianPreAltitudeEnergy(i)(4, 3);
    //yz
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(5, 4) = hessianPreAltitudeEnergy(i)(4, 5);


    hessianPreAltitudeEnergyOut += hessianPreAltitudeEnergy;
    hessianFrameOut = hessianFrame;
    hessianPosOut = hessianPos;
    
    
    gradPosOutPrev=part.gradPosOut;
    gradFrameOutPrev=part.gradFrameOut;
  }
  computationStatus[2]=true;
}

std::pair<Eigen::Vector3r, Eigen::Matrix3r> CSegmentSystem::endPosFrame()
{
  if(!computationStatus[0])energyPreCalc();
  return std::pair<Eigen::Vector3r, Eigen::Matrix3r>(endPos,endFrame);
}

real CSegmentSystem::energy()
{
  if(!computationStatus[0])energyPreCalc();
  return energyValue;
}

Eigen::Vector6r CSegmentSystem::gradient()
{
  if(!computationStatus[1])gradientPreCalc();
  return gradientValue;
}

Eigen::Matrix<Eigen::Vector6r, 3, 1> CSegmentSystem::gradPos()
{
  if(!computationStatus[1])gradientPreCalc();
  return parts.back().gradPosOut;
}

Eigen::Matrix<Eigen::Vector6r, 3, 3> CSegmentSystem::gradFrame()
{
  if(!computationStatus[1])gradientPreCalc();
  return parts.back().gradFrameOut;
}

Eigen::Matrix<Eigen::Matrix3r, 3, 1> CSegmentSystem::getdPosdFrame()
{
  if(!computationStatus[1])gradientPreCalc();
  return dPosdFrame;
}

Eigen::Matrix<Eigen::Matrix3r, 3, 3> CSegmentSystem::getdFramedFrame()
{
  if(!computationStatus[1])gradientPreCalc();
  return dFramedFrame;
}

Eigen::Matrix3r CSegmentSystem::getdEnergydFrameOut()
{
  if(!computationStatus[1])gradientPreCalc();
  return dEnergydFrameOut;
}

Eigen::Vector3r CSegmentSystem::getdEnergydPosOut()
{
  if(!computationStatus[1])gradientPreCalc();
  return dEnergydPosOut;
}

Eigen::Matrix6r CSegmentSystem::hessian()
{
  if(!computationStatus[2])hessianPreCalc();
  return -linearDensity*contractGeneralDotP(hessianPreAltitudeEnergyOut,G)+elasticEnergy.computeHessian();
}

Eigen::Matrix<Eigen::Matrix6r, 3, 1> CSegmentSystem::gethessianPosOut()
{
  if(!computationStatus[2])hessianPreCalc();
  return hessianPosOut;
}

Eigen::Matrix<Eigen::Matrix6r, 3, 3> CSegmentSystem::gethessianFrameOut()
{
  if(!computationStatus[2])hessianPreCalc();
  return hessianFrameOut;
}

std::pair<Eigen::Vector3r, Eigen::Matrix3r> CSegmentSystem::orPosFrame()
{
  return std::pair<Eigen::Vector3r, Eigen::Matrix3r>(pos,frame);
}

real CSegmentSystem::getd2EdFramedktimesdFramedz(Eigen::Matrix3r dFramedz, int k)
{
  return -linearDensity*(dFramedz *frame.inverse()*extractDerivation( gradPreAltitudeEnergyOut,k)).dot(G);
}
