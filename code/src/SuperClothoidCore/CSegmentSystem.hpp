/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef CSEGMENTSYSTEM_H
#define CSEGMENTSYSTEM_H

#include <Eigen/Dense>
#include <array>
#include <BaseCore/LinearScalar.hpp>
#include "SuperClothoidGeometrieSerie.hpp"
#include "CElasticEnergy.hpp"

class CSegmentSystem
{
public:
  CSegmentSystem(const Eigen::Vector3r& gravity, const real& linearDensity, const double& YoungModulus, const double& poissonRatio, const double& width, const double& thickness, const real& ribbonLength);
  CSegmentSystem(const Eigen::Vector3r& gravity, const real& linearDensity, const Eigen::Matrix3r& K, const real& ribbonLength);
  void setPoint(const Eigen::Vector3r& pos, const Eigen::Matrix3r& frame, const LinearScalar& omega1, const LinearScalar& omega2, const LinearScalar& omega3);
  void setPoint(const Eigen::Vector3r& pos, const Eigen::Matrix3r& frame, const LinearScalar& omega1, const LinearScalar& omega2, const LinearScalar& omega3, const LinearScalar& naturalOmega1, const LinearScalar& naturalOmega2, const LinearScalar& naturalOmega3);
  std::pair<Eigen::Vector3r,Eigen::Matrix3r> endPosFrame();
  std::pair<Eigen::Vector3r,Eigen::Matrix3r> orPosFrame();
  real energy();
  Eigen::Vector6r gradient();
  Eigen::Matrix6r hessian();
  Eigen::Matrix<Eigen::Vector6r, 3, 1> gradPos();
  Eigen::Matrix<Eigen::Vector6r, 3, 3> gradFrame();
  Eigen::Matrix<Eigen::Matrix3r, 3, 3> getdFramedFrame();
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> getdPosdFrame();
  Eigen::Vector3r getdEnergydPosOut();
  Eigen::Matrix3r getdEnergydFrameOut();
  Eigen::Matrix<Eigen::Matrix6r, 3, 1> gethessianPosOut();
  Eigen::Matrix<Eigen::Matrix6r, 3, 3> gethessianFrameOut();
  real getd2EdFramedktimesdFramedz(Eigen::Matrix3r dFramedz,int k);
private:
  void energyPreCalc();
  void gradientPreCalc();
  void hessianPreCalc();
  Eigen::Vector3r pos;
  Eigen::Matrix3r frame; 
  LinearScalar omega1;
  LinearScalar omega2;
  LinearScalar omega3;
  struct SegmentPart{
    SuperClothoidGeometrieSerie serie;
    real lenght;
    real s0;
    //first order
    Eigen::Matrix<Eigen::Vector6r, 3, 3> gradFrameOut;
    Eigen::Matrix<Eigen::Vector6r, 3, 1> gradPosOut;
  };
  std::vector<SegmentPart> parts;
  CElasticEnergy elasticEnergy;
  Eigen::Vector3r G;
  real linearDensity; 
  Eigen::Matrix3r K;
  real ribbonLength;
  
  std::array<bool,3> computationStatus;
  
  Eigen::Vector3r endPos;
  Eigen::Matrix3r endFrame;
  real energyValue;
  
  Eigen::Matrix<Eigen::Vector6r, 3, 1> gradPreAltitudeEnergyOut;
  Eigen::Vector3r dEnergydPosOut;
  Eigen::Matrix3r dEnergydFrameOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 3> dFramedFrame;
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPosdFrame;
  Eigen::Vector6r gradientValue;
  
  Eigen::Matrix<Eigen::Matrix6r, 3, 1> hessianPreAltitudeEnergyOut;
  Eigen::Matrix<Eigen::Matrix6r, 3, 1> hessianPosOut;
  Eigen::Matrix<Eigen::Matrix6r, 3, 3> hessianFrameOut;
  
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> getdPreAltdFrame(const Eigen::Vector3r& preAltitudeRotationalEnergy) const;
  Eigen::Matrix<Eigen::Matrix3r, 3, 3> getdFramedFrame(const Eigen::Matrix3r& preFrame) const;
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> getdPosdFrame(const Eigen::Vector3r& preTrans) const;
};

#endif // CSEGMENTSYSTEM_H
