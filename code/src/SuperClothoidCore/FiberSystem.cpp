/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "FiberSystem.hpp"
#include <MathOperations.hpp>
#include <Logger.hpp>

using namespace MathOperations;

size_t FiberSystem::vpointSize() const { return 3*(1+sc.getNbSegments()); }
constexpr size_t FiberSystem::vpointID0() const { return 0; }
constexpr size_t FiberSystem::vpointIDsgt( const size_t& segmentID ) const { return 3+3*segmentID; }

FiberSystem::FiberSystem(Eigen::Vector3r gravity):
  G(gravity)
{
}

void FiberSystem::set( const SuperClothoid& sr ) 
{
  this->sc = sr; 
  size_t t=sr.getNbSegments();
  parts.clear();
  parts.reserve(t);
  for(size_t i=0;i<t;i++)
    parts.push_back(CSegmentSystem(G, sr.getLinearDensity(), sr.getK(), sr.getLength(i)));
  updateParts();
}

SuperClothoid FiberSystem::get() const { return sc; }

void FiberSystem::setPoint ( const Eigen::VectorXr& x )
{
  int t = sc.getNbSegments();
  assert(x.size() == vpointSize());
  sc.setOmegaInit(x[vpointID0()],x[vpointID0()+1],x[vpointID0()+2]);
  for (int i = 0; i < t; i++) {
    sc.setOmegaPrime(i, x[vpointIDsgt(i)], x[vpointIDsgt(i)+1], x[vpointIDsgt(i)+2]);
  }
  updateParts();
}

void FiberSystem::updateParts()
{
  gradPos.clear();
  gradFrames.clear();
  hessPos.clear();
  hessFrames.clear();
  size_t t=sc.getNbSegments();
  Eigen::Vector3r p=sc.getPosOr();
  Eigen::Matrix3r f=sc.getFrameOr();
  for(size_t i=0;i<t;i++)
  {
    parts.at(i).setPoint(p,f, sc.getOmega1(i), sc.getOmega2(i), sc.getOmega3(i), sc.getNaturalOmega1(i), sc.getNaturalOmega2(i), sc.getNaturalOmega3(i));
    std::tie(p,f)=parts.at(i).endPosFrame();
  }
}


Eigen::VectorXr FiberSystem::getPoint() const
{
  int t = sc.getNbSegments();
  Eigen::VectorXr pt(vpointSize());
  pt[vpointID0()] = sc.getOmega1(0).B;
  pt[vpointID0()+1] = sc.getOmega2(0).B;
  pt[vpointID0()+2] = sc.getOmega3(0).B;
  for (int i = 0; i < t; i++) {
    pt[vpointIDsgt(i)] = sc.getOmega1(i).A;
    pt[vpointIDsgt(i)+1] = sc.getOmega2(i).A;
    pt[vpointIDsgt(i)+2] = sc.getOmega3(i).A;
  }
  return pt;
}

real FiberSystem::energy()
{
  real E=0;
  size_t t = static_cast<size_t>(sc.getNbSegments());
  for(size_t i=0;i<t;i++)
    E+=parts.at(i).energy();
  return E;
}

Eigen::VectorXr FiberSystem::gradient()
{
  Eigen::VectorXr grad(vpointSize());
  grad.setZero();
  if(gradPos.empty())
    computeGradPosFrames();
  addGradient(grad);
  return grad;
}

Eigen::MatrixXr FiberSystem::hessian()
{
  if(gradPos.empty())
    computeGradPosFrames();
  if(hessFrames.empty())
    computeHessPosFrames();
  Eigen::MatrixXr h(vpointSize(),vpointSize());
  h.setZero();
  addHessian(h);
  return h;
}

real FiberSystem::posX()
{
  return parts.back().endPosFrame().first[2];
}

Eigen::VectorXr FiberSystem::gradPosX()
{
  Eigen::VectorXr grad(vpointSize());
  grad.setZero();
  gradPos.clear();
  gradFrames.clear();
  computeGradPosFrames();
  size_t t=sc.getNbSegments();
  size_t n=vpointSize();
  for(size_t i=0;i<n;i++)
    grad[i] = gradPos.at({t,i})[2];
  return grad;
}

Eigen::MatrixXr FiberSystem::hessPosX()
{
  size_t t=sc.getNbSegments();
  Eigen::MatrixXr hess(vpointSize(),vpointSize());
  hess.setZero();
  gradPos.clear();
  gradFrames.clear();
  hessPos.clear();
  hessFrames.clear();
  computeGradPosFrames();
  computeHessPosFrames();
  size_t n=vpointSize();
  for(size_t i=0;i<n;i++)
    for(size_t j=0;j<n;j++)
      hess(i,j)=hessPos.at({t,i,j})[2];
  return hess;
}

real FiberSystem::frameX()
{
  return parts.back().endPosFrame().second(2,0);
}

Eigen::VectorXr FiberSystem::gradFrameX()
{
  Eigen::VectorXr grad(vpointSize());
  grad.setZero();
  gradPos.clear();
  gradFrames.clear();
  computeGradPosFrames();
  size_t t=sc.getNbSegments();
  size_t n=vpointSize();
  for(size_t i=0;i<n;i++)
    grad[i]=gradFrames.at({t,i})(2,0);
  return grad;
}

Eigen::MatrixXr FiberSystem::hessFrameX()
{
  size_t t=sc.getNbSegments();
  Eigen::MatrixXr hess(vpointSize(),vpointSize());
  hess.setZero();
  gradPos.clear();
  gradFrames.clear();
  hessPos.clear();
  hessFrames.clear();
  computeGradPosFrames();
  computeHessPosFrames();
  size_t n=vpointSize();
  for(size_t i=0;i<n;i++)
    for(size_t j=0;j<n;j++)
      hess(i,j)=hessFrames.at({t,i,j})(2,0);
  return hess;
}

void FiberSystem::computeGradPosFrames()
{
  Eigen::Matrix<Eigen::Matrix<real,3,1>,3,1> dPosdPos;
  dPosdPos<<Eigen::Vector3r(1.,0.,0.),Eigen::Vector3r(0.,1.,0.),Eigen::Vector3r(0.,0.,1.);
  
  size_t t = sc.getNbSegments();
  for(size_t segmentID = 1; segmentID<=t;segmentID++)
  {
    const auto partSegmentGradPos = parts.at(segmentID-1).gradPos();
    const auto partSegmentGradFrame = parts.at(segmentID-1).gradFrame();
    for(size_t i=0;i<3;i++)gradPos[{segmentID,vpointIDsgt(segmentID-1)+i}]=extractDerivation(partSegmentGradPos,2*i);
    for(size_t i=0;i<3;i++)gradFrames[{segmentID,vpointIDsgt(segmentID-1)+i}]=extractDerivation(partSegmentGradFrame,2*i);
    
    
    for(size_t j=0; j<segmentID-1;j++)
    {
      for(size_t i=0;i<3;i++)
        gradPos[{segmentID,vpointIDsgt(j)+i}] = sc.getLength(j) * extractDerivation(partSegmentGradPos,1+2*i)
        + compDeriv(parts.at(segmentID-1).getdPosdFrame(), gradFrames[{segmentID-1,vpointIDsgt(j)+i}]) 
        + compDeriv(dPosdPos, gradPos[{segmentID-1,vpointIDsgt(j)+i}]);
      for(size_t i=0;i<3;i++)
        gradFrames[{segmentID,vpointIDsgt(j)+i}]=sc.getLength(j) * extractDerivation(partSegmentGradFrame,1+2*i) 
        + compDeriv(parts.at(segmentID-1) .getdFramedFrame(), gradFrames[{segmentID-1,vpointIDsgt(j)+i}]);
    }
    
    
    for(size_t i=0;i<3;i++)gradPos[{segmentID,vpointID0()+i}]=extractDerivation(partSegmentGradPos,1+2*i);
    for(size_t i=0;i<3;i++)gradFrames[{segmentID,vpointID0()+i}]=extractDerivation(partSegmentGradFrame,1+2*i);

    if(segmentID>1)
    {
      for(size_t i=0;i<3;i++)gradPos[{segmentID,vpointID0()+i}] +=
        compDeriv(parts.at(segmentID-1).getdPosdFrame(), gradFrames.at({segmentID-1,vpointID0()+i})) + compDeriv(dPosdPos, gradPos[{segmentID-1,vpointID0()+i}]);
      for(size_t i=0;i<3;i++)gradFrames[{segmentID,vpointID0()+i}] +=
        compDeriv(parts.at(segmentID-1).getdFramedFrame(), gradFrames[{segmentID-1,vpointID0()+i}]);
    }
  }
}

void FiberSystem::computeHessPosFrames()
{
  Eigen::Matrix<Eigen::Matrix<real,3,1>,3,1> dPosdPos;
  dPosdPos<<Eigen::Vector3r(1.,0.,0.),Eigen::Vector3r(0.,1.,0.),Eigen::Vector3r(0.,0.,1.);
  
  size_t t = sc.getNbSegments();
  for(size_t segmentID = 1; segmentID<=t;segmentID++)
  {
    Eigen::Matrix3r frameIinv=parts.at(segmentID-1).orPosFrame().second.inverse();
    const auto partSegmentGradPos = parts.at(segmentID-1).gradPos();
    const auto partSegmentGradFrame = parts.at(segmentID-1).gradFrame();
    
    //i,j dans le segment
    Eigen::Matrix<Eigen::Matrix6r, 3, 1> hs = parts.at(segmentID-1).gethessianPosOut();
    auto ext=[](const Eigen::Matrix<Eigen::Matrix6r, 3, 1>& in,const int i,const int j){return Eigen::Vector3r(in[0](i,j),in[1](i,j),in[2](i,j));};
    Eigen::Matrix<Eigen::Matrix6r, 3, 3> hs2 = parts.at(segmentID-1).gethessianFrameOut();
    auto ext2=[](const Eigen::Matrix<Eigen::Matrix6r, 3, 3>& in,const int k,const int l){
      Eigen::Matrix3r res;
      for(int i=0;i<9;i++)
        res(i%3,i/3)=in(i%3,i/3)(k,l);
      return res;
    };
    
    
    for(int i=0;i<3;i++)for(int j=0;j<=i;j++)hessFrames[{segmentID,vpointIDsgt(segmentID-1)+i,vpointIDsgt(segmentID-1)+j}]=ext2(hs2,2*i,2*j);
    for(int i=0;i<3;i++)for(int j=0;j<=i;j++)hessPos[{segmentID,vpointIDsgt(segmentID-1)+i,vpointIDsgt(segmentID-1)+j}]=ext(hs,2*i,2*j);
    if(segmentID==1)
    {
      for(int i=0;i<3;i++)for(int j=0;j<=i;j++)hessFrames[{segmentID,vpointID0()+i,vpointID0()+j}]=ext2(hs2,2*j+1,2*i+1);
      hessFrames[{segmentID,vpointID0()+0,vpointIDsgt(segmentID-1)+0}]=ext2(hs2,0,1);
      hessFrames[{segmentID,vpointID0()+1,vpointIDsgt(segmentID-1)+0}]=ext2(hs2,0,3);
      hessFrames[{segmentID,vpointID0()+2,vpointIDsgt(segmentID-1)+0}]=ext2(hs2,0,5);
      hessFrames[{segmentID,vpointIDsgt(segmentID-1)+1,vpointID0()+0}]=ext2(hs2,1,2);
      hessFrames[{segmentID,vpointID0()+1,vpointIDsgt(segmentID-1)+1}]=ext2(hs2,2,3);
      hessFrames[{segmentID,vpointID0()+2,vpointIDsgt(segmentID-1)+1}]=ext2(hs2,2,5);
      hessFrames[{segmentID,vpointIDsgt(segmentID-1)+2,vpointID0()+0}]=ext2(hs2,1,4);
      hessFrames[{segmentID,vpointIDsgt(segmentID-1)+2,vpointID0()+1}]=ext2(hs2,3,4);
      hessFrames[{segmentID,vpointID0()+2,vpointIDsgt(segmentID-1)+2}]=ext2(hs2,4,5);
      for(int i=0;i<3;i++)for(int j=0;j<=i;j++)hessPos[{segmentID,vpointID0()+i,vpointID0()+j}]=ext(hs,2*j+1,2*i+1);
      hessPos[{segmentID,vpointID0()+0,vpointIDsgt(segmentID-1)+0}]=ext(hs,0,1);
      hessPos[{segmentID,vpointID0()+1,vpointIDsgt(segmentID-1)+0}]=ext(hs,0,3);
      hessPos[{segmentID,vpointID0()+2,vpointIDsgt(segmentID-1)+0}]=ext(hs,0,5);
      hessPos[{segmentID,vpointIDsgt(segmentID-1)+1,vpointID0()+0}]=ext(hs,1,2);
      hessPos[{segmentID,vpointID0()+1,vpointIDsgt(segmentID-1)+1}]=ext(hs,2,3);
      hessPos[{segmentID,vpointID0()+2,vpointIDsgt(segmentID-1)+1}]=ext(hs,2,5);
      hessPos[{segmentID,vpointIDsgt(segmentID-1)+2,vpointID0()+0}]=ext(hs,1,4);
      hessPos[{segmentID,vpointIDsgt(segmentID-1)+2,vpointID0()+1}]=ext(hs,3,4);
      hessPos[{segmentID,vpointID0()+2,vpointIDsgt(segmentID-1)+2}]=ext(hs,4,5);
    }
    else
    {
      hessFrames[{segmentID,vpointID0()+0,vpointID0()+0}]=compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointID0()+0,vpointID0()+0}))
          +ext2(hs2,1,1)
          +2*gradFrames.at({segmentID-1,vpointID0()+0}) * frameIinv*extractDerivation(partSegmentGradFrame,1);
      hessFrames[{segmentID,vpointID0()+1,vpointID0()+0}]=compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointID0()+0,vpointID0()+1}))
          +ext2(hs2,1,3)
          +gradFrames.at({segmentID-1,vpointID0()+0}) * frameIinv*extractDerivation(partSegmentGradFrame,3)
          +gradFrames.at({segmentID-1,vpointID0()+1}) * frameIinv*extractDerivation(partSegmentGradFrame,1);
      hessFrames[{segmentID,vpointID0()+2,vpointID0()+0}]=compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointID0()+0,vpointID0()+2}))
          +ext2(hs2,1,5)
          +gradFrames.at({segmentID-1,vpointID0()+0}) * frameIinv*extractDerivation(partSegmentGradFrame,5)
          +gradFrames.at({segmentID-1,vpointID0()+2}) * frameIinv*extractDerivation(partSegmentGradFrame,1);
      hessFrames[{segmentID,vpointID0()+1,vpointID0()+1}]=compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointID0()+1,vpointID0()+1}))
          +ext2(hs2,3,3)
          +2*gradFrames.at({segmentID-1,vpointID0()+1}) * frameIinv*extractDerivation(partSegmentGradFrame,3);
      hessFrames[{segmentID,vpointID0()+2,vpointID0()+1}]=compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointID0()+1,vpointID0()+2}))
          +ext2(hs2,3,5)
          +gradFrames.at({segmentID-1,vpointID0()+1}) * frameIinv*extractDerivation(partSegmentGradFrame,5)
          +gradFrames.at({segmentID-1,vpointID0()+2}) * frameIinv*extractDerivation(partSegmentGradFrame,3);
      hessFrames[{segmentID,vpointID0()+2,vpointID0()+2}]=compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointID0()+2,vpointID0()+2}))
          +ext2(hs2,5,5)
          +2*gradFrames.at({segmentID-1,vpointID0()+2}) * frameIinv*extractDerivation(partSegmentGradFrame,5);
          
      hessFrames[{segmentID,vpointID0()+0,vpointIDsgt(segmentID-1)+0}]=ext2(hs2,0,1)
          +gradFrames.at({segmentID-1,vpointID0()+0}) * frameIinv*extractDerivation(partSegmentGradFrame,0);
      hessFrames[{segmentID,vpointID0()+1,vpointIDsgt(segmentID-1)+0}]=ext2(hs2,0,3)
          +gradFrames.at({segmentID-1,vpointID0()+1}) * frameIinv*extractDerivation(partSegmentGradFrame,0);
      hessFrames[{segmentID,vpointID0()+2,vpointIDsgt(segmentID-1)+0}]=ext2(hs2,0,5)
          +gradFrames.at({segmentID-1,vpointID0()+2}) * frameIinv*extractDerivation(partSegmentGradFrame,0);
      hessFrames[{segmentID,vpointIDsgt(segmentID-1)+1,vpointID0()+0}]=ext2(hs2,1,2)
          +gradFrames.at({segmentID-1,vpointID0()+0}) * frameIinv*extractDerivation(partSegmentGradFrame,2);
      hessFrames[{segmentID,vpointID0()+1,vpointIDsgt(segmentID-1)+1}]=ext2(hs2,2,3)
          +gradFrames.at({segmentID-1,vpointID0()+1}) * frameIinv*extractDerivation(partSegmentGradFrame,2);
      hessFrames[{segmentID,vpointID0()+2,vpointIDsgt(segmentID-1)+1}]=ext2(hs2,2,5)
          +gradFrames.at({segmentID-1,vpointID0()+2}) * frameIinv*extractDerivation(partSegmentGradFrame,2);
      hessFrames[{segmentID,vpointIDsgt(segmentID-1)+2,vpointID0()+0}]=ext2(hs2,1,4)
          +gradFrames.at({segmentID-1,vpointID0()+0}) * frameIinv*extractDerivation(partSegmentGradFrame,4);
      hessFrames[{segmentID,vpointIDsgt(segmentID-1)+2,vpointID0()+1}]=ext2(hs2,3,4)
          +gradFrames.at({segmentID-1,vpointID0()+1}) * frameIinv*extractDerivation(partSegmentGradFrame,4);
      hessFrames[{segmentID,vpointID0()+2,vpointIDsgt(segmentID-1)+2}]=ext2(hs2,4,5)
          +gradFrames.at({segmentID-1,vpointID0()+2}) * frameIinv*extractDerivation(partSegmentGradFrame,4);
          
      hessPos[{segmentID,vpointID0()+0,vpointID0()+0}]=compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointID0()+0,vpointID0()+0}))
          + compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointID0()+0,vpointID0()+0}))
          +ext(hs,1,1)
          +2*gradFrames.at({segmentID-1,vpointID0()+0}) * frameIinv*extractDerivation(partSegmentGradPos,1);
      hessPos[{segmentID,vpointID0()+1,vpointID0()+0}]=compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointID0()+0,vpointID0()+1}))
        +compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointID0()+0,vpointID0()+1}))
          +ext(hs,1,3)
          +gradFrames.at({segmentID-1,vpointID0()+0}) * frameIinv*extractDerivation(partSegmentGradPos,3)
          +gradFrames.at({segmentID-1,vpointID0()+1}) * frameIinv*extractDerivation(partSegmentGradPos,1);
      hessPos[{segmentID,vpointID0()+2,vpointID0()+0}]=compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointID0()+0,vpointID0()+2}))
        +compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointID0()+0,vpointID0()+2}))
          +ext(hs,1,5)
          +gradFrames.at({segmentID-1,vpointID0()+0}) * frameIinv*extractDerivation(partSegmentGradPos,5)
          +gradFrames.at({segmentID-1,vpointID0()+2}) * frameIinv*extractDerivation(partSegmentGradPos,1);
      hessPos[{segmentID,vpointID0()+1,vpointID0()+1}]=compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointID0()+1,vpointID0()+1}))
          + compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointID0()+1,vpointID0()+1}))
          +ext(hs,3,3)
          +2*gradFrames.at({segmentID-1,vpointID0()+1}) * frameIinv*extractDerivation(partSegmentGradPos,3);
      hessPos[{segmentID,vpointID0()+2,vpointID0()+1}]=compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointID0()+1,vpointID0()+2}))
          +compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointID0()+1,vpointID0()+2}))
          +ext(hs,3,5)
          +gradFrames.at({segmentID-1,vpointID0()+1}) * frameIinv*extractDerivation(partSegmentGradPos,5)
          +gradFrames.at({segmentID-1,vpointID0()+2}) * frameIinv*extractDerivation(partSegmentGradPos,3);
      hessPos[{segmentID,vpointID0()+2,vpointID0()+2}]=compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointID0()+2,vpointID0()+2}))
          + compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointID0()+2,vpointID0()+2}))
          +ext(hs,5,5)
          +2*gradFrames.at({segmentID-1,vpointID0()+2}) * frameIinv*extractDerivation(partSegmentGradPos,5);
          
      hessPos[{segmentID,vpointID0()+0,vpointIDsgt(segmentID-1)+0}]=ext(hs,0,1)
        +gradFrames.at({segmentID-1,vpointID0()+0}) * frameIinv*extractDerivation(partSegmentGradPos,0);
      hessPos[{segmentID,vpointID0()+1,vpointIDsgt(segmentID-1)+0}]=ext(hs,0,3)
        +gradFrames.at({segmentID-1,vpointID0()+1}) * frameIinv*extractDerivation(partSegmentGradPos,0);
      hessPos[{segmentID,vpointID0()+2,vpointIDsgt(segmentID-1)+0}]=ext(hs,0,5)
        +gradFrames.at({segmentID-1,vpointID0()+2}) * frameIinv*extractDerivation(partSegmentGradPos,0);
      hessPos[{segmentID,vpointIDsgt(segmentID-1)+1,vpointID0()+0}]=ext(hs,1,2)
        +gradFrames.at({segmentID-1,vpointID0()+0}) * frameIinv*extractDerivation(partSegmentGradPos,2);
      hessPos[{segmentID,vpointID0()+1,vpointIDsgt(segmentID-1)+1}]=ext(hs,2,3)
        +gradFrames.at({segmentID-1,vpointID0()+1}) * frameIinv*extractDerivation(partSegmentGradPos,2);
      hessPos[{segmentID,vpointID0()+2,vpointIDsgt(segmentID-1)+1}]=ext(hs,2,5)
        +gradFrames.at({segmentID-1,vpointID0()+2}) * frameIinv*extractDerivation(partSegmentGradPos,2);
      hessPos[{segmentID,vpointIDsgt(segmentID-1)+2,vpointID0()+0}]=ext(hs,1,4)
        +gradFrames.at({segmentID-1,vpointID0()+0}) * frameIinv*extractDerivation(partSegmentGradPos,4);
      hessPos[{segmentID,vpointIDsgt(segmentID-1)+2,vpointID0()+1}]=ext(hs,3,4)
        +gradFrames.at({segmentID-1,vpointID0()+1}) * frameIinv*extractDerivation(partSegmentGradPos,4);
      hessPos[{segmentID,vpointID0()+2,vpointIDsgt(segmentID-1)+2}]=ext(hs,4,5)
        +gradFrames.at({segmentID-1,vpointID0()+2}) * frameIinv*extractDerivation(partSegmentGradPos,4);
    }
    
    
    hessFrames[{segmentID,vpointIDsgt(segmentID-1)+0,vpointID0()+0}]=hessFrames.at({segmentID,vpointID0()+0,vpointIDsgt(segmentID-1)+0});
    hessFrames[{segmentID,vpointIDsgt(segmentID-1)+0,vpointIDsgt(segmentID-1)+1}]=hessFrames.at({segmentID,vpointIDsgt(segmentID-1)+1,vpointIDsgt(segmentID-1)+0});
    hessFrames[{segmentID,vpointIDsgt(segmentID-1)+0,vpointID0()+1}]=hessFrames.at({segmentID,vpointID0()+1,vpointIDsgt(segmentID-1)+0});
    hessFrames[{segmentID,vpointIDsgt(segmentID-1)+0,vpointIDsgt(segmentID-1)+2}]=hessFrames.at({segmentID,vpointIDsgt(segmentID-1)+2,vpointIDsgt(segmentID-1)+0});
    hessFrames[{segmentID,vpointIDsgt(segmentID-1)+0,vpointID0()+2}]=hessFrames.at({segmentID,vpointID0()+2,vpointIDsgt(segmentID-1)+0});
    hessFrames[{segmentID,vpointID0()+0,vpointIDsgt(segmentID-1)+1}]=hessFrames.at({segmentID,vpointIDsgt(segmentID-1)+1,vpointID0()+0});
    hessFrames[{segmentID,vpointID0()+0,vpointID0()+1}]=hessFrames.at({segmentID,vpointID0()+1,vpointID0()+0});
    hessFrames[{segmentID,vpointID0()+0,vpointIDsgt(segmentID-1)+2}]=hessFrames.at({segmentID,vpointIDsgt(segmentID-1)+2,vpointID0()+0});
    hessFrames[{segmentID,vpointID0()+0,vpointID0()+2}]=hessFrames.at({segmentID,vpointID0()+2,vpointID0()+0});
    hessFrames[{segmentID,vpointIDsgt(segmentID-1)+1,vpointID0()+1}]=hessFrames.at({segmentID,vpointID0()+1,vpointIDsgt(segmentID-1)+1});
    hessFrames[{segmentID,vpointIDsgt(segmentID-1)+1,vpointIDsgt(segmentID-1)+2}]=hessFrames.at({segmentID,vpointIDsgt(segmentID-1)+2,vpointIDsgt(segmentID-1)+1});
    hessFrames[{segmentID,vpointIDsgt(segmentID-1)+1,vpointID0()+2}]=hessFrames.at({segmentID,vpointID0()+2,vpointIDsgt(segmentID-1)+1});
    hessFrames[{segmentID,vpointID0()+1,vpointIDsgt(segmentID-1)+2}]=hessFrames.at({segmentID,vpointIDsgt(segmentID-1)+2,vpointID0()+1});
    hessFrames[{segmentID,vpointID0()+1,vpointID0()+2}]=hessFrames.at({segmentID,vpointID0()+2,vpointID0()+1});
    hessFrames[{segmentID,vpointIDsgt(segmentID-1)+2,vpointID0()+2}]=hessFrames.at({segmentID,vpointID0()+2,vpointIDsgt(segmentID-1)+2});
    
    hessPos[{segmentID,vpointIDsgt(segmentID-1)+0,vpointID0()+0}]=hessPos.at({segmentID,vpointID0()+0,vpointIDsgt(segmentID-1)+0});
    hessPos[{segmentID,vpointIDsgt(segmentID-1)+0,vpointIDsgt(segmentID-1)+1}]=hessPos.at({segmentID,vpointIDsgt(segmentID-1)+1,vpointIDsgt(segmentID-1)+0});
    hessPos[{segmentID,vpointIDsgt(segmentID-1)+0,vpointID0()+1}]=hessPos.at({segmentID,vpointID0()+1,vpointIDsgt(segmentID-1)+0});
    hessPos[{segmentID,vpointIDsgt(segmentID-1)+0,vpointIDsgt(segmentID-1)+2}]=hessPos.at({segmentID,vpointIDsgt(segmentID-1)+2,vpointIDsgt(segmentID-1)+0});
    hessPos[{segmentID,vpointIDsgt(segmentID-1)+0,vpointID0()+2}]=hessPos.at({segmentID,vpointID0()+2,vpointIDsgt(segmentID-1)+0});
    hessPos[{segmentID,vpointID0()+0,vpointIDsgt(segmentID-1)+1}]=hessPos.at({segmentID,vpointIDsgt(segmentID-1)+1,vpointID0()+0});
    hessPos[{segmentID,vpointID0()+0,vpointID0()+1}]=hessPos.at({segmentID,vpointID0()+1,vpointID0()+0});
    hessPos[{segmentID,vpointID0()+0,vpointIDsgt(segmentID-1)+2}]=hessPos.at({segmentID,vpointIDsgt(segmentID-1)+2,vpointID0()+0});
    hessPos[{segmentID,vpointID0()+0,vpointID0()+2}]=hessPos.at({segmentID,vpointID0()+2,vpointID0()+0});
    hessPos[{segmentID,vpointIDsgt(segmentID-1)+1,vpointID0()+1}]=hessPos.at({segmentID,vpointID0()+1,vpointIDsgt(segmentID-1)+1});
    hessPos[{segmentID,vpointIDsgt(segmentID-1)+1,vpointIDsgt(segmentID-1)+2}]=hessPos.at({segmentID,vpointIDsgt(segmentID-1)+2,vpointIDsgt(segmentID-1)+1});
    hessPos[{segmentID,vpointIDsgt(segmentID-1)+1,vpointID0()+2}]=hessPos.at({segmentID,vpointID0()+2,vpointIDsgt(segmentID-1)+1});
    hessPos[{segmentID,vpointID0()+1,vpointIDsgt(segmentID-1)+2}]=hessPos.at({segmentID,vpointIDsgt(segmentID-1)+2,vpointID0()+1});
    hessPos[{segmentID,vpointID0()+1,vpointID0()+2}]=hessPos.at({segmentID,vpointID0()+2,vpointID0()+1});
    hessPos[{segmentID,vpointIDsgt(segmentID-1)+2,vpointID0()+2}]=hessPos.at({segmentID,vpointID0()+2,vpointIDsgt(segmentID-1)+2});
    //i ou(exclusif) j dans le segment
    for(size_t i=0;i<segmentID-1;i++)
    {
      
      hessPos[{segmentID,vpointIDsgt(i)+0,vpointIDsgt(segmentID-1)+0}] = gradFrames.at({segmentID-1,vpointIDsgt(i)+0}) * frameIinv * extractDerivation(partSegmentGradPos,0)
        + sc.getLength(i)* ext(hs,0,1);
      hessPos[{segmentID,vpointIDsgt(i)+0,vpointIDsgt(segmentID-1)+1}] = gradFrames.at({segmentID-1,vpointIDsgt(i)+0}) * frameIinv * extractDerivation(partSegmentGradPos,2)
        + sc.getLength(i)* ext(hs,2,1);
      hessPos[{segmentID,vpointIDsgt(i)+0,vpointIDsgt(segmentID-1)+2}] = gradFrames.at({segmentID-1,vpointIDsgt(i)+0}) * frameIinv * extractDerivation(partSegmentGradPos,4)
        + sc.getLength(i)* ext(hs,4,1);
      hessPos[{segmentID,vpointIDsgt(i)+1,vpointIDsgt(segmentID-1)+0}] = gradFrames.at({segmentID-1,vpointIDsgt(i)+1}) * frameIinv * extractDerivation(partSegmentGradPos,0)
        + sc.getLength(i)* ext(hs,0,3);
      hessPos[{segmentID,vpointIDsgt(i)+1,vpointIDsgt(segmentID-1)+1}] = gradFrames.at({segmentID-1,vpointIDsgt(i)+1}) * frameIinv * extractDerivation(partSegmentGradPos,2)
        + sc.getLength(i)* ext(hs,2,3);
      hessPos[{segmentID,vpointIDsgt(i)+1,vpointIDsgt(segmentID-1)+2}] = gradFrames.at({segmentID-1,vpointIDsgt(i)+1}) * frameIinv * extractDerivation(partSegmentGradPos,4)
        + sc.getLength(i)* ext(hs,4,3);
      hessPos[{segmentID,vpointIDsgt(i)+2,vpointIDsgt(segmentID-1)+0}] = gradFrames.at({segmentID-1,vpointIDsgt(i)+2}) * frameIinv * extractDerivation(partSegmentGradPos,0)
        + sc.getLength(i)* ext(hs,0,5);
      hessPos[{segmentID,vpointIDsgt(i)+2,vpointIDsgt(segmentID-1)+1}] = gradFrames.at({segmentID-1,vpointIDsgt(i)+2}) * frameIinv * extractDerivation(partSegmentGradPos,2)
        + sc.getLength(i)* ext(hs,2,5);
      hessPos[{segmentID,vpointIDsgt(i)+2,vpointIDsgt(segmentID-1)+2}] = gradFrames.at({segmentID-1,vpointIDsgt(i)+2}) * frameIinv * extractDerivation(partSegmentGradPos,4)
        + sc.getLength(i)* ext(hs,4,5);
        
      hessPos[{segmentID,vpointIDsgt(i)+0,vpointID0()+0}] =  gradFrames.at({segmentID-1,vpointIDsgt(i)+0}) * frameIinv*extractDerivation(partSegmentGradPos,1)
        + sc.getLength(i)*ext(hs,1,1) +sc.getLength(i)*gradFrames.at({segmentID-1,vpointID0()+0})*frameIinv*extractDerivation(partSegmentGradPos,1)
        + compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+0,vpointID0()+0}))
        + compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointIDsgt(i)+0,vpointID0()+0}));
      
      hessPos[{segmentID,vpointIDsgt(i)+0,vpointID0()+1}] =  gradFrames.at({segmentID-1,vpointIDsgt(i)+0}) * frameIinv*extractDerivation(partSegmentGradPos,3)
        + sc.getLength(i)*ext(hs,1,3) +sc.getLength(i)*gradFrames.at({segmentID-1,vpointID0()+1})*frameIinv*extractDerivation(partSegmentGradPos,1)
        + compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+0,vpointID0()+1}))
        + compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointIDsgt(i)+0,vpointID0()+1}));
        
      hessPos[{segmentID,vpointIDsgt(i)+0,vpointID0()+2}] =  gradFrames.at({segmentID-1,vpointIDsgt(i)+0}) * frameIinv*extractDerivation(partSegmentGradPos,5)
        + sc.getLength(i)*ext(hs,1,5) +sc.getLength(i)*gradFrames.at({segmentID-1,vpointID0()+2})*frameIinv*extractDerivation(partSegmentGradPos,1)
        + compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+0,vpointID0()+2}))
        + compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointIDsgt(i)+0,vpointID0()+2}));
        
      hessPos[{segmentID,vpointIDsgt(i)+1,vpointID0()+0}] =    gradFrames.at({segmentID-1,vpointIDsgt(i)+1}) * frameIinv*extractDerivation(partSegmentGradPos,1)
        + sc.getLength(i)*ext(hs,3,1) +sc.getLength(i)*gradFrames.at({segmentID-1,vpointID0()+0})*frameIinv*extractDerivation(partSegmentGradPos,3)
        + compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+1,vpointID0()+0}))
        + compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointIDsgt(i)+1,vpointID0()+0}));
        
      hessPos[{segmentID,vpointIDsgt(i)+1,vpointID0()+1}] =  gradFrames.at({segmentID-1,vpointIDsgt(i)+1}) * frameIinv*extractDerivation(partSegmentGradPos,3)
        + sc.getLength(i)*ext(hs,3,3) +sc.getLength(i)*gradFrames.at({segmentID-1,vpointID0()+1})*frameIinv*extractDerivation(partSegmentGradPos,3)
        + compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+1,vpointID0()+1}))
        + compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointIDsgt(i)+1,vpointID0()+1}));
        
      hessPos[{segmentID,vpointIDsgt(i)+1,vpointID0()+2}] =    gradFrames.at({segmentID-1,vpointIDsgt(i)+1}) * frameIinv*extractDerivation(partSegmentGradPos,5)
        + sc.getLength(i)*ext(hs,3,5) +sc.getLength(i)*gradFrames.at({segmentID-1,vpointID0()+2})*frameIinv*extractDerivation(partSegmentGradPos,3)
        + compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+1,vpointID0()+2}))
        + compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointIDsgt(i)+1,vpointID0()+2}));
        
      hessPos[{segmentID,vpointIDsgt(i)+2,vpointID0()+0}] =    gradFrames.at({segmentID-1,vpointIDsgt(i)+2}) * frameIinv*extractDerivation(partSegmentGradPos,1)
        + sc.getLength(i)*ext(hs,5,1) +sc.getLength(i)*gradFrames.at({segmentID-1,vpointID0()+0})*frameIinv*extractDerivation(partSegmentGradPos,5)
        + compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+2,vpointID0()+0}))
        + compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointIDsgt(i)+2,vpointID0()+0}));
        
      hessPos[{segmentID,vpointIDsgt(i)+2,vpointID0()+1}] =    gradFrames.at({segmentID-1,vpointIDsgt(i)+2}) * frameIinv*extractDerivation(partSegmentGradPos,3)
        + sc.getLength(i)*ext(hs,5,3) +sc.getLength(i)*gradFrames.at({segmentID-1,vpointID0()+1})*frameIinv*extractDerivation(partSegmentGradPos,5)
        + compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+2,vpointID0()+1}))
        + compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointIDsgt(i)+2,vpointID0()+1}));
        
      hessPos[{segmentID,vpointIDsgt(i)+2,vpointID0()+2}] =    gradFrames.at({segmentID-1,vpointIDsgt(i)+2}) * frameIinv*extractDerivation(partSegmentGradPos,5)
        + sc.getLength(i)*ext(hs,5,5) +sc.getLength(i)*gradFrames.at({segmentID-1,vpointID0()+2})*frameIinv*extractDerivation(partSegmentGradPos,5)
        + compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+2,vpointID0()+2}))
        + compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointIDsgt(i)+2,vpointID0()+2}));
                                                          
                                                          
      hessFrames[{segmentID,vpointIDsgt(i)+0,vpointIDsgt(segmentID-1)+0}] =  gradFrames.at({segmentID-1,vpointIDsgt(i)+0}) * frameIinv*extractDerivation(partSegmentGradFrame,0)
        +sc.getLength(i)*ext2(hs2,0,1);
      hessFrames[{segmentID,vpointIDsgt(i)+0,vpointIDsgt(segmentID-1)+1}] =  gradFrames.at({segmentID-1,vpointIDsgt(i)+0}) * frameIinv*extractDerivation(partSegmentGradFrame,2)
        +sc.getLength(i)*ext2(hs2,2,1);
      hessFrames[{segmentID,vpointIDsgt(i)+0,vpointIDsgt(segmentID-1)+2}] =  gradFrames.at({segmentID-1,vpointIDsgt(i)+0}) * frameIinv*extractDerivation(partSegmentGradFrame,4)
        +sc.getLength(i)*ext2(hs2,4,1);
      hessFrames[{segmentID,vpointIDsgt(i)+1,vpointIDsgt(segmentID-1)+0}] =  gradFrames.at({segmentID-1,vpointIDsgt(i)+1}) * frameIinv*extractDerivation(partSegmentGradFrame,0)
        +sc.getLength(i)*ext2(hs2,0,3);
      hessFrames[{segmentID,vpointIDsgt(i)+1,vpointIDsgt(segmentID-1)+1}] =  gradFrames.at({segmentID-1,vpointIDsgt(i)+1}) * frameIinv*extractDerivation(partSegmentGradFrame,2)
        +sc.getLength(i)*ext2(hs2,2,3);
      hessFrames[{segmentID,vpointIDsgt(i)+1,vpointIDsgt(segmentID-1)+2}] =  gradFrames.at({segmentID-1,vpointIDsgt(i)+1}) * frameIinv*extractDerivation(partSegmentGradFrame,4)
        +sc.getLength(i)*ext2(hs2,4,3);
      hessFrames[{segmentID,vpointIDsgt(i)+2,vpointIDsgt(segmentID-1)+0}] =  gradFrames.at({segmentID-1,vpointIDsgt(i)+2}) * frameIinv*extractDerivation(partSegmentGradFrame,0)
        +sc.getLength(i)*ext2(hs2,0,5);
      hessFrames[{segmentID,vpointIDsgt(i)+2,vpointIDsgt(segmentID-1)+1}] =  gradFrames.at({segmentID-1,vpointIDsgt(i)+2}) * frameIinv*extractDerivation(partSegmentGradFrame,2)
        +sc.getLength(i)*ext2(hs2,2,5);
      hessFrames[{segmentID,vpointIDsgt(i)+2,vpointIDsgt(segmentID-1)+2}] =  gradFrames.at({segmentID-1,vpointIDsgt(i)+2}) * frameIinv*extractDerivation(partSegmentGradFrame,4)
        +sc.getLength(i)*ext2(hs2,4,5);
                                                                  
      hessFrames[{segmentID,vpointIDsgt(i)+0,vpointID0()+0}] = gradFrames.at({segmentID-1,vpointIDsgt(i)+0}) * frameIinv*extractDerivation(partSegmentGradFrame,1)
        + sc.getLength(i)*ext2(hs2,1,1) +sc.getLength(i)*gradFrames.at({segmentID-1,vpointID0()+0})*frameIinv*extractDerivation(partSegmentGradFrame,1)
        + compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+0,vpointID0()+0}));
        
      hessFrames[{segmentID,vpointIDsgt(i)+0,vpointID0()+1}] =  gradFrames.at({segmentID-1,vpointIDsgt(i)+0}) * frameIinv*extractDerivation(partSegmentGradFrame,3)
        + sc.getLength(i)*ext2(hs2,1,3) +sc.getLength(i)*gradFrames.at({segmentID-1,vpointID0()+1})*frameIinv*extractDerivation(partSegmentGradFrame,1)
        + compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+0,vpointID0()+1}));
        
      hessFrames[{segmentID,vpointIDsgt(i)+0,vpointID0()+2}] =  gradFrames.at({segmentID-1,vpointIDsgt(i)+0}) * frameIinv*extractDerivation(partSegmentGradFrame,5)
        + sc.getLength(i)*ext2(hs2,1,5) +sc.getLength(i)*gradFrames.at({segmentID-1,vpointID0()+2})*frameIinv*extractDerivation(partSegmentGradFrame,1)
        + compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+0,vpointID0()+2}));
        
      hessFrames[{segmentID,vpointIDsgt(i)+1,vpointID0()+0}] =  gradFrames.at({segmentID-1,vpointIDsgt(i)+1}) * frameIinv*extractDerivation(partSegmentGradFrame,1)
        + sc.getLength(i)*ext2(hs2,3,1) +sc.getLength(i)*gradFrames.at({segmentID-1,vpointID0()+0})*frameIinv*extractDerivation(partSegmentGradFrame,3)
        + compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+1,vpointID0()+0}));
      
      hessFrames[{segmentID,vpointIDsgt(i)+1,vpointID0()+1}] =  gradFrames.at({segmentID-1,vpointIDsgt(i)+1}) * frameIinv*extractDerivation(partSegmentGradFrame,3)
        + sc.getLength(i)*ext2(hs2,3,3) +sc.getLength(i)*gradFrames.at({segmentID-1,vpointID0()+1})*frameIinv*extractDerivation(partSegmentGradFrame,3)
        + compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+1,vpointID0()+1}));
        
      hessFrames[{segmentID,vpointIDsgt(i)+1,vpointID0()+2}] =  gradFrames.at({segmentID-1,vpointIDsgt(i)+1}) * frameIinv*extractDerivation(partSegmentGradFrame,5)
        + sc.getLength(i)*ext2(hs2,3,5) +sc.getLength(i)*gradFrames.at({segmentID-1,vpointID0()+2})*frameIinv*extractDerivation(partSegmentGradFrame,3)
        + compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+1,vpointID0()+2}));
        
      hessFrames[{segmentID,vpointIDsgt(i)+2,vpointID0()+0}] =  gradFrames.at({segmentID-1,vpointIDsgt(i)+2}) * frameIinv*extractDerivation(partSegmentGradFrame,1)
        + sc.getLength(i)*ext2(hs2,5,1) +sc.getLength(i)*gradFrames.at({segmentID-1,vpointID0()+0})*frameIinv*extractDerivation(partSegmentGradFrame,5)
        + compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+2,vpointID0()+0}));
      
      hessFrames[{segmentID,vpointIDsgt(i)+2,vpointID0()+1}] =  gradFrames.at({segmentID-1,vpointIDsgt(i)+2}) * frameIinv*extractDerivation(partSegmentGradFrame,3)
        + sc.getLength(i)*ext2(hs2,5,3) +sc.getLength(i)*gradFrames.at({segmentID-1,vpointID0()+1})*frameIinv*extractDerivation(partSegmentGradFrame,5)
        + compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+2,vpointID0()+1}));
        
      hessFrames[{segmentID,vpointIDsgt(i)+2,vpointID0()+2}] =  gradFrames.at({segmentID-1,vpointIDsgt(i)+2}) * frameIinv*extractDerivation(partSegmentGradFrame,5)
        + sc.getLength(i)*ext2(hs2,5,5) +sc.getLength(i)*gradFrames.at({segmentID-1,vpointID0()+2})*frameIinv*extractDerivation(partSegmentGradFrame,5)
        + compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+2,vpointID0()+2}));
                                                          
      hessPos[{segmentID,vpointIDsgt(segmentID-1)+0,vpointIDsgt(i)+0}] = hessPos.at({segmentID,vpointIDsgt(i)+0,vpointIDsgt(segmentID-1)+0});
      hessPos[{segmentID,vpointIDsgt(segmentID-1)+1,vpointIDsgt(i)+0}] = hessPos.at({segmentID,vpointIDsgt(i)+0,vpointIDsgt(segmentID-1)+1});
      hessPos[{segmentID,vpointIDsgt(segmentID-1)+2,vpointIDsgt(i)+0}] = hessPos.at({segmentID,vpointIDsgt(i)+0,vpointIDsgt(segmentID-1)+2});
      hessPos[{segmentID,vpointIDsgt(segmentID-1)+0,vpointIDsgt(i)+1}] = hessPos.at({segmentID,vpointIDsgt(i)+1,vpointIDsgt(segmentID-1)+0});
      hessPos[{segmentID,vpointIDsgt(segmentID-1)+1,vpointIDsgt(i)+1}] = hessPos.at({segmentID,vpointIDsgt(i)+1,vpointIDsgt(segmentID-1)+1});
      hessPos[{segmentID,vpointIDsgt(segmentID-1)+2,vpointIDsgt(i)+1}] = hessPos.at({segmentID,vpointIDsgt(i)+1,vpointIDsgt(segmentID-1)+2});
      hessPos[{segmentID,vpointIDsgt(segmentID-1)+0,vpointIDsgt(i)+2}] = hessPos.at({segmentID,vpointIDsgt(i)+2,vpointIDsgt(segmentID-1)+0});
      hessPos[{segmentID,vpointIDsgt(segmentID-1)+1,vpointIDsgt(i)+2}] = hessPos.at({segmentID,vpointIDsgt(i)+2,vpointIDsgt(segmentID-1)+1});
      hessPos[{segmentID,vpointIDsgt(segmentID-1)+2,vpointIDsgt(i)+2}] = hessPos.at({segmentID,vpointIDsgt(i)+2,vpointIDsgt(segmentID-1)+2});
      hessPos[{segmentID,vpointID0()+0,vpointIDsgt(i)+0}] = hessPos.at({segmentID,vpointIDsgt(i)+0,vpointID0()+0});
      hessPos[{segmentID,vpointID0()+1,vpointIDsgt(i)+0}] = hessPos.at({segmentID,vpointIDsgt(i)+0,vpointID0()+1});
      hessPos[{segmentID,vpointID0()+2,vpointIDsgt(i)+0}] = hessPos.at({segmentID,vpointIDsgt(i)+0,vpointID0()+2});
      hessPos[{segmentID,vpointID0()+0,vpointIDsgt(i)+1}] = hessPos.at({segmentID,vpointIDsgt(i)+1,vpointID0()+0});
      hessPos[{segmentID,vpointID0()+1,vpointIDsgt(i)+1}] = hessPos.at({segmentID,vpointIDsgt(i)+1,vpointID0()+1});
      hessPos[{segmentID,vpointID0()+2,vpointIDsgt(i)+1}] = hessPos.at({segmentID,vpointIDsgt(i)+1,vpointID0()+2});
      hessPos[{segmentID,vpointID0()+0,vpointIDsgt(i)+2}] = hessPos.at({segmentID,vpointIDsgt(i)+2,vpointID0()+0});
      hessPos[{segmentID,vpointID0()+1,vpointIDsgt(i)+2}] = hessPos.at({segmentID,vpointIDsgt(i)+2,vpointID0()+1});
      hessPos[{segmentID,vpointID0()+2,vpointIDsgt(i)+2}] = hessPos.at({segmentID,vpointIDsgt(i)+2,vpointID0()+2});
      
      hessFrames[{segmentID,vpointIDsgt(segmentID-1)+0,vpointIDsgt(i)+0}] = hessFrames.at({segmentID,vpointIDsgt(i)+0,vpointIDsgt(segmentID-1)+0});
      hessFrames[{segmentID,vpointIDsgt(segmentID-1)+1,vpointIDsgt(i)+0}] = hessFrames.at({segmentID,vpointIDsgt(i)+0,vpointIDsgt(segmentID-1)+1});
      hessFrames[{segmentID,vpointIDsgt(segmentID-1)+2,vpointIDsgt(i)+0}] = hessFrames.at({segmentID,vpointIDsgt(i)+0,vpointIDsgt(segmentID-1)+2});
      hessFrames[{segmentID,vpointIDsgt(segmentID-1)+0,vpointIDsgt(i)+1}] = hessFrames.at({segmentID,vpointIDsgt(i)+1,vpointIDsgt(segmentID-1)+0});
      hessFrames[{segmentID,vpointIDsgt(segmentID-1)+1,vpointIDsgt(i)+1}] = hessFrames.at({segmentID,vpointIDsgt(i)+1,vpointIDsgt(segmentID-1)+1});
      hessFrames[{segmentID,vpointIDsgt(segmentID-1)+2,vpointIDsgt(i)+1}] = hessFrames.at({segmentID,vpointIDsgt(i)+1,vpointIDsgt(segmentID-1)+2});
      hessFrames[{segmentID,vpointIDsgt(segmentID-1)+0,vpointIDsgt(i)+2}] = hessFrames.at({segmentID,vpointIDsgt(i)+2,vpointIDsgt(segmentID-1)+0});
      hessFrames[{segmentID,vpointIDsgt(segmentID-1)+1,vpointIDsgt(i)+2}] = hessFrames.at({segmentID,vpointIDsgt(i)+2,vpointIDsgt(segmentID-1)+1});
      hessFrames[{segmentID,vpointIDsgt(segmentID-1)+2,vpointIDsgt(i)+2}] = hessFrames.at({segmentID,vpointIDsgt(i)+2,vpointIDsgt(segmentID-1)+2});
      hessFrames[{segmentID,vpointID0()+0,vpointIDsgt(i)+0}] = hessFrames.at({segmentID,vpointIDsgt(i)+0,vpointID0()+0});
      hessFrames[{segmentID,vpointID0()+1,vpointIDsgt(i)+0}] = hessFrames.at({segmentID,vpointIDsgt(i)+0,vpointID0()+1});
      hessFrames[{segmentID,vpointID0()+2,vpointIDsgt(i)+0}] = hessFrames.at({segmentID,vpointIDsgt(i)+0,vpointID0()+2});
      hessFrames[{segmentID,vpointID0()+0,vpointIDsgt(i)+1}] = hessFrames.at({segmentID,vpointIDsgt(i)+1,vpointID0()+0});
      hessFrames[{segmentID,vpointID0()+1,vpointIDsgt(i)+1}] = hessFrames.at({segmentID,vpointIDsgt(i)+1,vpointID0()+1});
      hessFrames[{segmentID,vpointID0()+2,vpointIDsgt(i)+1}] = hessFrames.at({segmentID,vpointIDsgt(i)+1,vpointID0()+2});
      hessFrames[{segmentID,vpointID0()+0,vpointIDsgt(i)+2}] = hessFrames.at({segmentID,vpointIDsgt(i)+2,vpointID0()+0});
      hessFrames[{segmentID,vpointID0()+1,vpointIDsgt(i)+2}] = hessFrames.at({segmentID,vpointIDsgt(i)+2,vpointID0()+1});
      hessFrames[{segmentID,vpointID0()+2,vpointIDsgt(i)+2}] = hessFrames.at({segmentID,vpointIDsgt(i)+2,vpointID0()+2});
    }
    //i,j non dans le segment
    for(size_t i=0;i<segmentID-1;i++)
    {
      for(size_t j=0;j<segmentID-1;j++)
      {
        hessPos[{segmentID,vpointIDsgt(i)+0,vpointIDsgt(j)+0}]=compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+0,vpointIDsgt(j)+0}))
          +compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointIDsgt(i)+0,vpointIDsgt(j)+0}))
          +sc.getLength(i)*sc.getLength(j)*ext(hs,1,1)
          +sc.getLength(i)*gradFrames.at({segmentID-1,vpointIDsgt(j)+0})*frameIinv*extractDerivation(partSegmentGradPos,1)
          +sc.getLength(j)*gradFrames.at({segmentID-1,vpointIDsgt(i)+0})*frameIinv*extractDerivation(partSegmentGradPos,1);
        
        hessPos[{segmentID,vpointIDsgt(i)+0,vpointIDsgt(j)+1}]=compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+0,vpointIDsgt(j)+1}))
          +compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointIDsgt(i)+0,vpointIDsgt(j)+1}))
          +sc.getLength(i)*sc.getLength(j)*ext(hs,1,3)
          +sc.getLength(i)*gradFrames.at({segmentID-1,vpointIDsgt(j)+1})*frameIinv*extractDerivation(partSegmentGradPos,1)
          +sc.getLength(j)*gradFrames.at({segmentID-1,vpointIDsgt(i)+0})*frameIinv*extractDerivation(partSegmentGradPos,3);
        
        hessPos[{segmentID,vpointIDsgt(i)+0,vpointIDsgt(j)+2}]=compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+0,vpointIDsgt(j)+2}))
          +compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointIDsgt(i)+0,vpointIDsgt(j)+2}))
          +sc.getLength(i)*sc.getLength(j)*ext(hs,1,5)
          +sc.getLength(i)*gradFrames.at({segmentID-1,vpointIDsgt(j)+2})*frameIinv*extractDerivation(partSegmentGradPos,1)
          +sc.getLength(j)*gradFrames.at({segmentID-1,vpointIDsgt(i)+0})*frameIinv*extractDerivation(partSegmentGradPos,5);
          
        hessPos[{segmentID,vpointIDsgt(i)+1,vpointIDsgt(j)+0}]=compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+1,vpointIDsgt(j)+0}))
          +compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointIDsgt(i)+1,vpointIDsgt(j)+0}))
          +sc.getLength(i)*sc.getLength(j)*ext(hs,3,1)
          +sc.getLength(i)*gradFrames.at({segmentID-1,vpointIDsgt(j)+0})*frameIinv*extractDerivation(partSegmentGradPos,3)
          +sc.getLength(j)*gradFrames.at({segmentID-1,vpointIDsgt(i)+1})*frameIinv*extractDerivation(partSegmentGradPos,1);
        
        hessPos[{segmentID,vpointIDsgt(i)+1,vpointIDsgt(j)+1}]=compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+1,vpointIDsgt(j)+1}))
          +compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointIDsgt(i)+1,vpointIDsgt(j)+1}))
          +sc.getLength(i)*sc.getLength(j)*ext(hs,3,3)
          +sc.getLength(i)*gradFrames.at({segmentID-1,vpointIDsgt(j)+1})*frameIinv*extractDerivation(partSegmentGradPos,3)
          +sc.getLength(j)*gradFrames.at({segmentID-1,vpointIDsgt(i)+1})*frameIinv*extractDerivation(partSegmentGradPos,3);
          
        hessPos[{segmentID,vpointIDsgt(i)+1,vpointIDsgt(j)+2}]=compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+1,vpointIDsgt(j)+2}))
          +compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointIDsgt(i)+1,vpointIDsgt(j)+2}))
          +sc.getLength(i)*sc.getLength(j)*ext(hs,3,5)
          +sc.getLength(i)*gradFrames.at({segmentID-1,vpointIDsgt(j)+2})*frameIinv*extractDerivation(partSegmentGradPos,3)
          +sc.getLength(j)*gradFrames.at({segmentID-1,vpointIDsgt(i)+1})*frameIinv*extractDerivation(partSegmentGradPos,5);
          
        hessPos[{segmentID,vpointIDsgt(i)+2,vpointIDsgt(j)+0}]=compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+2,vpointIDsgt(j)+0}))
          +compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointIDsgt(i)+2,vpointIDsgt(j)+0}))
          +sc.getLength(i)*sc.getLength(j)*ext(hs,5,1)
          +sc.getLength(i)*gradFrames.at({segmentID-1,vpointIDsgt(j)+0})*frameIinv*extractDerivation(partSegmentGradPos,5)
          +sc.getLength(j)*gradFrames.at({segmentID-1,vpointIDsgt(i)+2})*frameIinv*extractDerivation(partSegmentGradPos,1);
        
        hessPos[{segmentID,vpointIDsgt(i)+2,vpointIDsgt(j)+1}]=compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+2,vpointIDsgt(j)+1}))
          +compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointIDsgt(i)+2,vpointIDsgt(j)+1}))
          +sc.getLength(i)*sc.getLength(j)*ext(hs,5,3)
          +sc.getLength(i)*gradFrames.at({segmentID-1,vpointIDsgt(j)+1})*frameIinv*extractDerivation(partSegmentGradPos,5)
          +sc.getLength(j)*gradFrames.at({segmentID-1,vpointIDsgt(i)+2})*frameIinv*extractDerivation(partSegmentGradPos,3);
          
        hessPos[{segmentID,vpointIDsgt(i)+2,vpointIDsgt(j)+2}]=compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+2,vpointIDsgt(j)+2}))
          +compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointIDsgt(i)+2,vpointIDsgt(j)+2}))
          +sc.getLength(i)*sc.getLength(j)*ext(hs,5,5)
          +sc.getLength(i)*gradFrames.at({segmentID-1,vpointIDsgt(j)+2})*frameIinv*extractDerivation(partSegmentGradPos,5)
          +sc.getLength(j)*gradFrames.at({segmentID-1,vpointIDsgt(i)+2})*frameIinv*extractDerivation(partSegmentGradPos,5);
          
          
        hessFrames[{segmentID,vpointIDsgt(i)+0,vpointIDsgt(j)+0}]=compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+0,vpointIDsgt(j)+0}))
          +sc.getLength(i)*sc.getLength(j)*ext2(hs2,1,1)
          +sc.getLength(i)*gradFrames.at({segmentID-1,vpointIDsgt(j)+0})*frameIinv*extractDerivation(partSegmentGradFrame,1)
          +sc.getLength(j)*gradFrames.at({segmentID-1,vpointIDsgt(i)+0})*frameIinv*extractDerivation(partSegmentGradFrame,1);
        
        hessFrames[{segmentID,vpointIDsgt(i)+0,vpointIDsgt(j)+1}]=compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+0,vpointIDsgt(j)+1}))
          +sc.getLength(i)*sc.getLength(j)*ext2(hs2,1,3)
          +sc.getLength(i)*gradFrames.at({segmentID-1,vpointIDsgt(j)+1})*frameIinv*extractDerivation(partSegmentGradFrame,1)
          +sc.getLength(j)*gradFrames.at({segmentID-1,vpointIDsgt(i)+0})*frameIinv*extractDerivation(partSegmentGradFrame,3);
        
        hessFrames[{segmentID,vpointIDsgt(i)+0,vpointIDsgt(j)+2}]=compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+0,vpointIDsgt(j)+2}))
          +sc.getLength(i)*sc.getLength(j)*ext2(hs2,1,5)
          +sc.getLength(i)*gradFrames.at({segmentID-1,vpointIDsgt(j)+2})*frameIinv*extractDerivation(partSegmentGradFrame,1)
          +sc.getLength(j)*gradFrames.at({segmentID-1,vpointIDsgt(i)+0})*frameIinv*extractDerivation(partSegmentGradFrame,5);
          
        hessFrames[{segmentID,vpointIDsgt(i)+1,vpointIDsgt(j)+0}]=compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+1,vpointIDsgt(j)+0}))
          +sc.getLength(i)*sc.getLength(j)*ext2(hs2,3,1)
          +sc.getLength(i)*gradFrames.at({segmentID-1,vpointIDsgt(j)+0})*frameIinv*extractDerivation(partSegmentGradFrame,3)
          +sc.getLength(j)*gradFrames.at({segmentID-1,vpointIDsgt(i)+1})*frameIinv*extractDerivation(partSegmentGradFrame,1);
        
        hessFrames[{segmentID,vpointIDsgt(i)+1,vpointIDsgt(j)+1}]=compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+1,vpointIDsgt(j)+1}))
          +sc.getLength(i)*sc.getLength(j)*ext2(hs2,3,3)
          +sc.getLength(i)*gradFrames.at({segmentID-1,vpointIDsgt(j)+1})*frameIinv*extractDerivation(partSegmentGradFrame,3)
          +sc.getLength(j)*gradFrames.at({segmentID-1,vpointIDsgt(i)+1})*frameIinv*extractDerivation(partSegmentGradFrame,3);
          
        hessFrames[{segmentID,vpointIDsgt(i)+1,vpointIDsgt(j)+2}]=compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+1,vpointIDsgt(j)+2}))
          +sc.getLength(i)*sc.getLength(j)*ext2(hs2,3,5)
          +sc.getLength(i)*gradFrames.at({segmentID-1,vpointIDsgt(j)+2})*frameIinv*extractDerivation(partSegmentGradFrame,3)
          +sc.getLength(j)*gradFrames.at({segmentID-1,vpointIDsgt(i)+1})*frameIinv*extractDerivation(partSegmentGradFrame,5);
          
        hessFrames[{segmentID,vpointIDsgt(i)+2,vpointIDsgt(j)+0}]=compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+2,vpointIDsgt(j)+0}))
          +sc.getLength(i)*sc.getLength(j)*ext2(hs2,5,1)
          +sc.getLength(i)*gradFrames.at({segmentID-1,vpointIDsgt(j)+0})*frameIinv*extractDerivation(partSegmentGradFrame,5)
          +sc.getLength(j)*gradFrames.at({segmentID-1,vpointIDsgt(i)+2})*frameIinv*extractDerivation(partSegmentGradFrame,1);
        
        hessFrames[{segmentID,vpointIDsgt(i)+2,vpointIDsgt(j)+1}]=compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+2,vpointIDsgt(j)+1}))
          +sc.getLength(i)*sc.getLength(j)*ext2(hs2,5,3)
          +sc.getLength(i)*gradFrames.at({segmentID-1,vpointIDsgt(j)+1})*frameIinv*extractDerivation(partSegmentGradFrame,5)
          +sc.getLength(j)*gradFrames.at({segmentID-1,vpointIDsgt(i)+2})*frameIinv*extractDerivation(partSegmentGradFrame,3);
          
        hessFrames[{segmentID,vpointIDsgt(i)+2,vpointIDsgt(j)+2}]=compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointIDsgt(i)+2,vpointIDsgt(j)+2}))
          +sc.getLength(i)*sc.getLength(j)*ext2(hs2,5,5)
          +sc.getLength(i)*gradFrames.at({segmentID-1,vpointIDsgt(j)+2})*frameIinv*extractDerivation(partSegmentGradFrame,5)
          +sc.getLength(j)*gradFrames.at({segmentID-1,vpointIDsgt(i)+2})*frameIinv*extractDerivation(partSegmentGradFrame,5);
      }
    }
    
  }
}

void FiberSystem::addGradient( Eigen::VectorXr& grad)
{
  size_t t=sc.getNbSegments();
  for(size_t segmentID=0;segmentID<t;segmentID++)
  {
    Eigen::Vector6r tmp = parts.at(segmentID).gradient();
    grad[vpointIDsgt(segmentID)+0]+=tmp[0];
    grad[vpointIDsgt(segmentID)+1]+=tmp[2];
    grad[vpointIDsgt(segmentID)+2]+=tmp[4];
    
    const Eigen::Matrix3r dEnergydFrameSegment = parts.at(segmentID).getdEnergydFrameOut();
    const Eigen::Vector3r dEnergydPosSegment = parts.at(segmentID).getdEnergydPosOut();
    for(size_t j=0;j<segmentID;j++)
    {
      grad[vpointIDsgt(j)+0]+=sc.getLength(j)*tmp[1];
      grad[vpointIDsgt(j)+1]+=sc.getLength(j)*tmp[3];
      grad[vpointIDsgt(j)+2]+=sc.getLength(j)*tmp[5];
      
      grad[vpointIDsgt(j)+2] += 
        contractGeneralDotP(dEnergydFrameSegment, gradFrames.at({segmentID,vpointIDsgt(j)+2})) 
        + dEnergydPosSegment.dot(gradPos.at({segmentID,vpointIDsgt(j)+2}));
      grad[vpointIDsgt(j)+1] += 
        contractGeneralDotP(dEnergydFrameSegment, gradFrames.at({segmentID,vpointIDsgt(j)+1})) 
        + dEnergydPosSegment.dot(gradPos.at({segmentID,vpointIDsgt(j)+1}));
      grad[vpointIDsgt(j)+0] += 
        contractGeneralDotP(dEnergydFrameSegment, gradFrames.at({segmentID,vpointIDsgt(j)+0})) 
        + dEnergydPosSegment.dot(gradPos.at({segmentID,vpointIDsgt(j)+0}));
    }
  }
  
  
  {
    int t = sc.getNbSegments();
    grad[vpointID0()+0]=0.;
    grad[vpointID0()+1]=0.;
    grad[vpointID0()+2]=0.;
    for (int i = t - 1; i > 0; i--) {
      real dpreEdbSegment = parts.at(i).gradient()[1];
      real dpreEdmSegment = parts.at(i).gradient()[3];
      real dpreEdySegment = parts.at(i).gradient()[5];
      real gradIb = 
        dpreEdbSegment 
        + contractGeneralDotP(parts.at(i).getdEnergydFrameOut(), gradFrames.at({i,vpointID0()+0})) 
        + parts.at(i).getdEnergydPosOut().dot(gradPos.at({i,vpointID0()+0}));
      grad[vpointID0()+0] += gradIb;
      real gradIm = 
        dpreEdmSegment 
        + contractGeneralDotP(parts.at(i).getdEnergydFrameOut(), gradFrames.at({i,vpointID0()+1})) 
        + parts.at(i).getdEnergydPosOut().dot(gradPos.at({i,vpointID0()+1}));
      grad[vpointID0()+1] += gradIm;
      real gradIy = 
        dpreEdySegment 
        + contractGeneralDotP(parts.at(i).getdEnergydFrameOut(), gradFrames.at({i,vpointID0()+2})) 
        + parts.at(i).getdEnergydPosOut().dot(gradPos.at({i,vpointID0()+2}));
      grad[vpointID0()+2] += gradIy;
    }
    grad[vpointID0()+0] += parts.at(0).gradient()[1];
    grad[vpointID0()+1] += parts.at(0).gradient()[3];
    grad[vpointID0()+2] += parts.at(0).gradient()[5];
      
  }
}

void FiberSystem::addHessian(Eigen::MatrixXr& mat)
{
  size_t t=sc.getNbSegments();
  for(size_t segmentID=1;segmentID<=t;segmentID++)
  {
    Eigen::MatrixXr hpart;
    hpart.resizeLike(mat);
    hpart.setZero();
//     const auto partSegmentGradPos = parts.at(segmentID-1).gradPos();
//     const auto partSegmentGradFrame = parts.at(segmentID-1).gradFrame();
    
    //i,j dans le segment
    Eigen::Matrix6r hs = parts.at(segmentID-1).hessian();
    
    hpart(vpointIDsgt(segmentID-1)+2,vpointIDsgt(segmentID-1)+2)=hs(4,4);
    hpart(vpointIDsgt(segmentID-1)+2,vpointIDsgt(segmentID-1)+1)=hs(2,4);
    hpart(vpointIDsgt(segmentID-1)+2,vpointIDsgt(segmentID-1)+0)=hs(0,4);
    hpart(vpointIDsgt(segmentID-1)+1,vpointIDsgt(segmentID-1)+1)=hs(2,2);
    hpart(vpointIDsgt(segmentID-1)+1,vpointIDsgt(segmentID-1)+0)=hs(0,2);
    hpart(vpointIDsgt(segmentID-1)+0,vpointIDsgt(segmentID-1)+0)=hs(0,0);
    if(segmentID==1)
    {
      hpart(vpointID0()+0,vpointID0()+0)=hs(1,1);
      hpart(vpointID0()+1,vpointID0()+0)=hs(1,3);
      hpart(vpointID0()+2,vpointID0()+0)=hs(1,5);
      hpart(vpointID0()+1,vpointID0()+1)=hs(3,3);
      hpart(vpointID0()+2,vpointID0()+1)=hs(3,5);
      hpart(vpointID0()+2,vpointID0()+2)=hs(5,5);
      hpart(vpointID0()+0,vpointIDsgt(segmentID-1)+0)=hs(0,1);
      hpart(vpointID0()+1,vpointIDsgt(segmentID-1)+0)=hs(0,3);
      hpart(vpointID0()+2,vpointIDsgt(segmentID-1)+0)=hs(0,5);
      hpart(vpointIDsgt(segmentID-1)+1,vpointID0()+0)=hs(1,2);
      hpart(vpointID0()+1,vpointIDsgt(segmentID-1)+1)=hs(2,3);
      hpart(vpointID0()+2,vpointIDsgt(segmentID-1)+1)=hs(2,5);
      hpart(vpointIDsgt(segmentID-1)+2,vpointID0()+0)=hs(1,4);
      hpart(vpointIDsgt(segmentID-1)+2,vpointID0()+1)=hs(3,4);
      hpart(vpointID0()+2,vpointIDsgt(segmentID-1)+2)=hs(4,5);
    }
    else
    {
      hpart(vpointID0()+0,vpointID0()+0) = contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointID0()+0,vpointID0()+0}))
           + parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointID0()+0,vpointID0()+0}))
           +hs(1,1)
           +2.0* parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointID0()+0}) ,1);
       
      hpart(vpointID0()+1,vpointID0()+0) = contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointID0()+0,vpointID0()+1}))
           + parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointID0()+0,vpointID0()+1}))
           +hs(1,3)
           +parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointID0()+0}) ,3)
           +parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointID0()+1}) ,1);
           
      hpart(vpointID0()+1,vpointID0()+1) = contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointID0()+1,vpointID0()+1}))
           + parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointID0()+1,vpointID0()+1}))
           +hs(3,3)
           +2.0*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointID0()+1}) ,3);
           
      hpart(vpointID0()+2,vpointID0()+0) = contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointID0()+0,vpointID0()+2}))
           + parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointID0()+0,vpointID0()+2}))
           +hs(1,5)
           +parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointID0()+0}) ,5)
           +parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointID0()+2}) ,1);
      
      hpart(vpointID0()+2,vpointID0()+1) = contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointID0()+1,vpointID0()+2}))
           + parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointID0()+1,vpointID0()+2}))
           +hs(3,5)
           +parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointID0()+1}) ,5)
           +parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointID0()+2}) ,3);
           
      hpart(vpointID0()+2,vpointID0()+2) = contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointID0()+2,vpointID0()+2}))
           + parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointID0()+2,vpointID0()+2}))
           +hs(5,5)
           +2.0*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointID0()+2}) ,5);
           
      hpart(vpointID0()+0,vpointIDsgt(segmentID-1)+0)=hs(0,1)
           +parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointID0()+0}) ,0);
      hpart(vpointID0()+1,vpointIDsgt(segmentID-1)+0)=hs(0,3)
           +parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointID0()+1}) ,0);
      hpart(vpointID0()+2,vpointIDsgt(segmentID-1)+0)=hs(0,5)
           +parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointID0()+2}) ,0);
      hpart(vpointIDsgt(segmentID-1)+1,vpointID0()+0)=hs(1,2)
           +parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointID0()+0}) ,2);
      hpart(vpointID0()+1,vpointIDsgt(segmentID-1)+1)=hs(2,3)
           +parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointID0()+1}) ,2);
      hpart(vpointID0()+2,vpointIDsgt(segmentID-1)+1)=hs(2,5)
           +parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointID0()+2}) ,2);
      hpart(vpointIDsgt(segmentID-1)+2,vpointID0()+0)=hs(1,4)
           +parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointID0()+0}) ,4);
      hpart(vpointIDsgt(segmentID-1)+2,vpointID0()+1)=hs(3,4)
           +parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointID0()+1}) ,4);
      hpart(vpointID0()+2,vpointIDsgt(segmentID-1)+2)=hs(4,5)
           +parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointID0()+2}) ,4);
    }
    
    hpart(vpointIDsgt(segmentID-1)+0, vpointID0()+0)             =hpart(vpointID0()+0,              vpointIDsgt(segmentID-1)+0);
    hpart(vpointIDsgt(segmentID-1)+0, vpointIDsgt(segmentID-1)+1)=hpart(vpointIDsgt(segmentID-1)+1, vpointIDsgt(segmentID-1)+0);
    hpart(vpointIDsgt(segmentID-1)+0, vpointID0()+1)             =hpart(vpointID0()+1,              vpointIDsgt(segmentID-1)+0);
    hpart(vpointIDsgt(segmentID-1)+0, vpointIDsgt(segmentID-1)+2)=hpart(vpointIDsgt(segmentID-1)+2, vpointIDsgt(segmentID-1)+0);
    hpart(vpointIDsgt(segmentID-1)+0, vpointID0()+2)             =hpart(vpointID0()+2,              vpointIDsgt(segmentID-1)+0);
    hpart(vpointID0()+0,              vpointIDsgt(segmentID-1)+1)=hpart(vpointIDsgt(segmentID-1)+1, vpointID0()+0);
    hpart(vpointID0()+0,              vpointID0()+1)             =hpart(vpointID0()+1,              vpointID0()+0);
    hpart(vpointID0()+0,              vpointIDsgt(segmentID-1)+2)=hpart(vpointIDsgt(segmentID-1)+2, vpointID0()+0);
    hpart(vpointID0()+0,              vpointID0()+2)             =hpart(vpointID0()+2,              vpointID0()+0);
    hpart(vpointIDsgt(segmentID-1)+1, vpointID0()+1)             =hpart(vpointID0()+1,              vpointIDsgt(segmentID-1)+1);
    hpart(vpointIDsgt(segmentID-1)+1, vpointIDsgt(segmentID-1)+2)=hpart(vpointIDsgt(segmentID-1)+2, vpointIDsgt(segmentID-1)+1);
    hpart(vpointIDsgt(segmentID-1)+1, vpointID0()+2)             =hpart(vpointID0()+2,              vpointIDsgt(segmentID-1)+1);
    hpart(vpointID0()+1,              vpointIDsgt(segmentID-1)+2)=hpart(vpointIDsgt(segmentID-1)+2, vpointID0()+1);
    hpart(vpointID0()+1,              vpointID0()+2)             =hpart(vpointID0()+2,              vpointID0()+1);
    hpart(vpointIDsgt(segmentID-1)+2, vpointID0()+2)             =hpart(vpointID0()+2,              vpointIDsgt(segmentID-1)+2);
    
    
    //i ou(exclusif) j dans le segment
    for(size_t i=0;i<segmentID-1;i++)
    { 
      hpart(vpointIDsgt(i)+0,vpointIDsgt(segmentID-1)+0) = parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(i)+0}) ,0)
        + sc.getLength(i)* hs(0,1);
      hpart(vpointIDsgt(i)+0,vpointIDsgt(segmentID-1)+1) = parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(i)+0}) ,2)
        + sc.getLength(i)* hs(2,1);
      hpart(vpointIDsgt(i)+0,vpointIDsgt(segmentID-1)+2) = parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(i)+0}) ,4)
        + sc.getLength(i)* hs(4,1);
      hpart(vpointIDsgt(i)+1,vpointIDsgt(segmentID-1)+0) = parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(i)+1}) ,0)
        + sc.getLength(i)* hs(0,3);
      hpart(vpointIDsgt(i)+1,vpointIDsgt(segmentID-1)+1) = parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(i)+1}) ,2)
        + sc.getLength(i)* hs(2,3);
      hpart(vpointIDsgt(i)+1,vpointIDsgt(segmentID-1)+2) = parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(i)+1}) ,4)
        + sc.getLength(i)* hs(4,3);
      hpart(vpointIDsgt(i)+2,vpointIDsgt(segmentID-1)+0) = parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(i)+2}) ,0)
        + sc.getLength(i)* hs(0,5);
      hpart(vpointIDsgt(i)+2,vpointIDsgt(segmentID-1)+1) = parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(i)+2}) ,2)
        + sc.getLength(i)* hs(2,5);
      hpart(vpointIDsgt(i)+2,vpointIDsgt(segmentID-1)+2) = parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(i)+2}) ,4)
        + sc.getLength(i)* hs(4,5);
                                                                  
      hpart(vpointIDsgt(i)+0,vpointID0()+0) = parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(i)+0}) ,1)
        + sc.getLength(i)*hs(1,1) 
        + sc.getLength(i)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointID0()+0}) ,1)
        + contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointIDsgt(i)+0,vpointID0()+0}))
        + parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointIDsgt(i)+0,vpointID0()+0}));
      
      hpart(vpointIDsgt(i)+0,vpointID0()+1) = parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(i)+0}) ,3)
        + sc.getLength(i)*hs(1,3) 
        + sc.getLength(i)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointID0()+1}) ,1)
        + contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointIDsgt(i)+0,vpointID0()+1}))
        + parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointIDsgt(i)+0,vpointID0()+1}));
        
      hpart(vpointIDsgt(i)+0,vpointID0()+2) = parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(i)+0}) ,5)
        + sc.getLength(i)*hs(1,5) 
        + sc.getLength(i)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointID0()+2}) ,1)
        + contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointIDsgt(i)+0,vpointID0()+2}))
        + parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointIDsgt(i)+0,vpointID0()+2}));
        
      hpart(vpointIDsgt(i)+1,vpointID0()+0) = parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(i)+1}) ,1)
        + sc.getLength(i)*hs(3,1) 
        + sc.getLength(i)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointID0()+0}) ,3)
        + contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointIDsgt(i)+1,vpointID0()+0}))
        + parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointIDsgt(i)+1,vpointID0()+0}));
        
      hpart(vpointIDsgt(i)+1,vpointID0()+1) = parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(i)+1}) ,3)
        + sc.getLength(i)*hs(3,3) 
        + sc.getLength(i)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointID0()+1}) ,3)
        + contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointIDsgt(i)+1,vpointID0()+1}))
        + parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointIDsgt(i)+1,vpointID0()+1}));
        
      hpart(vpointIDsgt(i)+1,vpointID0()+2) = parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(i)+1}) ,5)
        + sc.getLength(i)*hs(3,5) 
        + sc.getLength(i)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointID0()+2}) ,3)
        + contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointIDsgt(i)+1,vpointID0()+2}))
        + parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointIDsgt(i)+1,vpointID0()+2}));
        
      hpart(vpointIDsgt(i)+2,vpointID0()+0) = parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(i)+2}) ,1)
        + sc.getLength(i)*hs(5,1) 
        + sc.getLength(i)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointID0()+0}) ,5)
        + contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointIDsgt(i)+2,vpointID0()+0}))
        + parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointIDsgt(i)+2,vpointID0()+0}));
        
      hpart(vpointIDsgt(i)+2,vpointID0()+1) = parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(i)+2}) ,3)
        + sc.getLength(i)*hs(5,3) 
        + sc.getLength(i)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointID0()+1}) ,5)
        + contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointIDsgt(i)+2,vpointID0()+1}))
        + parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointIDsgt(i)+2,vpointID0()+1}));
        
      hpart(vpointIDsgt(i)+2,vpointID0()+2) = parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(i)+2}) ,5)
        + sc.getLength(i)*hs(5,5) 
        + sc.getLength(i)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointID0()+2}) ,5)
        + contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointIDsgt(i)+2,vpointID0()+2}))
        + parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointIDsgt(i)+2,vpointID0()+2}));
                                                          
                                                          
      hpart(vpointIDsgt(segmentID-1)+0,vpointIDsgt(i)+0) = hpart(vpointIDsgt(i)+0,vpointIDsgt(segmentID-1)+0);
      hpart(vpointIDsgt(segmentID-1)+1,vpointIDsgt(i)+0) = hpart(vpointIDsgt(i)+0,vpointIDsgt(segmentID-1)+1);
      hpart(vpointIDsgt(segmentID-1)+2,vpointIDsgt(i)+0) = hpart(vpointIDsgt(i)+0,vpointIDsgt(segmentID-1)+2);
      hpart(vpointIDsgt(segmentID-1)+0,vpointIDsgt(i)+1) = hpart(vpointIDsgt(i)+1,vpointIDsgt(segmentID-1)+0);
      hpart(vpointIDsgt(segmentID-1)+1,vpointIDsgt(i)+1) = hpart(vpointIDsgt(i)+1,vpointIDsgt(segmentID-1)+1);
      hpart(vpointIDsgt(segmentID-1)+2,vpointIDsgt(i)+1) = hpart(vpointIDsgt(i)+1,vpointIDsgt(segmentID-1)+2);
      hpart(vpointIDsgt(segmentID-1)+0,vpointIDsgt(i)+2) = hpart(vpointIDsgt(i)+2,vpointIDsgt(segmentID-1)+0);
      hpart(vpointIDsgt(segmentID-1)+1,vpointIDsgt(i)+2) = hpart(vpointIDsgt(i)+2,vpointIDsgt(segmentID-1)+1);
      hpart(vpointIDsgt(segmentID-1)+2,vpointIDsgt(i)+2) = hpart(vpointIDsgt(i)+2,vpointIDsgt(segmentID-1)+2);
      hpart(vpointID0()+0,vpointIDsgt(i)+0) = hpart(vpointIDsgt(i)+0,vpointID0()+0);
      hpart(vpointID0()+1,vpointIDsgt(i)+0) = hpart(vpointIDsgt(i)+0,vpointID0()+1);
      hpart(vpointID0()+2,vpointIDsgt(i)+0) = hpart(vpointIDsgt(i)+0,vpointID0()+2);
      hpart(vpointID0()+0,vpointIDsgt(i)+1) = hpart(vpointIDsgt(i)+1,vpointID0()+0);
      hpart(vpointID0()+1,vpointIDsgt(i)+1) = hpart(vpointIDsgt(i)+1,vpointID0()+1);
      hpart(vpointID0()+2,vpointIDsgt(i)+1) = hpart(vpointIDsgt(i)+1,vpointID0()+2);
      hpart(vpointID0()+0,vpointIDsgt(i)+2) = hpart(vpointIDsgt(i)+2,vpointID0()+0);
      hpart(vpointID0()+1,vpointIDsgt(i)+2) = hpart(vpointIDsgt(i)+2,vpointID0()+1);
      hpart(vpointID0()+2,vpointIDsgt(i)+2) = hpart(vpointIDsgt(i)+2,vpointID0()+2);
      
    }
    //i,j non dans le segment
    for(size_t i=0;i<segmentID-1;i++)
    {
      for(size_t j=0;j<segmentID-1;j++)
      {
        hpart(vpointIDsgt(i)+0,vpointIDsgt(j)+0)=contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointIDsgt(i)+0,vpointIDsgt(j)+0}))
          +parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointIDsgt(i)+0,vpointIDsgt(j)+0}))
          +sc.getLength(i)*sc.getLength(j)*hs(1,1)
          +sc.getLength(i)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(j)+0}) ,1)
          +sc.getLength(j)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(i)+0}) ,1);
        
        hpart(vpointIDsgt(i)+0,vpointIDsgt(j)+1)=contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointIDsgt(i)+0,vpointIDsgt(j)+1}))
          +parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointIDsgt(i)+0,vpointIDsgt(j)+1}))
          +sc.getLength(i)*sc.getLength(j)*hs(1,3)
          +sc.getLength(i)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(j)+1}) ,1)
          +sc.getLength(j)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(i)+0}) ,3);
          
        hpart(vpointIDsgt(i)+0,vpointIDsgt(j)+2)=contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointIDsgt(i)+0,vpointIDsgt(j)+2}))
          +parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointIDsgt(i)+0,vpointIDsgt(j)+2}))
          +sc.getLength(i)*sc.getLength(j)*hs(1,5)
          +sc.getLength(i)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(j)+2}) ,1)
          +sc.getLength(j)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(i)+0}) ,5);
        
        hpart(vpointIDsgt(i)+1,vpointIDsgt(j)+0)=contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointIDsgt(i)+1,vpointIDsgt(j)+0}))
          +parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointIDsgt(i)+1,vpointIDsgt(j)+0}))
          +sc.getLength(i)*sc.getLength(j)*hs(3,1)
          +sc.getLength(i)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(j)+0}) ,3)
          +sc.getLength(j)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(i)+1}) ,1);
        
        hpart(vpointIDsgt(i)+1,vpointIDsgt(j)+1)=contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointIDsgt(i)+1,vpointIDsgt(j)+1}))
          +parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointIDsgt(i)+1,vpointIDsgt(j)+1}))
          +sc.getLength(i)*sc.getLength(j)*hs(3,3)
          +sc.getLength(i)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(j)+1}) ,3)
          +sc.getLength(j)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(i)+1}) ,3);
        
        hpart(vpointIDsgt(i)+1,vpointIDsgt(j)+2)=contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointIDsgt(i)+1,vpointIDsgt(j)+2}))
          +parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointIDsgt(i)+1,vpointIDsgt(j)+2}))
          +sc.getLength(i)*sc.getLength(j)*hs(3,5)
          +sc.getLength(i)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(j)+2}) ,3)
          +sc.getLength(j)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(i)+1}) ,5);
          
        hpart(vpointIDsgt(i)+2,vpointIDsgt(j)+0)=contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointIDsgt(i)+2,vpointIDsgt(j)+0}))
          +parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointIDsgt(i)+2,vpointIDsgt(j)+0}))
          +sc.getLength(i)*sc.getLength(j)*hs(5,1)
          +sc.getLength(i)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(j)+0}) ,5)
          +sc.getLength(j)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(i)+2}) ,1);
        
        hpart(vpointIDsgt(i)+2,vpointIDsgt(j)+1)=contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointIDsgt(i)+2,vpointIDsgt(j)+1}))
          +parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointIDsgt(i)+2,vpointIDsgt(j)+1}))
          +sc.getLength(i)*sc.getLength(j)*hs(5,3)
          +sc.getLength(i)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(j)+1}) ,5)
          +sc.getLength(j)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(i)+2}) ,3);
        
        hpart(vpointIDsgt(i)+2,vpointIDsgt(j)+2)=contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointIDsgt(i)+2,vpointIDsgt(j)+2}))
          +parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointIDsgt(i)+2,vpointIDsgt(j)+2}))
          +sc.getLength(i)*sc.getLength(j)*hs(5,5)
          +sc.getLength(i)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(j)+2}) ,5)
          +sc.getLength(j)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDsgt(i)+2}) ,5);
      }
    }
    mat+=hpart;
  }
}

Eigen::Vector3r FiberSystem::getEndPos()
{
  return parts.back().endPosFrame().first;
}

Eigen::Matrix3r FiberSystem::getEndFrame()
{
  return parts.back().endPosFrame().second;
}
