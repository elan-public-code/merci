/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef FIBERSYSTEM_H
#define FIBERSYSTEM_H

#include "CSegmentSystem.hpp"
#include "SuperClothoid.hpp"
#include <tuple>

class FiberSystem
{
public:
  FiberSystem(Eigen::Vector3r gravity);
  void set(const SuperClothoid& sr);
  SuperClothoid get() const;
  void setPoint(const Eigen::VectorXr& x);
  Eigen::VectorXr getPoint() const;
  
  size_t vpointSize() const;
  constexpr size_t vpointID0() const;
  constexpr size_t vpointIDsgt( const size_t& segmentID ) const;
  
  real energy();
  Eigen::VectorXr gradient();
  Eigen::MatrixXr hessian();
  
  Eigen::Vector3r getEndPos();
  Eigen::Matrix3r getEndFrame();
  
  //WARNING just for test purposes
  real posX();
  real frameX();
  Eigen::MatrixXr hessPosX();
  Eigen::VectorXr gradPosX();
  Eigen::MatrixXr hessFrameX();
  Eigen::VectorXr gradFrameX();
private:
  std::vector<CSegmentSystem> parts;
  SuperClothoid sc;
  Eigen::Vector3r G;
  
  void updateParts();
  void computeGradPosFrames();
  void computeHessPosFrames();
  void addGradient(Eigen::VectorXr& grad);
  void addHessian(Eigen::MatrixXr& mat);
  
  std::map<std::pair<size_t,size_t>, Eigen::Vector3r> gradPos;
  std::map<std::pair<size_t,size_t>, Eigen::Matrix3r> gradFrames;
  std::map<std::tuple<size_t,size_t,size_t>, Eigen::Vector3r> hessPos;
  std::map<std::tuple<size_t,size_t,size_t>, Eigen::Matrix3r> hessFrames;
};

#endif // FIBERSYSTEM_H
