/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "SCStaticProblem.hpp"

SCStaticProblem::SCStaticProblem(FiberSystem sys) : cppoptlib::Problem<real>(),
    sys(new FiberSystem(sys))
{
}

real SCStaticProblem::value(const cppoptlib::Problem<double, -1>::TVector& pt)
{
  sys->setPoint(pt);
  return sys->energy();
}

void SCStaticProblem::gradient(const cppoptlib::Problem<double, -1>::TVector& pt, cppoptlib::Problem<double, -1>::TVector& res)
{
  sys->setPoint(pt);
  res=sys->gradient();
}

void SCStaticProblem::hessian(const cppoptlib::Problem<double, -1>::TVector& pt, cppoptlib::Problem<double, -1>::THessian& res)
{
  sys->setPoint(pt);
  res=sys->hessian();
}

void SCStaticProblem::setPoint(const cppoptlib::Problem<double, -1>::TVector& pt)
{
  sys->setPoint(pt);
}

cppoptlib::Problem<double, -1>::TVector SCStaticProblem::getPoint()
{
  return sys->getPoint();
}

SuperClothoid SCStaticProblem::get()
{
  return sys->get();
}
