/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef SCSTATICPROBLEM_H
#define SCSTATICPROBLEM_H

#include <cppoptlib/meta.h>
#include <cppoptlib/problem.h>
#include <memory>
#include "FiberSystem.hpp"

class SCStaticProblem  : public cppoptlib::Problem<real>
{
public:
  using typename cppoptlib::Problem<real>::Scalar;
  using typename cppoptlib::Problem<real>::TVector;
  using typename cppoptlib::Problem<real>::THessian;
  SCStaticProblem(FiberSystem sys);
  
  virtual real value(const TVector &pt) override;
  virtual void gradient(const TVector &pt, TVector &res) override;
  virtual void hessian(const TVector &x, THessian &hessian) override;
  //virtual bool callback(const cppoptlib::Criteria<Scalar> &state, const TVector &x) override;
  void setPoint(const TVector &pt);
  TVector getPoint();
  SuperClothoid get();
private:
  std::unique_ptr<FiberSystem> sys;
};

#endif // SCSTATICPROBLEM_H
