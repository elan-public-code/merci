/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "SuperClothoid.hpp"
#include "CElasticEnergy.hpp"

SuperClothoid::SuperClothoid()
{
  posOr.setZero();
  frameOr.setIdentity();
}

void SuperClothoid::setPosOr(Eigen::Vector3r pos){ posOr = pos;}
void SuperClothoid::setFrameOr(Eigen::Matrix3r frame){ frameOr = frame;}

Eigen::Vector3r SuperClothoid::getPosOr() const{ return posOr;}
Eigen::Matrix3r SuperClothoid::getFrameOr() const{ return frameOr;}

size_t SuperClothoid::getNbSegments() const{ return segments.size();}

void SuperClothoid::updateSegments(size_t from)
{
  size_t t = segments.size();
  from = std::max(size_t(1), from);
  for (size_t i = from; i < t; i++)
  {
    segments[i].omega1.B = segments[i - 1].omega1.B + segments[i - 1].length * segments[i - 1].omega1.A;
    segments[i].omega2.B = segments[i - 1].omega2.B + segments[i - 1].length * segments[i - 1].omega2.A;
    segments[i].omega3.B = segments[i - 1].omega3.B + segments[i - 1].length * segments[i - 1].omega3.A;
    segments[i].naturalOmega1.B = segments[i - 1].naturalOmega1.B + segments[i - 1].length * segments[i - 1].naturalOmega1.A;
    segments[i].naturalOmega2.B = segments[i - 1].naturalOmega2.B + segments[i - 1].length * segments[i - 1].naturalOmega2.A;
    segments[i].naturalOmega3.B = segments[i - 1].naturalOmega3.B + segments[i - 1].length * segments[i - 1].naturalOmega3.A;
  }
}

void SuperClothoid::setOmegaInit(double w1, double w2, double w3)
{
  segments[0].omega1.B=w1;
  segments[0].omega2.B=w2;
  segments[0].omega3.B=w3;
  updateSegments(1);
}

void SuperClothoid::setNaturalOmegaInit(double nw1, double nw2, double nw3)
{
  segments[0].naturalOmega1.B=nw1;
  segments[0].naturalOmega2.B=nw2;
  segments[0].naturalOmega3.B=nw3;
  updateSegments(1);
}

void SuperClothoid::setOmegaPrime(int segment, double w1p, double w2p, double w3p)
{
  segments.at(segment).omega1.A=w1p;
  segments.at(segment).omega2.A=w2p;
  segments.at(segment).omega3.A=w3p;
  updateSegments(segment + 1);
}

void SuperClothoid::setNaturalOmegaPrime(int segment, double nw1p, double nw2p, double nw3p)
{
  segments.at(segment).naturalOmega1.A=nw1p;
  segments.at(segment).naturalOmega2.A=nw2p;
  segments.at(segment).naturalOmega3.A=nw3p;
  updateSegments(segment + 1);
}

real SuperClothoid::getLength(int segment) const{ return segments.at(segment).length;}

LinearScalar SuperClothoid::getOmega1(int segment) const{ return segments.at(segment).omega1;}
LinearScalar SuperClothoid::getOmega2(int segment) const{ return segments.at(segment).omega2;}
LinearScalar SuperClothoid::getOmega3(int segment) const{ return segments.at(segment).omega3;}
LinearScalar SuperClothoid::getNaturalOmega1(int segment) const{ return segments.at(segment).naturalOmega1;}
LinearScalar SuperClothoid::getNaturalOmega2(int segment) const{ return segments.at(segment).naturalOmega2;}
LinearScalar SuperClothoid::getNaturalOmega3(int segment) const{ return segments.at(segment).naturalOmega3;}


void SuperClothoid::addSegmentAt(int idx)
{
  segments.insert(segments.begin() + idx, SuperClothoidSegment());
  updateSegments(idx);
}

void SuperClothoid::deleteSegment(int idx)
{
  segments.erase(segments.begin() + idx);
  updateSegments(idx - 1);
}

void SuperClothoid::setLength(int segment, real l)
{
  segments.at(segment).length=l;
  updateSegments(segment + 1);
}

Eigen::Matrix3r SuperClothoid::getK() const { return K; }
real SuperClothoid::getLinearDensity() const { return linearDensity; }
Eigen::Vector2r SuperClothoid::getCrossSection() const { return Eigen::Vector2r(width, thickness);}

void SuperClothoid::setLinearDensity(real ad) { linearDensity = ad; }
void SuperClothoid::setElpiticSection(real width, real height){ this->width = width; this->thickness = height; }
void SuperClothoid::setElasticMatrix(Eigen::Matrix3r K) { this->K=K; }
void SuperClothoid::setElasticMatrix(double YoungModulus, double poissonRatio, double width, double thickness) { this->K=CElasticEnergy::ParamsToK(YoungModulus,poissonRatio,width,thickness); }

