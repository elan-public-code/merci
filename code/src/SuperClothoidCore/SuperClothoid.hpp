/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef SUPERCLOTHOID_H
#define SUPERCLOTHOID_H

#include <BaseCore/real.hpp>
#include <BaseCore/LinearScalar.hpp>
#include <Eigen/Dense>
#include <vector>
#include <optional>

class SuperClothoid
{
public:
  SuperClothoid();
  void deleteSegment(int idx);
  void addSegmentAt(int idx);
  
  void setPosOr(Eigen::Vector3r pos);
  void setFrameOr(Eigen::Matrix3r frame);
  
  void setOmegaInit(double w1, double w2, double w3);
  void setNaturalOmegaInit(double nw1, double nw2, double nw3);
  void setOmegaPrime(int segment, double w1p, double w2p, double w3p);
  void setNaturalOmegaPrime(int segment, double nw1p, double nw2p, double nw3p);
  
  void setLength(int segment, real l);
  
  void setElpiticSection(real width, real height);
  void setLinearDensity(real ad);
  void setElasticMatrix(Eigen::Matrix3r K);
  void setElasticMatrix(double YoungModulus, double poissonRatio, double width, double thickness);

  LinearScalar getOmega1(int segment) const;
  LinearScalar getOmega2(int segment) const;
  LinearScalar getOmega3(int segment) const;
  LinearScalar getNaturalOmega1(int segment) const;
  LinearScalar getNaturalOmega2(int segment) const;
  LinearScalar getNaturalOmega3(int segment) const;
  real getLength(int segment) const;

  size_t getNbSegments() const;
  Eigen::Vector3r getPosOr() const;
  Eigen::Matrix3r getFrameOr() const;
  Eigen::Vector2r getCrossSection() const;
  real getLinearDensity() const;
  Eigen::Matrix3r getK() const;
private:
  struct SuperClothoidSegment {
    LinearScalar omega1;
    LinearScalar omega2;
    LinearScalar omega3;
    LinearScalar naturalOmega1;
    LinearScalar naturalOmega2;
    LinearScalar naturalOmega3;
    real length = 0.;
  };
  Eigen::Vector3r posOr;
  Eigen::Matrix3r frameOr;
  real width = 0.01;
  real thickness = 0.01;
  Eigen::Matrix3r K;
  real linearDensity = 0.1;
  std::vector<SuperClothoidSegment> segments;
  void updateSegments(size_t from);
};

#endif // SUPERCLOTHOID_H
