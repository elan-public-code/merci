/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "SuperClothoidGeometrieSerie.hpp"
#include "Log/Logger.hpp"

Eigen::Matrix3r SuperClothoidGeometrieSerie::skewOf(Eigen::Vector3r w)
{
  Eigen::Matrix3r res;
  res << 0,    -w[2], w[1],
      w[2],  0,    -w[0],
      -w[1], w[0], 0;
  return res;
}

void SuperClothoidGeometrieSerie::Place(Eigen::Vector3r pos, Eigen::Matrix3r frame)
{
  called.clear();
  posOr = pos;
  frameOr = frame;
}

void SuperClothoidGeometrieSerie::Setup(LinearScalar omega1, LinearScalar omega2, LinearScalar omega3)
{
  called.clear();
  lambda0 = Eigen::Vector3r(omega1.B, omega2.B, omega3.B);
  lambda1 = Eigen::Vector3r(omega1.A, omega2.A, omega3.A);
  lambda0Sq = skewOf(Eigen::Vector3r(omega1.B, omega2.B, omega3.B));
  lambda1Sq = skewOf(Eigen::Vector3r(omega1.A, omega2.A, omega3.A));
  lambda = std::max(lambda0.lpNorm<Eigen::Infinity>(),lambda1.lpNorm<Eigen::Infinity>());
  this->omega1=omega1;
  this->omega2=omega2;
  this->omega3=omega3;
}

real SuperClothoidGeometrieSerie::estimRemainderOfTheSerie(int order, real s)
{
  assert(s > 0);
  assert(order > 3);
  return std::exp(C(2 * s)) / pow(2., order - 1);
}

real SuperClothoidGeometrieSerie::factorial(int k)
{
  assert(k >= 1);
  real res = 1.;
  for (int i = 2; i <= k; i++)
    res *= static_cast<real>(i);
  return res;
}

int SuperClothoidGeometrieSerie::estimNeededOrder(real prec)
{
  int a = static_cast<int>(std::log2(prec) - 0.95);
  assert(a < 0);
  return 52 - a;
}

int SuperClothoidGeometrieSerie::estimNeededOrder()
{
  return (52 * 3 / 2);
}

real SuperClothoidGeometrieSerie::C(real s)
{
  return 2.*std::abs(s) *
         (
           lambda0.lpNorm<Eigen::Infinity>()
           + (s * lambda1).lpNorm<Eigen::Infinity>()
         );
}

real SuperClothoidGeometrieSerie::estimSMax()
{
  real T_2 = (52. / 2.) * std::log(2) / 2.;
  real l0 = lambda0.lpNorm<Eigen::Infinity>();
  real l1 = lambda1.lpNorm<Eigen::Infinity>();
  l0 = std::max(static_cast<real>(1.), l0);
  l1 = std::max(static_cast<real>(1.), l1);

  if (std::isnan(l0) || std::isnan(l1)) {
    return std::numeric_limits<real>::infinity();
  }

  real res = std::numeric_limits<real>::infinity();
  //TODO search a better solution
  if (l0 + l1 < T_2) { //Smax>1
    res = std::cbrt(T_2 / (l0 + l1));
  } else { //Smax<1
    res = T_2 / (l0 + l1);
  }

  if (res < 1.e-8) {
    LoggerMsg message("Very small smax");
    message.addChild("smax", std::to_string(res));
    message.addChild("l0", std::to_string(l0));
    message.addChild("l1", std::to_string(l1));
    getLogger()->Write(LoggerLevel::WARNING, "Serie", message);
    res = std::numeric_limits<real>::infinity();
  }

  return res / 2.;
}

void SuperClothoidGeometrieSerie::computeSeriePreTerms()
{
  computeSeriePreTerms(estimNeededOrder());
}

void SuperClothoidGeometrieSerie::computeSeriePreTerms(int order)
{
  if (called["computeSeriePreTerms"])return;
  assert(order > 3);
  FramePreTerms.clear();
  FramePreTerms.resize(order+1);
  FramePreTerms[0]=Eigen::Matrix3r::Identity();
  FramePreTerms[1]=lambda0Sq;
  for (int n = 2; n <= order; n++) {
    FramePreTerms[n]=(1. / n) *
                            (
                              FramePreTerms[n - 1]*lambda0Sq
                              + FramePreTerms[n - 2]*lambda1Sq
                            );
  }
  PosPreTerms.resize(order+1);
  PosPreTerms[0]=Eigen::Vector3r(NAN, NAN, NAN);
  for (int n = 1; n <= order; n++) {
    PosPreTerms[n]=(1. / n) * (
                            FramePreTerms[n - 1].col(2));
  }
  called["computeSeriePreTerms"] = true;
}

Eigen::Matrix3r SuperClothoidGeometrieSerie::Frame_s(real s)
{
  assert(s <= estimSMax());
  return Frame(s);
}

Eigen::Vector3r SuperClothoidGeometrieSerie::Pos_s(real s)
{
  assert(s <= estimSMax());
  return Pos(s);
}

Eigen::Matrix3r SuperClothoidGeometrieSerie::Frame(real s)
{
  return frameOr * PreFrame(s);
}

Eigen::Matrix3r SuperClothoidGeometrieSerie::PreFrame(real s)
{
    if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  int order = FramePreTerms.size();
  computeSPowers(s);
  Eigen::Matrix3r res = FramePreTerms[0];
  for (int i = 1; i < order; i++)
    res += spowers[i] * FramePreTerms[i];
  return res;
}


Eigen::Vector3r SuperClothoidGeometrieSerie::Pos(real s)
{
  computeSPowers(s);
  Eigen::Vector3r res = frameOr * PreTranslation(s);
  res += posOr;
  return res;
}

Eigen::Vector3r SuperClothoidGeometrieSerie::PreTranslation(real s)
{
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  int order = PosPreTerms.size();
  computeSPowers(s);
  Eigen::Vector3r res(0., 0., 0.);
  for (int i = 1; i < order; i++)
    res += spowers[i] * PosPreTerms[i];
  return res;
}

std::pair<Eigen::Vector3r, Eigen::Matrix3r> SuperClothoidGeometrieSerie::PosFrame(real s)
{
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  int order = FramePreTerms.size();
  computeSPowers(s);
  Eigen::Vector3r resPos(0., 0., 0.);
  for (int i = 1; i < order; i++)
    resPos += spowers[i] * PosPreTerms[i];
  resPos = frameOr * resPos;
  resPos += posOr;
  Eigen::Matrix3r resFrame = FramePreTerms[0];
  for (int i = 1; i < order; i++) {
    resFrame += spowers[i] * FramePreTerms[i];
  }
  return std::make_pair(resPos, frameOr * resFrame);
}

void SuperClothoidGeometrieSerie::computeDerivationsPreTerms()
{
  if (called["computeDerivationsPreTerms"])return;
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  ex<<1.,0.,0.;
  ey<<0.,1.,0.;
  ez<<0.,0.,1.;
  exSq=skewOf(ex);
  eySq=skewOf(ey);
  ezSq=skewOf(ez);

  int order = FramePreTerms.size();
  assert(order > 0);

  //Derivation of the frame
  {
    dFramePreTermsdw1A.resize(order);
    dFramePreTermsdw1B.resize(order);
    dFramePreTermsdw2A.resize(order);
    dFramePreTermsdw2B.resize(order);
    dFramePreTermsdw3A.resize(order);
    dFramePreTermsdw3B.resize(order);
    
    Eigen::Matrix3r zero;
    zero.setZero();
    //order 0
    dFramePreTermsdw1A[0]=zero;
    dFramePreTermsdw1B[0]=zero;
    dFramePreTermsdw2A[0]=zero;
    dFramePreTermsdw2B[0]=zero;
    dFramePreTermsdw3A[0]=zero;
    dFramePreTermsdw3B[0]=zero;
    //order 1
    dFramePreTermsdw1A[1]=zero;
    dFramePreTermsdw1B[1]=exSq;
    dFramePreTermsdw2A[1]=zero;
    dFramePreTermsdw2B[1]=eySq;
    dFramePreTermsdw3A[1]=zero;
    dFramePreTermsdw3B[1]=ezSq;
    //order 2
    //order n
    for (int n = 2; n < order; n++) {
      dFramePreTermsdw1A[n]=((1. / static_cast<real>(n)) * (
                                   FramePreTerms[n - 2]*exSq 
                                   + dFramePreTermsdw1A[n - 1]*lambda0Sq + dFramePreTermsdw1A[n - 2]*lambda1Sq));
      dFramePreTermsdw1B[n]=((1. / static_cast<real>(n)) * (
                                   FramePreTerms[n - 1]*exSq 
                                   + dFramePreTermsdw1B[n - 1]*lambda0Sq + dFramePreTermsdw1B[n - 2]*lambda1Sq));
      dFramePreTermsdw2A[n]=((1. / static_cast<real>(n)) * (
                                   FramePreTerms[n - 2]*eySq 
                                   + dFramePreTermsdw2A[n - 1]*lambda0Sq + dFramePreTermsdw2A[n - 2]*lambda1Sq));
      dFramePreTermsdw2B[n]=((1. / static_cast<real>(n)) * (
                                   FramePreTerms[n - 1]*eySq 
                                   + dFramePreTermsdw2B[n - 1]*lambda0Sq + dFramePreTermsdw2B[n - 2]*lambda1Sq));
      dFramePreTermsdw3A[n]=((1. / static_cast<real>(n)) * (
                                   FramePreTerms[n - 2]*ezSq 
                                   + dFramePreTermsdw3A[n - 1]*lambda0Sq + dFramePreTermsdw3A[n - 2]*lambda1Sq));
      dFramePreTermsdw3B[n]=((1. / static_cast<real>(n)) * (
                                   FramePreTerms[n - 1]*ezSq 
                                   + dFramePreTermsdw3B[n - 1]*lambda0Sq + dFramePreTermsdw3B[n - 2]*lambda1Sq));
    }
  }

  //Derivation of the position
  {
    dPosPreTermsdw1A.resize(order);
    dPosPreTermsdw1B.resize(order);
    dPosPreTermsdw2A.resize(order);
    dPosPreTermsdw2B.resize(order);
    dPosPreTermsdw3A.resize(order);
    dPosPreTermsdw3B.resize(order);

    Eigen::Vector3r zero;
    zero.setZero();
    Eigen::Vector3r vnan(NAN, NAN, NAN);

    //order 0
    dPosPreTermsdw1A[0]=vnan;
    dPosPreTermsdw1B[0]=vnan;
    dPosPreTermsdw2A[0]=vnan;
    dPosPreTermsdw2B[0]=vnan;
    dPosPreTermsdw3A[0]=vnan;
    dPosPreTermsdw3B[0]=vnan;
    //order 1
    dPosPreTermsdw1A[1]=zero;
    dPosPreTermsdw1B[1]=zero;
    dPosPreTermsdw2A[1]=zero;
    dPosPreTermsdw2B[1]=zero;
    dPosPreTermsdw3A[1]=zero;
    dPosPreTermsdw3B[1]=zero;
    //order n
    for (int n = 2; n < order; n++) {
      dPosPreTermsdw1A[n]=(1. / static_cast<real>(n))*dFramePreTermsdw1A[n - 1].col(2);
      dPosPreTermsdw1B[n]=(1. / static_cast<real>(n))*dFramePreTermsdw1B[n - 1].col(2);
      dPosPreTermsdw2A[n]=(1. / static_cast<real>(n))*dFramePreTermsdw2A[n - 1].col(2);
      dPosPreTermsdw2B[n]=(1. / static_cast<real>(n))*dFramePreTermsdw2B[n - 1].col(2);
      dPosPreTermsdw3A[n]=(1. / static_cast<real>(n))*dFramePreTermsdw3A[n - 1].col(2);
      dPosPreTermsdw3B[n]=(1. / static_cast<real>(n))*dFramePreTermsdw3B[n - 1].col(2);
    }
  }
  called["computeDerivationsPreTerms"] = true;
}

void SuperClothoidGeometrieSerie::computeDerivationsPreTermsSecondOrder()
{
  if (called["computeDerivationsPreTermsSecondOrder"])return;
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();

  int order = FramePreTerms.size();
  assert(order > 0);

  //Derivation of the frame
  {
    dFramePreTermsdw1Aw1A.resize(order);
    dFramePreTermsdw1Aw1B.resize(order);
    dFramePreTermsdw1Aw2A.resize(order);
    dFramePreTermsdw1Aw2B.resize(order);
    dFramePreTermsdw1Aw3A.resize(order);
    dFramePreTermsdw1Aw3B.resize(order);
    dFramePreTermsdw1Bw1B.resize(order);
    dFramePreTermsdw1Bw2A.resize(order);
    dFramePreTermsdw1Bw2B.resize(order);
    dFramePreTermsdw1Bw3A.resize(order);
    dFramePreTermsdw1Bw3B.resize(order);
    dFramePreTermsdw2Aw2A.resize(order);
    dFramePreTermsdw2Aw2B.resize(order);
    dFramePreTermsdw2Aw3A.resize(order);
    dFramePreTermsdw2Aw3B.resize(order);
    dFramePreTermsdw2Bw2B.resize(order);
    dFramePreTermsdw2Bw3A.resize(order);
    dFramePreTermsdw2Bw3B.resize(order);
    dFramePreTermsdw3Aw3A.resize(order);
    dFramePreTermsdw3Aw3B.resize(order);
    dFramePreTermsdw3Bw3B.resize(order);
    Eigen::Matrix3r zero;
    zero.setZero();
    //order 0
    dFramePreTermsdw1Aw1A[0]=zero;
    dFramePreTermsdw1Aw1B[0]=zero;
    dFramePreTermsdw1Aw2A[0]=zero;
    dFramePreTermsdw1Aw2B[0]=zero;
    dFramePreTermsdw1Aw3A[0]=zero;
    dFramePreTermsdw1Aw3B[0]=zero;
    dFramePreTermsdw1Bw1B[0]=zero;
    dFramePreTermsdw1Bw2A[0]=zero;
    dFramePreTermsdw1Bw2B[0]=zero;
    dFramePreTermsdw1Bw3A[0]=zero;
    dFramePreTermsdw1Bw3B[0]=zero;
    dFramePreTermsdw2Aw2A[0]=zero;
    dFramePreTermsdw2Aw2B[0]=zero;
    dFramePreTermsdw2Aw3A[0]=zero;
    dFramePreTermsdw2Aw3B[0]=zero;
    dFramePreTermsdw2Bw2B[0]=zero;
    dFramePreTermsdw2Bw3A[0]=zero;
    dFramePreTermsdw2Bw3B[0]=zero;
    dFramePreTermsdw3Aw3A[0]=zero;
    dFramePreTermsdw3Aw3B[0]=zero;
    dFramePreTermsdw3Bw3B[0]=zero;
    //order 1
    dFramePreTermsdw1Aw1A[1]=zero;
    dFramePreTermsdw1Aw1B[1]=zero;
    dFramePreTermsdw1Aw2A[1]=zero;
    dFramePreTermsdw1Aw2B[1]=zero;
    dFramePreTermsdw1Aw3A[1]=zero;
    dFramePreTermsdw1Aw3B[1]=zero;
    dFramePreTermsdw1Bw1B[1]=zero;
    dFramePreTermsdw1Bw2A[1]=zero;
    dFramePreTermsdw1Bw2B[1]=zero;
    dFramePreTermsdw1Bw3A[1]=zero;
    dFramePreTermsdw1Bw3B[1]=zero;
    dFramePreTermsdw2Aw2A[1]=zero;
    dFramePreTermsdw2Aw2B[1]=zero;
    dFramePreTermsdw2Aw3A[1]=zero;
    dFramePreTermsdw2Aw3B[1]=zero;
    dFramePreTermsdw2Bw2B[1]=zero;
    dFramePreTermsdw2Bw3A[1]=zero;
    dFramePreTermsdw2Bw3B[1]=zero;
    dFramePreTermsdw3Aw3A[1]=zero;
    dFramePreTermsdw3Aw3B[1]=zero;
    dFramePreTermsdw3Bw3B[1]=zero;
    //order n
    for (int n = 2; n < order; n++) {
      dFramePreTermsdw1Aw1A[n]=((1. / static_cast<real>(n)) * (
        2.0* dFramePreTermsdw1A[n - 2]*exSq 
        + dFramePreTermsdw1Aw1A[n - 1]*lambda0Sq + dFramePreTermsdw1Aw1A[n - 2]*lambda1Sq));
      dFramePreTermsdw1Aw1B[n]= ((1. / static_cast<real>(n)) * (
        dFramePreTermsdw1A[n - 1]*exSq  + dFramePreTermsdw1B[n - 2]*exSq
        + dFramePreTermsdw1Aw1B[n - 1]*lambda0Sq + dFramePreTermsdw1Aw1B[n - 2]*lambda1Sq));
      dFramePreTermsdw1Aw2A[n]=((1. / static_cast<real>(n)) * (
        dFramePreTermsdw1A[n - 2]*eySq  + dFramePreTermsdw2A[n - 2]*exSq
        + dFramePreTermsdw1Aw2A[n - 1]*lambda0Sq + dFramePreTermsdw1Aw2A[n - 2]*lambda1Sq));
      dFramePreTermsdw1Aw2B[n]=((1. / static_cast<real>(n)) * (
        dFramePreTermsdw1A[n - 1]*eySq  + dFramePreTermsdw2B[n - 2]*exSq
        + dFramePreTermsdw1Aw2B[n - 1]*lambda0Sq + dFramePreTermsdw1Aw2B[n - 2]*lambda1Sq));
      dFramePreTermsdw1Aw3A[n]=((1. / static_cast<real>(n)) * (
        dFramePreTermsdw1A[n - 2]*ezSq  + dFramePreTermsdw3A[n - 2]*exSq
        + dFramePreTermsdw1Aw3A[n - 1]*lambda0Sq + dFramePreTermsdw1Aw3A[n - 2]*lambda1Sq));
      dFramePreTermsdw1Aw3B[n]=((1. / static_cast<real>(n)) * (
        dFramePreTermsdw1A[n - 1]*ezSq  + dFramePreTermsdw3B[n - 2]*exSq
        + dFramePreTermsdw1Aw3B[n - 1]*lambda0Sq + dFramePreTermsdw1Aw3B[n - 2]*lambda1Sq));
      
      dFramePreTermsdw1Bw1B[n]=dFramePreTermsdw1Bw1B[n]=((1. / static_cast<real>(n)) * (
        2.0* dFramePreTermsdw1B[n - 1]*exSq 
        + dFramePreTermsdw1Bw1B[n - 1]*lambda0Sq + dFramePreTermsdw1Bw1B[n - 2]*lambda1Sq));
      dFramePreTermsdw1Bw2A[n]=((1. / static_cast<real>(n)) * (
        dFramePreTermsdw2A[n - 1]*exSq  + dFramePreTermsdw1B[n - 2]*eySq
        + dFramePreTermsdw1Bw2A[n - 1]*lambda0Sq + dFramePreTermsdw1Bw2A[n - 2]*lambda1Sq));
      dFramePreTermsdw1Bw2B[n]=((1. / static_cast<real>(n)) * (
        dFramePreTermsdw1B[n - 1]*eySq  + dFramePreTermsdw2B[n - 1]*exSq
        + dFramePreTermsdw1Bw2B[n - 1]*lambda0Sq + dFramePreTermsdw1Bw2B[n - 2]*lambda1Sq));
      dFramePreTermsdw1Bw3A[n]=((1. / static_cast<real>(n)) * (
        dFramePreTermsdw3A[n - 1]*exSq  + dFramePreTermsdw1B[n - 2]*ezSq
        + dFramePreTermsdw1Bw3A[n - 1]*lambda0Sq + dFramePreTermsdw1Bw3A[n - 2]*lambda1Sq));
      dFramePreTermsdw1Bw3B[n]=((1. / static_cast<real>(n)) * (
        dFramePreTermsdw1B[n - 1]*ezSq  + dFramePreTermsdw3B[n - 1]*exSq
        + dFramePreTermsdw1Bw3B[n - 1]*lambda0Sq + dFramePreTermsdw1Bw3B[n - 2]*lambda1Sq));
      
      dFramePreTermsdw2Aw2A[n]=((1. / static_cast<real>(n)) * (
        2.0* dFramePreTermsdw2A[n - 2]*eySq 
        + dFramePreTermsdw2Aw2A[n - 1]*lambda0Sq + dFramePreTermsdw2Aw2A[n - 2]*lambda1Sq));
      dFramePreTermsdw2Aw2B[n]= ((1. / static_cast<real>(n)) * (
        dFramePreTermsdw2A[n - 1]*eySq  + dFramePreTermsdw2B[n - 2]*eySq
        + dFramePreTermsdw2Aw2B[n - 1]*lambda0Sq + dFramePreTermsdw2Aw2B[n - 2]*lambda1Sq));
      dFramePreTermsdw2Aw3A[n]=((1. / static_cast<real>(n)) * (
        dFramePreTermsdw2A[n - 2]*ezSq  + dFramePreTermsdw3A[n - 2]*eySq
        + dFramePreTermsdw2Aw3A[n - 1]*lambda0Sq + dFramePreTermsdw2Aw3A[n - 2]*lambda1Sq));
      dFramePreTermsdw2Aw3B[n]=((1. / static_cast<real>(n)) * (
        dFramePreTermsdw2A[n - 1]*ezSq  + dFramePreTermsdw3B[n - 2]*eySq
        + dFramePreTermsdw2Aw3B[n - 1]*lambda0Sq + dFramePreTermsdw2Aw3B[n - 2]*lambda1Sq));
      
      dFramePreTermsdw2Bw2B[n]=((1. / static_cast<real>(n)) * (
        2.0* dFramePreTermsdw2B[n - 1]*eySq 
        + dFramePreTermsdw2Bw2B[n - 1]*lambda0Sq + dFramePreTermsdw2Bw2B[n - 2]*lambda1Sq));
      dFramePreTermsdw2Bw3A[n]= ((1. / static_cast<real>(n)) * (
        dFramePreTermsdw3A[n - 1]*eySq  + dFramePreTermsdw2B[n - 2]*ezSq
        + dFramePreTermsdw2Bw3A[n - 1]*lambda0Sq + dFramePreTermsdw2Bw3A[n - 2]*lambda1Sq));
      dFramePreTermsdw2Bw3B[n]=((1. / static_cast<real>(n)) * (
        dFramePreTermsdw2B[n - 1]*ezSq  + dFramePreTermsdw3B[n - 1]*eySq
        + dFramePreTermsdw2Bw3B[n - 1]*lambda0Sq + dFramePreTermsdw2Bw3B[n - 2]*lambda1Sq));
      
      dFramePreTermsdw3Aw3A[n]=((1. / static_cast<real>(n)) * (
        2.0* dFramePreTermsdw3A[n - 2]*ezSq 
        + dFramePreTermsdw3Aw3A[n - 1]*lambda0Sq + dFramePreTermsdw3Aw3A[n - 2]*lambda1Sq));
      dFramePreTermsdw3Aw3B[n]= ((1. / static_cast<real>(n)) * (
        dFramePreTermsdw3A[n - 1]*ezSq  + dFramePreTermsdw3B[n - 2]*ezSq
        + dFramePreTermsdw3Aw3B[n - 1]*lambda0Sq + dFramePreTermsdw3Aw3B[n - 2]*lambda1Sq));
      
      dFramePreTermsdw3Bw3B[n]=((1. / static_cast<real>(n)) * (
        2.0* dFramePreTermsdw3B[n - 1]*ezSq 
        + dFramePreTermsdw3Bw3B[n - 1]*lambda0Sq + dFramePreTermsdw3Bw3B[n - 2]*lambda1Sq));
      
    }
  }

  //Derivation of the position
  { 
    dPosPreTermsdw1Aw1A.resize(order);
    dPosPreTermsdw1Aw1B.resize(order);
    dPosPreTermsdw1Aw2A.resize(order);
    dPosPreTermsdw1Aw2B.resize(order);
    dPosPreTermsdw1Aw3A.resize(order);
    dPosPreTermsdw1Aw3B.resize(order);
    dPosPreTermsdw1Bw1B.resize(order);
    dPosPreTermsdw1Bw2A.resize(order);
    dPosPreTermsdw1Bw2B.resize(order);
    dPosPreTermsdw1Bw3A.resize(order);
    dPosPreTermsdw1Bw3B.resize(order);
    dPosPreTermsdw2Aw2A.resize(order);
    dPosPreTermsdw2Aw2B.resize(order);
    dPosPreTermsdw2Aw3A.resize(order);
    dPosPreTermsdw2Aw3B.resize(order);
    dPosPreTermsdw2Bw2B.resize(order);
    dPosPreTermsdw2Bw3A.resize(order);
    dPosPreTermsdw2Bw3B.resize(order);
    dPosPreTermsdw3Aw3A.resize(order);
    dPosPreTermsdw3Aw3B.resize(order);
    dPosPreTermsdw3Bw3B.resize(order);
    Eigen::Vector3r zero;
    zero.setZero();
    Eigen::Vector3r vnan(NAN, NAN, NAN);

    //order 0
    dPosPreTermsdw1Aw1A[0]=vnan;
    dPosPreTermsdw1Aw1B[0]=vnan;
    dPosPreTermsdw1Aw2A[0]=vnan;
    dPosPreTermsdw1Aw2B[0]=vnan;
    dPosPreTermsdw1Aw3A[0]=vnan;
    dPosPreTermsdw1Aw3B[0]=vnan;
    dPosPreTermsdw1Bw1B[0]=vnan;
    dPosPreTermsdw1Bw2A[0]=vnan;
    dPosPreTermsdw1Bw2B[0]=vnan;
    dPosPreTermsdw1Bw3A[0]=vnan;
    dPosPreTermsdw1Bw3B[0]=vnan;
    dPosPreTermsdw2Aw2A[0]=vnan;
    dPosPreTermsdw2Aw2B[0]=vnan;
    dPosPreTermsdw2Aw3A[0]=vnan;
    dPosPreTermsdw2Aw3B[0]=vnan;
    dPosPreTermsdw2Bw2B[0]=vnan;
    dPosPreTermsdw2Bw3A[0]=vnan;
    dPosPreTermsdw2Bw3B[0]=vnan;
    dPosPreTermsdw3Aw3A[0]=vnan;
    dPosPreTermsdw3Aw3B[0]=vnan;
    dPosPreTermsdw3Bw3B[0]=vnan;
    //order 1
    dPosPreTermsdw1Aw1A[1]=zero;
    dPosPreTermsdw1Aw1B[1]=zero;
    dPosPreTermsdw1Aw2A[1]=zero;
    dPosPreTermsdw1Aw2B[1]=zero;
    dPosPreTermsdw1Aw3A[1]=zero;
    dPosPreTermsdw1Aw3B[1]=zero;
    dPosPreTermsdw1Bw1B[1]=zero;
    dPosPreTermsdw1Bw2A[1]=zero;
    dPosPreTermsdw1Bw2B[1]=zero;
    dPosPreTermsdw1Bw3A[1]=zero;
    dPosPreTermsdw1Bw3B[1]=zero;
    dPosPreTermsdw2Aw2A[1]=zero;
    dPosPreTermsdw2Aw2B[1]=zero;
    dPosPreTermsdw2Aw3A[1]=zero;
    dPosPreTermsdw2Aw3B[1]=zero;
    dPosPreTermsdw2Bw2B[1]=zero;
    dPosPreTermsdw2Bw3A[1]=zero;
    dPosPreTermsdw2Bw3B[1]=zero;
    dPosPreTermsdw3Aw3A[1]=zero;
    dPosPreTermsdw3Aw3B[1]=zero;
    dPosPreTermsdw3Bw3B[1]=zero;
    //order n
    for (int n = 2; n < order; n++) {
      dPosPreTermsdw1Aw1A[n]=(1. / static_cast<real>(n))*dFramePreTermsdw1Aw1A[n - 1].col(2);
      dPosPreTermsdw1Aw1B[n]=(1. / static_cast<real>(n))*dFramePreTermsdw1Aw1B[n - 1].col(2);
      dPosPreTermsdw1Aw2A[n]=(1. / static_cast<real>(n))*dFramePreTermsdw1Aw2A[n - 1].col(2);
      dPosPreTermsdw1Aw2B[n]=(1. / static_cast<real>(n))*dFramePreTermsdw1Aw2B[n - 1].col(2);
      dPosPreTermsdw1Aw3A[n]=(1. / static_cast<real>(n))*dFramePreTermsdw1Aw3A[n - 1].col(2);
      dPosPreTermsdw1Aw3B[n]=(1. / static_cast<real>(n))*dFramePreTermsdw1Aw3B[n - 1].col(2);
      dPosPreTermsdw1Bw1B[n]=(1. / static_cast<real>(n))*dFramePreTermsdw1Bw1B[n - 1].col(2);
      dPosPreTermsdw1Bw2A[n]=(1. / static_cast<real>(n))*dFramePreTermsdw1Bw2A[n - 1].col(2);
      dPosPreTermsdw1Bw2B[n]=(1. / static_cast<real>(n))*dFramePreTermsdw1Bw2B[n - 1].col(2);
      dPosPreTermsdw1Bw3A[n]=(1. / static_cast<real>(n))*dFramePreTermsdw1Bw3A[n - 1].col(2);
      dPosPreTermsdw1Bw3B[n]=(1. / static_cast<real>(n))*dFramePreTermsdw1Bw3B[n - 1].col(2);
      dPosPreTermsdw2Aw2A[n]=(1. / static_cast<real>(n))*dFramePreTermsdw2Aw2A[n - 1].col(2);
      dPosPreTermsdw2Aw2B[n]=(1. / static_cast<real>(n))*dFramePreTermsdw2Aw2B[n - 1].col(2);
      dPosPreTermsdw2Aw3A[n]=(1. / static_cast<real>(n))*dFramePreTermsdw2Aw3A[n - 1].col(2);
      dPosPreTermsdw2Aw3B[n]=(1. / static_cast<real>(n))*dFramePreTermsdw2Aw3B[n - 1].col(2);
      dPosPreTermsdw2Bw2B[n]=(1. / static_cast<real>(n))*dFramePreTermsdw2Bw2B[n - 1].col(2);
      dPosPreTermsdw2Bw3A[n]=(1. / static_cast<real>(n))*dFramePreTermsdw2Bw3A[n - 1].col(2);
      dPosPreTermsdw2Bw3B[n]=(1. / static_cast<real>(n))*dFramePreTermsdw2Bw3B[n - 1].col(2);
      dPosPreTermsdw3Aw3A[n]=(1. / static_cast<real>(n))*dFramePreTermsdw3Aw3A[n - 1].col(2);
      dPosPreTermsdw3Aw3B[n]=(1. / static_cast<real>(n))*dFramePreTermsdw3Aw3B[n - 1].col(2);
      dPosPreTermsdw3Bw3B[n]=(1. / static_cast<real>(n))*dFramePreTermsdw3Bw3B[n - 1].col(2);
    }
  }

  called["computeDerivationsPreTermsSecondOrder"] = true;
}

Eigen::Matrix<Eigen::Matrix3r,6,1> SuperClothoidGeometrieSerie::dFramedvars(real s)
{
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  int order = dFramePreTermsdw1A.size();
  computeSPowers(s);
  Eigen::Matrix<Eigen::Matrix3r,6,1> res;
  for(int i=0;i<6;i++)res(i).setZero();
  for (int i = 1; i < order; i++){
    res(0) += spowers[i] * dFramePreTermsdw1A[i];
    res(1) += spowers[i] * dFramePreTermsdw1B[i];
    res(2) += spowers[i] * dFramePreTermsdw2A[i];
    res(3) += spowers[i] * dFramePreTermsdw2B[i];
    res(4) += spowers[i] * dFramePreTermsdw3A[i];
    res(5) += spowers[i] * dFramePreTermsdw3B[i];
  }
  for(int i=0;i<6;i++)res(i) = frameOr * res(i);

  return res;
}

Eigen::Matrix<Eigen::Vector3r,6,1> SuperClothoidGeometrieSerie::dPosdvars(real s)
{
   if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  int order = dPosPreTermsdw1A.size();
  computeSPowers(s);
  Eigen::Matrix<Eigen::Vector3r,6,1> res;
  for(int i=0;i<6;i++)res(i).setZero();
  for (int i = 1; i < order; i++){
    res(0) += spowers[i] * dPosPreTermsdw1A[i];
    res(1) += spowers[i] * dPosPreTermsdw1B[i];
    res(2) += spowers[i] * dPosPreTermsdw2A[i];
    res(3) += spowers[i] * dPosPreTermsdw2B[i];
    res(4) += spowers[i] * dPosPreTermsdw3A[i];
    res(5) += spowers[i] * dPosPreTermsdw3B[i];
  }
  for(int i=0;i<6;i++)res(i) = frameOr * res(i);

  return res;
}

Eigen::Matrix<Eigen::Matrix6r, 3, 3> SuperClothoidGeometrieSerie::hessianFrame(real s)
{
  if (!called["computeDerivationsPreTermsSecondOrder"])computeDerivationsPreTermsSecondOrder();
  int order = dFramePreTermsdw1Aw1A.size();
  computeSPowers(s);
  Eigen::Matrix<Eigen::Matrix3r,6,6> res;
  for(int i=0;i<6;i++)for(int j=0;j<6;j++)res(i,j).setZero();

  for (int i = 1; i < order; i++) {
    res(0,0) += spowers[i] * dFramePreTermsdw1Aw1A[i];
    res(0,1) += spowers[i] * dFramePreTermsdw1Aw1B[i];
    res(0,2) += spowers[i] * dFramePreTermsdw1Aw2A[i];
    res(0,3) += spowers[i] * dFramePreTermsdw1Aw2B[i];
    res(0,4) += spowers[i] * dFramePreTermsdw1Aw3A[i];
    res(0,5) += spowers[i] * dFramePreTermsdw1Aw3B[i];
    
    res(1,1) += spowers[i] * dFramePreTermsdw1Bw1B[i];
    res(1,2) += spowers[i] * dFramePreTermsdw1Bw2A[i];
    res(1,3) += spowers[i] * dFramePreTermsdw1Bw2B[i];
    res(1,4) += spowers[i] * dFramePreTermsdw1Bw3A[i];
    res(1,5) += spowers[i] * dFramePreTermsdw1Bw3B[i];
    
    res(2,2) += spowers[i] * dFramePreTermsdw2Aw2A[i];
    res(2,3) += spowers[i] * dFramePreTermsdw2Aw2B[i];
    res(2,4) += spowers[i] * dFramePreTermsdw2Aw3A[i];
    res(2,5) += spowers[i] * dFramePreTermsdw2Aw3B[i];
    
    res(3,3) += spowers[i] * dFramePreTermsdw2Bw2B[i];
    res(3,4) += spowers[i] * dFramePreTermsdw2Bw3A[i];
    res(3,5) += spowers[i] * dFramePreTermsdw2Bw3B[i];
    
    res(4,4) += spowers[i] * dFramePreTermsdw3Aw3A[i];
    res(4,5) += spowers[i] * dFramePreTermsdw3Aw3B[i];
    
    res(5,5) += spowers[i] * dFramePreTermsdw3Bw3B[i];
  }
  for(int i=1;i<6;i++)
    for(int j=0;j<i;j++)
      res(i,j)=res(j,i);
  for(int i=0;i<6;i++)
    for(int j=0;j<6;j++)
      res(i,j)=frameOr*res(i,j);
  Eigen::Matrix<Eigen::Matrix6r, 3, 3> resR;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      for(int a=0;a<6;a++)
        for(int b=0;b<6;b++)
          resR(i, j)(a, b) = res(a,b)(i, j);
    }
  }
  return resR;
}

Eigen::Matrix<Eigen::Matrix6r, 3, 1> SuperClothoidGeometrieSerie::hessianPos(real s)
{
  if (!called["computeDerivationsPreTermsSecondOrder"])computeDerivationsPreTermsSecondOrder();
  int order = dPosPreTermsdw1Aw1A.size();
  computeSPowers(s);
  Eigen::Matrix<Eigen::Vector3r,6,6> res;
  for(int i=0;i<6;i++)for(int j=0;j<6;j++)res(i,j).setZero();
  
  for (int i = 1; i < order; i++) {
    res(0,0) += spowers[i] * dPosPreTermsdw1Aw1A[i];
    res(0,1) += spowers[i] * dPosPreTermsdw1Aw1B[i];
    res(0,2) += spowers[i] * dPosPreTermsdw1Aw2A[i];
    res(0,3) += spowers[i] * dPosPreTermsdw1Aw2B[i];
    res(0,4) += spowers[i] * dPosPreTermsdw1Aw3A[i];
    res(0,5) += spowers[i] * dPosPreTermsdw1Aw3B[i];
    
    res(1,1) += spowers[i] * dPosPreTermsdw1Bw1B[i];
    res(1,2) += spowers[i] * dPosPreTermsdw1Bw2A[i];
    res(1,3) += spowers[i] * dPosPreTermsdw1Bw2B[i];
    res(1,4) += spowers[i] * dPosPreTermsdw1Bw3A[i];
    res(1,5) += spowers[i] * dPosPreTermsdw1Bw3B[i];
    
    res(2,2) += spowers[i] * dPosPreTermsdw2Aw2A[i];
    res(2,3) += spowers[i] * dPosPreTermsdw2Aw2B[i];
    res(2,4) += spowers[i] * dPosPreTermsdw2Aw3A[i];
    res(2,5) += spowers[i] * dPosPreTermsdw2Aw3B[i];
    
    res(3,3) += spowers[i] * dPosPreTermsdw2Bw2B[i];
    res(3,4) += spowers[i] * dPosPreTermsdw2Bw3A[i];
    res(3,5) += spowers[i] * dPosPreTermsdw2Bw3B[i];
    
    res(4,4) += spowers[i] * dPosPreTermsdw3Aw3A[i];
    res(4,5) += spowers[i] * dPosPreTermsdw3Aw3B[i];
    
    res(5,5) += spowers[i] * dPosPreTermsdw3Bw3B[i];
  }
  for(int i=1;i<6;i++)for(int j=0;j<i;j++)res(i,j)=res(j,i);
  for(int i=0;i<6;i++)for(int j=0;j<6;j++)res(i,j)=frameOr*res(i,j);
  Eigen::Matrix<Eigen::Matrix6r, 3, 1> resR;
  for (int i = 0; i < 3; i++) {
    for(int a=0;a<6;a++)
        for(int b=0;b<6;b++)
          resR(i)(a, b) = res(a,b)(i);
  }
  return resR;
}


void SuperClothoidGeometrieSerie::computePreAltitudeEnergyPreTerms()
{
  if (called["computePreAltitudeEnergyPreTerms"])return;
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  int order = FramePreTerms.size();
  preAltitudeEnergyPreTerms.resize(order);
  preAltitudeEnergyLinearTerm = posOr;
  Eigen::Vector3r vnan(NAN, NAN, NAN);
  preAltitudeEnergyPreTerms[0]=vnan;
  preAltitudeEnergyPreTerms[1]=vnan;
  for (int k = 2; k < order; k++)
    preAltitudeEnergyPreTerms[k]=(1. / static_cast<real>(k)) * (PosPreTerms[k - 1]);
  called["computePreAltitudeEnergyPreTerms"] = true;
}

void SuperClothoidGeometrieSerie::computePreAltitudeEnergyPreTermsDerivations()
{
  if (called["computePreAltitudeEnergyPreTermsDerivations"])return;
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  int order = FramePreTerms.size();
  assert(order > 0);

  dPreAltitudeEnergyPreTermdw1A.resize(order);
  dPreAltitudeEnergyPreTermdw1B.resize(order);
  dPreAltitudeEnergyPreTermdw2A.resize(order);
  dPreAltitudeEnergyPreTermdw2B.resize(order);
  dPreAltitudeEnergyPreTermdw3A.resize(order);
  dPreAltitudeEnergyPreTermdw3B.resize(order);
  Eigen::Vector3r zero(0., 0., 0.);
  for(int idx=0;idx<2;idx++)
  {
    dPreAltitudeEnergyPreTermdw1A[idx]=zero;
    dPreAltitudeEnergyPreTermdw1B[idx]=zero;
    dPreAltitudeEnergyPreTermdw2A[idx]=zero;
    dPreAltitudeEnergyPreTermdw2B[idx]=zero;
    dPreAltitudeEnergyPreTermdw3A[idx]=zero;
    dPreAltitudeEnergyPreTermdw3B[idx]=zero;
  }
  for (int k = 2; k < order; k++) {
    dPreAltitudeEnergyPreTermdw1A[k]=(1. / static_cast<real>(k)) * (dPosPreTermsdw1A[k - 1]);
    dPreAltitudeEnergyPreTermdw1B[k]=(1. / static_cast<real>(k)) * (dPosPreTermsdw1B[k - 1]);
    dPreAltitudeEnergyPreTermdw2A[k]=(1. / static_cast<real>(k)) * (dPosPreTermsdw2A[k - 1]);
    dPreAltitudeEnergyPreTermdw2B[k]=(1. / static_cast<real>(k)) * (dPosPreTermsdw2B[k - 1]);
    dPreAltitudeEnergyPreTermdw3A[k]=(1. / static_cast<real>(k)) * (dPosPreTermsdw3A[k - 1]);
    dPreAltitudeEnergyPreTermdw3B[k]=(1. / static_cast<real>(k)) * (dPosPreTermsdw3B[k - 1]);
  }
  called["computePreAltitudeEnergyPreTermsDerivations"] = true;
}


void SuperClothoidGeometrieSerie::computePreAltitudeEnergyPreTermsDerivationsSecondOrder()
{
  if (called["computePreAltitudeEnergyPreTermsDerivationsSecondOrder"])return;
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  if (!called["computeDerivationsPreTermsSecondOrder"])computeDerivationsPreTermsSecondOrder();
  if (!called["computePreAltitudeEnergyPreTermsDerivations"])computePreAltitudeEnergyPreTermsDerivations();
  int order = FramePreTerms.size();
  assert(order > 0);

  
  dPreAltitudeEnergyPreTermdw1Aw1A.resize(order);
  dPreAltitudeEnergyPreTermdw1Aw1B.resize(order);
  dPreAltitudeEnergyPreTermdw1Aw2A.resize(order);
  dPreAltitudeEnergyPreTermdw1Aw2B.resize(order);
  dPreAltitudeEnergyPreTermdw1Aw3A.resize(order);
  dPreAltitudeEnergyPreTermdw1Aw3B.resize(order);
  dPreAltitudeEnergyPreTermdw1Bw1B.resize(order);
  dPreAltitudeEnergyPreTermdw1Bw2A.resize(order);
  dPreAltitudeEnergyPreTermdw1Bw2B.resize(order);
  dPreAltitudeEnergyPreTermdw1Bw3A.resize(order);
  dPreAltitudeEnergyPreTermdw1Bw3B.resize(order);  
  dPreAltitudeEnergyPreTermdw2Aw2A.resize(order);
  dPreAltitudeEnergyPreTermdw2Aw2B.resize(order);
  dPreAltitudeEnergyPreTermdw2Aw3A.resize(order);
  dPreAltitudeEnergyPreTermdw2Aw3B.resize(order);
  dPreAltitudeEnergyPreTermdw2Bw2B.resize(order);
  dPreAltitudeEnergyPreTermdw2Bw3A.resize(order);
  dPreAltitudeEnergyPreTermdw2Bw3B.resize(order);
  dPreAltitudeEnergyPreTermdw3Aw3A.resize(order);
  dPreAltitudeEnergyPreTermdw3Aw3B.resize(order);
  dPreAltitudeEnergyPreTermdw3Bw3B.resize(order);
  Eigen::Vector3r zero(0., 0., 0.);
  for(int idx=0;idx<2;idx++)
  {
    dPreAltitudeEnergyPreTermdw1Aw1A[idx]=zero;
    dPreAltitudeEnergyPreTermdw1Aw1B[idx]=zero;
    dPreAltitudeEnergyPreTermdw1Aw2A[idx]=zero;
    dPreAltitudeEnergyPreTermdw1Aw2B[idx]=zero;
    dPreAltitudeEnergyPreTermdw1Aw3A[idx]=zero;
    dPreAltitudeEnergyPreTermdw1Aw3B[idx]=zero;
    dPreAltitudeEnergyPreTermdw1Bw1B[idx]=zero;
    dPreAltitudeEnergyPreTermdw1Bw2A[idx]=zero;
    dPreAltitudeEnergyPreTermdw1Bw2B[idx]=zero;
    dPreAltitudeEnergyPreTermdw1Bw3A[idx]=zero;
    dPreAltitudeEnergyPreTermdw1Bw3B[idx]=zero;  
    dPreAltitudeEnergyPreTermdw2Aw2A[idx]=zero;
    dPreAltitudeEnergyPreTermdw2Aw2B[idx]=zero;
    dPreAltitudeEnergyPreTermdw2Aw3A[idx]=zero;
    dPreAltitudeEnergyPreTermdw2Aw3B[idx]=zero;
    dPreAltitudeEnergyPreTermdw2Bw2B[idx]=zero;
    dPreAltitudeEnergyPreTermdw2Bw3A[idx]=zero;
    dPreAltitudeEnergyPreTermdw2Bw3B[idx]=zero;
    dPreAltitudeEnergyPreTermdw3Aw3A[idx]=zero;
    dPreAltitudeEnergyPreTermdw3Aw3B[idx]=zero;
    dPreAltitudeEnergyPreTermdw3Bw3B[idx]=zero;
  }

  for (int k = 2; k < order; k++) {
    dPreAltitudeEnergyPreTermdw1Aw1A[k]=(1. / static_cast<real>(k)) * (dPosPreTermsdw1Aw1A[k - 1]);
    dPreAltitudeEnergyPreTermdw1Aw1B[k]=(1. / static_cast<real>(k)) * (dPosPreTermsdw1Aw1B[k - 1]);
    dPreAltitudeEnergyPreTermdw1Aw2A[k]=(1. / static_cast<real>(k)) * (dPosPreTermsdw1Aw2A[k - 1]);
    dPreAltitudeEnergyPreTermdw1Aw2B[k]=(1. / static_cast<real>(k)) * (dPosPreTermsdw1Aw2B[k - 1]);
    dPreAltitudeEnergyPreTermdw1Aw3A[k]=(1. / static_cast<real>(k)) * (dPosPreTermsdw1Aw3A[k - 1]);
    dPreAltitudeEnergyPreTermdw1Aw3B[k]=(1. / static_cast<real>(k)) * (dPosPreTermsdw1Aw3B[k - 1]);
    dPreAltitudeEnergyPreTermdw1Bw1B[k]=(1. / static_cast<real>(k)) * (dPosPreTermsdw1Bw1B[k - 1]);
    dPreAltitudeEnergyPreTermdw1Bw2A[k]=(1. / static_cast<real>(k)) * (dPosPreTermsdw1Bw2A[k - 1]);
    dPreAltitudeEnergyPreTermdw1Bw2B[k]=(1. / static_cast<real>(k)) * (dPosPreTermsdw1Bw2B[k - 1]);
    dPreAltitudeEnergyPreTermdw1Bw3A[k]=(1. / static_cast<real>(k)) * (dPosPreTermsdw1Bw3A[k - 1]);
    dPreAltitudeEnergyPreTermdw1Bw3B[k]=(1. / static_cast<real>(k)) * (dPosPreTermsdw1Bw3B[k - 1]);  
    dPreAltitudeEnergyPreTermdw2Aw2A[k]=(1. / static_cast<real>(k)) * (dPosPreTermsdw2Aw2A[k - 1]);
    dPreAltitudeEnergyPreTermdw2Aw2B[k]=(1. / static_cast<real>(k)) * (dPosPreTermsdw2Aw2B[k - 1]);
    dPreAltitudeEnergyPreTermdw2Aw3A[k]=(1. / static_cast<real>(k)) * (dPosPreTermsdw2Aw3A[k - 1]);
    dPreAltitudeEnergyPreTermdw2Aw3B[k]=(1. / static_cast<real>(k)) * (dPosPreTermsdw2Aw3B[k - 1]);
    dPreAltitudeEnergyPreTermdw2Bw2B[k]=(1. / static_cast<real>(k)) * (dPosPreTermsdw2Bw2B[k - 1]);
    dPreAltitudeEnergyPreTermdw2Bw3A[k]=(1. / static_cast<real>(k)) * (dPosPreTermsdw2Bw3A[k - 1]);
    dPreAltitudeEnergyPreTermdw2Bw3B[k]=(1. / static_cast<real>(k)) * (dPosPreTermsdw2Bw3B[k - 1]);
    dPreAltitudeEnergyPreTermdw3Aw3A[k]=(1. / static_cast<real>(k)) * (dPosPreTermsdw3Aw3A[k - 1]);
    dPreAltitudeEnergyPreTermdw3Aw3B[k]=(1. / static_cast<real>(k)) * (dPosPreTermsdw3Aw3B[k - 1]);
    dPreAltitudeEnergyPreTermdw3Bw3B[k]=(1. / static_cast<real>(k)) * (dPosPreTermsdw3Bw3B[k - 1]);
  }
  called["computePreAltitudeEnergyPreTermsDerivationsSecondOrder"] = true;
}


Eigen::Vector3r SuperClothoidGeometrieSerie::preAltitudeEnergy(real s)
{
  if (!called["computePreAltitudeEnergyPreTerms"])computePreAltitudeEnergyPreTerms();
  int order = preAltitudeEnergyPreTerms.size();
  computeSPowers(s);
  Eigen::Vector3r res(0., 0., 0.);
  for (int i = 2; i < order; i++)
    res += spowers[i] * preAltitudeEnergyPreTerms[i];
  res = frameOr * res;
  res += preAltitudeEnergyLinearTerm * spowers[1];
  return res;
}

Eigen::Vector3r SuperClothoidGeometrieSerie::preAltitudeRotationalEnergy(real s)
{
    if (!called["computePreAltitudeEnergyPreTerms"])computePreAltitudeEnergyPreTerms();
  int order = preAltitudeEnergyPreTerms.size();
  computeSPowers(s);
  Eigen::Vector3r res(0., 0., 0.);
  for (int i = 2; i < order; i++)
    res += spowers[i] * preAltitudeEnergyPreTerms[i];
  return res;
}


Eigen::Matrix<real, 3, 6> SuperClothoidGeometrieSerie::gradPreAltitudeEnergy(real s)
{
  if (!called["computePreAltitudeEnergyPreTermsDerivations"])computePreAltitudeEnergyPreTermsDerivations();
  Eigen::Matrix<real, 3, 6> res;
  res.setZero();
  int order = dPreAltitudeEnergyPreTermdw1A.size();
  computeSPowers(s);
  for (int i = 2; i < order; i++) {
    res.col(0) += spowers[i] * dPreAltitudeEnergyPreTermdw1A[i];
    res.col(1) += spowers[i] * dPreAltitudeEnergyPreTermdw1B[i];
    res.col(2) += spowers[i] * dPreAltitudeEnergyPreTermdw2A[i];
    res.col(3) += spowers[i] * dPreAltitudeEnergyPreTermdw2B[i];
    res.col(4) += spowers[i] * dPreAltitudeEnergyPreTermdw3A[i];
    res.col(5) += spowers[i] * dPreAltitudeEnergyPreTermdw3B[i];
  }
  return frameOr * res;
}

Eigen::Matrix<Eigen::Vector3r, 3, 1> SuperClothoidGeometrieSerie::dPreAltitudeEnergydPos(real s)
{
  Eigen::Matrix<Eigen::Vector3r, 3, 1> res;
  res << Eigen::Vector3r(s , 0., 0.), Eigen::Vector3r(0., s , 0.), Eigen::Vector3r(0., 0., s );
  return res;
}

Eigen::Matrix<Eigen::Matrix3r, 3, 1> SuperClothoidGeometrieSerie::dPreAltitudeEnergydFrame(real s)
{
  if (!called["computePreAltitudeEnergyPreTerms"])computePreAltitudeEnergyPreTerms();
  int order = preAltitudeEnergyPreTerms.size();
  computeSPowers(s);
  Eigen::Vector3r K(0., 0., 0.);
  for (int i = 2; i < order; i++)
    K += spowers[i] * preAltitudeEnergyPreTerms[i];
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> res;
  res.setConstant(Eigen::Matrix3r::Zero());
  Eigen::Matrix3r temp;
  Eigen::Vector3r vtemp;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      temp.setZero();
      temp(i, j) = 1.;
      vtemp = temp * K;
      for (int a = 0; a < 3; a++)
        res[a](i, j) += vtemp[a];
    }
  }
  return res;
}

Eigen::Matrix<Eigen::Matrix<Eigen::Vector6r, 3, 3>, 3, 1> SuperClothoidGeometrieSerie::d2PreAltitudeEnergydFramedvalues(real s)
{
  if (!called["computePreAltitudeEnergyPreTermsDerivations"])computePreAltitudeEnergyPreTermsDerivations();
  Eigen::Matrix<Eigen::Vector3r,6,1> K;
  for(int i=0;i<6;i++)K[i].setZero();
  int order = dPreAltitudeEnergyPreTermdw1A.size();
  computeSPowers(s);
  for (int i = 1; i < order; i++) {
    K(0) += spowers[i] * dPreAltitudeEnergyPreTermdw1A[i];
    K(1) += spowers[i] * dPreAltitudeEnergyPreTermdw1B[i];
    K(2) += spowers[i] * dPreAltitudeEnergyPreTermdw2A[i];
    K(3) += spowers[i] * dPreAltitudeEnergyPreTermdw2B[i];
    K(4) += spowers[i] * dPreAltitudeEnergyPreTermdw3A[i];
    K(5) += spowers[i] * dPreAltitudeEnergyPreTermdw3B[i];
  }
  Eigen::Matrix<Eigen::Matrix<Eigen::Vector6r, 3, 3>, 3, 1> res;
  Eigen::Matrix3r temp;
  Eigen::Matrix<Eigen::Vector3r,6,1> v;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      temp.setZero();
      temp(i, j) = 1.;
      for(int a=0;a<6;a++)
        v(a)=temp*K(a);
      for (int a = 0; a < 3; a++) {
        for(int b=0;b<6;b++)
          res[a](i, j)[b]=v[b][a];
      }
    }
  }
  return res;
}


Eigen::Matrix<Eigen::Matrix6r, 3, 1> SuperClothoidGeometrieSerie::hessianPreAltitudeEnergy(real s)
{
  if (!called["computePreAltitudeEnergyPreTermsDerivationsSecondOrder"])computePreAltitudeEnergyPreTermsDerivationsSecondOrder();
  Eigen::Matrix<Eigen::Matrix6r, 3, 1> preRes;
  preRes.setConstant(Eigen::Matrix6r::Zero());
  int order = dPreAltitudeEnergyPreTermdw1Aw1A.size();
  computeSPowers(s);
  for (int i = 1; i < order; i++) {
    for (int j = 0; j < 3; j++) {
      preRes[j](0, 0) += spowers[i] * dPreAltitudeEnergyPreTermdw1Aw1A[i][j];
      preRes[j](0, 1) += spowers[i] * dPreAltitudeEnergyPreTermdw1Aw1B[i][j];
      preRes[j](0, 2) += spowers[i] * dPreAltitudeEnergyPreTermdw1Aw2A[i][j];
      preRes[j](0, 3) += spowers[i] * dPreAltitudeEnergyPreTermdw1Aw2B[i][j];
      preRes[j](0, 4) += spowers[i] * dPreAltitudeEnergyPreTermdw1Aw3A[i][j];
      preRes[j](0, 5) += spowers[i] * dPreAltitudeEnergyPreTermdw1Aw3B[i][j];
      
      preRes[j](1, 1) += spowers[i] * dPreAltitudeEnergyPreTermdw1Bw1B[i][j];
      preRes[j](1, 2) += spowers[i] * dPreAltitudeEnergyPreTermdw1Bw2A[i][j];
      preRes[j](1, 3) += spowers[i] * dPreAltitudeEnergyPreTermdw1Bw2B[i][j];
      preRes[j](1, 4) += spowers[i] * dPreAltitudeEnergyPreTermdw1Bw3A[i][j];
      preRes[j](1, 5) += spowers[i] * dPreAltitudeEnergyPreTermdw1Bw3B[i][j];
      
      preRes[j](2, 2) += spowers[i] * dPreAltitudeEnergyPreTermdw2Aw2A[i][j];
      preRes[j](2, 3) += spowers[i] * dPreAltitudeEnergyPreTermdw2Aw2B[i][j];
      preRes[j](2, 4) += spowers[i] * dPreAltitudeEnergyPreTermdw2Aw3A[i][j];
      preRes[j](2, 5) += spowers[i] * dPreAltitudeEnergyPreTermdw2Aw3B[i][j];
      
      preRes[j](3, 3) += spowers[i] * dPreAltitudeEnergyPreTermdw2Bw2B[i][j];
      preRes[j](3, 4) += spowers[i] * dPreAltitudeEnergyPreTermdw2Bw3A[i][j];
      preRes[j](3, 5) += spowers[i] * dPreAltitudeEnergyPreTermdw2Bw3B[i][j];
      
      preRes[j](4, 4) += spowers[i] * dPreAltitudeEnergyPreTermdw3Aw3A[i][j];
      preRes[j](4, 5) += spowers[i] * dPreAltitudeEnergyPreTermdw3Aw3B[i][j];
      
      preRes[j](5, 5) += spowers[i] * dPreAltitudeEnergyPreTermdw3Bw3B[i][j];
      
      for(int a=1;a<6;a++)
        for(int b=0;b<a;b++)
          preRes[j](a,b)=preRes[j](b,a);
    }
  }
  Eigen::Matrix<Eigen::Matrix6r, 3, 1> res;
  for (int i = 0; i < 3; i++)res[i] = frameOr(i, 0) * preRes[0] + frameOr(i, 1) * preRes[1] + frameOr(i, 2) * preRes[2];
  return res;
}

Eigen::Matrix<Eigen::Vector3r, 3, 1> SuperClothoidGeometrieSerie::dPosdPos(real s)
{
  Eigen::Matrix<Eigen::Vector3r, 3, 1>res;
  res << Eigen::Vector3r(1., 0., 0.), Eigen::Vector3r(0., 1., 0.), Eigen::Vector3r(0., 0., 1.);
  return res;
}

Eigen::Matrix<Eigen::Matrix3r, 3, 3> SuperClothoidGeometrieSerie::dFramedFrame(real s)
{
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  int order = FramePreTerms.size();
  computeSPowers(s);
  Eigen::Matrix3r K = FramePreTerms[0];
  for (int i = 1; i < order; i++)
    K += spowers[i] * FramePreTerms[i];
  Eigen::Matrix<Eigen::Matrix3r, 3, 3> res;
  res.setConstant(Eigen::Matrix3r::Zero());
  Eigen::Matrix3r temp;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      temp.setZero();
      temp(i, j) = 1.;
      temp = temp * K;
      for (int a = 0; a < 3; a++) {
        for (int b = 0; b < 3; b++) {
          res(a, b)(i, j) = temp(a, b);
        }
      }
    }
  }
  return res;
}

Eigen::Matrix<Eigen::Matrix3r, 3, 1> SuperClothoidGeometrieSerie::dPosdFrame(real s)
{
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  int order = PosPreTerms.size();
  computeSPowers(s);
  Eigen::Vector3r K(0., 0., 0.);
  for (int i = 1; i < order; i++)
    K += spowers[i] * PosPreTerms[i];
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> res;
  Eigen::Matrix3r temp;
  Eigen::Vector3r vtemp;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      temp.setZero();
      temp(i, j) = 1.;
      vtemp = temp * K;
      for (int b = 0; b < 3; b++) {
        res[b](i, j) = vtemp(b);
      }
    }
  }
  return res;
}

Eigen::Matrix<Eigen::Matrix<Eigen::Vector6r, 3, 3>, 3, 3> SuperClothoidGeometrieSerie::d2FramedFramedvalues(real s)
{
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  int order = PosPreTerms.size();
  computeSPowers(s);
  Eigen::Matrix<Eigen::Matrix3r, 6, 1> K;
  for(int i=0;i<6;i++)K(i).setZero();
  for (int i = 1; i < order; i++) {
    K(0) += spowers[i] * dFramePreTermsdw1A[i];
    K(1) += spowers[i] * dFramePreTermsdw1B[i];
    K(2) += spowers[i] * dFramePreTermsdw2A[i];
    K(3) += spowers[i] * dFramePreTermsdw2B[i];
    K(4) += spowers[i] * dFramePreTermsdw3A[i];
    K(5) += spowers[i] * dFramePreTermsdw3B[i];
  }

  Eigen::Matrix<Eigen::Matrix<Eigen::Vector6r, 3, 3>, 3, 3> res;
  Eigen::Matrix<Eigen::Vector6r, 3, 3> tmp1;
  tmp1.setConstant(Eigen::Vector6r::Zero());
  res.setConstant(tmp1);
  Eigen::Matrix<Eigen::Matrix3r, 6, 1> tmp;
  Eigen::Matrix3r temp;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      temp.setZero();
      temp(i, j) = 1.;
      for(int a=0;a<6;a++)
        tmp(a)=temp*K(a);
      for (int a = 0; a < 3; a++) {
        for (int b = 0; b < 3; b++) {
          for(int v=0;v<6;v++)
            res(a, b)(i, j)[v] = tmp(v)(a, b);
        }
      }
    }
  }
  return res;
}

Eigen::Matrix<Eigen::Matrix<Eigen::Vector6r, 3, 3>, 3, 1> SuperClothoidGeometrieSerie::d2PosdFramedvalues(real s)
{
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  int order = PosPreTerms.size();
  computeSPowers(s);
  Eigen::Matrix<Eigen::Vector3r, 6, 1> K;
  for(int i=0;i<6;i++)K(i).setZero();
  for (int i = 1; i < order; i++) {
    K(0) += spowers[i] * dPosPreTermsdw1A[i];
    K(1) += spowers[i] * dPosPreTermsdw1B[i];
    K(2) += spowers[i] * dPosPreTermsdw2A[i];
    K(3) += spowers[i] * dPosPreTermsdw2B[i];
    K(4) += spowers[i] * dPosPreTermsdw3A[i];
    K(5) += spowers[i] * dPosPreTermsdw3B[i];
  }

  Eigen::Matrix<Eigen::Matrix<Eigen::Vector6r, 3, 3>, 3, 1> res;
  Eigen::Matrix<Eigen::Vector6r, 3, 3> tmp1;
  tmp1.setConstant(Eigen::Vector6r::Zero());
  res.setConstant(tmp1);
  Eigen::Matrix<Eigen::Vector3r,6,1> tmp;
  Eigen::Matrix3r temp;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      temp.setZero();
      temp(i, j) = 1.;
      for(int a=0;a<6;a++)
        tmp(a)=temp*K(a);
      for (int b = 0; b < 3; b++) {
        for(int v=0;v<6;v++)
          res(b)(i, j)[v] = tmp(v)(b);
      }
    }
  }
  return res;
}


void SuperClothoidGeometrieSerie::computeSPowers(real s)
{
  int order = FramePreTerms.size();
  assert(order > 0);
  if ((PosPreTerms.size() == spowers.size()) && (sPowersCurrentS == s))
    return;
  spowers.resize(order);
  spowers[0] = 1;
  spowers[1] = s;
  for (int i = 2; i < order; i++) {
    if (i % 2)
      spowers[i] = spowers[i / 2] * spowers[i / 2 + 1];
    else
      spowers[i] = spowers[i / 2] * spowers[i / 2];
  }
}
