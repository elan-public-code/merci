/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef SUPERCLOTHOIDGEOMETRIESERIE_H
#define SUPERCLOTHOIDGEOMETRIESERIE_H

#include <Eigen/Dense>
#include <vector>
#include <utility>
#include <iostream>
#include <map>
#include <BaseCore/LinearScalar.hpp>

/**
 * \file SuperClothoidGeometrieSerie.hpp
 * This file implement the serie to represent the geometrie of the ribbon.
 * It is based on the Ph.D. of Romain Casati, chapter 4, and even it is adapted,
 * the theoritical basis remainds the same.
 */

class SuperClothoidGeometrieSerie
{
public:
  void Place(Eigen::Vector3r pos, Eigen::Matrix3r frame);
  void Setup(LinearScalar omega1, LinearScalar omega2, LinearScalar omega3);

  /**
   * \brief Upper bound of the rest of the serie at order \c order
   */
  real estimRemainderOfTheSerie(int order, real s = 1.);

  /**
   * \brief Upper bound on the order of the serie to get the precision \c prec
   */
  int estimNeededOrder(real prec);

  /**
   * \brief Upper bound on the order of the serie to get a reasonnable precision
   */
  int estimNeededOrder();

  /**
   * \brief Upper bound on the maximal abscissa to use to avoid computation issues with the serie
   */
  real estimSMax();


  Eigen::Matrix3r Frame(real s);
  Eigen::Matrix3r PreFrame(real s);//Frame = FrameOr*PreFrame
  Eigen::Vector3r Pos(real s);
  Eigen::Vector3r PreTranslation(real s);//Pos = PosOr+FrameOr*PreTranslation

  Eigen::Matrix3r Frame_s(real s);
  Eigen::Vector3r Pos_s(real s);

  std::pair<Eigen::Vector3r, Eigen::Matrix3r> PosFrame(real s);

  Eigen::Vector3r preAltitudeEnergy(real s);
  Eigen::Vector3r preAltitudeRotationalEnergy(real s);//preAltitudeEnergy=surface*pos+frameOr*preAltitudeRotationalEnergy
  Eigen::Matrix<real, 3, 6> gradPreAltitudeEnergy(real s);
  Eigen::Matrix<Eigen::Vector3r, 3, 1> dPreAltitudeEnergydPos(real s);
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPreAltitudeEnergydFrame(real s);
  
  Eigen::Matrix<Eigen::Matrix6r, 3, 1> hessianPreAltitudeEnergy(real s);
  Eigen::Matrix<Eigen::Matrix<Eigen::Vector6r, 3, 3>, 3, 1> d2PreAltitudeEnergydFramedvalues(real s);

  Eigen::Matrix<Eigen::Matrix3r, 3, 3> dFramedFrame(real s);
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPosdFrame(real s);

  Eigen::Matrix<Eigen::Matrix3r,6,1> dFramedvars(real s);
  Eigen::Matrix<Eigen::Vector3r,6,1> dPosdvars(real s);
  
  Eigen::Matrix<Eigen::Matrix6r, 3, 3> hessianFrame(real s);
  Eigen::Matrix<Eigen::Matrix6r, 3, 1> hessianPos(real s);
  Eigen::Matrix<Eigen::Matrix<Eigen::Vector6r, 3, 3>, 3, 3> d2FramedFramedvalues(real s);
  Eigen::Matrix<Eigen::Matrix<Eigen::Vector6r, 3, 3>, 3, 1> d2PosdFramedvalues(real s);

  Eigen::Matrix<Eigen::Vector3r, 3, 1> dPosdPos(real s);
  void computeSeriePreTerms(int order);
private:
  std::map<std::string, bool> called;
  void computeSeriePreTerms();
  void computeDerivationsPreTerms();
  void computePreAltitudeEnergySerieDerivations();
  void computePreAltitudeEnergyPreTerms();
  void computePreAltitudeEnergyPreTermsDerivations();
  void computePreAltitudeEnergyPreTermsDerivationsSecondOrder();
  
  void computeDerivationsPreTermsSecondOrder();

  real factorial(int k);

  real C(real s);
  Eigen::Matrix3r skewOf(Eigen::Vector3r);
  Eigen::Vector3r posOr;
  Eigen::Matrix3r frameOr;
  Eigen::Matrix3r lambda0Sq;
  Eigen::Matrix3r lambda1Sq;
  Eigen::Vector3r lambda0;
  Eigen::Vector3r lambda1;
  real lambda;

  LinearScalar omega1;
  LinearScalar omega2;
  LinearScalar omega3;
  real ribbonWidth;

  std::vector<Eigen::Matrix3r> FramePreTerms;
  std::vector<Eigen::Vector3r> PosPreTerms;

  // Derivations

  Eigen::Vector3r ex,ey,ez;
  Eigen::Matrix3r exSq,eySq,ezSq;
  
  std::vector<Eigen::Matrix3r> dFramePreTermsdw1A;
  std::vector<Eigen::Matrix3r> dFramePreTermsdw1B;
  std::vector<Eigen::Matrix3r> dFramePreTermsdw2A;
  std::vector<Eigen::Matrix3r> dFramePreTermsdw2B;
  std::vector<Eigen::Matrix3r> dFramePreTermsdw3A;
  std::vector<Eigen::Matrix3r> dFramePreTermsdw3B;
  std::vector<Eigen::Vector3r> dPosPreTermsdw1A;
  std::vector<Eigen::Vector3r> dPosPreTermsdw1B;
  std::vector<Eigen::Vector3r> dPosPreTermsdw2A;
  std::vector<Eigen::Vector3r> dPosPreTermsdw2B;
  std::vector<Eigen::Vector3r> dPosPreTermsdw3A;
  std::vector<Eigen::Vector3r> dPosPreTermsdw3B;

  std::vector<Eigen::Vector3r> preAltitudeEnergyPreTerms;
  Eigen::Vector3r preAltitudeEnergyLinearTerm;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdw1A;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdw1B;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdw2A;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdw2B;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdw3A;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdw3B;
  
  //Second order derivations
  
  
  std::vector<Eigen::Matrix3r> dFramePreTermsdw1Aw1A;
  std::vector<Eigen::Matrix3r> dFramePreTermsdw1Aw1B;
  std::vector<Eigen::Matrix3r> dFramePreTermsdw1Aw2A;
  std::vector<Eigen::Matrix3r> dFramePreTermsdw1Aw2B;
  std::vector<Eigen::Matrix3r> dFramePreTermsdw1Aw3A;
  std::vector<Eigen::Matrix3r> dFramePreTermsdw1Aw3B;
  std::vector<Eigen::Matrix3r> dFramePreTermsdw1Bw1B;
  std::vector<Eigen::Matrix3r> dFramePreTermsdw1Bw2A;
  std::vector<Eigen::Matrix3r> dFramePreTermsdw1Bw2B;
  std::vector<Eigen::Matrix3r> dFramePreTermsdw1Bw3A;
  std::vector<Eigen::Matrix3r> dFramePreTermsdw1Bw3B;  
  std::vector<Eigen::Matrix3r> dFramePreTermsdw2Aw2A;
  std::vector<Eigen::Matrix3r> dFramePreTermsdw2Aw2B;
  std::vector<Eigen::Matrix3r> dFramePreTermsdw2Aw3A;
  std::vector<Eigen::Matrix3r> dFramePreTermsdw2Aw3B;
  std::vector<Eigen::Matrix3r> dFramePreTermsdw2Bw2B;
  std::vector<Eigen::Matrix3r> dFramePreTermsdw2Bw3A;
  std::vector<Eigen::Matrix3r> dFramePreTermsdw2Bw3B;
  std::vector<Eigen::Matrix3r> dFramePreTermsdw3Aw3A;
  std::vector<Eigen::Matrix3r> dFramePreTermsdw3Aw3B;
  std::vector<Eigen::Matrix3r> dFramePreTermsdw3Bw3B;
  
  std::vector<Eigen::Vector3r> dPosPreTermsdw1Aw1A;
  std::vector<Eigen::Vector3r> dPosPreTermsdw1Aw1B;
  std::vector<Eigen::Vector3r> dPosPreTermsdw1Aw2A;
  std::vector<Eigen::Vector3r> dPosPreTermsdw1Aw2B;
  std::vector<Eigen::Vector3r> dPosPreTermsdw1Aw3A;
  std::vector<Eigen::Vector3r> dPosPreTermsdw1Aw3B;
  std::vector<Eigen::Vector3r> dPosPreTermsdw1Bw1B;
  std::vector<Eigen::Vector3r> dPosPreTermsdw1Bw2A;
  std::vector<Eigen::Vector3r> dPosPreTermsdw1Bw2B;
  std::vector<Eigen::Vector3r> dPosPreTermsdw1Bw3A;
  std::vector<Eigen::Vector3r> dPosPreTermsdw1Bw3B;  
  std::vector<Eigen::Vector3r> dPosPreTermsdw2Aw2A;
  std::vector<Eigen::Vector3r> dPosPreTermsdw2Aw2B;
  std::vector<Eigen::Vector3r> dPosPreTermsdw2Aw3A;
  std::vector<Eigen::Vector3r> dPosPreTermsdw2Aw3B;
  std::vector<Eigen::Vector3r> dPosPreTermsdw2Bw2B;
  std::vector<Eigen::Vector3r> dPosPreTermsdw2Bw3A;
  std::vector<Eigen::Vector3r> dPosPreTermsdw2Bw3B;
  std::vector<Eigen::Vector3r> dPosPreTermsdw3Aw3A;
  std::vector<Eigen::Vector3r> dPosPreTermsdw3Aw3B;
  std::vector<Eigen::Vector3r> dPosPreTermsdw3Bw3B;

  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdw1Aw1A;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdw1Aw1B;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdw1Aw2A;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdw1Aw2B;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdw1Aw3A;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdw1Aw3B;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdw1Bw1B;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdw1Bw2A;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdw1Bw2B;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdw1Bw3A;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdw1Bw3B;  
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdw2Aw2A;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdw2Aw2B;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdw2Aw3A;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdw2Aw3B;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdw2Bw2B;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdw2Bw3A;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdw2Bw3B;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdw3Aw3A;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdw3Aw3B;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdw3Bw3B;
  
  std::vector<real> spowers;
  real sPowersCurrentS = 0.;
  void computeSPowers(real s);
};

#endif // SUPERCLOTHOIDGEOMETRIESERIE_H

