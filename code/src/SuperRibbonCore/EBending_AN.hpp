/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef EBENDING_AN_H
#define EBENDING_AN_H

#include <BaseCore/real.hpp>
#include <BaseCore/LinearScalar.hpp>


class FunFun{ //I know it's a strange name. Represent BendingAN but at given s. Has to be integrated
public:
  void setPhy(double width, double thickness, double YoungModulus, double poissonRatio);
  void set(double a, double b, double n, double m);
  double EAN(double s);
  Eigen::Vector4r GEAN ( double s );
  Eigen::Matrix4r HEAN ( double s );
private:
  inline double power2 ( double x ) const;
  double w=0.1;
  double h=0.001;
  double Y=1.e9;
  double nu=0.5;
  double a=10;
  double b=-2;
  double n=2;
  double m=-0.1;
};

template<class T> 
struct Trapzd
{
  int n; // Current level of refinement.
  std::function<T(double)> &func;
  double a, b;
  T s; // Limits of integration and current value of integral.

    Trapzd() { };

    // func is function or functor to be integrated between limits: a and b 
    Trapzd(std::function<T(double)> &funcc, const double aa, const double bb)   
        : func(funcc), a(aa), b(bb)
    {
        n = 0;
    }

    // Returns the nth stage of refinement of the extended trapezoidal rule. 
    // On the first call (n = 1), the routine returns the crudest estimate  
    // of integral of f x / dx in [a,b]. Subsequent calls set n=2,3,... and
    // improve the accuracy by adding 2n - 2 additional interior points.
    T next()
    {

        n++;
        if (n == 1)
        {
          s = 0.5 * (b-a) * (func(a) + func(b));
            return s;
        } 
        double x, tnm, del;
        int it, j;
          it=1<<(n-1); 
          tnm=it; 
          // This is the spacing of the points to be added.          
          del = (b - a) / tnm; 
          x = a + 0.5 * del;
          T sum = s-s;
          for (j = 0; j < it; j++, x += del)
          {
                sum += func(x);
          }
          // This replaces s by its refined value.  
          s = 0.5 * (s + ((b - a) / tnm) * sum ); 
          return s;
    }
};

class EANReal{ //I know it's a strange name. Represent BendingAN but at given s. Has to be integrated
public:
  void setPhy(double width, double thickness, double YoungModulus, double poissonRatio);
  void set(double a, double b, double n, double m);
  double EAN(double l);
  Eigen::Vector4r GEAN ( double l );
  Eigen::Matrix4r HEAN ( double l );
private:
  inline double power2 ( double x ) const;
  double w=0.1;
  double h=0.001;
  double Y=1.e9;
  double nu=0.5;
  double a=10;
  double b=-2;
  double n=2;
  double m=-0.1;
};

class EBending_AN
{
public:
  void FixPhysics(double D, double poissonRatio, double thickness, double width, double length);
  void FixPt(LinearScalar omega1, LinearScalar eta);
  void FixPt(double a, double b, double n, double m);
  double value();
  Eigen::Vector4r gradient();
  Eigen::Matrix4r hessian();
private:
  double length;
//   FunFun energy;
  EANReal Energy;

};

#endif // EBENDING_AN_H
