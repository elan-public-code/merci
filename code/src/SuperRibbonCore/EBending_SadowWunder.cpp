/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "EBending_SadowWunder.hpp"
#include "Log/Logger.hpp"
#include <string>
#include <cmath>

bool EBending_SW::useSadowskyApprox = false;
double EBending_SW::etaPrimeRegularizationFactor = 0.;

void EBending_SW::FixPhysics(real D, real ribbonWidth, real ribbonLength, std::optional<real> poissonRatio)
{
  this->D = D;
  this->ribbonWidth = ribbonWidth;
  this->ribbonLength = ribbonLength;
  this->poissonRatio = poissonRatio;
}

void EBending_SW::FixPt(LinearScalar omega1, LinearScalar eta, std::optional<LinearScalar> naturalOmega1)
{
  this->omega1 = omega1;
  this->eta = eta;
  this->naturalOmega1 = naturalOmega1;
}

#ifndef TEST_SUPERSIMPLE_ENERGY

real EBending_SW::computeEnergy()
{
  const real L = ribbonLength;
  const real a = omega1.A, b = omega1.B, n = eta.A, m = eta.B;

  real K, I, V;
  K = D * ribbonWidth / 2;
  const real a2 = a * a, n2 = n * n, m2 = m * m, b2 = b * b;
  const real n4 = n2 * n2, n3 = n2 * n;
  const real L2 = L * L, L3 = L2 * L, L4 = L2 * L2, L5 = L2 * L3, L6 = L3 * L3, L7 = L3 * L4;
  const real m2p1 = m2 + 1., m2p1sq = m2p1 * m2p1;
  real ENatural = 0.;
  if (poissonRatio && naturalOmega1) {
    real an = naturalOmega1.value().A;
    real bn = naturalOmega1.value().B;
    real nu = poissonRatio.value();
    ENatural = -D * ribbonWidth * (
                 an * a * nu * n2 * L5 / 5.0 + ((bn * a + an * b) * nu * n * n + 2.0 * an * a * nu * m * n) * L4 / 4.0 + (bn * b * nu * n2 + 2.0 * (bn * a + an * b) * nu * m * n + an * a * (nu * m2 + 1.0)) * L3 / 3.0 + (2.0 * bn * b * nu * m * n + (bn * a + an * b) * (nu * m2 + 1.0)) * L2 / 2.0 + bn * b * (nu * m2 + 1.0) * L);
  }
  //computed with maple
  I = a2 * n4 * L7 / 7.0
      + (4.0 * a2 * m * n3 + 2.0 * b * a * n4) * L6 / 6.0
      + (b2 * n4 + 8.0 * b * a * m * n3 + a2 * (2.0 * m2p1 * n2 + 4.0 * m2 * n2)) * L5 / 5.0
      + (4.0 * b2 * m * n3 + 2.0 * b * a * (2.0 * m2p1 * n2 + 4.0 * m2 * n2) + 4.0 * a2 * m2p1 * m * n) * L4 / 4.0
      + (b2 * (2.0 * m2p1 * n2 + 4.0 * m2 * n2) + 8.0 * b * a * m2p1 * m * n + a2 * m2p1sq) * L3 / 3.0
      + (4.0 * b2 * m2p1 * m * n + 2.0 * b * a * m2p1sq) * L2 / 2.0
      + b2 * m2p1sq * L;
      
  if(useSadowskyApprox)
  {
    V=1.0;
  }
  else
  {
    const real x = n * ribbonWidth / 2.;
    if (x < -1. || x > 1.) {
      LoggerMsg msg("Point out of constraints");
      msg.addChild("n", std::to_string(n));
      msg.addChild("w", std::to_string(ribbonWidth));
      getLogger()->Write(LoggerLevel::ERROR, "Model", msg);
    }
    if (abs(x) < 1e-5)
      V = 1. + (1. / 3.) * x * x;
    else
      V = std::log((1. + x) / (1. - x)) / (2.*x);
    if(std::isnan(V)){
      LoggerMsg msg("NaN error");
      msg.addChild("n", std::to_string(n));
      msg.addChild("w", std::to_string(ribbonWidth));
      getLogger()->Write(LoggerLevel::ERROR, "Computation", msg);
    }
  }
  double regul = etaPrimeRegularizationFactor*L*n*n*0.5;
  return V * K * I + ENatural+regul;
}

Eigen::Vector4r EBending_SW::computeGrad()
{
  Eigen::Vector4r res;
  const real L = ribbonLength;
  const real a = omega1.A, b = omega1.B, n = eta.A, m = eta.B;

  real K, I, V, Vn, Ia, Ib, In, Im;
  K = D * ribbonWidth / 2;
  const real a2 = a * a, n2 = n * n, m2 = m * m, b2 = b * b;
  const real n4 = n2 * n2, n3 = n2 * n;
  const real L2 = L * L, L3 = L2 * L, L4 = L2 * L2, L5 = L2 * L3, L6 = L3 * L3, L7 = L3 * L4;
  const real m2p1 = m2 + 1., m2p1sq = m2p1 * m2p1;
  Eigen::Vector4r EGNatural(0., 0., 0., 0.);
  if (poissonRatio && naturalOmega1) {
    real an = naturalOmega1.value().A;
    real bn = naturalOmega1.value().B;
    real nu = poissonRatio.value();
    EGNatural[0] = an * nu * n2 * L5 / 5.0 + (2.0 * an * m * n * nu + bn * n2 * nu) * L4 / 4.0 + (2.0 * bn * nu * m * n + an * (m2 * nu + 1.0)) * L3 / 3.0 + bn * (m2 * nu + 1.0) * L2 / 2.0;
    EGNatural[1] = an * nu * n2 * L4 / 4.0 + (2.0 * an * m * n * nu + bn * n2 * nu) * L3 / 3.0 + (2.0 * bn * nu * m * n + an * (m2 * nu + 1.0)) * L2 / 2.0 + bn * (m2 * nu + 1.0) * L;
    EGNatural[2] = 2.0 / 5.0 * an * a * nu * n * L5 + (2.0 * (a * bn + an * b) * nu * n + 2.0 * an * a * nu * m) * L4 / 4.0 + (2.0 * bn * b * nu * n + 2.0 * (a * bn + an * b) * nu * m) * L3 / 3.0 + bn * b * nu * m * L2;
    EGNatural[3] = an * a * nu * n * L4 / 2.0 + (2.0 * (a * bn + an * b) * nu * n + 2.0 * an * a * nu * m) * L3 / 3.0 + (2.0 * bn * b * nu * n + 2.0 * (a * bn + an * b) * nu * m) * L2 / 2.0 + 2.0 * bn * b * m * nu * L;
    EGNatural *= -D * ribbonWidth;
  }
  //computed with maple
  I = a2 * n4 * L7 / 7.0
      + (4.0 * a2 * m * n3 + 2.0 * b * a * n4) * L6 / 6.0
      + (b2 * n4 + 8.0 * b * a * m * n3 + a2 * (2.0 * m2p1 * n2 + 4.0 * m2 * n2)) * L5 / 5.0
      + (4.0 * b2 * m * n3 + 2.0 * b * a * (2.0 * m2p1 * n2 + 4.0 * m2 * n2) + 4.0 * a2 * m2p1 * m * n) * L4 / 4.0
      + (b2 * (2.0 * m2p1 * n2 + 4.0 * m2 * n2) + 8.0 * b * a * m2p1 * m * n + a2 * m2p1sq) * L3 / 3.0
      + (4.0 * b2 * m2p1 * m * n + 2.0 * b * a * m2p1sq) * L2 / 2.0
      + b2 * m2p1sq * L;
  Ia =
    2.0 / 7.0 * a * n4 * L7
    + (8.0 * a * m * n3 + 2.0 * b * n4) * L6 / 6.0
    + (8.0 * b * m * n3 + 2.0 * a * (2.0 * m2p1 * n2 + 4.0 * m2 * n2)) * L5 / 5.0
    + (2.0 * b * (2.0 * m2p1 * n2 + 4.0 * m2 * n2) + 8.0 * a * m2p1 * m * n) * L4 / 4.0
    + (8.0 * b * m2p1 * m * n + 2.0 * a * m2p1sq) * L3 / 3.0
    + b * m2p1sq * L2;
  Ib =
    a * n4 * L6 / 3.0 + (8.0 * a * m * n3 + 2.0 * b * n4) * L5 / 5.0
    + (8.0 * b * m * n3 + 2.0 * a * (2.0 * m2p1 * n2 + 4.0 * m2 * n2)) * L4 / 4.0
    + (2.0 * b * (2.0 * m2p1 * n2 + 4.0 * m2 * n2) + 8.0 * a * m2p1 * m * n) * L3 / 3.0
    + (8.0 * b * m2p1 * m * n + 2.0 * a * m2p1sq) * L2 / 2.0
    + 2.0 * b * m2p1sq * L;
  In =
    4.0 / 7.0 * a2 * n3 * L7
    + (12.0 * a2 * m * n2 + 8.0 * b * a * n3) * L6 / 6.0
    + (4.0 * b2 * n3 + 24.0 * b * a * m * n2 + a2 * (4.0 * m2p1 * n + 8.0 * m2 * n)) * L5 / 5.0
    + (12.0 * b2 * m * n2 + 2.0 * b * a * (4.0 * m2p1 * n + 8.0 * m2 * n) + 4.0 * a2 * m2p1 * m) * L4 / 4.0
    + (b2 * (4.0 * m2p1 * n + 8.0 * m2 * n) + 8.0 * b * a * m2p1 * m) * L3 / 3.0
    + 2.0 * b2 * m2p1 * m * L2;
  Im =
    2.0 / 3.0 * a2 * n3 * L6
    + (12.0 * a2 * m * n2 + 8.0 * b * a * n3) * L5 / 5.0
    + (4.0 * b2 * n3 + 24.0 * b * a * m * n2 + 8.0 * a2 * m2 * n + 4.0 * a2 * m2p1 * n) * L4 / 4.0
    + (12.0 * b2 * m * n2 + 16.0 * b * a * m2 * n + 8.0 * b * a * m2p1 * n + 4.0 * a2 * m2p1 * m) * L3 / 3.0
    + (8.0 * b2 * m2 * n + 4.0 * b2 * m2p1 * n + 8.0 * b * a * m2p1 * m) * L2 / 2.0
    + 4.0 * b2 * m2p1 * L * m;
  if(useSadowskyApprox)
  {
    V=1.0;
    Vn=0.0;
  }
  else
  {
    const real x = n * ribbonWidth / 2.;
    if (x <= -1. || x >= 1.) {
      LoggerMsg msg("Point out of constraints");
      msg.addChild("n", std::to_string(n));
      msg.addChild("w", std::to_string(ribbonWidth));
      getLogger()->Write(LoggerLevel::ERROR, "Model", msg);
    }
    if (abs(x) < 1e-5) {
      V = 1. + (1. / 3.) * x * x;
      Vn = (1. / 6.) * n * ribbonWidth * ribbonWidth;
    } else {
      V = std::log((1. + x) / (1. - x)) / (2.*x);
      real w = ribbonWidth;
      real t2 = n * w / 2.0;
      real   t3 = 1.0 - t2;
      real t4 = 1 / t3;
      real t6 = 1.0 + t2;
      real t7 = t3 * t3;
      real t16 = 1 / w;
      real t20 = log(t6 * t4);
      real t21 = n * n;
      Vn = (w * t4 + t6 / t7 * w) / t6 * t3 / n * t16 / 2.0 - t20 / t21 * t16;

    }
  }
  Eigen::Vector4r regul(0., 0., etaPrimeRegularizationFactor*L*n,0.);
  return K * Eigen::Vector4r(V * Ia, V * Ib, V * In + Vn * I, V * Im) + EGNatural + regul;
}

Eigen::Matrix4r EBending_SW::computeHessian()
{
  const real L = ribbonLength;
  const real a = omega1.A, b = omega1.B, n = eta.A, m = eta.B;

  real K, I, V, Vn, Vnn, Ia, Ib, In, Im,
         Iaa, Iba, Ina, Ima,
         Iab, Ibb, Inb, Imb,
         Ian, Ibn, Inn, Imn,
         Iam, Ibm, Inm, Imm;
  K = D * ribbonWidth / 2;
  const real a2 = a * a, n2 = n * n, m2 = m * m, b2 = b * b;
  const real n4 = n2 * n2, n3 = n2 * n;
  const real L2 = L * L, L3 = L2 * L, L4 = L2 * L2, L5 = L2 * L3, L6 = L3 * L3, L7 = L3 * L4;
  const real m2p1 = m2 + 1., m2p1sq = m2p1 * m2p1;
  Eigen::Matrix4r EHNatural;
  EHNatural.setZero();
  if (poissonRatio && naturalOmega1) {
    real an = naturalOmega1.value().A;
    real bn = naturalOmega1.value().B;
    real nu = poissonRatio.value();
    const real t1 = D * ribbonWidth;
    const real t2 = an * nu;
    const real t3 = L * L;
    const real t4 = t3 * t3;
    const real t5 = t4 * L;
    const real t10 = bn * nu;
    const real t12 = t2 * m + t10 * n;
    const real t15 = t3 * L;
    const real t20 = t1 * (2.0 / 5.0 * t2 * n * t5 + t12 * t4 / 2.0 + 2.0 / 3.0 * t10 * m * t15);
    const real t29 = t1 * (t2 * n * t4 / 2.0 + 2.0 / 3.0 * t12 * t15 + t10 * m * t3);
    const real t36 = nu * L;
    const real t40 = t1 * (2.0 / 3.0 * t2 * n * t15 + t12 * t3 + 2.0 * bn * m * t36);
    const real t41 = a * an;
    const real t48 = (a * bn + b * an) * nu;
    const real t51 = b * bn;
    const real t52 = nu * t15;
    const real t65 = t1 * (t41 * nu * t4 / 2.0 + 2.0 / 3.0 * t48 * t15 + t51 * nu * t3);
    EHNatural(0, 0) = 0.0;
    EHNatural(0, 1) = 0.0;
    EHNatural(0, 2) = -t20;
    EHNatural(0, 3) = -t29;
    EHNatural(1, 0) = 0.0;
    EHNatural(1, 1) = 0.0;
    EHNatural(1, 2) = -t29;
    EHNatural(1, 3) = -t40;
    EHNatural(2, 0) = -t20;
    EHNatural(2, 1) = -t29;
    EHNatural(2, 2) = -t1 * (2.0 / 5.0 * t41 * nu * t5 + t48 * t4 / 2.0 + 2.0 / 3.0 * t51 * t52);
    EHNatural(2, 3) = -t65;
    EHNatural(3, 0) = -t29;
    EHNatural(3, 1) = -t40;
    EHNatural(3, 2) = -t65;
    EHNatural(3, 3) = -t1 * (2.0 / 3.0 * t41 * t52 + t48 * t3 + 2.0 * t51 * t36);

  }
  //computed with maple
  I = a2 * n4 * L7 / 7.0
      + (4.0 * a2 * m * n3 + 2.0 * b * a * n4) * L6 / 6.0
      + (b2 * n4 + 8.0 * b * a * m * n3 + a2 * (2.0 * m2p1 * n2 + 4.0 * m2 * n2)) * L5 / 5.0
      + (4.0 * b2 * m * n3 + 2.0 * b * a * (2.0 * m2p1 * n2 + 4.0 * m2 * n2) + 4.0 * a2 * m2p1 * m * n) * L4 / 4.0
      + (b2 * (2.0 * m2p1 * n2 + 4.0 * m2 * n2) + 8.0 * b * a * m2p1 * m * n + a2 * m2p1sq) * L3 / 3.0
      + (4.0 * b2 * m2p1 * m * n + 2.0 * b * a * m2p1sq) * L2 / 2.0
      + b2 * m2p1sq * L;
  Ia =
    2.0 / 7.0 * a * n4 * L7
    + (8.0 * a * m * n3 + 2.0 * b * n4) * L6 / 6.0
    + (8.0 * b * m * n3 + 2.0 * a * (2.0 * m2p1 * n2 + 4.0 * m2 * n2)) * L5 / 5.0
    + (2.0 * b * (2.0 * m2p1 * n2 + 4.0 * m2 * n2) + 8.0 * a * m2p1 * m * n) * L4 / 4.0
    + (8.0 * b * m2p1 * m * n + 2.0 * a * m2p1sq) * L3 / 3.0
    + b * m2p1sq * L2;
  Ib =
    a * n4 * L6 / 3.0 + (8.0 * a * m * n3 + 2.0 * b * n4) * L5 / 5.0
    + (8.0 * b * m * n3 + 2.0 * a * (2.0 * m2p1 * n2 + 4.0 * m2 * n2)) * L4 / 4.0
    + (2.0 * b * (2.0 * m2p1 * n2 + 4.0 * m2 * n2) + 8.0 * a * m2p1 * m * n) * L3 / 3.0
    + (8.0 * b * m2p1 * m * n + 2.0 * a * m2p1sq) * L2 / 2.0
    + 2.0 * b * m2p1sq * L;
  In =
    4.0 / 7.0 * a2 * n3 * L7
    + (12.0 * a2 * m * n2 + 8.0 * b * a * n3) * L6 / 6.0
    + (4.0 * b2 * n3 + 24.0 * b * a * m * n2 + a2 * (4.0 * m2p1 * n + 8.0 * m2 * n)) * L5 / 5.0
    + (12.0 * b2 * m * n2 + 2.0 * b * a * (4.0 * m2p1 * n + 8.0 * m2 * n) + 4.0 * a2 * m2p1 * m) * L4 / 4.0
    + (b2 * (4.0 * m2p1 * n + 8.0 * m2 * n) + 8.0 * b * a * m2p1 * m) * L3 / 3.0
    + 2.0 * b2 * m2p1 * m * L2;
  Im =
    2.0 / 3.0 * a2 * n3 * L6
    + (12.0 * a2 * m * n2 + 8.0 * b * a * n3) * L5 / 5.0
    + (4.0 * b2 * n3 + 24.0 * b * a * m * n2 + 8.0 * a2 * m2 * n + 4.0 * a2 * m2p1 * n) * L4 / 4.0
    + (12.0 * b2 * m * n2 + 16.0 * b * a * m2 * n + 8.0 * b * a * m2p1 * n + 4.0 * a2 * m2p1 * m) * L3 / 3.0
    + (8.0 * b2 * m2 * n + 4.0 * b2 * m2p1 * n + 8.0 * b * a * m2p1 * m) * L2 / 2.0
    + 4.0 * b2 * m2p1 * L * m;
  {
    const real t1 = n * n;
    const real t2 = t1 * t1;
    const real t3 = L * L;
    const real t4 = t3 * L;
    const real t5 = t3 * t3;
    const real t6 = t5 * t4;
    const real t9 = t1 * n;
    const real t10 = m * t9;
    const real t11 = t5 * t3;
    const real t14 = m * m;
    const real t15 = t14 + 1.0;
    const real t20 = 8.0 * t14 * t1 + 4.0 * t15 * t1;
    const real t21 = t5 * L;
    const real t24 = t15 * m;
    const real t28 = t15 * t15;
    const real t42 = t2 * t11 / 3.0 + 8.0 / 5.0 * t10 * t21 + t20 * t5 / 4.0 + 8.0 / 3.0 * t24 * n * t4 + t28 * t3;
    const real t43 = a * t9;
    const real t51 = 24.0 * a * m * t1 + 8.0 * b * t9;
    const real t56 = 24.0 * b * m * t1;
    const real t61 = 8.0 * t14 * n + 4.0 * t15 * n;
    const real t64 = 2.0 * a * t61 + t56;
    const real t69 = a * t15;
    const real t71 = 8.0 * t69 * m;
    const real t72 = 2.0 * b * t61 + t71;
    const real t75 = b * t15;
    const real t79 = 8.0 / 7.0 * t43 * t6 + t51 * t11 / 6.0 + t64 * t21 / 5.0 + t72 * t5 / 4.0 + 8.0 / 3.0 * t75 * m * t4;
    const real t81 = 4.0 / 3.0 * t43 * t11;
    const real t83 = t51 * t21 / 5.0;
    const real t89 = 16.0 * a * t14 * n + 8.0 * t69 * n + t56;
    const real t97 = 16.0 * b * t14 * n + 8.0 * t75 * n + t71;
    const real t102 = 4.0 * t75 * m * t3;
    const real t103 = t81 + t83 + t89 * t5 / 4.0 + t97 * t4 / 3.0 + t102;
    const real t120 = t81 + t83 + t64 * t5 / 4.0 + t72 * t4 / 3.0 + t102;
    const real t132 = 8.0 / 5.0 * t43 * t21 + t51 * t5 / 4.0 + t89 * t4 / 3.0 + t97 * t3 / 2.0 + 8.0 * t75 * L * m;
    const real t133 = a * a;
    const real t134 = t133 * t1;
    const real t139 = b * a;
    const real t141 = t133 * m * n + t139 * t1;
    const real t144 = b * b;
    const real t146 = 12.0 * t144 * t1;
    const real t149 = 48.0 * t139 * m * n;
    const real t151 = 12.0 * t14 + 4.0;
    const real t158 = 24.0 * t144 * m * n;
    const real t169 = 2.0 * t134 * t11;
    const real t171 = 24.0 / 5.0 * t141 * t21;
    const real t176 = 8.0 * t133 * t14 + 4.0 * t133 * t15 + t146 + t149;
    const real t178 = t176 * t5 / 4.0;
    const real t183 = 16.0 * t139 * t14 + 8.0 * t139 * t15 + t158;
    const real t185 = t183 * t4 / 3.0;
    const real t186 = t144 * t14;
    const real t188 = t144 * t15;
    Iaa = 2.0 / 7.0 * t2 * t6 + 4.0 / 3.0 * t10 * t11 + t20 * t21 / 5.0 + 2.0 * t24 * n * t5 + 2.0 / 3.0 * t28 * t4;
    Iab = t42;
    Ian = t79;
    Iam = t103;
    Iba = t42;
    Ibb = 2.0 / 5.0 * t2 * t21 + 2.0 * t10 * t5 + t20 * t4 / 3.0 + 4.0 * t24 * n * t3 + 2.0 * t28 * L;
    Ibn = t120;
    Ibm = t132;
    Ina = t79;
    Inb = t120;
    Inn = 12.0 / 7.0 * t134 * t6 + 4.0 * t141 * t11 + (t133 * t151 + t146 + t149) * t21 / 5.0 + (2.0 * t139 * t151 + t158) * t5 / 4.0 + t144 * t151 * t4 / 3.0;
    Inm = t169 + t171 + t178 + t185 + (8.0 * t186 + 4.0 * t188) * t3 / 2.0;
    Ima = t103;
    Imb = t132;
    Imn = 4.0 * t186 * t3 + 2.0 * t188 * t3 + t169 + t171 + t178 + t185;
    Imm = 12.0 / 5.0 * t134 * t21 + 6.0 * t141 * t5 + t176 * t4 / 3.0 + t183 * t3 / 2.0 + 8.0 * t186 * L + 4.0 * t188 * L;
  }
  
  if(useSadowskyApprox)
  {
    V=1.0;
    Vn=0.0;
    Vnn=0.0;
  }
  else
  {
    const real x = n * ribbonWidth / 2.;
    if (x <= -1. || x >= 1.) {
      LoggerMsg msg("Point out of constraints");
      msg.addChild("n", std::to_string(n));
      msg.addChild("w", std::to_string(ribbonWidth));
      getLogger()->Write(LoggerLevel::ERROR, "Model", msg);
    }
    if (abs(x) < 1e-5) {
      V = 1. + (1. / 3.) * x * x;
      Vn = (1. / 6.) * n * ribbonWidth * ribbonWidth;
      Vnn = (1. / 6.) * ribbonWidth * ribbonWidth;
    } else {
      const real w = ribbonWidth;
      const real t2 = n * w / 2.0;
      const real t3 = t2 + 1.0;
      const real t4 = 1.0 - t2;
      const real t5 = 1 / t4;
      const real t7 = log(t5 * t3);
      const real t8 = 1 / n;
      const real t10 = 1 / w;
      const real t13 = t4 * t4;
      const real t14 = 1 / t13;
      const real t17 = t3 * t14 * w + w * t5;
      const real t18 = 1 / t3;
      const real t19 = t17 * t18 / 2.0;
      const real t20 = t4 * t8;
      const real t21 = t20 * t10;
      const real t23 = n * n;
      const real t24 = 1 / t23;
      const real t28 = w * w;
      const real t37 = t3 * t3;
      V = t7 * t8 * t10;
      Vn = -t7 * t24 * t10 + t19 * t21;
      Vnn = (t28 * t14 + t3 / t13 / t4 * t28) * t18 * t21 / 2.0 - t17 / t37 * t20 / 4.0 - t19 * t8 / 2.0 - 2.0 * t19 * t4 * t24 * t10 + 2.0 * t7 / t23 / n * t10;
    }
  }
  Eigen::Matrix4r res;
  res<< Iaa*V        , Iab*V        , Ian*V + Ia*Vn          , Iam*V,
        Iba*V        , Ibb*V        , Ibn*V + Ib*Vn          , Ibm*V,
        Ina*V + Ia*Vn, Inb*V + Ib*Vn, Inn*V + 2*In*Vn + I*Vnn, Inm*V + Im*Vn,
        Ima*V        , Imb*V        , Imn*V + Im*Vn          , Imm*V;
        
  Eigen::Matrix4r regul;
  regul<<0.,0.,0.,0.,
         0.,0.,0.,0.,
         0., 0., etaPrimeRegularizationFactor*L, 0.,
         0.,0.,0.,0.;
  return K * res + EHNatural+regul;
}

#endif // TEST_SUPERSIMPLE_ENERGY

void EBending_SW::YesIWantToUseSadowsky(bool IamStuborn)
{
  EBending_SW::useSadowskyApprox = IamStuborn;
}
