/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef EBENDING_SW_H
#define EBENDING_SW_H

#include <BaseCore/real.hpp>
#include <BaseCore/LinearScalar.hpp>
#include <Eigen/Dense>
#include <optional>

//#define TEST_SUPERSIMPLE_ENERGY

class EBending_SW
{
public:
  void FixPhysics(real D, real ribbonWidth, real ribbonLength, std::optional<real> poissonRatio/* = std::nullopt*/);
  void FixPt(LinearScalar omega1, LinearScalar eta, std::optional<LinearScalar> naturalOmega1/* = std::nullopt*/);
  real computeEnergy();
  Eigen::Vector4r computeGrad();
  Eigen::Matrix4r computeHessian();
  static void YesIWantToUseSadowsky(bool useSadowskyApprox);
  static double etaPrimeRegularizationFactor;
private:
  static bool useSadowskyApprox;
  LinearScalar omega1;
  LinearScalar eta;
  std::optional<LinearScalar> naturalOmega1;
  real D;
  real ribbonWidth;
  real ribbonLength;
  std::optional<real> poissonRatio;
};

#endif // ELASTICENERGY_H


