/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "EBending_fiber_SadowWunder.hpp"
#include "Log/Logger.hpp"
#include <string>
#include <cmath>

bool EBending_fiber_SW::useSadowskyApprox = false;
double EBending_fiber_SW::etaPrimeRegularizationFactor = 0.;

void EBending_fiber_SW::FixPhysics(real D, real ribbonWidth, real ribbonLength, std::optional<real> poissonRatio)
{
  this->D = D;
  this->ribbonWidth = ribbonWidth;
  this->ribbonLength = ribbonLength;
  this->poissonRatio = poissonRatio;
}

void EBending_fiber_SW::FixPt(LinearScalar omega1, LinearScalar eta, std::optional<LinearScalar> naturalOmega1)
{
  this->omega1 = omega1;
  this->eta = eta;
  this->naturalOmega1 = naturalOmega1;
}

real EBending_fiber_SW::computeEnergy()
{
  const real L = ribbonLength;
  const real a = omega1.A, b = omega1.B, n = eta.A, m = eta.B;

  real K, I, V;
  K = D * ribbonWidth / 2;
  const real a2 = a * a, n2 = n * n, m2 = m * m, b2 = b * b;
  const real L2 = L * L, L3 = L2 * L, L4 = L2 * L2, L5 = L2 * L3;
  const real m2p1 = m2 + 1.;
  real ENatural = 0.;
  if (poissonRatio && naturalOmega1) {
    real an = naturalOmega1.value().A;
    real bn = naturalOmega1.value().B;
    real nu = poissonRatio.value();
    ENatural = -D * ribbonWidth * (
                 an * a * nu * n2 * L5 / 5.0 + ((bn * a + an * b) * nu * n * n + 2.0 * an * a * nu * m * n) * L4 / 4.0 + (bn * b * nu * n2 + 2.0 * (bn * a + an * b) * nu * m * n + an * a * (nu * m2 + 1.0)) * L3 / 3.0 + (2.0 * bn * b * nu * m * n + (bn * a + an * b) * (nu * m2 + 1.0)) * L2 / 2.0 + bn * b * (nu * m2 + 1.0) * L);
  }
  //computed with maple
  I = 2.0 / 5.0 * a2 * n2 * L5 
    + (4 * a2 * m * n + 4.0 * a * b * n2) * L4 / 4.0 
    + (2 * b2 * n2 + 8.0 * b * a * m * n + a2 * (2 * m2p1)) * L3 / 3.0 
    + (4 * b2 * m * n + 2.0 * b * a * (2 * m2p1)) * L2 / 2.0 
    + b2 * (2 * m2p1) * L;
      
  if(useSadowskyApprox)
  {
    V=1.0;
  }
  else
  {
    const real x = n * ribbonWidth / 2.;
    if (x < -1. || x > 1.) {
      LoggerMsg msg("Point out of constraints");
      msg.addChild("n", std::to_string(n));
      msg.addChild("w", std::to_string(ribbonWidth));
      getLogger()->Write(LoggerLevel::ERROR, "Model", msg);
    }
    if (abs(x) < 1e-5)
      V = 1. + (1. / 3.) * x * x;
    else
      V = std::log((1. + x) / (1. - x)) / (2.*x);
    if(std::isnan(V)){
      LoggerMsg msg("NaN error");
      msg.addChild("n", std::to_string(n));
      msg.addChild("w", std::to_string(ribbonWidth));
      getLogger()->Write(LoggerLevel::ERROR, "Computation", msg);
    }
  }
  double regul = etaPrimeRegularizationFactor*L*n*n*0.5;
  return V * K * I + ENatural+regul;
}

Eigen::Vector4r EBending_fiber_SW::computeGrad()
{
  Eigen::Vector4r res;
  const real L = ribbonLength;
  const real a = omega1.A, b = omega1.B, n = eta.A, m = eta.B;

  real K, I, V, Vn, Ia, Ib, In, Im;
  K = D * ribbonWidth / 2;
  const real a2 = a * a, n2 = n * n, m2 = m * m, b2 = b * b;
  const real n4 = n2 * n2, n3 = n2 * n;
  const real L2 = L * L, L3 = L2 * L, L4 = L2 * L2, L5 = L2 * L3, L6 = L3 * L3, L7 = L3 * L4;
  const real m2p1 = m2 + 1., m2p1sq = m2p1 * m2p1;
  Eigen::Vector4r EGNatural(0., 0., 0., 0.);
  if (poissonRatio && naturalOmega1) {
    real an = naturalOmega1.value().A;
    real bn = naturalOmega1.value().B;
    real nu = poissonRatio.value();
    EGNatural[0] = an * nu * n2 * L5 / 5.0 + (2.0 * an * m * n * nu + bn * n2 * nu) * L4 / 4.0 + (2.0 * bn * nu * m * n + an * (m2 * nu + 1.0)) * L3 / 3.0 + bn * (m2 * nu + 1.0) * L2 / 2.0;
    EGNatural[1] = an * nu * n2 * L4 / 4.0 + (2.0 * an * m * n * nu + bn * n2 * nu) * L3 / 3.0 + (2.0 * bn * nu * m * n + an * (m2 * nu + 1.0)) * L2 / 2.0 + bn * (m2 * nu + 1.0) * L;
    EGNatural[2] = 2.0 / 5.0 * an * a * nu * n * L5 + (2.0 * (a * bn + an * b) * nu * n + 2.0 * an * a * nu * m) * L4 / 4.0 + (2.0 * bn * b * nu * n + 2.0 * (a * bn + an * b) * nu * m) * L3 / 3.0 + bn * b * nu * m * L2;
    EGNatural[3] = an * a * nu * n * L4 / 2.0 + (2.0 * (a * bn + an * b) * nu * n + 2.0 * an * a * nu * m) * L3 / 3.0 + (2.0 * bn * b * nu * n + 2.0 * (a * bn + an * b) * nu * m) * L2 / 2.0 + 2.0 * bn * b * m * nu * L;
    EGNatural *= -D * ribbonWidth;
  }
  //computed with maple
  I = 2.0 / 5.0 * a2 * n2 * L5 
    + (4 * a2 * m * n + 4.0 * a * b * n2) * L4 / 4.0 
    + (2 * b2 * n2 + 8.0 * b * a * m * n + a2 * (2 * m2p1)) * L3 / 3.0 
    + (4 * b2 * m * n + 2.0 * b * a * (2 * m2p1)) * L2 / 2.0 
    + b2 * (2 * m2p1) * L;
    
  Ia = 4.0 / 5.0 * a * n2 * L5 
     + (8 * a * m * n + 4.0 * b * n2) * L4 / 4.0 
     + (8 * b * m * n + 2.0 * a * (2 * m2p1)) * L3 / 3.0 
     + b * (2 * m2p1) * L2;
  Ib = a * n2 * L4 
     + (8 * a * m * n + 4.0 * b * n2) * L3 / 3.0 
     + (8 * b * m * n + 2.0 * a * (2 * m2p1)) * L2 / 2.0 
     + 2.0 * b * (2 * m2p1) * L;
  In = 4.0 / 5.0 * a2 * n * L5 
     + (4 * a2 * m + 8.0 * b * a * n) * L4 / 4.0 
     + (8 * b * a * m + 4.0 * b2 * n) * L3 / 3.0 
     + 2.0 * b2 * m * L2;
  Im = a2 * n * L4 
     + (4 * a2 * m + 8.0 * b * a * n) * L3 / 3.0 
     + (8 * b * a * m + 4.0 * b2 * n) * L2 / 2.0 
     + 4.0 * b2 * m * L;

  if(useSadowskyApprox)
  {
    V=1.0;
    Vn=0.0;
  }
  else
  {
    const real x = n * ribbonWidth / 2.;
    if (x <= -1. || x >= 1.) {
      LoggerMsg msg("Point out of constraints");
      msg.addChild("n", std::to_string(n));
      msg.addChild("w", std::to_string(ribbonWidth));
      getLogger()->Write(LoggerLevel::ERROR, "Model", msg);
    }
    if (abs(x) < 1e-5) {
      V = 1. + (1. / 3.) * x * x;
      Vn = (1. / 6.) * n * ribbonWidth * ribbonWidth;
    } else {
      V = std::log((1. + x) / (1. - x)) / (2.*x);
      real w = ribbonWidth;
      real t2 = n * w / 2.0;
      real   t3 = 1.0 - t2;
      real t4 = 1 / t3;
      real t6 = 1.0 + t2;
      real t7 = t3 * t3;
      real t16 = 1 / w;
      real t20 = log(t6 * t4);
      real t21 = n * n;
      Vn = (w * t4 + t6 / t7 * w) / t6 * t3 / n * t16 / 2.0 - t20 / t21 * t16;

    }
  }
  Eigen::Vector4r regul(0., 0., etaPrimeRegularizationFactor*L*n,0.);
  return K * Eigen::Vector4r(V * Ia, V * Ib, V * In + Vn * I, V * Im) + EGNatural + regul;
}

Eigen::Matrix4r EBending_fiber_SW::computeHessian()
{
  const real L = ribbonLength;
  const real a = omega1.A, b = omega1.B, n = eta.A, m = eta.B;

  real K, I, V, Vn, Vnn, Ia, Ib, In, Im,
         Iaa, Iba, Ina, Ima,
         Iab, Ibb, Inb, Imb,
         Ian, Ibn, Inn, Imn,
         Iam, Ibm, Inm, Imm;
  K = D * ribbonWidth / 2;
  const real a2 = a * a, n2 = n * n, m2 = m * m, b2 = b * b;
  const real n4 = n2 * n2, n3 = n2 * n;
  const real L2 = L * L, L3 = L2 * L, L4 = L2 * L2, L5 = L2 * L3, L6 = L3 * L3, L7 = L3 * L4;
  const real m2p1 = m2 + 1., m2p1sq = m2p1 * m2p1;
  Eigen::Matrix4r EHNatural;
  EHNatural.setZero();
  if (poissonRatio && naturalOmega1) {
    real an = naturalOmega1.value().A;
    real bn = naturalOmega1.value().B;
    real nu = poissonRatio.value();
    const real t1 = D * ribbonWidth;
    const real t2 = an * nu;
    const real t3 = L * L;
    const real t4 = t3 * t3;
    const real t5 = t4 * L;
    const real t10 = bn * nu;
    const real t12 = t2 * m + t10 * n;
    const real t15 = t3 * L;
    const real t20 = t1 * (2.0 / 5.0 * t2 * n * t5 + t12 * t4 / 2.0 + 2.0 / 3.0 * t10 * m * t15);
    const real t29 = t1 * (t2 * n * t4 / 2.0 + 2.0 / 3.0 * t12 * t15 + t10 * m * t3);
    const real t36 = nu * L;
    const real t40 = t1 * (2.0 / 3.0 * t2 * n * t15 + t12 * t3 + 2.0 * bn * m * t36);
    const real t41 = a * an;
    const real t48 = (a * bn + b * an) * nu;
    const real t51 = b * bn;
    const real t52 = nu * t15;
    const real t65 = t1 * (t41 * nu * t4 / 2.0 + 2.0 / 3.0 * t48 * t15 + t51 * nu * t3);
    EHNatural(0, 0) = 0.0;
    EHNatural(0, 1) = 0.0;
    EHNatural(0, 2) = -t20;
    EHNatural(0, 3) = -t29;
    EHNatural(1, 0) = 0.0;
    EHNatural(1, 1) = 0.0;
    EHNatural(1, 2) = -t29;
    EHNatural(1, 3) = -t40;
    EHNatural(2, 0) = -t20;
    EHNatural(2, 1) = -t29;
    EHNatural(2, 2) = -t1 * (2.0 / 5.0 * t41 * nu * t5 + t48 * t4 / 2.0 + 2.0 / 3.0 * t51 * t52);
    EHNatural(2, 3) = -t65;
    EHNatural(3, 0) = -t29;
    EHNatural(3, 1) = -t40;
    EHNatural(3, 2) = -t65;
    EHNatural(3, 3) = -t1 * (2.0 / 3.0 * t41 * t52 + t48 * t3 + 2.0 * t51 * t36);

  }
  //computed with maple
  I = 2.0 / 5.0 * a2 * n2 * L5 
    + (4 * a2 * m * n + 4.0 * a * b * n2) * L4 / 4.0 
    + (2 * b2 * n2 + 8.0 * b * a * m * n + a2 * (2 * m2p1)) * L3 / 3.0 
    + (4 * b2 * m * n + 2.0 * b * a * (2 * m2p1)) * L2 / 2.0 
    + b2 * (2 * m2p1) * L;
    Ia = 4.0 / 5.0 * a * n2 * L5 
     + (8 * a * m * n + 4.0 * b * n2) * L4 / 4.0 
     + (8 * b * m * n + 2.0 * a * (2 * m2p1)) * L3 / 3.0 
     + b * (2 * m2p1) * L2;
  Ib = a * n2 * L4 
     + (8 * a * m * n + 4.0 * b * n2) * L3 / 3.0 
     + (8 * b * m * n + 2.0 * a * (2 * m2p1)) * L2 / 2.0 
     + 2.0 * b * (2 * m2p1) * L;
  In = 4.0 / 5.0 * a2 * n * L5 
     + (4 * a2 * m + 8.0 * b * a * n) * L4 / 4.0 
     + (8 * b * a * m + 4.0 * b2 * n) * L3 / 3.0 
     + 2.0 * b2 * m * L2;
  Im = a2 * n * L4 
     + (4 * a2 * m + 8.0 * b * a * n) * L3 / 3.0 
     + (8 * b * a * m + 4.0 * b2 * n) * L2 / 2.0 
     + 4.0 * b2 * m * L;
  {
    const real t1 = n * n;
    const real t2 = L * L;
    const real t3 = (t2 * t2);
    const real t4 = t3 * L;
    const real t7 = m * n;
    const real t10 = (m * m);
    const real t12 = 4 * t10 + 2;
    const real t13 = t2 * L;
    const real t17 = t1 * t3;
    const real t19 = 8 / 3 * t7 * t13;
    const real t23 = a * n;
    const real t28 = (a * m + b * n);
    const real t31 = b * m;
    const real t34 = 8 / 5 * t23 * t4 + (2 * t28 * t3) + 8 / 3 * t31 * t13;
    const real t41 = 2 * t23 * t3 + 8 / 3 * t28 * t13 + 4 * t31 * t2;
    const real t43 = 2 * t10 + 1;
    const real t59 = 8 / 3 * t23 * t13 + 4 * t28 * t2 + 8 * t31 * L;
    const real t60 = (a * a);
    const real t63 = a * b;
    const real t66 = b * b;
    const real t75 = (t60 * t3) + 8 / 3 * t63 * t13 + 2 * t66 * t2;
    Iaa = 4 / 5 * t1 * t4 + 2 * t7 * t3 + t12 * t13 / 3;
    Iab = t17 + t19 + t12 * t2 / 2;
    Ian = t34;
    Iam = t41;
    Iba = t43 * t2 + t17 + t19;
    Ibb = 4 / 3 * t1 * t13 + 4 * t7 * t2 + 2 * t43 * L;
    Ibn = t41;
    Ibm = t59;
    Ina = t34;
    Inb = t41;
    Inn = 4 / 5 * t60 * t4 + 2 * t63 * t3 + 4 / 3 * t66 * t13;
    Inm = t75;
    Ima = t41;
    Imb = t59;
    Imn = t75;
    Imm = 4 / 3 * t60 * t13 + 4 * t63 * t2 + 4 * t66 * L;
  }
  
  if(useSadowskyApprox)
  {
    V=1.0;
    Vn=0.0;
    Vnn=0.0;
  }
  else
  {
    const real x = n * ribbonWidth / 2.;
    if (x <= -1. || x >= 1.) {
      LoggerMsg msg("Point out of constraints");
      msg.addChild("n", std::to_string(n));
      msg.addChild("w", std::to_string(ribbonWidth));
      getLogger()->Write(LoggerLevel::ERROR, "Model", msg);
    }
    if (abs(x) < 1e-5) {
      V = 1. + (1. / 3.) * x * x;
      Vn = (1. / 6.) * n * ribbonWidth * ribbonWidth;
      Vnn = (1. / 6.) * ribbonWidth * ribbonWidth;
    } else {
      const real w = ribbonWidth;
      const real t2 = n * w / 2.0;
      const real t3 = t2 + 1.0;
      const real t4 = 1.0 - t2;
      const real t5 = 1 / t4;
      const real t7 = log(t5 * t3);
      const real t8 = 1 / n;
      const real t10 = 1 / w;
      const real t13 = t4 * t4;
      const real t14 = 1 / t13;
      const real t17 = t3 * t14 * w + w * t5;
      const real t18 = 1 / t3;
      const real t19 = t17 * t18 / 2.0;
      const real t20 = t4 * t8;
      const real t21 = t20 * t10;
      const real t23 = n * n;
      const real t24 = 1 / t23;
      const real t28 = w * w;
      const real t37 = t3 * t3;
      V = t7 * t8 * t10;
      Vn = -t7 * t24 * t10 + t19 * t21;
      Vnn = (t28 * t14 + t3 / t13 / t4 * t28) * t18 * t21 / 2.0 - t17 / t37 * t20 / 4.0 - t19 * t8 / 2.0 - 2.0 * t19 * t4 * t24 * t10 + 2.0 * t7 / t23 / n * t10;
    }
  }
  Eigen::Matrix4r res;
  res<< Iaa*V        , Iab*V        , Ian*V + Ia*Vn          , Iam*V,
        Iba*V        , Ibb*V        , Ibn*V + Ib*Vn          , Ibm*V,
        Ina*V + Ia*Vn, Inb*V + Ib*Vn, Inn*V + 2*In*Vn + I*Vnn, Inm*V + Im*Vn,
        Ima*V        , Imb*V        , Imn*V + Im*Vn          , Imm*V;
        
  Eigen::Matrix4r regul;
  regul<<0.,0.,0.,0.,
         0.,0.,0.,0.,
         0., 0., etaPrimeRegularizationFactor*L, 0.,
         0.,0.,0.,0.;
  return K * res + EHNatural+regul;
}

void EBending_fiber_SW::YesIWantToUseSadowsky(bool IamStuborn)
{
  EBending_fiber_SW::useSadowskyApprox = IamStuborn;
}
