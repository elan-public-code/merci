/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "ElasticEnergy.hpp"
#include "Log/Logger.hpp"
#include <string>
#include <cmath>

#define EE_MODEL_SW 0
#define EE_MODEL_SW_FIBER 1
#define EE_MODEL_AN 2

int ElasticEnergy::useModel = EE_MODEL_SW;

void ElasticEnergy::FixPhysics(real D, real ribbonThickness, real ribbonWidth, real ribbonLength, real poissonRatio) {
  model_AN.FixPhysics(D, poissonRatio, ribbonThickness, ribbonWidth, ribbonLength);
  model_SW.FixPhysics(D, ribbonWidth, ribbonLength, poissonRatio);
}

void ElasticEnergy::FixPt(LinearScalar omega1, LinearScalar eta, std::optional<LinearScalar> naturalOmega1)
{
  switch(useModel)
  {
    case EE_MODEL_SW:
      model_SW.FixPt(omega1, eta, naturalOmega1);
      break;
    case EE_MODEL_SW_FIBER:
      model_fSW.FixPt(omega1, eta, naturalOmega1);
      break;
    case EE_MODEL_AN:
      model_AN.FixPt(omega1, eta);
      break;
    default:
      throw "Not implemented";
  }
}


real ElasticEnergy::computeEnergy()
{
  switch(useModel)
  {
    case EE_MODEL_SW:
      return model_SW.computeEnergy();
      break;
    case EE_MODEL_SW_FIBER:
      return model_fSW.computeEnergy();
      break;
    case EE_MODEL_AN:
      return model_AN.value();
      break;
    default:
      throw "Not implemented";
  }
}

Eigen::Vector4r ElasticEnergy::computeGrad()
{
  switch(useModel)
  {
    case EE_MODEL_SW:
      return model_SW.computeGrad();
      break;
    case EE_MODEL_SW_FIBER:
      return model_fSW.computeGrad();
      break;
    case EE_MODEL_AN:
      return model_AN.gradient();
      break;
    default:
      throw "Not implemented";
  }
}

Eigen::Matrix4r ElasticEnergy::computeHessian()
{
  switch(useModel)
  {
    case EE_MODEL_SW:
      return model_SW.computeHessian();
      break;
    case EE_MODEL_SW_FIBER:
      return model_fSW.computeHessian();
      break;
    case EE_MODEL_AN:
      return model_AN.hessian();
      break;
    default:
      throw "Not implemented";
  }
}

void ElasticEnergy::ChooseModel(ElasticEnergy::Model which)
{
  switch(which)
  {
    case Model::Sadowsky:
      useModel=EE_MODEL_SW;
      EBending_SW::YesIWantToUseSadowsky(true);
      break;
    case Model::Wunderlich:
      useModel=EE_MODEL_SW;
      EBending_SW::YesIWantToUseSadowsky(false);
      break;
    case Model::FiberSadowsky:
      useModel=EE_MODEL_SW_FIBER;
      EBending_fiber_SW::YesIWantToUseSadowsky(true);
      break;
    case Model::FiberWunderlich:
      useModel=EE_MODEL_SW_FIBER;
      EBending_fiber_SW::YesIWantToUseSadowsky(false);
      break;
    case Model::AudolyNeukirch:
      useModel=EE_MODEL_AN;
      break;
  }
}
