/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef ELASTICENERGY_H
#define ELASTICENERGY_H

#include <BaseCore/real.hpp>
#include <BaseCore/LinearScalar.hpp>
#include <Eigen/Dense>
#include <optional>
#include "EBending_AN.hpp"
#include "EBending_SadowWunder.hpp"
#include "EBending_fiber_SadowWunder.hpp"

//#define TEST_SUPERSIMPLE_ENERGY

class ElasticEnergy
{
public:
  void FixPhysics(real D, real ribbonThickness, real ribbonWidth, real ribbonLength, real poissonRatio);
  void FixPt(LinearScalar omega1, LinearScalar eta, std::optional<LinearScalar> naturalOmega1/* = std::nullopt*/);
  real computeEnergy();
  Eigen::Vector4r computeGrad();
  Eigen::Matrix4r computeHessian();
  enum class Model{Sadowsky, Wunderlich, FiberSadowsky, FiberWunderlich, AudolyNeukirch};
  static void ChooseModel(Model which);
private:
  static int useModel;
  EBending_AN model_AN;
  EBending_SW model_SW;
  EBending_fiber_SW model_fSW;
};

#endif // ELASTICENERGY_H


