/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "ElasticEnergy.hpp"
#include "Log/Logger.hpp"
#include <string>
#include <cmath>

#ifdef TEST_SUPERSIMPLE_ENERGY
real ElasticEnergy::computeEnergy()
{
  const real L = ribbonLength;
  const real a = omega1.A, b = omega1.B, n = eta.A, m = eta.B;

  real K;
  K = D * ribbonWidth / 2;
  
  const real an = naturalOmega1.value_or(LinearScalar(0,0)).A;
  const real bn = naturalOmega1.value_or(LinearScalar(0,0)).B;
 
  double t1 = a * a;
  double t3 = n * n;
  double t4 = L * L;
  double t5 = t4 * t4;
  double t12 = b * a;
  double t18 = a - an;
  double t19 = t18 * t18;
  double t20 = m * m;
  double t25 = b * b;
  double t32 = b - bn;
  double t41 = t32 * t32;
  return L * t5 * t3 * t1 * K / 0.5e1 + t5 * (n * m * t1 + t3 * t12) * K / 0.2e1 + L * t4 * (0.4e1 * m * n * t12 + t20 * t1 + t3 * t25 + t19) * K / 0.3e1 + t4 * (n * m * t25 + t20 * t12 + t18 * t32) * K + L * (t20 * t25 + t41) * K;

}

Eigen::Vector4r ElasticEnergy::computeGrad()
{
  Eigen::Vector4r G;
  const real L = ribbonLength;
  const real a = omega1.A, b = omega1.B, n = eta.A, m = eta.B;

  real K;
  K = D * ribbonWidth / 2;
  
  const real an = naturalOmega1.value_or(LinearScalar(0,0)).A;
  const real bn = naturalOmega1.value_or(LinearScalar(0,0)).B;
  
  double t1 = K * a;
  double t2 = (n * n);
  double t3 = L * L;
  double t4 = t3 * t3;
  double t5 = L * t4;
  double t15 = (0.4e1 * a * m * n + (2 * t2 * b)) * K;
  double t18 = m * m;
  double t27 = (0.4e1 *  b * m * n + 0.2e1 * t18 * a + 0.2e1 * a - 0.2e1 * an) * K;
  double t28 = L * t3;
  double t33 = 0.2e1 * (t18 *  b +  b - bn) * K;
  double t46 = a * a;
  double t47 = t46 * K;
  double t53 =  b * a;
  double t57 = (0.2e1 * m * t46 + 0.4e1 * n * t53) * K;
  double t62 = b * b;
  double t66 = (0.4e1 * m * t53 + 0.2e1 * n *  t62) * K;
  double t69 =  t62 * K;
  G[0] = 0.2e1 / 0.5e1 * t5 *  t2 * t1 + t4 * t15 / 0.4e1 + t28 * t27 / 0.3e1 + t3 * t33 / 0.2e1;
  G[1] = t4 *  t2 * t1 / 0.2e1 + t28 * t15 / 0.3e1 + t3 * t27 / 0.2e1 + L * t33;
  G[2] = 0.2e1 / 0.5e1 * t5 * n * t47 + t4 * t57 / 0.4e1 + t28 * t66 / 0.3e1 + t3 * m * t69;
  G[3] = t4 * n * t47 / 0.2e1 + t28 * t57 / 0.3e1 + t3 * t66 / 0.2e1 + 0.2e1 * m * L * t69;

  return G;
}

Eigen::Matrix4r ElasticEnergy::computeHessian()
{
  Eigen::Matrix4r H;
  const real L = ribbonLength;
  const real a = omega1.A, b = omega1.B, n = eta.A, m = eta.B;

  real K;
  K = D * ribbonWidth / 2;
  
  const real an = naturalOmega1.value_or(LinearScalar(0,0)).A;
  const real bn = naturalOmega1.value_or(LinearScalar(0,0)).B;
  
  double t1 = n * n;
  double t2 = t1 * K;
  double t3 = L * L;
  double t4 = t3 * t3;
  double t5 = L * t4;
  double t8 = K * m;
  double t9 = t4 * n;
  double t11 = m * m;
  double t13 = 0.2e1 * (t11 + 0.1e1) * K;
  double t14 = L * t3;
  double t20 = t14 * n;
  double t25 = t4 * t2 / 0.2e1 + 0.4e1 / 0.3e1 * t20 * t8 + t3 * t13 / 0.2e1;
  double t26 = K * a;
  double t33 = 0.4e1 * (a * m + b * n) * K;
  double t36 = K * b;
  double t40 = 0.4e1 / 0.5e1 * t5 * n * t26 + t4 * t33 / 0.4e1 + 0.4e1 / 0.3e1 * t14 * m * t36;
  double t47 = t9 * t26 + t14 * t33 / 0.3e1 + 0.2e1 * t3 * m * t36;
  double t62 = 0.4e1 / 0.3e1 * t20 * t26 + t3 * t33 / 0.2e1 + 0.4e1 * m * L * t36;
  double t63 = a * a;
  double t64 = t63 * K;
  double t69 = b * b;
  double t70 = t69 * K;
  double t80 = t4 * t64 / 0.2e1 + 0.4e1 / 0.3e1 * t14 * a * t36 + t3 * t70;
  H(0,0) = 0.2e1 / 0.5e1 * t5 * t2 + t9 * t8 + t14 * t13 / 0.3e1;
  H(0,1) = t25;
  H(0,2) = t40;
  H(0,3) = t47;
  H(1,0) = t25;
  H(1,1) = 0.2e1 / 0.3e1 * t14 * t2 + 0.2e1 * t3 * n * t8 + L * t13;
  H(1,2) = t47;
  H(1,3) = t62;
  H(2,0) = t40;
  H(2,1) = t47;
  H(2,2) = 0.2e1 / 0.5e1 * t5 * t64 + t4 * a * t36 + 0.2e1 / 0.3e1 * t14 * t70;
  H(2,3) = t80;
  H(3,0) = t47;
  H(3,1) = t62;
  H(3,2) = t80;
  H(3,3) = 0.2e1 / 0.3e1 * t14 * t64 + 0.2e1 * t3 * a * t36 + 0.2e1 * L * t70;

  return H;
}

#endif //TEST_SUPERSIMPLE_ENERGY
