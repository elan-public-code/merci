/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "RibbonSystem.hpp"
#include "MathOperations.hpp"
#include <Logger.hpp>

using namespace MathOperations;

size_t RibbonSystem::vpointSize() const { return 2*(1+sr.getNbSegments()); }
size_t RibbonSystem::vpointIDb0() const { return 0; }
size_t RibbonSystem::vpointIDm0() const { return 1; }
size_t RibbonSystem::vpointIDa ( const size_t& segmentID ) const { return 2+2*segmentID; }
size_t RibbonSystem::vpointIDn ( const size_t& segmentID ) const { return 2+2*segmentID+1; }

RibbonSystem::RibbonSystem(Eigen::Vector3r gravity):
  G(gravity)
{
}

void RibbonSystem::set( const SuperRibbon& sr ) 
{
  this->sr = sr; 
  size_t t=sr.getNbSegments();
  parts.clear();
  parts.reserve(t);
  for(size_t i=0;i<t;i++)
    parts.push_back(SegmentSystem(G, sr.getAreaDensity(), sr.getD(), sr.getThickness(), sr.getWidth(), sr.getLength(i), sr.getPoissonRatio().value_or(0.5)));
  updateParts();
}

SuperRibbon RibbonSystem::get() const { return sr; }

void RibbonSystem::setPoint ( const Eigen::VectorXr& x )
{
  int t = sr.getNbSegments();
  assert(x.size() == vpointSize());
  sr.setOmega1BOr(x[vpointIDb0()]);
  sr.setEtaMOr(x[vpointIDm0()]);
  for (int i = 0; i < t; i++) {
    sr.setOmega1A(i, x[vpointIDa(i)]);
    sr.setEtaN(i, x[vpointIDn(i)]);
  }
  updateParts();
}

void RibbonSystem::updateParts()
{
  gradPos.clear();
  gradFrames.clear();
  hessPos.clear();
  hessFrames.clear();
  size_t t=sr.getNbSegments();
  Eigen::Vector3r p=sr.getPosOr();
  Eigen::Matrix3r f=sr.getFrameOr();
  for(size_t i=0;i<t;i++)
  {
    parts.at(i).setPoint(p,f, sr.getOmega1(i), sr.getEta(i), sr.getNatOmega1(i));
    std::tie(p,f)=parts.at(i).endPosFrame();
  }
}

Eigen::Vector3r RibbonSystem::getEndPos()
{
  return parts.back().endPosFrame().first;
}

Eigen::Matrix3r RibbonSystem::getEndFrame()
{
  return parts.back().endPosFrame().second;
}

Eigen::VectorXr RibbonSystem::getPoint() const
{
  int t = sr.getNbSegments();
  Eigen::VectorXr pt(vpointSize());
  pt[vpointIDb0()] = sr.getOmega1(0).B;
  pt[vpointIDm0()] = sr.getEta(0).B;
  for (int i = 0; i < t; i++) {
    pt[vpointIDa(i)] = sr.getOmega1(i).A;
    pt[vpointIDn(i)] = sr.getEta(i).A;
  }
  return pt;
}

Eigen::VectorXr RibbonSystem::getLowerBound() const
{
  real lb = -2. / sr.getWidth();
  int t = sr.getNbSegments();
  Eigen::VectorXr pt(vpointSize());
  pt[vpointIDb0()] = -std::numeric_limits<real>::infinity();
  pt[vpointIDm0()] = -std::numeric_limits<real>::infinity();
  for (int i = 0; i < t; i++) {
    pt[vpointIDa(i)] = -std::numeric_limits<real>::infinity();
    pt[vpointIDn(i)] = lb;
  }
  return pt;
}

Eigen::VectorXr RibbonSystem::getUpperBound() const
{
  real ub = 2. / sr.getWidth();
  int t = sr.getNbSegments();
  Eigen::VectorXr pt(vpointSize());
  pt[vpointIDb0()] = std::numeric_limits<real>::infinity();
  pt[vpointIDm0()] = std::numeric_limits<real>::infinity();
  for (int i = 0; i < t; i++) {
    pt[vpointIDa(i)] = std::numeric_limits<real>::infinity();
    pt[vpointIDn(i)] = ub;
  }
  return pt;
}

void RibbonSystem::logHealthReport()
{
  LoggerMsg msg("Health repport");
  Eigen::VectorXr pt=getPoint();
  Eigen::VectorXr ub=getUpperBound();
  Eigen::VectorXr lb=getLowerBound();
  bool noMesg=true;
  for(int i=0;i<pt.size();i++)
  {
    double lerr=pt[i]-lb[i];
    if(lerr<0.)
    {
      std::stringstream ptVal,lVal;
      ptVal<<pt[i];
      lVal<<lb[i];
      auto ssmsg=msg.addChild("CRITICAL","Lower bound violation");
      ssmsg->addChild("segment",std::to_string(i));
      ssmsg->addChild("lower bound",lVal.str());
      ssmsg->addChild("value",ptVal.str());
      noMesg=false;
      continue;
    }
    if(lerr<1.e-5)
    {
      std::stringstream errVal;
      errVal<<lerr;
      auto ssmsg=msg.addChild("Touching","Lower bound");
      ssmsg->addChild("segment",std::to_string(i));
      ssmsg->addChild("distance",errVal.str());
      noMesg=false;
      continue;
    }
      
    double uerr=ub[i]-pt[i];
    if(uerr<0.)
    {
      std::stringstream ptVal,uVal;
      ptVal<<pt[i];
      uVal<<ub[i];
      auto ssmsg=msg.addChild("CRITICAL","Upper bound violation");
      ssmsg->addChild("segment",std::to_string(i));
      ssmsg->addChild("upper bound",uVal.str());
      ssmsg->addChild("value",ptVal.str());
      noMesg=false;
      continue;
    }
    if(uerr<1.e-5)
    {
      std::stringstream errVal;
      errVal<<uerr;
      auto ssmsg=msg.addChild("Touching","Upper bound");
      ssmsg->addChild("segment",std::to_string(i));
      ssmsg->addChild("distance",errVal.str());
      noMesg=false;
      continue;
    }
  }
  if(noMesg)msg.addChild("Sound","Nothing to repport");
  getLogger()->Write(LoggerLevel::INFORMATION,"RibbonSystem",msg);
}

real RibbonSystem::energy()
{
  real E=0;
  size_t t = static_cast<size_t>(sr.getNbSegments());
  for(size_t i=0;i<t;i++)
    E+=parts.at(i).energy();
  return E;
}

Eigen::VectorXr RibbonSystem::gradient()
{
  Eigen::VectorXr grad(vpointSize());
  grad.setZero();
  if(gradPos.empty())
    computeGradPosFrames();
  addGradient(grad);
  return grad;
}

Eigen::MatrixXr RibbonSystem::hessian()
{
  if(gradPos.empty())
    computeGradPosFrames();
  if(hessFrames.empty())
    computeHessPosFrames();
  Eigen::MatrixXr h(vpointSize(),vpointSize());
  h.setZero();
  addHessian(h);
  return h;
}

Eigen::VectorXr RibbonSystem::gradPosX()
{
  Eigen::VectorXr grad(vpointSize());
  grad.setZero();
  gradPos.clear();
  gradFrames.clear();
  computeGradPosFrames();
  size_t t=sr.getNbSegments();
  size_t n=vpointSize();
  for(size_t i=0;i<n;i++)
    grad[i] = gradPos.at({t,i})[2];
  return grad;
}

Eigen::MatrixXr RibbonSystem::hessPosX()
{
  size_t t=sr.getNbSegments();
  Eigen::MatrixXr hess(vpointSize(),vpointSize());
  hess.setZero();
  gradPos.clear();
  gradFrames.clear();
  hessPos.clear();
  hessFrames.clear();
  computeGradPosFrames();
  computeHessPosFrames();
  size_t n=vpointSize();
  for(size_t i=0;i<n;i++)
    for(size_t j=0;j<n;j++)
      hess(i,j)=hessPos.at({t,i,j})[2];
  return hess;
}

Eigen::VectorXr RibbonSystem::gradFrameX()
{
  Eigen::VectorXr grad(vpointSize());
  grad.setZero();
  gradPos.clear();
  gradFrames.clear();
  computeGradPosFrames();
  size_t t=sr.getNbSegments();
  size_t n=vpointSize();
  for(size_t i=0;i<n;i++)
    grad[i]=gradFrames.at({t,i})(2,0);
  return grad;
}

Eigen::MatrixXr RibbonSystem::hessFrameX()
{
  size_t t=sr.getNbSegments();
  Eigen::MatrixXr hess(vpointSize(),vpointSize());
  hess.setZero();
  gradPos.clear();
  gradFrames.clear();
  hessPos.clear();
  hessFrames.clear();
  computeGradPosFrames();
  computeHessPosFrames();
  size_t n=vpointSize();
  for(size_t i=0;i<n;i++)
    for(size_t j=0;j<n;j++)
      hess(i,j)=hessFrames.at({t,i,j})(2,0);
  return hess;
}


real RibbonSystem::posX()
{
  return parts.back().endPosFrame().first[2];
}

Eigen::Matrix<Eigen::VectorXr, 3, 1> RibbonSystem::gradEndPos()
{
  Eigen::Matrix<Eigen::VectorXr, 3, 1> grad;
  Eigen::VectorXr myZero(vpointSize());
  myZero.setZero();
  grad.setConstant(myZero);
  gradPos.clear();
  gradFrames.clear();
  computeGradPosFrames();
  size_t t=sr.getNbSegments();
  size_t n=vpointSize();
  for(size_t i=0;i<n;i++)
  {
    grad[0][i] = gradPos.at({t,i})[0];
    grad[1][i] = gradPos.at({t,i})[1];
    grad[2][i] = gradPos.at({t,i})[2];
  }
  return grad;
}

Eigen::Matrix<Eigen::VectorXr, 3, 3> RibbonSystem::gradEndFrame()
{
  Eigen::Matrix<Eigen::VectorXr, 3, 3> grad;
  Eigen::VectorXr myZero(vpointSize());
  myZero.setZero();
  grad.setConstant(myZero);
  gradPos.clear();
  gradFrames.clear();
  computeGradPosFrames();
  size_t t=sr.getNbSegments();
  size_t n=vpointSize();
  for(size_t i=0;i<n;i++)
    for(size_t idx=0;idx<9;idx++) grad(idx/3,idx%3)[i]=gradFrames.at({t,i})(idx/3,idx%3);
  return grad;
}

Eigen::Matrix<Eigen::MatrixXr, 3, 1> RibbonSystem::hessEndPos()
{
  Eigen::Matrix<Eigen::MatrixXr, 3, 1> hess;
  size_t t=sr.getNbSegments();
  Eigen::MatrixXr myZero(vpointSize(),vpointSize());
  myZero.setZero();
  hess.setConstant(myZero);
  gradPos.clear();
  gradFrames.clear();
  hessPos.clear();
  hessFrames.clear();
  computeGradPosFrames();
  computeHessPosFrames();
  size_t n=vpointSize();
  for(size_t i=0;i<n;i++)
    for(size_t j=0;j<n;j++)
    {
      hess[0](i,j)=hessPos.at({t,i,j})[0];
      hess[1](i,j)=hessPos.at({t,i,j})[1];
      hess[2](i,j)=hessPos.at({t,i,j})[2];
    }
  return hess;
}

Eigen::Matrix<Eigen::MatrixXr, 3, 3> RibbonSystem::hessEndFrame()
{
  Eigen::Matrix<Eigen::MatrixXr, 3, 3> hess;
  size_t t=sr.getNbSegments();
  Eigen::MatrixXr myZero(vpointSize(),vpointSize());
  myZero.setZero();
  hess.setConstant(myZero);
  gradPos.clear();
  gradFrames.clear();
  hessPos.clear();
  hessFrames.clear();
  computeGradPosFrames();
  computeHessPosFrames();
  size_t n=vpointSize();
  for(size_t i=0;i<n;i++)
    for(size_t j=0;j<n;j++)
      for(size_t idx=0;idx<9;idx++)hess(idx/3,idx%3)(i,j)=hessFrames.at({t,i,j})(idx/3,idx%3);
  return hess;
}

void RibbonSystem::computeGradPosFrames()
{
  Eigen::Matrix<Eigen::Matrix<real,3,1>,3,1> dPosdPos;
  dPosdPos<<Eigen::Vector3r(1.,0.,0.),Eigen::Vector3r(0.,1.,0.),Eigen::Vector3r(0.,0.,1.);
  
  size_t t = sr.getNbSegments();
  for(size_t segmentID = 1; segmentID<=t;segmentID++)
  {
    const auto partSegmentGradPos = parts.at(segmentID-1).gradPos();
    const auto partSegmentGradFrame = parts.at(segmentID-1).gradFrame();
    gradPos[{segmentID,vpointIDa(segmentID-1)}]=extractDerivation(partSegmentGradPos,0);
    gradPos[{segmentID,vpointIDn(segmentID-1)}]=extractDerivation(partSegmentGradPos,2);
    gradFrames[{segmentID,vpointIDa(segmentID-1)}]=extractDerivation(partSegmentGradFrame,0);
    gradFrames[{segmentID,vpointIDn(segmentID-1)}]=extractDerivation(partSegmentGradFrame,2);
    
    
    for(size_t j=0; j<segmentID-1;j++)
    {
      gradPos[{segmentID,vpointIDa(j)}] = sr.getLength(j) * extractDerivation(partSegmentGradPos,1)
        + compDeriv(parts.at(segmentID-1).getdPosdFrame(), gradFrames[{segmentID-1,vpointIDa(j)}]) 
        + compDeriv(dPosdPos, gradPos[{segmentID-1,vpointIDa(j)}]);
      gradPos[{segmentID,vpointIDn(j)}]=sr.getLength(j) * extractDerivation(partSegmentGradPos,3)
        + compDeriv(parts.at(segmentID-1).getdPosdFrame(), gradFrames[{segmentID-1,vpointIDn(j)}]) 
        + compDeriv(dPosdPos, gradPos[{segmentID-1,vpointIDn(j)}]);
      gradFrames[{segmentID,vpointIDa(j)}]=sr.getLength(j) * extractDerivation(partSegmentGradFrame,1) 
        + compDeriv(parts.at(segmentID-1) .getdFramedFrame(), gradFrames[{segmentID-1,vpointIDa(j)}]);
      gradFrames[{segmentID,vpointIDn(j)}]=sr.getLength(j) * extractDerivation(partSegmentGradFrame,3) 
        + compDeriv(parts.at(segmentID-1).getdFramedFrame(), gradFrames[{segmentID-1,vpointIDn(j)}]);
    }
    
    
    gradPos[{segmentID,vpointIDb0()}]=extractDerivation(partSegmentGradPos,1);
    gradPos[{segmentID,vpointIDm0()}]=extractDerivation(partSegmentGradPos,3);
    gradFrames[{segmentID,vpointIDb0()}]=extractDerivation(partSegmentGradFrame,1);
    gradFrames[{segmentID,vpointIDm0()}]=extractDerivation(partSegmentGradFrame,3);

    if(segmentID>1)
    {
      gradPos[{segmentID,vpointIDb0()}] +=
        compDeriv(parts.at(segmentID-1).getdPosdFrame(), gradFrames.at({segmentID-1,vpointIDb0()})) + compDeriv(dPosdPos, gradPos[{segmentID-1,vpointIDb0()}]);
      gradPos[{segmentID,vpointIDm0()}] +=
        compDeriv(parts.at(segmentID-1).getdPosdFrame(), gradFrames[{segmentID-1,vpointIDm0()}]) + compDeriv(dPosdPos, gradPos[{segmentID-1,vpointIDm0()}]);
      gradFrames[{segmentID,vpointIDb0()}] +=
        compDeriv(parts.at(segmentID-1).getdFramedFrame(), gradFrames[{segmentID-1,vpointIDb0()}]);
      gradFrames[{segmentID,vpointIDm0()}] +=
        compDeriv(parts.at(segmentID-1).getdFramedFrame(), gradFrames[{segmentID-1,vpointIDm0()}]);
    }
  }
}

void RibbonSystem::computeHessPosFrames()
{
  Eigen::Matrix<Eigen::Matrix<real,3,1>,3,1> dPosdPos;
  dPosdPos<<Eigen::Vector3r(1.,0.,0.),Eigen::Vector3r(0.,1.,0.),Eigen::Vector3r(0.,0.,1.);
  
  size_t t = sr.getNbSegments();
  for(size_t segmentID = 1; segmentID<=t;segmentID++)
  {
    Eigen::Matrix3r frameIinv=parts.at(segmentID-1).orPosFrame().second.inverse();
    const auto partSegmentGradPos = parts.at(segmentID-1).gradPos();
    const auto partSegmentGradFrame = parts.at(segmentID-1).gradFrame();
    
    //i,j dans le segment
    Eigen::Matrix<Eigen::Matrix4r, 3, 1> hs = parts.at(segmentID-1).gethessianPosOut();
    auto ext=[](const Eigen::Matrix<Eigen::Matrix4r, 3, 1>& in,const int i,const int j){return Eigen::Vector3r(in[0](i,j),in[1](i,j),in[2](i,j));};
    Eigen::Matrix<Eigen::Matrix4r, 3, 3> hs2 = parts.at(segmentID-1).gethessianFrameOut();
    auto ext2=[](const Eigen::Matrix<Eigen::Matrix4r, 3, 3>& in,const int k,const int l){
      Eigen::Matrix3r res;
      for(int i=0;i<9;i++)
        res(i%3,i/3)=in(i%3,i/3)(k,l);
      return res;
    };
    
    
    hessFrames[{segmentID,vpointIDa(segmentID-1),vpointIDa(segmentID-1)}]=ext2(hs2,0,0);
    hessFrames[{segmentID,vpointIDn(segmentID-1),vpointIDa(segmentID-1)}]=ext2(hs2,0,2);
    hessFrames[{segmentID,vpointIDn(segmentID-1),vpointIDn(segmentID-1)}]=ext2(hs2,2,2);
    hessPos[{segmentID,vpointIDn(segmentID-1),vpointIDn(segmentID-1)}]=ext(hs,2,2);
    hessPos[{segmentID,vpointIDn(segmentID-1),vpointIDa(segmentID-1)}]=ext(hs,0,2);
    hessPos[{segmentID,vpointIDa(segmentID-1),vpointIDa(segmentID-1)}]=ext(hs,0,0);
    if(segmentID==1)
    {
      hessFrames[{segmentID,vpointIDb0(),vpointIDb0()}]=ext2(hs2,1,1);
      hessFrames[{segmentID,vpointIDm0(),vpointIDb0()}]=ext2(hs2,1,3); 
      hessFrames[{segmentID,vpointIDm0(),vpointIDm0()}]=ext2(hs2,3,3);
      hessFrames[{segmentID,vpointIDb0(),vpointIDa(segmentID-1)}]=ext2(hs2,0,1);
      hessFrames[{segmentID,vpointIDm0(),vpointIDa(segmentID-1)}]=ext2(hs2,0,3);
      hessFrames[{segmentID,vpointIDn(segmentID-1),vpointIDb0()}]=ext2(hs2,1,2);
      hessFrames[{segmentID,vpointIDm0(),vpointIDn(segmentID-1)}]=ext2(hs2,2,3);
      hessPos[{segmentID,vpointIDb0(),vpointIDb0()}]=ext(hs,1,1);
      hessPos[{segmentID,vpointIDm0(),vpointIDb0()}]=ext(hs,1,3);
      hessPos[{segmentID,vpointIDm0(),vpointIDm0()}]=ext(hs,3,3);
      hessPos[{segmentID,vpointIDb0(),vpointIDa(segmentID-1)}]=ext(hs,0,1);
      hessPos[{segmentID,vpointIDm0(),vpointIDa(segmentID-1)}]=ext(hs,0,3);
      hessPos[{segmentID,vpointIDn(segmentID-1),vpointIDb0()}]=ext(hs,1,2);
      hessPos[{segmentID,vpointIDm0(),vpointIDn(segmentID-1)}]=ext(hs,2,3);
    }
    else
    {
      hessFrames[{segmentID,vpointIDb0(),vpointIDb0()}]=compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointIDb0(),vpointIDb0()}))
          +ext2(hs2,1,1)
          +2*gradFrames.at({segmentID-1,vpointIDb0()}) * frameIinv*extractDerivation(partSegmentGradFrame,1);
      hessFrames[{segmentID,vpointIDm0(),vpointIDb0()}]=compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointIDb0(),vpointIDm0()}))
          +ext2(hs2,1,3)
          +gradFrames.at({segmentID-1,vpointIDb0()}) * frameIinv*extractDerivation(partSegmentGradFrame,3)
          +gradFrames.at({segmentID-1,vpointIDm0()}) * frameIinv*extractDerivation(partSegmentGradFrame,1);
      hessFrames[{segmentID,vpointIDm0(),vpointIDm0()}]=compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointIDm0(),vpointIDm0()}))
          +ext2(hs2,3,3)
          +2*gradFrames.at({segmentID-1,vpointIDm0()}) * frameIinv*extractDerivation(partSegmentGradFrame,3);
      hessFrames[{segmentID,vpointIDb0(),vpointIDa(segmentID-1)}]=ext2(hs2,0,1)
          +gradFrames.at({segmentID-1,vpointIDb0()}) * frameIinv*extractDerivation(partSegmentGradFrame,0);
      hessFrames[{segmentID,vpointIDm0(),vpointIDa(segmentID-1)}]=ext2(hs2,0,3)
          +gradFrames.at({segmentID-1,vpointIDm0()}) * frameIinv*extractDerivation(partSegmentGradFrame,0);
      hessFrames[{segmentID,vpointIDn(segmentID-1),vpointIDb0()}]=ext2(hs2,1,2)
          +gradFrames.at({segmentID-1,vpointIDb0()}) * frameIinv*extractDerivation(partSegmentGradFrame,2);
      hessFrames[{segmentID,vpointIDm0(),vpointIDn(segmentID-1)}]=ext2(hs2,2,3)
          +gradFrames.at({segmentID-1,vpointIDm0()}) * frameIinv*extractDerivation(partSegmentGradFrame,2);
      hessPos[{segmentID,vpointIDb0(),vpointIDb0()}]=compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointIDb0(),vpointIDb0()}))
          + compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointIDb0(),vpointIDb0()}))
          +ext(hs,1,1)
          +2*gradFrames.at({segmentID-1,vpointIDb0()}) * frameIinv*extractDerivation(partSegmentGradPos,1);
      hessPos[{segmentID,vpointIDm0(),vpointIDb0()}]=compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointIDb0(),vpointIDm0()}))
        +compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointIDb0(),vpointIDm0()}))
          +ext(hs,1,3)
          +gradFrames.at({segmentID-1,vpointIDb0()}) * frameIinv*extractDerivation(partSegmentGradPos,3)
          +gradFrames.at({segmentID-1,vpointIDm0()}) * frameIinv*extractDerivation(partSegmentGradPos,1);
      hessPos[{segmentID,vpointIDm0(),vpointIDm0()}]=compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointIDm0(),vpointIDm0()}))
          + compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointIDm0(),vpointIDm0()}))
          +ext(hs,3,3)
          +2*gradFrames.at({segmentID-1,vpointIDm0()}) * frameIinv*extractDerivation(partSegmentGradPos,3);
      hessPos[{segmentID,vpointIDb0(),vpointIDa(segmentID-1)}]=ext(hs,0,1)
        +gradFrames.at({segmentID-1,vpointIDb0()}) * frameIinv*extractDerivation(partSegmentGradPos,0);
      hessPos[{segmentID,vpointIDm0(),vpointIDa(segmentID-1)}]=ext(hs,0,3)
        +gradFrames.at({segmentID-1,vpointIDm0()}) * frameIinv*extractDerivation(partSegmentGradPos,0);
      hessPos[{segmentID,vpointIDn(segmentID-1),vpointIDb0()}]=ext(hs,1,2)
        +gradFrames.at({segmentID-1,vpointIDb0()}) * frameIinv*extractDerivation(partSegmentGradPos,2);
      hessPos[{segmentID,vpointIDm0(),vpointIDn(segmentID-1)}]=ext(hs,2,3)
        +gradFrames.at({segmentID-1,vpointIDm0()}) * frameIinv*extractDerivation(partSegmentGradPos,2);
    }
    
    
    hessFrames[{segmentID,vpointIDa(segmentID-1),vpointIDb0()}]=hessFrames[{segmentID,vpointIDb0(),vpointIDa(segmentID-1)}];
    hessFrames[{segmentID,vpointIDa(segmentID-1),vpointIDn(segmentID-1)}]=hessFrames[{segmentID,vpointIDn(segmentID-1),vpointIDa(segmentID-1)}];
    hessFrames[{segmentID,vpointIDa(segmentID-1),vpointIDm0()}]=hessFrames[{segmentID,vpointIDm0(),vpointIDa(segmentID-1)}];
    hessFrames[{segmentID,vpointIDb0(),vpointIDn(segmentID-1)}]=hessFrames[{segmentID,vpointIDn(segmentID-1),vpointIDb0()}];
    hessFrames[{segmentID,vpointIDb0(),vpointIDm0()}]=hessFrames[{segmentID,vpointIDm0(),vpointIDb0()}];
    hessFrames[{segmentID,vpointIDn(segmentID-1),vpointIDm0()}]=hessFrames[{segmentID,vpointIDm0(),vpointIDn(segmentID-1)}];
    hessPos[{segmentID,vpointIDa(segmentID-1),vpointIDb0()}]=hessPos[{segmentID,vpointIDb0(),vpointIDa(segmentID-1)}];
    hessPos[{segmentID,vpointIDa(segmentID-1),vpointIDn(segmentID-1)}]=hessPos[{segmentID,vpointIDn(segmentID-1),vpointIDa(segmentID-1)}];
    hessPos[{segmentID,vpointIDa(segmentID-1),vpointIDm0()}]=hessPos[{segmentID,vpointIDm0(),vpointIDa(segmentID-1)}];
    hessPos[{segmentID,vpointIDb0(),vpointIDn(segmentID-1)}]=hessPos[{segmentID,vpointIDn(segmentID-1),vpointIDb0()}];
    hessPos[{segmentID,vpointIDb0(),vpointIDm0()}]=hessPos[{segmentID,vpointIDm0(),vpointIDb0()}];
    hessPos[{segmentID,vpointIDn(segmentID-1),vpointIDm0()}]=hessPos[{segmentID,vpointIDm0(),vpointIDn(segmentID-1)}];
    
    //i ou(exclusif) j dans le segment
    for(size_t i=0;i<segmentID-1;i++)
    {
      
      hessPos[{segmentID,vpointIDa(i),vpointIDa(segmentID-1)}] = gradFrames.at({segmentID-1,vpointIDa(i)}) * frameIinv * extractDerivation(partSegmentGradPos,0)
        + sr.getLength(i)* ext(hs,0,1);
      hessPos[{segmentID,vpointIDa(i),vpointIDn(segmentID-1)}] = gradFrames.at({segmentID-1,vpointIDa(i)}) * frameIinv * extractDerivation(partSegmentGradPos,2)
        + sr.getLength(i)* ext(hs,2,1);
      hessPos[{segmentID,vpointIDn(i),vpointIDa(segmentID-1)}] = gradFrames.at({segmentID-1,vpointIDn(i)}) * frameIinv * extractDerivation(partSegmentGradPos,0)
        + sr.getLength(i)* ext(hs,0,3);
      hessPos[{segmentID,vpointIDn(i),vpointIDn(segmentID-1)}] = gradFrames.at({segmentID-1,vpointIDn(i)}) * frameIinv * extractDerivation(partSegmentGradPos,2)
        + sr.getLength(i)* ext(hs,2,3);
                                                                  
      hessPos[{segmentID,vpointIDa(i),vpointIDb0()}] =  gradFrames.at({segmentID-1,vpointIDa(i)}) * frameIinv*extractDerivation(partSegmentGradPos,1)
        + sr.getLength(i)*ext(hs,1,1) +sr.getLength(i)*gradFrames.at({segmentID-1,vpointIDb0()})*frameIinv*extractDerivation(partSegmentGradPos,1)
        + compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointIDa(i),vpointIDb0()}))
        + compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointIDa(i),vpointIDb0()}));
      
      hessPos[{segmentID,vpointIDa(i),vpointIDm0()}] =  gradFrames.at({segmentID-1,vpointIDa(i)}) * frameIinv*extractDerivation(partSegmentGradPos,3)
        + sr.getLength(i)*ext(hs,1,3) +sr.getLength(i)*gradFrames.at({segmentID-1,vpointIDm0()})*frameIinv*extractDerivation(partSegmentGradPos,1)
        + compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointIDa(i),vpointIDm0()}))
        + compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointIDa(i),vpointIDm0()}));
        
      hessPos[{segmentID,vpointIDn(i),vpointIDb0()}] =    gradFrames.at({segmentID-1,vpointIDn(i)}) * frameIinv*extractDerivation(partSegmentGradPos,1)
        + sr.getLength(i)*ext(hs,3,1) +sr.getLength(i)*gradFrames.at({segmentID-1,vpointIDb0()})*frameIinv*extractDerivation(partSegmentGradPos,3)
        + compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointIDn(i),vpointIDb0()}))
        + compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointIDn(i),vpointIDb0()}));
        
      hessPos[{segmentID,vpointIDn(i),vpointIDm0()}] =  gradFrames.at({segmentID-1,vpointIDn(i)}) * frameIinv*extractDerivation(partSegmentGradPos,3)
        + sr.getLength(i)*ext(hs,3,3) +sr.getLength(i)*gradFrames.at({segmentID-1,vpointIDm0()})*frameIinv*extractDerivation(partSegmentGradPos,3)
        + compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointIDn(i),vpointIDm0()}))
        + compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointIDn(i),vpointIDm0()}));
                                                          
                                                          
      hessFrames[{segmentID,vpointIDa(i),vpointIDa(segmentID-1)}] =  gradFrames.at({segmentID-1,vpointIDa(i)}) * frameIinv*extractDerivation(partSegmentGradFrame,0)
        +sr.getLength(i)*ext2(hs2,0,1);
      hessFrames[{segmentID,vpointIDa(i),vpointIDn(segmentID-1)}] =  gradFrames.at({segmentID-1,vpointIDa(i)}) * frameIinv*extractDerivation(partSegmentGradFrame,2)
        +sr.getLength(i)*ext2(hs2,2,1);
      hessFrames[{segmentID,vpointIDn(i),vpointIDa(segmentID-1)}] =  gradFrames.at({segmentID-1,vpointIDn(i)}) * frameIinv*extractDerivation(partSegmentGradFrame,0)
        +sr.getLength(i)*ext2(hs2,0,3);
      hessFrames[{segmentID,vpointIDn(i),vpointIDn(segmentID-1)}] =  gradFrames.at({segmentID-1,vpointIDn(i)}) * frameIinv*extractDerivation(partSegmentGradFrame,2)
        +sr.getLength(i)*ext2(hs2,2,3);
                                                                  
      hessFrames[{segmentID,vpointIDa(i),vpointIDb0()}] = gradFrames.at({segmentID-1,vpointIDa(i)}) * frameIinv*extractDerivation(partSegmentGradFrame,1)
        + sr.getLength(i)*ext2(hs2,1,1) +sr.getLength(i)*gradFrames.at({segmentID-1,vpointIDb0()})*frameIinv*extractDerivation(partSegmentGradFrame,1)
        + compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointIDa(i),vpointIDb0()}));
        
      hessFrames[{segmentID,vpointIDa(i),vpointIDm0()}] =  gradFrames.at({segmentID-1,vpointIDa(i)}) * frameIinv*extractDerivation(partSegmentGradFrame,3)
        + sr.getLength(i)*ext2(hs2,1,3) +sr.getLength(i)*gradFrames.at({segmentID-1,vpointIDm0()})*frameIinv*extractDerivation(partSegmentGradFrame,1)
        + compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointIDa(i),vpointIDm0()}));
        
      hessFrames[{segmentID,vpointIDn(i),vpointIDb0()}] =  gradFrames.at({segmentID-1,vpointIDn(i)}) * frameIinv*extractDerivation(partSegmentGradFrame,1)
        + sr.getLength(i)*ext2(hs2,3,1) +sr.getLength(i)*gradFrames.at({segmentID-1,vpointIDb0()})*frameIinv*extractDerivation(partSegmentGradFrame,3)
        + compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointIDn(i),vpointIDb0()}));
      
      hessFrames[{segmentID,vpointIDn(i),vpointIDm0()}] =  gradFrames.at({segmentID-1,vpointIDn(i)}) * frameIinv*extractDerivation(partSegmentGradFrame,3)
        + sr.getLength(i)*ext2(hs2,3,3) +sr.getLength(i)*gradFrames.at({segmentID-1,vpointIDm0()})*frameIinv*extractDerivation(partSegmentGradFrame,3)
        + compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointIDn(i),vpointIDm0()}));
                                                          
      hessPos[{segmentID,vpointIDa(segmentID-1),vpointIDa(i)}] = hessPos.at({segmentID,vpointIDa(i),vpointIDa(segmentID-1)});
      hessPos[{segmentID,vpointIDn(segmentID-1),vpointIDa(i)}] = hessPos.at({segmentID,vpointIDa(i),vpointIDn(segmentID-1)});
      hessPos[{segmentID,vpointIDa(segmentID-1),vpointIDn(i)}] = hessPos.at({segmentID,vpointIDn(i),vpointIDa(segmentID-1)});
      hessPos[{segmentID,vpointIDn(segmentID-1),vpointIDn(i)}] = hessPos.at({segmentID,vpointIDn(i),vpointIDn(segmentID-1)});
      hessPos[{segmentID,vpointIDb0(),vpointIDa(i)}] = hessPos.at({segmentID,vpointIDa(i),vpointIDb0()});
      hessPos[{segmentID,vpointIDm0(),vpointIDa(i)}] = hessPos.at({segmentID,vpointIDa(i),vpointIDm0()});
      hessPos[{segmentID,vpointIDb0(),vpointIDn(i)}] = hessPos.at({segmentID,vpointIDn(i),vpointIDb0()});
      hessPos[{segmentID,vpointIDm0(),vpointIDn(i)}] = hessPos.at({segmentID,vpointIDn(i),vpointIDm0()});
      
      hessFrames[{segmentID,vpointIDa(segmentID-1),vpointIDa(i)}] = hessFrames.at({segmentID,vpointIDa(i),vpointIDa(segmentID-1)});
      hessFrames[{segmentID,vpointIDn(segmentID-1),vpointIDa(i)}] = hessFrames.at({segmentID,vpointIDa(i),vpointIDn(segmentID-1)});
      hessFrames[{segmentID,vpointIDa(segmentID-1),vpointIDn(i)}] = hessFrames.at({segmentID,vpointIDn(i),vpointIDa(segmentID-1)});
      hessFrames[{segmentID,vpointIDn(segmentID-1),vpointIDn(i)}] = hessFrames.at({segmentID,vpointIDn(i),vpointIDn(segmentID-1)});
      hessFrames[{segmentID,vpointIDb0(),vpointIDa(i)}] = hessFrames.at({segmentID,vpointIDa(i),vpointIDb0()});
      hessFrames[{segmentID,vpointIDm0(),vpointIDa(i)}] = hessFrames.at({segmentID,vpointIDa(i),vpointIDm0()});
      hessFrames[{segmentID,vpointIDb0(),vpointIDn(i)}] = hessFrames.at({segmentID,vpointIDn(i),vpointIDb0()});
      hessFrames[{segmentID,vpointIDm0(),vpointIDn(i)}] = hessFrames.at({segmentID,vpointIDn(i),vpointIDm0()});
    }
    //i,j non dans le segment
    for(size_t i=0;i<segmentID-1;i++)
    {
      for(size_t j=0;j<segmentID-1;j++)
      {
        hessPos[{segmentID,vpointIDa(i),vpointIDa(j)}]=compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointIDa(i),vpointIDa(j)}))
          +compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointIDa(i),vpointIDa(j)}))
          +sr.getLength(i)*sr.getLength(j)*ext(hs,1,1)
          +sr.getLength(i)*gradFrames.at({segmentID-1,vpointIDa(j)})*frameIinv*extractDerivation(partSegmentGradPos,1)
          +sr.getLength(j)*gradFrames.at({segmentID-1,vpointIDa(i)})*frameIinv*extractDerivation(partSegmentGradPos,1);
        
        hessPos[{segmentID,vpointIDa(i),vpointIDn(j)}]=compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointIDa(i),vpointIDn(j)}))
          +compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointIDa(i),vpointIDn(j)}))
          +sr.getLength(i)*sr.getLength(j)*ext(hs,1,3)
          +sr.getLength(i)*gradFrames.at({segmentID-1,vpointIDn(j)})*frameIinv*extractDerivation(partSegmentGradPos,1)
          +sr.getLength(j)*gradFrames.at({segmentID-1,vpointIDa(i)})*frameIinv*extractDerivation(partSegmentGradPos,3);
        
        hessPos[{segmentID,vpointIDn(i),vpointIDa(j)}]=compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointIDn(i),vpointIDa(j)}))
          +compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointIDn(i),vpointIDa(j)}))
          +sr.getLength(i)*sr.getLength(j)*ext(hs,3,1)
          +sr.getLength(i)*gradFrames.at({segmentID-1,vpointIDa(j)})*frameIinv*extractDerivation(partSegmentGradPos,3)
          +sr.getLength(j)*gradFrames.at({segmentID-1,vpointIDn(i)})*frameIinv*extractDerivation(partSegmentGradPos,1);
        
        hessPos[{segmentID,vpointIDn(i),vpointIDn(j)}]=compDeriv(parts.at(segmentID-1).getdPosdFrame(),hessFrames.at({segmentID-1,vpointIDn(i),vpointIDn(j)}))
          +compDeriv(dPosdPos,hessPos.at({segmentID-1,vpointIDn(i),vpointIDn(j)}))
          +sr.getLength(i)*sr.getLength(j)*ext(hs,3,3)
          +sr.getLength(i)*gradFrames.at({segmentID-1,vpointIDn(j)})*frameIinv*extractDerivation(partSegmentGradPos,3)
          +sr.getLength(j)*gradFrames.at({segmentID-1,vpointIDn(i)})*frameIinv*extractDerivation(partSegmentGradPos,3);
          
          
        hessFrames[{segmentID,vpointIDa(i),vpointIDa(j)}]=compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointIDa(i),vpointIDa(j)}))
          +sr.getLength(i)*sr.getLength(j)*ext2(hs2,1,1)
          +sr.getLength(i)*gradFrames.at({segmentID-1,vpointIDa(j)})*frameIinv*extractDerivation(partSegmentGradFrame,1)
          +sr.getLength(j)*gradFrames.at({segmentID-1,vpointIDa(i)})*frameIinv*extractDerivation(partSegmentGradFrame,1);
        
        hessFrames[{segmentID,vpointIDa(i),vpointIDn(j)}]=compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointIDa(i),vpointIDn(j)}))
          +sr.getLength(i)*sr.getLength(j)*ext2(hs2,1,3)
          +sr.getLength(i)*gradFrames.at({segmentID-1,vpointIDn(j)})*frameIinv*extractDerivation(partSegmentGradFrame,1)
          +sr.getLength(j)*gradFrames.at({segmentID-1,vpointIDa(i)})*frameIinv*extractDerivation(partSegmentGradFrame,3);
        
        hessFrames[{segmentID,vpointIDn(i),vpointIDa(j)}]=compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointIDn(i),vpointIDa(j)}))
          +sr.getLength(i)*sr.getLength(j)*ext2(hs2,3,1)
          +sr.getLength(i)*gradFrames.at({segmentID-1,vpointIDa(j)})*frameIinv*extractDerivation(partSegmentGradFrame,3)
          +sr.getLength(j)*gradFrames.at({segmentID-1,vpointIDn(i)})*frameIinv*extractDerivation(partSegmentGradFrame,1);
        
        hessFrames[{segmentID,vpointIDn(i),vpointIDn(j)}]=compDeriv(parts.at(segmentID-1).getdFramedFrame(),hessFrames.at({segmentID-1,vpointIDn(i),vpointIDn(j)}))
          +sr.getLength(i)*sr.getLength(j)*ext2(hs2,3,3)
          +sr.getLength(i)*gradFrames.at({segmentID-1,vpointIDn(j)})*frameIinv*extractDerivation(partSegmentGradFrame,3)
          +sr.getLength(j)*gradFrames.at({segmentID-1,vpointIDn(i)})*frameIinv*extractDerivation(partSegmentGradFrame,3);
      }
    }
    
  }
}

void RibbonSystem::addGradient( Eigen::VectorXr& grad)
{
  size_t t=sr.getNbSegments();
  for(size_t segmentID=0;segmentID<t;segmentID++)
  {
    Eigen::Vector4r tmp = parts.at(segmentID).gradient();
    grad[vpointIDa(segmentID)]+=tmp[0];
    grad[vpointIDn(segmentID)]+=tmp[2];
    
    const Eigen::Matrix3r dEnergydFrameSegment = parts.at(segmentID).getdEnergydFrameOut();
    const Eigen::Vector3r dEnergydPosSegment = parts.at(segmentID).getdEnergydPosOut();
    for(size_t j=0;j<segmentID;j++)
    {
      grad[vpointIDa(j)]+=sr.getLength(j)*tmp[1];
      grad[vpointIDn(j)]+=sr.getLength(j)*tmp[3];
      
      grad[vpointIDn(j)] += 
        contractGeneralDotP(dEnergydFrameSegment, gradFrames.at({segmentID,vpointIDn(j)})) 
        + dEnergydPosSegment.dot(gradPos.at({segmentID,vpointIDn(j)}));
      grad[vpointIDa(j)] += 
        contractGeneralDotP(dEnergydFrameSegment, gradFrames.at({segmentID,vpointIDa(j)})) 
        + dEnergydPosSegment.dot(gradPos.at({segmentID,vpointIDa(j)}));
    }
  }
  
  
  {
    int t = sr.getNbSegments();
    grad[vpointIDb0()]=0.;
    grad[vpointIDm0()]=0.;
    for (int i = t - 1; i > 0; i--) {
      real dpreEdbSegment = parts.at(i).gradient()[1];
      real dpreEdmSegment = parts.at(i).gradient()[3];
      real gradIb = 
        dpreEdbSegment 
        + contractGeneralDotP(parts.at(i).getdEnergydFrameOut(), gradFrames.at({i,vpointIDb0()})) 
        + parts.at(i).getdEnergydPosOut().dot(gradPos.at({i,vpointIDb0()}));
      grad[vpointIDb0()] += gradIb;
      real gradIm = 
        dpreEdmSegment 
        + contractGeneralDotP(parts.at(i).getdEnergydFrameOut(), gradFrames.at({i,vpointIDm0()})) 
        + parts.at(i).getdEnergydPosOut().dot(gradPos.at({i,vpointIDm0()}));
      grad[vpointIDm0()] += gradIm;
    }
    grad[vpointIDb0()] += parts.at(0).gradient()[1];
    grad[vpointIDm0()] += parts.at(0).gradient()[3];
      
  }
}

void RibbonSystem::addHessian(Eigen::MatrixXr& mat)
{
  size_t t=sr.getNbSegments();
  for(size_t segmentID=1;segmentID<=t;segmentID++)
  {
    Eigen::MatrixXr hpart;
    hpart.resizeLike(mat);
    hpart.setZero();
//     const auto partSegmentGradPos = parts.at(segmentID-1).gradPos();
//     const auto partSegmentGradFrame = parts.at(segmentID-1).gradFrame();
    
    //i,j dans le segment
    Eigen::Matrix4r hs = parts.at(segmentID-1).hessian();
    
    hpart(vpointIDn(segmentID-1),vpointIDn(segmentID-1))=hs(2,2);
    hpart(vpointIDn(segmentID-1),vpointIDa(segmentID-1))=hs(0,2);
    hpart(vpointIDa(segmentID-1),vpointIDa(segmentID-1))=hs(0,0);
    if(segmentID==1)
    {
      hpart(vpointIDb0(),vpointIDb0())=hs(1,1);
      hpart(vpointIDm0(),vpointIDb0())=hs(1,3);
      hpart(vpointIDm0(),vpointIDm0())=hs(3,3);
      hpart(vpointIDb0(),vpointIDa(segmentID-1))=hs(0,1);
      hpart(vpointIDm0(),vpointIDa(segmentID-1))=hs(0,3);
      hpart(vpointIDn(segmentID-1),vpointIDb0())=hs(1,2);
      hpart(vpointIDm0(),vpointIDn(segmentID-1))=hs(2,3);
    }
    else
    {
      hpart(vpointIDb0(),vpointIDb0()) = contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointIDb0(),vpointIDb0()}))
           + parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointIDb0(),vpointIDb0()}))
           +hs(1,1)
           +2.0* parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDb0()}) ,1);
       
      hpart(vpointIDm0(),vpointIDb0()) = contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointIDb0(),vpointIDm0()}))
           + parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointIDb0(),vpointIDm0()}))
           +hs(1,3)
           +parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDb0()}) ,3)
           +parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDm0()}) ,1);
           
      hpart(vpointIDm0(),vpointIDm0()) = contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointIDm0(),vpointIDm0()}))
           + parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointIDm0(),vpointIDm0()}))
           +hs(3,3)
           +2.0*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDm0()}) ,3);
           
      hpart(vpointIDb0(),vpointIDa(segmentID-1))=hs(0,1)
           +parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDb0()}) ,0);
      hpart(vpointIDm0(),vpointIDa(segmentID-1))=hs(0,3)
           +parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDm0()}) ,0);
      hpart(vpointIDn(segmentID-1),vpointIDb0())=hs(1,2)
           +parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDb0()}) ,2);
      hpart(vpointIDm0(),vpointIDn(segmentID-1))=hs(2,3)
           +parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDm0()}) ,2);
    }
    
    hpart(vpointIDa(segmentID-1),vpointIDb0())=hpart(vpointIDb0(),vpointIDa(segmentID-1));
    hpart(vpointIDa(segmentID-1),vpointIDn(segmentID-1))=hpart(vpointIDn(segmentID-1),vpointIDa(segmentID-1));
    hpart(vpointIDa(segmentID-1),vpointIDm0())=hpart(vpointIDm0(),vpointIDa(segmentID-1));
    hpart(vpointIDb0(),vpointIDn(segmentID-1))=hpart(vpointIDn(segmentID-1),vpointIDb0());
    hpart(vpointIDb0(),vpointIDm0())=hpart(vpointIDm0(),vpointIDb0());
    hpart(vpointIDn(segmentID-1),vpointIDm0())=hpart(vpointIDm0(),vpointIDn(segmentID-1));
    
    //i ou(exclusif) j dans le segment
    for(size_t i=0;i<segmentID-1;i++)
    { 
      hpart(vpointIDa(i),vpointIDa(segmentID-1)) = parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDa(i)}) ,0)
         + sr.getLength(i)* hs(0,1);
      hpart(vpointIDa(i),vpointIDn(segmentID-1)) = parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDa(i)}) ,2)
        + sr.getLength(i)* hs(2,1);
      hpart(vpointIDn(i),vpointIDa(segmentID-1)) = parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDn(i)}) ,0)
        + sr.getLength(i)* hs(0,3);
      hpart(vpointIDn(i),vpointIDn(segmentID-1)) = parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDn(i)}) ,2)
        + sr.getLength(i)* hs(2,3);
                                                                  
      hpart(vpointIDa(i),vpointIDb0()) =  parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDa(i)}) ,1)
        + sr.getLength(i)*hs(1,1) 
        + sr.getLength(i)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDb0()}) ,1)
        + contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointIDa(i),vpointIDb0()}))
        + parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointIDa(i),vpointIDb0()}));
      
      hpart(vpointIDa(i),vpointIDm0()) =  parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDa(i)}) ,3)
        + sr.getLength(i)*hs(1,3) 
        + sr.getLength(i)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDm0()}) ,1)
        + contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointIDa(i),vpointIDm0()}))
        + parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointIDa(i),vpointIDm0()}));
        
      hpart(vpointIDn(i),vpointIDb0()) =  parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDn(i)}) ,1)
        + sr.getLength(i)*hs(3,1) 
        + sr.getLength(i)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDb0()}) ,3)
        + contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointIDn(i),vpointIDb0()}))
        + parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointIDn(i),vpointIDb0()}));
        
      hpart(vpointIDn(i),vpointIDm0()) = parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDn(i)}) ,3)
        + sr.getLength(i)*hs(3,3) 
        + sr.getLength(i)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDm0()}) ,3)
        + contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointIDn(i),vpointIDm0()}))
        + parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointIDn(i),vpointIDm0()}));
                                                          
                                                          
      hpart(vpointIDa(segmentID-1),vpointIDa(i)) = hpart(vpointIDa(i),vpointIDa(segmentID-1));
      hpart(vpointIDn(segmentID-1),vpointIDa(i)) = hpart(vpointIDa(i),vpointIDn(segmentID-1));
      hpart(vpointIDa(segmentID-1),vpointIDn(i)) = hpart(vpointIDn(i),vpointIDa(segmentID-1));
      hpart(vpointIDn(segmentID-1),vpointIDn(i)) = hpart(vpointIDn(i),vpointIDn(segmentID-1));
      hpart(vpointIDb0(),vpointIDa(i)) = hpart(vpointIDa(i),vpointIDb0());
      hpart(vpointIDm0(),vpointIDa(i)) = hpart(vpointIDa(i),vpointIDm0());
      hpart(vpointIDb0(),vpointIDn(i)) = hpart(vpointIDn(i),vpointIDb0());
      hpart(vpointIDm0(),vpointIDn(i)) = hpart(vpointIDn(i),vpointIDm0());
      
    }
    //i,j non dans le segment
    for(size_t i=0;i<segmentID-1;i++)
    {
      for(size_t j=0;j<segmentID-1;j++)
      {
        hpart(vpointIDa(i),vpointIDa(j))=contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointIDa(i),vpointIDa(j)}))
          +parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointIDa(i),vpointIDa(j)}))
          +sr.getLength(i)*sr.getLength(j)*hs(1,1)
          +sr.getLength(i)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDa(j)}) ,1)
          +sr.getLength(j)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDa(i)}) ,1);
        
        hpart(vpointIDa(i),vpointIDn(j))=contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointIDa(i),vpointIDn(j)}))
          +parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointIDa(i),vpointIDn(j)}))
          +sr.getLength(i)*sr.getLength(j)*hs(1,3)
          +sr.getLength(i)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDn(j)}) ,1)
          +sr.getLength(j)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDa(i)}) ,3);
        
        hpart(vpointIDn(i),vpointIDa(j))=contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointIDn(i),vpointIDa(j)}))
          +parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointIDn(i),vpointIDa(j)}))
          +sr.getLength(i)*sr.getLength(j)*hs(3,1)
          +sr.getLength(i)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDa(j)}) ,3)
          +sr.getLength(j)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDn(i)}) ,1);
        
        hpart(vpointIDn(i),vpointIDn(j))=contractGeneralDotP(parts.at(segmentID-1).getdEnergydFrameOut(),hessFrames.at({segmentID-1,vpointIDn(i),vpointIDn(j)}))
          +parts.at(segmentID-1).getdEnergydPosOut().dot(hessPos.at({segmentID-1,vpointIDn(i),vpointIDn(j)}))
          +sr.getLength(i)*sr.getLength(j)*hs(3,3)
          +sr.getLength(i)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDn(j)}) ,3)
          +sr.getLength(j)*parts.at(segmentID-1).getd2EdFramedktimesdFramedz(gradFrames.at({segmentID-1,vpointIDn(i)}) ,3);
        
      }
    }
    mat+=hpart;
  }
}

