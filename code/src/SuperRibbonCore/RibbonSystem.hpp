/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef RIBBONSYSTEM_H
#define RIBBONSYSTEM_H

#include "SegmentSystem.hpp"
#include "SuperRibbon.hpp"
#include <tuple>

class RibbonSystem
{
public:
  RibbonSystem(Eigen::Vector3r gravity);
  void set(const SuperRibbon& sr);
  SuperRibbon get() const;
  void setPoint(const Eigen::VectorXr& x);
  Eigen::VectorXr getPoint() const;
  
  Eigen::VectorXr getUpperBound() const;
  Eigen::VectorXr getLowerBound() const;
  
  void logHealthReport();
  
  size_t vpointSize() const;
  size_t vpointIDb0() const;
  size_t vpointIDm0() const;
  size_t vpointIDa( const size_t& segmentID ) const;
  size_t vpointIDn( const size_t& segmentID ) const;
  
  real energy();
  Eigen::VectorXr gradient();
  Eigen::MatrixXr hessian();
  
  Eigen::Vector3r getEndPos();
  Eigen::Matrix3r getEndFrame();
  
  //WARNING just for test purposes
  Eigen::MatrixXr hessPosX();
  Eigen::VectorXr gradPosX();
  Eigen::MatrixXr hessFrameX();
  Eigen::VectorXr gradFrameX();
  real posX();
  
  Eigen::Matrix<Eigen::MatrixXr,3,1> hessEndPos();
  Eigen::Matrix<Eigen::VectorXr,3,1> gradEndPos();
  Eigen::Matrix<Eigen::MatrixXr,3,3> hessEndFrame();
  Eigen::Matrix<Eigen::VectorXr,3,3> gradEndFrame();
private:
  std::vector<SegmentSystem> parts;
  SuperRibbon sr;
  Eigen::Vector3r G;
  
  void updateParts();
  void computeGradPosFrames();
  void computeHessPosFrames();
  void addGradient(Eigen::VectorXr& grad);
  void addHessian(Eigen::MatrixXr& mat);
  
  std::map<std::pair<size_t,size_t>, Eigen::Vector3r> gradPos;
  std::map<std::pair<size_t,size_t>, Eigen::Matrix3r> gradFrames;
  std::map<std::tuple<size_t,size_t,size_t>, Eigen::Vector3r> hessPos;
  std::map<std::tuple<size_t,size_t,size_t>, Eigen::Matrix3r> hessFrames;
};

#endif // RIBBONSYSTEM_H
