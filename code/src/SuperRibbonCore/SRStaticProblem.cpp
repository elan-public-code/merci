/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "SRStaticProblem.hpp"

SRStaticProblem::SRStaticProblem(RibbonSystem sys)
  : cppoptlib::BoundedProblem<real>(sys.getLowerBound(), sys.getUpperBound()),
    sys(new RibbonSystem(sys))
{
}

real SRStaticProblem::value(const cppoptlib::Problem < real, -1 >::TVector &pt)
{
  sys->setPoint(pt);
  return sys->energy();
}

void SRStaticProblem::gradient(const cppoptlib::Problem < real, -1 >::TVector &pt, cppoptlib::Problem < real, -1 >::TVector &res)
{
  sys->setPoint(pt);
  res=sys->gradient();
}

void SRStaticProblem::hessian(const cppoptlib::Problem<real, -1>::TVector& pt, cppoptlib::Problem<real, -1>::THessian& res)
{
  sys->setPoint(pt);
  res=sys->hessian();
}

bool SRStaticProblem::callback(const cppoptlib::Criteria<cppoptlib::Problem<real, -1>::Scalar>& state, const cppoptlib::Problem<real, -1>::TVector& x)
{
  return true;
}


void SRStaticProblem::setPoint(const cppoptlib::Problem < real, -1 >::TVector &pt)
{
  sys->setPoint(pt);
}

cppoptlib::Problem < real, -1 >::TVector SRStaticProblem::getPoint()
{
  return sys->getPoint();
}

SuperRibbon SRStaticProblem::get()
{
  return sys->get();
}
