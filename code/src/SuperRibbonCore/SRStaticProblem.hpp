/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef SRSTATICPROBLEM_H
#define SRSTATICPROBLEM_H

#include <cppoptlib/meta.h>
#include <cppoptlib/boundedproblem.h>
#include <memory>
#include "RibbonSystem.hpp"


class SRStaticProblem : public cppoptlib::BoundedProblem<real>
{
public:
  using typename cppoptlib::Problem<real>::Scalar;
  using typename cppoptlib::Problem<real>::TVector;
  using typename cppoptlib::Problem<real>::THessian;
  SRStaticProblem(RibbonSystem sys);
  
  virtual real value(const TVector &pt) override;
  virtual void gradient(const TVector &pt, TVector &res) override;
  virtual void hessian(const TVector &x, THessian &hessian) override;
  virtual bool callback(const cppoptlib::Criteria<Scalar> &state, const TVector &x) override;
  void setPoint(const TVector &pt);
  TVector getPoint();
  SuperRibbon get();
private:
  std::unique_ptr<RibbonSystem> sys;
};

#endif // SRSTATICPROBLEM_H
