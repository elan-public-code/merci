/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "SegmentSystem.hpp"
#include "MathOperations.hpp"

using namespace MathOperations;

SegmentSystem::SegmentSystem(const Eigen::Vector3r& gravity, const real& surfassicMass, const real& D, const real& ribbonThickness, const real& ribbonWidth, const real& ribbonLength, real poissonRatio):
  G(gravity),
  surfassicMass(surfassicMass),
  ribbonWidth(ribbonWidth),
  ribbonLength(ribbonLength),
  poissonRatio(poissonRatio),
  computationStatus({false,false,false})
{
  elasticEnergy.FixPhysics(D, ribbonThickness, ribbonWidth, ribbonLength, poissonRatio);
}


void SegmentSystem::setPoint(const Eigen::Vector3r& pos, const Eigen::Matrix3r& frame, const LinearScalar& omega, const LinearScalar& eta, const std::optional<LinearScalar>& naturalOmega)
{
  this->pos=pos;
  this->frame=frame;
  this->omega=omega;
  this->eta=eta;
  elasticEnergy.FixPt(omega, eta, naturalOmega);
  computationStatus[0]=computationStatus[1]=computationStatus[2]=false;
  parts.clear();
}

void SegmentSystem::energyPreCalc()
{
  Eigen::Vector3r eg(0., 0., 0.);
  LinearScalar w = omega;
  LinearScalar n = eta;
  Eigen::Vector3r p=pos;
  Eigen::Matrix3r f=frame;
  real sStart=0.;
  do{
    parts.push_back(SegmentPart());
    SegmentPart& part=parts.back();
    part.s0=sStart;
    part.serie.Place(p,f);
    part.serie.Setup(w,n);
    part.serie.SetRibbonWidth(ribbonWidth);
    part.lenght=part.serie.estimSMax();
    if(part.lenght+sStart>ribbonLength)
    {
      part.lenght=ribbonLength-sStart;
      std::tie(endPos, endFrame)=part.serie.PosFrame(part.lenght);
      eg+=part.serie.preAltitudeEnergy(part.lenght);
      break;
    }
    sStart+=part.lenght;
    w=LinearScalar(omega.A,omega.B+sStart*omega.A);
    n=LinearScalar(eta.A,eta.B+sStart*eta.A);
    std::tie(p, f)=part.serie.PosFrame(part.lenght);
    eg+=part.serie.preAltitudeEnergy(part.lenght);
  }
  while(true);
  energyValue = -surfassicMass* eg.dot(G) + elasticEnergy.computeEnergy();
  computationStatus[0]=true;
}

void SegmentSystem::gradientPreCalc()
{
  if(!computationStatus[0])energyPreCalc();
  Eigen::Matrix<Eigen::Vector4r, 3, 3> gradFrameOut;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPosOut;
  
  gradPreAltitudeEnergyOut.setConstant(Eigen::Vector4r::Zero());
  gradFrameOut.setConstant(Eigen::Vector4r::Zero());
  gradPosOut.setConstant(Eigen::Vector4r::Zero());
  
  Eigen::Matrix3r preframeF = Eigen::Matrix3r::Identity();
  Eigen::Vector3r preTransl = Eigen::Vector3r(0.,0.,0.);
  Eigen::Vector3r preAltitudeRotationalEnergy = Eigen::Vector3r(0.,0.,0.);
  for(SegmentPart& part:parts)
  {
    Eigen::Matrix<real, 3, 4> gradPreAltitudeEnergySegment = part.serie.gradPreAltitudeEnergy(part.lenght);
    Eigen::Matrix<Eigen::Vector3r, 3, 1> dPreAltitudeEnergydPosSegment = part.serie.dPreAltitudeEnergydPos(part.lenght);
    Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPreAltitudeEnergydFrameSegment = part.serie.dPreAltitudeEnergydFrame(part.lenght);
    Eigen::Matrix<Eigen::Matrix3r, 3, 3> dFramedFrameSegment = part.serie.dFramedFrame(part.lenght);
    Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPosdFrameSegment = part.serie.dPosdFrame(part.lenght);
    Eigen::Matrix<Eigen::Vector3r, 3, 1> dPosdPosSegment = part.serie.dPosdPos(part.lenght);

    Eigen::Matrix3r dFdaSegment = part.serie.dFrameda(part.lenght);
    Eigen::Matrix3r dFdbSegment = part.serie.dFramedb(part.lenght);
    Eigen::Matrix3r dFdnSegment = part.serie.dFramedn(part.lenght);
    Eigen::Matrix3r dFdmSegment = part.serie.dFramedm(part.lenght);
    Eigen::Vector3r dPdaSegment = part.serie.dPosda(part.lenght);
    Eigen::Vector3r dPdbSegment = part.serie.dPosdb(part.lenght);
    Eigen::Vector3r dPdnSegment = part.serie.dPosdn(part.lenght);
    Eigen::Vector3r dPdmSegment = part.serie.dPosdm(part.lenght);

    dFdaSegment += part.s0 * dFdbSegment;
    dFdnSegment += part.s0 * dFdmSegment;
    dPdaSegment += part.s0 * dPdbSegment;
    dPdnSegment += part.s0 * dPdmSegment;
    gradPreAltitudeEnergySegment.col(0) += part.s0 * gradPreAltitudeEnergySegment.col(1);
    gradPreAltitudeEnergySegment.col(2) += part.s0 * gradPreAltitudeEnergySegment.col(3);

    for (int k = 0; k < 3; k++) gradPreAltitudeEnergyOut[k] += gradPreAltitudeEnergySegment.row(k);
    gradPreAltitudeEnergyOut += compDeriv(dPreAltitudeEnergydFrameSegment, gradFrameOut) + compDeriv(dPreAltitudeEnergydPosSegment, gradPosOut);

    gradPosOut = compDeriv(dPosdPosSegment, gradPosOut) + compDeriv(dPosdFrameSegment, gradFrameOut);
    gradFrameOut = compDeriv(dFramedFrameSegment, gradFrameOut);
    for (int a = 0; a < 3; a++) {
      for (int b = 0; b < 3; b++) {
        gradFrameOut(a, b)[0] += dFdaSegment(a, b);
        gradFrameOut(a, b)[1] += dFdbSegment(a, b);
        gradFrameOut(a, b)[2] += dFdnSegment(a, b);
        gradFrameOut(a, b)[3] += dFdmSegment(a, b);
      }
      gradPosOut(a)[0] += dPdaSegment(a);
      gradPosOut(a)[1] += dPdbSegment(a);
      gradPosOut(a)[2] += dPdnSegment(a);
      gradPosOut(a)[3] += dPdmSegment(a);
    }
    preAltitudeRotationalEnergy+=part.lenght*ribbonWidth*preTransl+ preframeF*part.serie.preAltitudeRotationalEnergy(part.lenght);
    preTransl+=preframeF*part.serie.PreTranslation(part.lenght);
    preframeF *= part.serie.PreFrame(part.lenght);
    part.gradFrameOut=gradFrameOut;
    part.gradPosOut=gradPosOut;
  }
  
  real surface = ribbonLength * ribbonWidth;
  
  Eigen::Matrix<Eigen::Vector3r, 3, 1> dPreAltitudeEnergydPosOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPreAltitudeEnergydFrameOut;
  dPreAltitudeEnergydPosOut = Eigen::Matrix<Eigen::Vector3r, 3, 1>(Eigen::Vector3r(surface, 0., 0.), Eigen::Vector3r(0., surface, 0.), Eigen::Vector3r(0., 0., surface));
  dPreAltitudeEnergydFrameOut = getdPreAltdFrame(preAltitudeRotationalEnergy);
  dFramedFrame = getdFramedFrame(preframeF);
  dPosdFrame = getdPosdFrame(preTransl);
  dEnergydFrameOut = -surfassicMass* contractGeneralDotP(dPreAltitudeEnergydFrameOut,G);
  dEnergydPosOut = -surfassicMass* contractGeneralDotP(dPreAltitudeEnergydPosOut,G);
  gradientValue = -surfassicMass*contractGeneralDotP(gradPreAltitudeEnergyOut,G)+elasticEnergy.computeGrad();
  computationStatus[1]=true;
}

Eigen::Matrix<Eigen::Matrix3r, 3, 1> SegmentSystem::getdPreAltdFrame(const Eigen::Vector3r& preAltitudeRotationalEnergy) const
{
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> out;

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++) {
      Eigen::Matrix3r tM;
      Eigen::Vector3r tMv;
      tM.setZero();
      tM(i, j) = 1.;
      tMv = tM * preAltitudeRotationalEnergy;
      for (int k = 0; k < 3; k++)
        out[k](i, j) = tMv(k);
    }
  return out;
}

Eigen::Matrix<Eigen::Matrix3r, 3, 1> SegmentSystem::getdPosdFrame(const Eigen::Vector3r& preTrans) const
{
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> out;

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++) {
      Eigen::Matrix3r tM;
      Eigen::Vector3r tMv;
      tM.setZero();
      tM(i, j) = 1.;
      tMv = tM * preTrans;
      for (int k = 0; k < 3; k++)
        out[k](i, j) = tMv(k);
    }
  return out;
}

Eigen::Matrix<Eigen::Matrix3r, 3, 3> SegmentSystem::getdFramedFrame(const Eigen::Matrix3r& preFrame) const
{
  Eigen::Matrix<Eigen::Matrix3r, 3, 3> out;

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++) {
      Eigen::Matrix3r tM;
      tM.setZero();
      tM(i, j) = 1.;
      tM = tM * preFrame;
      for (int k = 0; k < 3; k++)
        for (int l = 0; l < 3; l++)
          out(k, l)(i, j) = tM(k, l);
    }
  return out;
}


void SegmentSystem::hessianPreCalc()
{
  if(!computationStatus[1])gradientPreCalc();
  Eigen::Matrix<Eigen::Vector4r, 3, 3> gradFrameOutPrev;
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPosOutPrev;
  
  gradFrameOutPrev.setConstant(Eigen::Vector4r::Zero());
  gradPosOutPrev.setConstant(Eigen::Vector4r::Zero());
  
  hessianPreAltitudeEnergyOut.setConstant(Eigen::Matrix4r::Zero());
  hessianPosOut.setConstant(Eigen::Matrix4r::Zero());
  hessianFrameOut.setConstant(Eigen::Matrix4r::Zero());
  
  for(auto& part:parts)
  {
    Eigen::Matrix<Eigen::Matrix4r, 3, 1> hessianPreAltitudeEnergy;
    Eigen::Matrix<Eigen::Matrix4r, 3, 1> hessianPos;
    Eigen::Matrix<Eigen::Matrix4r, 3, 3> hessianFrame;

    Eigen::Matrix<Eigen::Matrix<Eigen::Vector4r, 3, 3>, 3, 1> d2PosdFramedabnm = part.serie.d2PosdFramedabnm(part.lenght);
    Eigen::Matrix<Eigen::Matrix<Eigen::Vector4r, 3, 3>, 3, 3> d2FramedFramedabnm = part.serie.d2FramedFramedabnm(part.lenght);
    Eigen::Matrix<Eigen::Matrix<Eigen::Vector4r, 3, 3>, 3, 1> d2PreAltitudeEnergydFramedabnm = part.serie.d2PreAltitudeEnergydFramedabnm(part.lenght);
    Eigen::Matrix<Eigen::Matrix4r, 3, 1> hessianPosSegment = part.serie.hessianPos(part.lenght);
    Eigen::Matrix<Eigen::Matrix4r, 3, 3> hessianFrameSegment = part.serie.hessianFrame(part.lenght);
    Eigen::Matrix<Eigen::Matrix4r, 3, 1> hessianPreAltitudeEnergySegment = part.serie.hessianPreAltitudeEnergy(part.lenght);
    Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPosdFrameSegment = part.serie.dPosdFrame(part.lenght);
    Eigen::Matrix<Eigen::Vector3r, 3, 1> dPosdPosSegment = part.serie.dPosdPos(part.lenght);
    Eigen::Matrix<Eigen::Vector3r, 3, 1> dPreAltitudeEnergydPosSegment = part.serie.dPreAltitudeEnergydPos(part.lenght);
    Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPreAltitudeEnergydFrameSegment = part.serie.dPreAltitudeEnergydFrame(part.lenght);
    Eigen::Matrix<Eigen::Matrix3r, 3, 3> dFramedFrameSegment = part.serie.dFramedFrame(part.lenght);

    for (int i = 0; i < 3; i++)hessianPos(i) = addTransposeToItself(compDeriv2(d2PosdFramedabnm(i), gradFrameOutPrev));
    for (int i = 0; i < 3; i++)hessianPos(i) += contractGeneralDotP(hessianFrameOut, dPosdFrameSegment(i));
    for (int i = 0; i < 3; i++)hessianPos(i) += contractGeneralDotP(hessianPosOut, dPosdPosSegment(i));
    for (int i = 0; i < 3; i++)hessianPos(i) += hessianPosSegment(i);
    //aa
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 0) += 2. * part.s0 * hessianPosSegment(i)(0, 1);
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 0) += 2. * part.s0 * contractGeneralDotP(extractDerivation(d2PosdFramedabnm(i), 1), extractDerivation(gradFrameOutPrev, 0));
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 0) += part.s0 * part.s0 * hessianPosSegment(i)(1, 1);
    //ab
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 1) += part.s0 * hessianPosSegment(i)(1, 1);
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 1) += part.s0 * contractGeneralDotP(extractDerivation(d2PosdFramedabnm(i), 1), extractDerivation(gradFrameOutPrev, 1));
    //an
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 2) += part.s0 * (hessianPosSegment(i)(1, 2) + hessianPosSegment(i)(0, 3));
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 2) += part.s0 * (contractGeneralDotP(extractDerivation(d2PosdFramedabnm(i), 1), extractDerivation(gradFrameOutPrev, 2))
          + contractGeneralDotP(extractDerivation(d2PosdFramedabnm(i), 3), extractDerivation(gradFrameOutPrev, 0)));
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 2) += part.s0 * part.s0 * hessianPosSegment(i)(1, 3);
    //na
    for (int i = 0; i < 3; i++)hessianPos(i)(2, 0) = hessianPos(i)(0, 2);
    //am
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 3) += part.s0 * hessianPosSegment(i)(1, 3);
    for (int i = 0; i < 3; i++)hessianPos(i)(0, 3) += part.s0 * contractGeneralDotP(extractDerivation(d2PosdFramedabnm(i), 1), extractDerivation(gradFrameOutPrev, 3));
    //ma
    for (int i = 0; i < 3; i++)hessianPos(i)(3, 0) = hessianPos(i)(0, 3);
    //nb
    for (int i = 0; i < 3; i++)hessianPos(i)(2, 1) += part.s0 * hessianPosSegment(i)(3, 1);
    for (int i = 0; i < 3; i++)hessianPos(i)(2, 1) += part.s0 * contractGeneralDotP(extractDerivation(d2PosdFramedabnm(i), 3), extractDerivation(gradFrameOutPrev, 1));
    //bn
    for (int i = 0; i < 3; i++)hessianPos(i)(1, 2) = hessianPos(i)(2, 1);
    //nn
    for (int i = 0; i < 3; i++)hessianPos(i)(2, 2) += 2. * part.s0 * hessianPosSegment(i)(2, 3);
    for (int i = 0; i < 3; i++)hessianPos(i)(2, 2) += 2. * part.s0 * contractGeneralDotP(extractDerivation(d2PosdFramedabnm(i), 3), extractDerivation(gradFrameOutPrev, 2));
    for (int i = 0; i < 3; i++)hessianPos(i)(2, 2) += part.s0 * part.s0 * hessianPosSegment(i)(3, 3);
    //nm
    for (int i = 0; i < 3; i++)hessianPos(i)(2, 3) += part.s0 * hessianPosSegment(i)(3, 3);
    for (int i = 0; i < 3; i++)hessianPos(i)(2, 3) += part.s0 * contractGeneralDotP(extractDerivation(d2PosdFramedabnm(i), 3), extractDerivation(gradFrameOutPrev, 3));
    //ba
    for (int i = 0; i < 3; i++)hessianPos(i)(1, 0) = hessianPos(i)(0, 1);
    //mn
    for (int i = 0; i < 3; i++)hessianPos(i)(3, 2) = hessianPos(i)(2, 3);




    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j) = addTransposeToItself(compDeriv2(d2FramedFramedabnm(i, j), gradFrameOutPrev));
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j) += contractGeneralDotP(hessianFrameOut, dFramedFrameSegment(i, j));
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j) += hessianFrameSegment(i, j);
    //aa
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 0) += 2 * part.s0 * hessianFrameSegment(i, j)(0, 1);
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 0) += 2 * part.s0 * contractGeneralDotP(extractDerivation(d2FramedFramedabnm(i, j), 1), extractDerivation(gradFrameOutPrev, 0));
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 0) += part.s0 * part.s0 * hessianFrameSegment(i, j)(1, 1);
    //ab
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 1) += part.s0 * hessianFrameSegment(i, j)(1, 1);
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 1) += part.s0 * contractGeneralDotP(extractDerivation(d2FramedFramedabnm(i, j), 1), extractDerivation(gradFrameOutPrev, 1));
    //an
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 2) += part.s0 * (hessianFrameSegment(i, j)(1, 2) + hessianFrameSegment(i, j)(0, 3));
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 2) += part.s0 * (contractGeneralDotP(extractDerivation(d2FramedFramedabnm(i, j), 1), extractDerivation(gradFrameOutPrev, 2))
            + contractGeneralDotP(extractDerivation(d2FramedFramedabnm(i, j), 3), extractDerivation(gradFrameOutPrev, 0))
                                                                                               );
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 2) += part.s0 * part.s0 * hessianFrameSegment(i, j)(1, 3);
    //na
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(2, 0) = hessianFrame(i, j)(0, 2);
    //am
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 3) += part.s0 * hessianFrameSegment(i, j)(1, 3);
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(0, 3) += part.s0 * contractGeneralDotP(extractDerivation(d2FramedFramedabnm(i, j), 1), extractDerivation(gradFrameOutPrev, 3));
    //ma
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(3, 0) = hessianFrame(i, j)(0, 3);
    //nb
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(2, 1) += part.s0 * hessianFrameSegment(i, j)(3, 1);
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(2, 1) += part.s0 * contractGeneralDotP(extractDerivation(d2FramedFramedabnm(i, j), 3), extractDerivation(gradFrameOutPrev, 1));
    //bn
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(1, 2) = hessianFrame(i, j)(2, 1);
    //nn
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(2, 2) += 2 * part.s0 * hessianFrameSegment(i, j)(2, 3);
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(2, 2) += 2 * part.s0 * contractGeneralDotP(extractDerivation(d2FramedFramedabnm(i, j), 3), extractDerivation(gradFrameOutPrev, 2));
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(2, 2) += part.s0 * part.s0 * hessianFrameSegment(i, j)(3, 3);
    //nm
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(2, 3) += part.s0 * hessianFrameSegment(i, j)(3, 3);
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(2, 3) += part.s0 * contractGeneralDotP(extractDerivation(d2FramedFramedabnm(i, j), 3), extractDerivation(gradFrameOutPrev, 3));
    //ba
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(1, 0) = hessianFrame(i, j)(0, 1);
    //mn
    for (int i = 0; i < 3; i++)for (int j = 0; j < 3; j++)hessianFrame(i, j)(3, 2) = hessianFrame(i, j)(2, 3);


    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i) = addTransposeToItself(compDeriv2(d2PreAltitudeEnergydFramedabnm(i), gradFrameOutPrev));
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i) += contractGeneralDotP(hessianFrameOut, dPreAltitudeEnergydFrameSegment(i));
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i) += contractGeneralDotP(hessianPosOut, dPreAltitudeEnergydPosSegment(i));
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i) += hessianPreAltitudeEnergySegment(i);
    //aa
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 0) += 2. * part.s0 * hessianPreAltitudeEnergySegment(i)(0, 1);
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 0) += 2. * part.s0 * contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedabnm(i), 1), extractDerivation(gradFrameOutPrev, 0));
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 0) += part.s0 * part.s0 * hessianPreAltitudeEnergySegment(i)(1, 1);
    //ab
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 1) += part.s0 * hessianPreAltitudeEnergySegment(i)(1, 1);
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 1) += part.s0 * contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedabnm(i), 1), extractDerivation(gradFrameOutPrev, 1));
    //an
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 2) += part.s0 * (hessianPreAltitudeEnergySegment(i)(1, 2) + hessianPreAltitudeEnergySegment(i)(0, 3));
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 2) += part.s0 * (contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedabnm(i), 1), extractDerivation(gradFrameOutPrev, 2))
          + contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedabnm(i), 3), extractDerivation(gradFrameOutPrev, 0)));
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 2) += part.s0 * part.s0 * hessianPreAltitudeEnergySegment(i)(1, 3);
    //na
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(2, 0) = hessianPreAltitudeEnergy(i)(0, 2);
    //am
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 3) += part.s0 * hessianPreAltitudeEnergySegment(i)(1, 3);
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(0, 3) += part.s0 * contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedabnm(i), 1), extractDerivation(gradFrameOutPrev, 3));
    //ma
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(3, 0) = hessianPreAltitudeEnergy(i)(0, 3);
    //nb
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(2, 1) += part.s0 * hessianPreAltitudeEnergySegment(i)(3, 1);
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(2, 1) += part.s0 * contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedabnm(i), 3), extractDerivation(gradFrameOutPrev, 1));
    //bn
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(1, 2) = hessianPreAltitudeEnergy(i)(2, 1);
    //nn
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(2, 2) += 2. * part.s0 * hessianPreAltitudeEnergySegment(i)(2, 3);
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(2, 2) += 2. * part.s0 * contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedabnm(i), 3), extractDerivation(gradFrameOutPrev, 2));
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(2, 2) += part.s0 * part.s0 * hessianPreAltitudeEnergySegment(i)(3, 3);
    //nm
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(2, 3) += part.s0 * hessianPreAltitudeEnergySegment(i)(3, 3);
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(2, 3) += part.s0 * contractGeneralDotP(extractDerivation(d2PreAltitudeEnergydFramedabnm(i), 3), extractDerivation(gradFrameOutPrev, 3));
    //ba
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(1, 0) = hessianPreAltitudeEnergy(i)(0, 1);
    //mn
    for (int i = 0; i < 3; i++)hessianPreAltitudeEnergy(i)(3, 2) = hessianPreAltitudeEnergy(i)(2, 3);


    hessianPreAltitudeEnergyOut += hessianPreAltitudeEnergy;
    hessianFrameOut = hessianFrame;
    hessianPosOut = hessianPos;
    
    
    gradPosOutPrev=part.gradPosOut;
    gradFrameOutPrev=part.gradFrameOut;
  }
  computationStatus[2]=true;
}

std::pair<Eigen::Vector3r, Eigen::Matrix3r> SegmentSystem::endPosFrame()
{
  if(!computationStatus[0])energyPreCalc();
  return std::pair<Eigen::Vector3r, Eigen::Matrix3r>(endPos,endFrame);
}

real SegmentSystem::energy()
{
  if(!computationStatus[0])energyPreCalc();
  return energyValue;
}

Eigen::Vector4r SegmentSystem::gradient()
{
  if(!computationStatus[1])gradientPreCalc();
  return gradientValue;
}

Eigen::Matrix<Eigen::Vector4r, 3, 1> SegmentSystem::gradPos()
{
  if(!computationStatus[1])gradientPreCalc();
  return parts.back().gradPosOut;
}

Eigen::Matrix<Eigen::Vector4r, 3, 3> SegmentSystem::gradFrame()
{
  if(!computationStatus[1])gradientPreCalc();
  return parts.back().gradFrameOut;
}

Eigen::Matrix<Eigen::Matrix3r, 3, 1> SegmentSystem::getdPosdFrame()
{
  if(!computationStatus[1])gradientPreCalc();
  return dPosdFrame;
}

Eigen::Matrix<Eigen::Matrix3r, 3, 3> SegmentSystem::getdFramedFrame()
{
  if(!computationStatus[1])gradientPreCalc();
  return dFramedFrame;
}

Eigen::Matrix3r SegmentSystem::getdEnergydFrameOut()
{
  if(!computationStatus[1])gradientPreCalc();
  return dEnergydFrameOut;
}

Eigen::Vector3r SegmentSystem::getdEnergydPosOut()
{
  if(!computationStatus[1])gradientPreCalc();
  return dEnergydPosOut;
}

Eigen::Matrix4r SegmentSystem::hessian()
{
  if(!computationStatus[2])hessianPreCalc();
  return -surfassicMass*contractGeneralDotP(hessianPreAltitudeEnergyOut,G)+elasticEnergy.computeHessian();
}

Eigen::Matrix<Eigen::Matrix4r, 3, 1> SegmentSystem::gethessianPosOut()
{
  if(!computationStatus[2])hessianPreCalc();
  return hessianPosOut;
}

Eigen::Matrix<Eigen::Matrix4r, 3, 3> SegmentSystem::gethessianFrameOut()
{
  if(!computationStatus[2])hessianPreCalc();
  return hessianFrameOut;
}

std::pair<Eigen::Vector3r, Eigen::Matrix3r> SegmentSystem::orPosFrame()
{
  return std::pair<Eigen::Vector3r, Eigen::Matrix3r>(pos,frame);
}

real SegmentSystem::getd2EdFramedktimesdFramedz(Eigen::Matrix3r dFramedz, int k)
{
  return -surfassicMass*(dFramedz *frame.inverse()*extractDerivation( gradPreAltitudeEnergyOut,k)).dot(G);
}
