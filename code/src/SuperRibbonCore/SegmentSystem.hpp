/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef SEGMENTSYSTEM_H
#define SEGMENTSYSTEM_H

#include <Eigen/Dense>
#include <array>
#include <BaseCore/LinearScalar.hpp>
#include "SuperRibbonGeometrieSerie.hpp"
#include "ElasticEnergy.hpp"

class SegmentSystem
{
public:
  SegmentSystem(const Eigen::Vector3r& gravity, const real& surfassicMass, const real& D, const real& ribbonThickness, const real& ribbonWidth, const real& ribbonLength, real poissonRatio);
  void setPoint(const Eigen::Vector3r& pos, const Eigen::Matrix3r& frame, const LinearScalar& omega, const LinearScalar& eta, const std::optional<LinearScalar>& naturalOmega);
  std::pair<Eigen::Vector3r,Eigen::Matrix3r> endPosFrame();
  std::pair<Eigen::Vector3r,Eigen::Matrix3r> orPosFrame();
  real energy();
  Eigen::Vector4r gradient();
  Eigen::Matrix4r hessian();
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPos();
  Eigen::Matrix<Eigen::Vector4r, 3, 3> gradFrame();
  Eigen::Matrix<Eigen::Matrix3r, 3, 3> getdFramedFrame();
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> getdPosdFrame();
  Eigen::Vector3r getdEnergydPosOut();
  Eigen::Matrix3r getdEnergydFrameOut();
  Eigen::Matrix<Eigen::Matrix4r, 3, 1> gethessianPosOut();
  Eigen::Matrix<Eigen::Matrix4r, 3, 3> gethessianFrameOut();
  real getd2EdFramedktimesdFramedz(Eigen::Matrix3r dFramedz,int k);
private:
  void energyPreCalc();
  void gradientPreCalc();
  void hessianPreCalc();
  Eigen::Vector3r pos;
  Eigen::Matrix3r frame; 
  LinearScalar omega;
  LinearScalar eta;
  struct SegmentPart{
    SuperRibbonGeometrieSerie serie;
    real lenght;
    real s0;
    //first order
    Eigen::Matrix<Eigen::Vector4r, 3, 3> gradFrameOut;
    Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPosOut;
  };
  std::vector<SegmentPart> parts;
  ElasticEnergy elasticEnergy;
  Eigen::Vector3r G;
  real surfassicMass; 
  real ribbonWidth;
  real ribbonLength;
  std::optional<real> poissonRatio;
  
  std::array<bool,3> computationStatus;
  
  Eigen::Vector3r endPos;
  Eigen::Matrix3r endFrame;
  real energyValue;
  
  Eigen::Matrix<Eigen::Vector4r, 3, 1> gradPreAltitudeEnergyOut;
  Eigen::Vector3r dEnergydPosOut;
  Eigen::Matrix3r dEnergydFrameOut;
  Eigen::Matrix<Eigen::Matrix3r, 3, 3> dFramedFrame;
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPosdFrame;
  Eigen::Vector4r gradientValue;
  
  Eigen::Matrix<Eigen::Matrix4r, 3, 1> hessianPreAltitudeEnergyOut;
  Eigen::Matrix<Eigen::Matrix4r, 3, 1> hessianPosOut;
  Eigen::Matrix<Eigen::Matrix4r, 3, 3> hessianFrameOut;
  
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> getdPreAltdFrame(const Eigen::Vector3r& preAltitudeRotationalEnergy) const;
  Eigen::Matrix<Eigen::Matrix3r, 3, 3> getdFramedFrame(const Eigen::Matrix3r& preFrame) const;
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> getdPosdFrame(const Eigen::Vector3r& preTrans) const;
};

#endif // SEGMENTSYSTEM_H
