/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "SuperRibbon.hpp"

SuperRibbon::SuperRibbon()
{
  posOr.setZero();
  frameOr.setIdentity();
}

void SuperRibbon::setPosOr(Eigen::Vector3r pos)
{
  posOr = pos;
}

void SuperRibbon::setFrameOr(Eigen::Matrix3r frame)
{
  frameOr = frame;
}

Eigen::Vector3r SuperRibbon::getPosOr() const
{
  return posOr;
}

Eigen::Matrix3r SuperRibbon::getFrameOr() const
{
  return frameOr;
}

size_t SuperRibbon::getNbSegments() const
{
  return segments.size();
}

void SuperRibbon::updateSegments(size_t from)
{
  size_t t = segments.size();
  from = std::max(size_t(1), from);
  for (size_t i = from; i < t; i++)
  {
    segments[i].eta.B = segments[i - 1].eta.B + segments[i - 1].length * segments[i - 1].eta.A;
    segments[i].omega1.B = segments[i - 1].omega1.B + segments[i - 1].length * segments[i - 1].omega1.A;
  }
}

void SuperRibbon::setEtaMOr(real etaM)
{
  segments[0].eta.B = etaM;
  updateSegments(1);
}

void SuperRibbon::setOmega1BOr(real omega1B)
{
  segments[0].omega1.B = omega1B;
  updateSegments(1);
}

void SuperRibbon::setEtaN(int segment, real etaN)
{
  segments.at(segment).eta.A = etaN;
  updateSegments(segment + 1);
}

void SuperRibbon::setLength(int segment, real l)
{
  segments.at(segment).length = l;
  updateSegments(segment + 1);
}

void SuperRibbon::setOmega1A(int segment, real omega1A)
{
  segments.at(segment).omega1.A = omega1A;
  updateSegments(segment + 1);
}

void SuperRibbon::setNatOmega1(int segment, std::optional<LinearScalar> natomega1)
{
  segments.at(segment).natOmega1 = natomega1;
}

real SuperRibbon::getLength(int segment) const
{
  return segments.at(segment).length;
}

LinearScalar SuperRibbon::getOmega1(int segment) const
{
  return segments.at(segment).omega1;
}

std::optional<LinearScalar> SuperRibbon::getNatOmega1(int segment) const
{
  return segments.at(segment).natOmega1;
}


LinearScalar SuperRibbon::getEta(int segment) const
{
  return segments.at(segment).eta;
}

void SuperRibbon::addSegmentAt(int idx)
{
  segments.insert(segments.begin() + idx, SuperRibbonSegment());
  updateSegments(idx);
}

void SuperRibbon::deleteSegment(int idx)
{
  segments.erase(segments.begin() + idx);
  updateSegments(idx - 1);
}

real SuperRibbon::getD() const
{
  return D;
}

std::optional<real> SuperRibbon::getPoissonRatio() const
{
  return poissonRatio;
}

real SuperRibbon::getAreaDensity() const
{
  return areaDensity;
}

real SuperRibbon::getWidth() const
{
  return width;
}

real SuperRibbon::getThickness() const
{
  return thickness;
}

void SuperRibbon::setD(real d)
{
  D = d;
}

void SuperRibbon::setPoissonRatio(std::optional<real> nu)
{
  poissonRatio = nu;
}

void SuperRibbon::setAreaDensity(real ad)
{
  areaDensity = ad;
}

void SuperRibbon::setWidth(real w)
{
  width = w;
}

void SuperRibbon::setThickness(real h)
{
  thickness=h;
}
