/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef SUPERRIBBON_H
#define SUPERRIBBON_H

#include <BaseCore/real.hpp>
#include <BaseCore/LinearScalar.hpp>
#include <Eigen/Dense>
#include <vector>
#include <optional>

class SuperRibbon
{
public:
  SuperRibbon();
  void deleteSegment(int idx);
  void addSegmentAt(int idx);
  void setOmega1A(int segment, real omega1A);
  
  //[[deprecated("The natural curvature might be discontinuous as specified by the user")]]
  void setNatOmega1(int segment, std::optional<LinearScalar> natomega1);
  void setEtaN(int segment, real etaN);
  void setLength(int segment, real l);
  void setEtaMOr(real etaM);
  void setOmega1BOr(real omega1B);
  void setPosOr(Eigen::Vector3r pos);
  void setFrameOr(Eigen::Matrix3r frame);
  void setWidth(real w);
  void setThickness(real h);
  void setAreaDensity(real ad);
  void setD(real d);
  void setPoissonRatio(std::optional<real> nu);

  LinearScalar getOmega1(int segment) const;
  std::optional<LinearScalar> getNatOmega1(int segment) const;
  LinearScalar getEta(int segment) const;
  real getLength(int segment) const;

  size_t getNbSegments() const;
  Eigen::Vector3r getPosOr() const;
  Eigen::Matrix3r getFrameOr() const;
  real getWidth() const;
  real getThickness() const;
  real getAreaDensity() const;
  real getD() const;
  std::optional<real> getPoissonRatio() const;
private:
  struct SuperRibbonSegment {
    LinearScalar eta;
    LinearScalar omega1;
    std::optional<LinearScalar> natOmega1;
    real length = 0.;
  };
  Eigen::Vector3r posOr;
  Eigen::Matrix3r frameOr;
  real width = 0.01;
  real thickness = 0.00001;
  real D = 1.e-4;
  std::optional<real> poissonRatio = std::nullopt; 
  real areaDensity = 0.1;
  std::vector<SuperRibbonSegment> segments;
  void updateSegments(size_t from);
};

#endif // SUPERRIBBON_H
