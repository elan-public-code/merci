/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "SuperRibbonGeometrieSerie.hpp"
#include "Log/Logger.hpp"
#include <cmath>

Eigen::Matrix3r SuperRibbonGeometrieSerie::skewOf(Eigen::Vector3r w)
{
  Eigen::Matrix3r res;
  res << 0,    -w[2], w[1],
      w[2],  0,    -w[0],
      -w[1], w[0], 0;
  return res;
}

void SuperRibbonGeometrieSerie::Place(Eigen::Vector3r pos, Eigen::Matrix3r frame)
{
  called.clear();
  posOr = pos;
  frameOr = frame;
}

void SuperRibbonGeometrieSerie::Setup(LinearScalar omega1, LinearScalar eta)
{
  if (false) {
    LoggerMsg message("Setup");
    message.addChild("omega1", std::to_string(omega1.A)+", "+std::to_string(omega1.B));
    message.addChild("eta", std::to_string(eta.A)+", "+std::to_string(eta.B));
    getLogger()->Write(LoggerLevel::INFORMATION, "Serie", message);
  }
  called.clear();
  this->omega1 = omega1;
  this->eta = eta;
  lambda0Sq = skewOf(Eigen::Vector3r(omega1.B, 0.0, eta.B * omega1.B));
  lambda1Sq = skewOf(Eigen::Vector3r(omega1.A, 0.0, eta.B * omega1.A + eta.A * omega1.B));
  lambda2Sq = skewOf(Eigen::Vector3r(0.,       0.0, eta.A * omega1.A));
  lambda0 = Eigen::Vector3r(omega1.B, 0.0, eta.B * omega1.B);
  lambda1 = Eigen::Vector3r(omega1.A, 0.0, eta.B * omega1.A + eta.A * omega1.B);
  lambda2 = Eigen::Vector3r(0.,       0.0, eta.A * omega1.A);
  lambda = std::max(
             lambda0.lpNorm<Eigen::Infinity>(),
             std::max(lambda1.lpNorm<Eigen::Infinity>(),
                      lambda2.lpNorm<Eigen::Infinity>()));
}

void SuperRibbonGeometrieSerie::SetRibbonWidth(real w)
{
  called.clear();
  ribbonWidth = w;
}

real SuperRibbonGeometrieSerie::estimRemainderOfTheSerie(int order, real s)
{
  assert(s > 0);
  assert(order > 3);
  return std::exp(C(2 * s)) / pow(2., order - 1);
}

real SuperRibbonGeometrieSerie::factorial(int k)
{
  assert(k >= 1);
  real res = 1.;
  for (int i = 2; i <= k; i++)
    res *= static_cast<real>(i);
  return res;
}

int SuperRibbonGeometrieSerie::estimNeededOrder(real prec)
{
  int a = static_cast<int>(std::log2(prec) - 0.95);
  assert(a < 0);
  return 52 - a;
}

int SuperRibbonGeometrieSerie::valNeededOrder = 52 * 3 / 2;

int SuperRibbonGeometrieSerie::estimNeededOrder()
{
  return valNeededOrder;
  //return (52 * 3 / 2);
}

void SuperRibbonGeometrieSerie::experimentalySetNeededOrder(int val){valNeededOrder=val;}
void SuperRibbonGeometrieSerie::experimentalyResetNeededOrder(){valNeededOrder=52 * 3 / 2;}


real SuperRibbonGeometrieSerie::C(real s)
{
  return 2.*std::abs(s) *
         (
           lambda0.lpNorm<Eigen::Infinity>()
           + (s * lambda1).lpNorm<Eigen::Infinity>() +
           (s * s * lambda2).lpNorm<Eigen::Infinity>()
         );
}

bool SuperRibbonGeometrieSerie::enableSubdivision=true;

void SuperRibbonGeometrieSerie::experimentalyToggleSubdivision(bool val)
{
  enableSubdivision=val;
}

double solveP1(double a, double b)
{
  if(a==0.) 
    return std::numeric_limits<real>::infinity();
  return -b/a;
}

double solveP2(double a, double b, double c)
{
  if(a==0.)
    return solveP1(b, c);
  const double delta = b*b-4.*a*c;
  assert(delta>=0.);
  const double sdelta=sqrt(delta);
  assert(b<=sdelta);
  return (b+sdelta)/(2.*a);
}

double solveP3(double a, double b, double c, double d)
{
  b /= a;
  c /= a;
  d /= a;
  
  double disc, q, r, dum1, s, t, term1, r13;
  q = (3.0*c - (b*b))/9.0;
  r = -(27.0*d) + b*(9.0*c - 2.0*(b*b));
  r /= 54.0;
  disc = q*q*q + r*r;
  term1 = (b/3.0);
  
  double x1_real, x2_real, x3_real;
  if (disc > 0)   // One root real, two are complex
  {
    s = r + sqrt(disc);
    s = s<0 ? -cbrt(-s) : cbrt(s);
    t = r - sqrt(disc);
    t = t<0 ? -cbrt(-t) : cbrt(t);
    x1_real = -term1 + s + t;
    
    assert(x1_real>0.);
    return x1_real;
  } 
  // The remaining options are all real
  else if (disc == 0)  // All roots real, at least two are equal.
  { 
    r13 = r<0 ? -cbrt(-r) : cbrt(r);
    x1_real = -term1 + 2.0*r13;
    x2_real = -(r13 + term1);
    if(x1_real>0.)
      return x1_real;
    return x2_real;
  }
  // Only option left is that all roots are real and unequal (to get here, q < 0)
  else
  {
    q = -q;
    dum1 = q*q*q;
    dum1 = acos(r/sqrt(dum1));
    r13 = 2.0*sqrt(q);
    x1_real = -term1 + r13*cos(dum1/3.0);
    x2_real = -term1 + r13*cos((dum1 + 2.0*M_PI)/3.0);
    x3_real = -term1 + r13*cos((dum1 + 4.0*M_PI)/3.0);
    if(x1_real>0.)
      return x1_real;
    assert(x2_real*x3_real>0);
    if(x2_real>0.)
      return x2_real;
    assert(x3_real>0);
    return x3_real;
  }
}


real SuperRibbonGeometrieSerie::estimSMax()
{
  real T_2 = (52. / 2.) * std::log(2) / 2.;
  real l0 = lambda0.lpNorm<Eigen::Infinity>();
  real l1 = lambda1.lpNorm<Eigen::Infinity>();
  real l2 = lambda2.lpNorm<Eigen::Infinity>();
  l0 = std::max(static_cast<real>(1.), l0);
  l1 = std::max(static_cast<real>(1.), l1);
  l2 = std::max(static_cast<real>(1.), l2);

  if (std::isnan(l0) || std::isnan(l1) || std::isnan(l2)) {
    return std::numeric_limits<real>::infinity();
  }

  real res = std::numeric_limits<real>::infinity();
  //TODO search a better solution
  if (l0 + l1 + l2 < T_2) { //Smax>1
    res = std::cbrt(T_2 / (l0 + l1 + l2));
  } else { //Smax<1
    res = T_2 / (l0 + l1 + l2);
  }
  
  res = solveP3(l2,l1,l0,-T_2);

  if (res < 1.e-8) {
    LoggerMsg message("Very small smax");
    message.addChild("smax", std::to_string(res));
    message.addChild("l0", std::to_string(l0));
    message.addChild("l1", std::to_string(l1));
    message.addChild("l2", std::to_string(l2));
    getLogger()->Write(LoggerLevel::WARNING, "Serie", message);
    res = std::numeric_limits<real>::infinity();
  }

  return res / 2.;
}

void SuperRibbonGeometrieSerie::computeSeriePreTerms()
{
  computeSeriePreTerms(estimNeededOrder());
}

void SuperRibbonGeometrieSerie::computeSeriePreTerms(int order)
{
  if (called["computeSeriePreTerms"])return;
  assert(order > 3);
  FramePreTerms.clear();
  FramePreTerms.resize(order+1);
  FramePreTerms[0]=Eigen::Matrix3r::Identity();
  FramePreTerms[1]=lambda0Sq;
  FramePreTerms[2]=0.5 * (FramePreTerms[1]*lambda0Sq + lambda1Sq);
  for (int n = 3; n <= order; n++) {
    FramePreTerms[n]=(1. / n) *
                            (
                              FramePreTerms[n - 1]*lambda0Sq
                              + FramePreTerms[n - 2]*lambda1Sq
                              + FramePreTerms[n - 3]*lambda2Sq
                            );
  }
  PosPreTerms.resize(order+1);
  PosPreTerms[0]=Eigen::Vector3r(NAN, NAN, NAN);
  for (int n = 1; n <= order; n++) {
    PosPreTerms[n]=(1. / n) * (
                            FramePreTerms[n - 1].col(2));
  }
  called["computeSeriePreTerms"] = true;
}

Eigen::Matrix3r SuperRibbonGeometrieSerie::Frame_s(real s)
{
  assert(s <= estimSMax());
  return Frame(s);
}

Eigen::Vector3r SuperRibbonGeometrieSerie::Pos_s(real s)
{
  assert(s <= estimSMax());
  return Pos(s);
}

Eigen::Matrix3r SuperRibbonGeometrieSerie::Frame(real s)
{
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  int order = FramePreTerms.size();
  computeSPowers(s);
  Eigen::Matrix3r res = FramePreTerms[0];
  for (int i = 1; i < order; i++)
    res += spowers[i] * FramePreTerms[i];
  return frameOr * res;
}

Eigen::Matrix3r SuperRibbonGeometrieSerie::PreFrame(real s)
{
    if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  int order = FramePreTerms.size();
  computeSPowers(s);
  Eigen::Matrix3r res = FramePreTerms[0];
  for (int i = 1; i < order; i++)
    res += spowers[i] * FramePreTerms[i];
  return res;
}


Eigen::Vector3r SuperRibbonGeometrieSerie::Pos(real s)
{
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  int order = PosPreTerms.size();
  computeSPowers(s);
  Eigen::Vector3r res(0., 0., 0.);
  //for (int i = 1; i < order; i++)
  for (int i = order-1; i > 0; i--)
    res += spowers[i] * PosPreTerms[i];
  res = frameOr * res;
  res += posOr;
  return res;
}

Eigen::Vector3r SuperRibbonGeometrieSerie::PreTranslation(real s)
{
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  int order = PosPreTerms.size();
  computeSPowers(s);
  Eigen::Vector3r res(0., 0., 0.);
  //for (int i = 1; i < order; i++)
  for (int i = order-1; i > 0; i--)
    res += spowers[i] * PosPreTerms[i];
  return res;
}

std::pair<Eigen::Vector3r, Eigen::Matrix3r> SuperRibbonGeometrieSerie::PosFrame(real s)
{
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  int order = FramePreTerms.size();
  computeSPowers(s);
  Eigen::Vector3r resPos(0., 0., 0.);
  //for (int i = 1; i < order; i++)
  for (int i = order-1; i > 0; i--)
    resPos += spowers[i] * PosPreTerms[i];
  resPos = frameOr * resPos;
  resPos += posOr;
  Eigen::Matrix3r resFrame = FramePreTerms[0];
  for (int i = 1; i < order; i++) {
    resFrame += spowers[i] * FramePreTerms[i];
  }
  return std::make_pair(resPos, frameOr * resFrame);
}

void SuperRibbonGeometrieSerie::computeDerivationsPreTerms()
{
  if (called["computeDerivationsPreTerms"])return;
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  dLambda0db << 1., 0., eta.B;
  dLambda0dm << 0., 0., omega1.B;
  dLambda1da << 1., 0., eta.B;
  dLambda1db << 0., 0., eta.A;
  dLambda1dn << 0., 0., omega1.B;
  dLambda1dm << 0., 0., omega1.A;
  dLambda2da << 0., 0., eta.A;
  dLambda2dn << 0., 0., omega1.A;

  dLambda0dbSq = skewOf(dLambda0db);
  dLambda0dmSq = skewOf(dLambda0dm);
  dLambda1daSq = skewOf(dLambda1da);
  dLambda1dbSq = skewOf(dLambda1db);
  dLambda1dnSq = skewOf(dLambda1dn);
  dLambda1dmSq = skewOf(dLambda1dm);
  dLambda2daSq = skewOf(dLambda2da);
  dLambda2dnSq = skewOf(dLambda2dn);

  int order = FramePreTerms.size();
  assert(order > 0);

  //Derivation of the frame
  {
    dFramePreTermsda.resize(order);
    dFramePreTermsdb.resize(order);
    dFramePreTermsdn.resize(order);
    dFramePreTermsdm.resize(order);
    
    Eigen::Matrix3r zero;
    zero.setZero();
    //order 0
    dFramePreTermsda[0]=zero;
    dFramePreTermsdb[0]=zero;
    dFramePreTermsdn[0]=zero;
    dFramePreTermsdm[0]=zero;
    //order 1
    dFramePreTermsda[1]=zero;
    dFramePreTermsdb[1]=dLambda0dbSq;
    dFramePreTermsdn[1]=zero;
    dFramePreTermsdm[1]=dLambda0dmSq;
    //order 2
    dFramePreTermsda[2]=0.5 * dLambda1daSq;
    dFramePreTermsdb[2]=0.5 * (dLambda0dbSq * lambda0Sq + lambda0Sq * dLambda0dbSq + dLambda1dbSq);
    dFramePreTermsdn[2]=0.5 * dLambda1dnSq;
    dFramePreTermsdm[2]=0.5 * (dLambda0dmSq * lambda0Sq + lambda0Sq * dLambda0dmSq + dLambda1dmSq);
    //order n
    for (int n = 3; n < order; n++) {
      dFramePreTermsda[n]=((1. / static_cast<real>(n)) * (
                                   FramePreTerms[n - 2]*dLambda1daSq + FramePreTerms[n - 3]*dLambda2daSq
                                   + dFramePreTermsda[n - 1]*lambda0Sq + dFramePreTermsda[n - 2]*lambda1Sq + dFramePreTermsda[n - 3]*lambda2Sq));
      dFramePreTermsdb[n]=((1. / static_cast<real>(n)) * (
                                   FramePreTerms[n - 1]*dLambda0dbSq + FramePreTerms[n - 2]*dLambda1dbSq
                                   + dFramePreTermsdb[n - 1]*lambda0Sq + dFramePreTermsdb[n - 2]*lambda1Sq + dFramePreTermsdb[n - 3]*lambda2Sq));
      dFramePreTermsdn[n]=((1. / static_cast<real>(n)) * (
                                   FramePreTerms[n - 2]*dLambda1dnSq + FramePreTerms[n - 3]*dLambda2dnSq
                                   + dFramePreTermsdn[n - 1]*lambda0Sq + dFramePreTermsdn[n - 2]*lambda1Sq + dFramePreTermsdn[n - 3]*lambda2Sq));
      dFramePreTermsdm[n]=((1. / static_cast<real>(n)) * (
                                   FramePreTerms[n - 1]*dLambda0dmSq + FramePreTerms[n - 2]*dLambda1dmSq
                                   + dFramePreTermsdm[n - 1]*lambda0Sq + dFramePreTermsdm[n - 2]*lambda1Sq + dFramePreTermsdm[n - 3]*lambda2Sq));
    }
  }

  //Derivation of the position
  {
    dPosPreTermsda.resize(order);
    dPosPreTermsdb.resize(order);
    dPosPreTermsdn.resize(order);
    dPosPreTermsdm.resize(order);

    Eigen::Vector3r zero;
    zero.setZero();
    Eigen::Vector3r vnan(NAN, NAN, NAN);

    //order 0
    dPosPreTermsda[0]=vnan;
    dPosPreTermsdb[0]=vnan;
    dPosPreTermsdn[0]=vnan;
    dPosPreTermsdm[0]=vnan;
    //order 1
    dPosPreTermsda[1]=zero;
    dPosPreTermsdb[1]=zero;
    dPosPreTermsdn[1]=zero;
    dPosPreTermsdm[1]=zero;
    //order n
    for (int n = 2; n < order; n++) {
      dPosPreTermsda[n]=(1. / static_cast<real>(n))*dFramePreTermsda[n - 1].col(2);
      dPosPreTermsdb[n]=(1. / static_cast<real>(n))*dFramePreTermsdb[n - 1].col(2);
      dPosPreTermsdn[n]=(1. / static_cast<real>(n))*dFramePreTermsdn[n - 1].col(2);
      dPosPreTermsdm[n]=(1. / static_cast<real>(n))*dFramePreTermsdm[n - 1].col(2);

    }
  }
  called["computeDerivationsPreTerms"] = true;
}

void SuperRibbonGeometrieSerie::computeDerivationsPreTermsSecondOrder()
{
  if (called["computeDerivationsPreTermsSecondOrder"])return;
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  dLambda0dbm << 0., 0., 1.;
  dLambda0dmb << 0., 0., 1.;
  dLambda1dam << 0., 0., 1.;
  dLambda1dbn << 0., 0., 1.;
  dLambda1dnb << 0., 0., 1.;
  dLambda1dma << 0., 0., 1.;
  dLambda2dan << 0., 0., 1.;
  dLambda2dna << 0., 0., 1.;
  dLambda0dbmSq = skewOf(dLambda0dbm);
  dLambda0dmbSq = skewOf(dLambda0dmb);
  dLambda1damSq = skewOf(dLambda1dam);
  dLambda1dbnSq = skewOf(dLambda1dbn);
  dLambda1dnbSq = skewOf(dLambda1dnb);
  dLambda1dmaSq = skewOf(dLambda1dma);
  dLambda2danSq = skewOf(dLambda2dan);
  dLambda2dnaSq = skewOf(dLambda2dna);

  int order = FramePreTerms.size();
  assert(order > 0);

  //Derivation of the frame
  {
    dFramePreTermsdaa.resize(order);
    dFramePreTermsdba.resize(order);
    dFramePreTermsdna.resize(order);
    dFramePreTermsdma.resize(order);
    dFramePreTermsdab.resize(order);
    dFramePreTermsdbb.resize(order);
    dFramePreTermsdnb.resize(order);
    dFramePreTermsdmb.resize(order);
    dFramePreTermsdan.resize(order);
    dFramePreTermsdbn.resize(order);
    dFramePreTermsdnn.resize(order);
    dFramePreTermsdmn.resize(order);
    dFramePreTermsdam.resize(order);
    dFramePreTermsdbm.resize(order);
    dFramePreTermsdnm.resize(order);
    dFramePreTermsdmm.resize(order);
    Eigen::Matrix3r zero;
    zero.setZero();
    //order 0
    dFramePreTermsdaa[0]=zero;
    dFramePreTermsdba[0]=zero;
    dFramePreTermsdna[0]=zero;
    dFramePreTermsdma[0]=zero;
    dFramePreTermsdab[0]=zero;
    dFramePreTermsdbb[0]=zero;
    dFramePreTermsdnb[0]=zero;
    dFramePreTermsdmb[0]=zero;
    dFramePreTermsdan[0]=zero;
    dFramePreTermsdbn[0]=zero;
    dFramePreTermsdnn[0]=zero;
    dFramePreTermsdmn[0]=zero;
    dFramePreTermsdam[0]=zero;
    dFramePreTermsdbm[0]=zero;
    dFramePreTermsdnm[0]=zero;
    dFramePreTermsdmm[0]=zero;
    //order 1
    dFramePreTermsdaa[1]=zero;
    dFramePreTermsdab[1]=zero;
    dFramePreTermsdan[1]=zero;
    dFramePreTermsdam[1]=zero;
    dFramePreTermsdba[1]=zero;
    dFramePreTermsdbb[1]=zero;
    dFramePreTermsdbn[1]=zero;
    dFramePreTermsdbm[1]=dLambda0dbmSq;
    dFramePreTermsdna[1]=zero;
    dFramePreTermsdnb[1]=zero;
    dFramePreTermsdnn[1]=zero;
    dFramePreTermsdnm[1]=zero;
    dFramePreTermsdma[1]=zero;
    dFramePreTermsdmb[1]=dLambda0dmbSq;
    dFramePreTermsdmn[1]=zero;
    dFramePreTermsdmm[1]=zero;
    //order 2
    dFramePreTermsdaa[2]=zero;
    dFramePreTermsdab[2]=zero;
    dFramePreTermsdan[2]=zero;
    dFramePreTermsdam[2]=0.5 * dLambda1damSq;
    dFramePreTermsdba[2]=zero;
    dFramePreTermsdbb[2]=dLambda0dbSq * dLambda0dbSq;
    dFramePreTermsdbn[2]=0.5 * dLambda1dbnSq;
    dFramePreTermsdbm[2]=0.5 * (dLambda0dbmSq * lambda0Sq + lambda0Sq * dLambda0dbmSq + dLambda0dbSq * dLambda0dmSq + dLambda0dmSq * dLambda0dbSq);
    dFramePreTermsdna[2]=zero;
    dFramePreTermsdnb[2]=0.5 * dLambda1dnbSq;
    dFramePreTermsdnn[2]=zero;
    dFramePreTermsdnm[2]=zero;
    dFramePreTermsdma[2]=0.5 * dLambda1dmaSq;
    dFramePreTermsdmb[2]=0.5 * (dLambda0dbmSq * lambda0Sq + lambda0Sq * dLambda0dbmSq + dLambda0dbSq * dLambda0dmSq + dLambda0dmSq * dLambda0dbSq);
    dFramePreTermsdmn[2]=zero;
    dFramePreTermsdmm[2]=dLambda0dmSq * dLambda0dmSq;
    //order n
    for (int n = 3; n < order; n++) {
      dFramePreTermsdaa[n]=((1. / static_cast<real>(n)) * (
                                    dFramePreTermsda[n - 2]*dLambda1daSq + dFramePreTermsda[n - 3]*dLambda2daSq
                                    + dFramePreTermsdaa[n - 1]*lambda0Sq + dFramePreTermsdaa[n - 2]*lambda1Sq + dFramePreTermsdaa[n - 3]*lambda2Sq
                                    + dFramePreTermsda[n - 2]*dLambda1daSq + dFramePreTermsda[n - 3]*dLambda2daSq));
      dFramePreTermsdab[n]=((1. / static_cast<real>(n)) * (
                                    dFramePreTermsdb[n - 2]*dLambda1daSq + dFramePreTermsdb[n - 3]*dLambda2daSq
                                    + dFramePreTermsda[n - 1]*dLambda0dbSq + dFramePreTermsda[n - 2]*dLambda1dbSq
                                    + dFramePreTermsdab[n - 1]*lambda0Sq + dFramePreTermsdab[n - 2]*lambda1Sq + dFramePreTermsdab[n - 3]*lambda2Sq));
      dFramePreTermsdan[n]=((1. / static_cast<real>(n)) * (
                                    FramePreTerms[n - 3]*dLambda2danSq
                                    + dFramePreTermsdn[n - 2]*dLambda1daSq + dFramePreTermsdn[n - 3]*dLambda2daSq
                                    + dFramePreTermsda[n - 2]*dLambda1dnSq + dFramePreTermsda[n - 3]*dLambda2dnSq
                                    + dFramePreTermsdan[n - 1]*lambda0Sq + dFramePreTermsdan[n - 2]*lambda1Sq + dFramePreTermsdan[n - 3]*lambda2Sq));
      dFramePreTermsdam[n]=((1. / static_cast<real>(n)) * (
                                    FramePreTerms[n - 2]*dLambda1damSq
                                    + dFramePreTermsdm[n - 2]*dLambda1daSq + dFramePreTermsdm[n - 3]*dLambda2daSq
                                    + dFramePreTermsda[n - 1]*dLambda0dmSq + dFramePreTermsda[n - 2]*dLambda1dmSq
                                    + dFramePreTermsdam[n - 1]*lambda0Sq + dFramePreTermsdam[n - 2]*lambda1Sq + dFramePreTermsdam[n - 3]*lambda2Sq));

      dFramePreTermsdba[n]=((1. / static_cast<real>(n)) * (
                                    dFramePreTermsda[n - 1]*dLambda0dbSq + dFramePreTermsda[n - 2]*dLambda1dbSq
                                    + dFramePreTermsdba[n - 1]*lambda0Sq + dFramePreTermsdba[n - 2]*lambda1Sq + dFramePreTermsdba[n - 3]*lambda2Sq
                                    + dFramePreTermsdb[n - 2]*dLambda1daSq + dFramePreTermsdb[n - 3]*dLambda2daSq));
      dFramePreTermsdbb[n]=((1. / static_cast<real>(n)) * (
                                    dFramePreTermsdb[n - 1]*dLambda0dbSq + dFramePreTermsdb[n - 2]*dLambda1dbSq
                                    + dFramePreTermsdbb[n - 1]*lambda0Sq + dFramePreTermsdbb[n - 2]*lambda1Sq + dFramePreTermsdbb[n - 3]*lambda2Sq
                                    + dFramePreTermsdb[n - 1]*dLambda0dbSq + dFramePreTermsdb[n - 2]*dLambda1dbSq));
      dFramePreTermsdbn[n]=((1. / static_cast<real>(n)) * (
                                    FramePreTerms[n - 2]*dLambda1dbnSq
                                    + dFramePreTermsdn[n - 1]*dLambda0dbSq + dFramePreTermsdn[n - 2]*dLambda1dbSq
                                    + dFramePreTermsdbn[n - 1]*lambda0Sq + dFramePreTermsdbn[n - 2]*lambda1Sq + dFramePreTermsdbn[n - 3]*lambda2Sq
                                    + dFramePreTermsdb[n - 2]*dLambda1dnSq + dFramePreTermsdb[n - 3]*dLambda2dnSq));
      dFramePreTermsdbm[n]=((1. / static_cast<real>(n)) * (
                                    FramePreTerms[n - 1]*dLambda0dbmSq
                                    + dFramePreTermsdm[n - 1]*dLambda0dbSq + dFramePreTermsdm[n - 2]*dLambda1dbSq
                                    + dFramePreTermsdbm[n - 1]*lambda0Sq + dFramePreTermsdbm[n - 2]*lambda1Sq + dFramePreTermsdbm[n - 3]*lambda2Sq
                                    + dFramePreTermsdb[n - 1]*dLambda0dmSq + dFramePreTermsdb[n - 2]*dLambda1dmSq));

      dFramePreTermsdna[n]=((1. / static_cast<real>(n)) * (
                                    FramePreTerms[n - 3]*dLambda2dnaSq
                                    + dFramePreTermsda[n - 2]*dLambda1dnSq + dFramePreTermsda[n - 3]*dLambda2dnSq
                                    + dFramePreTermsdna[n - 1]*lambda0Sq + dFramePreTermsdna[n - 2]*lambda1Sq + dFramePreTermsdna[n - 3]*lambda2Sq
                                    + dFramePreTermsdn[n - 2]*dLambda1daSq + dFramePreTermsdn[n - 3]*dLambda2daSq));
      dFramePreTermsdnb[n]=((1. / static_cast<real>(n)) * (
                                    FramePreTerms[n - 2]*dLambda1dnbSq
                                    + dFramePreTermsdb[n - 2]*dLambda1dnSq + dFramePreTermsdb[n - 3]*dLambda2dnSq
                                    + dFramePreTermsdnb[n - 1]*lambda0Sq + dFramePreTermsdnb[n - 2]*lambda1Sq + dFramePreTermsdnb[n - 3]*lambda2Sq
                                    + dFramePreTermsdn[n - 1]*dLambda0dbSq + dFramePreTermsdn[n - 2]*dLambda1dbSq));
      dFramePreTermsdnn[n]=((1. / static_cast<real>(n)) * (
                                    dFramePreTermsdn[n - 2]*dLambda1dnSq + dFramePreTermsdn[n - 3]*dLambda2dnSq
                                    + dFramePreTermsdnn[n - 1]*lambda0Sq + dFramePreTermsdnn[n - 2]*lambda1Sq + dFramePreTermsdnn[n - 3]*lambda2Sq
                                    + dFramePreTermsdn[n - 2]*dLambda1dnSq + dFramePreTermsdn[n - 3]*dLambda2dnSq));
      dFramePreTermsdnm[n]=((1. / static_cast<real>(n)) * (
                                    dFramePreTermsdm[n - 2]*dLambda1dnSq + dFramePreTermsdm[n - 3]*dLambda2dnSq
                                    + dFramePreTermsdnm[n - 1]*lambda0Sq + dFramePreTermsdnm[n - 2]*lambda1Sq + dFramePreTermsdnm[n - 3]*lambda2Sq
                                    + dFramePreTermsdn[n - 1]*dLambda0dmSq + dFramePreTermsdn[n - 2]*dLambda1dmSq));

      dFramePreTermsdma[n]=((1. / static_cast<real>(n)) * (
                                    FramePreTerms[n - 2]*dLambda1dmaSq
                                    + dFramePreTermsda[n - 1]*dLambda0dmSq + dFramePreTermsda[n - 2]*dLambda1dmSq
                                    + dFramePreTermsdma[n - 1]*lambda0Sq + dFramePreTermsdma[n - 2]*lambda1Sq + dFramePreTermsdma[n - 3]*lambda2Sq
                                    + dFramePreTermsdm[n - 2]*dLambda1daSq + dFramePreTermsdm[n - 3]*dLambda2daSq));
      dFramePreTermsdmb[n]=((1. / static_cast<real>(n)) * (
                                    FramePreTerms[n - 1]*dLambda0dmbSq
                                    + dFramePreTermsdb[n - 1]*dLambda0dmSq + dFramePreTermsdb[n - 2]*dLambda1dmSq
                                    + dFramePreTermsdmb[n - 1]*lambda0Sq + dFramePreTermsdmb[n - 2]*lambda1Sq + dFramePreTermsdmb[n - 3]*lambda2Sq
                                    + dFramePreTermsdm[n - 1]*dLambda0dbSq + dFramePreTermsdm[n - 2]*dLambda1dbSq));
      dFramePreTermsdmn[n]=((1. / static_cast<real>(n)) * (
                                    dFramePreTermsdn[n - 1]*dLambda0dmSq + dFramePreTermsdn[n - 2]*dLambda1dmSq
                                    + dFramePreTermsdmn[n - 1]*lambda0Sq + dFramePreTermsdmn[n - 2]*lambda1Sq + dFramePreTermsdmn[n - 3]*lambda2Sq
                                    + dFramePreTermsdm[n - 2]*dLambda1dnSq + dFramePreTermsdm[n - 3]*dLambda2dnSq));
      dFramePreTermsdmm[n]=((1. / static_cast<real>(n)) * (
                                    dFramePreTermsdm[n - 1]*dLambda0dmSq + dFramePreTermsdm[n - 2]*dLambda1dmSq
                                    + dFramePreTermsdmm[n - 1]*lambda0Sq + dFramePreTermsdmm[n - 2]*lambda1Sq + dFramePreTermsdmm[n - 3]*lambda2Sq
                                    + dFramePreTermsdm[n - 1]*dLambda0dmSq + dFramePreTermsdm[n - 2]*dLambda1dmSq));
    }
  }

  //Derivation of the position
  { 
    dPosPreTermsdaa.resize(order);
    dPosPreTermsdba.resize(order);
    dPosPreTermsdna.resize(order);
    dPosPreTermsdma.resize(order);
    dPosPreTermsdab.resize(order);
    dPosPreTermsdbb.resize(order);
    dPosPreTermsdnb.resize(order);
    dPosPreTermsdmb.resize(order);
    dPosPreTermsdan.resize(order);
    dPosPreTermsdbn.resize(order);
    dPosPreTermsdnn.resize(order);
    dPosPreTermsdmn.resize(order);
    dPosPreTermsdam.resize(order);
    dPosPreTermsdbm.resize(order);
    dPosPreTermsdnm.resize(order);
    dPosPreTermsdmm.resize(order);
    Eigen::Vector3r zero;
    zero.setZero();
    Eigen::Vector3r vnan(NAN, NAN, NAN);

    //order 0
    dPosPreTermsdaa[0]=vnan;
    dPosPreTermsdba[0]=vnan;
    dPosPreTermsdna[0]=vnan;
    dPosPreTermsdma[0]=vnan;
    dPosPreTermsdab[0]=vnan;
    dPosPreTermsdbb[0]=vnan;
    dPosPreTermsdnb[0]=vnan;
    dPosPreTermsdmb[0]=vnan;
    dPosPreTermsdan[0]=vnan;
    dPosPreTermsdbn[0]=vnan;
    dPosPreTermsdnn[0]=vnan;
    dPosPreTermsdmn[0]=vnan;
    dPosPreTermsdam[0]=vnan;
    dPosPreTermsdbm[0]=vnan;
    dPosPreTermsdnm[0]=vnan;
    dPosPreTermsdmm[0]=vnan;
    //order 1
    dPosPreTermsdaa[1]=zero;
    dPosPreTermsdba[1]=zero;
    dPosPreTermsdna[1]=zero;
    dPosPreTermsdma[1]=zero;
    dPosPreTermsdab[1]=zero;
    dPosPreTermsdbb[1]=zero;
    dPosPreTermsdnb[1]=zero;
    dPosPreTermsdmb[1]=zero;
    dPosPreTermsdan[1]=zero;
    dPosPreTermsdbn[1]=zero;
    dPosPreTermsdnn[1]=zero;
    dPosPreTermsdmn[1]=zero;
    dPosPreTermsdam[1]=zero;
    dPosPreTermsdbm[1]=zero;
    dPosPreTermsdnm[1]=zero;
    dPosPreTermsdmm[1]=zero;
    //order n
    for (int n = 2; n < order; n++) {
      dPosPreTermsdaa[n]=(1. / static_cast<real>(n))*dFramePreTermsdaa[n - 1].col(2);
      dPosPreTermsdba[n]=(1. / static_cast<real>(n))*dFramePreTermsdba[n - 1].col(2);
      dPosPreTermsdna[n]=(1. / static_cast<real>(n))*dFramePreTermsdna[n - 1].col(2);
      dPosPreTermsdma[n]=(1. / static_cast<real>(n))*dFramePreTermsdma[n - 1].col(2);
      dPosPreTermsdab[n]=(1. / static_cast<real>(n))*dFramePreTermsdab[n - 1].col(2);
      dPosPreTermsdbb[n]=(1. / static_cast<real>(n))*dFramePreTermsdbb[n - 1].col(2);
      dPosPreTermsdnb[n]=(1. / static_cast<real>(n))*dFramePreTermsdnb[n - 1].col(2);
      dPosPreTermsdmb[n]=(1. / static_cast<real>(n))*dFramePreTermsdmb[n - 1].col(2);
      dPosPreTermsdan[n]=(1. / static_cast<real>(n))*dFramePreTermsdan[n - 1].col(2);
      dPosPreTermsdbn[n]=(1. / static_cast<real>(n))*dFramePreTermsdbn[n - 1].col(2);
      dPosPreTermsdnn[n]=(1. / static_cast<real>(n))*dFramePreTermsdnn[n - 1].col(2);
      dPosPreTermsdmn[n]=(1. / static_cast<real>(n))*dFramePreTermsdmn[n - 1].col(2);
      dPosPreTermsdam[n]=(1. / static_cast<real>(n))*dFramePreTermsdam[n - 1].col(2);
      dPosPreTermsdbm[n]=(1. / static_cast<real>(n))*dFramePreTermsdbm[n - 1].col(2);
      dPosPreTermsdnm[n]=(1. / static_cast<real>(n))*dFramePreTermsdnm[n - 1].col(2);
      dPosPreTermsdmm[n]=(1. / static_cast<real>(n))*dFramePreTermsdmm[n - 1].col(2);
    }
  }

  called["computeDerivationsPreTermsSecondOrder"] = true;
}

Eigen::Matrix3r SuperRibbonGeometrieSerie::dFrameda(real s)
{
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  int order = dFramePreTermsda.size();
  computeSPowers(s);
  Eigen::Matrix3r res;
  res.setZero();
  //for (int i = 1; i < order; i++)
  for (int i = order-1; i > 0; i--)
    res += spowers[i] * dFramePreTermsda[i];
  res = frameOr * res;

  return res;
}

Eigen::Matrix3r SuperRibbonGeometrieSerie::dFramedb(real s)
{
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  int order = dFramePreTermsdb.size();
  computeSPowers(s);
  Eigen::Matrix3r res;
  res.setZero();
  //for (int i = 1; i < order; i++)
  for (int i = order-1; i > 0; i--)
    res += spowers[i] * dFramePreTermsdb[i];
  res = frameOr * res;

  return res;
}

Eigen::Matrix3r SuperRibbonGeometrieSerie::dFramedn(real s)
{
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  int order = dFramePreTermsdn.size();
  computeSPowers(s);
  Eigen::Matrix3r res;
  res.setZero();
  //for (int i = 1; i < order; i++)
  for (int i = order-1; i > 0; i--)
    res += spowers[i] * dFramePreTermsdn[i];
  res = frameOr * res;

  return res;
}

Eigen::Matrix3r SuperRibbonGeometrieSerie::dFramedm(real s)
{
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  int order = dFramePreTermsdm.size();
  computeSPowers(s);
  Eigen::Matrix3r res;
  res.setZero();
  //for (int i = 1; i < order; i++)
  for (int i = order-1; i > 0; i--)
    res += spowers[i] * dFramePreTermsdm[i];
  res = frameOr * res;

  return res;
}

Eigen::Vector3r SuperRibbonGeometrieSerie::dPosda(real s)
{
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  int order = dPosPreTermsda.size();
  computeSPowers(s);
  Eigen::Vector3r res(0., 0., 0.);
  //for (int i = 1; i < order; i++)
  for (int i = order-1; i > 0; i--)
    res += spowers[i] * dPosPreTermsda[i];
  res = frameOr * res;

  return res;
}

Eigen::Vector3r SuperRibbonGeometrieSerie::dPosdb(real s)
{
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  int order = dPosPreTermsdb.size();
  computeSPowers(s);
  Eigen::Vector3r res(0., 0., 0.);
  //for (int i = 1; i < order; i++)
  for (int i = order-1; i > 0; i--)
    res += spowers[i] * dPosPreTermsdb[i];
  res = frameOr * res;

  return res;
}

Eigen::Vector3r SuperRibbonGeometrieSerie::dPosdn(real s)
{
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  int order = dPosPreTermsdn.size();
  computeSPowers(s);
  Eigen::Vector3r res(0., 0., 0.);
  //for (int i = 1; i < order; i++)
  for (int i = order-1; i > 0; i--)
    res += spowers[i] * dPosPreTermsdn[i];
  res = frameOr * res;

  return res;
}

Eigen::Vector3r SuperRibbonGeometrieSerie::dPosdm(real s)
{
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  int order = dPosPreTermsdm.size();
  computeSPowers(s);
  Eigen::Vector3r res(0., 0., 0.);
  //for (int i = 1; i < order; i++)
  for (int i = order-1; i > 0; i--)
    res += spowers[i] * dPosPreTermsdm[i];
  res = frameOr * res;

  return res;
}

Eigen::Matrix<Eigen::Matrix4r, 3, 3> SuperRibbonGeometrieSerie::hessianFrame(real s)
{
  if (!called["computeDerivationsPreTermsSecondOrder"])computeDerivationsPreTermsSecondOrder();
  int order = dFramePreTermsdaa.size();
  computeSPowers(s);
  Eigen::Matrix3r resaa;
  Eigen::Matrix3r resba;
  Eigen::Matrix3r resna;
  Eigen::Matrix3r resma;
  Eigen::Matrix3r resab;
  Eigen::Matrix3r resbb;
  Eigen::Matrix3r resnb;
  Eigen::Matrix3r resmb;
  Eigen::Matrix3r resan;
  Eigen::Matrix3r resbn;
  Eigen::Matrix3r resnn;
  Eigen::Matrix3r resmn;
  Eigen::Matrix3r resam;
  Eigen::Matrix3r resbm;
  Eigen::Matrix3r resnm;
  Eigen::Matrix3r resmm;
  resaa.setZero();
  resba.setZero();
  resna.setZero();
  resma.setZero();
  resab.setZero();
  resbb.setZero();
  resnb.setZero();
  resmb.setZero();
  resan.setZero();
  resbn.setZero();
  resnn.setZero();
  resmn.setZero();
  resam.setZero();
  resbm.setZero();
  resnm.setZero();
  resmm.setZero();

  //for (int i = 1; i < order; i++) {
  for (int i = order-1; i > 0; i--) {
    resaa += spowers[i] * dFramePreTermsdaa[i];
    resba += spowers[i] * dFramePreTermsdba[i];
    resna += spowers[i] * dFramePreTermsdna[i];
    resma += spowers[i] * dFramePreTermsdma[i];
    resab += spowers[i] * dFramePreTermsdab[i];
    resbb += spowers[i] * dFramePreTermsdbb[i];
    resnb += spowers[i] * dFramePreTermsdnb[i];
    resmb += spowers[i] * dFramePreTermsdmb[i];
    resan += spowers[i] * dFramePreTermsdan[i];
    resbn += spowers[i] * dFramePreTermsdbn[i];
    resnn += spowers[i] * dFramePreTermsdnn[i];
    resmn += spowers[i] * dFramePreTermsdmn[i];
    resam += spowers[i] * dFramePreTermsdam[i];
    resbm += spowers[i] * dFramePreTermsdbm[i];
    resnm += spowers[i] * dFramePreTermsdnm[i];
    resmm += spowers[i] * dFramePreTermsdmm[i];
  }
  resaa = frameOr * resaa;
  resba = frameOr * resba;
  resna = frameOr * resna;
  resma = frameOr * resma;
  resab = frameOr * resab;
  resbb = frameOr * resbb;
  resnb = frameOr * resnb;
  resmb = frameOr * resmb;
  resan = frameOr * resan;
  resbn = frameOr * resbn;
  resnn = frameOr * resnn;
  resmn = frameOr * resmn;
  resam = frameOr * resam;
  resbm = frameOr * resbm;
  resnm = frameOr * resnm;
  resmm = frameOr * resmm;
  Eigen::Matrix<Eigen::Matrix4r, 3, 3> res;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      res(i, j)(0, 0) = resaa(i, j);
      res(i, j)(0, 1) = resab(i, j);
      res(i, j)(0, 2) = resan(i, j);
      res(i, j)(0, 3) = resam(i, j);
      res(i, j)(1, 0) = resba(i, j);
      res(i, j)(1, 1) = resbb(i, j);
      res(i, j)(1, 2) = resbn(i, j);
      res(i, j)(1, 3) = resbm(i, j);
      res(i, j)(2, 0) = resna(i, j);
      res(i, j)(2, 1) = resnb(i, j);
      res(i, j)(2, 2) = resnn(i, j);
      res(i, j)(2, 3) = resnm(i, j);
      res(i, j)(3, 0) = resma(i, j);
      res(i, j)(3, 1) = resmb(i, j);
      res(i, j)(3, 2) = resmn(i, j);
      res(i, j)(3, 3) = resmm(i, j);
    }
  }
  return res;
}

Eigen::Matrix<Eigen::Matrix4r, 3, 1> SuperRibbonGeometrieSerie::hessianPos(real s)
{
  if (!called["computeDerivationsPreTermsSecondOrder"])computeDerivationsPreTermsSecondOrder();
  int order = dPosPreTermsdaa.size();
  computeSPowers(s);
  Eigen::Vector3r resaa;
  Eigen::Vector3r resba;
  Eigen::Vector3r resna;
  Eigen::Vector3r resma;
  Eigen::Vector3r resab;
  Eigen::Vector3r resbb;
  Eigen::Vector3r resnb;
  Eigen::Vector3r resmb;
  Eigen::Vector3r resan;
  Eigen::Vector3r resbn;
  Eigen::Vector3r resnn;
  Eigen::Vector3r resmn;
  Eigen::Vector3r resam;
  Eigen::Vector3r resbm;
  Eigen::Vector3r resnm;
  Eigen::Vector3r resmm;
  resaa.setZero();
  resba.setZero();
  resna.setZero();
  resma.setZero();
  resab.setZero();
  resbb.setZero();
  resnb.setZero();
  resmb.setZero();
  resan.setZero();
  resbn.setZero();
  resnn.setZero();
  resmn.setZero();
  resam.setZero();
  resbm.setZero();
  resnm.setZero();
  resmm.setZero();

  //for (int i = 1; i < order; i++) {
  for (int i = order-1; i > 0; i--) {
    resaa += spowers[i] * dPosPreTermsdaa[i];
    resba += spowers[i] * dPosPreTermsdba[i];
    resna += spowers[i] * dPosPreTermsdna[i];
    resma += spowers[i] * dPosPreTermsdma[i];
    resab += spowers[i] * dPosPreTermsdab[i];
    resbb += spowers[i] * dPosPreTermsdbb[i];
    resnb += spowers[i] * dPosPreTermsdnb[i];
    resmb += spowers[i] * dPosPreTermsdmb[i];
    resan += spowers[i] * dPosPreTermsdan[i];
    resbn += spowers[i] * dPosPreTermsdbn[i];
    resnn += spowers[i] * dPosPreTermsdnn[i];
    resmn += spowers[i] * dPosPreTermsdmn[i];
    resam += spowers[i] * dPosPreTermsdam[i];
    resbm += spowers[i] * dPosPreTermsdbm[i];
    resnm += spowers[i] * dPosPreTermsdnm[i];
    resmm += spowers[i] * dPosPreTermsdmm[i];
  }
  resaa = frameOr * resaa;
  resba = frameOr * resba;
  resna = frameOr * resna;
  resma = frameOr * resma;
  resab = frameOr * resab;
  resbb = frameOr * resbb;
  resnb = frameOr * resnb;
  resmb = frameOr * resmb;
  resan = frameOr * resan;
  resbn = frameOr * resbn;
  resnn = frameOr * resnn;
  resmn = frameOr * resmn;
  resam = frameOr * resam;
  resbm = frameOr * resbm;
  resnm = frameOr * resnm;
  resmm = frameOr * resmm;
  Eigen::Matrix<Eigen::Matrix4r, 3, 1> res;
  for (int i = 0; i < 3; i++) {
    res(i)(0, 0) = resaa(i);
    res(i)(0, 1) = resab(i);
    res(i)(0, 2) = resan(i);
    res(i)(0, 3) = resam(i);
    res(i)(1, 0) = resba(i);
    res(i)(1, 1) = resbb(i);
    res(i)(1, 2) = resbn(i);
    res(i)(1, 3) = resbm(i);
    res(i)(2, 0) = resna(i);
    res(i)(2, 1) = resnb(i);
    res(i)(2, 2) = resnn(i);
    res(i)(2, 3) = resnm(i);
    res(i)(3, 0) = resma(i);
    res(i)(3, 1) = resmb(i);
    res(i)(3, 2) = resmn(i);
    res(i)(3, 3) = resmm(i);
  }
  return res;
}

bool SuperRibbonGeometrieSerie::checkDerivations()
{
  std::cout << "=== test SuperRibbonGeometrieSerie ===" << std::endl;
  LinearScalar omega1Or = omega1;
  LinearScalar etaOr = eta;

  auto initvars = [&](bool varomega, bool varlin, real offset) {
    omega1 = omega1Or;
    eta = etaOr;
    if (varomega) {
      if (varlin)omega1.A += offset;
      else omega1.B += offset;
    } else {
      if (varlin)eta.A += offset;
      else eta.B += offset;
    }
    Setup(omega1, eta);
  };
  auto Test = [&](bool varomega, bool varlin, real offset)->std::vector<real> {
    std::vector<Eigen::Matrix3r> estimV;

    initvars(varomega, varlin, offset);
    computeSeriePreTerms(10);
    estimV = FramePreTerms;
    initvars(varomega, varlin, -offset);
    computeSeriePreTerms(10);
    size_t t = estimV.size();
    for (size_t i = 0; i < t; i++)
      estimV[i] -= FramePreTerms[i];
    real varlen = 2.*offset;
    for (size_t i = 0; i < t; i++)
      estimV[i] = (1. / varlen) * estimV[i].eval();
    Setup(omega1Or, etaOr);
    computeSeriePreTerms(10);
    computeDerivationsPreTerms();
    std::vector<real> res(t);
    std::vector<Eigen::Matrix3r> &dFrameSeriedRef = std::ref(dFramePreTermsda);
    if (varomega)
    {
      if (varlin)dFrameSeriedRef = std::ref(dFramePreTermsda);
      else dFrameSeriedRef = std::ref(dFramePreTermsdb);
    } else
    {
      if (varlin)dFrameSeriedRef = std::ref(dFramePreTermsdn);
      else dFrameSeriedRef = std::ref(dFramePreTermsdm);
    }
    for (size_t i = 0; i < t; i++)
    {
      res[i] = (estimV[i] - dFrameSeriedRef[i]).lpNorm<Eigen::Infinity>();
    }
    return res;
  };
  auto Validate = [&](std::vector<real> &V, real tolerance)->bool {
    for (auto x : V)if (x > tolerance)return false;
    return true;
  };
  auto res = Test(true, true, 0.5e-5);
  if (!Validate(res, 1.e-10))return false;
  std::cout << "[1/4]PASSED" << std::endl;
  res = Test(true, false, 0.5e-5);
  if (!Validate(res, 1.e-10))return false;
  std::cout << "[2/4]PASSED" << std::endl;
  res = Test(false, true, 0.5e-5);
  if (!Validate(res, 1.e-10))return false;
  std::cout << "[3/4]PASSED" << std::endl;
  res = Test(false, false, 0.5e-5);
  if (!Validate(res, 1.e-10))return false;
  std::cout << "[4/4]PASSED" << std::endl;
  return true;
}

void SuperRibbonGeometrieSerie::computePreAltitudeEnergyPreTerms()
{
  if (called["computePreAltitudeEnergyPreTerms"])return;
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  const real w = ribbonWidth;
  int order = FramePreTerms.size();
  preAltitudeEnergyPreTerms.resize(order);
  preAltitudeEnergyLinearTerm = w * posOr;
  Eigen::Vector3r vnan(NAN, NAN, NAN);
  preAltitudeEnergyPreTerms[0]=vnan;
  preAltitudeEnergyPreTerms[1]=vnan;
  for (int k = 2; k < order; k++)
    preAltitudeEnergyPreTerms[k]=(1. / static_cast<real>(k)) * (w * PosPreTerms[k - 1]);
  called["computePreAltitudeEnergyPreTerms"] = true;
}

void SuperRibbonGeometrieSerie::computePreAltitudeEnergyPreTermsDerivations()
{
  if (called["computePreAltitudeEnergyPreTermsDerivations"])return;
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  const real w = ribbonWidth;
  int order = FramePreTerms.size();
  assert(order > 0);

  dPreAltitudeEnergyPreTermda.resize(order);
  dPreAltitudeEnergyPreTermdb.resize(order);
  dPreAltitudeEnergyPreTermdn.resize(order);
  dPreAltitudeEnergyPreTermdm.resize(order);
  for(int idx=0;idx<2;idx++)
  {
    dPreAltitudeEnergyPreTermda[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdb[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdn[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdm[idx]=Eigen::Vector3r(0., 0., 0.);
  }
  for (int k = 2; k < order; k++) {
    dPreAltitudeEnergyPreTermda[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsda[k - 1]);
    dPreAltitudeEnergyPreTermdb[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdb[k - 1]);
    dPreAltitudeEnergyPreTermdn[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdn[k - 1]);
    dPreAltitudeEnergyPreTermdm[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdm[k - 1]);
  }
  called["computePreAltitudeEnergyPreTermsDerivations"] = true;
}



void SuperRibbonGeometrieSerie::computePreAltitudeEnergyPreTermsDerivationsSecondOrder()
{
  if (called["computePreAltitudeEnergyPreTermsDerivationsSecondOrder"])return;
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  if (!called["computeDerivationsPreTermsSecondOrder"])computeDerivationsPreTermsSecondOrder();
  if (!called["computePreAltitudeEnergyPreTermsDerivations"])computePreAltitudeEnergyPreTermsDerivations();
  const real w = ribbonWidth;
  int order = FramePreTerms.size();
  assert(order > 0);

  dPreAltitudeEnergyPreTermdaa.resize(order);
  dPreAltitudeEnergyPreTermdba.resize(order);
  dPreAltitudeEnergyPreTermdna.resize(order);
  dPreAltitudeEnergyPreTermdma.resize(order);
  dPreAltitudeEnergyPreTermdab.resize(order);
  dPreAltitudeEnergyPreTermdbb.resize(order);
  dPreAltitudeEnergyPreTermdnb.resize(order);
  dPreAltitudeEnergyPreTermdmb.resize(order);
  dPreAltitudeEnergyPreTermdan.resize(order);
  dPreAltitudeEnergyPreTermdbn.resize(order);
  dPreAltitudeEnergyPreTermdnn.resize(order);
  dPreAltitudeEnergyPreTermdmn.resize(order);
  dPreAltitudeEnergyPreTermdam.resize(order);
  dPreAltitudeEnergyPreTermdbm.resize(order);
  dPreAltitudeEnergyPreTermdnm.resize(order);
  dPreAltitudeEnergyPreTermdmm.resize(order);
  for(int idx=0;idx<2;idx++)
  {
    dPreAltitudeEnergyPreTermdaa[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdba[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdna[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdma[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdab[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdbb[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdnb[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdmb[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdan[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdbn[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdnn[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdmn[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdam[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdbm[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdnm[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdmm[idx]=Eigen::Vector3r(0., 0., 0.);
  }

  for (int k = 2; k < order; k++) {
    dPreAltitudeEnergyPreTermdaa[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdaa[k - 1]);
    dPreAltitudeEnergyPreTermdba[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdba[k - 1]);
    dPreAltitudeEnergyPreTermdna[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdna[k - 1]);
    dPreAltitudeEnergyPreTermdma[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdma[k - 1]);
    dPreAltitudeEnergyPreTermdab[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdab[k - 1]);
    dPreAltitudeEnergyPreTermdbb[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdbb[k - 1]);
    dPreAltitudeEnergyPreTermdnb[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdnb[k - 1]);
    dPreAltitudeEnergyPreTermdmb[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdmb[k - 1]);
    dPreAltitudeEnergyPreTermdan[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdan[k - 1]);
    dPreAltitudeEnergyPreTermdbn[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdbn[k - 1]);
    dPreAltitudeEnergyPreTermdnn[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdnn[k - 1]);
    dPreAltitudeEnergyPreTermdmn[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdmn[k - 1]);
    dPreAltitudeEnergyPreTermdam[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdam[k - 1]);
    dPreAltitudeEnergyPreTermdbm[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdbm[k - 1]);
    dPreAltitudeEnergyPreTermdnm[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdnm[k - 1]);
    dPreAltitudeEnergyPreTermdmm[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdmm[k - 1]);
  }
  called["computePreAltitudeEnergyPreTermsDerivationsSecondOrder"] = true;
}


Eigen::Vector3r SuperRibbonGeometrieSerie::preAltitudeEnergy(real s)
{
  if (!called["computePreAltitudeEnergyPreTerms"])computePreAltitudeEnergyPreTerms();
  int order = preAltitudeEnergyPreTerms.size();
  computeSPowers(s);
  Eigen::Vector3r res(0., 0., 0.);
  //for (int i = 2; i < order; i++)
  for (int i = order-1; i > 1; i--)
    res += spowers[i] * preAltitudeEnergyPreTerms[i];
  res = frameOr * res;
  res += preAltitudeEnergyLinearTerm * spowers[1];
  return res;
}

Eigen::Vector3r SuperRibbonGeometrieSerie::preAltitudeRotationalEnergy(real s)
{
    if (!called["computePreAltitudeEnergyPreTerms"])computePreAltitudeEnergyPreTerms();
  int order = preAltitudeEnergyPreTerms.size();
  computeSPowers(s);
  Eigen::Vector3r res(0., 0., 0.);
  //for (int i = 2; i < order; i++)
  for (int i = order-1; i > 1; i--)
    res += spowers[i] * preAltitudeEnergyPreTerms[i];
  return res;
}


Eigen::Matrix<real, 3, 4> SuperRibbonGeometrieSerie::gradPreAltitudeEnergy(real s)
{
  if (!called["computePreAltitudeEnergyPreTermsDerivations"])computePreAltitudeEnergyPreTermsDerivations();
  Eigen::Matrix<real, 3, 4> res;
  res.setZero();
  int order = dPreAltitudeEnergyPreTermda.size();
  computeSPowers(s);
  //for (int i = 2; i < order; i++) {
  for (int i = order-1; i > 1; i--) {
    res.col(0) += spowers[i] * dPreAltitudeEnergyPreTermda[i];
    res.col(1) += spowers[i] * dPreAltitudeEnergyPreTermdb[i];
    res.col(2) += spowers[i] * dPreAltitudeEnergyPreTermdn[i];
    res.col(3) += spowers[i] * dPreAltitudeEnergyPreTermdm[i];
  }
  return frameOr * res;
}

Eigen::Matrix<Eigen::Vector3r, 3, 1> SuperRibbonGeometrieSerie::dPreAltitudeEnergydPos(real s)
{
  Eigen::Matrix<Eigen::Vector3r, 3, 1> res;
  res << Eigen::Vector3r(s * ribbonWidth, 0., 0.), Eigen::Vector3r(0., s * ribbonWidth, 0.), Eigen::Vector3r(0., 0., s * ribbonWidth);
  return res;
}

Eigen::Matrix<Eigen::Matrix3r, 3, 1> SuperRibbonGeometrieSerie::dPreAltitudeEnergydFrame(real s)
{
  if (!called["computePreAltitudeEnergyPreTerms"])computePreAltitudeEnergyPreTerms();
  int order = preAltitudeEnergyPreTerms.size();
  computeSPowers(s);
  Eigen::Vector3r K(0., 0., 0.);
  //for (int i = 2; i < order; i++)
  for (int i = order-1; i > 1; i--)
    K += spowers[i] * preAltitudeEnergyPreTerms[i];
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> res;
  res.setConstant(Eigen::Matrix3r::Zero());
  Eigen::Matrix3r temp;
  Eigen::Vector3r vtemp;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      temp.setZero();
      temp(i, j) = 1.;
      vtemp = temp * K;
      for (int a = 0; a < 3; a++)
        res[a](i, j) += vtemp[a];
    }
  }
  return res;
}

Eigen::Matrix<Eigen::Matrix<Eigen::Vector4r, 3, 3>, 3, 1> SuperRibbonGeometrieSerie::d2PreAltitudeEnergydFramedabnm(real s)
{
  if (!called["computePreAltitudeEnergyPreTermsDerivations"])computePreAltitudeEnergyPreTermsDerivations();
  Eigen::Vector3r Ka, Kb, Kn, Km;
  Ka.setZero();
  Kb.setZero();
  Kn.setZero();
  Km.setZero();
  int order = dPreAltitudeEnergyPreTermda.size();
  computeSPowers(s);
  //for (int i = 1; i < order; i++) {
  for (int i = order-1; i > 0; i--) {
    Ka += spowers[i] * dPreAltitudeEnergyPreTermda[i];
    Kb += spowers[i] * dPreAltitudeEnergyPreTermdb[i];
    Kn += spowers[i] * dPreAltitudeEnergyPreTermdn[i];
    Km += spowers[i] * dPreAltitudeEnergyPreTermdm[i];
  }
  Eigen::Matrix<Eigen::Matrix<Eigen::Vector4r, 3, 3>, 3, 1> res;
  Eigen::Matrix3r temp;
  Eigen::Vector3r va, vb, vn, vm;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      temp.setZero();
      temp(i, j) = 1.;
      va = temp * Ka;
      vb = temp * Kb;
      vn = temp * Kn;
      vm = temp * Km;
      for (int a = 0; a < 3; a++) {
        res[a](i, j)[0] = va[a];
        res[a](i, j)[1] = vb[a];
        res[a](i, j)[2] = vn[a];
        res[a](i, j)[3] = vm[a];
      }
    }
  }
  return res;
}


Eigen::Matrix<Eigen::Matrix4r, 3, 1> SuperRibbonGeometrieSerie::hessianPreAltitudeEnergy(real s)
{
  if (!called["computePreAltitudeEnergyPreTermsDerivationsSecondOrder"])computePreAltitudeEnergyPreTermsDerivationsSecondOrder();
  Eigen::Matrix<Eigen::Matrix4r, 3, 1> preRes;
  preRes.setConstant(Eigen::Matrix4r::Zero());
  int order = dPreAltitudeEnergyPreTermdaa.size();
  computeSPowers(s);
  //for (int i = 1; i < order; i++) {
  for (int i = order-1; i > 0; i--) {
    for (int j = 0; j < 3; j++) {
      preRes[j](0, 0) += spowers[i] * dPreAltitudeEnergyPreTermdaa[i][j];
      preRes[j](1, 0) += spowers[i] * dPreAltitudeEnergyPreTermdba[i][j];
      preRes[j](2, 0) += spowers[i] * dPreAltitudeEnergyPreTermdna[i][j];
      preRes[j](3, 0) += spowers[i] * dPreAltitudeEnergyPreTermdma[i][j];
      preRes[j](0, 1) += spowers[i] * dPreAltitudeEnergyPreTermdab[i][j];
      preRes[j](1, 1) += spowers[i] * dPreAltitudeEnergyPreTermdbb[i][j];
      preRes[j](2, 1) += spowers[i] * dPreAltitudeEnergyPreTermdnb[i][j];
      preRes[j](3, 1) += spowers[i] * dPreAltitudeEnergyPreTermdmb[i][j];
      preRes[j](0, 2) += spowers[i] * dPreAltitudeEnergyPreTermdan[i][j];
      preRes[j](1, 2) += spowers[i] * dPreAltitudeEnergyPreTermdbn[i][j];
      preRes[j](2, 2) += spowers[i] * dPreAltitudeEnergyPreTermdnn[i][j];
      preRes[j](3, 2) += spowers[i] * dPreAltitudeEnergyPreTermdmn[i][j];
      preRes[j](0, 3) += spowers[i] * dPreAltitudeEnergyPreTermdam[i][j];
      preRes[j](1, 3) += spowers[i] * dPreAltitudeEnergyPreTermdbm[i][j];
      preRes[j](2, 3) += spowers[i] * dPreAltitudeEnergyPreTermdnm[i][j];
      preRes[j](3, 3) += spowers[i] * dPreAltitudeEnergyPreTermdmm[i][j];
    }
  }
  Eigen::Matrix<Eigen::Matrix4r, 3, 1> res;
  for (int i = 0; i < 3; i++)res[i] = frameOr(i, 0) * preRes[0] + frameOr(i, 1) * preRes[1] + frameOr(i, 2) * preRes[2];
  return res;
}

Eigen::Matrix<Eigen::Vector3r, 3, 1> SuperRibbonGeometrieSerie::dPosdPos(real s)
{
  Eigen::Matrix<Eigen::Vector3r, 3, 1>res;
  res << Eigen::Vector3r(1., 0., 0.), Eigen::Vector3r(0., 1., 0.), Eigen::Vector3r(0., 0., 1.);
  return res;
}

Eigen::Matrix<Eigen::Matrix3r, 3, 3> SuperRibbonGeometrieSerie::dFramedFrame(real s)
{
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  int order = FramePreTerms.size();
  computeSPowers(s);
  Eigen::Matrix3r K = FramePreTerms[0];
  //for (int i = 1; i < order; i++)
  for (int i = order-1; i > 0; i--)
    K += spowers[i] * FramePreTerms[i];
  Eigen::Matrix<Eigen::Matrix3r, 3, 3> res;
  res.setConstant(Eigen::Matrix3r::Zero());
  Eigen::Matrix3r temp;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      temp.setZero();
      temp(i, j) = 1.;
      temp = temp * K;
      for (int a = 0; a < 3; a++) {
        for (int b = 0; b < 3; b++) {
          res(a, b)(i, j) = temp(a, b);
        }
      }
    }
  }
  return res;
}

Eigen::Matrix<Eigen::Matrix3r, 3, 1> SuperRibbonGeometrieSerie::dPosdFrame(real s)
{
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  int order = PosPreTerms.size();
  computeSPowers(s);
  Eigen::Vector3r K(0., 0., 0.);
  //for (int i = 1; i < order; i++)
  for (int i = order-1; i > 0; i--)
    K += spowers[i] * PosPreTerms[i];
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> res;
  Eigen::Matrix3r temp;
  Eigen::Vector3r vtemp;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      temp.setZero();
      temp(i, j) = 1.;
      vtemp = temp * K;
      for (int b = 0; b < 3; b++) {
        res[b](i, j) = vtemp(b);
      }
    }
  }
  return res;
}

Eigen::Matrix<Eigen::Matrix<Eigen::Vector4r, 3, 3>, 3, 3> SuperRibbonGeometrieSerie::d2FramedFramedabnm(real s)
{
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  int order = PosPreTerms.size();
  computeSPowers(s);
  Eigen::Matrix3r Ka = Eigen::Matrix3r::Zero();
  Eigen::Matrix3r Kb = Eigen::Matrix3r::Zero();
  Eigen::Matrix3r Kn = Eigen::Matrix3r::Zero();
  Eigen::Matrix3r Km = Eigen::Matrix3r::Zero();
  //for (int i = 1; i < order; i++) {
  for (int i = order-1; i > 0; i--) {
    Ka += spowers[i] * dFramePreTermsda[i];
    Kb += spowers[i] * dFramePreTermsdb[i];
    Kn += spowers[i] * dFramePreTermsdn[i];
    Km += spowers[i] * dFramePreTermsdm[i];
  }

  Eigen::Matrix<Eigen::Matrix<Eigen::Vector4r, 3, 3>, 3, 3> res;
  Eigen::Matrix<Eigen::Vector4r, 3, 3> tmp1;
  tmp1.setConstant(Eigen::Vector4r(0., 0., 0., 0.));
  res.setConstant(tmp1);
  Eigen::Matrix3r tmpa;
  Eigen::Matrix3r tmpb;
  Eigen::Matrix3r tmpn;
  Eigen::Matrix3r tmpm;
  Eigen::Matrix3r temp;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      temp.setZero();
      temp(i, j) = 1.;
      tmpa = temp * Ka;
      tmpb = temp * Kb;
      tmpn = temp * Kn;
      tmpm = temp * Km;
      for (int a = 0; a < 3; a++) {
        for (int b = 0; b < 3; b++) {
          res(a, b)(i, j)[0] = tmpa(a, b);
          res(a, b)(i, j)[1] = tmpb(a, b);
          res(a, b)(i, j)[2] = tmpn(a, b);
          res(a, b)(i, j)[3] = tmpm(a, b);
        }
      }
    }
  }
  return res;
}

Eigen::Matrix<Eigen::Matrix<Eigen::Vector4r, 3, 3>, 3, 1> SuperRibbonGeometrieSerie::d2PosdFramedabnm(real s)
{
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  int order = PosPreTerms.size();
  computeSPowers(s);
  Eigen::Vector3r Ka = Eigen::Vector3r::Zero();
  Eigen::Vector3r Kb = Eigen::Vector3r::Zero();
  Eigen::Vector3r Kn = Eigen::Vector3r::Zero();
  Eigen::Vector3r Km = Eigen::Vector3r::Zero();
  //for (int i = 1; i < order; i++) {
  for (int i = order-1; i > 0; i--) {
    Ka += spowers[i] * dPosPreTermsda[i];
    Kb += spowers[i] * dPosPreTermsdb[i];
    Kn += spowers[i] * dPosPreTermsdn[i];
    Km += spowers[i] * dPosPreTermsdm[i];
  }

  Eigen::Matrix<Eigen::Matrix<Eigen::Vector4r, 3, 3>, 3, 1> res;
  Eigen::Matrix<Eigen::Vector4r, 3, 3> tmp1;
  tmp1.setConstant(Eigen::Vector4r(0., 0., 0., 0.));
  res.setConstant(tmp1);
  Eigen::Vector3r tmpa;
  Eigen::Vector3r tmpb;
  Eigen::Vector3r tmpn;
  Eigen::Vector3r tmpm;
  Eigen::Matrix3r temp;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      temp.setZero();
      temp(i, j) = 1.;
      tmpa = temp * Ka;
      tmpb = temp * Kb;
      tmpn = temp * Kn;
      tmpm = temp * Km;
      for (int b = 0; b < 3; b++) {
        res(b)(i, j)[0] = tmpa(b);
        res(b)(i, j)[1] = tmpb(b);
        res(b)(i, j)[2] = tmpn(b);
        res(b)(i, j)[3] = tmpm(b);
      }
    }
  }
  return res;
}




void SuperRibbonGeometrieSerie::computeSPowers(real s)
{
  int order = FramePreTerms.size();
  assert(order > 0);
  if ((PosPreTerms.size() == spowers.size()) && (sPowersCurrentS == s))
    return;
  spowers.resize(order);
  spowers[0] = 1;
  spowers[1] = s;
  for (int i = 2; i < order; i++) {
    if (i % 2)
      spowers[i] = spowers[i / 2] * spowers[i / 2 + 1];
    else
      spowers[i] = spowers[i / 2] * spowers[i / 2];
  }
}
