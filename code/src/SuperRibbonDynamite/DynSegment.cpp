/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "DynSegment.hpp"
#include "PartSymbolic.hpp"
#include "PartSemiSymbolic.hpp"
#include "PartApproxCenterline.hpp"
#include "PartApproxRules.hpp"
#include <MathOperations.hpp>
#include <iostream>

void DynSegment::operator()()
{
#define APPROX
#ifdef APPROX
  //FunctionPartApproxCenterline part;
  FunctionPartApproxRules part;
#else
  FunctionPartSemiSymbolic part;
#endif
  
 //Parameters
  part.G=G;
  part.D=D;
  part.areaDensity=areaDensity;
  part.soffset=0.;
  part.width=width;
  part.anat=anat;
  part.bnat=bnat;
  part.nu=nu;

  part.a=a;
  part.adot=adot;
  part.b=b;
  part.bdot=bdot;
  part.m=m;
  part.mdot=mdot;
  part.n=n;
  part.ndot=ndot;
  //part.length=length;//later in loop

 //In functions
  
#ifndef APPROX
  part.rx=r[0]; part.ry=r[1]; part.rz=r[2];
  part.Rxx=R(0,0); part.Rxy=R(0,1); part.Rxz=R(0,2);  
  part.Ryx=R(1,0); part.Ryy=R(1,1); part.Ryz=R(1,2);
  part.Rzx=R(2,0); part.Rzy=R(2,1); part.Rzz=R(2,2);
  part.d1rxda=0.;part.d1rxdb=0.;part.d1rxdm=0.;part.d1rxdn=0.;part.d1rxdt=0.;
  part.d1ryda=0.;part.d1rydb=0.;part.d1rydm=0.;part.d1rydn=0.;part.d1rydt=0.;
  part.d1rzda=0.;part.d1rzdb=0.;part.d1rzdm=0.;part.d1rzdn=0.;part.d1rzdt=0.;
  part.d1Rxxda=0.;part.d1Rxxdb=0.;part.d1Rxxdm=0.;part.d1Rxxdn=0.;part.d1Rxxdt=0.;
  part.d1Rxyda=0.;part.d1Rxydb=0.;part.d1Rxydm=0.;part.d1Rxydn=0.;part.d1Rxydt=0.;
  part.d1Rxzda=0.;part.d1Rxzdb=0.;part.d1Rxzdm=0.;part.d1Rxzdn=0.;part.d1Rxzdt=0.;
  part.d1Ryxda=0.;part.d1Ryxdb=0.;part.d1Ryxdm=0.;part.d1Ryxdn=0.;part.d1Ryxdt=0.;
  part.d1Ryyda=0.;part.d1Ryydb=0.;part.d1Ryydm=0.;part.d1Ryydn=0.;part.d1Ryydt=0.;
  part.d1Ryzda=0.;part.d1Ryzdb=0.;part.d1Ryzdm=0.;part.d1Ryzdn=0.;part.d1Ryzdt=0.;
  part.d1Rzxda=0.;part.d1Rzxdb=0.;part.d1Rzxdm=0.;part.d1Rzxdn=0.;part.d1Rzxdt=0.;
  part.d1Rzyda=0.;part.d1Rzydb=0.;part.d1Rzydm=0.;part.d1Rzydn=0.;part.d1Rzydt=0.;
  part.d1Rzzda=0.;part.d1Rzzdb=0.;part.d1Rzzdm=0.;part.d1Rzzdn=0.;part.d1Rzzdt=0.;
  part.d2rxdtda=0.;part.d2rxdtdadot=0.;part.d2rxdtdb=0.;part.d2rxdtdbdot=0.;part.d2rxdtdm=0.;part.d2rxdtdmdot=0.;part.d2rxdtdn=0.;part.d2rxdtdndot=0.;
  part.d2rydtda=0.;part.d2rydtdadot=0.;part.d2rydtdb=0.;part.d2rydtdbdot=0.;part.d2rydtdm=0.;part.d2rydtdmdot=0.;part.d2rydtdn=0.;part.d2rydtdndot=0.;
  part.d2rzdtda=0.;part.d2rzdtdadot=0.;part.d2rzdtdb=0.;part.d2rzdtdbdot=0.;part.d2rzdtdm=0.;part.d2rzdtdmdot=0.;part.d2rzdtdn=0.;part.d2rzdtdndot=0.;
  part.d2Rxxdtda=0.;part.d2Rxxdtdadot=0.;part.d2Rxxdtdb=0.;part.d2Rxxdtdbdot=0.;part.d2Rxxdtdm=0.;part.d2Rxxdtdmdot=0.;part.d2Rxxdtdn=0.;part.d2Rxxdtdndot=0.;
  part.d2Rxydtda=0.;part.d2Rxydtdadot=0.;part.d2Rxydtdb=0.;part.d2Rxydtdbdot=0.;part.d2Rxydtdm=0.;part.d2Rxydtdmdot=0.;part.d2Rxydtdn=0.;part.d2Rxydtdndot=0.;
  part.d2Rxzdtda=0.;part.d2Rxzdtdadot=0.;part.d2Rxzdtdb=0.;part.d2Rxzdtdbdot=0.;part.d2Rxzdtdm=0.;part.d2Rxzdtdmdot=0.;part.d2Rxzdtdn=0.;part.d2Rxzdtdndot=0.;
  part.d2Ryxdtda=0.;part.d2Ryxdtdadot=0.;part.d2Ryxdtdb=0.;part.d2Ryxdtdbdot=0.;part.d2Ryxdtdm=0.;part.d2Ryxdtdmdot=0.;part.d2Ryxdtdn=0.;part.d2Ryxdtdndot=0.;
  part.d2Ryydtda=0.;part.d2Ryydtdadot=0.;part.d2Ryydtdb=0.;part.d2Ryydtdbdot=0.;part.d2Ryydtdm=0.;part.d2Ryydtdmdot=0.;part.d2Ryydtdn=0.;part.d2Ryydtdndot=0.;
  part.d2Ryzdtda=0.;part.d2Ryzdtdadot=0.;part.d2Ryzdtdb=0.;part.d2Ryzdtdbdot=0.;part.d2Ryzdtdm=0.;part.d2Ryzdtdmdot=0.;part.d2Ryzdtdn=0.;part.d2Ryzdtdndot=0.;
  part.d2Rzxdtda=0.;part.d2Rzxdtdadot=0.;part.d2Rzxdtdb=0.;part.d2Rzxdtdbdot=0.;part.d2Rzxdtdm=0.;part.d2Rzxdtdmdot=0.;part.d2Rzxdtdn=0.;part.d2Rzxdtdndot=0.;
  part.d2Rzydtda=0.;part.d2Rzydtdadot=0.;part.d2Rzydtdb=0.;part.d2Rzydtdbdot=0.;part.d2Rzydtdm=0.;part.d2Rzydtdmdot=0.;part.d2Rzydtdn=0.;part.d2Rzydtdndot=0.;
  part.d2Rzzdtda=0.;part.d2Rzzdtdadot=0.;part.d2Rzzdtdb=0.;part.d2Rzzdtdbdot=0.;part.d2Rzzdtdm=0.;part.d2Rzzdtdmdot=0.;part.d2Rzzdtdn=0.;part.d2Rzzdtdndot=0.;
  
  part.d3rxdtdadotda=0.;part.d3rxdtdadotdadot=0.;part.d3rxdtdadotdb=0.;part.d3rxdtdadotdbdot=0.; part.d3rxdtdadotdm=0.;part.d3rxdtdadotdmdot=0.;part.d3rxdtdadotdn=0.;part.d3rxdtdadotdndot=0.;
  part.d3rxdtdbdotda=0.;part.d3rxdtdbdotdadot=0.;part.d3rxdtdbdotdb=0.;part.d3rxdtdbdotdbdot=0.; part.d3rxdtdbdotdm=0.;part.d3rxdtdbdotdmdot=0.;part.d3rxdtdbdotdn=0.;part.d3rxdtdbdotdndot=0.;
  part.d3rxdtdmdotda=0.;part.d3rxdtdmdotdadot=0.;part.d3rxdtdmdotdb=0.;part.d3rxdtdmdotdbdot=0.; part.d3rxdtdmdotdm=0.;part.d3rxdtdmdotdmdot=0.;part.d3rxdtdmdotdn=0.;part.d3rxdtdmdotdndot=0.;
  part.d3rxdtdndotda=0.;part.d3rxdtdndotdadot=0.;part.d3rxdtdndotdb=0.;part.d3rxdtdndotdbdot=0.; part.d3rxdtdndotdm=0.;part.d3rxdtdndotdmdot=0.;part.d3rxdtdndotdn=0.;part.d3rxdtdndotdndot=0.;
  
  part.d3rydtdadotda=0.;part.d3rydtdadotdadot=0.;part.d3rydtdadotdb=0.;part.d3rydtdadotdbdot=0.; part.d3rydtdadotdm=0.;part.d3rydtdadotdmdot=0.;part.d3rydtdadotdn=0.;part.d3rydtdadotdndot=0.;
  part.d3rydtdbdotda=0.;part.d3rydtdbdotdadot=0.;part.d3rydtdbdotdb=0.;part.d3rydtdbdotdbdot=0.; part.d3rydtdbdotdm=0.;part.d3rydtdbdotdmdot=0.;part.d3rydtdbdotdn=0.;part.d3rydtdbdotdndot=0.;
  part.d3rydtdmdotda=0.;part.d3rydtdmdotdadot=0.;part.d3rydtdmdotdb=0.;part.d3rydtdmdotdbdot=0.; part.d3rydtdmdotdm=0.;part.d3rydtdmdotdmdot=0.;part.d3rydtdmdotdn=0.;part.d3rydtdmdotdndot=0.;
  part.d3rydtdndotda=0.;part.d3rydtdndotdadot=0.;part.d3rydtdndotdb=0.;part.d3rydtdndotdbdot=0.; part.d3rydtdndotdm=0.;part.d3rydtdndotdmdot=0.;part.d3rydtdndotdn=0.;part.d3rydtdndotdndot=0.;
  
  part.d3rzdtdadotda=0.;part.d3rzdtdadotdadot=0.;part.d3rzdtdadotdb=0.;part.d3rzdtdadotdbdot=0.; part.d3rzdtdadotdm=0.;part.d3rzdtdadotdmdot=0.;part.d3rzdtdadotdn=0.;part.d3rzdtdadotdndot=0.;
  part.d3rzdtdbdotda=0.;part.d3rzdtdbdotdadot=0.;part.d3rzdtdbdotdb=0.;part.d3rzdtdbdotdbdot=0.; part.d3rzdtdbdotdm=0.;part.d3rzdtdbdotdmdot=0.;part.d3rzdtdbdotdn=0.;part.d3rzdtdbdotdndot=0.;
  part.d3rzdtdmdotda=0.;part.d3rzdtdmdotdadot=0.;part.d3rzdtdmdotdb=0.;part.d3rzdtdmdotdbdot=0.; part.d3rzdtdmdotdm=0.;part.d3rzdtdmdotdmdot=0.;part.d3rzdtdmdotdn=0.;part.d3rzdtdmdotdndot=0.;
  part.d3rzdtdndotda=0.;part.d3rzdtdndotdadot=0.;part.d3rzdtdndotdb=0.;part.d3rzdtdndotdbdot=0.; part.d3rzdtdndotdm=0.;part.d3rzdtdndotdmdot=0.;part.d3rzdtdndotdn=0.;part.d3rzdtdndotdndot=0.;
  
  part.d3Rxxdtdadotda=0.;part.d3Rxxdtdadotdadot=0.;part.d3Rxxdtdadotdb=0.;part.d3Rxxdtdadotdbdot=0.; part.d3Rxxdtdadotdm=0.;part.d3Rxxdtdadotdmdot=0.;part.d3Rxxdtdadotdn=0.;part.d3Rxxdtdadotdndot=0.;
  part.d3Rxxdtdbdotda=0.;part.d3Rxxdtdbdotdadot=0.;part.d3Rxxdtdbdotdb=0.;part.d3Rxxdtdbdotdbdot=0.; part.d3Rxxdtdbdotdm=0.;part.d3Rxxdtdbdotdmdot=0.;part.d3Rxxdtdbdotdn=0.;part.d3Rxxdtdbdotdndot=0.;
  part.d3Rxxdtdmdotda=0.;part.d3Rxxdtdmdotdadot=0.;part.d3Rxxdtdmdotdb=0.;part.d3Rxxdtdmdotdbdot=0.; part.d3Rxxdtdmdotdm=0.;part.d3Rxxdtdmdotdmdot=0.;part.d3Rxxdtdmdotdn=0.;part.d3Rxxdtdmdotdndot=0.;
  part.d3Rxxdtdndotda=0.;part.d3Rxxdtdndotdadot=0.;part.d3Rxxdtdndotdb=0.;part.d3Rxxdtdndotdbdot=0.; part.d3Rxxdtdndotdm=0.;part.d3Rxxdtdndotdmdot=0.;part.d3Rxxdtdndotdn=0.;part.d3Rxxdtdndotdndot=0.;
  
  part.d3Rxydtdadotda=0.;part.d3Rxydtdadotdadot=0.;part.d3Rxydtdadotdb=0.;part.d3Rxydtdadotdbdot=0.; part.d3Rxydtdadotdm=0.;part.d3Rxydtdadotdmdot=0.;part.d3Rxydtdadotdn=0.;part.d3Rxydtdadotdndot=0.;
  part.d3Rxydtdbdotda=0.;part.d3Rxydtdbdotdadot=0.;part.d3Rxydtdbdotdb=0.;part.d3Rxydtdbdotdbdot=0.; part.d3Rxydtdbdotdm=0.;part.d3Rxydtdbdotdmdot=0.;part.d3Rxydtdbdotdn=0.;part.d3Rxydtdbdotdndot=0.; 
  part.d3Rxydtdmdotda=0.;part.d3Rxydtdmdotdadot=0.;part.d3Rxydtdmdotdb=0.;part.d3Rxydtdmdotdbdot=0.; part.d3Rxydtdmdotdm=0.;part.d3Rxydtdmdotdmdot=0.;part.d3Rxydtdmdotdn=0.;part.d3Rxydtdmdotdndot=0.;
  part.d3Rxydtdndotda=0.;part.d3Rxydtdndotdadot=0.;part.d3Rxydtdndotdb=0.;part.d3Rxydtdndotdbdot=0.; part.d3Rxydtdndotdm=0.;part.d3Rxydtdndotdmdot=0.;part.d3Rxydtdndotdn=0.;part.d3Rxydtdndotdndot=0.;
  
  part.d3Rxzdtdadotda=0.;part.d3Rxzdtdadotdadot=0.;part.d3Rxzdtdadotdb=0.;part.d3Rxzdtdadotdbdot=0.; part.d3Rxzdtdadotdm=0.;part.d3Rxzdtdadotdmdot=0.;part.d3Rxzdtdadotdn=0.;part.d3Rxzdtdadotdndot=0.;
  part.d3Rxzdtdbdotda=0.;part.d3Rxzdtdbdotdadot=0.;part.d3Rxzdtdbdotdb=0.;part.d3Rxzdtdbdotdbdot=0.; part.d3Rxzdtdbdotdm=0.;part.d3Rxzdtdbdotdmdot=0.;part.d3Rxzdtdbdotdn=0.;part.d3Rxzdtdbdotdndot=0.;
  part.d3Rxzdtdmdotda=0.;part.d3Rxzdtdmdotdadot=0.;part.d3Rxzdtdmdotdb=0.;part.d3Rxzdtdmdotdbdot=0.; part.d3Rxzdtdmdotdm=0.;part.d3Rxzdtdmdotdmdot=0.;part.d3Rxzdtdmdotdn=0.;part.d3Rxzdtdmdotdndot=0.;
  part.d3Rxzdtdndotda=0.;part.d3Rxzdtdndotdadot=0.;part.d3Rxzdtdndotdb=0.;part.d3Rxzdtdndotdbdot=0.; part.d3Rxzdtdndotdm=0.;part.d3Rxzdtdndotdmdot=0.;part.d3Rxzdtdndotdn=0.;part.d3Rxzdtdndotdndot=0.;
  
  part.d3Ryxdtdadotda=0.;part.d3Ryxdtdadotdadot=0.;part.d3Ryxdtdadotdb=0.;part.d3Ryxdtdadotdbdot=0.; part.d3Ryxdtdadotdm=0.;part.d3Ryxdtdadotdmdot=0.;part.d3Ryxdtdadotdn=0.;part.d3Ryxdtdadotdndot=0.;
  part.d3Ryxdtdbdotda=0.;part.d3Ryxdtdbdotdadot=0.;part.d3Ryxdtdbdotdb=0.;part.d3Ryxdtdbdotdbdot=0.; part.d3Ryxdtdbdotdm=0.;part.d3Ryxdtdbdotdmdot=0.;part.d3Ryxdtdbdotdn=0.;part.d3Ryxdtdbdotdndot=0.;
  part.d3Ryxdtdmdotda=0.;part.d3Ryxdtdmdotdadot=0.;part.d3Ryxdtdmdotdb=0.;part.d3Ryxdtdmdotdbdot=0.; part.d3Ryxdtdmdotdm=0.;part.d3Ryxdtdmdotdmdot=0.;part.d3Ryxdtdmdotdn=0.;part.d3Ryxdtdmdotdndot=0.;
  part.d3Ryxdtdndotda=0.;part.d3Ryxdtdndotdadot=0.;part.d3Ryxdtdndotdb=0.;part.d3Ryxdtdndotdbdot=0.; part.d3Ryxdtdndotdm=0.;part.d3Ryxdtdndotdmdot=0.;part.d3Ryxdtdndotdn=0.;part.d3Ryxdtdndotdndot=0.;
  
  part.d3Ryydtdadotda=0.;part.d3Ryydtdadotdadot=0.;part.d3Ryydtdadotdb=0.;part.d3Ryydtdadotdbdot=0.; part.d3Ryydtdadotdm=0.;part.d3Ryydtdadotdmdot=0.;part.d3Ryydtdadotdn=0.;part.d3Ryydtdadotdndot=0.;
  part.d3Ryydtdbdotda=0.;part.d3Ryydtdbdotdadot=0.;part.d3Ryydtdbdotdb=0.;part.d3Ryydtdbdotdbdot=0.; part.d3Ryydtdbdotdm=0.;part.d3Ryydtdbdotdmdot=0.;part.d3Ryydtdbdotdn=0.;part.d3Ryydtdbdotdndot=0.;
  part.d3Ryydtdmdotda=0.;part.d3Ryydtdmdotdadot=0.;part.d3Ryydtdmdotdb=0.;part.d3Ryydtdmdotdbdot=0.; part.d3Ryydtdmdotdm=0.;part.d3Ryydtdmdotdmdot=0.;part.d3Ryydtdmdotdn=0.;part.d3Ryydtdmdotdndot=0.;
  part.d3Ryydtdndotda=0.;part.d3Ryydtdndotdadot=0.;part.d3Ryydtdndotdb=0.;part.d3Ryydtdndotdbdot=0.; part.d3Ryydtdndotdm=0.;part.d3Ryydtdndotdmdot=0.;part.d3Ryydtdndotdn=0.;part.d3Ryydtdndotdndot=0.;
  
  part.d3Ryzdtdadotda=0.;part.d3Ryzdtdadotdadot=0.;part.d3Ryzdtdadotdb=0.;part.d3Ryzdtdadotdbdot=0.; part.d3Ryzdtdadotdm=0.;part.d3Ryzdtdadotdmdot=0.;part.d3Ryzdtdadotdn=0.;part.d3Ryzdtdadotdndot=0.;
  part.d3Ryzdtdbdotda=0.;part.d3Ryzdtdbdotdadot=0.;part.d3Ryzdtdbdotdb=0.;part.d3Ryzdtdbdotdbdot=0.; part.d3Ryzdtdbdotdm=0.;part.d3Ryzdtdbdotdmdot=0.;part.d3Ryzdtdbdotdn=0.;part.d3Ryzdtdbdotdndot=0.;
  part.d3Ryzdtdmdotda=0.;part.d3Ryzdtdmdotdadot=0.;part.d3Ryzdtdmdotdb=0.;part.d3Ryzdtdmdotdbdot=0.; part.d3Ryzdtdmdotdm=0.;part.d3Ryzdtdmdotdmdot=0.;part.d3Ryzdtdmdotdn=0.;part.d3Ryzdtdmdotdndot=0.;
  part.d3Ryzdtdndotda=0.;part.d3Ryzdtdndotdadot=0.;part.d3Ryzdtdndotdb=0.;part.d3Ryzdtdndotdbdot=0.; part.d3Ryzdtdndotdm=0.;part.d3Ryzdtdndotdmdot=0.;part.d3Ryzdtdndotdn=0.;part.d3Ryzdtdndotdndot=0.;
  
  part.d3Rzxdtdadotda=0.;part.d3Rzxdtdadotdadot=0.;part.d3Rzxdtdadotdb=0.;part.d3Rzxdtdadotdbdot=0.; part.d3Rzxdtdadotdm=0.;part.d3Rzxdtdadotdmdot=0.;part.d3Rzxdtdadotdn=0.;part.d3Rzxdtdadotdndot=0.;
  part.d3Rzxdtdbdotda=0.;part.d3Rzxdtdbdotdadot=0.;part.d3Rzxdtdbdotdb=0.;part.d3Rzxdtdbdotdbdot=0.; part.d3Rzxdtdbdotdm=0.;part.d3Rzxdtdbdotdmdot=0.;part.d3Rzxdtdbdotdn=0.;part.d3Rzxdtdbdotdndot=0.;
  part.d3Rzxdtdmdotda=0.;part.d3Rzxdtdmdotdadot=0.;part.d3Rzxdtdmdotdb=0.;part.d3Rzxdtdmdotdbdot=0.; part.d3Rzxdtdmdotdm=0.;part.d3Rzxdtdmdotdmdot=0.;part.d3Rzxdtdmdotdn=0.;part.d3Rzxdtdmdotdndot=0.;
  part.d3Rzxdtdndotda=0.;part.d3Rzxdtdndotdadot=0.;part.d3Rzxdtdndotdb=0.;part.d3Rzxdtdndotdbdot=0.; part.d3Rzxdtdndotdm=0.;part.d3Rzxdtdndotdmdot=0.;part.d3Rzxdtdndotdn=0.;part.d3Rzxdtdndotdndot=0.;
  
  part.d3Rzydtdadotda=0.;part.d3Rzydtdadotdadot=0.;part.d3Rzydtdadotdb=0.;part.d3Rzydtdadotdbdot=0.; part.d3Rzydtdadotdm=0.;part.d3Rzydtdadotdmdot=0.;part.d3Rzydtdadotdn=0.;part.d3Rzydtdadotdndot=0.;
  part.d3Rzydtdbdotda=0.;part.d3Rzydtdbdotdadot=0.;part.d3Rzydtdbdotdb=0.;part.d3Rzydtdbdotdbdot=0.; part.d3Rzydtdbdotdm=0.;part.d3Rzydtdbdotdmdot=0.;part.d3Rzydtdbdotdn=0.;part.d3Rzydtdbdotdndot=0.;
  part.d3Rzydtdmdotda=0.;part.d3Rzydtdmdotdadot=0.;part.d3Rzydtdmdotdb=0.;part.d3Rzydtdmdotdbdot=0.; part.d3Rzydtdmdotdm=0.;part.d3Rzydtdmdotdmdot=0.;part.d3Rzydtdmdotdn=0.;part.d3Rzydtdmdotdndot=0.;
  part.d3Rzydtdndotda=0.;part.d3Rzydtdndotdadot=0.;part.d3Rzydtdndotdb=0.;part.d3Rzydtdndotdbdot=0.; part.d3Rzydtdndotdm=0.;part.d3Rzydtdndotdmdot=0.;part.d3Rzydtdndotdn=0.;part.d3Rzydtdndotdndot=0.;
  
  part.d3Rzzdtdadotda=0.;part.d3Rzzdtdadotdadot=0.;part.d3Rzzdtdadotdb=0.;part.d3Rzzdtdadotdbdot=0.; part.d3Rzzdtdadotdm=0.;part.d3Rzzdtdadotdmdot=0.;part.d3Rzzdtdadotdn=0.;part.d3Rzzdtdadotdndot=0.;
  part.d3Rzzdtdbdotda=0.;part.d3Rzzdtdbdotdadot=0.;part.d3Rzzdtdbdotdb=0.;part.d3Rzzdtdbdotdbdot=0.; part.d3Rzzdtdbdotdm=0.;part.d3Rzzdtdbdotdmdot=0.;part.d3Rzzdtdbdotdn=0.;part.d3Rzzdtdbdotdndot=0.;
  part.d3Rzzdtdmdotda=0.;part.d3Rzzdtdmdotdadot=0.;part.d3Rzzdtdmdotdb=0.;part.d3Rzzdtdmdotdbdot=0.; part.d3Rzzdtdmdotdm=0.;part.d3Rzzdtdmdotdmdot=0.;part.d3Rzzdtdmdotdn=0.;part.d3Rzzdtdmdotdndot=0.;
  part.d3Rzzdtdndotda=0.;part.d3Rzzdtdndotdadot=0.;part.d3Rzzdtdndotdb=0.;part.d3Rzzdtdndotdbdot=0.; part.d3Rzzdtdndotdm=0.;part.d3Rzzdtdndotdmdot=0.;part.d3Rzzdtdndotdn=0.;part.d3Rzzdtdndotdndot=0.;
#else
  part.r=r;
  part.R=R;
  part.d1rda.setZero(); part.d1rdb.setZero(); part.d1rdn.setZero(); part.d1rdm.setZero();
  part.d1Rda.setZero(); part.d1Rdb.setZero(); part.d1Rdn.setZero(); part.d1Rdm.setZero();
  part.d2rdtda.setZero(); part.d2rdtdb.setZero(); part.d2rdtdn.setZero(); part.d2rdtdm.setZero();
  part.d2rdtdadot.setZero(); part.d2rdtdbdot.setZero(); part.d2rdtdndot.setZero(); part.d2rdtdmdot.setZero();
  part.d2Rdtda.setZero(); part.d2Rdtdb.setZero(); part.d2Rdtdn.setZero(); part.d2Rdtdm.setZero();
  part.d2Rdtdadot.setZero(); part.d2Rdtdbdot.setZero(); part.d2Rdtdndot.setZero(); part.d2Rdtdmdot.setZero();
  part.d3rdtdadotda.setZero();part.d3rdtdadotdadot.setZero();part.d3rdtdadotdb.setZero();part.d3rdtdadotdbdot.setZero();
  part.d3rdtdadotdm.setZero();part.d3rdtdadotdmdot.setZero();part.d3rdtdadotdn.setZero();part.d3rdtdadotdndot.setZero();
  part.d3rdtdbdotda.setZero();part.d3rdtdbdotdadot.setZero();part.d3rdtdbdotdb.setZero();part.d3rdtdbdotdbdot.setZero(); part.d3rdtdbdotdm.setZero();part.d3rdtdbdotdmdot.setZero();part.d3rdtdbdotdn.setZero();part.d3rdtdbdotdndot.setZero();
  part.d3rdtdmdotda.setZero();part.d3rdtdmdotdadot.setZero();part.d3rdtdmdotdb.setZero();part.d3rdtdmdotdbdot.setZero(); part.d3rdtdmdotdm.setZero();part.d3rdtdmdotdmdot.setZero();part.d3rdtdmdotdn.setZero();part.d3rdtdmdotdndot.setZero();
  part.d3rdtdndotda.setZero();part.d3rdtdndotdadot.setZero();part.d3rdtdndotdb.setZero();part.d3rdtdndotdbdot.setZero(); part.d3rdtdndotdm.setZero();part.d3rdtdndotdmdot.setZero();part.d3rdtdndotdn.setZero();part.d3rdtdndotdndot.setZero();
  
  part.d3Rdtdadotda.setZero();part.d3Rdtdadotdadot.setZero();part.d3Rdtdadotdb.setZero();part.d3Rdtdadotdbdot.setZero();
  part.d3Rdtdadotdm.setZero();part.d3Rdtdadotdmdot.setZero();part.d3Rdtdadotdn.setZero();part.d3Rdtdadotdndot.setZero();
  part.d3Rdtdbdotda.setZero();part.d3Rdtdbdotdadot.setZero();part.d3Rdtdbdotdb.setZero();part.d3Rdtdbdotdbdot.setZero(); part.d3Rdtdbdotdm.setZero();part.d3Rdtdbdotdmdot.setZero();part.d3Rdtdbdotdn.setZero();part.d3Rdtdbdotdndot.setZero();
  part.d3Rdtdmdotda.setZero();part.d3Rdtdmdotdadot.setZero();part.d3Rdtdmdotdb.setZero();part.d3Rdtdmdotdbdot.setZero(); part.d3Rdtdmdotdm.setZero();part.d3Rdtdmdotdmdot.setZero();part.d3Rdtdmdotdn.setZero();part.d3Rdtdmdotdndot.setZero();
  part.d3Rdtdndotda.setZero();part.d3Rdtdndotdadot.setZero();part.d3Rdtdndotdb.setZero();part.d3Rdtdndotdbdot.setZero(); part.d3Rdtdndotdm.setZero();part.d3Rdtdndotdmdot.setZero();part.d3Rdtdndotdn.setZero();part.d3Rdtdndotdndot.setZero();
#endif
  
  Ec=0.;
  Eg=0.;
  Ew=0.;
  L=0.;
  gradL.setZero();
  d2Ecdqdotdq.setZero();
  d2Ecdqdot2.setZero();
  int nbParts=0;
  double localsStart=0.;
  while(localsStart<length)
  {
    
    double localLength =  estimSMax(a,b+localsStart*a,n,m+localsStart*n,adot,bdot+localsStart*adot,ndot,mdot+localsStart*ndot);
    //double localLength =  estimSMax(a,b+localsStart*a,n,m+localsStart*n);
    nbParts++;
    //localLength /= 2.;
    if(localsStart+localLength>length)
      localLength=length-localsStart;
    part.soffset=localsStart;
    part.length=localLength;
    part();
    Ec+=part.Ec;
    Eg+=part.Eg;
    Ew+=part.Ew;
    L+=part.L;
    gradL+=part.gradL;
    d2Ecdqdot2+=part.d2Ecdqdot2;
    d2Ecdqdotdq+=part.d2Ecdqdotdq;
    
#ifndef APPROX
    part.rx=part.endPos[0];
    part.ry=part.endPos[1];
    part.rz=part.endPos[2];
    part.Rxx=part.endFrame(0,0); part.Rxy=part.endFrame(0,1); part.Rxz=part.endFrame(0,2);  
    part.Ryx=part.endFrame(1,0); part.Ryy=part.endFrame(1,1); part.Ryz=part.endFrame(1,2);
    part.Rzx=part.endFrame(2,0); part.Rzy=part.endFrame(2,1); part.Rzz=part.endFrame(2,2);
    part.d1rxda=part.dPosda[0]; part.d1rxdb=part.dPosdb[0]; part.d1rxdm=part.dPosdm[0]; part.d1rxdn=part.dPosdn[0]; part.d1rxdt=part.dPosdt[0]; 
    part.d1ryda=part.dPosda[1]; part.d1rydb=part.dPosdb[1]; part.d1rydm=part.dPosdm[1]; part.d1rydn=part.dPosdn[1]; part.d1rydt=part.dPosdt[1]; 
    part.d1rzda=part.dPosda[2]; part.d1rzdb=part.dPosdb[2]; part.d1rzdm=part.dPosdm[2]; part.d1rzdn=part.dPosdn[2]; part.d1rzdt=part.dPosdt[2]; 
    part.d1Rxxda = part.dFrameda(0,0); part.d1Rxxdb = part.dFramedb(0,0); part.d1Rxxdm = part.dFramedm(0,0); part.d1Rxxdn = part.dFramedn(0,0); part.d1Rxxdt = part.dFramedt(0,0); 
    part.d1Rxyda = part.dFrameda(0,1); part.d1Rxydb = part.dFramedb(0,1); part.d1Rxydm = part.dFramedm(0,1); part.d1Rxydn = part.dFramedn(0,1); part.d1Rxydt = part.dFramedt(0,1); 
    part.d1Rxzda = part.dFrameda(0,2); part.d1Rxzdb = part.dFramedb(0,2); part.d1Rxzdm = part.dFramedm(0,2); part.d1Rxzdn = part.dFramedn(0,2); part.d1Rxzdt = part.dFramedt(0,2); 
    part.d1Ryxda = part.dFrameda(1,0); part.d1Ryxdb = part.dFramedb(1,0); part.d1Ryxdm = part.dFramedm(1,0); part.d1Ryxdn = part.dFramedn(1,0); part.d1Ryxdt = part.dFramedt(1,0); 
    part.d1Ryyda = part.dFrameda(1,1); part.d1Ryydb = part.dFramedb(1,1); part.d1Ryydm = part.dFramedm(1,1); part.d1Ryydn = part.dFramedn(1,1); part.d1Ryydt = part.dFramedt(1,1); 
    part.d1Ryzda = part.dFrameda(1,2); part.d1Ryzdb = part.dFramedb(1,2); part.d1Ryzdm = part.dFramedm(1,2); part.d1Ryzdn = part.dFramedn(1,2); part.d1Ryzdt = part.dFramedt(1,2); 
    part.d1Rzxda = part.dFrameda(2,0); part.d1Rzxdb = part.dFramedb(2,0); part.d1Rzxdm = part.dFramedm(2,0); part.d1Rzxdn = part.dFramedn(2,0); part.d1Rzxdt = part.dFramedt(2,0); 
    part.d1Rzyda = part.dFrameda(2,1); part.d1Rzydb = part.dFramedb(2,1); part.d1Rzydm = part.dFramedm(2,1); part.d1Rzydn = part.dFramedn(2,1); part.d1Rzydt = part.dFramedt(2,1); 
    part.d1Rzzda = part.dFrameda(2,2); part.d1Rzzdb = part.dFramedb(2,2); part.d1Rzzdm = part.dFramedm(2,2); part.d1Rzzdn = part.dFramedn(2,2); part.d1Rzzdt = part.dFramedt(2,2); 
    part.d2rxdtda = part.d2Posdtda[0];  part.d2rxdtdadot = part.d2Posdtdadot[0];  part.d2rxdtdb = part.d2Posdtdb[0];  part.d2rxdtdbdot = part.d2Posdtdbdot[0];  
    part.d2rxdtdm = part.d2Posdtdm[0];  part.d2rxdtdmdot = part.d2Posdtdmdot[0];  part.d2rxdtdn = part.d2Posdtdn[0];  part.d2rxdtdndot = part.d2Posdtdndot[0];  
    part.d2rydtda = part.d2Posdtda[1];  part.d2rydtdadot = part.d2Posdtdadot[1];  part.d2rydtdb = part.d2Posdtdb[1];  part.d2rydtdbdot = part.d2Posdtdbdot[1];  
    part.d2rydtdm = part.d2Posdtdm[1];  part.d2rydtdmdot = part.d2Posdtdmdot[1];  part.d2rydtdn = part.d2Posdtdn[1];  part.d2rydtdndot = part.d2Posdtdndot[1];  
    part.d2rzdtda = part.d2Posdtda[2];  part.d2rzdtdadot = part.d2Posdtdadot[2];  part.d2rzdtdb = part.d2Posdtdb[2];  part.d2rzdtdbdot = part.d2Posdtdbdot[2];  
    part.d2rzdtdm = part.d2Posdtdm[2];  part.d2rzdtdmdot = part.d2Posdtdmdot[2];  part.d2rzdtdn = part.d2Posdtdn[2];  part.d2rzdtdndot = part.d2Posdtdndot[2]; 
    part.d2Rxxdtda = part.d2Framedtda(0,0); part.d2Rxxdtdadot = part.d2Framedtdadot(0,0); part.d2Rxxdtdb = part.d2Framedtdb(0,0); part.d2Rxxdtdbdot = part.d2Framedtdbdot(0,0); 
    part.d2Rxxdtdm = part.d2Framedtdm(0,0); part.d2Rxxdtdmdot = part.d2Framedtdmdot(0,0); part.d2Rxxdtdn = part.d2Framedtdn(0,0); part.d2Rxxdtdndot = part.d2Framedtdndot(0,0); 
    part.d2Rxydtda = part.d2Framedtda(0,1); part.d2Rxydtdadot = part.d2Framedtdadot(0,1); part.d2Rxydtdb = part.d2Framedtdb(0,1); part.d2Rxydtdbdot = part.d2Framedtdbdot(0,1); 
    part.d2Rxydtdm = part.d2Framedtdm(0,1); part.d2Rxydtdmdot = part.d2Framedtdmdot(0,1); part.d2Rxydtdn = part.d2Framedtdn(0,1); part.d2Rxydtdndot = part.d2Framedtdndot(0,1); 
    part.d2Rxzdtda = part.d2Framedtda(0,2); part.d2Rxzdtdadot = part.d2Framedtdadot(0,2); part.d2Rxzdtdb = part.d2Framedtdb(0,2); part.d2Rxzdtdbdot = part.d2Framedtdbdot(0,2); 
    part.d2Rxzdtdm = part.d2Framedtdm(0,2); part.d2Rxzdtdmdot = part.d2Framedtdmdot(0,2); part.d2Rxzdtdn = part.d2Framedtdn(0,2); part.d2Rxzdtdndot = part.d2Framedtdndot(0,2); 
    part.d2Ryxdtda = part.d2Framedtda(1,0); part.d2Ryxdtdadot = part.d2Framedtdadot(1,0); part.d2Ryxdtdb = part.d2Framedtdb(1,0); part.d2Ryxdtdbdot = part.d2Framedtdbdot(1,0); 
    part.d2Ryxdtdm = part.d2Framedtdm(1,0); part.d2Ryxdtdmdot = part.d2Framedtdmdot(1,0); part.d2Ryxdtdn = part.d2Framedtdn(1,0); part.d2Ryxdtdndot = part.d2Framedtdndot(1,0); 
    part.d2Ryydtda = part.d2Framedtda(1,1); part.d2Ryydtdadot = part.d2Framedtdadot(1,1); part.d2Ryydtdb = part.d2Framedtdb(1,1); part.d2Ryydtdbdot = part.d2Framedtdbdot(1,1); 
    part.d2Ryydtdm = part.d2Framedtdm(1,1); part.d2Ryydtdmdot = part.d2Framedtdmdot(1,1); part.d2Ryydtdn = part.d2Framedtdn(1,1); part.d2Ryydtdndot = part.d2Framedtdndot(1,1); 
    part.d2Ryzdtda = part.d2Framedtda(1,2); part.d2Ryzdtdadot = part.d2Framedtdadot(1,2); part.d2Ryzdtdb = part.d2Framedtdb(1,2); part.d2Ryzdtdbdot = part.d2Framedtdbdot(1,2); 
    part.d2Ryzdtdm = part.d2Framedtdm(1,2); part.d2Ryzdtdmdot = part.d2Framedtdmdot(1,2); part.d2Ryzdtdn = part.d2Framedtdn(1,2); part.d2Ryzdtdndot = part.d2Framedtdndot(1,2); 
    part.d2Rzxdtda = part.d2Framedtda(2,0); part.d2Rzxdtdadot = part.d2Framedtdadot(2,0); part.d2Rzxdtdb = part.d2Framedtdb(2,0); part.d2Rzxdtdbdot = part.d2Framedtdbdot(2,0); 
    part.d2Rzxdtdm = part.d2Framedtdm(2,0); part.d2Rzxdtdmdot = part.d2Framedtdmdot(2,0); part.d2Rzxdtdn = part.d2Framedtdn(2,0); part.d2Rzxdtdndot = part.d2Framedtdndot(2,0); 
    part.d2Rzydtda = part.d2Framedtda(2,1); part.d2Rzydtdadot = part.d2Framedtdadot(2,1); part.d2Rzydtdb = part.d2Framedtdb(2,1); part.d2Rzydtdbdot = part.d2Framedtdbdot(2,1); 
    part.d2Rzydtdm = part.d2Framedtdm(2,1); part.d2Rzydtdmdot = part.d2Framedtdmdot(2,1); part.d2Rzydtdn = part.d2Framedtdn(2,1); part.d2Rzydtdndot = part.d2Framedtdndot(2,1); 
    part.d2Rzzdtda = part.d2Framedtda(2,2); part.d2Rzzdtdadot = part.d2Framedtdadot(2,2); part.d2Rzzdtdb = part.d2Framedtdb(2,2); part.d2Rzzdtdbdot = part.d2Framedtdbdot(2,2); 
    part.d2Rzzdtdm = part.d2Framedtdm(2,2); part.d2Rzzdtdmdot = part.d2Framedtdmdot(2,2); part.d2Rzzdtdn = part.d2Framedtdn(2,2); part.d2Rzzdtdndot = part.d2Framedtdndot(2,2); 
     
    part.d3rxdtdadotda = part.d3Posdtdadotda[0];  part.d3rxdtdadotdadot = part.d3Posdtdadotdadot[0];  part.d3rxdtdadotdb = part.d3Posdtdadotdb[0];  part.d3rxdtdadotdbdot = part.d3Posdtdadotdbdot[0];   
    part.d3rxdtdadotdm = part.d3Posdtdadotdm[0];  part.d3rxdtdadotdmdot = part.d3Posdtdadotdmdot[0];  part.d3rxdtdadotdn = part.d3Posdtdadotdn[0];  part.d3rxdtdadotdndot = part.d3Posdtdadotdndot[0];  
    part.d3rxdtdbdotda = part.d3Posdtdbdotda[0];  part.d3rxdtdbdotdadot = part.d3Posdtdbdotdadot[0];  part.d3rxdtdbdotdb = part.d3Posdtdbdotdb[0];  part.d3rxdtdbdotdbdot = part.d3Posdtdbdotdbdot[0];   
    part.d3rxdtdbdotdm = part.d3Posdtdbdotdm[0];  part.d3rxdtdbdotdmdot = part.d3Posdtdbdotdmdot[0];  part.d3rxdtdbdotdn = part.d3Posdtdbdotdn[0];  part.d3rxdtdbdotdndot = part.d3Posdtdbdotdndot[0];  
    part.d3rxdtdmdotda = part.d3Posdtdmdotda[0];  part.d3rxdtdmdotdadot = part.d3Posdtdmdotdadot[0];  part.d3rxdtdmdotdb = part.d3Posdtdmdotdb[0];  part.d3rxdtdmdotdbdot = part.d3Posdtdmdotdbdot[0];   
    part.d3rxdtdmdotdm = part.d3Posdtdmdotdm[0];  part.d3rxdtdmdotdmdot = part.d3Posdtdmdotdmdot[0];  part.d3rxdtdmdotdn = part.d3Posdtdmdotdn[0];  part.d3rxdtdmdotdndot = part.d3Posdtdmdotdndot[0];  
    part.d3rxdtdndotda = part.d3Posdtdndotda[0];  part.d3rxdtdndotdadot = part.d3Posdtdndotdadot[0];  part.d3rxdtdndotdb = part.d3Posdtdndotdb[0];  part.d3rxdtdndotdbdot = part.d3Posdtdndotdbdot[0];   
    part.d3rxdtdndotdm = part.d3Posdtdndotdm[0];  part.d3rxdtdndotdmdot = part.d3Posdtdndotdmdot[0];  part.d3rxdtdndotdn = part.d3Posdtdndotdn[0];  part.d3rxdtdndotdndot = part.d3Posdtdndotdndot[0];  
    
    part.d3rydtdadotda = part.d3Posdtdadotda[1];  part.d3rydtdadotdadot = part.d3Posdtdadotdadot[1];  part.d3rydtdadotdb = part.d3Posdtdadotdb[1];  part.d3rydtdadotdbdot = part.d3Posdtdadotdbdot[1];   
    part.d3rydtdadotdm = part.d3Posdtdadotdm[1];  part.d3rydtdadotdmdot = part.d3Posdtdadotdmdot[1];  part.d3rydtdadotdn = part.d3Posdtdadotdn[1];  part.d3rydtdadotdndot = part.d3Posdtdadotdndot[1];  
    part.d3rydtdbdotda = part.d3Posdtdbdotda[1];  part.d3rydtdbdotdadot = part.d3Posdtdbdotdadot[1];  part.d3rydtdbdotdb = part.d3Posdtdbdotdb[1];  part.d3rydtdbdotdbdot = part.d3Posdtdbdotdbdot[1];   
    part.d3rydtdbdotdm = part.d3Posdtdbdotdm[1];  part.d3rydtdbdotdmdot = part.d3Posdtdbdotdmdot[1];  part.d3rydtdbdotdn = part.d3Posdtdbdotdn[1];  part.d3rydtdbdotdndot = part.d3Posdtdbdotdndot[1];  
    part.d3rydtdmdotda = part.d3Posdtdmdotda[1];  part.d3rydtdmdotdadot = part.d3Posdtdmdotdadot[1];  part.d3rydtdmdotdb = part.d3Posdtdmdotdb[1];  part.d3rydtdmdotdbdot = part.d3Posdtdmdotdbdot[1];   
    part.d3rydtdmdotdm = part.d3Posdtdmdotdm[1];  part.d3rydtdmdotdmdot = part.d3Posdtdmdotdmdot[1];  part.d3rydtdmdotdn = part.d3Posdtdmdotdn[1];  part.d3rydtdmdotdndot = part.d3Posdtdmdotdndot[1];  
    part.d3rydtdndotda = part.d3Posdtdndotda[1];  part.d3rydtdndotdadot = part.d3Posdtdndotdadot[1];  part.d3rydtdndotdb = part.d3Posdtdndotdb[1];  part.d3rydtdndotdbdot = part.d3Posdtdndotdbdot[1];   
    part.d3rydtdndotdm = part.d3Posdtdndotdm[1];  part.d3rydtdndotdmdot = part.d3Posdtdndotdmdot[1];  part.d3rydtdndotdn = part.d3Posdtdndotdn[1];  part.d3rydtdndotdndot = part.d3Posdtdndotdndot[1];  
    
    part.d3rzdtdadotda = part.d3Posdtdadotda[2];  part.d3rzdtdadotdadot = part.d3Posdtdadotdadot[2];  part.d3rzdtdadotdb = part.d3Posdtdadotdb[2];  part.d3rzdtdadotdbdot = part.d3Posdtdadotdbdot[2];   
    part.d3rzdtdadotdm = part.d3Posdtdadotdm[2];  part.d3rzdtdadotdmdot = part.d3Posdtdadotdmdot[2];  part.d3rzdtdadotdn = part.d3Posdtdadotdn[2];  part.d3rzdtdadotdndot = part.d3Posdtdadotdndot[2];  
    part.d3rzdtdbdotda = part.d3Posdtdbdotda[2];  part.d3rzdtdbdotdadot = part.d3Posdtdbdotdadot[2];  part.d3rzdtdbdotdb = part.d3Posdtdbdotdb[2];  part.d3rzdtdbdotdbdot = part.d3Posdtdbdotdbdot[2];   
    part.d3rzdtdbdotdm = part.d3Posdtdbdotdm[2];  part.d3rzdtdbdotdmdot = part.d3Posdtdbdotdmdot[2];  part.d3rzdtdbdotdn = part.d3Posdtdbdotdn[2];  part.d3rzdtdbdotdndot = part.d3Posdtdbdotdndot[2];  
    part.d3rzdtdmdotda = part.d3Posdtdmdotda[2];  part.d3rzdtdmdotdadot = part.d3Posdtdmdotdadot[2];  part.d3rzdtdmdotdb = part.d3Posdtdmdotdb[2];  part.d3rzdtdmdotdbdot = part.d3Posdtdmdotdbdot[2];   
    part.d3rzdtdmdotdm = part.d3Posdtdmdotdm[2];  part.d3rzdtdmdotdmdot = part.d3Posdtdmdotdmdot[2];  part.d3rzdtdmdotdn = part.d3Posdtdmdotdn[2];  part.d3rzdtdmdotdndot = part.d3Posdtdmdotdndot[2];  
    part.d3rzdtdndotda = part.d3Posdtdndotda[2];  part.d3rzdtdndotdadot = part.d3Posdtdndotdadot[2];  part.d3rzdtdndotdb = part.d3Posdtdndotdb[2];  part.d3rzdtdndotdbdot = part.d3Posdtdndotdbdot[2];   
    part.d3rzdtdndotdm = part.d3Posdtdndotdm[2];  part.d3rzdtdndotdmdot = part.d3Posdtdndotdmdot[2];  part.d3rzdtdndotdn = part.d3Posdtdndotdn[2];  part.d3rzdtdndotdndot = part.d3Posdtdndotdndot[2];
    
    part.d3Rxxdtdadotda = part.d3Framedtdadotda(0,0); part.d3Rxxdtdadotdadot = part.d3Framedtdadotdadot(0,0); part.d3Rxxdtdadotdb = part.d3Framedtdadotdb(0,0); part.d3Rxxdtdadotdbdot = part.d3Framedtdadotdbdot(0,0); 
    part.d3Rxxdtdadotdm = part.d3Framedtdadotdm(0,0); part.d3Rxxdtdadotdmdot = part.d3Framedtdadotdmdot(0,0); part.d3Rxxdtdadotdn = part.d3Framedtdadotdn(0,0); part.d3Rxxdtdadotdndot = part.d3Framedtdadotdndot(0,0); 
    part.d3Rxxdtdbdotda = part.d3Framedtdbdotda(0,0); part.d3Rxxdtdbdotdadot = part.d3Framedtdbdotdadot(0,0); part.d3Rxxdtdbdotdb = part.d3Framedtdbdotdb(0,0); part.d3Rxxdtdbdotdbdot = part.d3Framedtdbdotdbdot(0,0); 
    part.d3Rxxdtdbdotdm = part.d3Framedtdbdotdm(0,0); part.d3Rxxdtdbdotdmdot = part.d3Framedtdbdotdmdot(0,0); part.d3Rxxdtdbdotdn = part.d3Framedtdbdotdn(0,0); part.d3Rxxdtdbdotdndot = part.d3Framedtdbdotdndot(0,0); 
    part.d3Rxxdtdmdotda = part.d3Framedtdmdotda(0,0); part.d3Rxxdtdmdotdadot = part.d3Framedtdmdotdadot(0,0); part.d3Rxxdtdmdotdb = part.d3Framedtdmdotdb(0,0); part.d3Rxxdtdmdotdbdot = part.d3Framedtdmdotdbdot(0,0); 
    part.d3Rxxdtdmdotdm = part.d3Framedtdmdotdm(0,0); part.d3Rxxdtdmdotdmdot = part.d3Framedtdmdotdmdot(0,0); part.d3Rxxdtdmdotdn = part.d3Framedtdmdotdn(0,0); part.d3Rxxdtdmdotdndot = part.d3Framedtdmdotdndot(0,0); 
    part.d3Rxxdtdndotda = part.d3Framedtdndotda(0,0); part.d3Rxxdtdndotdadot = part.d3Framedtdndotdadot(0,0); part.d3Rxxdtdndotdb = part.d3Framedtdndotdb(0,0); part.d3Rxxdtdndotdbdot = part.d3Framedtdndotdbdot(0,0); 
    part.d3Rxxdtdndotdm = part.d3Framedtdndotdm(0,0); part.d3Rxxdtdndotdmdot = part.d3Framedtdndotdmdot(0,0); part.d3Rxxdtdndotdn = part.d3Framedtdndotdn(0,0); part.d3Rxxdtdndotdndot = part.d3Framedtdndotdndot(0,0); 
    
    part.d3Rxydtdadotda = part.d3Framedtdadotda(0,1); part.d3Rxydtdadotdadot = part.d3Framedtdadotdadot(0,1); part.d3Rxydtdadotdb = part.d3Framedtdadotdb(0,1); part.d3Rxydtdadotdbdot = part.d3Framedtdadotdbdot(0,1);  
    part.d3Rxydtdadotdm = part.d3Framedtdadotdm(0,1); part.d3Rxydtdadotdmdot = part.d3Framedtdadotdmdot(0,1); part.d3Rxydtdadotdn = part.d3Framedtdadotdn(0,1); part.d3Rxydtdadotdndot = part.d3Framedtdadotdndot(0,1); 
    part.d3Rxydtdbdotda = part.d3Framedtdbdotda(0,1); part.d3Rxydtdbdotdadot = part.d3Framedtdbdotdadot(0,1); part.d3Rxydtdbdotdb = part.d3Framedtdbdotdb(0,1); part.d3Rxydtdbdotdbdot = part.d3Framedtdbdotdbdot(0,1);  
    part.d3Rxydtdbdotdm = part.d3Framedtdbdotdm(0,1); part.d3Rxydtdbdotdmdot = part.d3Framedtdbdotdmdot(0,1); part.d3Rxydtdbdotdn = part.d3Framedtdbdotdn(0,1); part.d3Rxydtdbdotdndot = part.d3Framedtdbdotdndot(0,1);   
    part.d3Rxydtdmdotda = part.d3Framedtdmdotda(0,1); part.d3Rxydtdmdotdadot = part.d3Framedtdmdotdadot(0,1); part.d3Rxydtdmdotdb = part.d3Framedtdmdotdb(0,1); part.d3Rxydtdmdotdbdot = part.d3Framedtdmdotdbdot(0,1);  
    part.d3Rxydtdmdotdm = part.d3Framedtdmdotdm(0,1); part.d3Rxydtdmdotdmdot = part.d3Framedtdmdotdmdot(0,1); part.d3Rxydtdmdotdn = part.d3Framedtdmdotdn(0,1); part.d3Rxydtdmdotdndot = part.d3Framedtdmdotdndot(0,1);   
    part.d3Rxydtdndotda = part.d3Framedtdndotda(0,1); part.d3Rxydtdndotdadot = part.d3Framedtdndotdadot(0,1); part.d3Rxydtdndotdb = part.d3Framedtdndotdb(0,1); part.d3Rxydtdndotdbdot = part.d3Framedtdndotdbdot(0,1);  
    part.d3Rxydtdndotdm = part.d3Framedtdndotdm(0,1); part.d3Rxydtdndotdmdot = part.d3Framedtdndotdmdot(0,1); part.d3Rxydtdndotdn = part.d3Framedtdndotdn(0,1); part.d3Rxydtdndotdndot = part.d3Framedtdndotdndot(0,1); 
    
    part.d3Rxzdtdadotda = part.d3Framedtdadotda(0,2); part.d3Rxzdtdadotdadot = part.d3Framedtdadotdadot(0,2); part.d3Rxzdtdadotdb = part.d3Framedtdadotdb(0,2); part.d3Rxzdtdadotdbdot = part.d3Framedtdadotdbdot(0,2);  
    part.d3Rxzdtdadotdm = part.d3Framedtdadotdm(0,2); part.d3Rxzdtdadotdmdot = part.d3Framedtdadotdmdot(0,2); part.d3Rxzdtdadotdn = part.d3Framedtdadotdn(0,2); part.d3Rxzdtdadotdndot = part.d3Framedtdadotdndot(0,2); 
    part.d3Rxzdtdbdotda = part.d3Framedtdbdotda(0,2); part.d3Rxzdtdbdotdadot = part.d3Framedtdbdotdadot(0,2); part.d3Rxzdtdbdotdb = part.d3Framedtdbdotdb(0,2); part.d3Rxzdtdbdotdbdot = part.d3Framedtdbdotdbdot(0,2);  
    part.d3Rxzdtdbdotdm = part.d3Framedtdbdotdm(0,2); part.d3Rxzdtdbdotdmdot = part.d3Framedtdbdotdmdot(0,2); part.d3Rxzdtdbdotdn = part.d3Framedtdbdotdn(0,2); part.d3Rxzdtdbdotdndot = part.d3Framedtdbdotdndot(0,2); 
    part.d3Rxzdtdmdotda = part.d3Framedtdmdotda(0,2); part.d3Rxzdtdmdotdadot = part.d3Framedtdmdotdadot(0,2); part.d3Rxzdtdmdotdb = part.d3Framedtdmdotdb(0,2); part.d3Rxzdtdmdotdbdot = part.d3Framedtdmdotdbdot(0,2);  
    part.d3Rxzdtdmdotdm = part.d3Framedtdmdotdm(0,2); part.d3Rxzdtdmdotdmdot = part.d3Framedtdmdotdmdot(0,2); part.d3Rxzdtdmdotdn = part.d3Framedtdmdotdn(0,2); part.d3Rxzdtdmdotdndot = part.d3Framedtdmdotdndot(0,2); 
    part.d3Rxzdtdndotda = part.d3Framedtdndotda(0,2); part.d3Rxzdtdndotdadot = part.d3Framedtdndotdadot(0,2); part.d3Rxzdtdndotdb = part.d3Framedtdndotdb(0,2); part.d3Rxzdtdndotdbdot = part.d3Framedtdndotdbdot(0,2);  
    part.d3Rxzdtdndotdm = part.d3Framedtdndotdm(0,2); part.d3Rxzdtdndotdmdot = part.d3Framedtdndotdmdot(0,2); part.d3Rxzdtdndotdn = part.d3Framedtdndotdn(0,2); part.d3Rxzdtdndotdndot = part.d3Framedtdndotdndot(0,2); 
    
    part.d3Ryxdtdadotda = part.d3Framedtdadotda(1,0); part.d3Ryxdtdadotdadot = part.d3Framedtdadotdadot(1,0); part.d3Ryxdtdadotdb = part.d3Framedtdadotdb(1,0); part.d3Ryxdtdadotdbdot = part.d3Framedtdadotdbdot(1,0);  
    part.d3Ryxdtdadotdm = part.d3Framedtdadotdm(1,0); part.d3Ryxdtdadotdmdot = part.d3Framedtdadotdmdot(1,0); part.d3Ryxdtdadotdn = part.d3Framedtdadotdn(1,0); part.d3Ryxdtdadotdndot = part.d3Framedtdadotdndot(1,0); 
    part.d3Ryxdtdbdotda = part.d3Framedtdbdotda(1,0); part.d3Ryxdtdbdotdadot = part.d3Framedtdbdotdadot(1,0); part.d3Ryxdtdbdotdb = part.d3Framedtdbdotdb(1,0); part.d3Ryxdtdbdotdbdot = part.d3Framedtdbdotdbdot(1,0);  
    part.d3Ryxdtdbdotdm = part.d3Framedtdbdotdm(1,0); part.d3Ryxdtdbdotdmdot = part.d3Framedtdbdotdmdot(1,0); part.d3Ryxdtdbdotdn = part.d3Framedtdbdotdn(1,0); part.d3Ryxdtdbdotdndot = part.d3Framedtdbdotdndot(1,0); 
    part.d3Ryxdtdmdotda = part.d3Framedtdmdotda(1,0); part.d3Ryxdtdmdotdadot = part.d3Framedtdmdotdadot(1,0); part.d3Ryxdtdmdotdb = part.d3Framedtdmdotdb(1,0); part.d3Ryxdtdmdotdbdot = part.d3Framedtdmdotdbdot(1,0);  
    part.d3Ryxdtdmdotdm = part.d3Framedtdmdotdm(1,0); part.d3Ryxdtdmdotdmdot = part.d3Framedtdmdotdmdot(1,0); part.d3Ryxdtdmdotdn = part.d3Framedtdmdotdn(1,0); part.d3Ryxdtdmdotdndot = part.d3Framedtdmdotdndot(1,0); 
    part.d3Ryxdtdndotda = part.d3Framedtdndotda(1,0); part.d3Ryxdtdndotdadot = part.d3Framedtdndotdadot(1,0); part.d3Ryxdtdndotdb = part.d3Framedtdndotdb(1,0); part.d3Ryxdtdndotdbdot = part.d3Framedtdndotdbdot(1,0);  
    part.d3Ryxdtdndotdm = part.d3Framedtdndotdm(1,0); part.d3Ryxdtdndotdmdot = part.d3Framedtdndotdmdot(1,0); part.d3Ryxdtdndotdn = part.d3Framedtdndotdn(1,0); part.d3Ryxdtdndotdndot = part.d3Framedtdndotdndot(1,0); 
    
    part.d3Ryydtdadotda = part.d3Framedtdadotda(1,1); part.d3Ryydtdadotdadot = part.d3Framedtdadotdadot(1,1); part.d3Ryydtdadotdb = part.d3Framedtdadotdb(1,1); part.d3Ryydtdadotdbdot = part.d3Framedtdadotdbdot(1,1);  
    part.d3Ryydtdadotdm = part.d3Framedtdadotdm(1,1); part.d3Ryydtdadotdmdot = part.d3Framedtdadotdmdot(1,1); part.d3Ryydtdadotdn = part.d3Framedtdadotdn(1,1); part.d3Ryydtdadotdndot = part.d3Framedtdadotdndot(1,1); 
    part.d3Ryydtdbdotda = part.d3Framedtdbdotda(1,1); part.d3Ryydtdbdotdadot = part.d3Framedtdbdotdadot(1,1); part.d3Ryydtdbdotdb = part.d3Framedtdbdotdb(1,1); part.d3Ryydtdbdotdbdot = part.d3Framedtdbdotdbdot(1,1);  
    part.d3Ryydtdbdotdm = part.d3Framedtdbdotdm(1,1); part.d3Ryydtdbdotdmdot = part.d3Framedtdbdotdmdot(1,1); part.d3Ryydtdbdotdn = part.d3Framedtdbdotdn(1,1); part.d3Ryydtdbdotdndot = part.d3Framedtdbdotdndot(1,1); 
    part.d3Ryydtdmdotda = part.d3Framedtdmdotda(1,1); part.d3Ryydtdmdotdadot = part.d3Framedtdmdotdadot(1,1); part.d3Ryydtdmdotdb = part.d3Framedtdmdotdb(1,1); part.d3Ryydtdmdotdbdot = part.d3Framedtdmdotdbdot(1,1);  
    part.d3Ryydtdmdotdm = part.d3Framedtdmdotdm(1,1); part.d3Ryydtdmdotdmdot = part.d3Framedtdmdotdmdot(1,1); part.d3Ryydtdmdotdn = part.d3Framedtdmdotdn(1,1); part.d3Ryydtdmdotdndot = part.d3Framedtdmdotdndot(1,1); 
    part.d3Ryydtdndotda = part.d3Framedtdndotda(1,1); part.d3Ryydtdndotdadot = part.d3Framedtdndotdadot(1,1); part.d3Ryydtdndotdb = part.d3Framedtdndotdb(1,1); part.d3Ryydtdndotdbdot = part.d3Framedtdndotdbdot(1,1);  
    part.d3Ryydtdndotdm = part.d3Framedtdndotdm(1,1); part.d3Ryydtdndotdmdot = part.d3Framedtdndotdmdot(1,1); part.d3Ryydtdndotdn = part.d3Framedtdndotdn(1,1); part.d3Ryydtdndotdndot = part.d3Framedtdndotdndot(1,1); 
    
    part.d3Ryzdtdadotda = part.d3Framedtdadotda(1,2); part.d3Ryzdtdadotdadot = part.d3Framedtdadotdadot(1,2); part.d3Ryzdtdadotdb = part.d3Framedtdadotdb(1,2); part.d3Ryzdtdadotdbdot = part.d3Framedtdadotdbdot(1,2);  
    part.d3Ryzdtdadotdm = part.d3Framedtdadotdm(1,2); part.d3Ryzdtdadotdmdot = part.d3Framedtdadotdmdot(1,2); part.d3Ryzdtdadotdn = part.d3Framedtdadotdn(1,2); part.d3Ryzdtdadotdndot = part.d3Framedtdadotdndot(1,2); 
    part.d3Ryzdtdbdotda = part.d3Framedtdbdotda(1,2); part.d3Ryzdtdbdotdadot = part.d3Framedtdbdotdadot(1,2); part.d3Ryzdtdbdotdb = part.d3Framedtdbdotdb(1,2); part.d3Ryzdtdbdotdbdot = part.d3Framedtdbdotdbdot(1,2);  
    part.d3Ryzdtdbdotdm = part.d3Framedtdbdotdm(1,2); part.d3Ryzdtdbdotdmdot = part.d3Framedtdbdotdmdot(1,2); part.d3Ryzdtdbdotdn = part.d3Framedtdbdotdn(1,2); part.d3Ryzdtdbdotdndot = part.d3Framedtdbdotdndot(1,2); 
    part.d3Ryzdtdmdotda = part.d3Framedtdmdotda(1,2); part.d3Ryzdtdmdotdadot = part.d3Framedtdmdotdadot(1,2); part.d3Ryzdtdmdotdb = part.d3Framedtdmdotdb(1,2); part.d3Ryzdtdmdotdbdot = part.d3Framedtdmdotdbdot(1,2);  
    part.d3Ryzdtdmdotdm = part.d3Framedtdmdotdm(1,2); part.d3Ryzdtdmdotdmdot = part.d3Framedtdmdotdmdot(1,2); part.d3Ryzdtdmdotdn = part.d3Framedtdmdotdn(1,2); part.d3Ryzdtdmdotdndot = part.d3Framedtdmdotdndot(1,2); 
    part.d3Ryzdtdndotda = part.d3Framedtdndotda(1,2); part.d3Ryzdtdndotdadot = part.d3Framedtdndotdadot(1,2); part.d3Ryzdtdndotdb = part.d3Framedtdndotdb(1,2); part.d3Ryzdtdndotdbdot = part.d3Framedtdndotdbdot(1,2);  
    part.d3Ryzdtdndotdm = part.d3Framedtdndotdm(1,2); part.d3Ryzdtdndotdmdot = part.d3Framedtdndotdmdot(1,2); part.d3Ryzdtdndotdn = part.d3Framedtdndotdn(1,2); part.d3Ryzdtdndotdndot = part.d3Framedtdndotdndot(1,2); 
    
    part.d3Rzxdtdadotda = part.d3Framedtdadotda(2,0); part.d3Rzxdtdadotdadot = part.d3Framedtdadotdadot(2,0); part.d3Rzxdtdadotdb = part.d3Framedtdadotdb(2,0); part.d3Rzxdtdadotdbdot = part.d3Framedtdadotdbdot(2,0);  
    part.d3Rzxdtdadotdm = part.d3Framedtdadotdm(2,0); part.d3Rzxdtdadotdmdot = part.d3Framedtdadotdmdot(2,0); part.d3Rzxdtdadotdn = part.d3Framedtdadotdn(2,0); part.d3Rzxdtdadotdndot = part.d3Framedtdadotdndot(2,0); 
    part.d3Rzxdtdbdotda = part.d3Framedtdbdotda(2,0); part.d3Rzxdtdbdotdadot = part.d3Framedtdbdotdadot(2,0); part.d3Rzxdtdbdotdb = part.d3Framedtdbdotdb(2,0); part.d3Rzxdtdbdotdbdot = part.d3Framedtdbdotdbdot(2,0);  
    part.d3Rzxdtdbdotdm = part.d3Framedtdbdotdm(2,0); part.d3Rzxdtdbdotdmdot = part.d3Framedtdbdotdmdot(2,0); part.d3Rzxdtdbdotdn = part.d3Framedtdbdotdn(2,0); part.d3Rzxdtdbdotdndot = part.d3Framedtdbdotdndot(2,0); 
    part.d3Rzxdtdmdotda = part.d3Framedtdmdotda(2,0); part.d3Rzxdtdmdotdadot = part.d3Framedtdmdotdadot(2,0); part.d3Rzxdtdmdotdb = part.d3Framedtdmdotdb(2,0); part.d3Rzxdtdmdotdbdot = part.d3Framedtdmdotdbdot(2,0);  
    part.d3Rzxdtdmdotdm = part.d3Framedtdmdotdm(2,0); part.d3Rzxdtdmdotdmdot = part.d3Framedtdmdotdmdot(2,0); part.d3Rzxdtdmdotdn = part.d3Framedtdmdotdn(2,0); part.d3Rzxdtdmdotdndot = part.d3Framedtdmdotdndot(2,0); 
    part.d3Rzxdtdndotda = part.d3Framedtdndotda(2,0); part.d3Rzxdtdndotdadot = part.d3Framedtdndotdadot(2,0); part.d3Rzxdtdndotdb = part.d3Framedtdndotdb(2,0); part.d3Rzxdtdndotdbdot = part.d3Framedtdndotdbdot(2,0);  
    part.d3Rzxdtdndotdm = part.d3Framedtdndotdm(2,0); part.d3Rzxdtdndotdmdot = part.d3Framedtdndotdmdot(2,0); part.d3Rzxdtdndotdn = part.d3Framedtdndotdn(2,0); part.d3Rzxdtdndotdndot = part.d3Framedtdndotdndot(2,0); 
    
    part.d3Rzydtdadotda = part.d3Framedtdadotda(2,1); part.d3Rzydtdadotdadot = part.d3Framedtdadotdadot(2,1); part.d3Rzydtdadotdb = part.d3Framedtdadotdb(2,1); part.d3Rzydtdadotdbdot = part.d3Framedtdadotdbdot(2,1);  
    part.d3Rzydtdadotdm = part.d3Framedtdadotdm(2,1); part.d3Rzydtdadotdmdot = part.d3Framedtdadotdmdot(2,1); part.d3Rzydtdadotdn = part.d3Framedtdadotdn(2,1); part.d3Rzydtdadotdndot = part.d3Framedtdadotdndot(2,1); 
    part.d3Rzydtdbdotda = part.d3Framedtdbdotda(2,1); part.d3Rzydtdbdotdadot = part.d3Framedtdbdotdadot(2,1); part.d3Rzydtdbdotdb = part.d3Framedtdbdotdb(2,1); part.d3Rzydtdbdotdbdot = part.d3Framedtdbdotdbdot(2,1);  
    part.d3Rzydtdbdotdm = part.d3Framedtdbdotdm(2,1); part.d3Rzydtdbdotdmdot = part.d3Framedtdbdotdmdot(2,1); part.d3Rzydtdbdotdn = part.d3Framedtdbdotdn(2,1); part.d3Rzydtdbdotdndot = part.d3Framedtdbdotdndot(2,1); 
    part.d3Rzydtdmdotda = part.d3Framedtdmdotda(2,1); part.d3Rzydtdmdotdadot = part.d3Framedtdmdotdadot(2,1); part.d3Rzydtdmdotdb = part.d3Framedtdmdotdb(2,1); part.d3Rzydtdmdotdbdot = part.d3Framedtdmdotdbdot(2,1);  
    part.d3Rzydtdmdotdm = part.d3Framedtdmdotdm(2,1); part.d3Rzydtdmdotdmdot = part.d3Framedtdmdotdmdot(2,1); part.d3Rzydtdmdotdn = part.d3Framedtdmdotdn(2,1); part.d3Rzydtdmdotdndot = part.d3Framedtdmdotdndot(2,1); 
    part.d3Rzydtdndotda = part.d3Framedtdndotda(2,1); part.d3Rzydtdndotdadot = part.d3Framedtdndotdadot(2,1); part.d3Rzydtdndotdb = part.d3Framedtdndotdb(2,1); part.d3Rzydtdndotdbdot = part.d3Framedtdndotdbdot(2,1);  
    part.d3Rzydtdndotdm = part.d3Framedtdndotdm(2,1); part.d3Rzydtdndotdmdot = part.d3Framedtdndotdmdot(2,1); part.d3Rzydtdndotdn = part.d3Framedtdndotdn(2,1); part.d3Rzydtdndotdndot = part.d3Framedtdndotdndot(2,1); 
    
    part.d3Rzzdtdadotda = part.d3Framedtdadotda(2,2); part.d3Rzzdtdadotdadot = part.d3Framedtdadotdadot(2,2); part.d3Rzzdtdadotdb = part.d3Framedtdadotdb(2,2); part.d3Rzzdtdadotdbdot = part.d3Framedtdadotdbdot(2,2);  
    part.d3Rzzdtdadotdm = part.d3Framedtdadotdm(2,2); part.d3Rzzdtdadotdmdot = part.d3Framedtdadotdmdot(2,2); part.d3Rzzdtdadotdn = part.d3Framedtdadotdn(2,2); part.d3Rzzdtdadotdndot = part.d3Framedtdadotdndot(2,2); 
    part.d3Rzzdtdbdotda = part.d3Framedtdbdotda(2,2); part.d3Rzzdtdbdotdadot = part.d3Framedtdbdotdadot(2,2); part.d3Rzzdtdbdotdb = part.d3Framedtdbdotdb(2,2); part.d3Rzzdtdbdotdbdot = part.d3Framedtdbdotdbdot(2,2);  
    part.d3Rzzdtdbdotdm = part.d3Framedtdbdotdm(2,2); part.d3Rzzdtdbdotdmdot = part.d3Framedtdbdotdmdot(2,2); part.d3Rzzdtdbdotdn = part.d3Framedtdbdotdn(2,2); part.d3Rzzdtdbdotdndot = part.d3Framedtdbdotdndot(2,2); 
    part.d3Rzzdtdmdotda = part.d3Framedtdmdotda(2,2); part.d3Rzzdtdmdotdadot = part.d3Framedtdmdotdadot(2,2); part.d3Rzzdtdmdotdb = part.d3Framedtdmdotdb(2,2); part.d3Rzzdtdmdotdbdot = part.d3Framedtdmdotdbdot(2,2);  
    part.d3Rzzdtdmdotdm = part.d3Framedtdmdotdm(2,2); part.d3Rzzdtdmdotdmdot = part.d3Framedtdmdotdmdot(2,2); part.d3Rzzdtdmdotdn = part.d3Framedtdmdotdn(2,2); part.d3Rzzdtdmdotdndot = part.d3Framedtdmdotdndot(2,2); 
    part.d3Rzzdtdndotda = part.d3Framedtdndotda(2,2); part.d3Rzzdtdndotdadot = part.d3Framedtdndotdadot(2,2); part.d3Rzzdtdndotdb = part.d3Framedtdndotdb(2,2); part.d3Rzzdtdndotdbdot = part.d3Framedtdndotdbdot(2,2);  
    part.d3Rzzdtdndotdm = part.d3Framedtdndotdm(2,2); part.d3Rzzdtdndotdmdot = part.d3Framedtdndotdmdot(2,2); part.d3Rzzdtdndotdn = part.d3Framedtdndotdn(2,2); part.d3Rzzdtdndotdndot = part.d3Framedtdndotdndot(2,2); 
#else
    part.r=part.endPos;
    part.R=part.endFrame;
    part.d1rda=part.dPosda; part.d1rdb=part.dPosdb; part.d1rdn=part.dPosdn; part.d1rdm=part.dPosdm; part.d1rdt=part.dPosdt;
    part.d1Rda=part.dFrameda; part.d1Rdb=part.dFramedb; part.d1Rdn=part.dFramedn; part.d1Rdm=part.dFramedm; part.d1Rdt=part.dFramedt;
    part.d2rdtda=part.d2Posdtda; part.d2rdtdadot=part.d2Posdtdadot; part.d2rdtdb=part.d2Posdtdb; part.d2rdtdbdot=part.d2Posdtdbdot;  
    part.d2rdtdm=part.d2Posdtdm; part.d2rdtdmdot=part.d2Posdtdmdot; part.d2rdtdn=part.d2Posdtdn; part.d2rdtdndot=part.d2Posdtdndot; 
    part.d2Rdtda=part.d2Framedtda; part.d2Rdtdadot=part.d2Framedtdadot; part.d2Rdtdb=part.d2Framedtdb; part.d2Rdtdbdot=part.d2Framedtdbdot;  
    part.d2Rdtdm=part.d2Framedtdm; part.d2Rdtdmdot=part.d2Framedtdmdot; part.d2Rdtdn=part.d2Framedtdn; part.d2Rdtdndot=part.d2Framedtdndot;
    part.d3rdtdadotda=part.d3Posdtdadotda; part.d3rdtdadotdadot=part.d3Posdtdadotdadot; part.d3rdtdadotdb=part.d3Posdtdadotdb; part.d3rdtdadotdbdot=part.d3Posdtdadotdbdot;  
    part.d3rdtdadotdm=part.d3Posdtdadotdm; part.d3rdtdadotdmdot=part.d3Posdtdadotdmdot; part.d3rdtdadotdn=part.d3Posdtdadotdn; part.d3rdtdadotdndot=part.d3Posdtdadotdndot; 
    part.d3rdtdbdotda=part.d3Posdtdbdotda; part.d3rdtdbdotdadot=part.d3Posdtdbdotdadot; part.d3rdtdbdotdb=part.d3Posdtdbdotdb; part.d3rdtdbdotdbdot=part.d3Posdtdbdotdbdot;  
    part.d3rdtdbdotdm=part.d3Posdtdbdotdm; part.d3rdtdbdotdmdot=part.d3Posdtdbdotdmdot; part.d3rdtdbdotdn=part.d3Posdtdbdotdn; part.d3rdtdbdotdndot=part.d3Posdtdbdotdndot; 
    part.d3rdtdmdotda=part.d3Posdtdmdotda; part.d3rdtdmdotdadot=part.d3Posdtdmdotdadot; part.d3rdtdmdotdb=part.d3Posdtdmdotdb; part.d3rdtdmdotdbdot=part.d3Posdtdmdotdbdot;  
    part.d3rdtdmdotdm=part.d3Posdtdmdotdm; part.d3rdtdmdotdmdot=part.d3Posdtdmdotdmdot; part.d3rdtdmdotdn=part.d3Posdtdmdotdn; part.d3rdtdmdotdndot=part.d3Posdtdmdotdndot; 
    part.d3rdtdndotda=part.d3Posdtdndotda; part.d3rdtdndotdadot=part.d3Posdtdndotdadot; part.d3rdtdndotdb=part.d3Posdtdndotdb; part.d3rdtdndotdbdot=part.d3Posdtdndotdbdot;  
    part.d3rdtdndotdm=part.d3Posdtdndotdm; part.d3rdtdndotdmdot=part.d3Posdtdndotdmdot; part.d3rdtdndotdn=part.d3Posdtdndotdn; part.d3rdtdndotdndot=part.d3Posdtdndotdndot; 
    part.d3Rdtdadotda=part.d3Framedtdadotda; part.d3Rdtdadotdadot=part.d3Framedtdadotdadot; part.d3Rdtdadotdb=part.d3Framedtdadotdb; part.d3Rdtdadotdbdot=part.d3Framedtdadotdbdot; 
    part.d3Rdtdadotdm=part.d3Framedtdadotdm; part.d3Rdtdadotdmdot=part.d3Framedtdadotdmdot; part.d3Rdtdadotdn=part.d3Framedtdadotdn; part.d3Rdtdadotdndot=part.d3Framedtdadotdndot; 
    part.d3Rdtdbdotda=part.d3Framedtdbdotda; part.d3Rdtdbdotdadot=part.d3Framedtdbdotdadot; part.d3Rdtdbdotdb=part.d3Framedtdbdotdb; part.d3Rdtdbdotdbdot=part.d3Framedtdbdotdbdot; 
    part.d3Rdtdbdotdm=part.d3Framedtdbdotdm; part.d3Rdtdbdotdmdot=part.d3Framedtdbdotdmdot; part.d3Rdtdbdotdn=part.d3Framedtdbdotdn; part.d3Rdtdbdotdndot=part.d3Framedtdbdotdndot; 
    part.d3Rdtdmdotda=part.d3Framedtdmdotda; part.d3Rdtdmdotdadot=part.d3Framedtdmdotdadot; part.d3Rdtdmdotdb=part.d3Framedtdmdotdb; part.d3Rdtdmdotdbdot=part.d3Framedtdmdotdbdot; 
    part.d3Rdtdmdotdm=part.d3Framedtdmdotdm; part.d3Rdtdmdotdmdot=part.d3Framedtdmdotdmdot; part.d3Rdtdmdotdn=part.d3Framedtdmdotdn; part.d3Rdtdmdotdndot=part.d3Framedtdmdotdndot; 
    part.d3Rdtdndotda=part.d3Framedtdndotda; part.d3Rdtdndotdadot=part.d3Framedtdndotdadot; part.d3Rdtdndotdb=part.d3Framedtdndotdb; part.d3Rdtdndotdbdot=part.d3Framedtdndotdbdot; 
    part.d3Rdtdndotdm=part.d3Framedtdndotdm; part.d3Rdtdndotdmdot=part.d3Framedtdndotdmdot; part.d3Rdtdndotdn=part.d3Framedtdndotdn; part.d3Rdtdndotdndot=part.d3Framedtdndotdndot; 
#endif  
    
    localsStart += localLength;
  }
  endPos=part.endPos;
  endFrame=part.endFrame;
  //std::cout<<"nbParts: "<<nbParts<<std::endl;
}

double DynSegment::estimSMax(double vA, double vB, double vN, double vM, double vAdot, double vBdot, double vNdot, double vMdot) 
{
  double T_2 = (52. / 2.) * std::log(2) / 2.;
  double l0 = std::max(abs(vB), abs(vM * vB));
  double l1 = std::max(abs(vA), abs(vM * vA + vN * vB));
  double l2 = abs(vN * vA);
  l0 = std::max(static_cast<double>(1.), l0);
  l1 = std::max(static_cast<double>(1.), l1);
  l2 = std::max(static_cast<double>(1.), l2);
  
  double l0dot = std::max(abs(vBdot), abs(vMdot * vBdot));
  double l1dot = std::max(abs(vAdot), abs(vMdot * vAdot + vNdot * vBdot));
  double l2dot = abs(vNdot * vAdot);
  l0dot = std::max(static_cast<double>(1.), l0dot);
  l1dot = std::max(static_cast<double>(1.), l1dot);
  l2dot = std::max(static_cast<double>(1.), l2dot);

  if (std::isnan(l0) || std::isnan(l1) || std::isnan(l2)) {
    return std::numeric_limits<double>::infinity();
  }
  if (std::isnan(l0dot) || std::isnan(l1dot) || std::isnan(l2dot)) {
    return std::numeric_limits<double>::infinity();
  }

  double res = std::numeric_limits<double>::infinity();
  if (l0 + l1 + l2 < T_2) { //Smax>1
    res = std::cbrt(T_2 / (l0 + l1 + l2));
  } else { //Smax<1
    res = T_2 / (l0 + l1 + l2);
  }
  
  double resdot = std::numeric_limits<double>::infinity();
  if (l0dot + l1dot + l2dot < T_2) { //Smax>1
    resdot = std::cbrt(T_2 / (l0dot + l1dot + l2dot));
  } else { //Smax<1
    resdot = T_2 / (l0dot + l1dot + l2dot);
  }

  if (res < 1.e-8) {
    res = std::numeric_limits<double>::infinity();
  }
  if (resdot < 1.e-8) {
    resdot = std::numeric_limits<double>::infinity();
  }
  return std::min(res, resdot) / 2.;
}

double DynSegment::estimSMax(double vA, double vB, double vN, double vM)
{
  double T_2 = (52. / 2.) * std::log(2) / 2.;
  double l0 = std::max(abs(vB), abs(vM * vB));
  double l1 = std::max(abs(vA), abs(vM * vA + vN * vB));
  double l2 = abs(vN * vA);
  l0 = std::max(static_cast<double>(1.), l0);
  l1 = std::max(static_cast<double>(1.), l1);
  l2 = std::max(static_cast<double>(1.), l2);

  if (std::isnan(l0) || std::isnan(l1) || std::isnan(l2)) {
    return std::numeric_limits<double>::infinity();
  }

  double res = std::numeric_limits<double>::infinity();
  if (l0 + l1 + l2 < T_2) { //Smax>1
    res = std::cbrt(T_2 / (l0 + l1 + l2));
  } else { //Smax<1
    res = T_2 / (l0 + l1 + l2);
    //let's do a better thing
//     double resinf=res;
//     double ressup=1.;
//     do
//     {
//       res=0.5*resinf+0.5*ressup;
//       if(2.*res*(l0+res*l1+res*res*l2)<T_2)//ok
//         resinf=res;
//       else
//         ressup=res;
//     }
//     while (ressup-resinf>1.e-7);
//     res=resinf;
  }

  if (res < 1.e-8) {
    res = std::numeric_limits<double>::infinity();
  }
  return res / 8.;
}

template<class FunctionPart> void DynSegment::partialStep(Eigen::Vector3d& localr, Eigen::Matrix3d& localR,
    double& localb, double& localbdot, double& localm, double& localmdot, double& localsStart, double& localLength, Eigen::Matrix3d& preK, Eigen::Vector3d& preT)
{
  FunctionPart fun;
  fun.areaDensity = areaDensity;
  fun.width = width;
  fun.D = D;
  fun.G = G;

  fun.r = localr;
  fun.R = localR;
  fun.a = a;
  fun.adot = adot;
  fun.b = localb;
  fun.bdot = localbdot;
  fun.m = localm;
  fun.mdot = localmdot;
  fun.n = n;
  fun.ndot = ndot;
  fun.s =localLength;
  fun();
  Ec+=fun.Ec;
  Eg+=fun.Eg;
  Ew+=fun.Ew;
  L+=fun.L;
  endFrame=fun.endFrame;
  endPos=fun.endPos;
  
  Eigen::FullPivHouseholderQR<Eigen::Matrix3d> invSolver(localR);
  invSolver.setThreshold(1.e-16);
  Eigen::Matrix3d localRinverse = invSolver.inverse();
  
  //std::cout<<(localR*localRinverse-Eigen::Matrix3d::Identity())<<std::endl;
  
  Eigen::Matrix3d K = localRinverse*fun.endFrame;
  Eigen::Vector3d T = localRinverse*(fun.endPos-localr);
  
  Eigen::Matrix3d dFramedaNew = fun.dFrameda + localsStart*fun.dFramedb + dFrameda*K;
  Eigen::Matrix3d dFramedbNew = fun.dFramedb + dFramedb*K;
  Eigen::Matrix3d dFramednNew = fun.dFramedn + localsStart*fun.dFramedm + dFramedn*K;
  Eigen::Matrix3d dFramedmNew = fun.dFramedm + dFramedm*K;
  Eigen::Vector3d dPosdaNew = fun.dPosda + localsStart*fun.dPosdb + dPosda + dFrameda*T;
  Eigen::Vector3d dPosdbNew = fun.dPosdb + dPosdb + dFramedb*T;
  Eigen::Vector3d dPosdnNew = fun.dPosdn + localsStart*fun.dPosdm + dPosdn + dFramedn*T;
  Eigen::Vector3d dPosdmNew = fun.dPosdm + dPosdm + dFramedm*T;
  
  Eigen::Vector4d gradLNew = gradL;
  gradLNew[0] += fun.gradL[0] + localsStart*fun.gradL[1] + 
  MathOperations::contractGeneralDotP(dPosda,fun.dLdPos) +
  MathOperations::contractGeneralDotP(dFrameda,fun.dLdFrame);
  gradLNew[1] += fun.gradL[1] + 
  MathOperations::contractGeneralDotP(dPosdb,fun.dLdPos) +
  MathOperations::contractGeneralDotP(dFramedb,fun.dLdFrame);
  gradLNew[2] += fun.gradL[2] + localsStart*fun.gradL[3] +
  MathOperations::contractGeneralDotP(dPosdn,fun.dLdPos) +
  MathOperations::contractGeneralDotP(dFramedn,fun.dLdFrame);
  gradLNew[3] += fun.gradL[3] + 
  MathOperations::contractGeneralDotP(dPosdm,fun.dLdPos) +
  MathOperations::contractGeneralDotP(dFramedm,fun.dLdFrame);
  
  Eigen::Matrix3d dLdFrameNew;
  for(int i=0;i<3;i++)for(int j=0;j<3;j++)dLdFrameNew(i,j) = dLdFrame(i,j) + preT(j)*fun.dLdPos(i) + preK.row(j).dot(fun.dLdFrame.row(i));
  Eigen::Vector3d dLdPosNew = dLdPos + fun.dLdPos;
  Eigen::IOFormat CleanFmt(4, 0, ", ", "\n", "[", "]");
  
  Eigen::Matrix4d d2Ecdqdot2New=d2Ecdqdot2;
  d2Ecdqdot2New += fun.d2Ecdqdot2;
  std::cout<<fun.d2Ecdqdot2.format(CleanFmt)<<std::endl;
  for(int i=0;i<4;i++)
  {
    d2Ecdqdot2New(0,i) += localsStart*fun.d2Ecdqdot2(1,i);
    d2Ecdqdot2New(2,i) += localsStart*fun.d2Ecdqdot2(3,i);
    d2Ecdqdot2New(i,0) += localsStart*fun.d2Ecdqdot2(i,1);
    d2Ecdqdot2New(i,2) += localsStart*fun.d2Ecdqdot2(i,3);
  }
  d2Ecdqdot2New(0,0) += localsStart*localsStart*fun.d2Ecdqdot2(1,1);
  d2Ecdqdot2New(2,0) += localsStart*localsStart*fun.d2Ecdqdot2(3,1);
  d2Ecdqdot2New(0,2) += localsStart*localsStart*fun.d2Ecdqdot2(1,3);
  d2Ecdqdot2New(2,2) += localsStart*localsStart*fun.d2Ecdqdot2(3,3);
  
  Eigen::Matrix4d d2EcdqdotdqNew=d2Ecdqdotdq;
  //adot,a
  d2EcdqdotdqNew(0,0)  += fun.d2Ecdqdotdq(0,0) + localsStart*fun.d2Ecdqdotdq(1,0) + localsStart*fun.d2Ecdqdotdq(0,1) + localsStart*localsStart*fun.d2Ecdqdotdq(1,1) ;
  //bdot,a
  d2EcdqdotdqNew(1,0)  += fun.d2Ecdqdotdq(1,0) + localsStart*fun.d2Ecdqdotdq(1,1);
  //ndot,a
  d2EcdqdotdqNew(2,0)  += fun.d2Ecdqdotdq(2,0) + localsStart*fun.d2Ecdqdotdq(3,0) + localsStart*fun.d2Ecdqdotdq(2,1) + localsStart*localsStart*fun.d2Ecdqdotdq(3,1) ;
  //mdot,a
  d2EcdqdotdqNew(3,0)  += fun.d2Ecdqdotdq(3,0) + localsStart*fun.d2Ecdqdotdq(3,1);
  
  //adot,b
  d2EcdqdotdqNew(0,1)  += fun.d2Ecdqdotdq(0,1) + localsStart*fun.d2Ecdqdotdq(1,1);
  //bdot,b
  d2EcdqdotdqNew(1,1)  += fun.d2Ecdqdotdq(1,1);
  //ndot,b
  d2EcdqdotdqNew(2,1)  += fun.d2Ecdqdotdq(2,1) + localsStart*fun.d2Ecdqdotdq(3,1);
  //mdot,b
  d2EcdqdotdqNew(3,1)  += fun.d2Ecdqdotdq(3,1);
                          
  //adot,n
  d2EcdqdotdqNew(0,2)  += fun.d2Ecdqdotdq(0,2) + localsStart*fun.d2Ecdqdotdq(1,2) + localsStart*fun.d2Ecdqdotdq(0,3) + localsStart*localsStart*fun.d2Ecdqdotdq(1,3) ;
  //bdot,n
  d2EcdqdotdqNew(1,2)  += fun.d2Ecdqdotdq(1,2) + localsStart*fun.d2Ecdqdotdq(1,3);
  //ndot,n
  d2EcdqdotdqNew(2,2)  += fun.d2Ecdqdotdq(2,2) + localsStart*fun.d2Ecdqdotdq(3,2) + localsStart*fun.d2Ecdqdotdq(2,3) + localsStart*localsStart*fun.d2Ecdqdotdq(3,3) ;
  //mdot,n
  d2EcdqdotdqNew(3,2)  += fun.d2Ecdqdotdq(3,2) + localsStart*fun.d2Ecdqdotdq(3,3);
                          
  //adot,m
  d2EcdqdotdqNew(0,3)  += fun.d2Ecdqdotdq(0,3) + localsStart*fun.d2Ecdqdotdq(1,3);
  //bdot,m
  d2EcdqdotdqNew(1,3)  += fun.d2Ecdqdotdq(1,3);
  //ndot,m
  d2EcdqdotdqNew(2,3)  += fun.d2Ecdqdotdq(2,3) + localsStart*fun.d2Ecdqdotdq(3,3);
  //mdot,m
  d2EcdqdotdqNew(3,3)  += fun.d2Ecdqdotdq(3,3);
  
  dFrameda=dFramedaNew;
  dFramedb=dFramedbNew;
  dFramedn=dFramednNew;
  dFramedm=dFramedmNew;
  dPosda=dPosdaNew;
  dPosdb=dPosdbNew;
  dPosdn=dPosdnNew;
  dPosdm=dPosdmNew;
  dLdFrame=dLdFrameNew;
  dLdPos=dLdPosNew;
  gradL=gradLNew;
  d2Ecdqdot2=d2Ecdqdot2New;
  d2Ecdqdotdq=d2EcdqdotdqNew;
}
