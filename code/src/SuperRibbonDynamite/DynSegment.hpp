/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef DYNSEGMENT_H
#define DYNSEGMENT_H

#include <Eigen/Dense>

class DynSegment
{
public:
  //Parameters
  double areaDensity;
  double width;
  double D;
  Eigen::Vector3d G;
  double anat;
  double bnat;
  double nu;

 //In arguments
  Eigen::Vector3d r;
  Eigen::Matrix3d R;
  double a;
  double adot;
  double b;
  double bdot;
  double m;
  double mdot;
  double n;
  double ndot;
  double length;

 //Out arguments
  double Ec;
  double Eg;
  double Ew;
  double L;
  Eigen::Matrix3d endFrame;
  Eigen::Vector3d endPos;
  Eigen::Matrix3d dFrameda;
  Eigen::Matrix3d dFramedb;
  Eigen::Matrix3d dFramedm;
  Eigen::Matrix3d dFramedn;
  Eigen::Vector3d dPosda;
  Eigen::Vector3d dPosdb;
  Eigen::Vector3d dPosdm;
  Eigen::Vector3d dPosdn;
  Eigen::Matrix3d dLdFrame;
  Eigen::Vector3d dLdPos;
  Eigen::Vector4d gradL;
  Eigen::Matrix4d d2Ecdqdotdq;
  Eigen::Matrix4d d2Ecdqdot2;

  void operator()();
private:
  static double estimSMax(double vA, double vB, double vN, double vM);
  static double estimSMax(double vA, double vB, double vN, double vM, double vAdot, double vBdot, double vNdot, double vMdot);
  template<class FunctionPart> void partialStep(Eigen::Vector3d& localr, Eigen::Matrix3d& localR,
    double& localb, double& localbdot, double& localm, double& localmdot, double& localsStart, double& localLength, Eigen::Matrix3d& preK, Eigen::Vector3d& preT);

};

#endif // DYNSEGMENT_H
