/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "FunctionSegmentSuperClothoid.hpp"
#include "FunctionSuperClothoidPart.hpp"

double FunctionSegmentSuperClothoid::estimSMax(double w1A, double w1B, double w2A, double w2B, double w3A, double w3B, double soffset)
{
  double T_2 = (52. / 2.) * std::log(2) / 2.;
  Eigen::Vector3d lambda0(w1B+soffset*w1A, w2B+soffset*w2A, w3B+soffset*w3A);
  Eigen::Vector3d lambda1(w1A,w2A,w3A);
  double l0 = lambda0.lpNorm<Eigen::Infinity>();
  double l1 = lambda1.lpNorm<Eigen::Infinity>();
  l0 = std::max(static_cast<double>(1.), l0);
  l1 = std::max(static_cast<double>(1.), l1);

  if (std::isnan(l0) || std::isnan(l1)) {
    return std::numeric_limits<real>::infinity();
  }

  double res = std::numeric_limits<real>::infinity();
  if (l0 + l1 < T_2) { //Smax>1
    res = std::cbrt(T_2 / (l0 + l1));
  } else { //Smax<1
    res = T_2 / (l0 + l1);
  }

  if (res < 1.e-8) {
    res = std::numeric_limits<real>::infinity();
  }

  return res / 2.;
}

void FunctionSegmentSuperClothoid::operator()()
{
  FunctionSuperClothoidPart part;
  part.G = G;
  part.Y = Y;
  part.nu = nu;
  part.thickness = thickness;
  part.areaDensity = areaDensity;
  part.soffset = 0.;
  part.width = width;

 //In arguments
  part.q = q;
  part.qdot = qdot;
  part.qnat = qnat;
  part.length = length;
  
 //In functions
  part.R = R;
  part.d1Rdq = d1Rdq;
  part.d1Rdt = d1Rdt;
  part.d2Rdtdq = d2Rdtdq;
  part.d2Rdtdqdot = d2Rdtdqdot;
  part.d2Rdqdq = d2Rdqdq;
  
  part.d1rdq = d1rdq;
  part.d2rdtdq = d2rdtdq;
  part.d2rdtdqdot = d2rdtdqdot;
  part.d2rdqdq = d2rdqdq;
  part.d1rdt = d1rdt;
  part.r = r;
  
  Ec=0.;
  Eg=0.;
  Ew=0.;
  L=0.;
  gradL.setZero();
  sysB.setZero();
  d2Ecdqdot2.setZero();
  gradEg.setZero();
  gradEw.setZero();
  hessEw.setZero();
  hessEg.setZero();
  int nbParts=0;
  double localsStart=0.;
  //std::cout<<"length="<<length<<std::endl;
  while(localsStart<length)
  {
    
    double localLength =  estimSMax(q[0],q[1],q[2],q[3],q[4],q[5],localsStart);
    nbParts++;
    if(localsStart+localLength>length)
      localLength=length-localsStart;
    part.soffset=localsStart;
    part.length=localLength;
    part();
    Ec+=part.Ec;
    Eg+=part.Eg;
    Ew+=part.Ew;
    L+=part.L;
    gradL+=part.gradL;
    gradEw+=part.gradEw;
    gradEg+=part.gradEg;
    hessEw+=part.hessEw;
    hessEg+=part.hessEg;
    //std::cout<<"Partial H(l="<<localLength<<")"<<std::endl<<part.hessEg<<std::endl;
    d2Ecdqdot2+=part.d2Ecdqdot2;
    sysB+=part.sysB;
    part.R = part.endFrame;
    part.d1Rdq = part.dFramedq;
    part.d1Rdt = part.dFramedt;
    part.d2Rdtdq = part.d2Framedtdq;
    part.d2Rdtdqdot = part.d2Framedtdqdot;
    part.d2Rdqdq = part.d2Framedqdq;
    
    part.d1rdq = part.dPosdq;
    part.d2rdtdq = part.d2Posdtdq;
    part.d2rdtdqdot = part.d2Posdtdqdot;
    part.d2rdqdq = part.d2Posdqdq;
    part.d1rdt = part.dPosdt;
    part.r=part.endPos;
    localsStart += localLength;
  }
  d2Framedqdq = part.d2Framedqdq;
  d2Framedtdq = part.d2Framedtdq;
  d2Framedtdqdot = part.d2Framedtdqdot;
  dFramedq = part.dFramedq;
  dFramedt = part.dFramedt;
  endFrame = part.endFrame;
  d2Ecdqdot2 = part.d2Ecdqdot2;
  d2Posdqdq = part.d2Posdqdq;
  d2Posdtdq = part.d2Posdtdq;
  d2Posdtdqdot = part.d2Posdtdqdot;
  dPosdq = part.dPosdq;
  dPosdt = part.dPosdt;
  endPos = part.endPos;
}
