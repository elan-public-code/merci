/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef FUNCTIONSEGMENTSUPERCLOTHOID_H
#define FUNCTIONSEGMENTSUPERCLOTHOID_H

#include <Eigen/Dense>
#include <BaseCore/real.hpp>

class FunctionSegmentSuperClothoid
{
public:
  Eigen::Vector3d G;
  double Y;
  double nu;
  double thickness;
  double areaDensity;
  double width;

 //In arguments
  Eigen::Vector6r q;
  Eigen::Vector6r qdot;
  Eigen::Vector6r qnat;
  double length;
  
 //In functions
  Eigen::Matrix3d R;
  Eigen::Matrix<Eigen::Matrix3d,6,1> d1Rdq;
  Eigen::Matrix3d d1Rdt;
  Eigen::Matrix<Eigen::Matrix3d,6,1> d2Rdtdq;
  Eigen::Matrix<Eigen::Matrix3d,6,1> d2Rdtdqdot;
  Eigen::Matrix<Eigen::Matrix3d,6,6> d2Rdqdq;
  
  Eigen::Matrix<Eigen::Vector3d,6,1> d1rdq;
  Eigen::Matrix<Eigen::Vector3d,6,1> d2rdtdq;
  Eigen::Matrix<Eigen::Vector3d,6,1> d2rdtdqdot;
  Eigen::Matrix<Eigen::Vector3d,6,6> d2rdqdq;
  Eigen::Vector3d d1rdt;
  Eigen::Vector3d r;

 //Out arguments
  Eigen::Matrix<Eigen::Matrix3d,6,6> d2Framedqdq;
  Eigen::Matrix<Eigen::Matrix3d,6,1> d2Framedtdq;
  Eigen::Matrix<Eigen::Matrix3d,6,1> d2Framedtdqdot;
  Eigen::Matrix<Eigen::Matrix3d,6,1> dFramedq;
  Eigen::Matrix3d dFramedt;//
  Eigen::Matrix3d endFrame;//
  Eigen::Matrix6r d2Ecdqdot2;//
  Eigen::Matrix<Eigen::Vector3d,6,6> d2Posdqdq;
  Eigen::Matrix<Eigen::Vector3d,6,1> d2Posdtdq;
  Eigen::Matrix<Eigen::Vector3d,6,1> d2Posdtdqdot;
  Eigen::Matrix<Eigen::Vector3d,6,1> dPosdq;
  Eigen::Vector3d dPosdt;//
  Eigen::Vector3d endPos;//
  Eigen::Vector6r gradL;//
  Eigen::Vector6r gradEg;//
  Eigen::Vector6r gradEw;//
  Eigen::Vector6r gradEc;//
  Eigen::Matrix6r hessEw;
  Eigen::Matrix6r hessEg;
  double Ec;//
  double Eg;//
  double Ew;//
  double L;//
  
  Eigen::Vector6r sysB;

  void operator()();
private:
  static double estimSMax(double w1A, double w1B, double w2A, double w2B, double w3A, double w3B, double offset);
};

#endif // FUNCTIONSEGMENTSUPERCLOTHOID_H
