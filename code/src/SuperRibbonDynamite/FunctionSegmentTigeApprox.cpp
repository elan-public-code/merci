/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "FunctionSegmentTigeApprox.hpp"
#include "FunctionTigeApprox.hpp"

double FunctionSegmentTigeApprox::estimSMax(double vA, double vB, double vN, double vM, double offset)
{
  double vBtranslated=vB+vA*offset;
  double vMtranslated=vM+vN*offset;
  
  double T_2 = (52. / 2.) * std::log(2) / 2.;
  double l0 = std::max(abs(vBtranslated), abs(vMtranslated * vBtranslated));
  double l1 = std::max(abs(vA), abs(vMtranslated * vA + vN * vBtranslated));
  double l2 = abs(vN * vA);
  l0 = std::max(static_cast<double>(1.), l0);
  l1 = std::max(static_cast<double>(1.), l1);
  l2 = std::max(static_cast<double>(1.), l2);

  if (std::isnan(l0) || std::isnan(l1) || std::isnan(l2)) {
    return std::numeric_limits<double>::infinity();
  }

  double res = std::numeric_limits<double>::infinity();
  if (l0 + l1 + l2 < T_2) { //Smax>1
    res = std::cbrt(T_2 / (l0 + l1 + l2));
  } else { //Smax<1
    res = T_2 / (l0 + l1 + l2);
    //let's do a better thing
//     double resinf=res;
//     double ressup=1.;
//     do
//     {
//       res=0.5*resinf+0.5*ressup;
//       if(2.*res*(l0+res*l1+res*res*l2)<T_2)//ok
//         resinf=res;
//       else
//         ressup=res;
//     }
//     while (ressup-resinf>1.e-7);
//     res=resinf;
  }

  if (res < 1.e-8) {
    res = std::numeric_limits<double>::infinity();
  }
  return res / 8.;
}

void FunctionSegmentTigeApprox::operator()()
{
  FunctionPartApproxTige part;
  //Parameters
  part.G=G;
  part.D=D;
  part.areaDensity=areaDensity;
  part.soffset=0.;
  part.width=width;

 //In arguments
  part.a=a;
  part.adot=adot;
  part.anat=anat;
  part.b=b;
  part.bdot=bdot;
  part.bnat=bnat;
  part.length=length;
  part.m=m;
  part.mdot=mdot;
  part.n=n;
  part.ndot=ndot;
  part.nu=nu;

 //In functions
  part.R=R;
  part.d1Rda=d1Rda;
  part.d1Rdb=d1Rdb;
  part.d1Rdm=d1Rdm;
  part.d1Rdn=d1Rdn;
  part.d1Rdt=d1Rdt;
  part.d2Rdtda=d2Rdtda;
  part.d2Rdtdadot=d2Rdtdadot;
  part.d2Rdtdb=d2Rdtdb;
  part.d2Rdtdbdot=d2Rdtdbdot;
  part.d2Rdtdm=d2Rdtdm;
  part.d2Rdtdmdot=d2Rdtdmdot;
  part.d2Rdtdn=d2Rdtdn;
  part.d2Rdtdndot=d2Rdtdndot;
  part.d2Rdada=d2Rdada;
  part.d2Rdadb=d2Rdadb;
  part.d2Rdadn=d2Rdadn;
  part.d2Rdadm=d2Rdadm;
  part.d2Rdbda=d2Rdbda;
  part.d2Rdbdb=d2Rdbdb;
  part.d2Rdbdn=d2Rdbdn;
  part.d2Rdbdm=d2Rdbdm;
  part.d2Rdnda=d2Rdnda;
  part.d2Rdndb=d2Rdndb;
  part.d2Rdndn=d2Rdndn;
  part.d2Rdndm=d2Rdndm;
  part.d2Rdmda=d2Rdmda;
  part.d2Rdmdb=d2Rdmdb;
  part.d2Rdmdn=d2Rdmdn;
  part.d2Rdmdm=d2Rdmdm;
  part.d1rda=d1rda;
  part.d1rdb=d1rdb;
  part.d1rdm=d1rdm;
  part.d1rdn=d1rdn;
  part.d1rdt=d1rdt;
  part.d2rdtda=d2rdtda;
  part.d2rdtdadot=d2rdtdadot;
  part.d2rdtdb=d2rdtdb;
  part.d2rdtdbdot=d2rdtdbdot;
  part.d2rdtdm=d2rdtdm;
  part.d2rdtdmdot=d2rdtdmdot;
  part.d2rdtdn=d2rdtdn;
  part.d2rdtdndot=d2rdtdndot;
  part.d2rdada=d2rdada;
  part.d2rdadb=d2rdadb;
  part.d2rdadn=d2rdadn;
  part.d2rdadm=d2rdadm;
  part.d2rdbda=d2rdbda;
  part.d2rdbdb=d2rdbdb;
  part.d2rdbdn=d2rdbdn;
  part.d2rdbdm=d2rdbdm;
  part.d2rdnda=d2rdnda;
  part.d2rdndb=d2rdndb;
  part.d2rdndn=d2rdndn;
  part.d2rdndm=d2rdndm;
  part.d2rdmda=d2rdmda;
  part.d2rdmdb=d2rdmdb;
  part.d2rdmdn=d2rdmdn;
  part.d2rdmdm=d2rdmdm;
  part.r=r;
  
  Ec=0.;
  Eg=0.;
  Ew=0.;
  L=0.;
  gradL.setZero();
  sysB.setZero();
  d2Ecdqdot2.setZero();
  int nbParts=0;
  double localsStart=0.;
  while(localsStart<length)
  {
    
    double localLength =  estimSMax(a,b,n,m,localsStart);
    nbParts++;
    if(localsStart+localLength>length)
      localLength=length-localsStart;
    part.soffset=localsStart;
    part.length=localLength;
    part();
    Ec+=part.Ec;
    Eg+=part.Eg;
    Ew+=part.Ew;
    L+=part.L;
    gradL+=part.gradL;
    d2Ecdqdot2+=part.d2Ecdqdot2;
    sysB+=part.sysB;
    part.R=part.endFrame;
    part.d1Rda=part.dFrameda;
    part.d1Rdb=part.dFramedb;
    part.d1Rdm=part.dFramedm;
    part.d1Rdn=part.dFramedn;
    part.d1Rdt=part.dFramedt;
    part.d2Rdtda=part.d2Framedtda;
    part.d2Rdtdadot=part.d2Framedtdadot;
    part.d2Rdtdb=part.d2Framedtdb;
    part.d2Rdtdbdot=part.d2Framedtdbdot;
    part.d2Rdtdm=part.d2Framedtdm;
    part.d2Rdtdmdot=part.d2Framedtdmdot;
    part.d2Rdtdn=part.d2Framedtdn;
    part.d2Rdtdndot=part.d2Framedtdndot;
    part.d2Rdada=part.d2Framedada;
    part.d2Rdadb=part.d2Framedadb;
    part.d2Rdadn=part.d2Framedadn;
    part.d2Rdadm=part.d2Framedadm;
    part.d2Rdbda=part.d2Framedbda;
    part.d2Rdbdb=part.d2Framedbdb;
    part.d2Rdbdn=part.d2Framedbdn;
    part.d2Rdbdm=part.d2Framedbdm;
    part.d2Rdnda=part.d2Framednda;
    part.d2Rdndb=part.d2Framedndb;
    part.d2Rdndn=part.d2Framedndn;
    part.d2Rdndm=part.d2Framedndm;
    part.d2Rdmda=part.d2Framedmda;
    part.d2Rdmdb=part.d2Framedmdb;
    part.d2Rdmdn=part.d2Framedmdn;
    part.d2Rdmdm=part.d2Framedmdm;
    part.d1rda=part.dPosda;
    part.d1rdb=part.dPosdb;
    part.d1rdm=part.dPosdm;
    part.d1rdn=part.dPosdn;
    part.d1rdt=part.dPosdt;
    part.d2rdtda=part.d2Posdtda;
    part.d2rdtdadot=part.d2Posdtdadot;
    part.d2rdtdb=part.d2Posdtdb;
    part.d2rdtdbdot=part.d2Posdtdbdot;
    part.d2rdtdm=part.d2Posdtdm;
    part.d2rdtdmdot=part.d2Posdtdmdot;
    part.d2rdtdn=part.d2Posdtdn;
    part.d2rdtdndot=part.d2Posdtdndot;
    part.d2rdada=part.d2Posdada;
    part.d2rdadb=part.d2Posdadb;
    part.d2rdadn=part.d2Posdadn;
    part.d2rdadm=part.d2Posdadm;
    part.d2rdbda=part.d2Posdbda;
    part.d2rdbdb=part.d2Posdbdb;
    part.d2rdbdn=part.d2Posdbdn;
    part.d2rdbdm=part.d2Posdbdm;
    part.d2rdnda=part.d2Posdnda;
    part.d2rdndb=part.d2Posdndb;
    part.d2rdndn=part.d2Posdndn;
    part.d2rdndm=part.d2Posdndm;
    part.d2rdmda=part.d2Posdmda;
    part.d2rdmdb=part.d2Posdmdb;
    part.d2rdmdn=part.d2Posdmdn;
    part.d2rdmdm=part.d2Posdmdm;
    part.r=part.endPos;
    localsStart += localLength;
  }
  endFrame=part.endFrame;
  dFrameda=part.dFrameda;
  dFramedb=part.dFramedb;
  dFramedm=part.dFramedm;
  dFramedn=part.dFramedn;
  dFramedt=part.dFramedt;
  d2Framedtda=part.d2Framedtda;
  d2Framedtdadot=part.d2Framedtdadot;
  d2Framedtdb=part.d2Framedtdb;
  d2Framedtdbdot=part.d2Framedtdbdot;
  d2Framedtdm=part.d2Framedtdm;
  d2Framedtdmdot=part.d2Framedtdmdot;
  d2Framedtdn=part.d2Framedtdn;
  d2Framedtdndot=part.d2Framedtdndot;
  d2Framedada=part.d2Framedada;
  d2Framedadb=part.d2Framedadb;
  d2Framedadn=part.d2Framedadn;
  d2Framedadm=part.d2Framedadm;
  d2Framedbda=part.d2Framedbda;
  d2Framedbdb=part.d2Framedbdb;
  d2Framedbdn=part.d2Framedbdn;
  d2Framedbdm=part.d2Framedbdm;
  d2Framednda=part.d2Framednda;
  d2Framedndb=part.d2Framedndb;
  d2Framedndn=part.d2Framedndn;
  d2Framedndm=part.d2Framedndm;
  d2Framedmda=part.d2Framedmda;
  d2Framedmdb=part.d2Framedmdb;
  d2Framedmdn=part.d2Framedmdn;
  d2Framedmdm=part.d2Framedmdm;
  dPosda=part.dPosda;
  dPosdb=part.dPosdb;
  dPosdm=part.dPosdm;
  dPosdn=part.dPosdn;
  dPosdt=part.dPosdt;
  d2Posdtda=part.d2Posdtda;
  d2Posdtdadot=part.d2Posdtdadot;
  d2Posdtdb=part.d2Posdtdb;
  d2Posdtdbdot=part.d2Posdtdbdot;
  d2Posdtdm=part.d2Posdtdm;
  d2Posdtdmdot=part.d2Posdtdmdot;
  d2Posdtdn=part.d2Posdtdn;
  d2Posdtdndot=part.d2Posdtdndot;
  d2Posdada=part.d2Posdada;
  d2Posdadb=part.d2Posdadb;
  d2Posdadn=part.d2Posdadn;
  d2Posdadm=part.d2Posdadm;
  d2Posdbda=part.d2Posdbda;
  d2Posdbdb=part.d2Posdbdb;
  d2Posdbdn=part.d2Posdbdn;
  d2Posdbdm=part.d2Posdbdm;
  d2Posdnda=part.d2Posdnda;
  d2Posdndb=part.d2Posdndb;
  d2Posdndn=part.d2Posdndn;
  d2Posdndm=part.d2Posdndm;
  d2Posdmda=part.d2Posdmda;
  d2Posdmdb=part.d2Posdmdb;
  d2Posdmdn=part.d2Posdmdn;
  d2Posdmdm=part.d2Posdmdm;
  endPos=part.endPos;
}
