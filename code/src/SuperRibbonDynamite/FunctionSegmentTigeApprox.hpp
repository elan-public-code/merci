/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef FUNCTIONSEGMENTTIGEAPPROX_H
#define FUNCTIONSEGMENTTIGEAPPROX_H

#include <Eigen/Dense>

class FunctionSegmentTigeApprox
{
public:
  //Parameters
  Eigen::Vector3d G;
  double D;
  double areaDensity;
  double width;

 //In arguments
  double a;
  double adot;
  double anat;
  double b;
  double bdot;
  double bnat;
  double length;
  double m;
  double mdot;
  double n;
  double ndot;
  double nu;

 //In functions
  Eigen::Matrix3d R;
  Eigen::Matrix3d d1Rda;
  Eigen::Matrix3d d1Rdb;
  Eigen::Matrix3d d1Rdm;
  Eigen::Matrix3d d1Rdn;
  Eigen::Matrix3d d1Rdt;
  Eigen::Matrix3d d2Rdtda;
  Eigen::Matrix3d d2Rdtdadot;
  Eigen::Matrix3d d2Rdtdb;
  Eigen::Matrix3d d2Rdtdbdot;
  Eigen::Matrix3d d2Rdtdm;
  Eigen::Matrix3d d2Rdtdmdot;
  Eigen::Matrix3d d2Rdtdn;
  Eigen::Matrix3d d2Rdtdndot;
  Eigen::Matrix3d d2Rdada;
  Eigen::Matrix3d d2Rdadb;
  Eigen::Matrix3d d2Rdadn;
  Eigen::Matrix3d d2Rdadm;
  Eigen::Matrix3d d2Rdbda;
  Eigen::Matrix3d d2Rdbdb;
  Eigen::Matrix3d d2Rdbdn;
  Eigen::Matrix3d d2Rdbdm;
  Eigen::Matrix3d d2Rdnda;
  Eigen::Matrix3d d2Rdndb;
  Eigen::Matrix3d d2Rdndn;
  Eigen::Matrix3d d2Rdndm;
  Eigen::Matrix3d d2Rdmda;
  Eigen::Matrix3d d2Rdmdb;
  Eigen::Matrix3d d2Rdmdn;
  Eigen::Matrix3d d2Rdmdm;
  Eigen::Vector3d d1rda;
  Eigen::Vector3d d1rdb;
  Eigen::Vector3d d1rdm;
  Eigen::Vector3d d1rdn;
  Eigen::Vector3d d1rdt;
  Eigen::Vector3d d2rdtda;
  Eigen::Vector3d d2rdtdadot;
  Eigen::Vector3d d2rdtdb;
  Eigen::Vector3d d2rdtdbdot;
  Eigen::Vector3d d2rdtdm;
  Eigen::Vector3d d2rdtdmdot;
  Eigen::Vector3d d2rdtdn;
  Eigen::Vector3d d2rdtdndot;
  Eigen::Vector3d d2rdada;
  Eigen::Vector3d d2rdadb;
  Eigen::Vector3d d2rdadn;
  Eigen::Vector3d d2rdadm;
  Eigen::Vector3d d2rdbda;
  Eigen::Vector3d d2rdbdb;
  Eigen::Vector3d d2rdbdn;
  Eigen::Vector3d d2rdbdm;
  Eigen::Vector3d d2rdnda;
  Eigen::Vector3d d2rdndb;
  Eigen::Vector3d d2rdndn;
  Eigen::Vector3d d2rdndm;
  Eigen::Vector3d d2rdmda;
  Eigen::Vector3d d2rdmdb;
  Eigen::Vector3d d2rdmdn;
  Eigen::Vector3d d2rdmdm;
  Eigen::Vector3d r;

 //Out arguments
  Eigen::Matrix3d d2Framedada;//
  Eigen::Matrix3d d2Framedadb;//
  Eigen::Matrix3d d2Framedadn;//
  Eigen::Matrix3d d2Framedadm;//
  Eigen::Matrix3d d2Framedbda;//
  Eigen::Matrix3d d2Framedbdb;//
  Eigen::Matrix3d d2Framedbdn;//
  Eigen::Matrix3d d2Framedbdm;//
  Eigen::Matrix3d d2Framednda;//
  Eigen::Matrix3d d2Framedndb;//
  Eigen::Matrix3d d2Framedndn;//
  Eigen::Matrix3d d2Framedndm;//
  Eigen::Matrix3d d2Framedmda;//
  Eigen::Matrix3d d2Framedmdb;//
  Eigen::Matrix3d d2Framedmdn;//
  Eigen::Matrix3d d2Framedmdm;//
  Eigen::Matrix3d d2Framedtda;//
  Eigen::Matrix3d d2Framedtdadot;//
  Eigen::Matrix3d d2Framedtdb;//
  Eigen::Matrix3d d2Framedtdbdot;//
  Eigen::Matrix3d d2Framedtdm;//
  Eigen::Matrix3d d2Framedtdmdot;//
  Eigen::Matrix3d d2Framedtdn;//
  Eigen::Matrix3d d2Framedtdndot;//
  Eigen::Matrix3d dFrameda;//
  Eigen::Matrix3d dFramedb;//
  Eigen::Matrix3d dFramedm;//
  Eigen::Matrix3d dFramedn;//
  Eigen::Matrix3d dFramedt;//
  Eigen::Matrix3d endFrame;//
  Eigen::Matrix4d d2Ecdqdot2;//
  Eigen::Vector3d d2Posdada;//
  Eigen::Vector3d d2Posdadb;//
  Eigen::Vector3d d2Posdadn;//
  Eigen::Vector3d d2Posdadm;//
  Eigen::Vector3d d2Posdbda;//
  Eigen::Vector3d d2Posdbdb;//
  Eigen::Vector3d d2Posdbdn;//
  Eigen::Vector3d d2Posdbdm;//
  Eigen::Vector3d d2Posdnda;//
  Eigen::Vector3d d2Posdndb;//
  Eigen::Vector3d d2Posdndn;//
  Eigen::Vector3d d2Posdndm;//
  Eigen::Vector3d d2Posdmda;//
  Eigen::Vector3d d2Posdmdb;//
  Eigen::Vector3d d2Posdmdn;//
  Eigen::Vector3d d2Posdmdm;//
  Eigen::Vector3d d2Posdtda;//
  Eigen::Vector3d d2Posdtdadot;//
  Eigen::Vector3d d2Posdtdb;//
  Eigen::Vector3d d2Posdtdbdot;//
  Eigen::Vector3d d2Posdtdm;//
  Eigen::Vector3d d2Posdtdmdot;//
  Eigen::Vector3d d2Posdtdn;//
  Eigen::Vector3d d2Posdtdndot;//
  Eigen::Vector3d dPosda;//
  Eigen::Vector3d dPosdb;//
  Eigen::Vector3d dPosdm;//
  Eigen::Vector3d dPosdn;//
  Eigen::Vector3d dPosdt;//
  Eigen::Vector3d endPos;//
  Eigen::Vector4d gradL;//
  Eigen::Vector4d gradEg;//
  Eigen::Vector4d gradEw;//
  Eigen::Vector4d gradEc;//
  double Ec;//
  double Eg;//
  double Ew;//
  double L;//
  
  Eigen::Vector4d sysB;

  void operator()();
private:
  
  static double estimSMax(double vA, double vB, double vN, double vM, double offset);
  
};

#endif // FUNCTIONSEGMENTTIGEAPPROX_H
