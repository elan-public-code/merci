/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "FunctionSuperClothoidPart.hpp"
#include <CElasticEnergy.hpp>

#define SERIE_NEEDED_ORDER (52 * 3 / 2)

FunctionSuperClothoidPart::FunctionSuperClothoidPart():
  sK({},0.),  su({},0.),


  sdKdw1A({},0.), sdKdw1B({},0.), sdKdw2A({},0.), sdKdw2B({},0.), sdKdw3A({},0.), sdKdw3B({},0.),
  sdKdw1A0({},0.), sdKdw2A0({},0.), sdKdw3A0({},0.),
  sdudw1A({},0.), sdudw1B({},0.), sdudw2A({},0.), sdudw2B({},0.), sdudw3A({},0.), sdudw3B({},0.),
  
  sd2Kdw1A0dw1A0({},0.), sd2Kdw1Bdw1A0({},0.), sd2Kdw2A0dw1A0({},0.), sd2Kdw2Bdw1A0({},0.),
  sd2Kdw3A0dw1A0({},0.), sd2Kdw3Bdw1A0({},0.), sd2Kdw1A0dw1B({},0.), sd2Kdw2A0dw1B({},0.),
  sd2Kdw3A0dw1B({},0.), sd2Kdw1A0dw2A0({},0.), sd2Kdw1Bdw2A0({},0.), sd2Kdw2A0dw2A0({},0.),
  sd2Kdw2Bdw2A0({},0.), sd2Kdw3A0dw2A0({},0.), sd2Kdw3Bdw2A0({},0.), sd2Kdw1A0dw2B({},0.),
  sd2Kdw2A0dw2B({},0.), sd2Kdw3A0dw2B({},0.), sd2Kdw1A0dw3A0({},0.), sd2Kdw1Bdw3A0({},0.),
  sd2Kdw2A0dw3A0({},0.), sd2Kdw2Bdw3A0({},0.), sd2Kdw3A0dw3A0({},0.), sd2Kdw3Bdw3A0({},0.),
  sd2Kdw1A0dw3B({},0.), sd2Kdw2A0dw3B({},0.), sd2Kdw3A0dw3B({},0.),
  
  sd2Kdw1Adw1A({},0.), sd2Kdw1Bdw1A({},0.), sd2Kdw2Adw1A({},0.), sd2Kdw2Bdw1A({},0.),
  sd2Kdw3Adw1A({},0.), sd2Kdw3Bdw1A({},0.), sd2Kdw1Adw1B({},0.), sd2Kdw1Bdw1B({},0.),
  sd2Kdw2Adw1B({},0.), sd2Kdw2Bdw1B({},0.), sd2Kdw3Adw1B({},0.), sd2Kdw3Bdw1B({},0.),
  sd2Kdw1Adw2A({},0.), sd2Kdw1Bdw2A({},0.), sd2Kdw2Adw2A({},0.), sd2Kdw2Bdw2A({},0.),
  sd2Kdw3Adw2A({},0.), sd2Kdw3Bdw2A({},0.), sd2Kdw1Adw2B({},0.), sd2Kdw1Bdw2B({},0.),
  sd2Kdw2Adw2B({},0.), sd2Kdw2Bdw2B({},0.), sd2Kdw3Adw2B({},0.), sd2Kdw3Bdw2B({},0.),
  sd2Kdw1Adw3A({},0.), sd2Kdw1Bdw3A({},0.), sd2Kdw2Adw3A({},0.), sd2Kdw2Bdw3A({},0.),
  sd2Kdw3Adw3A({},0.), sd2Kdw3Bdw3A({},0.), sd2Kdw1Adw3B({},0.), sd2Kdw1Bdw3B({},0.),
  sd2Kdw2Adw3B({},0.), sd2Kdw2Bdw3B({},0.), sd2Kdw3Adw3B({},0.), sd2Kdw3Bdw3B({},0.),
  
  sd2udw1Adw1A({},0.), sd2udw1Bdw1A({},0.), sd2udw2Adw1A({},0.), sd2udw2Bdw1A({},0.),
  sd2udw3Adw1A({},0.), sd2udw3Bdw1A({},0.), sd2udw1Adw1B({},0.), sd2udw1Bdw1B({},0.),
  sd2udw2Adw1B({},0.), sd2udw2Bdw1B({},0.), sd2udw3Adw1B({},0.), sd2udw3Bdw1B({},0.),
  sd2udw1Adw2A({},0.), sd2udw1Bdw2A({},0.), sd2udw2Adw2A({},0.), sd2udw2Bdw2A({},0.),
  sd2udw3Adw2A({},0.), sd2udw3Bdw2A({},0.), sd2udw1Adw2B({},0.), sd2udw1Bdw2B({},0.),
  sd2udw2Adw2B({},0.), sd2udw2Bdw2B({},0.), sd2udw3Adw2B({},0.), sd2udw3Bdw2B({},0.),
  sd2udw1Adw3A({},0.), sd2udw1Bdw3A({},0.), sd2udw2Adw3A({},0.), sd2udw2Bdw3A({},0.),
  sd2udw3Adw3A({},0.), sd2udw3Bdw3A({},0.), sd2udw1Adw3B({},0.), sd2udw1Bdw3B({},0.),
  sd2udw2Adw3B({},0.), sd2udw2Bdw3B({},0.), sd2udw3Adw3B({},0.), sd2udw3Bdw3B({},0.)
{
}


Eigen::Matrix3r FunctionSuperClothoidPart::skewOf(Eigen::Vector3r w)
{
  Eigen::Matrix3r res;
  res << 0,    -w[2], w[1],
      w[2],  0,    -w[0],
      -w[1], w[0], 0;
  return res;
}

void FunctionSuperClothoidPart::operator()()
{
  computeSeriePreTerms();
  computeDerivationsPreTerms();
  computeDerivationsPreTermsSecondOrder();
  computeEc();
  computeEgAndGradEg();
  computeEwAndGradEw();
  computed2Ecdqdot2();
  computeGradEc();
  computeSysB();
  L=Ec-Eg-Ew;
  gradL=gradEc-gradEg-gradEw;
  computeEnds();
}

void FunctionSuperClothoidPart::computeSeriePreTerms()
{
  w1Btranslated=q[1]+soffset*q[0];
  w2Btranslated=q[3]+soffset*q[2];
  w3Btranslated=q[5]+soffset*q[4];
  lambda0Sq = skewOf(Eigen::Vector3r(w1Btranslated, w2Btranslated, w3Btranslated));
  lambda1Sq = skewOf(Eigen::Vector3r(q[0],  q[2], q[3]));
  int order = SERIE_NEEDED_ORDER;
  std::vector<Eigen::Matrix3r> FramePreTerms(order+1);
  FramePreTerms[0]=Eigen::Matrix3r::Identity();
  FramePreTerms[1]=lambda0Sq;
  for (int n = 2; n <= order; n++) {
    FramePreTerms[n]=(1. / n) *
                            (
                              FramePreTerms[n - 1]*lambda0Sq
                              + FramePreTerms[n - 2]*lambda1Sq
                            );
  }
  sK=SerieMat(FramePreTerms, length);
  su=sK.col(2).integrate();
}

void FunctionSuperClothoidPart::computeDerivationsPreTerms()
{
  Eigen::Vector3r ex, ey, ez;
  Eigen::Matrix3r exSq, eySq, ezSq;
  ex<<1.,0.,0.;
  ey<<0.,1.,0.;
  ez<<0.,0.,1.;
  exSq=skewOf(ex);
  eySq=skewOf(ey);
  ezSq=skewOf(ez);

  int order = sK.size();
  assert(order > 0);

  //Derivation of the frame
  {
    std::vector<Eigen::Matrix3r> dFramePreTermsdw1A(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdw1B(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdw2A(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdw2B(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdw3A(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdw3B(order);
    
    Eigen::Matrix3r zero;
    zero.setZero();
    //order 0
    dFramePreTermsdw1A[0]=zero;
    dFramePreTermsdw1B[0]=zero;
    dFramePreTermsdw2A[0]=zero;
    dFramePreTermsdw2B[0]=zero;
    dFramePreTermsdw3A[0]=zero;
    dFramePreTermsdw3B[0]=zero;
    //order 1
    dFramePreTermsdw1A[1]=zero;
    dFramePreTermsdw1B[1]=exSq;
    dFramePreTermsdw2A[1]=zero;
    dFramePreTermsdw2B[1]=eySq;
    dFramePreTermsdw3A[1]=zero;
    dFramePreTermsdw3B[1]=ezSq;
    //order 2
    //order n
    for (int n = 2; n < order; n++) {
      dFramePreTermsdw1A[n]=((1. / static_cast<real>(n)) * (
                                   sK[n - 2]*exSq 
                                   + dFramePreTermsdw1A[n - 1]*lambda0Sq + dFramePreTermsdw1A[n - 2]*lambda1Sq));
      dFramePreTermsdw1B[n]=((1. / static_cast<real>(n)) * (
                                   sK[n - 1]*exSq 
                                   + dFramePreTermsdw1B[n - 1]*lambda0Sq + dFramePreTermsdw1B[n - 2]*lambda1Sq));
      dFramePreTermsdw2A[n]=((1. / static_cast<real>(n)) * (
                                   sK[n - 2]*eySq 
                                   + dFramePreTermsdw2A[n - 1]*lambda0Sq + dFramePreTermsdw2A[n - 2]*lambda1Sq));
      dFramePreTermsdw2B[n]=((1. / static_cast<real>(n)) * (
                                   sK[n - 1]*eySq 
                                   + dFramePreTermsdw2B[n - 1]*lambda0Sq + dFramePreTermsdw2B[n - 2]*lambda1Sq));
      dFramePreTermsdw3A[n]=((1. / static_cast<real>(n)) * (
                                   sK[n - 2]*ezSq 
                                   + dFramePreTermsdw3A[n - 1]*lambda0Sq + dFramePreTermsdw3A[n - 2]*lambda1Sq));
      dFramePreTermsdw3B[n]=((1. / static_cast<real>(n)) * (
                                   sK[n - 1]*ezSq 
                                   + dFramePreTermsdw3B[n - 1]*lambda0Sq + dFramePreTermsdw3B[n - 2]*lambda1Sq));
    }
    sdKdw1A0 = SerieMat(dFramePreTermsdw1A, length);
    sdKdw1B = SerieMat(dFramePreTermsdw1B, length);
    sdKdw2A0 = SerieMat(dFramePreTermsdw2A, length);
    sdKdw2B = SerieMat(dFramePreTermsdw2B, length);
    sdKdw3A0 = SerieMat(dFramePreTermsdw3A, length);
    sdKdw3B = SerieMat(dFramePreTermsdw3B, length);
    sdKdw1A = sdKdw1A0 + length*sdKdw1B;
    sdKdw2A = sdKdw2A0 + length*sdKdw2B;
    sdKdw3A = sdKdw3A0 + length*sdKdw3B;
  }
    //Derivation of the position
  {
    sdudw1A = sdKdw1A.col(2).integrate();
    sdudw1B = sdKdw1B.col(2).integrate();
    sdudw2A = sdKdw2A.col(2).integrate();
    sdudw2B = sdKdw2B.col(2).integrate();
    sdudw3A = sdKdw3A.col(2).integrate();
    sdudw3B = sdKdw3B.col(2).integrate();
  }
}

void FunctionSuperClothoidPart::computeDerivationsPreTermsSecondOrder()
{
  int order = sK.size();
  assert(order > 0);

  //Derivation of the frame
  {
    Eigen::Vector3r ex, ey, ez;
    Eigen::Matrix3r exSq, eySq, ezSq;
    ex<<1.,0.,0.;
    ey<<0.,1.,0.;
    ez<<0.,0.,1.;
    exSq=skewOf(ex);
    eySq=skewOf(ey);
    ezSq=skewOf(ez);
    std::vector<Eigen::Matrix3r> dFramePreTermsdw1Aw1A(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdw1Aw1B(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdw1Aw2A(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdw1Aw2B(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdw1Aw3A(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdw1Aw3B(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdw1Bw1B(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdw1Bw2A(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdw1Bw2B(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdw1Bw3A(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdw1Bw3B(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdw2Aw2A(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdw2Aw2B(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdw2Aw3A(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdw2Aw3B(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdw2Bw2B(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdw2Bw3A(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdw2Bw3B(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdw3Aw3A(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdw3Aw3B(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdw3Bw3B(order);
    Eigen::Matrix3r zero;
    zero.setZero();
    //order 0
    dFramePreTermsdw1Aw1A[0]=zero;
    dFramePreTermsdw1Aw1B[0]=zero;
    dFramePreTermsdw1Aw2A[0]=zero;
    dFramePreTermsdw1Aw2B[0]=zero;
    dFramePreTermsdw1Aw3A[0]=zero;
    dFramePreTermsdw1Aw3B[0]=zero;
    dFramePreTermsdw1Bw1B[0]=zero;
    dFramePreTermsdw1Bw2A[0]=zero;
    dFramePreTermsdw1Bw2B[0]=zero;
    dFramePreTermsdw1Bw3A[0]=zero;
    dFramePreTermsdw1Bw3B[0]=zero;
    dFramePreTermsdw2Aw2A[0]=zero;
    dFramePreTermsdw2Aw2B[0]=zero;
    dFramePreTermsdw2Aw3A[0]=zero;
    dFramePreTermsdw2Aw3B[0]=zero;
    dFramePreTermsdw2Bw2B[0]=zero;
    dFramePreTermsdw2Bw3A[0]=zero;
    dFramePreTermsdw2Bw3B[0]=zero;
    dFramePreTermsdw3Aw3A[0]=zero;
    dFramePreTermsdw3Aw3B[0]=zero;
    dFramePreTermsdw3Bw3B[0]=zero;
    //order 1
    dFramePreTermsdw1Aw1A[1]=zero;
    dFramePreTermsdw1Aw1B[1]=zero;
    dFramePreTermsdw1Aw2A[1]=zero;
    dFramePreTermsdw1Aw2B[1]=zero;
    dFramePreTermsdw1Aw3A[1]=zero;
    dFramePreTermsdw1Aw3B[1]=zero;
    dFramePreTermsdw1Bw1B[1]=zero;
    dFramePreTermsdw1Bw2A[1]=zero;
    dFramePreTermsdw1Bw2B[1]=zero;
    dFramePreTermsdw1Bw3A[1]=zero;
    dFramePreTermsdw1Bw3B[1]=zero;
    dFramePreTermsdw2Aw2A[1]=zero;
    dFramePreTermsdw2Aw2B[1]=zero;
    dFramePreTermsdw2Aw3A[1]=zero;
    dFramePreTermsdw2Aw3B[1]=zero;
    dFramePreTermsdw2Bw2B[1]=zero;
    dFramePreTermsdw2Bw3A[1]=zero;
    dFramePreTermsdw2Bw3B[1]=zero;
    dFramePreTermsdw3Aw3A[1]=zero;
    dFramePreTermsdw3Aw3B[1]=zero;
    dFramePreTermsdw3Bw3B[1]=zero;
    //order n
    for (int n = 2; n < order; n++) {
      dFramePreTermsdw1Aw1A[n]=((1. / static_cast<real>(n)) * (
        2.0* sdKdw1A0[n - 2]*exSq 
        + dFramePreTermsdw1Aw1A[n - 1]*lambda0Sq + dFramePreTermsdw1Aw1A[n - 2]*lambda1Sq));
      dFramePreTermsdw1Aw1B[n]= ((1. / static_cast<real>(n)) * (
        sdKdw1A0[n - 1]*exSq  + sdKdw1B[n - 2]*exSq
        + dFramePreTermsdw1Aw1B[n - 1]*lambda0Sq + dFramePreTermsdw1Aw1B[n - 2]*lambda1Sq));
      dFramePreTermsdw1Aw2A[n]=((1. / static_cast<real>(n)) * (
        sdKdw1A0[n - 2]*eySq  + sdKdw2A0[n - 2]*exSq
        + dFramePreTermsdw1Aw2A[n - 1]*lambda0Sq + dFramePreTermsdw1Aw2A[n - 2]*lambda1Sq));
      dFramePreTermsdw1Aw2B[n]=((1. / static_cast<real>(n)) * (
        sdKdw1A0[n - 1]*eySq  + sdKdw1B[n - 2]*exSq
        + dFramePreTermsdw1Aw2B[n - 1]*lambda0Sq + dFramePreTermsdw1Aw2B[n - 2]*lambda1Sq));
      dFramePreTermsdw1Aw3A[n]=((1. / static_cast<real>(n)) * (
        sdKdw1A0[n - 2]*ezSq  + sdKdw3A0[n - 2]*exSq
        + dFramePreTermsdw1Aw3A[n - 1]*lambda0Sq + dFramePreTermsdw1Aw3A[n - 2]*lambda1Sq));
      dFramePreTermsdw1Aw3B[n]=((1. / static_cast<real>(n)) * (
        sdKdw1A0[n - 1]*ezSq  + sdKdw1B[n - 2]*exSq
        + dFramePreTermsdw1Aw3B[n - 1]*lambda0Sq + dFramePreTermsdw1Aw3B[n - 2]*lambda1Sq));
      
      dFramePreTermsdw1Bw1B[n]=dFramePreTermsdw1Bw1B[n]=((1. / static_cast<real>(n)) * (
        2.0* sdKdw1B[n - 1]*exSq 
        + dFramePreTermsdw1Bw1B[n - 1]*lambda0Sq + dFramePreTermsdw1Bw1B[n - 2]*lambda1Sq));
      dFramePreTermsdw1Bw2A[n]=((1. / static_cast<real>(n)) * (
        sdKdw2A0[n - 1]*exSq  + sdKdw1B[n - 2]*eySq
        + dFramePreTermsdw1Bw2A[n - 1]*lambda0Sq + dFramePreTermsdw1Bw2A[n - 2]*lambda1Sq));
      dFramePreTermsdw1Bw2B[n]=((1. / static_cast<real>(n)) * (
        sdKdw1B[n - 1]*eySq  + sdKdw1B[n - 1]*exSq
        + dFramePreTermsdw1Bw2B[n - 1]*lambda0Sq + dFramePreTermsdw1Bw2B[n - 2]*lambda1Sq));
      dFramePreTermsdw1Bw3A[n]=((1. / static_cast<real>(n)) * (
        sdKdw3A0[n - 1]*exSq  + sdKdw1B[n - 2]*ezSq
        + dFramePreTermsdw1Bw3A[n - 1]*lambda0Sq + dFramePreTermsdw1Bw3A[n - 2]*lambda1Sq));
      dFramePreTermsdw1Bw3B[n]=((1. / static_cast<real>(n)) * (
        sdKdw1B[n - 1]*ezSq  + sdKdw1B[n - 1]*exSq
        + dFramePreTermsdw1Bw3B[n - 1]*lambda0Sq + dFramePreTermsdw1Bw3B[n - 2]*lambda1Sq));
      
      dFramePreTermsdw2Aw2A[n]=((1. / static_cast<real>(n)) * (
        2.0* sdKdw2A0[n - 2]*eySq 
        + dFramePreTermsdw2Aw2A[n - 1]*lambda0Sq + dFramePreTermsdw2Aw2A[n - 2]*lambda1Sq));
      dFramePreTermsdw2Aw2B[n]= ((1. / static_cast<real>(n)) * (
        sdKdw2A0[n - 1]*eySq  + sdKdw1B[n - 2]*eySq
        + dFramePreTermsdw2Aw2B[n - 1]*lambda0Sq + dFramePreTermsdw2Aw2B[n - 2]*lambda1Sq));
      dFramePreTermsdw2Aw3A[n]=((1. / static_cast<real>(n)) * (
        sdKdw2A0[n - 2]*ezSq  + sdKdw3A0[n - 2]*eySq
        + dFramePreTermsdw2Aw3A[n - 1]*lambda0Sq + dFramePreTermsdw2Aw3A[n - 2]*lambda1Sq));
      dFramePreTermsdw2Aw3B[n]=((1. / static_cast<real>(n)) * (
        sdKdw2A0[n - 1]*ezSq  + sdKdw1B[n - 2]*eySq
        + dFramePreTermsdw2Aw3B[n - 1]*lambda0Sq + dFramePreTermsdw2Aw3B[n - 2]*lambda1Sq));
      
      dFramePreTermsdw2Bw2B[n]=((1. / static_cast<real>(n)) * (
        2.0* sdKdw1B[n - 1]*eySq 
        + dFramePreTermsdw2Bw2B[n - 1]*lambda0Sq + dFramePreTermsdw2Bw2B[n - 2]*lambda1Sq));
      dFramePreTermsdw2Bw3A[n]= ((1. / static_cast<real>(n)) * (
        sdKdw3A0[n - 1]*eySq  + sdKdw1B[n - 2]*ezSq
        + dFramePreTermsdw2Bw3A[n - 1]*lambda0Sq + dFramePreTermsdw2Bw3A[n - 2]*lambda1Sq));
      dFramePreTermsdw2Bw3B[n]=((1. / static_cast<real>(n)) * (
        sdKdw1B[n - 1]*ezSq  + sdKdw1B[n - 1]*eySq
        + dFramePreTermsdw2Bw3B[n - 1]*lambda0Sq + dFramePreTermsdw2Bw3B[n - 2]*lambda1Sq));
      
      dFramePreTermsdw3Aw3A[n]=((1. / static_cast<real>(n)) * (
        2.0* sdKdw3A0[n - 2]*ezSq 
        + dFramePreTermsdw3Aw3A[n - 1]*lambda0Sq + dFramePreTermsdw3Aw3A[n - 2]*lambda1Sq));
      dFramePreTermsdw3Aw3B[n]= ((1. / static_cast<real>(n)) * (
        sdKdw3A0[n - 1]*ezSq  + sdKdw1B[n - 2]*ezSq
        + dFramePreTermsdw3Aw3B[n - 1]*lambda0Sq + dFramePreTermsdw3Aw3B[n - 2]*lambda1Sq));
      
      dFramePreTermsdw3Bw3B[n]=((1. / static_cast<real>(n)) * (
        2.0* sdKdw1B[n - 1]*ezSq 
        + dFramePreTermsdw3Bw3B[n - 1]*lambda0Sq + dFramePreTermsdw3Bw3B[n - 2]*lambda1Sq));      
    }
    sd2Kdw1A0dw1A0 = SerieMat(dFramePreTermsdw1Aw1A, length);
    sd2Kdw1A0dw1B = SerieMat(dFramePreTermsdw1Aw1B, length);
    sd2Kdw1A0dw2A0 = SerieMat(dFramePreTermsdw1Aw2A, length);
    sd2Kdw1A0dw2B = SerieMat(dFramePreTermsdw1Aw2B, length);
    sd2Kdw1A0dw3A0 = SerieMat(dFramePreTermsdw1Aw3A, length);
    sd2Kdw1A0dw3B = SerieMat(dFramePreTermsdw1Aw3B, length);
    sd2Kdw1Bdw1A0 = SerieMat(dFramePreTermsdw1Aw1B, length);
    sd2Kdw1Bdw1B = SerieMat(dFramePreTermsdw1Bw1B, length);
    sd2Kdw1Bdw2A0 = SerieMat(dFramePreTermsdw1Bw2A, length);
    sd2Kdw1Bdw2B = SerieMat(dFramePreTermsdw1Bw2B, length);
    sd2Kdw1Bdw3A0 = SerieMat(dFramePreTermsdw1Bw3A, length);
    sd2Kdw1Bdw3B = SerieMat(dFramePreTermsdw1Bw3B, length);
    sd2Kdw2A0dw1A0 = SerieMat(dFramePreTermsdw1Aw2A, length); 
    sd2Kdw2A0dw1B = SerieMat(dFramePreTermsdw1Aw1B, length);
    sd2Kdw2A0dw2A0 = SerieMat(dFramePreTermsdw2Aw2A, length);
    sd2Kdw2A0dw2B = SerieMat(dFramePreTermsdw2Aw2B, length);
    sd2Kdw2A0dw3A0 = SerieMat(dFramePreTermsdw2Aw3A, length);
    sd2Kdw2A0dw3B = SerieMat(dFramePreTermsdw2Aw3B, length);
    sd2Kdw2Bdw1A0 = SerieMat(dFramePreTermsdw1Aw2B, length);
    sd2Kdw2Bdw1B = SerieMat(dFramePreTermsdw1Bw2B, length);
    sd2Kdw2Bdw2A0 = SerieMat(dFramePreTermsdw2Aw2B, length);
    sd2Kdw2Bdw2B = SerieMat(dFramePreTermsdw2Bw2B, length);
    sd2Kdw2Bdw3A0 = SerieMat(dFramePreTermsdw2Bw3A, length);
    sd2Kdw2Bdw3B = SerieMat(dFramePreTermsdw2Bw3B, length);
    sd2Kdw3A0dw1A0 = SerieMat(dFramePreTermsdw1Aw3A, length);
    sd2Kdw3A0dw1B = SerieMat(dFramePreTermsdw1Bw3A, length);
    sd2Kdw3A0dw2A0 = SerieMat(dFramePreTermsdw2Aw3A, length);
    sd2Kdw3A0dw2B = SerieMat(dFramePreTermsdw2Bw3A, length);
    sd2Kdw3A0dw3A0 = SerieMat(dFramePreTermsdw3Aw3A, length);
    sd2Kdw3A0dw3B = SerieMat(dFramePreTermsdw3Aw3B, length);
    sd2Kdw3Bdw1A0 = SerieMat(dFramePreTermsdw1Aw3B, length);
    sd2Kdw3Bdw1B = SerieMat(dFramePreTermsdw1Bw3B, length);
    sd2Kdw3Bdw2A0 = SerieMat(dFramePreTermsdw2Aw3B, length);
    sd2Kdw3Bdw2B = SerieMat(dFramePreTermsdw2Bw3B, length);
    sd2Kdw3Bdw3A0 = SerieMat(dFramePreTermsdw3Aw3B, length);
    sd2Kdw3Bdw3B = SerieMat(dFramePreTermsdw3Bw3B, length);
    
    sd2Kdw1Adw1A = sd2Kdw1A0dw1A0 + 2*length*sd2Kdw1A0dw1B + length*length*sd2Kdw1Bdw1B;
    sd2Kdw1Bdw1A = sd2Kdw1Bdw1A0 + length*sd2Kdw1Bdw1B;
    sd2Kdw2Adw1A = sd2Kdw2A0dw1A0 + length*sd2Kdw2A0dw1B + length*sd2Kdw2Bdw1A0 + length*length*sd2Kdw2Bdw1B;
    sd2Kdw2Bdw1A = sd2Kdw2Bdw1A0 + length*sd2Kdw2Bdw1B;
    sd2Kdw3Adw1A = sd2Kdw3A0dw1A0 + length*sd2Kdw3A0dw1B + length*sd2Kdw3Bdw1A0 + length*length*sd2Kdw3Bdw1B;
    sd2Kdw3Bdw1A = sd2Kdw3Bdw1A0 + length*sd2Kdw3Bdw1B;
    sd2Kdw1Adw1B = sd2Kdw1A0dw1B + length*sd2Kdw1Bdw1B;
    sd2Kdw2Adw1B = sd2Kdw2A0dw1B + length*sd2Kdw2Bdw1B;
    sd2Kdw3Adw1B = sd2Kdw3A0dw1B + length*sd2Kdw3Bdw1B;
    sd2Kdw1Adw2A = sd2Kdw1A0dw2A0 + length*sd2Kdw1A0dw2B + length*sd2Kdw1Bdw2A0 + length*length*sd2Kdw1Bdw2B;
    sd2Kdw1Bdw2A = sd2Kdw1Bdw2A0 + length*sd2Kdw1Bdw2B;
    sd2Kdw2Adw2A = sd2Kdw2A0dw2A0 + 2*length*sd2Kdw2A0dw2B + length*length*sd2Kdw2Bdw2B;
    sd2Kdw2Bdw2A = sd2Kdw2Bdw2A0 + length*sd2Kdw2Bdw2B;
    sd2Kdw3Adw2A = sd2Kdw3A0dw2A0 + length*sd2Kdw3A0dw2B + length*sd2Kdw3Bdw2A0 + length*length*sd2Kdw3Bdw2B;
    sd2Kdw3Bdw2A = sd2Kdw3Bdw2A0 + length*sd2Kdw3Bdw2B;
    sd2Kdw1Adw2B = sd2Kdw1A0dw2B + length*sd2Kdw1Bdw2B;
    sd2Kdw2Adw2B = sd2Kdw2A0dw2B + length*sd2Kdw2Bdw2B;
    sd2Kdw3Adw2B = sd2Kdw3A0dw2B + length*sd2Kdw3Bdw2B;
    sd2Kdw1Adw3A = sd2Kdw1A0dw3A0 + length*sd2Kdw1A0dw3B + length*sd2Kdw1Bdw3A0 + length*length*sd2Kdw1Bdw3B;
    sd2Kdw1Bdw3A = sd2Kdw1Bdw3A0 + length*sd2Kdw1Bdw3B;
    sd2Kdw2Adw3A = sd2Kdw2A0dw3A0 + length*sd2Kdw2A0dw3B + length*sd2Kdw2Bdw3A0 + length*length*sd2Kdw2Bdw3B;
    sd2Kdw2Bdw3A = sd2Kdw2Bdw3A0 + length*sd2Kdw2Bdw3B;
    sd2Kdw3Adw3A = sd2Kdw3A0dw3A0 + 2*length*sd2Kdw3A0dw3B + length*length*sd2Kdw3Bdw3B;
    sd2Kdw3Bdw3A = sd2Kdw3Bdw3A0 + length*sd2Kdw3Bdw3B;
    sd2Kdw1Adw3B = sd2Kdw1A0dw3B + length*sd2Kdw1Bdw3B;
    sd2Kdw2Adw3B = sd2Kdw2A0dw3B + length*sd2Kdw2Bdw3B;
    sd2Kdw3Adw3B = sd2Kdw3A0dw3B + length*sd2Kdw3Bdw3B;
  }
  //Derivation of the position
  {
    sd2udw1Adw1A = sd2Kdw1Adw1A.col(2).integrate();
    sd2udw1Bdw1A = sd2Kdw1Bdw1A.col(2).integrate();
    sd2udw2Adw1A = sd2Kdw2Adw1A.col(2).integrate();
    sd2udw2Bdw1A = sd2Kdw2Bdw1A.col(2).integrate();
    sd2udw3Adw1A = sd2Kdw3Adw1A.col(2).integrate();
    sd2udw3Bdw1A = sd2Kdw3Bdw1A.col(2).integrate();
    sd2udw1Adw1B = sd2Kdw1Adw1B.col(2).integrate();
    sd2udw1Bdw1B = sd2Kdw1Bdw1B.col(2).integrate();
    sd2udw2Adw1B = sd2Kdw2Adw1B.col(2).integrate();
    sd2udw2Bdw1B = sd2Kdw2Bdw1B.col(2).integrate();
    sd2udw3Adw1B = sd2Kdw3Adw1B.col(2).integrate();
    sd2udw3Bdw1B = sd2Kdw3Bdw1B.col(2).integrate();
    sd2udw1Adw2A = sd2Kdw1Adw2A.col(2).integrate();
    sd2udw1Bdw2A = sd2Kdw1Bdw2A.col(2).integrate();
    sd2udw2Adw2A = sd2Kdw2Adw2A.col(2).integrate();
    sd2udw2Bdw2A = sd2Kdw2Bdw2A.col(2).integrate();
    sd2udw3Adw2A = sd2Kdw3Adw2A.col(2).integrate();
    sd2udw3Bdw2A = sd2Kdw3Bdw2A.col(2).integrate();
    sd2udw1Adw2B = sd2Kdw1Adw2B.col(2).integrate();
    sd2udw1Bdw2B = sd2Kdw1Bdw2B.col(2).integrate();
    sd2udw2Adw2B = sd2Kdw2Adw2B.col(2).integrate();
    sd2udw2Bdw2B = sd2Kdw2Bdw2B.col(2).integrate();
    sd2udw3Adw2B = sd2Kdw3Adw2B.col(2).integrate();
    sd2udw3Bdw2B = sd2Kdw3Bdw2B.col(2).integrate();
    sd2udw1Adw3A = sd2Kdw1Adw3A.col(2).integrate();
    sd2udw1Bdw3A = sd2Kdw1Bdw3A.col(2).integrate();
    sd2udw2Adw3A = sd2Kdw2Adw3A.col(2).integrate();
    sd2udw2Bdw3A = sd2Kdw2Bdw3A.col(2).integrate();
    sd2udw3Adw3A = sd2Kdw3Adw3A.col(2).integrate();
    sd2udw3Bdw3A = sd2Kdw3Bdw3A.col(2).integrate();
    sd2udw1Adw3B = sd2Kdw1Adw3B.col(2).integrate();
    sd2udw1Bdw3B = sd2Kdw1Bdw3B.col(2).integrate();
    sd2udw2Adw3B = sd2Kdw2Adw3B.col(2).integrate();
    sd2udw2Bdw3B = sd2Kdw2Bdw3B.col(2).integrate();
    sd2udw3Adw3B = sd2Kdw3Adw3B.col(2).integrate();
    sd2udw3Bdw3B = sd2Kdw3Bdw3B.col(2).integrate();
  }
}

void FunctionSuperClothoidPart::computeEnds()
{
  Eigen::Matrix3r K=sK.sum();
  Eigen::Vector3r u=su.sum();
  
  endFrame = R*K;
  endPos = r+R*u;
  
  std::array<Eigen::Matrix3d,6> dKdq{ sdKdw1A.sum(), sdKdw1B.sum(), sdKdw2A.sum(), sdKdw2B.sum(), sdKdw3A.sum(), sdKdw3B.sum()};
  std::array<Eigen::Vector3d,6> dudq{ sdudw1A.sum(), sdudw1B.sum(), sdudw2A.sum(), sdudw2B.sum(), sdudw3A.sum(), sdudw3B.sum()};
  
  for(int i=0;i<6;i++)
    dFramedq[i] = d1Rdq[i]*K+R*dKdq[i];
  for(int i=0;i<6;i++)
    dPosdq[i] = d1rdq[i]+d1Rdq[i]*u+R*dudq[i];
  
  dFramedt = qdot[0]*dFramedq[0] + qdot[1]*dFramedq[1] + qdot[2]*dFramedq[2]
           + qdot[3]*dFramedq[3] + qdot[4]*dFramedq[4] + qdot[5]*dFramedq[5];
  
  dPosdt = qdot[0]*dPosdq[0] + qdot[1]*dPosdq[1] + qdot[2]*dPosdq[2]
        + qdot[3]*dPosdq[3] + qdot[4]*dPosdq[4] + qdot[5]*dPosdq[5];
 
  d2Framedtdqdot=dFramedq;
  d2Posdtdqdot=dPosdq;
  
  Eigen::Matrix<Eigen::Matrix3d,6,6> d2Kdqdq;
  d2Kdqdq<<sd2Kdw1Adw1A.sum(),sd2Kdw1Adw1B.sum(),sd2Kdw1Adw2A.sum(),sd2Kdw1Adw2B.sum(),sd2Kdw1Adw3A.sum(),sd2Kdw1Adw3B.sum(),
           sd2Kdw1Bdw1A.sum(),sd2Kdw1Bdw1B.sum(),sd2Kdw1Bdw2A.sum(),sd2Kdw1Bdw2B.sum(),sd2Kdw1Bdw3A.sum(),sd2Kdw1Bdw3B.sum(),
           sd2Kdw2Adw1A.sum(),sd2Kdw2Adw1B.sum(),sd2Kdw2Adw2A.sum(),sd2Kdw2Adw2B.sum(),sd2Kdw2Adw3A.sum(),sd2Kdw2Adw3B.sum(),
           sd2Kdw2Bdw1A.sum(),sd2Kdw2Bdw1B.sum(),sd2Kdw2Bdw2A.sum(),sd2Kdw2Bdw2B.sum(),sd2Kdw2Bdw3A.sum(),sd2Kdw2Bdw3B.sum(),
           sd2Kdw3Adw1A.sum(),sd2Kdw3Adw1B.sum(),sd2Kdw3Adw2A.sum(),sd2Kdw3Adw2B.sum(),sd2Kdw3Adw3A.sum(),sd2Kdw3Adw3B.sum(),
           sd2Kdw3Bdw1A.sum(),sd2Kdw3Bdw1B.sum(),sd2Kdw3Bdw2A.sum(),sd2Kdw3Bdw2B.sum(),sd2Kdw3Bdw3A.sum(),sd2Kdw3Bdw3B.sum();
  Eigen::Matrix<Eigen::Vector3d,6,6> d2udqdq;
  d2udqdq<<sd2udw1Adw1A.sum(),sd2udw1Adw1B.sum(),sd2udw1Adw2A.sum(),sd2udw1Adw2B.sum(),sd2udw1Adw3A.sum(),sd2udw1Adw3B.sum(),
           sd2udw1Bdw1A.sum(),sd2udw1Bdw1B.sum(),sd2udw1Bdw2A.sum(),sd2udw1Bdw2B.sum(),sd2udw1Bdw3A.sum(),sd2udw1Bdw3B.sum(),
           sd2udw2Adw1A.sum(),sd2udw2Adw1B.sum(),sd2udw2Adw2A.sum(),sd2udw2Adw2B.sum(),sd2udw2Adw3A.sum(),sd2udw2Adw3B.sum(),
           sd2udw2Bdw1A.sum(),sd2udw2Bdw1B.sum(),sd2udw2Bdw2A.sum(),sd2udw2Bdw2B.sum(),sd2udw2Bdw3A.sum(),sd2udw2Bdw3B.sum(),
           sd2udw3Adw1A.sum(),sd2udw3Adw1B.sum(),sd2udw3Adw2A.sum(),sd2udw3Adw2B.sum(),sd2udw3Adw3A.sum(),sd2udw3Adw3B.sum(),
           sd2udw3Bdw1A.sum(),sd2udw3Bdw1B.sum(),sd2udw3Bdw2A.sum(),sd2udw3Bdw2B.sum(),sd2udw3Bdw3A.sum(),sd2udw3Bdw3B.sum();
           
  for(int i=0;i<36;i++)
    d2Framedqdq(i/6,i%6) = d2Rdqdq(i/6,i%6)*K+d1Rdq[i/6]*dKdq[i%6]+d1Rdq[i%6]*dKdq[i/6]+R*d2Kdqdq(i/6,i%6);
  for(int i=0;i<36;i++)
    d2Posdqdq(i/6,i%6) = d2rdqdq(i/6,i%6)+d2Rdqdq(i/6,i%6)*u+d1Rdq[i/6]*dudq[i%6]+d1Rdq[i%6]*dudq[i/6]+R*d2udqdq(i/6,i%6);
  
  for(int i=0;i<6;i++)
    d2Framedtdq[i] = d2Framedqdq(0,i)*qdot[0] + d2Framedqdq(1,i)*qdot[1] + d2Framedqdq(2,i)*qdot[2] 
                   + d2Framedqdq(3,i)*qdot[3] + d2Framedqdq(4,i)*qdot[4] + d2Framedqdq(5,i)*qdot[5];
  for(int i=0;i<6;i++)
    d2Posdtdq[i] = d2Posdqdq(0,i)*qdot[0] + d2Posdqdq(1,i)*qdot[1] + d2Posdqdq(2,i)*qdot[2] 
                   + d2Posdqdq(3,i)*qdot[3] + d2Posdqdq(4,i)*qdot[4] + d2Posdqdq(5,i)*qdot[5];
}

void FunctionSuperClothoidPart::computeEgAndGradEg()
{
  auto  uIsum = su.integrate().sum();
  Eg = areaDensity*width*(r*length+ R*uIsum).dot(G);
  std::array<Eigen::Vector3r, 6> dudqIs = { sdudw1A.integrate().sum(), sdudw1B.integrate().sum(), 
                                            sdudw2A.integrate().sum(), sdudw2B.integrate().sum(),
                                            sdudw3A.integrate().sum(), sdudw3B.integrate().sum()};
  for(int i=0;i<6;i++)gradEg[i] = areaDensity*width*(d1rdq[i]*length+ d1Rdq[i]*uIsum+R*dudqIs[i]).dot(G);
  
    Eigen::Matrix<Eigen::Vector3d,6,6> d2udqdqIs;
  d2udqdqIs<<sd2udw1Adw1A.integrate().sum(),sd2udw1Adw1B.integrate().sum(),sd2udw1Adw2A.integrate().sum(),sd2udw1Adw2B.integrate().sum(),sd2udw1Adw3A.integrate().sum(),sd2udw1Adw3B.integrate().sum(),
            sd2udw1Bdw1A.integrate().sum(),sd2udw1Bdw1B.integrate().sum(),sd2udw1Bdw2A.integrate().sum(),sd2udw1Bdw2B.integrate().sum(),sd2udw1Bdw3A.integrate().sum(),sd2udw1Bdw3B.integrate().sum(),
            sd2udw2Adw1A.integrate().sum(),sd2udw2Adw1B.integrate().sum(),sd2udw2Adw2A.integrate().sum(),sd2udw2Adw2B.integrate().sum(),sd2udw2Adw3A.integrate().sum(),sd2udw2Adw3B.integrate().sum(),
            sd2udw2Bdw1A.integrate().sum(),sd2udw2Bdw1B.integrate().sum(),sd2udw2Bdw2A.integrate().sum(),sd2udw2Bdw2B.integrate().sum(),sd2udw2Bdw3A.integrate().sum(),sd2udw2Bdw3B.integrate().sum(),
            sd2udw3Adw1A.integrate().sum(),sd2udw3Adw1B.integrate().sum(),sd2udw3Adw2A.integrate().sum(),sd2udw3Adw2B.integrate().sum(),sd2udw3Adw3A.integrate().sum(),sd2udw3Adw3B.integrate().sum(),
            sd2udw3Bdw1A.integrate().sum(),sd2udw3Bdw1B.integrate().sum(),sd2udw3Bdw2A.integrate().sum(),sd2udw3Bdw2B.integrate().sum(),sd2udw3Bdw3A.integrate().sum(),sd2udw3Bdw3B.integrate().sum();
  
  for(int i=0;i<6;i++)
    for(int j=0;j<6;j++)
      hessEg(i,j) = areaDensity*width*(d2rdqdq(i,j)*length + d1Rdq[i]*dudqIs[j] + d1Rdq[j]*dudqIs[i] + d2Rdqdq(i,j)*uIsum+R*d2udqdqIs(i,j)).dot(G);
    
//     std::cout<<"areaDensity:"<<areaDensity<<", width:"<<width<<", length:"<<length<<", G:"<<G.transpose()<<std::endl;
//   std::cout<<"check nb terms :"<<sd2udw1Adw1A.size()<<std::endl;
//   for(int i=0;i<6;i++)
//     for(int j=0;j<6;j++)std::cout<<"d2udqdqIs("<<i<<','<<j<<")="<<d2udqdqIs(i,j).transpose()<<std::endl;
}

void FunctionSuperClothoidPart::computeEwAndGradEw()
{
  CElasticEnergy el;
  Eigen::Matrix3d K = CElasticEnergy::ParamsToK(Y,nu,width,thickness);
  el.FixPhysics(K,length);
  el.FixPt(LinearScalar(q[0],w1Btranslated), LinearScalar(q[2],w2Btranslated), LinearScalar(q[4],w3Btranslated),
           LinearScalar(qnat[0],qnat[1]+qnat[0]*soffset), LinearScalar(qnat[2],qnat[3]+qnat[2]*soffset), LinearScalar(qnat[4],qnat[5]+qnat[4]*soffset));
  Ew=el.computeEnergy();
  gradEw=el.computeGrad();
  gradEw[0]+=length*gradEw[1];
  gradEw[2]+=length*gradEw[3];
  gradEw[4]+=length*gradEw[5];
  
  Eigen::Matrix6r partialHessEw = el.computeHessian();
  
  hessEw(0,0) = partialHessEw(0,0) + length*partialHessEw(0,1) + length*partialHessEw(1,0) + length*length*partialHessEw(1,1);
  hessEw(1,0) = partialHessEw(1,0) + length*partialHessEw(1,1);
  hessEw(2,0) = partialHessEw(2,0) + length*partialHessEw(2,1) + length*partialHessEw(3,0) + length*length*partialHessEw(3,1);
  hessEw(3,0) = partialHessEw(3,0) + length*partialHessEw(3,1);
  hessEw(4,0) = partialHessEw(4,0) + length*partialHessEw(4,1) + length*partialHessEw(5,0) + length*length*partialHessEw(5,1);
  hessEw(5,0) = partialHessEw(5,0) + length*partialHessEw(5,1);
  hessEw(0,1) = partialHessEw(0,1) + length*partialHessEw(1,1);
  hessEw(1,1) = partialHessEw(1,1);
  hessEw(2,1) = partialHessEw(2,1) + length*partialHessEw(3,1);
  hessEw(3,1) = partialHessEw(3,1);
  hessEw(4,1) = partialHessEw(4,1) + length*partialHessEw(5,1);
  hessEw(5,1) = partialHessEw(5,1);
  hessEw(0,2) = partialHessEw(0,2) + length*partialHessEw(0,3) + length*partialHessEw(1,2) + length*length*partialHessEw(1,3);
  hessEw(1,2) = partialHessEw(1,2) + length*partialHessEw(1,3);
  hessEw(2,2) = partialHessEw(2,2) + length*partialHessEw(2,3) + length*partialHessEw(3,2) + length*length*partialHessEw(3,3);
  hessEw(3,2) = partialHessEw(3,2) + length*partialHessEw(3,3);
  hessEw(4,2) = partialHessEw(4,2) + length*partialHessEw(4,3) + length*partialHessEw(5,2) + length*length*partialHessEw(5,3);
  hessEw(5,2) = partialHessEw(5,2) + length*partialHessEw(5,3);
  hessEw(0,3) = partialHessEw(0,3) + length*partialHessEw(1,3);
  hessEw(1,3) = partialHessEw(1,3);
  hessEw(2,3) = partialHessEw(2,3) + length*partialHessEw(3,3);
  hessEw(3,3) = partialHessEw(3,3);
  hessEw(4,3) = partialHessEw(4,3) + length*partialHessEw(5,3);
  hessEw(5,3) = partialHessEw(5,3);
  hessEw(0,4) = partialHessEw(0,4) + length*partialHessEw(0,5) + length*partialHessEw(1,4) + length*length*partialHessEw(1,5);
  hessEw(1,4) = partialHessEw(1,4) + length*partialHessEw(1,5);
  hessEw(2,4) = partialHessEw(2,4) + length*partialHessEw(2,5) + length*partialHessEw(3,4) + length*length*partialHessEw(3,5);
  hessEw(3,4) = partialHessEw(3,4) + length*partialHessEw(3,5);
  hessEw(4,4) = partialHessEw(4,4) + length*partialHessEw(4,5) + length*partialHessEw(5,4) + length*length*partialHessEw(5,5);
  hessEw(5,4) = partialHessEw(5,4) + length*partialHessEw(5,5);
  hessEw(0,5) = partialHessEw(0,5) + length*partialHessEw(1,5);
  hessEw(1,5) = partialHessEw(1,5);
  hessEw(2,5) = partialHessEw(2,5) + length*partialHessEw(3,5);
  hessEw(3,5) = partialHessEw(3,5);
  hessEw(4,5) = partialHessEw(4,5) + length*partialHessEw(5,5);
  hessEw(5,5) = partialHessEw(5,5);
}

void FunctionSuperClothoidPart::computeEc()
{
  real t1= length*d1rdt.squaredNorm();
  real t2= 2.0*d1rdt.dot( d1Rdt*su.integrate().sum() );
  
  Eigen::Vector3d tt3=qdot[0]*sdudw1A.integrate().sum()
                    + qdot[1]*sdudw1B.integrate().sum()
                    + qdot[2]*sdudw2A.integrate().sum()
                    + qdot[3]*sdudw2B.integrate().sum()
                    + qdot[4]*sdudw3A.integrate().sum()
                    + qdot[5]*sdudw3B.integrate().sum();
  real t3= 2.0*d1rdt.dot( R*tt3 );
  real tt4=qdot[0]*(d1Rdt*su).dot(R*sdudw1A).integrate().sum()
         + qdot[1]*(d1Rdt*su).dot(R*sdudw1B).integrate().sum()
         + qdot[2]*(d1Rdt*su).dot(R*sdudw2A).integrate().sum()
         + qdot[3]*(d1Rdt*su).dot(R*sdudw2B).integrate().sum()
         + qdot[4]*(d1Rdt*su).dot(R*sdudw3A).integrate().sum()
         + qdot[5]*(d1Rdt*su).dot(R*sdudw3B).integrate().sum();
  real t4=2.*tt4;
  real t5=(d1Rdt*su).dot(d1Rdt*su).integrate().sum();
  std::array<SerieVec*,6> sdudq{&sdudw1A,&sdudw1B,&sdudw2A,&sdudw2B,&sdudw3A,&sdudw3B}; 
  real t6=0.;
  for(int i=0;i<36;i++)
    t6+=qdot[i/6]*(*sdudq[i/6]).dot(*sdudq[i%6]).integrate().sum()*qdot[i%6];
  Ec = 0.5*areaDensity*width*(t1+t2+t3+t4+t5+t6);
}

void FunctionSuperClothoidPart::computed2Ecdqdot2()
{
  Eigen::Matrix6r t1;
  for(int i=0;i<36;i++)
    t1(i/6, i%6) = d2rdtdqdot[i/6].dot(d2rdtdqdot[i%6]);
  t1*=length;
  
  Eigen::Matrix6r t2;
  auto suIsum=su.integrate().sum();
  for(int i=0;i<36;i++)
    t2(i/6, i%6) = 2.0*(d2rdtdqdot[i/6].dot( d2Rdtdqdot[i%6]*suIsum )+d2rdtdqdot[i%6].dot( d2Rdtdqdot[i/6]*suIsum ));
  
  Eigen::Matrix6r t3;
  Eigen::Matrix<Eigen::Vector3d,6,1> sdudqIsum;
  sdudqIsum<<sdudw1A.integrate().sum(), sdudw1B.integrate().sum(),
             sdudw2A.integrate().sum(), sdudw2B.integrate().sum(),
             sdudw3A.integrate().sum(), sdudw3B.integrate().sum();
  for(int i=0;i<36;i++)
    t3(i/6, i%6) = 2.0*(d2rdtdqdot[i/6].dot( R*sdudqIsum[i%6] )+d2rdtdqdot[i%6].dot( R*sdudqIsum[i/6] ));
  
  Eigen::Matrix6r t4;
  std::array<SerieVec,6> d2Rdtdqdotsu{d2Rdtdqdot[0]*su,d2Rdtdqdot[1]*su,d2Rdtdqdot[2]*su,d2Rdtdqdot[3]*su,d2Rdtdqdot[4]*su,d2Rdtdqdot[5]*su};
  std::array<SerieVec*,6> sdudq{&sdudw1A,&sdudw1B,&sdudw2A,&sdudw2B,&sdudw3A,&sdudw3B}; 
  std::array<SerieVec,6> Rsdudq{R*(*sdudq[0]),R*(*sdudq[1]),R*(*sdudq[2]),R*(*sdudq[3]),R*(*sdudq[4]),R*(*sdudq[5])};
  
  for(int i=0;i<36;i++)
    t4(i/6, i%6)=2.0*((d2Rdtdqdotsu[i%6]).dot(Rsdudq[i/6]).integrate().sum()+(d2Rdtdqdotsu[i/6]).dot(Rsdudq[i%6]).integrate().sum());

  Eigen::Matrix6r t5;
  for(int i=0;i<36;i++)
    t5(i/6, i%6)=2.0*(d2Rdtdqdot[i/6]*su).dot(d2Rdtdqdot[i%6]*su).integrate().sum();
  Eigen::Matrix6r t6;
  for(int i=0;i<36;i++)
    t6(i/6, i%6)=2.0*(*sdudq[i/6]).dot(*sdudq[i%6]).integrate().sum();
  d2Ecdqdot2 = 0.5*areaDensity*width*(t1+t2+t3+t4+t5+t6);
}

void FunctionSuperClothoidPart::computeGradEc()
{
  
  Eigen::Vector6r t1;
  for(int i=0;i<6;i++) t1(i) = 2.0*length*d2rdtdq(i).dot(d1rdt);
  
  Eigen::Vector6r t2;
  Eigen::Matrix<Eigen::Vector3d,6,1> sdudqIsum;
  sdudqIsum<<sdudw1A.integrate().sum(), sdudw1B.integrate().sum(),
             sdudw2A.integrate().sum(), sdudw2B.integrate().sum(),
             sdudw3A.integrate().sum(), sdudw3B.integrate().sum();
  auto suIsum = su.integrate().sum();
  for(int i=0;i<6;i++)
    t2(i) = 2.0*(d2rdtdq[i].dot( d1Rdt* suIsum )+d1rdt.dot( d2Rdtdq[i]* suIsum )+d1rdt.dot( d1Rdt* sdudqIsum[i] ));
  
  Eigen::Vector6r t3;
  Eigen::Vector3d dudt=qdot[0]*sdudqIsum[0]
                    + qdot[1]*sdudqIsum[1]
                    + qdot[2]*sdudqIsum[2]
                    + qdot[3]*sdudqIsum[3]
                    + qdot[4]*sdudqIsum[4]
                    + qdot[5]*sdudqIsum[5];
  std::array<Eigen::Vector3d,6> d2udtdq;
  d2udtdq[0]= qdot[0]*sd2udw1Adw1A.integrate().sum()
            + qdot[1]*sd2udw1Bdw1A.integrate().sum()
            + qdot[2]*sd2udw2Adw1A.integrate().sum()
            + qdot[3]*sd2udw2Bdw1A.integrate().sum()
            + qdot[4]*sd2udw3Adw1A.integrate().sum()
            + qdot[5]*sd2udw3Bdw1A.integrate().sum();
  d2udtdq[1]= qdot[0]*sd2udw1Adw1B.integrate().sum()
            + qdot[1]*sd2udw1Bdw1B.integrate().sum()
            + qdot[2]*sd2udw2Adw1B.integrate().sum()
            + qdot[3]*sd2udw2Bdw1B.integrate().sum()
            + qdot[4]*sd2udw3Adw1B.integrate().sum()
            + qdot[5]*sd2udw3Bdw1B.integrate().sum();
  d2udtdq[2]= qdot[0]*sd2udw1Adw2A.integrate().sum()
            + qdot[1]*sd2udw1Bdw2A.integrate().sum()
            + qdot[2]*sd2udw2Adw2A.integrate().sum()
            + qdot[3]*sd2udw2Bdw2A.integrate().sum()
            + qdot[4]*sd2udw3Adw2A.integrate().sum()
            + qdot[5]*sd2udw3Bdw2A.integrate().sum();
  d2udtdq[3]= qdot[0]*sd2udw1Adw2B.integrate().sum()
            + qdot[1]*sd2udw1Bdw2B.integrate().sum()
            + qdot[2]*sd2udw2Adw2B.integrate().sum()
            + qdot[3]*sd2udw2Bdw2B.integrate().sum()
            + qdot[4]*sd2udw3Adw2B.integrate().sum()
            + qdot[5]*sd2udw3Bdw2B.integrate().sum();
  d2udtdq[4]= qdot[0]*sd2udw1Adw3A.integrate().sum()
            + qdot[1]*sd2udw1Bdw3A.integrate().sum()
            + qdot[2]*sd2udw2Adw3A.integrate().sum()
            + qdot[3]*sd2udw2Bdw3A.integrate().sum()
            + qdot[4]*sd2udw3Adw3A.integrate().sum()
            + qdot[5]*sd2udw3Bdw3A.integrate().sum();
  d2udtdq[5]= qdot[0]*sd2udw1Adw3B.integrate().sum()
            + qdot[1]*sd2udw1Bdw3B.integrate().sum()
            + qdot[2]*sd2udw2Adw3B.integrate().sum()
            + qdot[3]*sd2udw2Bdw3B.integrate().sum()
            + qdot[4]*sd2udw3Adw3B.integrate().sum()
            + qdot[5]*sd2udw3Bdw3B.integrate().sum();
  for(int i=0;i<6;i++) t3(i) = 2.0*d2rdtdq[i].dot( R*dudt )+2.0*d1rdt.dot( d1Rdq[i]*dudt )+2.0*d1rdt.dot( R*d2udtdq[i]);
  
  Eigen::Vector6r t4;
  std::array<SerieVec*,6> sdudq{&sdudw1A,&sdudw1B,&sdudw2A,&sdudw2B,&sdudw3A,&sdudw3B}; 
  Eigen::Matrix<SerieVec*,6,6> sd2udqdq;
  sd2udqdq<<&sd2udw1Adw1A,&sd2udw1Adw1B,&sd2udw1Adw2A,&sd2udw1Adw2B,&sd2udw1Adw3A,&sd2udw1Adw3B,
           &sd2udw1Bdw1A,&sd2udw1Bdw1B,&sd2udw1Bdw2A,&sd2udw1Bdw2B,&sd2udw1Bdw3A,&sd2udw1Bdw3B,
           &sd2udw2Adw1A,&sd2udw2Adw1B,&sd2udw2Adw2A,&sd2udw2Adw2B,&sd2udw2Adw3A,&sd2udw2Adw3B,
           &sd2udw2Bdw1A,&sd2udw2Bdw1B,&sd2udw2Bdw2A,&sd2udw2Bdw2B,&sd2udw2Bdw3A,&sd2udw2Bdw3B,
           &sd2udw3Adw1A,&sd2udw3Adw1B,&sd2udw3Adw2A,&sd2udw3Adw2B,&sd2udw3Adw3A,&sd2udw3Adw3B,
           &sd2udw3Bdw1A,&sd2udw3Bdw1B,&sd2udw3Bdw2A,&sd2udw3Bdw2B,&sd2udw3Bdw3A,&sd2udw3Bdw3B;
  
  for(int i=0;i<6;i++) {
  t4(i) = 0 ;
  for(int j=0;j<6;j++)
    t4(i)+=2.*qdot[j]*(d2Rdtdq[i]*su).dot(R*(*sdudq[j])).integrate().sum();
  }
  
  
  Eigen::Vector6r t5;
  for(int i=0;i<6;i++) t5(i)=2.0*(d2Rdtdq[i]*su+d1Rdt*(*sdudq[i])).dot(d1Rdt*su).integrate().sum();
  
  Eigen::Vector6r t6;
  for(int i=0;i<6;i++)
  {
    t6(i)=0;
    for(int j=0;j<6;j++)for(int k=0;k<6;k++)
    t6(i) += 2.*qdot[j]*(*sd2udqdq(j,i)).dot(*sdudq[k]).integrate().sum()*qdot[k];
          
  }
        
  gradEc=0.5*areaDensity*width*(t1+t2+t3+t4+t5+t6);
}

void FunctionSuperClothoidPart::computeSysB()
{
  //dL/dq-d2L/dqdqdot qdot
  Eigen::Vector6r t1;
  for(int i=0;i<6;i++) t1(i) = 2.0*length*d2rdtdq(i).dot(d1rdt);
  
  Eigen::Vector6r t2;
  Eigen::Matrix<Eigen::Vector3d,6,1> sdudqIsum;
  sdudqIsum<<sdudw1A.integrate().sum(), sdudw1B.integrate().sum(),
             sdudw2A.integrate().sum(), sdudw2B.integrate().sum(),
             sdudw3A.integrate().sum(), sdudw3B.integrate().sum();
  auto suIsum = su.integrate().sum();
  for(int i=0;i<6;i++)
    t2(i) = 2.0*(d2rdtdq[i].dot( d1Rdt* suIsum )+d1rdt.dot( d2Rdtdq[i]* suIsum )+d1rdt.dot( d1Rdt* sdudqIsum[i] ));
  
  //term3 is null
  //term4 is null
  
  std::array<SerieVec*,6> sdudq{&sdudw1A,&sdudw1B,&sdudw2A,&sdudw2B,&sdudw3A,&sdudw3B}; 
  Eigen::Matrix<SerieVec*,6,6> sd2udqdq;
  sd2udqdq<<&sd2udw1Adw1A,&sd2udw1Adw1B,&sd2udw1Adw2A,&sd2udw1Adw2B,&sd2udw1Adw3A,&sd2udw1Adw3B,
           &sd2udw1Bdw1A,&sd2udw1Bdw1B,&sd2udw1Bdw2A,&sd2udw1Bdw2B,&sd2udw1Bdw3A,&sd2udw1Bdw3B,
           &sd2udw2Adw1A,&sd2udw2Adw1B,&sd2udw2Adw2A,&sd2udw2Adw2B,&sd2udw2Adw3A,&sd2udw2Adw3B,
           &sd2udw2Bdw1A,&sd2udw2Bdw1B,&sd2udw2Bdw2A,&sd2udw2Bdw2B,&sd2udw2Bdw3A,&sd2udw2Bdw3B,
           &sd2udw3Adw1A,&sd2udw3Adw1B,&sd2udw3Adw2A,&sd2udw3Adw2B,&sd2udw3Adw3A,&sd2udw3Adw3B,
           &sd2udw3Bdw1A,&sd2udw3Bdw1B,&sd2udw3Bdw2A,&sd2udw3Bdw2B,&sd2udw3Bdw3A,&sd2udw3Bdw3B;
  
  Eigen::Vector6r t5;
  for(int i=0;i<6;i++) t5(i)=2.0*(d2Rdtdq[i]*su+d1Rdt*(*sdudq[i])).dot(d1Rdt*su).integrate().sum();
  
  Eigen::Vector6r t6;
  for(int i=0;i<6;i++)
  {
    t6(i)=0;
    for(int j=0;j<6;j++)for(int k=0;k<6;k++)
    t6(i) += -2.*qdot[j]*(*sd2udqdq(j,i)).dot(*sdudq[k]).integrate().sum()*qdot[k];
          
  }
        
  sysB = 0.5*areaDensity*width*(t1+t2+t5+t6)-gradEg-gradEw;
}
