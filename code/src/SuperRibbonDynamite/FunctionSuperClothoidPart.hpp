/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef FUNCTIONSUPERCLOTHOIDPART_H
#define FUNCTIONSUPERCLOTHOIDPART_H

#include <Eigen/Dense>
#include <BaseCore/real.hpp>
#include <Serie.hpp>

class FunctionSuperClothoidPart
{
public:
  FunctionSuperClothoidPart();
  //Parameters
  Eigen::Vector3d G;
  double Y;
  double nu;
  double thickness;
  double areaDensity;
  double soffset;
  double width;

 //In arguments
  Eigen::Vector6r q;
  Eigen::Vector6r qdot;
  Eigen::Vector6r qnat;
  double length;
  
 //In functions
  Eigen::Matrix3d R;
  Eigen::Matrix<Eigen::Matrix3d,6,1> d1Rdq;
  Eigen::Matrix3d d1Rdt;
  Eigen::Matrix<Eigen::Matrix3d,6,1> d2Rdtdq;
  Eigen::Matrix<Eigen::Matrix3d,6,1> d2Rdtdqdot;
  Eigen::Matrix<Eigen::Matrix3d,6,6> d2Rdqdq;
  
  Eigen::Matrix<Eigen::Vector3d,6,1> d1rdq;
  Eigen::Matrix<Eigen::Vector3d,6,1> d2rdtdq;
  Eigen::Matrix<Eigen::Vector3d,6,1> d2rdtdqdot;
  Eigen::Matrix<Eigen::Vector3d,6,6> d2rdqdq;
  Eigen::Vector3d d1rdt;
  Eigen::Vector3d r;

 //Out arguments
  Eigen::Matrix<Eigen::Matrix3d,6,6> d2Framedqdq;
  Eigen::Matrix<Eigen::Matrix3d,6,1> d2Framedtdq;
  Eigen::Matrix<Eigen::Matrix3d,6,1> d2Framedtdqdot;
  Eigen::Matrix<Eigen::Matrix3d,6,1> dFramedq;
  Eigen::Matrix3d dFramedt;//
  Eigen::Matrix3d endFrame;//
  Eigen::Matrix6r d2Ecdqdot2;//
  Eigen::Matrix<Eigen::Vector3d,6,6> d2Posdqdq;
  Eigen::Matrix<Eigen::Vector3d,6,1> d2Posdtdq;
  Eigen::Matrix<Eigen::Vector3d,6,1> d2Posdtdqdot;
  Eigen::Matrix<Eigen::Vector3d,6,1> dPosdq;
  Eigen::Vector3d dPosdt;//
  Eigen::Vector3d endPos;//
  Eigen::Vector6r gradL;//
  Eigen::Vector6r gradEg;//
  Eigen::Vector6r gradEw;//
  Eigen::Vector6r gradEc;//
  Eigen::Matrix6r hessEw;
  Eigen::Matrix6r hessEg;
  double Ec;//
  double Eg;//
  double Ew;//
  double L;//
  
  Eigen::Vector6r sysB;

  void operator()();
  
  
private: 
  void computeSeriePreTerms();
  void computeDerivationsPreTerms();
  void computeEc();
  void computeEgAndGradEg();
  void computeEwAndGradEw();
  void computed2Ecdqdot2();
  void computeDerivationsPreTermsSecondOrder();
  void computeGradEc();
  void computeSysB();
  
  void computeEnds();

  
private:
  
  Eigen::Matrix3r skewOf(Eigen::Vector3r);
  Eigen::Matrix3r lambda0Sq;
  Eigen::Matrix3r lambda1Sq;
  
  double w1Btranslated, w2Btranslated, w3Btranslated;;
  
  SerieMat sK;
  SerieVec su;

  // Derivations
  Eigen::Matrix3r dLambda0dbSq;
  Eigen::Matrix3r dLambda0dmSq;
  Eigen::Matrix3r dLambda1daSq;
  Eigen::Matrix3r dLambda1dbSq;
  Eigen::Matrix3r dLambda1dnSq;
  Eigen::Matrix3r dLambda1dmSq;
  Eigen::Matrix3r dLambda2daSq;
  Eigen::Matrix3r dLambda2dnSq;

  SerieMat sdKdw1A;
  SerieMat sdKdw1B;
  SerieMat sdKdw2A;
  SerieMat sdKdw2B;
  SerieMat sdKdw3A;
  SerieMat sdKdw3B;
  SerieMat sdKdw1A0;
  SerieMat sdKdw2A0;
  SerieMat sdKdw3A0;
  
  SerieVec sdudw1A;
  SerieVec sdudw1B;
  SerieVec sdudw2A;
  SerieVec sdudw2B;
  SerieVec sdudw3A;
  SerieVec sdudw3B;
  
  //Second order derivations
  
  Eigen::Matrix3r dLambda0dbmSq;
  Eigen::Matrix3r dLambda0dmbSq;
  Eigen::Matrix3r dLambda1damSq;
  Eigen::Matrix3r dLambda1dbnSq;
  Eigen::Matrix3r dLambda1dnbSq;
  Eigen::Matrix3r dLambda1dmaSq;
  Eigen::Matrix3r dLambda2danSq;
  Eigen::Matrix3r dLambda2dnaSq;
  
  SerieMat sd2Kdw1A0dw1A0;
  SerieMat sd2Kdw1Bdw1A0;
  SerieMat sd2Kdw2A0dw1A0;
  SerieMat sd2Kdw2Bdw1A0;
  SerieMat sd2Kdw3A0dw1A0;
  SerieMat sd2Kdw3Bdw1A0;
  SerieMat sd2Kdw1A0dw1B;
  SerieMat sd2Kdw2A0dw1B;
  SerieMat sd2Kdw3A0dw1B;
  SerieMat sd2Kdw1A0dw2A0;
  SerieMat sd2Kdw1Bdw2A0;
  SerieMat sd2Kdw2A0dw2A0;
  SerieMat sd2Kdw2Bdw2A0;
  SerieMat sd2Kdw3A0dw2A0;
  SerieMat sd2Kdw3Bdw2A0;
  SerieMat sd2Kdw1A0dw2B;
  SerieMat sd2Kdw2A0dw2B;
  SerieMat sd2Kdw3A0dw2B;
  SerieMat sd2Kdw1A0dw3A0;
  SerieMat sd2Kdw1Bdw3A0;
  SerieMat sd2Kdw2A0dw3A0;
  SerieMat sd2Kdw2Bdw3A0;
  SerieMat sd2Kdw3A0dw3A0;
  SerieMat sd2Kdw3Bdw3A0;
  SerieMat sd2Kdw1A0dw3B;
  SerieMat sd2Kdw2A0dw3B;
  SerieMat sd2Kdw3A0dw3B;
  
  SerieMat sd2Kdw1Adw1A;
  SerieMat sd2Kdw1Bdw1A;
  SerieMat sd2Kdw2Adw1A;
  SerieMat sd2Kdw2Bdw1A;
  SerieMat sd2Kdw3Adw1A;
  SerieMat sd2Kdw3Bdw1A;
  SerieMat sd2Kdw1Adw1B;
  SerieMat sd2Kdw1Bdw1B;
  SerieMat sd2Kdw2Adw1B;
  SerieMat sd2Kdw2Bdw1B;
  SerieMat sd2Kdw3Adw1B;
  SerieMat sd2Kdw3Bdw1B;
  SerieMat sd2Kdw1Adw2A;
  SerieMat sd2Kdw1Bdw2A;
  SerieMat sd2Kdw2Adw2A;
  SerieMat sd2Kdw2Bdw2A;
  SerieMat sd2Kdw3Adw2A;
  SerieMat sd2Kdw3Bdw2A;
  SerieMat sd2Kdw1Adw2B;
  SerieMat sd2Kdw1Bdw2B;
  SerieMat sd2Kdw2Adw2B;
  SerieMat sd2Kdw2Bdw2B;
  SerieMat sd2Kdw3Adw2B;
  SerieMat sd2Kdw3Bdw2B;
  SerieMat sd2Kdw1Adw3A;
  SerieMat sd2Kdw1Bdw3A;
  SerieMat sd2Kdw2Adw3A;
  SerieMat sd2Kdw2Bdw3A;
  SerieMat sd2Kdw3Adw3A;
  SerieMat sd2Kdw3Bdw3A;
  SerieMat sd2Kdw1Adw3B;
  SerieMat sd2Kdw1Bdw3B;
  SerieMat sd2Kdw2Adw3B;
  SerieMat sd2Kdw2Bdw3B;
  SerieMat sd2Kdw3Adw3B;
  SerieMat sd2Kdw3Bdw3B;
  
  SerieVec sd2udw1Adw1A;
  SerieVec sd2udw1Bdw1A;
  SerieVec sd2udw2Adw1A;
  SerieVec sd2udw2Bdw1A;
  SerieVec sd2udw3Adw1A;
  SerieVec sd2udw3Bdw1A;
  SerieVec sd2udw1Adw1B;
  SerieVec sd2udw1Bdw1B;
  SerieVec sd2udw2Adw1B;
  SerieVec sd2udw2Bdw1B;
  SerieVec sd2udw3Adw1B;
  SerieVec sd2udw3Bdw1B;
  SerieVec sd2udw1Adw2A;
  SerieVec sd2udw1Bdw2A;
  SerieVec sd2udw2Adw2A;
  SerieVec sd2udw2Bdw2A;
  SerieVec sd2udw3Adw2A;
  SerieVec sd2udw3Bdw2A;
  SerieVec sd2udw1Adw2B;
  SerieVec sd2udw1Bdw2B;
  SerieVec sd2udw2Adw2B;
  SerieVec sd2udw2Bdw2B;
  SerieVec sd2udw3Adw2B;
  SerieVec sd2udw3Bdw2B;
  SerieVec sd2udw1Adw3A;
  SerieVec sd2udw1Bdw3A;
  SerieVec sd2udw2Adw3A;
  SerieVec sd2udw2Bdw3A;
  SerieVec sd2udw3Adw3A;
  SerieVec sd2udw3Bdw3A;
  SerieVec sd2udw1Adw3B;
  SerieVec sd2udw1Bdw3B;
  SerieVec sd2udw2Adw3B;
  SerieVec sd2udw2Bdw3B;
  SerieVec sd2udw3Adw3B;
  SerieVec sd2udw3Bdw3B;
};

#endif // FUNCTIONSUPERCLOTHOIDPART_H
