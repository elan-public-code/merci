/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "FunctionTigeApprox.hpp"
#include "SuperRibbonCore/ElasticEnergy.hpp"
#include<type_traits>

#define SERIE_NEEDED_ORDER (52 * 3 / 2)

FunctionPartApproxTige::FunctionPartApproxTige():
  sK({},0.), su({},0.),
  sdKda0({},0.), sdKdn0({},0.), sdKda({},0.), sdKdb({},0.), sdKdn({},0.), sdKdm({},0.),
  sduda({},0.), sdudb({},0.), sdudn({},0.), sdudm({},0.),
  
  sd2Kda0da0({},0.), sd2Kdbda0({},0.), sd2Kdn0da0({},0.), sd2Kdmda0({},0.), sd2Kda0db({},0.), sd2Kdn0db({},0.),
  sd2Kda0dn0({},0.), sd2Kdbdn0({},0.), sd2Kdn0dn0({},0.), sd2Kdmdn0({},0.), sd2Kda0dm({},0.), sd2Kdn0dm({},0.),
  sd2Kdada({},0.), sd2Kdbda({},0.), sd2Kdnda({},0.), sd2Kdmda({},0.),
  sd2Kdadb({},0.), sd2Kdbdb({},0.), sd2Kdndb({},0.), sd2Kdmdb({},0.),
  sd2Kdadn({},0.), sd2Kdbdn({},0.), sd2Kdndn({},0.), sd2Kdmdn({},0.),
  sd2Kdadm({},0.), sd2Kdbdm({},0.), sd2Kdndm({},0.), sd2Kdmdm({},0.),
  sd2udada({},0.), sd2udbda({},0.), sd2udnda({},0.), sd2udmda({},0.),
  sd2udadb({},0.), sd2udbdb({},0.), sd2udndb({},0.), sd2udmdb({},0.),
  sd2udadn({},0.), sd2udbdn({},0.), sd2udndn({},0.), sd2udmdn({},0.),
  sd2udadm({},0.), sd2udbdm({},0.), sd2udndm({},0.), sd2udmdm({},0.)
{
}

void FunctionPartApproxTige::operator()()
{
  computeSeriePreTerms();
  computeDerivationsPreTerms();
  computeDerivationsPreTermsSecondOrder();
  computeEc();
  computeEgAndGradEg();
  computeEwAndGradEw();
  computed2Ecdqdot2();
  computeGradEc();
  computeSysB();
  L=Ec-Eg-Ew;
  gradL=gradEc-gradEg-gradEw;
  computeEnds();
}


Eigen::Matrix3r FunctionPartApproxTige::skewOf(Eigen::Vector3r w)
{
  Eigen::Matrix3r res;
  res << 0,    -w[2], w[1],
      w[2],  0,    -w[0],
      -w[1], w[0], 0;
  return res;
}

void FunctionPartApproxTige::computeSeriePreTerms()
{
  btranslated=b+a*soffset; mtranslated=m+n*soffset;
  lambda0Sq = skewOf(Eigen::Vector3r(btranslated,  0.0, mtranslated * btranslated));
  lambda1Sq = skewOf(Eigen::Vector3r(a,  0.0, mtranslated * a + n * btranslated));
  lambda2Sq = skewOf(Eigen::Vector3r(0., 0.0, n * a));
  int order = SERIE_NEEDED_ORDER;
  std::vector<Eigen::Matrix3r> FramePreTerms(order+1);
  FramePreTerms[0]=Eigen::Matrix3r::Identity();
  FramePreTerms[1]=lambda0Sq;
  FramePreTerms[2]=0.5 * (FramePreTerms[1]*lambda0Sq + lambda1Sq);
  for (int n = 3; n <= order; n++) {
    FramePreTerms[n]=(1. / n) *
                            (
                              FramePreTerms[n - 1]*lambda0Sq
                              + FramePreTerms[n - 2]*lambda1Sq
                              + FramePreTerms[n - 3]*lambda2Sq
                            );
  }
  sK=SerieMat(FramePreTerms, length);
  su=sK.col(2).integrate();
}

void FunctionPartApproxTige::computeDerivationsPreTerms()
{
  Eigen::Vector3r dLambda0db;
  Eigen::Vector3r dLambda0dm;
  Eigen::Vector3r dLambda1da;
  Eigen::Vector3r dLambda1db;
  Eigen::Vector3r dLambda1dn;
  Eigen::Vector3r dLambda1dm;
  Eigen::Vector3r dLambda2da;
  Eigen::Vector3r dLambda2dn;
  
  dLambda0db << 1., 0., m;
  dLambda0dm << 0., 0., b;
  dLambda1da << 1., 0., m;
  dLambda1db << 0., 0., n;
  dLambda1dn << 0., 0., b;
  dLambda1dm << 0., 0., a;
  dLambda2da << 0., 0., n;
  dLambda2dn << 0., 0., a;

  dLambda0dbSq = skewOf(dLambda0db);
  dLambda0dmSq = skewOf(dLambda0dm);
  dLambda1daSq = skewOf(dLambda1da);
  dLambda1dbSq = skewOf(dLambda1db);
  dLambda1dnSq = skewOf(dLambda1dn);
  dLambda1dmSq = skewOf(dLambda1dm);
  dLambda2daSq = skewOf(dLambda2da);
  dLambda2dnSq = skewOf(dLambda2dn);

  int order = sK.size();
  assert(order > 0);
  
  std::vector<Eigen::Matrix3r> dFramePreTermsda(order);
  std::vector<Eigen::Matrix3r> dFramePreTermsdb(order);
  std::vector<Eigen::Matrix3r> dFramePreTermsdn(order);
  std::vector<Eigen::Matrix3r> dFramePreTermsdm(order);
  std::vector<Eigen::Vector3r> dPosPreTermsda(order);
  std::vector<Eigen::Vector3r> dPosPreTermsdb(order);
  std::vector<Eigen::Vector3r> dPosPreTermsdn(order);
  std::vector<Eigen::Vector3r> dPosPreTermsdm;
  //Derivation of the frame
  {
    Eigen::Matrix3r zero;
    zero.setZero();
    //order 0
    dFramePreTermsda[0]=zero;
    dFramePreTermsdb[0]=zero;
    dFramePreTermsdn[0]=zero;
    dFramePreTermsdm[0]=zero;
    //order 1
    dFramePreTermsda[1]=zero;
    dFramePreTermsdb[1]=dLambda0dbSq;
    dFramePreTermsdn[1]=zero;
    dFramePreTermsdm[1]=dLambda0dmSq;
    //order 2
    dFramePreTermsda[2]=0.5 * dLambda1daSq;
    dFramePreTermsdb[2]=0.5 * (dLambda0dbSq * lambda0Sq + lambda0Sq * dLambda0dbSq + dLambda1dbSq);
    dFramePreTermsdn[2]=0.5 * dLambda1dnSq;
    dFramePreTermsdm[2]=0.5 * (dLambda0dmSq * lambda0Sq + lambda0Sq * dLambda0dmSq + dLambda1dmSq);
    //order n
    for (int n = 3; n < order; n++) {
      dFramePreTermsda[n]=((1. / static_cast<real>(n)) * (
                                   sK[n - 2]*dLambda1daSq + sK[n - 3]*dLambda2daSq
                                   + dFramePreTermsda[n - 1]*lambda0Sq + dFramePreTermsda[n - 2]*lambda1Sq + dFramePreTermsda[n - 3]*lambda2Sq));
      dFramePreTermsdb[n]=((1. / static_cast<real>(n)) * (
                                   sK[n - 1]*dLambda0dbSq + sK[n - 2]*dLambda1dbSq
                                   + dFramePreTermsdb[n - 1]*lambda0Sq + dFramePreTermsdb[n - 2]*lambda1Sq + dFramePreTermsdb[n - 3]*lambda2Sq));
      dFramePreTermsdn[n]=((1. / static_cast<real>(n)) * (
                                   sK[n - 2]*dLambda1dnSq + sK[n - 3]*dLambda2dnSq
                                   + dFramePreTermsdn[n - 1]*lambda0Sq + dFramePreTermsdn[n - 2]*lambda1Sq + dFramePreTermsdn[n - 3]*lambda2Sq));
      dFramePreTermsdm[n]=((1. / static_cast<real>(n)) * (
                                   sK[n - 1]*dLambda0dmSq + sK[n - 2]*dLambda1dmSq
                                   + dFramePreTermsdm[n - 1]*lambda0Sq + dFramePreTermsdm[n - 2]*lambda1Sq + dFramePreTermsdm[n - 3]*lambda2Sq));
    }
    sdKda0 = SerieMat(dFramePreTermsda, length);
    sdKdb = SerieMat(dFramePreTermsdb, length);
    sdKdn0 = SerieMat(dFramePreTermsdn, length);
    sdKdm = SerieMat(dFramePreTermsdm, length);
    sdKda = sdKda0 + length*sdKdb;
    sdKdn = sdKdn0 + length*sdKdm;
  }

  //Derivation of the position
  {
    sduda = sdKda.col(2).integrate();
    sdudb = sdKdb.col(2).integrate();
    sdudn = sdKdn.col(2).integrate();
    sdudm = sdKdm.col(2).integrate();
  }
}

void FunctionPartApproxTige::computeDerivationsPreTermsSecondOrder()
{
  Eigen::Vector3r dLambda0dbm;
  Eigen::Vector3r dLambda0dmb;
  Eigen::Vector3r dLambda1dam;
  Eigen::Vector3r dLambda1dbn;
  Eigen::Vector3r dLambda1dnb;
  Eigen::Vector3r dLambda1dma;
  Eigen::Vector3r dLambda2dan;
  Eigen::Vector3r dLambda2dna;
  dLambda0dbm << 0., 0., 1.;
  dLambda0dmb << 0., 0., 1.;
  dLambda1dam << 0., 0., 1.;
  dLambda1dbn << 0., 0., 1.;
  dLambda1dnb << 0., 0., 1.;
  dLambda1dma << 0., 0., 1.;
  dLambda2dan << 0., 0., 1.;
  dLambda2dna << 0., 0., 1.;
  dLambda0dbmSq = skewOf(dLambda0dbm);
  dLambda0dmbSq = skewOf(dLambda0dmb);
  dLambda1damSq = skewOf(dLambda1dam);
  dLambda1dbnSq = skewOf(dLambda1dbn);
  dLambda1dnbSq = skewOf(dLambda1dnb);
  dLambda1dmaSq = skewOf(dLambda1dma);
  dLambda2danSq = skewOf(dLambda2dan);
  dLambda2dnaSq = skewOf(dLambda2dna);

  int order = sK.size();
  assert(order > 0);

  //Derivation of the frame
  {
    std::vector<Eigen::Matrix3r> dFramePreTermsdaa(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdba(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdna(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdma(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdab(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdbb(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdnb(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdmb(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdan(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdbn(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdnn(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdmn(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdam(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdbm(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdnm(order);
    std::vector<Eigen::Matrix3r> dFramePreTermsdmm(order);
    Eigen::Matrix3r zero;
    zero.setZero();
    //order 0
    dFramePreTermsdaa[0]=zero;
    dFramePreTermsdba[0]=zero;
    dFramePreTermsdna[0]=zero;
    dFramePreTermsdma[0]=zero;
    dFramePreTermsdab[0]=zero;
    dFramePreTermsdbb[0]=zero;
    dFramePreTermsdnb[0]=zero;
    dFramePreTermsdmb[0]=zero;
    dFramePreTermsdan[0]=zero;
    dFramePreTermsdbn[0]=zero;
    dFramePreTermsdnn[0]=zero;
    dFramePreTermsdmn[0]=zero;
    dFramePreTermsdam[0]=zero;
    dFramePreTermsdbm[0]=zero;
    dFramePreTermsdnm[0]=zero;
    dFramePreTermsdmm[0]=zero;
    //order 1
    dFramePreTermsdaa[1]=zero;
    dFramePreTermsdab[1]=zero;
    dFramePreTermsdan[1]=zero;
    dFramePreTermsdam[1]=zero;
    dFramePreTermsdba[1]=zero;
    dFramePreTermsdbb[1]=zero;
    dFramePreTermsdbn[1]=zero;
    dFramePreTermsdbm[1]=dLambda0dbmSq;
    dFramePreTermsdna[1]=zero;
    dFramePreTermsdnb[1]=zero;
    dFramePreTermsdnn[1]=zero;
    dFramePreTermsdnm[1]=zero;
    dFramePreTermsdma[1]=zero;
    dFramePreTermsdmb[1]=dLambda0dmbSq;
    dFramePreTermsdmn[1]=zero;
    dFramePreTermsdmm[1]=zero;
    //order 2
    dFramePreTermsdaa[2]=zero;
    dFramePreTermsdab[2]=zero;
    dFramePreTermsdan[2]=zero;
    dFramePreTermsdam[2]=0.5 * dLambda1damSq;
    dFramePreTermsdba[2]=zero;
    dFramePreTermsdbb[2]=dLambda0dbSq * dLambda0dbSq;
    dFramePreTermsdbn[2]=0.5 * dLambda1dbnSq;
    dFramePreTermsdbm[2]=0.5 * (dLambda0dbmSq * lambda0Sq + lambda0Sq * dLambda0dbmSq + dLambda0dbSq * dLambda0dmSq + dLambda0dmSq * dLambda0dbSq);
    dFramePreTermsdna[2]=zero;
    dFramePreTermsdnb[2]=0.5 * dLambda1dnbSq;
    dFramePreTermsdnn[2]=zero;
    dFramePreTermsdnm[2]=zero;
    dFramePreTermsdma[2]=0.5 * dLambda1dmaSq;
    dFramePreTermsdmb[2]=0.5 * (dLambda0dbmSq * lambda0Sq + lambda0Sq * dLambda0dbmSq + dLambda0dbSq * dLambda0dmSq + dLambda0dmSq * dLambda0dbSq);
    dFramePreTermsdmn[2]=zero;
    dFramePreTermsdmm[2]=dLambda0dmSq * dLambda0dmSq;
    //order n
    for (int n = 3; n < order; n++) {
      dFramePreTermsdaa[n]=((1. / static_cast<real>(n)) * (
                                    sdKda0[n - 2]*dLambda1daSq + sdKda0[n - 3]*dLambda2daSq
                                    + dFramePreTermsdaa[n - 1]*lambda0Sq + dFramePreTermsdaa[n - 2]*lambda1Sq + dFramePreTermsdaa[n - 3]*lambda2Sq
                                    + sdKda0[n - 2]*dLambda1daSq + sdKda0[n - 3]*dLambda2daSq));
      dFramePreTermsdab[n]=((1. / static_cast<real>(n)) * (
                                    sdKdb[n - 2]*dLambda1daSq + sdKdb[n - 3]*dLambda2daSq
                                    + sdKda0[n - 1]*dLambda0dbSq + sdKda0[n - 2]*dLambda1dbSq
                                    + dFramePreTermsdab[n - 1]*lambda0Sq + dFramePreTermsdab[n - 2]*lambda1Sq + dFramePreTermsdab[n - 3]*lambda2Sq));
      dFramePreTermsdan[n]=((1. / static_cast<real>(n)) * (
                                    sK[n - 3]*dLambda2danSq
                                    + sdKdn0[n - 2]*dLambda1daSq + sdKdn0[n - 3]*dLambda2daSq
                                    + sdKda0[n - 2]*dLambda1dnSq + sdKda0[n - 3]*dLambda2dnSq
                                    + dFramePreTermsdan[n - 1]*lambda0Sq + dFramePreTermsdan[n - 2]*lambda1Sq + dFramePreTermsdan[n - 3]*lambda2Sq));
      dFramePreTermsdam[n]=((1. / static_cast<real>(n)) * (
                                    sK[n - 2]*dLambda1damSq
                                    + sdKdm[n - 2]*dLambda1daSq + sdKdm[n - 3]*dLambda2daSq
                                    + sdKda0[n - 1]*dLambda0dmSq + sdKda0[n - 2]*dLambda1dmSq
                                    + dFramePreTermsdam[n - 1]*lambda0Sq + dFramePreTermsdam[n - 2]*lambda1Sq + dFramePreTermsdam[n - 3]*lambda2Sq));

      dFramePreTermsdba[n]=((1. / static_cast<real>(n)) * (
                                    sdKda0[n - 1]*dLambda0dbSq + sdKda0[n - 2]*dLambda1dbSq
                                    + dFramePreTermsdba[n - 1]*lambda0Sq + dFramePreTermsdba[n - 2]*lambda1Sq + dFramePreTermsdba[n - 3]*lambda2Sq
                                    + sdKdb[n - 2]*dLambda1daSq + sdKdb[n - 3]*dLambda2daSq));
      dFramePreTermsdbb[n]=((1. / static_cast<real>(n)) * (
                                    sdKdb[n - 1]*dLambda0dbSq + sdKdb[n - 2]*dLambda1dbSq
                                    + dFramePreTermsdbb[n - 1]*lambda0Sq + dFramePreTermsdbb[n - 2]*lambda1Sq + dFramePreTermsdbb[n - 3]*lambda2Sq
                                    + sdKdb[n - 1]*dLambda0dbSq + sdKdb[n - 2]*dLambda1dbSq));
      dFramePreTermsdbn[n]=((1. / static_cast<real>(n)) * (
                                    sK[n - 2]*dLambda1dbnSq
                                    + sdKdn0[n - 1]*dLambda0dbSq + sdKdn0[n - 2]*dLambda1dbSq
                                    + dFramePreTermsdbn[n - 1]*lambda0Sq + dFramePreTermsdbn[n - 2]*lambda1Sq + dFramePreTermsdbn[n - 3]*lambda2Sq
                                    + sdKdb[n - 2]*dLambda1dnSq + sdKdb[n - 3]*dLambda2dnSq));
      dFramePreTermsdbm[n]=((1. / static_cast<real>(n)) * (
                                    sK[n - 1]*dLambda0dbmSq
                                    + sdKdm[n - 1]*dLambda0dbSq + sdKdm[n - 2]*dLambda1dbSq
                                    + dFramePreTermsdbm[n - 1]*lambda0Sq + dFramePreTermsdbm[n - 2]*lambda1Sq + dFramePreTermsdbm[n - 3]*lambda2Sq
                                    + sdKdb[n - 1]*dLambda0dmSq + sdKdb[n - 2]*dLambda1dmSq));

      dFramePreTermsdna[n]=((1. / static_cast<real>(n)) * (
                                    sK[n - 3]*dLambda2dnaSq
                                    + sdKda0[n - 2]*dLambda1dnSq + sdKda0[n - 3]*dLambda2dnSq
                                    + dFramePreTermsdna[n - 1]*lambda0Sq + dFramePreTermsdna[n - 2]*lambda1Sq + dFramePreTermsdna[n - 3]*lambda2Sq
                                    + sdKdn0[n - 2]*dLambda1daSq + sdKdn0[n - 3]*dLambda2daSq));
      dFramePreTermsdnb[n]=((1. / static_cast<real>(n)) * (
                                    sK[n - 2]*dLambda1dnbSq
                                    + sdKdb[n - 2]*dLambda1dnSq + sdKdb[n - 3]*dLambda2dnSq
                                    + dFramePreTermsdnb[n - 1]*lambda0Sq + dFramePreTermsdnb[n - 2]*lambda1Sq + dFramePreTermsdnb[n - 3]*lambda2Sq
                                    + sdKdn0[n - 1]*dLambda0dbSq + sdKdn0[n - 2]*dLambda1dbSq));
      dFramePreTermsdnn[n]=((1. / static_cast<real>(n)) * (
                                    sdKdn0[n - 2]*dLambda1dnSq + sdKdn0[n - 3]*dLambda2dnSq
                                    + dFramePreTermsdnn[n - 1]*lambda0Sq + dFramePreTermsdnn[n - 2]*lambda1Sq + dFramePreTermsdnn[n - 3]*lambda2Sq
                                    + sdKdn0[n - 2]*dLambda1dnSq + sdKdn0[n - 3]*dLambda2dnSq));
      dFramePreTermsdnm[n]=((1. / static_cast<real>(n)) * (
                                    sdKdm[n - 2]*dLambda1dnSq + sdKdm[n - 3]*dLambda2dnSq
                                    + dFramePreTermsdnm[n - 1]*lambda0Sq + dFramePreTermsdnm[n - 2]*lambda1Sq + dFramePreTermsdnm[n - 3]*lambda2Sq
                                    + sdKdn0[n - 1]*dLambda0dmSq + sdKdn0[n - 2]*dLambda1dmSq));

      dFramePreTermsdma[n]=((1. / static_cast<real>(n)) * (
                                    sK[n - 2]*dLambda1dmaSq
                                    + sdKda0[n - 1]*dLambda0dmSq + sdKda0[n - 2]*dLambda1dmSq
                                    + dFramePreTermsdma[n - 1]*lambda0Sq + dFramePreTermsdma[n - 2]*lambda1Sq + dFramePreTermsdma[n - 3]*lambda2Sq
                                    + sdKdm[n - 2]*dLambda1daSq + sdKdm[n - 3]*dLambda2daSq));
      dFramePreTermsdmb[n]=((1. / static_cast<real>(n)) * (
                                    sK[n - 1]*dLambda0dmbSq
                                    + sdKdb[n - 1]*dLambda0dmSq + sdKdb[n - 2]*dLambda1dmSq
                                    + dFramePreTermsdmb[n - 1]*lambda0Sq + dFramePreTermsdmb[n - 2]*lambda1Sq + dFramePreTermsdmb[n - 3]*lambda2Sq
                                    + sdKdm[n - 1]*dLambda0dbSq + sdKdm[n - 2]*dLambda1dbSq));
      dFramePreTermsdmn[n]=((1. / static_cast<real>(n)) * (
                                    sdKdn0[n - 1]*dLambda0dmSq + sdKdn0[n - 2]*dLambda1dmSq
                                    + dFramePreTermsdmn[n - 1]*lambda0Sq + dFramePreTermsdmn[n - 2]*lambda1Sq + dFramePreTermsdmn[n - 3]*lambda2Sq
                                    + sdKdm[n - 2]*dLambda1dnSq + sdKdm[n - 3]*dLambda2dnSq));
      dFramePreTermsdmm[n]=((1. / static_cast<real>(n)) * (
                                    sdKdm[n - 1]*dLambda0dmSq + sdKdm[n - 2]*dLambda1dmSq
                                    + dFramePreTermsdmm[n - 1]*lambda0Sq + dFramePreTermsdmm[n - 2]*lambda1Sq + dFramePreTermsdmm[n - 3]*lambda2Sq
                                    + sdKdm[n - 1]*dLambda0dmSq + sdKdm[n - 2]*dLambda1dmSq));
    }
    sd2Kda0da0 = SerieMat(dFramePreTermsdaa, length);
    sd2Kda0db = SerieMat(dFramePreTermsdab, length);
    sd2Kda0dn0 = SerieMat(dFramePreTermsdan, length);
    sd2Kda0dm = SerieMat(dFramePreTermsdam, length);
    sd2Kdbda0 = SerieMat(dFramePreTermsdba, length);
    sd2Kdbdb = SerieMat(dFramePreTermsdbb, length);
    sd2Kdbdn0 = SerieMat(dFramePreTermsdbn, length);
    sd2Kdbdm = SerieMat(dFramePreTermsdbm, length);
    sd2Kdn0da0 = SerieMat(dFramePreTermsdna, length);
    sd2Kdn0db = SerieMat(dFramePreTermsdnb, length);
    sd2Kdn0dn0 = SerieMat(dFramePreTermsdnn, length);
    sd2Kdn0dm = SerieMat(dFramePreTermsdnm, length);
    sd2Kdmda0 = SerieMat(dFramePreTermsdma, length);
    sd2Kdmdb = SerieMat(dFramePreTermsdmb, length);
    sd2Kdmdn0 = SerieMat(dFramePreTermsdmn, length);
    sd2Kdmdm = SerieMat(dFramePreTermsdmm, length);
    
    sd2Kdada = sd2Kda0da0+2*length*sd2Kda0db+length*length*sd2Kdbdb;
    sd2Kdadb = sd2Kda0db+length*sd2Kdbdb;
    sd2Kdadn = sd2Kda0dn0 + length*sd2Kda0dm + length*sd2Kdbdn0 + length*length*sd2Kdbdm;
    sd2Kdadm = sd2Kda0dm+length*sd2Kdbdm;
    sd2Kdbda = sd2Kdbda0 + length*sd2Kdbdb;
    sd2Kdbdn = sd2Kdbdn0 + length*sd2Kdbdm;
    sd2Kdnda = sd2Kdn0da0 + length*sd2Kdmda0 + length*sd2Kdn0db + length*length*sd2Kdmdb;
    sd2Kdndb = sd2Kdn0db + length*sd2Kdmdb;
    sd2Kdndn = sd2Kdn0dn0+2*length*sd2Kdn0dm+length*length*sd2Kdmdm;
    sd2Kdndm = sd2Kdn0dm+length*sd2Kdmdm;
    sd2Kdmda = sd2Kdmda0+length*sd2Kdmdb;
    sd2Kdmdn = sd2Kdmdn0+length*sd2Kdmdm;
  }

  //Derivation of the position
  { 
    sd2udada=sd2Kdada.col(2).integrate();
    sd2udadb=sd2Kdadb.col(2).integrate();
    sd2udadn=sd2Kdadn.col(2).integrate();
    sd2udadm=sd2Kdadm.col(2).integrate();
    sd2udbda=sd2Kdbda.col(2).integrate();
    sd2udbdb=sd2Kdbdb.col(2).integrate();
    sd2udbdn=sd2Kdbdn.col(2).integrate();
    sd2udbdm=sd2Kdbdm.col(2).integrate();
    sd2udnda=sd2Kdnda.col(2).integrate();
    sd2udndb=sd2Kdndb.col(2).integrate();
    sd2udndn=sd2Kdndn.col(2).integrate();
    sd2udndm=sd2Kdndm.col(2).integrate();
    sd2udmda=sd2Kdmda.col(2).integrate();
    sd2udmdb=sd2Kdmdb.col(2).integrate();
    sd2udmdn=sd2Kdmdn.col(2).integrate();
    sd2udmdm=sd2Kdmdm.col(2).integrate();
  }
}

void FunctionPartApproxTige::computeEnds()
{
  Eigen::Matrix3r K=sK.sum();
  Eigen::Vector3r u=su.sum();
  
  endFrame = R*K;
  endPos = r+R*u;
  
  auto dKda=sdKda.sum();
  auto dKdb=sdKdb.sum();
  auto dKdn=sdKdn.sum();
  auto dKdm=sdKdm.sum();
  auto duda=sduda.sum();
  auto dudb=sdudb.sum();
  auto dudn=sdudn.sum();
  auto dudm=sdudm.sum();
  
  dFrameda = d1Rda*K+R*dKda;
  dFramedb = d1Rdb*K+R*dKdb;
  dFramedn = d1Rdn*K+R*dKdn;
  dFramedm = d1Rdm*K+R*dKdm;
  dPosda = d1rda+d1Rda*u+R*duda;
  dPosdb = d1rdb+d1Rdb*u+R*dudb;
  dPosdn = d1rdn+d1Rdn*u+R*dudn;
  dPosdm = d1rdm+d1Rdm*u+R*dudm;
  
  dFramedt = dFrameda*adot + dFramedb*bdot + dFramedn*ndot + dFramedm*mdot;
  dPosdt = dPosda*adot + dPosdb*bdot + dPosdn*ndot + dPosdm*mdot;
 
  d2Framedtdadot=dFrameda;
  d2Framedtdbdot=dFramedb;
  d2Framedtdndot=dFramedn;
  d2Framedtdmdot=dFramedm;
  d2Posdtdadot=dPosda;
  d2Posdtdbdot=dPosdb;
  d2Posdtdndot=dPosdn;
  d2Posdtdmdot=dPosdm;
  
  d2Framedada = d2Rdada*K+d1Rda*dKda+d1Rda*dKda+R*sd2Kdada.sum();
  d2Framedadb = d2Rdadb*K+d1Rda*dKdb+d1Rdb*dKda+R*sd2Kdadb.sum();
  d2Framedadn = d2Rdadn*K+d1Rda*dKdn+d1Rdn*dKda+R*sd2Kdadn.sum();
  d2Framedadm = d2Rdadm*K+d1Rda*dKdm+d1Rdm*dKda+R*sd2Kdadm.sum();
  d2Framedbda = d2Rdbda*K+d1Rdb*dKda+d1Rda*dKdb+R*sd2Kdbda.sum();
  d2Framedbdb = d2Rdbdb*K+d1Rdb*dKdb+d1Rdb*dKdb+R*sd2Kdbdb.sum();
  d2Framedbdn = d2Rdbdn*K+d1Rdb*dKdn+d1Rdn*dKdb+R*sd2Kdbdn.sum();
  d2Framedbdm = d2Rdbdm*K+d1Rdb*dKdm+d1Rdm*dKdb+R*sd2Kdbdm.sum();
  d2Framednda = d2Rdnda*K+d1Rdn*dKda+d1Rda*dKdn+R*sd2Kdnda.sum();
  d2Framedndb = d2Rdndb*K+d1Rdn*dKdb+d1Rdb*dKdn+R*sd2Kdndb.sum();
  d2Framedndn = d2Rdndn*K+d1Rdn*dKdn+d1Rdn*dKdn+R*sd2Kdndn.sum();
  d2Framedndm = d2Rdndm*K+d1Rdn*dKdm+d1Rdm*dKdn+R*sd2Kdndm.sum();
  d2Framedmda = d2Rdmda*K+d1Rdm*dKda+d1Rda*dKdm+R*sd2Kdmda.sum();
  d2Framedmdb = d2Rdmdb*K+d1Rdm*dKdb+d1Rdb*dKdm+R*sd2Kdmdb.sum();
  d2Framedmdn = d2Rdmdn*K+d1Rdm*dKdn+d1Rdn*dKdm+R*sd2Kdmdn.sum();
  d2Framedmdm = d2Rdmdm*K+d1Rdm*dKdm+d1Rdm*dKdm+R*sd2Kdmdm.sum();
  
  d2Posdada = d2rdada+d2Rdada*u+d1Rda*duda+d1Rda*duda+R*sd2udada.sum();
  d2Posdadb = d2rdadb+d2Rdadb*u+d1Rda*dudb+d1Rdb*duda+R*sd2udadb.sum();
  d2Posdadn = d2rdadn+d2Rdadn*u+d1Rda*dudn+d1Rdn*duda+R*sd2udadn.sum();
  d2Posdadm = d2rdadm+d2Rdadm*u+d1Rda*dudm+d1Rdm*duda+R*sd2udadm.sum();
  d2Posdbda = d2rdbda+d2Rdbda*u+d1Rdb*duda+d1Rda*dudb+R*sd2udbda.sum();
  d2Posdbdb = d2rdbdb+d2Rdbdb*u+d1Rdb*dudb+d1Rdb*dudb+R*sd2udbdb.sum();
  d2Posdbdn = d2rdbdn+d2Rdbdn*u+d1Rdb*dudn+d1Rdn*dudb+R*sd2udbdn.sum();
  d2Posdbdm = d2rdbdm+d2Rdbdm*u+d1Rdb*dudm+d1Rdm*dudb+R*sd2udbdm.sum();
  d2Posdnda = d2rdnda+d2Rdnda*u+d1Rdn*duda+d1Rda*dudn+R*sd2udnda.sum();
  d2Posdndb = d2rdndb+d2Rdndb*u+d1Rdn*dudb+d1Rdb*dudn+R*sd2udndb.sum();
  d2Posdndn = d2rdndn+d2Rdndn*u+d1Rdn*dudn+d1Rdn*dudn+R*sd2udndn.sum();
  d2Posdndm = d2rdndm+d2Rdndm*u+d1Rdn*dudm+d1Rdm*dudn+R*sd2udndm.sum();
  d2Posdmda = d2rdmda+d2Rdmda*u+d1Rdm*duda+d1Rda*dudm+R*sd2udmda.sum();
  d2Posdmdb = d2rdmdb+d2Rdmdb*u+d1Rdm*dudb+d1Rdb*dudm+R*sd2udmdb.sum();
  d2Posdmdn = d2rdmdn+d2Rdmdn*u+d1Rdm*dudn+d1Rdn*dudm+R*sd2udmdn.sum();
  d2Posdmdm = d2rdmdm+d2Rdmdm*u+d1Rdm*dudm+d1Rdm*dudm+R*sd2udmdm.sum();
  
  d2Framedtda=d2Framedada*adot + d2Framedbda*bdot + d2Framednda*ndot + d2Framedmda*mdot;
  d2Framedtdb=d2Framedadb*adot + d2Framedbdb*bdot + d2Framedndb*ndot + d2Framedmdb*mdot;
  d2Framedtdn=d2Framedadn*adot + d2Framedbdn*bdot + d2Framedndn*ndot + d2Framedmdn*mdot;
  d2Framedtdm=d2Framedadm*adot + d2Framedbdm*bdot + d2Framedndm*ndot + d2Framedmdm*mdot;
  
  d2Posdtda=d2Posdada*adot + d2Posdbda*bdot + d2Posdnda*ndot + d2Posdmda*mdot;
  d2Posdtdb=d2Posdadb*adot + d2Posdbdb*bdot + d2Posdndb*ndot + d2Posdmdb*mdot;
  d2Posdtdn=d2Posdadn*adot + d2Posdbdn*bdot + d2Posdndn*ndot + d2Posdmdn*mdot;
  d2Posdtdm=d2Posdadm*adot + d2Posdbdm*bdot + d2Posdndm*ndot + d2Posdmdm*mdot;
}

void FunctionPartApproxTige::computeEgAndGradEg()
{
  auto  uIsum = su.integrate().sum();
  Eg = areaDensity*width*(r*length+ R*uIsum).dot(G);
  gradEg(0) = areaDensity*width*(d1rda*length+ d1Rda*uIsum+R*sduda.integrate().sum()).dot(G);
  gradEg(1) = areaDensity*width*(d1rdb*length+ d1Rdb*uIsum+R*sdudb.integrate().sum()).dot(G);
  gradEg(2) = areaDensity*width*(d1rdn*length+ d1Rdn*uIsum+R*sdudn.integrate().sum()).dot(G);
  gradEg(3) = areaDensity*width*(d1rdm*length+ d1Rdm*uIsum+R*sdudm.integrate().sum()).dot(G);
}

void FunctionPartApproxTige::computeEwAndGradEw()
{
  ElasticEnergy::YesIWantToUseSadowsky(true);
  ElasticEnergy el;
  el.FixPhysics(D,width,length,nu);
  el.FixPt(LinearScalar(a,btranslated),LinearScalar(n,mtranslated),LinearScalar(anat,bnat+anat*soffset));
  Ew=el.computeEnergy();
  gradEw=el.computeGrad();
  gradEw[0]+=length*gradEw[1];
  gradEw[2]+=length*gradEw[3];
}

void FunctionPartApproxTige::computeEc()
{
  real t1= length*d1rdt.squaredNorm();
  real t2= 2.0*d1rdt.dot( d1Rdt*su.integrate().sum() );
  
  Eigen::Vector3d tt3=adot*sduda.integrate().sum()
                    + bdot*sdudb.integrate().sum()
                    + ndot*sdudn.integrate().sum()
                    + mdot*sdudm.integrate().sum();
  real t3= 2.0*d1rdt.dot( R*tt3 );
  real tt4=adot*(d1Rdt*su).dot(R*sduda).integrate().sum()
         + bdot*(d1Rdt*su).dot(R*sdudb).integrate().sum()
         + ndot*(d1Rdt*su).dot(R*sdudn).integrate().sum()
         + mdot*(d1Rdt*su).dot(R*sdudm).integrate().sum();
  real t4=2.*tt4;
  real t5=(d1Rdt*su).dot(d1Rdt*su).integrate().sum();
  real term6aa = adot*sduda.dot(sduda).integrate().sum()*adot;
  real term6ba = bdot*sdudb.dot(sduda).integrate().sum()*adot;
  real term6na = ndot*sdudn.dot(sduda).integrate().sum()*adot;
  real term6ma = mdot*sdudm.dot(sduda).integrate().sum()*adot;
  real term6ab = term6ba;
  real term6bb = bdot*sdudb.dot(sdudb).integrate().sum()*bdot;
  real term6nb = ndot*sdudn.dot(sdudb).integrate().sum()*bdot;
  real term6mb = mdot*sdudm.dot(sdudb).integrate().sum()*bdot;
  real term6an = term6na;
  real term6bn = term6nb;
  real term6nn = ndot*sdudn.dot(sdudn).integrate().sum()*ndot;
  real term6mn = mdot*sdudm.dot(sdudn).integrate().sum()*ndot;
  real term6am = term6ma;
  real term6bm = term6mb;
  real term6nm = term6mn;
  real term6mm = mdot*sdudm.dot(sdudm).integrate().sum()*mdot;
  real t6=term6aa + term6ab + term6an + term6am
        + term6ba + term6bb + term6bn + term6bm
        + term6na + term6nb + term6nn + term6nm
        + term6ma + term6mb + term6mn + term6mm;
  Ec = 0.5*areaDensity*width*(t1+t2+t3+t4+t5+t6);
}

void FunctionPartApproxTige::computed2Ecdqdot2()
{
  Eigen::Matrix4r t1;
  t1(0,0) = d2rdtdadot.dot(d2rdtdadot);
  t1(0,1) = d2rdtdadot.dot(d2rdtdbdot);
  t1(0,2) = d2rdtdadot.dot(d2rdtdndot);
  t1(0,3) = d2rdtdadot.dot(d2rdtdmdot);
  t1(1,0) = d2rdtdbdot.dot(d2rdtdadot);
  t1(1,1) = d2rdtdbdot.dot(d2rdtdbdot);
  t1(1,2) = d2rdtdbdot.dot(d2rdtdndot);
  t1(1,3) = d2rdtdbdot.dot(d2rdtdmdot);
  t1(2,0) = d2rdtdndot.dot(d2rdtdadot);
  t1(2,1) = d2rdtdndot.dot(d2rdtdbdot);
  t1(2,2) = d2rdtdndot.dot(d2rdtdndot);
  t1(2,3) = d2rdtdndot.dot(d2rdtdmdot);
  t1(3,0) = d2rdtdmdot.dot(d2rdtdadot);
  t1(3,1) = d2rdtdmdot.dot(d2rdtdbdot);
  t1(3,2) = d2rdtdmdot.dot(d2rdtdndot);
  t1(3,3) = d2rdtdmdot.dot(d2rdtdmdot);
  t1*=length;
  
  Eigen::Matrix4r t2;
  auto suIsum=su.integrate().sum();
  t2(0,0)= 4.0*d2rdtdadot.dot( d2Rdtdadot*suIsum );
  t2(0,1)= 2.0*(d2rdtdadot.dot( d2Rdtdbdot*suIsum )+d2rdtdbdot.dot( d2Rdtdadot*suIsum ));
  t2(0,2)= 2.0*(d2rdtdadot.dot( d2Rdtdndot*suIsum )+d2rdtdndot.dot( d2Rdtdadot*suIsum ));
  t2(0,3)= 2.0*(d2rdtdadot.dot( d2Rdtdmdot*suIsum )+d2rdtdmdot.dot( d2Rdtdadot*suIsum ));
  t2(1,0)= t2(0,1);
  t2(1,1)= 4.0*d2rdtdbdot.dot( d2Rdtdbdot*suIsum );
  t2(1,2)= 2.0*(d2rdtdbdot.dot( d2Rdtdndot*suIsum )+d2rdtdndot.dot( d2Rdtdbdot*suIsum ));
  t2(1,3)= 2.0*(d2rdtdbdot.dot( d2Rdtdmdot*suIsum )+d2rdtdmdot.dot( d2Rdtdbdot*suIsum ));
  t2(2,0)= t2(0,2);
  t2(2,1)= t2(1,2);
  t2(2,2)= 4.0*d2rdtdndot.dot( d2Rdtdndot*suIsum );
  t2(2,3)= 2.0*(d2rdtdndot.dot( d2Rdtdmdot*suIsum )+d2rdtdmdot.dot( d2Rdtdndot*suIsum ));
  t2(3,0)= t2(0,3);
  t2(3,1)= t2(1,3);
  t2(3,2)= t2(2,3);
  t2(3,3)= 4.0*d2rdtdmdot.dot( d2Rdtdmdot*suIsum );
  
  Eigen::Matrix4r t3;
  auto sdudaIsum = sduda.integrate().sum();
  auto sdudbIsum = sdudb.integrate().sum();
  auto sdudnIsum = sdudn.integrate().sum();
  auto sdudmIsum = sdudm.integrate().sum();
  t3(0,0) = 4.0*d2rdtdadot.dot( R*sdudaIsum );
  t3(0,1) = 2.0*(d2rdtdadot.dot( R*sdudbIsum )+d2rdtdbdot.dot( R*sdudaIsum ));
  t3(0,2) = 2.0*(d2rdtdadot.dot( R*sdudnIsum )+d2rdtdndot.dot( R*sdudaIsum ));
  t3(0,3) = 2.0*(d2rdtdadot.dot( R*sdudmIsum )+d2rdtdmdot.dot( R*sdudaIsum ));
  t3(1,0) = t3(0,1);
  t3(1,1) = 4.0*d2rdtdbdot.dot( R*sdudbIsum );
  t3(1,2) = 2.0*(d2rdtdbdot.dot( R*sdudnIsum )+d2rdtdndot.dot( R*sdudbIsum ));
  t3(1,3) = 2.0*(d2rdtdbdot.dot( R*sdudmIsum )+d2rdtdmdot.dot( R*sdudbIsum ));
  t3(2,0) = t3(0,2);
  t3(2,1) = t3(1,2);
  t3(2,2) = 4.0*d2rdtdndot.dot( R*sdudnIsum );
  t3(2,3) = 2.0*(d2rdtdndot.dot( R*sdudmIsum )+d2rdtdmdot.dot( R*sdudnIsum ));
  t3(3,0) = t3(2,3);
  t3(3,1) = t3(2,3);
  t3(3,2) = t3(2,3);
  t3(3,3) = 4.0*d2rdtdmdot.dot( R*sdudmIsum );
  
  Eigen::Matrix4r t4;
  auto d2Rdtdadotsu=d2Rdtdadot*su;
  auto d2Rdtdbdotsu=d2Rdtdbdot*su;
  auto d2Rdtdndotsu=d2Rdtdndot*su;
  auto d2Rdtdmdotsu=d2Rdtdmdot*su;
  auto Rsduda=R*sduda;
  auto Rsdudb=R*sdudb;
  auto Rsdudn=R*sdudn;
  auto Rsdudm=R*sdudm;
  t4(0,0)=4.0*(d2Rdtdadotsu).dot(Rsduda).integrate().sum();
  t4(0,1)=2.0*((d2Rdtdbdotsu).dot(Rsduda).integrate().sum()+(d2Rdtdadotsu).dot(Rsdudb).integrate().sum());
  t4(0,2)=2.0*((d2Rdtdndotsu).dot(Rsduda).integrate().sum()+(d2Rdtdadotsu).dot(Rsdudn).integrate().sum());
  t4(0,3)=2.0*((d2Rdtdmdotsu).dot(Rsduda).integrate().sum()+(d2Rdtdadotsu).dot(Rsdudm).integrate().sum());
  t4(1,0)=t4(0,1);
  t4(1,1)=4.0*(d2Rdtdbdotsu).dot(Rsdudb).integrate().sum();
  t4(1,2)=2.0*((d2Rdtdndotsu).dot(Rsdudb).integrate().sum()+(d2Rdtdbdotsu).dot(Rsdudn).integrate().sum());
  t4(1,3)=2.0*((d2Rdtdmdotsu).dot(Rsdudb).integrate().sum()+(d2Rdtdbdotsu).dot(Rsdudm).integrate().sum());
  t4(2,0)=t4(0,2);
  t4(2,1)=t4(1,2);
  t4(2,2)=4.0*(d2Rdtdndotsu).dot(Rsdudn).integrate().sum();
  t4(2,3)=2.0*((d2Rdtdmdotsu).dot(Rsdudn).integrate().sum()+(d2Rdtdndotsu).dot(Rsdudm).integrate().sum());
  t4(3,0)=t4(0,3);
  t4(3,1)=t4(1,3);
  t4(3,2)=t4(2,3);
  t4(3,3)=4.0*(d2Rdtdmdotsu).dot(Rsdudm).integrate().sum();

  Eigen::Matrix4r t5;
  t5(0,0)=2.0*(d2Rdtdadot*su).dot(d2Rdtdadot*su).integrate().sum();
  t5(0,1)=2.0*(d2Rdtdadot*su).dot(d2Rdtdbdot*su).integrate().sum();
  t5(0,2)=2.0*(d2Rdtdadot*su).dot(d2Rdtdndot*su).integrate().sum();
  t5(0,3)=2.0*(d2Rdtdadot*su).dot(d2Rdtdmdot*su).integrate().sum();
  t5(1,0)=t5(0,1);
  t5(1,1)=2.0*(d2Rdtdbdot*su).dot(d2Rdtdbdot*su).integrate().sum();
  t5(1,2)=2.0*(d2Rdtdbdot*su).dot(d2Rdtdndot*su).integrate().sum();
  t5(1,3)=2.0*(d2Rdtdbdot*su).dot(d2Rdtdmdot*su).integrate().sum();
  t5(2,0)=t5(0,2);
  t5(2,1)=t5(1,2);
  t5(2,2)=2.0*(d2Rdtdndot*su).dot(d2Rdtdndot*su).integrate().sum();
  t5(2,3)=2.0*(d2Rdtdndot*su).dot(d2Rdtdmdot*su).integrate().sum();
  t5(3,0)=t5(0,3);
  t5(3,1)=t5(1,3);
  t5(3,2)=t5(2,3);
  t5(3,3)=2.0*(d2Rdtdmdot*su).dot(d2Rdtdmdot*su).integrate().sum();
  Eigen::Matrix4r t6;
  t6(0,0) = 2.0*sduda.dot(sduda).integrate().sum();
  t6(1,0) = 2.0*sdudb.dot(sduda).integrate().sum();
  t6(2,0) = 2.0*sdudn.dot(sduda).integrate().sum();
  t6(3,0) = 2.0*sdudm.dot(sduda).integrate().sum();
  t6(0,1) = 2.0*t6(1,0);
  t6(1,1) = 2.0*sdudb.dot(sdudb).integrate().sum();
  t6(2,1) = 2.0*sdudn.dot(sdudb).integrate().sum();
  t6(3,1) = 2.0*sdudm.dot(sdudb).integrate().sum();
  t6(0,2) = 2.0*t6(2,0);
  t6(1,2) = 2.0*t6(2,1);
  t6(2,2) = 2.0*sdudn.dot(sdudn).integrate().sum();
  t6(3,2) = 2.0*sdudm.dot(sdudn).integrate().sum();
  t6(0,3) = 2.0*t6(3,0);
  t6(1,3) = 2.0*t6(3,1);
  t6(2,3) = 2.0*t6(3,2);
  t6(3,3) = 2.0*sdudm.dot(sdudm).integrate().sum();
  d2Ecdqdot2 = 0.5*areaDensity*width*(t1+t2+t3+t4+t5+t6);
}

void FunctionPartApproxTige::computeGradEc()
{
  Eigen::Vector4r t1;
  t1 (0) = 2.0*length*d2rdtda.dot(d1rdt);
  t1 (1) = 2.0*length*d2rdtdb.dot(d1rdt);
  t1 (2) = 2.0*length*d2rdtdn.dot(d1rdt);
  t1 (3) = 2.0*length*d2rdtdm.dot(d1rdt);
  
  Eigen::Vector4r t2;
  auto sdudaIsum = sduda.integrate().sum();
  auto sdudbIsum = sdudb.integrate().sum();
  auto sdudnIsum = sdudn.integrate().sum();
  auto sdudmIsum = sdudm.integrate().sum();
  auto suIsum = su.integrate().sum();
  t2(0) = 2.0*(d2rdtda.dot( d1Rdt* suIsum )+d1rdt.dot( d2Rdtda* suIsum )+d1rdt.dot( d1Rdt* sdudaIsum ));
  t2(1) = 2.0*(d2rdtdb.dot( d1Rdt* suIsum )+d1rdt.dot( d2Rdtdb* suIsum )+d1rdt.dot( d1Rdt* sdudbIsum ));
  t2(2) = 2.0*(d2rdtdn.dot( d1Rdt* suIsum )+d1rdt.dot( d2Rdtdn* suIsum )+d1rdt.dot( d1Rdt* sdudnIsum ));
  t2(3) = 2.0*(d2rdtdm.dot( d1Rdt* suIsum )+d1rdt.dot( d2Rdtdm* suIsum )+d1rdt.dot( d1Rdt* sdudmIsum ));
  
  Eigen::Vector4r t3;
  Eigen::Vector3d dudt=adot*sdudaIsum
                    + bdot*sdudbIsum
                    + ndot*sdudnIsum
                    + mdot*sdudmIsum;
  Eigen::Vector3d d2udtda=adot*sd2udada.integrate().sum()
                     + bdot*sd2udbda.integrate().sum()
                     + ndot*sd2udnda.integrate().sum()
                     + mdot*sd2udmda.integrate().sum();
  Eigen::Vector3d d2udtdb=adot*sd2udadb.integrate().sum()
                     + bdot*sd2udbdb.integrate().sum()
                     + ndot*sd2udndb.integrate().sum()
                     + mdot*sd2udmdb.integrate().sum();
  Eigen::Vector3d d2udtdn=adot*sd2udadn.integrate().sum()
                     + bdot*sd2udbdn.integrate().sum()
                     + ndot*sd2udndn.integrate().sum()
                     + mdot*sd2udmdn.integrate().sum();
  Eigen::Vector3d d2udtdm=adot*sd2udadm.integrate().sum()
                     + bdot*sd2udbdm.integrate().sum()
                     + ndot*sd2udndm.integrate().sum()
                     + mdot*sd2udmdm.integrate().sum();
  t3(0) = 2.0*d2rdtda.dot( R*dudt )+2.0*d1rdt.dot( d1Rda*dudt )+2.0*d1rdt.dot( R*d2udtda );
  t3(1) = 2.0*d2rdtdb.dot( R*dudt )+2.0*d1rdt.dot( d1Rdb*dudt )+2.0*d1rdt.dot( R*d2udtdb );
  t3(2) = 2.0*d2rdtdn.dot( R*dudt )+2.0*d1rdt.dot( d1Rdn*dudt )+2.0*d1rdt.dot( R*d2udtdn );
  t3(3) = 2.0*d2rdtdm.dot( R*dudt )+2.0*d1rdt.dot( d1Rdm*dudt )+2.0*d1rdt.dot( R*d2udtdm );
  
  Eigen::Vector4r t4;
  t4(0) = 2.*(adot*(d2Rdtda*su).dot(R*sduda).integrate().sum()
         + bdot*(d2Rdtda*su).dot(R*sdudb).integrate().sum()
         + ndot*(d2Rdtda*su).dot(R*sdudn).integrate().sum()
         + mdot*(d2Rdtda*su).dot(R*sdudm).integrate().sum()
             +
         adot*(d1Rdt*sduda).dot(R*sduda).integrate().sum()
         + bdot*(d1Rdt*sduda).dot(R*sdudb).integrate().sum()
         + ndot*(d1Rdt*sduda).dot(R*sdudn).integrate().sum()
         + mdot*(d1Rdt*sduda).dot(R*sdudm).integrate().sum()
             +
         adot*(d1Rdt*su).dot(d1Rda*sduda).integrate().sum()
         + bdot*(d1Rdt*su).dot(d1Rda*sdudb).integrate().sum()
         + ndot*(d1Rdt*su).dot(d1Rda*sdudn).integrate().sum()
         + mdot*(d1Rdt*su).dot(d1Rda*sdudm).integrate().sum()
             +
         adot*(d1Rdt*su).dot(R*sd2udada).integrate().sum()
         + bdot*(d1Rdt*su).dot(R*sd2udbda).integrate().sum()
         + ndot*(d1Rdt*su).dot(R*sd2udnda).integrate().sum()
         + mdot*(d1Rdt*su).dot(R*sd2udmda).integrate().sum());
  
  
  t4(1) = 2.*(adot*(d2Rdtdb*su).dot(R*sduda).integrate().sum()
         + bdot*(d2Rdtdb*su).dot(R*sdudb).integrate().sum()
         + ndot*(d2Rdtdb*su).dot(R*sdudn).integrate().sum()
         + mdot*(d2Rdtdb*su).dot(R*sdudm).integrate().sum()
             +
         adot*(d1Rdt*sdudb).dot(R*sduda).integrate().sum()
         + bdot*(d1Rdt*sdudb).dot(R*sdudb).integrate().sum()
         + ndot*(d1Rdt*sdudb).dot(R*sdudn).integrate().sum()
         + mdot*(d1Rdt*sdudb).dot(R*sdudm).integrate().sum()
             +
         adot*(d1Rdt*su).dot(d1Rdb*sduda).integrate().sum()
         + bdot*(d1Rdt*su).dot(d1Rdb*sdudb).integrate().sum()
         + ndot*(d1Rdt*su).dot(d1Rdb*sdudn).integrate().sum()
         + mdot*(d1Rdt*su).dot(d1Rdb*sdudm).integrate().sum()
             +
         adot*(d1Rdt*su).dot(R*sd2udadb).integrate().sum()
         + bdot*(d1Rdt*su).dot(R*sd2udbdb).integrate().sum()
         + ndot*(d1Rdt*su).dot(R*sd2udndb).integrate().sum()
         + mdot*(d1Rdt*su).dot(R*sd2udmdb).integrate().sum());
  t4(2) = 2.*(adot*(d2Rdtdn*su).dot(R*sduda).integrate().sum()
         + bdot*(d2Rdtdn*su).dot(R*sdudb).integrate().sum()
         + ndot*(d2Rdtdn*su).dot(R*sdudn).integrate().sum()
         + mdot*(d2Rdtdn*su).dot(R*sdudm).integrate().sum()
             +
         adot*(d1Rdt*sdudn).dot(R*sduda).integrate().sum()
         + bdot*(d1Rdt*sdudn).dot(R*sdudb).integrate().sum()
         + ndot*(d1Rdt*sdudn).dot(R*sdudn).integrate().sum()
         + mdot*(d1Rdt*sdudn).dot(R*sdudm).integrate().sum()
             +
         adot*(d1Rdt*su).dot(d1Rdn*sduda).integrate().sum()
         + bdot*(d1Rdt*su).dot(d1Rdn*sdudb).integrate().sum()
         + ndot*(d1Rdt*su).dot(d1Rdn*sdudn).integrate().sum()
         + mdot*(d1Rdt*su).dot(d1Rdn*sdudm).integrate().sum()
             +
         adot*(d1Rdt*su).dot(R*sd2udadn).integrate().sum()
         + bdot*(d1Rdt*su).dot(R*sd2udbdn).integrate().sum()
         + ndot*(d1Rdt*su).dot(R*sd2udndn).integrate().sum()
         + mdot*(d1Rdt*su).dot(R*sd2udmdn).integrate().sum());
  
  t4(3) = 2.*(adot*(d2Rdtdm*su).dot(R*sduda).integrate().sum()
         + bdot*(d2Rdtdm*su).dot(R*sdudb).integrate().sum()
         + ndot*(d2Rdtdm*su).dot(R*sdudn).integrate().sum()
         + mdot*(d2Rdtdm*su).dot(R*sdudm).integrate().sum()
             +
         adot*(d1Rdt*sdudm).dot(R*sduda).integrate().sum()
         + bdot*(d1Rdt*sdudm).dot(R*sdudb).integrate().sum()
         + ndot*(d1Rdt*sdudm).dot(R*sdudn).integrate().sum()
         + mdot*(d1Rdt*sdudm).dot(R*sdudm).integrate().sum()
             +
         adot*(d1Rdt*su).dot(d1Rdm*sduda).integrate().sum()
         + bdot*(d1Rdt*su).dot(d1Rdm*sdudb).integrate().sum()
         + ndot*(d1Rdt*su).dot(d1Rdm*sdudn).integrate().sum()
         + mdot*(d1Rdt*su).dot(d1Rdm*sdudm).integrate().sum()
             +
         adot*(d1Rdt*su).dot(R*sd2udadm).integrate().sum()
         + bdot*(d1Rdt*su).dot(R*sd2udbdm).integrate().sum()
         + ndot*(d1Rdt*su).dot(R*sd2udndm).integrate().sum()
         + mdot*(d1Rdt*su).dot(R*sd2udmdm).integrate().sum());
  
  Eigen::Vector4r t5;
  t5(0)=2.0*(d2Rdtda*su+d1Rdt*sduda).dot(d1Rdt*su).integrate().sum();
  t5(1)=2.0*(d2Rdtdb*su+d1Rdt*sdudb).dot(d1Rdt*su).integrate().sum();
  t5(2)=2.0*(d2Rdtdn*su+d1Rdt*sdudn).dot(d1Rdt*su).integrate().sum();
  t5(3)=2.0*(d2Rdtdm*su+d1Rdt*sdudm).dot(d1Rdt*su).integrate().sum();
  
  Eigen::Vector4r t6;
  t6(0) = 2.*adot*sd2udada.dot(sduda).integrate().sum()*adot
        + 2.*bdot*sd2udbda.dot(sduda).integrate().sum()*adot
        + 2.*ndot*sd2udnda.dot(sduda).integrate().sum()*adot
        + 2.*mdot*sd2udmda.dot(sduda).integrate().sum()*adot
        + 2.*bdot*sd2udbda.dot(sdudb).integrate().sum()*bdot
        + 2.*ndot*sd2udnda.dot(sdudb).integrate().sum()*bdot
        + 2.*mdot*sd2udmda.dot(sdudb).integrate().sum()*bdot
        + 2.*ndot*sd2udnda.dot(sdudn).integrate().sum()*ndot
        + 2.*mdot*sd2udmda.dot(sdudn).integrate().sum()*ndot
        + 2.*mdot*sd2udmda.dot(sdudm).integrate().sum()*mdot
        + 2.*bdot*sdudb.dot(sd2udada).integrate().sum()*adot
        + 2.*ndot*sdudn.dot(sd2udada).integrate().sum()*adot
        + 2.*mdot*sdudm.dot(sd2udada).integrate().sum()*adot
        + 2.*ndot*sdudn.dot(sd2udbda).integrate().sum()*bdot
        + 2.*mdot*sdudm.dot(sd2udbda).integrate().sum()*bdot
        + 2.*mdot*sdudm.dot(sd2udnda).integrate().sum()*ndot;
  t6(1) = 2.*adot*sd2udadb.dot(sduda).integrate().sum()*adot
        + 2.*bdot*sd2udbdb.dot(sduda).integrate().sum()*adot
        + 2.*ndot*sd2udndb.dot(sduda).integrate().sum()*adot
        + 2.*mdot*sd2udmdb.dot(sduda).integrate().sum()*adot
        + 2.*bdot*sd2udbdb.dot(sdudb).integrate().sum()*bdot
        + 2.*ndot*sd2udndb.dot(sdudb).integrate().sum()*bdot
        + 2.*mdot*sd2udmdb.dot(sdudb).integrate().sum()*bdot
        + 2.*ndot*sd2udndb.dot(sdudn).integrate().sum()*ndot
        + 2.*mdot*sd2udmdb.dot(sdudn).integrate().sum()*ndot
        + 2.*mdot*sd2udmdb.dot(sdudm).integrate().sum()*mdot
        + 2.*bdot*sdudb.dot(sd2udadb).integrate().sum()*adot
        + 2.*ndot*sdudn.dot(sd2udadb).integrate().sum()*adot
        + 2.*mdot*sdudm.dot(sd2udadb).integrate().sum()*adot
        + 2.*ndot*sdudn.dot(sd2udbdb).integrate().sum()*bdot
        + 2.*mdot*sdudm.dot(sd2udbdb).integrate().sum()*bdot
        + 2.*mdot*sdudm.dot(sd2udndb).integrate().sum()*ndot;
  t6(2) = 2.*adot*sd2udadn.dot(sduda).integrate().sum()*adot
        + 2.*bdot*sd2udbdn.dot(sduda).integrate().sum()*adot
        + 2.*ndot*sd2udndn.dot(sduda).integrate().sum()*adot
        + 2.*mdot*sd2udmdn.dot(sduda).integrate().sum()*adot
        + 2.*bdot*sd2udbdn.dot(sdudb).integrate().sum()*bdot
        + 2.*ndot*sd2udndn.dot(sdudb).integrate().sum()*bdot
        + 2.*mdot*sd2udmdn.dot(sdudb).integrate().sum()*bdot
        + 2.*ndot*sd2udndn.dot(sdudn).integrate().sum()*ndot
        + 2.*mdot*sd2udmdn.dot(sdudn).integrate().sum()*ndot
        + 2.*mdot*sd2udmdn.dot(sdudm).integrate().sum()*mdot
        + 2.*bdot*sdudb.dot(sd2udadn).integrate().sum()*adot
        + 2.*ndot*sdudn.dot(sd2udadn).integrate().sum()*adot
        + 2.*mdot*sdudm.dot(sd2udadn).integrate().sum()*adot
        + 2.*ndot*sdudn.dot(sd2udbdn).integrate().sum()*bdot
        + 2.*mdot*sdudm.dot(sd2udbdn).integrate().sum()*bdot
        + 2.*mdot*sdudm.dot(sd2udndn).integrate().sum()*ndot;
  t6(3) = 2.*adot*sd2udadm.dot(sduda).integrate().sum()*adot
        + 2.*bdot*sd2udbdm.dot(sduda).integrate().sum()*adot
        + 2.*ndot*sd2udndm.dot(sduda).integrate().sum()*adot
        + 2.*mdot*sd2udmdm.dot(sduda).integrate().sum()*adot
        + 2.*bdot*sd2udbdm.dot(sdudb).integrate().sum()*bdot
        + 2.*ndot*sd2udndm.dot(sdudb).integrate().sum()*bdot
        + 2.*mdot*sd2udmdm.dot(sdudb).integrate().sum()*bdot
        + 2.*ndot*sd2udndm.dot(sdudn).integrate().sum()*ndot
        + 2.*mdot*sd2udmdm.dot(sdudn).integrate().sum()*ndot
        + 2.*mdot*sd2udmdm.dot(sdudm).integrate().sum()*mdot
        + 2.*bdot*sdudb.dot(sd2udadm).integrate().sum()*adot
        + 2.*ndot*sdudn.dot(sd2udadm).integrate().sum()*adot
        + 2.*mdot*sdudm.dot(sd2udadm).integrate().sum()*adot
        + 2.*ndot*sdudn.dot(sd2udbdm).integrate().sum()*bdot
        + 2.*mdot*sdudm.dot(sd2udbdm).integrate().sum()*bdot
        + 2.*mdot*sdudm.dot(sd2udndm).integrate().sum()*ndot;
        
  gradEc=0.5*areaDensity*width*(t1+t2+t3+t4+t5+t6);
}

void FunctionPartApproxTige::computeSysB()
{
  //dL/dq-d2L/dqdqdot qdot
  Eigen::Vector4r t1;
  t1 (0) = 2.0*length*d2rdtda.dot(d1rdt);
  t1 (1) = 2.0*length*d2rdtdb.dot(d1rdt);
  t1 (2) = 2.0*length*d2rdtdn.dot(d1rdt);
  t1 (3) = 2.0*length*d2rdtdm.dot(d1rdt);
  
  Eigen::Vector4r t2;
  auto sdudaIsum = sduda.integrate().sum();
  auto sdudbIsum = sdudb.integrate().sum();
  auto sdudnIsum = sdudn.integrate().sum();
  auto sdudmIsum = sdudm.integrate().sum();
  auto suIsum = su.integrate().sum();
  t2(0) = 2.0*(d2rdtda.dot( d1Rdt* suIsum )+d1rdt.dot( d2Rdtda* suIsum )+d1rdt.dot( d1Rdt* sdudaIsum ));
  t2(1) = 2.0*(d2rdtdb.dot( d1Rdt* suIsum )+d1rdt.dot( d2Rdtdb* suIsum )+d1rdt.dot( d1Rdt* sdudbIsum ));
  t2(2) = 2.0*(d2rdtdn.dot( d1Rdt* suIsum )+d1rdt.dot( d2Rdtdn* suIsum )+d1rdt.dot( d1Rdt* sdudnIsum ));
  t2(3) = 2.0*(d2rdtdm.dot( d1Rdt* suIsum )+d1rdt.dot( d2Rdtdm* suIsum )+d1rdt.dot( d1Rdt* sdudmIsum ));
  
  //term3 is null
  //term4 is null
  
  Eigen::Vector4r t5;
  t5(0)=2.0*(d2Rdtda*su+d1Rdt*sduda).dot(d1Rdt*su).integrate().sum();
  t5(1)=2.0*(d2Rdtdb*su+d1Rdt*sdudb).dot(d1Rdt*su).integrate().sum();
  t5(2)=2.0*(d2Rdtdn*su+d1Rdt*sdudn).dot(d1Rdt*su).integrate().sum();
  t5(3)=2.0*(d2Rdtdm*su+d1Rdt*sdudm).dot(d1Rdt*su).integrate().sum();
  
  Eigen::Vector4r t6;
  t6(0) = -2.*adot*sd2udada.dot(sduda).integrate().sum()*adot
        - 2.*bdot*sd2udbda.dot(sduda).integrate().sum()*adot
        - 2.*ndot*sd2udnda.dot(sduda).integrate().sum()*adot
        - 2.*mdot*sd2udmda.dot(sduda).integrate().sum()*adot
        - 2.*bdot*sd2udbda.dot(sdudb).integrate().sum()*bdot
        - 2.*ndot*sd2udnda.dot(sdudb).integrate().sum()*bdot
        - 2.*mdot*sd2udmda.dot(sdudb).integrate().sum()*bdot
        - 2.*ndot*sd2udnda.dot(sdudn).integrate().sum()*ndot
        - 2.*mdot*sd2udmda.dot(sdudn).integrate().sum()*ndot
        - 2.*mdot*sd2udmda.dot(sdudm).integrate().sum()*mdot
        - 2.*bdot*sdudb.dot(sd2udada).integrate().sum()*adot
        - 2.*ndot*sdudn.dot(sd2udada).integrate().sum()*adot
        - 2.*mdot*sdudm.dot(sd2udada).integrate().sum()*adot
        - 2.*ndot*sdudn.dot(sd2udbda).integrate().sum()*bdot
        - 2.*mdot*sdudm.dot(sd2udbda).integrate().sum()*bdot
        - 2.*mdot*sdudm.dot(sd2udnda).integrate().sum()*ndot;
  t6(1) = -2.*adot*sd2udadb.dot(sduda).integrate().sum()*adot
        - 2.*bdot*sd2udbdb.dot(sduda).integrate().sum()*adot
        - 2.*ndot*sd2udndb.dot(sduda).integrate().sum()*adot
        - 2.*mdot*sd2udmdb.dot(sduda).integrate().sum()*adot
        - 2.*bdot*sd2udbdb.dot(sdudb).integrate().sum()*bdot
        - 2.*ndot*sd2udndb.dot(sdudb).integrate().sum()*bdot
        - 2.*mdot*sd2udmdb.dot(sdudb).integrate().sum()*bdot
        - 2.*ndot*sd2udndb.dot(sdudn).integrate().sum()*ndot
        - 2.*mdot*sd2udmdb.dot(sdudn).integrate().sum()*ndot
        - 2.*mdot*sd2udmdb.dot(sdudm).integrate().sum()*mdot
        - 2.*bdot*sdudb.dot(sd2udadb).integrate().sum()*adot
        - 2.*ndot*sdudn.dot(sd2udadb).integrate().sum()*adot
        - 2.*mdot*sdudm.dot(sd2udadb).integrate().sum()*adot
        - 2.*ndot*sdudn.dot(sd2udbdb).integrate().sum()*bdot
        - 2.*mdot*sdudm.dot(sd2udbdb).integrate().sum()*bdot
        - 2.*mdot*sdudm.dot(sd2udndb).integrate().sum()*ndot;
  t6(2) = -2.*adot*sd2udadn.dot(sduda).integrate().sum()*adot
        - 2.*bdot*sd2udbdn.dot(sduda).integrate().sum()*adot
        - 2.*ndot*sd2udndn.dot(sduda).integrate().sum()*adot
        - 2.*mdot*sd2udmdn.dot(sduda).integrate().sum()*adot
        - 2.*bdot*sd2udbdn.dot(sdudb).integrate().sum()*bdot
        - 2.*ndot*sd2udndn.dot(sdudb).integrate().sum()*bdot
        - 2.*mdot*sd2udmdn.dot(sdudb).integrate().sum()*bdot
        - 2.*ndot*sd2udndn.dot(sdudn).integrate().sum()*ndot
        - 2.*mdot*sd2udmdn.dot(sdudn).integrate().sum()*ndot
        - 2.*mdot*sd2udmdn.dot(sdudm).integrate().sum()*mdot
        - 2.*bdot*sdudb.dot(sd2udadn).integrate().sum()*adot
        - 2.*ndot*sdudn.dot(sd2udadn).integrate().sum()*adot
        - 2.*mdot*sdudm.dot(sd2udadn).integrate().sum()*adot
        - 2.*ndot*sdudn.dot(sd2udbdn).integrate().sum()*bdot
        - 2.*mdot*sdudm.dot(sd2udbdn).integrate().sum()*bdot
        - 2.*mdot*sdudm.dot(sd2udndn).integrate().sum()*ndot;
  t6(3) = -2.*adot*sd2udadm.dot(sduda).integrate().sum()*adot
        - 2.*bdot*sd2udbdm.dot(sduda).integrate().sum()*adot
        - 2.*ndot*sd2udndm.dot(sduda).integrate().sum()*adot
        - 2.*mdot*sd2udmdm.dot(sduda).integrate().sum()*adot
        - 2.*bdot*sd2udbdm.dot(sdudb).integrate().sum()*bdot
        - 2.*ndot*sd2udndm.dot(sdudb).integrate().sum()*bdot
        - 2.*mdot*sd2udmdm.dot(sdudb).integrate().sum()*bdot
        - 2.*ndot*sd2udndm.dot(sdudn).integrate().sum()*ndot
        - 2.*mdot*sd2udmdm.dot(sdudn).integrate().sum()*ndot
        - 2.*mdot*sd2udmdm.dot(sdudm).integrate().sum()*mdot
        - 2.*bdot*sdudb.dot(sd2udadm).integrate().sum()*adot
        - 2.*ndot*sdudn.dot(sd2udadm).integrate().sum()*adot
        - 2.*mdot*sdudm.dot(sd2udadm).integrate().sum()*adot
        - 2.*ndot*sdudn.dot(sd2udbdm).integrate().sum()*bdot
        - 2.*mdot*sdudm.dot(sd2udbdm).integrate().sum()*bdot
        - 2.*mdot*sdudm.dot(sd2udndm).integrate().sum()*ndot;
        
  sysB = 0.5*areaDensity*width*(t1+t2+t5+t6)-gradEg-gradEw;
}

