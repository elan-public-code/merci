/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "MovingRibbon.hpp"
#include <iostream>
#include <iomanip>

MovingRibbon::MovingRibbon()
{
  point.resize(4);
  natCurv.resize(1);
  posOr.setZero();
  frameOr.setIdentity();
}

size_t MovingRibbon::vpointIDb() const
{
  return 0;
}

size_t MovingRibbon::vpointIDbdot() const
{
  return 1;
}

size_t MovingRibbon::vpointIDm() const
{
  return 2;
}

size_t MovingRibbon::vpointIDmdot() const
{
  return 3;
}

size_t MovingRibbon::vpointIDa(const size_t& segmentID) const
{
  return 4*(segmentID+1);
}

size_t MovingRibbon::vpointIDadot(const size_t& segmentID) const
{
  return 4*segmentID+5;
}

size_t MovingRibbon::vpointIDn(const size_t& segmentID) const
{
  return 4*segmentID+6;
}

size_t MovingRibbon::vpointIDndot(const size_t& segmentID) const
{
  return 4*segmentID+7;
}

size_t MovingRibbon::vpointSize() const
{
  return point.size();
}

double & MovingRibbon::vpointb()
{
  return point.at(vpointIDb());
}

double & MovingRibbon::vpointbdot()
{
  return point.at(vpointIDbdot());
}

double & MovingRibbon::vpointm()
{
  return point.at(vpointIDm());
}

double & MovingRibbon::vpointmdot()
{
  return point.at(vpointIDmdot());
}

double & MovingRibbon::vpointa(const size_t& segment)
{
  return point.at(vpointIDa(segment));
}
 
double & MovingRibbon::vpointadot(const size_t& segment)
{
  return point.at(vpointIDadot(segment));
}

double & MovingRibbon::vpointn(const size_t& segment)
{
  return point.at(vpointIDn(segment));
}

double & MovingRibbon::vpointndot(const size_t& segment)
{
  return point.at(vpointIDndot(segment));
}

double & MovingRibbon::segmentLength(const size_t& segment)
{
  return lengths.at(segment);
}

double & MovingRibbon::vpointbnat()
{
  return natCurv.at(0);
}

double & MovingRibbon::vpointanat(const size_t& segment)
{
  return natCurv.at(1+segment);
}

void MovingRibbon::setNbSegments(size_t nbSeg)
{
  point.resize(nbSeg*4+4);
  natCurv.resize(nbSeg+1);
  lengths.resize(nbSeg);
}

size_t MovingRibbon::getNbSegments()
{
  return lengths.size();
}

void MovingRibbon::setCrossSection(std::array<double, 2> cs)
{
  width=std::max(cs[0],cs[1]);
  thickness=std::min(cs[0],cs[1]);
}

std::array<double, 2> MovingRibbon::getCrossSection()
{
  return {width,thickness};
}

void MovingRibbon::place(Eigen::Vector3d pos, Eigen::Matrix3d frame)
{
  posOr=pos;
  frameOr=frame;
}

std::pair<Eigen::Vector3d, Eigen::Matrix3d> MovingRibbon::getPlace()
{
  return {posOr,frameOr};
}

void MovingRibbon::setPhysicalParams(double D, double areaDensity, double poissonRatio)
{
  this->D=D;
  this->areaDensity=areaDensity;
  this->poissonRatio=poissonRatio;
}

double MovingRibbon::getD()
{
  return D;
}

double MovingRibbon::getAreaDensity()
{
  return areaDensity;
}

void MovingRibbon::printPoint(std::ostream& os)
{
  auto t = lengths.size();
  os << std::left << std::setw(6) << std::setfill(' ') << " ";
  os << std::left << std::setw(11) << std::setfill(' ') << "b0";
  os << std::left << std::setw(11) << std::setfill(' ') << "m0";
  for(size_t i=0;i<t;i++){
    std::string namea="a"+std::to_string(i);
    os  << std::left << std::setw(11) << std::setfill(' ') << namea;
    std::string namen="n"+std::to_string(i);
    os  << std::left << std::setw(11) << std::setfill(' ') << namen;
  }
  os << std::endl;
  os << std::left << std::setw(6) << std::setfill(' ') << "val.";
  os << std::left << std::setw(10) << std::setfill(' ') << vpointb()<<' ';
  os << std::left << std::setw(10) << std::setfill(' ') << vpointm()<<' ';
  for(size_t i=0;i<t;i++){
    os  << std::left << std::setw(10) << std::setfill(' ') << vpointa(i)<<' ';
    os  << std::left << std::setw(10) << std::setfill(' ') << vpointn(i)<<' ';
  }
  os << std::endl;
  os << std::left << std::setw(6) << std::setfill(' ') << "dot.";
  os << std::left << std::setw(10) << std::setfill(' ') << vpointbdot()<<' ';
  os << std::left << std::setw(10) << std::setfill(' ') << vpointmdot()<<' ';
  for(size_t i=0;i<t;i++){
    os  << std::left << std::setw(10) << std::setfill(' ') << vpointadot(i)<<' ';
    os  << std::left << std::setw(10) << std::setfill(' ') << vpointndot(i)<<' ';
  }
  os << std::endl;
}

Eigen::VectorXd MovingRibbon::getAllSysPoint()
{
  size_t t = point.size();
  Eigen::VectorXd res(t);
  for(size_t i=0;i<t;i++)
    res[i]=point[i];
  return res;
}

void MovingRibbon::setAllSysPoint(const Eigen::VectorXd& pt)
{
  int t = pt.size();
  Eigen::VectorXd res(t);
  for(int i=0;i<t;i++)
    point.at(i)=pt(i);
}

std::pair<Eigen::VectorXd, Eigen::VectorXd> MovingRibbon::getPointAndSpeed()
{
  auto t = lengths.size();
  Eigen::VectorXd pt(2*t+2);
  Eigen::VectorXd speed(2*t+2);
  pt(0) = vpointb();
  pt(1) = vpointm();
  for(size_t i=0;i<t;i++){
    pt(2*i+2) = vpointa(i);
    pt(2*i+3) = vpointn(i);
  }
  speed(0) = vpointbdot();
  speed(1) = vpointmdot();
  for(size_t i=0;i<t;i++){
    speed(2*i+2) = vpointadot(i);
    speed(2*i+3) = vpointndot(i);
  }
  return {pt, speed};
}
  
void MovingRibbon::setPointAndSpeed(const Eigen::VectorXd& pt, const Eigen::VectorXd speed)
{
  auto t = lengths.size();
  vpointb() = pt(0);
  vpointm() = pt(1);
  for(size_t i=0;i<t;i++){
    vpointa(i) = pt(2*i+2);
    vpointn(i) = pt(2*i+3);
  }
  vpointbdot() = speed(0);
  vpointmdot() = speed(1);
  for(size_t i=0;i<t;i++){
    vpointadot(i) = speed(2*i+2);
    vpointndot(i) = speed(2*i+3);
  }
}
