/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef MOVINGRIBBON_H
#define MOVINGRIBBON_H

#include <vector>
#include <optional>
#include <Eigen/Dense>
#include <iostream>

class MovingRibbon
{
public:
  MovingRibbon();
  
  size_t vpointSize() const;
  size_t vpointIDb() const;
  size_t vpointIDm() const;
  size_t vpointIDa( const size_t& segmentID ) const;
  size_t vpointIDn( const size_t& segmentID ) const;
  size_t vpointIDbdot() const;
  size_t vpointIDmdot() const;
  size_t vpointIDadot( const size_t& segmentID ) const;
  size_t vpointIDndot( const size_t& segmentID ) const;
  
  double& vpointb();
  double& vpointm();
  double& vpointa(const size_t& segment);
  double& vpointn(const size_t& segment);
  double& vpointbdot();
  double& vpointmdot();
  double& vpointadot(const size_t& segment);
  double& vpointndot(const size_t& segment);
  
  double& vpointbnat();
  double& vpointanat(const size_t& segment);
  
  double& segmentLength(const size_t& segment);
  
  void setNbSegments(size_t nbSeg);
  size_t getNbSegments();
  
  void setCrossSection(std::array<double,2> cs);//order doesn't matter thickness is the smallest.
  std::array<double,2> getCrossSection();//width x thickness
  
  void place(Eigen::Vector3d pos, Eigen::Matrix3d frame);
  std::pair<Eigen::Vector3d, Eigen::Matrix3d> getPlace();
  
  void setPhysicalParams(double D, double areaDensity, double poissonRatio);
  double getD();
  double getAreaDensity();
  
  void printPoint(std::ostream& os=std::cout);
  Eigen::VectorXd getAllSysPoint();
  void setAllSysPoint(const Eigen::VectorXd& pt);
  std::pair<Eigen::VectorXd, Eigen::VectorXd> getPointAndSpeed();
  void setPointAndSpeed(const Eigen::VectorXd& pt, const Eigen::VectorXd speed);
private:
  std::vector<double> point;
  std::vector<double> natCurv;
  std::vector<double> lengths;
  
  Eigen::Vector3d posOr;
  Eigen::Matrix3d frameOr;
  double width = 0.01;
  double thickness = 0.00001;
  double D = 1.e-4;
  double poissonRatio = 0.5; 
  double areaDensity = 0.1;
};

#endif // MOVINGRIBBON_H
