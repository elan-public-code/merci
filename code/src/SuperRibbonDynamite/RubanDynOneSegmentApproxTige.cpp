/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "RubanDynOneSegmentApproxTige.hpp"
#include "FunctionSegmentTigeApprox.hpp"
#include <iomanip>
#include <fstream>

RubanDynOneSegmentApproxTige::RubanDynOneSegmentApproxTige()
{
  sysPt.setZero();
  sysPtdot.setZero();
  R.setIdentity();
  r.setZero();
  length=1;
  width=0.01;
  D=1;
  areaDensity=1;
  G<<0,-10,0;
  nu=0.4;
  anat=0.;
  bnat=0.;
  virtuaDamping=0;
  useRK4=true;
  dt=1.e-3;
}


void RubanDynOneSegmentApproxTige::set(Eigen::Vector4d pt, Eigen::Vector4d speed)
{
  cached = false;
  sysPt = pt;
  sysPtdot = speed;
}

Eigen::Vector4d RubanDynOneSegmentApproxTige::getPoint() const
{
  return sysPt;
}

Eigen::Vector4d RubanDynOneSegmentApproxTige::getPointSpeed() const
{
  return sysPtdot;
}

void RubanDynOneSegmentApproxTige::print(std::ostream& os) const
{
  os << std::left << std::setw(6) << std::setfill(' ') << " ";
  os << std::left << std::setw(11) << std::setfill(' ') << "a";
  os << std::left << std::setw(11) << std::setfill(' ') << "b";
  os << std::left << std::setw(11) << std::setfill(' ') << "n";
  os << std::left << std::setw(11) << std::setfill(' ') << "m";
  os << std::endl;
  os << std::left << std::setw(6) << std::setfill(' ') << "val.";
  os << std::left << std::setw(10) << std::setfill(' ') << sysPt[0] <<' ';
  os << std::left << std::setw(10) << std::setfill(' ') << sysPt[1]<<' ';
  os << std::left << std::setw(10) << std::setfill(' ') << sysPt[2] <<' ';
  os << std::left << std::setw(10) << std::setfill(' ') << sysPt[3]<<' ';
  os << std::endl;
  os << std::left << std::setw(6) << std::setfill(' ') << "dot.";
  os << std::left << std::setw(10) << std::setfill(' ') << sysPtdot[0] <<' ';
  os << std::left << std::setw(10) << std::setfill(' ') << sysPtdot[1]<<' ';
  os << std::left << std::setw(10) << std::setfill(' ') << sysPtdot[2] <<' ';
  os << std::left << std::setw(10) << std::setfill(' ') << sysPtdot[3]<<' ';
  os << std::endl;
}

std::ostream & RubanDynOneSegmentApproxTige::operator<<(std::ostream& os) const
{
  print(os);
  return os;
}

void RubanDynOneSegmentApproxTige::setupGeo(Eigen::Vector3d pos, Eigen::Matrix3d frame, double ribbonWidth, double ribbonLength, double curvANat, double curvBNat)
{
  cached=false;
  r=pos;
  R=frame;
  width=ribbonWidth;
  length=ribbonLength;
  anat=curvANat;
  bnat=curvBNat;
}

void RubanDynOneSegmentApproxTige::setupPhy(Eigen::Vector3d gravity, double D, double areaDensity, double nu)
{
  cached=false;
  G=gravity;
  this->D=D;
  this->areaDensity=areaDensity;
  this->nu=nu;
}

void RubanDynOneSegmentApproxTige::setupTimeDynamic(double dt, bool useRK4, double virtuaDamping)
{
  cached=false;
  this->dt=dt;
  this->useRK4=useRK4;
  this->virtuaDamping=virtuaDamping;
}


Eigen::Vector4d RubanDynOneSegmentApproxTige::computeAcc()
{
  if(cached)return acc;
  FunctionSegmentTigeApprox fun;
  //Parameters
  fun.G=G;
  fun.D=D;
  fun.areaDensity=areaDensity;
  fun.width=width;
  fun.nu=nu;
  fun.length=length;

 //In arguments
  fun.a=sysPt[0];
  fun.adot=sysPtdot[0];
  fun.anat=anat;
  fun.b=sysPt[1];
  fun.bdot=sysPtdot[1];
  fun.bnat=bnat;
  fun.n=sysPt[2];
  fun.ndot=sysPtdot[2];
  fun.m=sysPt[3];
  fun.mdot=sysPtdot[3];

 //In functions
  fun.R=R;
  fun.d1Rda.setZero();
  fun.d1Rdb.setZero();
  fun.d1Rdm.setZero();
  fun.d1Rdn.setZero();
  fun.d1Rdt.setZero();
  fun.d2Rdtda.setZero();
  fun.d2Rdtdadot.setZero();
  fun.d2Rdtdb.setZero();
  fun.d2Rdtdbdot.setZero();
  fun.d2Rdtdm.setZero();
  fun.d2Rdtdmdot.setZero();
  fun.d2Rdtdn.setZero();
  fun.d2Rdtdndot.setZero();
  fun.d2Rdada.setZero();
  fun.d2Rdadb.setZero();
  fun.d2Rdadn.setZero();
  fun.d2Rdadm.setZero();
  fun.d2Rdbda.setZero();
  fun.d2Rdbdb.setZero();
  fun.d2Rdbdn.setZero();
  fun.d2Rdbdm.setZero();
  fun.d2Rdnda.setZero();
  fun.d2Rdndb.setZero();
  fun.d2Rdndn.setZero();
  fun.d2Rdndm.setZero();
  fun.d2Rdmda.setZero();
  fun.d2Rdmdb.setZero();
  fun.d2Rdmdn.setZero();
  fun.d2Rdmdm.setZero();
  fun.d1rda.setZero();
  fun.d1rdb.setZero();
  fun.d1rdm.setZero();
  fun.d1rdn.setZero();
  fun.d1rdt.setZero();
  fun.d2rdtda.setZero();
  fun.d2rdtdadot.setZero();
  fun.d2rdtdb.setZero();
  fun.d2rdtdbdot.setZero();
  fun.d2rdtdm.setZero();
  fun.d2rdtdmdot.setZero();
  fun.d2rdtdn.setZero();
  fun.d2rdtdndot.setZero();
  fun.d2rdada.setZero();
  fun.d2rdadb.setZero();
  fun.d2rdadn.setZero();
  fun.d2rdadm.setZero();
  fun.d2rdbda.setZero();
  fun.d2rdbdb.setZero();
  fun.d2rdbdn.setZero();
  fun.d2rdbdm.setZero();
  fun.d2rdnda.setZero();
  fun.d2rdndb.setZero();
  fun.d2rdndn.setZero();
  fun.d2rdndm.setZero();
  fun.d2rdmda.setZero();
  fun.d2rdmdb.setZero();
  fun.d2rdmdn.setZero();
  fun.d2rdmdm.setZero();
  fun.r=r;
  fun();
  Eigen::FullPivHouseholderQR<Eigen::Matrix4d> solver(fun.d2Ecdqdot2);
  solver.setThreshold(1.e-16);
  std::cout<<" =========== Solving =========== "<<std::endl;;
  std::cout<<fun.d2Ecdqdot2<<std::endl<<std::endl;
  std::cout<<fun.sysB.transpose()<<std::endl<<std::endl;
  Eigen::SelfAdjointEigenSolver<Eigen::Matrix4d> eigensolver(fun.d2Ecdqdot2);
  std::cout<<"eigenvalues: "<<eigensolver.eigenvalues().transpose()<<std::endl<<std::endl;
  
  std::cout<<"eigenvectors "<<std::endl<<eigensolver.eigenvectors()<<std::endl;
  Eigen::Vector4d acceleration = solver.solve(fun.sysB);
  std::cout<<"acceleration = "<<acceleration.transpose()<<std::endl;
  this->acc=acceleration;
  this->E=fun.Eg+fun.Ew;
  cached = true;
  return acceleration;
}

double RubanDynOneSegmentApproxTige::computeE()
{
  if(cached)return E;
  computeAcc();
  return E;
}


void RubanDynOneSegmentApproxTige::DummyStep()
{
  //std::ofstream os;
  auto& os=std::cout;
  PtV acc = computeAcc();
  os << "Point 0"<<std::endl;
  print(os);
  this->set(sysPt+dt*sysPtdot,sysPtdot+dt*acc);
  os << "Point 1"<<std::endl;
    print(os);
}

void RubanDynOneSegmentApproxTige::RK4()
{
  //std::ofstream os;
  auto& os=std::cout;
  Eigen::VectorXd k1, k2, k3, k4;
  double dt2=dt*dt;
  PtV yn=sysPt;
  PtV yndot=sysPtdot;
  os << "RK4"<<std::endl;
  {
    os << "Point 0"<<std::endl;
    print(os);
    k1=computeAcc();
  }
  {
    set(yn+0.5*dt*yndot,yndot+0.5*dt*k1);
    os << "Point 1"<<std::endl;
    print(os);
    k2=computeAcc();
  }
  {
    set(yn+0.5*dt*yndot+0.25*dt2*k1,yndot+0.5*dt*k2);
    os << "Point 2"<<std::endl;
    print(os);
    k3=computeAcc();
  }
  {
    set(yn+dt*yndot+0.5*dt2*k2,yndot+dt*k3);
    os << "Point 3"<<std::endl;
    print(os);
    k4=computeAcc();
  }
  
  Eigen::VectorXd ynp=yn+dt*yndot+(dt2/6.)*(k1+k2+k3);
  Eigen::VectorXd ynpdot=yndot+(dt/6)*(k1+2*k2+2*k3+k4);
  
  set(ynp,ynpdot);
  std::cout << "Point final"<<std::endl;
  print(os);
}

void RubanDynOneSegmentApproxTige::step()
{
  if(virtuaDamping>0.)
  {
    sysPtdot*=(1.-virtuaDamping);
  }
  if(useRK4)RK4();
  else DummyStep();
}
