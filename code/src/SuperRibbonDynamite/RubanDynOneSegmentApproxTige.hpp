/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef RUBANDYNONESEGMENTAPPROXTIGE_H
#define RUBANDYNONESEGMENTAPPROXTIGE_H

#include <array>
#include <iostream>
#include <Eigen/Dense>

class RubanDynOneSegmentApproxTige
{
public:
  RubanDynOneSegmentApproxTige();
  using PtV = Eigen::Vector4d;
  PtV getPoint() const;
  PtV getPointSpeed() const;
  void set(PtV pt, PtV speed);
  void print(std::ostream& os) const;
  std::ostream& operator<<(std::ostream& os) const;
  void setupGeo(Eigen::Vector3d pos, Eigen::Matrix3d frame, double ribbonWidth, double ribbonLength, double curvANat, double curvBNat);
  void setupPhy(Eigen::Vector3d gravity, double D, double areaDensity, double nu);
  void setupTimeDynamic(double dt, bool useRK4, double virtuaDamping);
  void step();
  PtV computeAcc();
  double computeE();
private:
  void RK4();
  void DummyStep();
  
  PtV sysPt;
  PtV sysPtdot;
  
  double virtuaDamping;
  bool useRK4;
  double dt;
  
  
  Eigen::Vector3d G;
  double D;
  double areaDensity;
  double width;
  double nu;
  double length;
  Eigen::Matrix3d R;
  Eigen::Vector3d r;
  double anat;
  double bnat;
  
  bool cached=false;
  PtV acc;
  double E;
};

#endif // RUBANDYNONESEGMENTAPPROXTIGE_H
