/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "RungeKutta4.hpp"
#include <iostream>
#include <fstream>

MovingRibbon RK4(MovingRibbon mr, double h, Eigen::Vector3d G)
{
  std::ofstream os;
  //auto& os=std::cout;
  Eigen::VectorXd k1, k2, k3, k4;
  double h2=h*h;
  auto [yn,yndot] = mr.getPointAndSpeed();
  os << "RK4"<<std::endl;
  {
    SystemDynamite sys(mr, G);
    os << "Point 0"<<std::endl;
    mr.printPoint(os);
    sys();
    k1=sys.acceleration;
  }
  {
    mr.setPointAndSpeed(yn+0.5*h*yndot,yndot+0.5*h*k1);
    os << "Point 1"<<std::endl;
    mr.printPoint(os);
    SystemDynamite sys2(mr, G);
    sys2();
    k2=sys2.acceleration;
  }
  {
    mr.setPointAndSpeed(yn+0.5*h*yndot+0.25*h2*k1,yndot+0.5*h*k2);
    os << "Point 2"<<std::endl;
    mr.printPoint(os);
    SystemDynamite sys3(mr, G);
    sys3();
    k3=sys3.acceleration;
  }
  {
    mr.setPointAndSpeed(yn+h*yndot+0.5*h2*k2,yndot+h*k3);
    os << "Point 3"<<std::endl;
    mr.printPoint(os);
    SystemDynamite sys4(mr, G);
    sys4();
    k4=sys4.acceleration;
  }
  
  Eigen::VectorXd ynp=yn+h*yndot+(h2/6.)*(k1+k2+k3);
  Eigen::VectorXd ynpdot=yndot+(h/6)*(k1+2*k2+2*k3+k4);
  
  mr.setPointAndSpeed(ynp,ynpdot);
  std::cout << "Point final"<<std::endl;
  mr.printPoint();
  return mr;
}

MovingRibbon DummyStep(MovingRibbon mr, double h, Eigen::Vector3d G)
{
  auto [yn,yndot] = mr.getPointAndSpeed();
  
  SystemDynamite sys(mr, G);
  sys();
  auto acc=sys.acceleration;
//   mr.setPointAndSpeed(yn+h*yndot,0.95*(yndot+h*acc));
  std::cout<<" acc="<<acc.norm()<<" "<<" v="<<yndot.norm()<<" "<<std::endl;
  std::cout<<" acc="<<acc.transpose()<<std::endl;
//   std::cout << "Point final"<<std::endl;
//   mr.printPoint();
  return mr;
}
