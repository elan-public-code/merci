/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef RUNGE_KUTTA_4_HPP 
#define RUNGE_KUTTA_4_HPP

#include "../SuperRibbonDynamite/MovingRibbon.hpp"
#include "../SuperRibbonDynamite/SystemDynamite.hpp"

MovingRibbon RK4(MovingRibbon mr, double hstep, Eigen::Vector3d G);
MovingRibbon DummyStep(MovingRibbon mr, double hstep, Eigen::Vector3d G);

#endif
