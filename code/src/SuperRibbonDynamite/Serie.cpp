/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "Serie.hpp"

Serie::Serie(std::vector<real> terms, real var) :
  terms(terms),
  nbTerms(terms.size()),
  var(var)
{
  static_assert(sizeof(uint8_t)==1);
}

SerieVec::SerieVec(std::vector<Eigen::Vector3r> terms, real var) :
  terms(terms),
  nbTerms(terms.size()),
  var(var)
{
}

SerieMat::SerieMat(std::vector<Eigen::Matrix3r> terms, real var) :
  terms(terms),
  nbTerms(terms.size()),
  var(var)
{
}

int Serie::size()
{
  return nbTerms;
}

int SerieVec::size()
{
  return nbTerms;
}

int SerieMat::size()
{
  return nbTerms;
}

const real & Serie::operator[](int i)
{
  return terms[i];
}

const Eigen::Vector3r & SerieVec::operator[](int i)
{
  return terms[i];  
}

const Eigen::Matrix3r & SerieMat::operator[](int i)
{
  return terms[i];
}


real Serie::sum()
{
  if(theSum)return *theSum;
  std::vector<real> spowers;
  spowers.push_back( real(1.) );
  spowers.push_back(var);
  for (size_t i = 2; i < nbTerms; i++) {
    if (i % 2)
      spowers.push_back(spowers[i / 2] * spowers[i / 2 + 1]);
    else
      spowers.push_back(spowers[i / 2] * spowers[i / 2]);
  }
  
  real res = terms.at(0);
  for (size_t i = 1; i < nbTerms; i++)
    res += spowers.at(i) * terms.at(i);
  theSum.reset(new real(res));
  return res;
}

Eigen::Vector3r SerieVec::sum()
{
  if(theSum)return *theSum;
  std::vector<real> spowers;
  spowers.push_back( real(1.) );
  spowers.push_back(var);
  for (size_t i = 2; i < nbTerms; i++) {
    if (i % 2)
      spowers.push_back(spowers[i / 2] * spowers[i / 2 + 1]);
    else
      spowers.push_back(spowers[i / 2] * spowers[i / 2]);
  }
  
  Eigen::Vector3r res = terms.at(0);
  for (size_t i = 1; i < nbTerms; i++)
    res += spowers.at(i) * terms.at(i);
  theSum.reset(new Eigen::Vector3r(res));
  return res;
}

Eigen::Matrix3r SerieMat::sum()
{
  if(theSum)return *theSum;
  std::vector<real> spowers;
  spowers.push_back( real(1.) );
  spowers.push_back(var);
  for (size_t i = 2; i < nbTerms; i++) {
    if (i % 2)
      spowers.push_back(spowers[i / 2] * spowers[i / 2 + 1]);
    else
      spowers.push_back(spowers[i / 2] * spowers[i / 2]);
  }
  
  Eigen::Matrix3r res = terms.at(0);
  for (size_t i = 1; i < nbTerms; i++)
    res += spowers.at(i) * terms.at(i);
  theSum.reset(new Eigen::Matrix3r(res));
  return res;
}

Serie Serie::derivate() const
{
  std::vector<real> vres;
  for (size_t i = 1; i < nbTerms; i++)
    vres.push_back(real(i)*terms.at(i));
  return Serie(vres, var);
}

SerieVec SerieVec::derivate() const
{
  std::vector<Eigen::Vector3r> vres;
  for (size_t i = 1; i < nbTerms; i++)
    vres.push_back(real(i)*terms.at(i));
  return SerieVec(vres, var);
}

SerieMat SerieMat::derivate() const
{
  std::vector<Eigen::Matrix3r> vres;
  for (size_t i = 1; i < nbTerms; i++)
    vres.push_back(real(i)*terms.at(i));
  return SerieMat(vres, var);
}

Serie Serie::integrate() const
{
  std::vector<real> vres;
  vres.push_back(0.);
  for (size_t i = 0; i < nbTerms; i++)
    vres.push_back(1./(i+1)*terms.at(i));
  return Serie(vres, var);
}

SerieVec SerieVec::integrate() const
{
  std::vector<Eigen::Vector3r> vres;
  vres.push_back(Eigen::Vector3r(0., 0., 0.));
  for (size_t i = 0; i < nbTerms; i++)
    vres.push_back(1./(i+1)*terms.at(i));
  return SerieVec(vres, var);
}

SerieMat SerieMat::integrate() const
{
  std::vector<Eigen::Matrix3r> vres;
  vres.push_back(Eigen::Matrix3r::Zero());
  for (size_t i = 0; i < nbTerms; i++)
    vres.push_back(1./(i+1)*terms.at(i));
  return SerieMat(vres, var);
}


Serie SerieVec::dot(const SerieVec& other) const
{
  std::vector<real> vres;
  size_t order = std::min(nbTerms, other.nbTerms);
  vres.push_back(terms.at(0).dot(other.terms.at(0)));
  for(size_t i=1; i<order; i++)
  {
    real tsum=0.;
    for(size_t j=0; j<=i; j++)
      tsum += terms.at(j).dot(other.terms.at(i-j));
    vres.push_back(tsum);
  }
  return Serie(vres, var);
}

Serie SerieVec::dot(const Eigen::Vector3r& other) const
{
  std::vector<real> vres;
  for(size_t i=0; i<nbTerms; i++)
    vres.push_back(terms.at(i).dot(other));
  return Serie(vres, var);
}

Serie Serie::operator+(const Serie& other) const
{
  std::vector<real> vres;
  size_t order = std::min(nbTerms, other.nbTerms);
  for(size_t i=0; i<order; i++)
    vres.push_back(terms.at(i)+other.terms.at(i));
  return Serie(vres, var);
}

SerieVec SerieVec::operator+(const SerieVec& other) const
{
  std::vector<Eigen::Vector3r> vres;
  size_t order = std::min(nbTerms, other.nbTerms);
  for(size_t i=0; i<order; i++)
    vres.push_back(terms.at(i)+other.terms.at(i));
  return SerieVec(vres, var);
}

SerieMat SerieMat::operator+(const SerieMat& other) const
{
  std::vector<Eigen::Matrix3r> vres;
  size_t order = std::min(nbTerms, other.nbTerms);
  for(size_t i=0; i<order; i++)
    vres.push_back(terms.at(i)+other.terms.at(i));
  return SerieMat(vres, var);
}

SerieVec SerieVec::operator+(const Eigen::Vector3r& other) const
{
  std::vector<Eigen::Vector3r> vres = terms;
  vres.at(0)+=other;
  return SerieVec(vres, var);
}


Serie Serie::operator-(const Serie& other) const
{
  std::vector<real> vres;
  size_t order = std::min(nbTerms, other.nbTerms);
  for(size_t i=0; i<order; i++)
    vres.push_back(terms.at(i)-other.terms.at(i));
  return Serie(vres, var);
}

Serie Serie::operator*(const real& other) const
{
  std::vector<real> vres;
  for(real sv:terms)
    vres.push_back(sv*other);
  return Serie(vres, var);
}

SerieMat Serie::operator*(const Eigen::Matrix3r& other) const
{
  std::vector<Eigen::Matrix3r> vres;
  for(real sv:terms)
    vres.push_back(sv*other);
  return SerieMat(vres, var);
}

SerieVec SerieVec::operator*(const real& other) const
{
  std::vector<Eigen::Vector3r> vres;
  for(Eigen::Vector3r sv:terms)
    vres.push_back(other*sv);
  return SerieVec(vres, var);
}

Serie Serie::operator/(const real& other) const
{
  std::vector<real> vres;
  for(real sv:terms)
    vres.push_back(sv/other);
  return Serie(vres, var);
}

SerieVec operator+(const Eigen::Vector3r& a, const SerieVec& b)
{
  return b+a;
}

Serie operator*(const real& a, const Serie& b)
{
  return b*a;
}

SerieVec operator*(const real& a, const SerieVec& b)
{
  return b*a;
}

SerieVec SerieMat::col(int i) const
{
  std::vector<Eigen::Vector3r> vres;
  for(Eigen::Matrix3r sv:terms)
    vres.push_back(sv.col(i));
  return SerieVec(vres, var);
}

SerieMat SerieMat::operator*(const Eigen::Matrix3r& other) const
{
  std::vector<Eigen::Matrix3r> vres;
  for(Eigen::Matrix3r sv:terms)
    vres.push_back(sv*other);
  return SerieMat(vres, var);
}

SerieMat SerieMat::operator*(const real& other) const
{
  std::vector<Eigen::Matrix3r> vres;
  for(Eigen::Matrix3r sv:terms)
    vres.push_back(sv*other);
  return SerieMat(vres, var);
}

SerieVec SerieMat::operator*(const SerieVec& other) const
{
  //whole product
  std::vector<Eigen::Vector3r> vres;
  int order=nbTerms+other.nbTerms;
  vres.push_back(terms.at(0)*other.terms.at(0));
  for(int i=1; i<order; i++)
  {
    Eigen::Vector3r tsum=Eigen::Vector3r::Zero();
    for(int j=std::max(0,i-static_cast<int>(other.nbTerms)+1); j<=i&&j<static_cast<int>(nbTerms); j++)
    {
      tsum += terms.at(j)*other.terms.at(i-j);
    }
    vres.push_back(tsum);
  }
  return SerieVec(vres, var);
}

SerieMat Serie::operator*(const SerieMat& other) const
{
  //whole product
  std::vector<Eigen::Matrix3r> vres;
  int order=nbTerms+other.nbTerms;
  vres.push_back(terms.at(0)*other.terms.at(0));
  for(int i=1; i<order; i++)
  {
    Eigen::Matrix3r tsum=Eigen::Matrix3r::Zero();
    for(int j=std::max(0,i-static_cast<int>(other.nbTerms)+1); j<=i&&j<static_cast<int>(nbTerms); j++)
    {
      tsum += terms.at(j)*other.terms.at(i-j);
    }
    vres.push_back(tsum);
  }
  return SerieMat(vres, var);
}

SerieMat SerieMat::operator*(const SerieMat& other) const
{
  //whole product
  std::vector<Eigen::Matrix3r> vres;
  int order=nbTerms+other.nbTerms;
  vres.push_back(terms.at(0)*other.terms.at(0));
  for(int i=1; i<order; i++)
  {
    Eigen::Matrix3r tsum=Eigen::Matrix3r::Zero();
    for(int j=std::max(0,i-static_cast<int>(other.nbTerms)+1); j<=i&&j<static_cast<int>(nbTerms); j++)
    {
      tsum += terms.at(j)*other.terms.at(i-j);
    }
    vres.push_back(tsum);
  }
  return SerieMat(vres, var);
}

Serie Serie::operator*(const Serie& other) const
{
  //whole product
  std::vector<real> vres;
  int order=nbTerms+other.nbTerms;
  vres.push_back(terms.at(0)*other.terms.at(0));
  for(int i=1; i<order; i++)
  {
    real tsum=0.;
    for(int j=std::max(0,i-static_cast<int>(other.nbTerms)+1); j<=i&&j<static_cast<int>(nbTerms); j++)
    {
      tsum += terms.at(j)*other.terms.at(i-j);
    }
    vres.push_back(tsum);
  }
  return Serie(vres, var);
}

SerieMat SerieMat::multAsRightObject(const Eigen::Matrix3r& other) const
{
  std::vector<Eigen::Matrix3r> vres;
  for(Eigen::Matrix3r sv:terms)
    vres.push_back(other*sv);
  return SerieMat(vres, var);
}

SerieMat operator*(const Eigen::Matrix3r& a, const SerieMat& b)
{
  return b.multAsRightObject(a);
}

SerieMat operator*(const real& a, const SerieMat& b)
{
  return b*a;
}

SerieVec SerieVec::multAsRightObject(const Eigen::Matrix3r& other) const
{
  std::vector<Eigen::Vector3r> vres;
  for(Eigen::Vector3r sv:terms)
    vres.push_back(other*sv);
  return SerieVec(vres, var);
}

SerieVec operator*(const Eigen::Matrix3r& a, const SerieVec& b)
{
  return b.multAsRightObject(a);
}
