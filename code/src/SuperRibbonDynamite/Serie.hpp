/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef SERIE_H
#define SERIE_H

#include <Eigen/Dense>
#include <memory>
#include <optional>
#include <BaseCore/real.hpp>
#include <BaseCore/LinearScalar.hpp>

class SerieMat;

class Serie
{
public:
  Serie(std::vector<real> terms, real var);
  real sum();
  Serie derivate() const;
  Serie integrate() const;
//   Serie derivateWithTime() const;
//   Serie derivate(SymbolicVar v) const;
  Serie operator +(const Serie& other) const;
  Serie operator -(const Serie& other) const;
  Serie operator *(const real& other) const;
  SerieMat operator *(const Eigen::Matrix3r& other) const;
  SerieMat operator *(const SerieMat& other) const;
  Serie operator *(const Serie& other) const;
  Serie operator /(const real& other) const;
  int size();
  const real& operator[](int i);
private:
  std::vector<real> terms;
  size_t nbTerms;
  real var;
  std::unique_ptr<real> theSum;
  friend class SerieMat;
};

Serie operator*(const real& a, const Serie& b);

class SerieVec
{
public:
  SerieVec(std::vector<Eigen::Vector3r> terms, real var);
  Eigen::Vector3r sum();
  SerieVec derivate() const;
  SerieVec integrate() const;
//   SerieVec derivateWithTime() const;
//   SerieVec derivate(SymbolicVar v) const;
  Serie dot(const SerieVec& other) const;
  Serie dot(const Eigen::Vector3r& other) const;
  
  SerieVec operator +(const SerieVec& other) const;
  SerieVec operator +(const Eigen::Vector3r& other) const;
  SerieVec operator *(const real& other) const;
  SerieVec multAsRightObject(const Eigen::Matrix3r& other) const;
  
  int size();
  const Eigen::Vector3r& operator[](int i);
private:
  std::vector<Eigen::Vector3r> terms;
  size_t nbTerms;
  real var;
  std::unique_ptr<Eigen::Vector3r> theSum;
  friend class SerieMat;
};

SerieVec operator+(const Eigen::Vector3r& a, const SerieVec& b);
SerieVec operator*(const real& a, const SerieVec& b);
SerieVec operator *(const Eigen::Matrix3r& a, const SerieVec& b);

class SerieMat
{
public:
  SerieMat(std::vector<Eigen::Matrix3r> terms, real var);
  Eigen::Matrix3r sum();
  SerieMat derivate() const;
  SerieMat integrate() const;
//   SerieMat derivateWithTime() const;
//   SerieMat derivate(SymbolicVar v) const;
  SerieVec col(int i) const;
  
  SerieMat operator *(const Eigen::Matrix3r& other) const;
  SerieMat operator *(const real& other) const;
  SerieVec operator *(const SerieVec& other) const;
  SerieMat operator *(const SerieMat& other) const;
  SerieMat multAsRightObject(const Eigen::Matrix3r& other) const;
  SerieMat operator +(const SerieMat& other) const;
  
  int size();
  const Eigen::Matrix3r& operator[](int i);
private:
  std::vector<Eigen::Matrix3r> terms;
  size_t nbTerms;
  real var;
  std::unique_ptr<Eigen::Matrix3r> theSum;
  friend class Serie;
};

SerieMat operator *(const Eigen::Matrix3r& a, const SerieMat& b);
SerieMat operator *(const real& a, const SerieMat& b);

#endif // SERIE_H
