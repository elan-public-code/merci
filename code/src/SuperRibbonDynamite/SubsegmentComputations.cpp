/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "SubsegmentComputations.hpp"
#include "Log/Logger.hpp"
#include<type_traits>

Eigen::Matrix3r SubSegmentComputations::skewOf(Eigen::Vector3r w)
{
  Eigen::Matrix3r res;
  res << 0,    -w[2], w[1],
      w[2],  0,    -w[0],
      -w[1], w[0], 0;
  return res;
}

void SubSegmentComputations::Place(Eigen::Vector3r pos, Eigen::Matrix3r frame, Eigen::Vector3r dposdt, Eigen::Matrix3r dframedt)
{
  called.clear();
  posOr = pos;
  frameOr = frame;
  dposOrdt = dposdt;
  dframeOrdt = dframedt;
}

void SubSegmentComputations::Setup(LinearScalar omega1, LinearScalar eta, LinearScalar omega1Dot, LinearScalar etaDot)
{
  if (false) {
    LoggerMsg message("Setup");
    message.addChild("omega1", std::to_string(omega1.A)+", "+std::to_string(omega1.B));
    message.addChild("eta", std::to_string(eta.A)+", "+std::to_string(eta.B));
    getLogger()->Write(LoggerLevel::INFORMATION, "Serie", message);
  }
  called.clear();
  this->omega1 = omega1;
  this->eta = eta;
  this->omega1Dot = omega1Dot;
  this->etaDot = etaDot;
  lambda0Sq = skewOf(Eigen::Vector3r(omega1.B, 0.0, eta.B * omega1.B));
  lambda1Sq = skewOf(Eigen::Vector3r(omega1.A, 0.0, eta.B * omega1.A + eta.A * omega1.B));
  lambda2Sq = skewOf(Eigen::Vector3r(0.,       0.0, eta.A * omega1.A));
  lambda0 = Eigen::Vector3r(omega1.B, 0.0, eta.B * omega1.B);
  lambda1 = Eigen::Vector3r(omega1.A, 0.0, eta.B * omega1.A + eta.A * omega1.B);
  lambda2 = Eigen::Vector3r(0.,       0.0, eta.A * omega1.A);
  lambda = std::max(
             lambda0.lpNorm<Eigen::Infinity>(),
             std::max(lambda1.lpNorm<Eigen::Infinity>(),
                      lambda2.lpNorm<Eigen::Infinity>()));
}

void SubSegmentComputations::SetRibbonWidth(real w)
{
  called.clear();
  ribbonWidth = w;
}

real SubSegmentComputations::estimRemainderOfTheSerie(int order, real s)
{
  assert(s > 0);
  assert(order > 3);
  return std::exp(C(2 * s)) / pow(2., order - 1);
}

int SubSegmentComputations::estimNeededOrder()
{
  return (52 * 3 / 2);
}

real SubSegmentComputations::C(real s)
{
  return 2.*std::abs(s) *
         (
           lambda0.lpNorm<Eigen::Infinity>()
           + (s * lambda1).lpNorm<Eigen::Infinity>() +
           (s * s * lambda2).lpNorm<Eigen::Infinity>()
         );
}

real SubSegmentComputations::estimSMax()
{
  real T_2 = (52. / 2.) * std::log(2) / 2.;
  real l0 = lambda0.lpNorm<Eigen::Infinity>();
  real l1 = lambda1.lpNorm<Eigen::Infinity>();
  real l2 = lambda2.lpNorm<Eigen::Infinity>();
  l0 = std::max(static_cast<real>(1.), l0);
  l1 = std::max(static_cast<real>(1.), l1);
  l2 = std::max(static_cast<real>(1.), l2);

  if (std::isnan(l0) || std::isnan(l1) || std::isnan(l2)) {
    return std::numeric_limits<real>::infinity();
  }

  real res = std::numeric_limits<real>::infinity();
  //TODO search a better solution
  if (l0 + l1 + l2 < T_2) { //Smax>1
    res = std::cbrt(T_2 / (l0 + l1 + l2));
  } else { //Smax<1
    res = T_2 / (l0 + l1 + l2);
  }

  if (res < 1.e-8) {
    LoggerMsg message("Very small smax");
    message.addChild("smax", std::to_string(res));
    message.addChild("l0", std::to_string(l0));
    message.addChild("l1", std::to_string(l1));
    message.addChild("l2", std::to_string(l2));
    getLogger()->Write(LoggerLevel::WARNING, "Serie", message);
    res = std::numeric_limits<real>::infinity();
  }

  return res / 2.;
}

void SubSegmentComputations::computeSeriePreTerms()
{
  computeSeriePreTerms(estimNeededOrder());
}

void SubSegmentComputations::computeSeriePreTerms(int order)
{
  if (called["computeSeriePreTerms"])return;
  assert(order > 3);
  FramePreTerms.clear();
  FramePreTerms.resize(order+1);
  FramePreTerms[0]=Eigen::Matrix3r::Identity();
  FramePreTerms[1]=lambda0Sq;
  FramePreTerms[2]=0.5 * (FramePreTerms[1]*lambda0Sq + lambda1Sq);
  for (int n = 3; n <= order; n++) {
    FramePreTerms[n]=(1. / n) *
                            (
                              FramePreTerms[n - 1]*lambda0Sq
                              + FramePreTerms[n - 2]*lambda1Sq
                              + FramePreTerms[n - 3]*lambda2Sq
                            );
  }
  PosPreTerms.resize(order+1);
  PosPreTerms[0]=Eigen::Vector3r(NAN, NAN, NAN);
  for (int n = 1; n <= order; n++) {
    PosPreTerms[n]=(1. / n) * (
                            FramePreTerms[n - 1].col(2));
  }
  called["computeSeriePreTerms"] = true;
}

Eigen::Matrix3r SubSegmentComputations::Frame(real s)
{
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  int order = FramePreTerms.size();
  computeSPowers(s);
  Eigen::Matrix3r res = FramePreTerms[0];
  for (int i = 1; i < order; i++)
    res += spowers[i] * FramePreTerms[i];
  return frameOr * res;
}

Eigen::Matrix3r SubSegmentComputations::PreFrame(real s)
{
    if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  int order = FramePreTerms.size();
  computeSPowers(s);
  Eigen::Matrix3r res = FramePreTerms[0];
  for (int i = 1; i < order; i++)
    res += spowers[i] * FramePreTerms[i];
  return res;
}


Eigen::Vector3r SubSegmentComputations::Pos(real s)
{
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  int order = PosPreTerms.size();
  computeSPowers(s);
  Eigen::Vector3r res(0., 0., 0.);
  //for (int i = 1; i < order; i++)
  for (int i = order-1; i > 0; i--)
    res += spowers[i] * PosPreTerms[i];
  res = frameOr * res;
  res += posOr;
  return res;
}

Eigen::Vector3r SubSegmentComputations::PreTranslation(real s)
{
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  int order = PosPreTerms.size();
  computeSPowers(s);
  Eigen::Vector3r res(0., 0., 0.);
  //for (int i = 1; i < order; i++)
  for (int i = order-1; i > 0; i--)
    res += spowers[i] * PosPreTerms[i];
  return res;
}

std::pair<Eigen::Vector3r, Eigen::Matrix3r> SubSegmentComputations::PosFrame(real s)
{
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  int order = FramePreTerms.size();
  computeSPowers(s);
  Eigen::Vector3r resPos(0., 0., 0.);
  //for (int i = 1; i < order; i++)
  for (int i = order-1; i > 0; i--)
    resPos += spowers[i] * PosPreTerms[i];
  resPos = frameOr * resPos;
  resPos += posOr;
  Eigen::Matrix3r resFrame = FramePreTerms[0];
  for (int i = 1; i < order; i++) {
    resFrame += spowers[i] * FramePreTerms[i];
  }
  return std::make_pair(resPos, frameOr * resFrame);
}

void SubSegmentComputations::computeDerivationsPreTerms()
{
  if (called["computeDerivationsPreTerms"])return;
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  dLambda0db << 1., 0., eta.B;
  dLambda0dm << 0., 0., omega1.B;
  dLambda1da << 1., 0., eta.B;
  dLambda1db << 0., 0., eta.A;
  dLambda1dn << 0., 0., omega1.B;
  dLambda1dm << 0., 0., omega1.A;
  dLambda2da << 0., 0., eta.A;
  dLambda2dn << 0., 0., omega1.A;

  dLambda0dbSq = skewOf(dLambda0db);
  dLambda0dmSq = skewOf(dLambda0dm);
  dLambda1daSq = skewOf(dLambda1da);
  dLambda1dbSq = skewOf(dLambda1db);
  dLambda1dnSq = skewOf(dLambda1dn);
  dLambda1dmSq = skewOf(dLambda1dm);
  dLambda2daSq = skewOf(dLambda2da);
  dLambda2dnSq = skewOf(dLambda2dn);

  int order = FramePreTerms.size();
  assert(order > 0);

  //Derivation of the frame
  {
    dFramePreTermsda.resize(order);
    dFramePreTermsdb.resize(order);
    dFramePreTermsdn.resize(order);
    dFramePreTermsdm.resize(order);
    
    Eigen::Matrix3r zero;
    zero.setZero();
    //order 0
    dFramePreTermsda[0]=zero;
    dFramePreTermsdb[0]=zero;
    dFramePreTermsdn[0]=zero;
    dFramePreTermsdm[0]=zero;
    //order 1
    dFramePreTermsda[1]=zero;
    dFramePreTermsdb[1]=dLambda0dbSq;
    dFramePreTermsdn[1]=zero;
    dFramePreTermsdm[1]=dLambda0dmSq;
    //order 2
    dFramePreTermsda[2]=0.5 * dLambda1daSq;
    dFramePreTermsdb[2]=0.5 * (dLambda0dbSq * lambda0Sq + lambda0Sq * dLambda0dbSq + dLambda1dbSq);
    dFramePreTermsdn[2]=0.5 * dLambda1dnSq;
    dFramePreTermsdm[2]=0.5 * (dLambda0dmSq * lambda0Sq + lambda0Sq * dLambda0dmSq + dLambda1dmSq);
    //order n
    for (int n = 3; n < order; n++) {
      dFramePreTermsda[n]=((1. / static_cast<real>(n)) * (
                                   FramePreTerms[n - 2]*dLambda1daSq + FramePreTerms[n - 3]*dLambda2daSq
                                   + dFramePreTermsda[n - 1]*lambda0Sq + dFramePreTermsda[n - 2]*lambda1Sq + dFramePreTermsda[n - 3]*lambda2Sq));
      dFramePreTermsdb[n]=((1. / static_cast<real>(n)) * (
                                   FramePreTerms[n - 1]*dLambda0dbSq + FramePreTerms[n - 2]*dLambda1dbSq
                                   + dFramePreTermsdb[n - 1]*lambda0Sq + dFramePreTermsdb[n - 2]*lambda1Sq + dFramePreTermsdb[n - 3]*lambda2Sq));
      dFramePreTermsdn[n]=((1. / static_cast<real>(n)) * (
                                   FramePreTerms[n - 2]*dLambda1dnSq + FramePreTerms[n - 3]*dLambda2dnSq
                                   + dFramePreTermsdn[n - 1]*lambda0Sq + dFramePreTermsdn[n - 2]*lambda1Sq + dFramePreTermsdn[n - 3]*lambda2Sq));
      dFramePreTermsdm[n]=((1. / static_cast<real>(n)) * (
                                   FramePreTerms[n - 1]*dLambda0dmSq + FramePreTerms[n - 2]*dLambda1dmSq
                                   + dFramePreTermsdm[n - 1]*lambda0Sq + dFramePreTermsdm[n - 2]*lambda1Sq + dFramePreTermsdm[n - 3]*lambda2Sq));
    }
  }

  //Derivation of the position
  {
    dPosPreTermsda.resize(order);
    dPosPreTermsdb.resize(order);
    dPosPreTermsdn.resize(order);
    dPosPreTermsdm.resize(order);

    Eigen::Vector3r zero;
    zero.setZero();
    Eigen::Vector3r vnan(NAN, NAN, NAN);

    //order 0
    dPosPreTermsda[0]=vnan;
    dPosPreTermsdb[0]=vnan;
    dPosPreTermsdn[0]=vnan;
    dPosPreTermsdm[0]=vnan;
    //order 1
    dPosPreTermsda[1]=zero;
    dPosPreTermsdb[1]=zero;
    dPosPreTermsdn[1]=zero;
    dPosPreTermsdm[1]=zero;
    //order n
    for (int n = 2; n < order; n++) {
      dPosPreTermsda[n]=(1. / static_cast<real>(n))*dFramePreTermsda[n - 1].col(2);
      dPosPreTermsdb[n]=(1. / static_cast<real>(n))*dFramePreTermsdb[n - 1].col(2);
      dPosPreTermsdn[n]=(1. / static_cast<real>(n))*dFramePreTermsdn[n - 1].col(2);
      dPosPreTermsdm[n]=(1. / static_cast<real>(n))*dFramePreTermsdm[n - 1].col(2);

    }
  }
  called["computeDerivationsPreTerms"] = true;
}

void SubSegmentComputations::computeDerivationsPreTermsSecondOrder()
{
  if (called["computeDerivationsPreTermsSecondOrder"])return;
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  dLambda0dbm << 0., 0., 1.;
  dLambda0dmb << 0., 0., 1.;
  dLambda1dam << 0., 0., 1.;
  dLambda1dbn << 0., 0., 1.;
  dLambda1dnb << 0., 0., 1.;
  dLambda1dma << 0., 0., 1.;
  dLambda2dan << 0., 0., 1.;
  dLambda2dna << 0., 0., 1.;
  dLambda0dbmSq = skewOf(dLambda0dbm);
  dLambda0dmbSq = skewOf(dLambda0dmb);
  dLambda1damSq = skewOf(dLambda1dam);
  dLambda1dbnSq = skewOf(dLambda1dbn);
  dLambda1dnbSq = skewOf(dLambda1dnb);
  dLambda1dmaSq = skewOf(dLambda1dma);
  dLambda2danSq = skewOf(dLambda2dan);
  dLambda2dnaSq = skewOf(dLambda2dna);

  int order = FramePreTerms.size();
  assert(order > 0);

  //Derivation of the frame
  {
    dFramePreTermsdaa.resize(order);
    dFramePreTermsdba.resize(order);
    dFramePreTermsdna.resize(order);
    dFramePreTermsdma.resize(order);
    dFramePreTermsdab.resize(order);
    dFramePreTermsdbb.resize(order);
    dFramePreTermsdnb.resize(order);
    dFramePreTermsdmb.resize(order);
    dFramePreTermsdan.resize(order);
    dFramePreTermsdbn.resize(order);
    dFramePreTermsdnn.resize(order);
    dFramePreTermsdmn.resize(order);
    dFramePreTermsdam.resize(order);
    dFramePreTermsdbm.resize(order);
    dFramePreTermsdnm.resize(order);
    dFramePreTermsdmm.resize(order);
    Eigen::Matrix3r zero;
    zero.setZero();
    //order 0
    dFramePreTermsdaa[0]=zero;
    dFramePreTermsdba[0]=zero;
    dFramePreTermsdna[0]=zero;
    dFramePreTermsdma[0]=zero;
    dFramePreTermsdab[0]=zero;
    dFramePreTermsdbb[0]=zero;
    dFramePreTermsdnb[0]=zero;
    dFramePreTermsdmb[0]=zero;
    dFramePreTermsdan[0]=zero;
    dFramePreTermsdbn[0]=zero;
    dFramePreTermsdnn[0]=zero;
    dFramePreTermsdmn[0]=zero;
    dFramePreTermsdam[0]=zero;
    dFramePreTermsdbm[0]=zero;
    dFramePreTermsdnm[0]=zero;
    dFramePreTermsdmm[0]=zero;
    //order 1
    dFramePreTermsdaa[1]=zero;
    dFramePreTermsdab[1]=zero;
    dFramePreTermsdan[1]=zero;
    dFramePreTermsdam[1]=zero;
    dFramePreTermsdba[1]=zero;
    dFramePreTermsdbb[1]=zero;
    dFramePreTermsdbn[1]=zero;
    dFramePreTermsdbm[1]=dLambda0dbmSq;
    dFramePreTermsdna[1]=zero;
    dFramePreTermsdnb[1]=zero;
    dFramePreTermsdnn[1]=zero;
    dFramePreTermsdnm[1]=zero;
    dFramePreTermsdma[1]=zero;
    dFramePreTermsdmb[1]=dLambda0dmbSq;
    dFramePreTermsdmn[1]=zero;
    dFramePreTermsdmm[1]=zero;
    //order 2
    dFramePreTermsdaa[2]=zero;
    dFramePreTermsdab[2]=zero;
    dFramePreTermsdan[2]=zero;
    dFramePreTermsdam[2]=0.5 * dLambda1damSq;
    dFramePreTermsdba[2]=zero;
    dFramePreTermsdbb[2]=dLambda0dbSq * dLambda0dbSq;
    dFramePreTermsdbn[2]=0.5 * dLambda1dbnSq;
    dFramePreTermsdbm[2]=0.5 * (dLambda0dbmSq * lambda0Sq + lambda0Sq * dLambda0dbmSq + dLambda0dbSq * dLambda0dmSq + dLambda0dmSq * dLambda0dbSq);
    dFramePreTermsdna[2]=zero;
    dFramePreTermsdnb[2]=0.5 * dLambda1dnbSq;
    dFramePreTermsdnn[2]=zero;
    dFramePreTermsdnm[2]=zero;
    dFramePreTermsdma[2]=0.5 * dLambda1dmaSq;
    dFramePreTermsdmb[2]=0.5 * (dLambda0dbmSq * lambda0Sq + lambda0Sq * dLambda0dbmSq + dLambda0dbSq * dLambda0dmSq + dLambda0dmSq * dLambda0dbSq);
    dFramePreTermsdmn[2]=zero;
    dFramePreTermsdmm[2]=dLambda0dmSq * dLambda0dmSq;
    //order n
    for (int n = 3; n < order; n++) {
      dFramePreTermsdaa[n]=((1. / static_cast<real>(n)) * (
                                    dFramePreTermsda[n - 2]*dLambda1daSq + dFramePreTermsda[n - 3]*dLambda2daSq
                                    + dFramePreTermsdaa[n - 1]*lambda0Sq + dFramePreTermsdaa[n - 2]*lambda1Sq + dFramePreTermsdaa[n - 3]*lambda2Sq
                                    + dFramePreTermsda[n - 2]*dLambda1daSq + dFramePreTermsda[n - 3]*dLambda2daSq));
      dFramePreTermsdab[n]=((1. / static_cast<real>(n)) * (
                                    dFramePreTermsdb[n - 2]*dLambda1daSq + dFramePreTermsdb[n - 3]*dLambda2daSq
                                    + dFramePreTermsda[n - 1]*dLambda0dbSq + dFramePreTermsda[n - 2]*dLambda1dbSq
                                    + dFramePreTermsdab[n - 1]*lambda0Sq + dFramePreTermsdab[n - 2]*lambda1Sq + dFramePreTermsdab[n - 3]*lambda2Sq));
      dFramePreTermsdan[n]=((1. / static_cast<real>(n)) * (
                                    FramePreTerms[n - 3]*dLambda2danSq
                                    + dFramePreTermsdn[n - 2]*dLambda1daSq + dFramePreTermsdn[n - 3]*dLambda2daSq
                                    + dFramePreTermsda[n - 2]*dLambda1dnSq + dFramePreTermsda[n - 3]*dLambda2dnSq
                                    + dFramePreTermsdan[n - 1]*lambda0Sq + dFramePreTermsdan[n - 2]*lambda1Sq + dFramePreTermsdan[n - 3]*lambda2Sq));
      dFramePreTermsdam[n]=((1. / static_cast<real>(n)) * (
                                    FramePreTerms[n - 2]*dLambda1damSq
                                    + dFramePreTermsdm[n - 2]*dLambda1daSq + dFramePreTermsdm[n - 3]*dLambda2daSq
                                    + dFramePreTermsda[n - 1]*dLambda0dmSq + dFramePreTermsda[n - 2]*dLambda1dmSq
                                    + dFramePreTermsdam[n - 1]*lambda0Sq + dFramePreTermsdam[n - 2]*lambda1Sq + dFramePreTermsdam[n - 3]*lambda2Sq));

      dFramePreTermsdba[n]=((1. / static_cast<real>(n)) * (
                                    dFramePreTermsda[n - 1]*dLambda0dbSq + dFramePreTermsda[n - 2]*dLambda1dbSq
                                    + dFramePreTermsdba[n - 1]*lambda0Sq + dFramePreTermsdba[n - 2]*lambda1Sq + dFramePreTermsdba[n - 3]*lambda2Sq
                                    + dFramePreTermsdb[n - 2]*dLambda1daSq + dFramePreTermsdb[n - 3]*dLambda2daSq));
      dFramePreTermsdbb[n]=((1. / static_cast<real>(n)) * (
                                    dFramePreTermsdb[n - 1]*dLambda0dbSq + dFramePreTermsdb[n - 2]*dLambda1dbSq
                                    + dFramePreTermsdbb[n - 1]*lambda0Sq + dFramePreTermsdbb[n - 2]*lambda1Sq + dFramePreTermsdbb[n - 3]*lambda2Sq
                                    + dFramePreTermsdb[n - 1]*dLambda0dbSq + dFramePreTermsdb[n - 2]*dLambda1dbSq));
      dFramePreTermsdbn[n]=((1. / static_cast<real>(n)) * (
                                    FramePreTerms[n - 2]*dLambda1dbnSq
                                    + dFramePreTermsdn[n - 1]*dLambda0dbSq + dFramePreTermsdn[n - 2]*dLambda1dbSq
                                    + dFramePreTermsdbn[n - 1]*lambda0Sq + dFramePreTermsdbn[n - 2]*lambda1Sq + dFramePreTermsdbn[n - 3]*lambda2Sq
                                    + dFramePreTermsdb[n - 2]*dLambda1dnSq + dFramePreTermsdb[n - 3]*dLambda2dnSq));
      dFramePreTermsdbm[n]=((1. / static_cast<real>(n)) * (
                                    FramePreTerms[n - 1]*dLambda0dbmSq
                                    + dFramePreTermsdm[n - 1]*dLambda0dbSq + dFramePreTermsdm[n - 2]*dLambda1dbSq
                                    + dFramePreTermsdbm[n - 1]*lambda0Sq + dFramePreTermsdbm[n - 2]*lambda1Sq + dFramePreTermsdbm[n - 3]*lambda2Sq
                                    + dFramePreTermsdb[n - 1]*dLambda0dmSq + dFramePreTermsdb[n - 2]*dLambda1dmSq));

      dFramePreTermsdna[n]=((1. / static_cast<real>(n)) * (
                                    FramePreTerms[n - 3]*dLambda2dnaSq
                                    + dFramePreTermsda[n - 2]*dLambda1dnSq + dFramePreTermsda[n - 3]*dLambda2dnSq
                                    + dFramePreTermsdna[n - 1]*lambda0Sq + dFramePreTermsdna[n - 2]*lambda1Sq + dFramePreTermsdna[n - 3]*lambda2Sq
                                    + dFramePreTermsdn[n - 2]*dLambda1daSq + dFramePreTermsdn[n - 3]*dLambda2daSq));
      dFramePreTermsdnb[n]=((1. / static_cast<real>(n)) * (
                                    FramePreTerms[n - 2]*dLambda1dnbSq
                                    + dFramePreTermsdb[n - 2]*dLambda1dnSq + dFramePreTermsdb[n - 3]*dLambda2dnSq
                                    + dFramePreTermsdnb[n - 1]*lambda0Sq + dFramePreTermsdnb[n - 2]*lambda1Sq + dFramePreTermsdnb[n - 3]*lambda2Sq
                                    + dFramePreTermsdn[n - 1]*dLambda0dbSq + dFramePreTermsdn[n - 2]*dLambda1dbSq));
      dFramePreTermsdnn[n]=((1. / static_cast<real>(n)) * (
                                    dFramePreTermsdn[n - 2]*dLambda1dnSq + dFramePreTermsdn[n - 3]*dLambda2dnSq
                                    + dFramePreTermsdnn[n - 1]*lambda0Sq + dFramePreTermsdnn[n - 2]*lambda1Sq + dFramePreTermsdnn[n - 3]*lambda2Sq
                                    + dFramePreTermsdn[n - 2]*dLambda1dnSq + dFramePreTermsdn[n - 3]*dLambda2dnSq));
      dFramePreTermsdnm[n]=((1. / static_cast<real>(n)) * (
                                    dFramePreTermsdm[n - 2]*dLambda1dnSq + dFramePreTermsdm[n - 3]*dLambda2dnSq
                                    + dFramePreTermsdnm[n - 1]*lambda0Sq + dFramePreTermsdnm[n - 2]*lambda1Sq + dFramePreTermsdnm[n - 3]*lambda2Sq
                                    + dFramePreTermsdn[n - 1]*dLambda0dmSq + dFramePreTermsdn[n - 2]*dLambda1dmSq));

      dFramePreTermsdma[n]=((1. / static_cast<real>(n)) * (
                                    FramePreTerms[n - 2]*dLambda1dmaSq
                                    + dFramePreTermsda[n - 1]*dLambda0dmSq + dFramePreTermsda[n - 2]*dLambda1dmSq
                                    + dFramePreTermsdma[n - 1]*lambda0Sq + dFramePreTermsdma[n - 2]*lambda1Sq + dFramePreTermsdma[n - 3]*lambda2Sq
                                    + dFramePreTermsdm[n - 2]*dLambda1daSq + dFramePreTermsdm[n - 3]*dLambda2daSq));
      dFramePreTermsdmb[n]=((1. / static_cast<real>(n)) * (
                                    FramePreTerms[n - 1]*dLambda0dmbSq
                                    + dFramePreTermsdb[n - 1]*dLambda0dmSq + dFramePreTermsdb[n - 2]*dLambda1dmSq
                                    + dFramePreTermsdmb[n - 1]*lambda0Sq + dFramePreTermsdmb[n - 2]*lambda1Sq + dFramePreTermsdmb[n - 3]*lambda2Sq
                                    + dFramePreTermsdm[n - 1]*dLambda0dbSq + dFramePreTermsdm[n - 2]*dLambda1dbSq));
      dFramePreTermsdmn[n]=((1. / static_cast<real>(n)) * (
                                    dFramePreTermsdn[n - 1]*dLambda0dmSq + dFramePreTermsdn[n - 2]*dLambda1dmSq
                                    + dFramePreTermsdmn[n - 1]*lambda0Sq + dFramePreTermsdmn[n - 2]*lambda1Sq + dFramePreTermsdmn[n - 3]*lambda2Sq
                                    + dFramePreTermsdm[n - 2]*dLambda1dnSq + dFramePreTermsdm[n - 3]*dLambda2dnSq));
      dFramePreTermsdmm[n]=((1. / static_cast<real>(n)) * (
                                    dFramePreTermsdm[n - 1]*dLambda0dmSq + dFramePreTermsdm[n - 2]*dLambda1dmSq
                                    + dFramePreTermsdmm[n - 1]*lambda0Sq + dFramePreTermsdmm[n - 2]*lambda1Sq + dFramePreTermsdmm[n - 3]*lambda2Sq
                                    + dFramePreTermsdm[n - 1]*dLambda0dmSq + dFramePreTermsdm[n - 2]*dLambda1dmSq));
    }
  }

  //Derivation of the position
  { 
    dPosPreTermsdaa.resize(order);
    dPosPreTermsdba.resize(order);
    dPosPreTermsdna.resize(order);
    dPosPreTermsdma.resize(order);
    dPosPreTermsdab.resize(order);
    dPosPreTermsdbb.resize(order);
    dPosPreTermsdnb.resize(order);
    dPosPreTermsdmb.resize(order);
    dPosPreTermsdan.resize(order);
    dPosPreTermsdbn.resize(order);
    dPosPreTermsdnn.resize(order);
    dPosPreTermsdmn.resize(order);
    dPosPreTermsdam.resize(order);
    dPosPreTermsdbm.resize(order);
    dPosPreTermsdnm.resize(order);
    dPosPreTermsdmm.resize(order);
    Eigen::Vector3r zero;
    zero.setZero();
    Eigen::Vector3r vnan(NAN, NAN, NAN);

    //order 0
    dPosPreTermsdaa[0]=vnan;
    dPosPreTermsdba[0]=vnan;
    dPosPreTermsdna[0]=vnan;
    dPosPreTermsdma[0]=vnan;
    dPosPreTermsdab[0]=vnan;
    dPosPreTermsdbb[0]=vnan;
    dPosPreTermsdnb[0]=vnan;
    dPosPreTermsdmb[0]=vnan;
    dPosPreTermsdan[0]=vnan;
    dPosPreTermsdbn[0]=vnan;
    dPosPreTermsdnn[0]=vnan;
    dPosPreTermsdmn[0]=vnan;
    dPosPreTermsdam[0]=vnan;
    dPosPreTermsdbm[0]=vnan;
    dPosPreTermsdnm[0]=vnan;
    dPosPreTermsdmm[0]=vnan;
    //order 1
    dPosPreTermsdaa[1]=zero;
    dPosPreTermsdba[1]=zero;
    dPosPreTermsdna[1]=zero;
    dPosPreTermsdma[1]=zero;
    dPosPreTermsdab[1]=zero;
    dPosPreTermsdbb[1]=zero;
    dPosPreTermsdnb[1]=zero;
    dPosPreTermsdmb[1]=zero;
    dPosPreTermsdan[1]=zero;
    dPosPreTermsdbn[1]=zero;
    dPosPreTermsdnn[1]=zero;
    dPosPreTermsdmn[1]=zero;
    dPosPreTermsdam[1]=zero;
    dPosPreTermsdbm[1]=zero;
    dPosPreTermsdnm[1]=zero;
    dPosPreTermsdmm[1]=zero;
    //order n
    for (int n = 2; n < order; n++) {
      dPosPreTermsdaa[n]=(1. / static_cast<real>(n))*dFramePreTermsdaa[n - 1].col(2);
      dPosPreTermsdba[n]=(1. / static_cast<real>(n))*dFramePreTermsdba[n - 1].col(2);
      dPosPreTermsdna[n]=(1. / static_cast<real>(n))*dFramePreTermsdna[n - 1].col(2);
      dPosPreTermsdma[n]=(1. / static_cast<real>(n))*dFramePreTermsdma[n - 1].col(2);
      dPosPreTermsdab[n]=(1. / static_cast<real>(n))*dFramePreTermsdab[n - 1].col(2);
      dPosPreTermsdbb[n]=(1. / static_cast<real>(n))*dFramePreTermsdbb[n - 1].col(2);
      dPosPreTermsdnb[n]=(1. / static_cast<real>(n))*dFramePreTermsdnb[n - 1].col(2);
      dPosPreTermsdmb[n]=(1. / static_cast<real>(n))*dFramePreTermsdmb[n - 1].col(2);
      dPosPreTermsdan[n]=(1. / static_cast<real>(n))*dFramePreTermsdan[n - 1].col(2);
      dPosPreTermsdbn[n]=(1. / static_cast<real>(n))*dFramePreTermsdbn[n - 1].col(2);
      dPosPreTermsdnn[n]=(1. / static_cast<real>(n))*dFramePreTermsdnn[n - 1].col(2);
      dPosPreTermsdmn[n]=(1. / static_cast<real>(n))*dFramePreTermsdmn[n - 1].col(2);
      dPosPreTermsdam[n]=(1. / static_cast<real>(n))*dFramePreTermsdam[n - 1].col(2);
      dPosPreTermsdbm[n]=(1. / static_cast<real>(n))*dFramePreTermsdbm[n - 1].col(2);
      dPosPreTermsdnm[n]=(1. / static_cast<real>(n))*dFramePreTermsdnm[n - 1].col(2);
      dPosPreTermsdmm[n]=(1. / static_cast<real>(n))*dFramePreTermsdmm[n - 1].col(2);
    }
  }

  called["computeDerivationsPreTermsSecondOrder"] = true;
}

Eigen::Matrix3r SubSegmentComputations::dFrameda(real s)
{
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  int order = dFramePreTermsda.size();
  computeSPowers(s);
  Eigen::Matrix3r res;
  res.setZero();
  //for (int i = 1; i < order; i++)
  for (int i = order-1; i > 0; i--)
    res += spowers[i] * dFramePreTermsda[i];
  res = frameOr * res;

  return res;
}

Eigen::Matrix3r SubSegmentComputations::dFramedb(real s)
{
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  int order = dFramePreTermsdb.size();
  computeSPowers(s);
  Eigen::Matrix3r res;
  res.setZero();
  //for (int i = 1; i < order; i++)
  for (int i = order-1; i > 0; i--)
    res += spowers[i] * dFramePreTermsdb[i];
  res = frameOr * res;

  return res;
}

Eigen::Matrix3r SubSegmentComputations::dFramedn(real s)
{
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  int order = dFramePreTermsdn.size();
  computeSPowers(s);
  Eigen::Matrix3r res;
  res.setZero();
  //for (int i = 1; i < order; i++)
  for (int i = order-1; i > 0; i--)
    res += spowers[i] * dFramePreTermsdn[i];
  res = frameOr * res;

  return res;
}

Eigen::Matrix3r SubSegmentComputations::dFramedm(real s)
{
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  int order = dFramePreTermsdm.size();
  computeSPowers(s);
  Eigen::Matrix3r res;
  res.setZero();
  //for (int i = 1; i < order; i++)
  for (int i = order-1; i > 0; i--)
    res += spowers[i] * dFramePreTermsdm[i];
  res = frameOr * res;

  return res;
}

Eigen::Vector3r SubSegmentComputations::dPosda(real s)
{
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  int order = dPosPreTermsda.size();
  computeSPowers(s);
  Eigen::Vector3r res(0., 0., 0.);
  //for (int i = 1; i < order; i++)
  for (int i = order-1; i > 0; i--)
    res += spowers[i] * dPosPreTermsda[i];
  res = frameOr * res;

  return res;
}

Eigen::Vector3r SubSegmentComputations::dPosdb(real s)
{
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  int order = dPosPreTermsdb.size();
  computeSPowers(s);
  Eigen::Vector3r res(0., 0., 0.);
  //for (int i = 1; i < order; i++)
  for (int i = order-1; i > 0; i--)
    res += spowers[i] * dPosPreTermsdb[i];
  res = frameOr * res;

  return res;
}

Eigen::Vector3r SubSegmentComputations::dPosdn(real s)
{
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  int order = dPosPreTermsdn.size();
  computeSPowers(s);
  Eigen::Vector3r res(0., 0., 0.);
  //for (int i = 1; i < order; i++)
  for (int i = order-1; i > 0; i--)
    res += spowers[i] * dPosPreTermsdn[i];
  res = frameOr * res;

  return res;
}

Eigen::Vector3r SubSegmentComputations::dPosdm(real s)
{
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  int order = dPosPreTermsdm.size();
  computeSPowers(s);
  Eigen::Vector3r res(0., 0., 0.);
  //for (int i = 1; i < order; i++)
  for (int i = order-1; i > 0; i--)
    res += spowers[i] * dPosPreTermsdm[i];
  res = frameOr * res;

  return res;
}

Eigen::Matrix<Eigen::Matrix4r, 3, 3> SubSegmentComputations::hessianFrame(real s)
{
  if (!called["computeDerivationsPreTermsSecondOrder"])computeDerivationsPreTermsSecondOrder();
  int order = dFramePreTermsdaa.size();
  computeSPowers(s);
  Eigen::Matrix3r resaa;
  Eigen::Matrix3r resba;
  Eigen::Matrix3r resna;
  Eigen::Matrix3r resma;
  Eigen::Matrix3r resab;
  Eigen::Matrix3r resbb;
  Eigen::Matrix3r resnb;
  Eigen::Matrix3r resmb;
  Eigen::Matrix3r resan;
  Eigen::Matrix3r resbn;
  Eigen::Matrix3r resnn;
  Eigen::Matrix3r resmn;
  Eigen::Matrix3r resam;
  Eigen::Matrix3r resbm;
  Eigen::Matrix3r resnm;
  Eigen::Matrix3r resmm;
  resaa.setZero();
  resba.setZero();
  resna.setZero();
  resma.setZero();
  resab.setZero();
  resbb.setZero();
  resnb.setZero();
  resmb.setZero();
  resan.setZero();
  resbn.setZero();
  resnn.setZero();
  resmn.setZero();
  resam.setZero();
  resbm.setZero();
  resnm.setZero();
  resmm.setZero();

  //for (int i = 1; i < order; i++) {
  for (int i = order-1; i > 0; i--) {
    resaa += spowers[i] * dFramePreTermsdaa[i];
    resba += spowers[i] * dFramePreTermsdba[i];
    resna += spowers[i] * dFramePreTermsdna[i];
    resma += spowers[i] * dFramePreTermsdma[i];
    resab += spowers[i] * dFramePreTermsdab[i];
    resbb += spowers[i] * dFramePreTermsdbb[i];
    resnb += spowers[i] * dFramePreTermsdnb[i];
    resmb += spowers[i] * dFramePreTermsdmb[i];
    resan += spowers[i] * dFramePreTermsdan[i];
    resbn += spowers[i] * dFramePreTermsdbn[i];
    resnn += spowers[i] * dFramePreTermsdnn[i];
    resmn += spowers[i] * dFramePreTermsdmn[i];
    resam += spowers[i] * dFramePreTermsdam[i];
    resbm += spowers[i] * dFramePreTermsdbm[i];
    resnm += spowers[i] * dFramePreTermsdnm[i];
    resmm += spowers[i] * dFramePreTermsdmm[i];
  }
  resaa = frameOr * resaa;
  resba = frameOr * resba;
  resna = frameOr * resna;
  resma = frameOr * resma;
  resab = frameOr * resab;
  resbb = frameOr * resbb;
  resnb = frameOr * resnb;
  resmb = frameOr * resmb;
  resan = frameOr * resan;
  resbn = frameOr * resbn;
  resnn = frameOr * resnn;
  resmn = frameOr * resmn;
  resam = frameOr * resam;
  resbm = frameOr * resbm;
  resnm = frameOr * resnm;
  resmm = frameOr * resmm;
  Eigen::Matrix<Eigen::Matrix4r, 3, 3> res;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      res(i, j)(0, 0) = resaa(i, j);
      res(i, j)(0, 1) = resab(i, j);
      res(i, j)(0, 2) = resan(i, j);
      res(i, j)(0, 3) = resam(i, j);
      res(i, j)(1, 0) = resba(i, j);
      res(i, j)(1, 1) = resbb(i, j);
      res(i, j)(1, 2) = resbn(i, j);
      res(i, j)(1, 3) = resbm(i, j);
      res(i, j)(2, 0) = resna(i, j);
      res(i, j)(2, 1) = resnb(i, j);
      res(i, j)(2, 2) = resnn(i, j);
      res(i, j)(2, 3) = resnm(i, j);
      res(i, j)(3, 0) = resma(i, j);
      res(i, j)(3, 1) = resmb(i, j);
      res(i, j)(3, 2) = resmn(i, j);
      res(i, j)(3, 3) = resmm(i, j);
    }
  }
  return res;
}

Eigen::Matrix<Eigen::Matrix4r, 3, 1> SubSegmentComputations::hessianPos(real s)
{
  if (!called["computeDerivationsPreTermsSecondOrder"])computeDerivationsPreTermsSecondOrder();
  int order = dPosPreTermsdaa.size();
  computeSPowers(s);
  Eigen::Vector3r resaa;
  Eigen::Vector3r resba;
  Eigen::Vector3r resna;
  Eigen::Vector3r resma;
  Eigen::Vector3r resab;
  Eigen::Vector3r resbb;
  Eigen::Vector3r resnb;
  Eigen::Vector3r resmb;
  Eigen::Vector3r resan;
  Eigen::Vector3r resbn;
  Eigen::Vector3r resnn;
  Eigen::Vector3r resmn;
  Eigen::Vector3r resam;
  Eigen::Vector3r resbm;
  Eigen::Vector3r resnm;
  Eigen::Vector3r resmm;
  resaa.setZero();
  resba.setZero();
  resna.setZero();
  resma.setZero();
  resab.setZero();
  resbb.setZero();
  resnb.setZero();
  resmb.setZero();
  resan.setZero();
  resbn.setZero();
  resnn.setZero();
  resmn.setZero();
  resam.setZero();
  resbm.setZero();
  resnm.setZero();
  resmm.setZero();

  //for (int i = 1; i < order; i++) {
  for (int i = order-1; i > 0; i--) {
    resaa += spowers[i] * dPosPreTermsdaa[i];
    resba += spowers[i] * dPosPreTermsdba[i];
    resna += spowers[i] * dPosPreTermsdna[i];
    resma += spowers[i] * dPosPreTermsdma[i];
    resab += spowers[i] * dPosPreTermsdab[i];
    resbb += spowers[i] * dPosPreTermsdbb[i];
    resnb += spowers[i] * dPosPreTermsdnb[i];
    resmb += spowers[i] * dPosPreTermsdmb[i];
    resan += spowers[i] * dPosPreTermsdan[i];
    resbn += spowers[i] * dPosPreTermsdbn[i];
    resnn += spowers[i] * dPosPreTermsdnn[i];
    resmn += spowers[i] * dPosPreTermsdmn[i];
    resam += spowers[i] * dPosPreTermsdam[i];
    resbm += spowers[i] * dPosPreTermsdbm[i];
    resnm += spowers[i] * dPosPreTermsdnm[i];
    resmm += spowers[i] * dPosPreTermsdmm[i];
  }
  resaa = frameOr * resaa;
  resba = frameOr * resba;
  resna = frameOr * resna;
  resma = frameOr * resma;
  resab = frameOr * resab;
  resbb = frameOr * resbb;
  resnb = frameOr * resnb;
  resmb = frameOr * resmb;
  resan = frameOr * resan;
  resbn = frameOr * resbn;
  resnn = frameOr * resnn;
  resmn = frameOr * resmn;
  resam = frameOr * resam;
  resbm = frameOr * resbm;
  resnm = frameOr * resnm;
  resmm = frameOr * resmm;
  Eigen::Matrix<Eigen::Matrix4r, 3, 1> res;
  for (int i = 0; i < 3; i++) {
    res(i)(0, 0) = resaa(i);
    res(i)(0, 1) = resab(i);
    res(i)(0, 2) = resan(i);
    res(i)(0, 3) = resam(i);
    res(i)(1, 0) = resba(i);
    res(i)(1, 1) = resbb(i);
    res(i)(1, 2) = resbn(i);
    res(i)(1, 3) = resbm(i);
    res(i)(2, 0) = resna(i);
    res(i)(2, 1) = resnb(i);
    res(i)(2, 2) = resnn(i);
    res(i)(2, 3) = resnm(i);
    res(i)(3, 0) = resma(i);
    res(i)(3, 1) = resmb(i);
    res(i)(3, 2) = resmn(i);
    res(i)(3, 3) = resmm(i);
  }
  return res;
}

void SubSegmentComputations::computePreAltitudeEnergyPreTerms()
{
  if (called["computePreAltitudeEnergyPreTerms"])return;
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  const real w = ribbonWidth;
  int order = FramePreTerms.size();
  preAltitudeEnergyPreTerms.resize(order);
  preAltitudeEnergyLinearTerm = w * posOr;
  Eigen::Vector3r vnan(NAN, NAN, NAN);
  preAltitudeEnergyPreTerms[0]=vnan;
  preAltitudeEnergyPreTerms[1]=vnan;
  for (int k = 2; k < order; k++)
    preAltitudeEnergyPreTerms[k]=(1. / static_cast<real>(k)) * (w * PosPreTerms[k - 1]);
  called["computePreAltitudeEnergyPreTerms"] = true;
}

void SubSegmentComputations::computePreAltitudeEnergyPreTermsDerivations()
{
  if (called["computePreAltitudeEnergyPreTermsDerivations"])return;
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  const real w = ribbonWidth;
  int order = FramePreTerms.size();
  assert(order > 0);

  dPreAltitudeEnergyPreTermda.resize(order);
  dPreAltitudeEnergyPreTermdb.resize(order);
  dPreAltitudeEnergyPreTermdn.resize(order);
  dPreAltitudeEnergyPreTermdm.resize(order);
  for(int idx=0;idx<2;idx++)
  {
    dPreAltitudeEnergyPreTermda[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdb[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdn[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdm[idx]=Eigen::Vector3r(0., 0., 0.);
  }
  for (int k = 2; k < order; k++) {
    dPreAltitudeEnergyPreTermda[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsda[k - 1]);
    dPreAltitudeEnergyPreTermdb[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdb[k - 1]);
    dPreAltitudeEnergyPreTermdn[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdn[k - 1]);
    dPreAltitudeEnergyPreTermdm[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdm[k - 1]);
  }
  called["computePreAltitudeEnergyPreTermsDerivations"] = true;
}



void SubSegmentComputations::computePreAltitudeEnergyPreTermsDerivationsSecondOrder()
{
  if (called["computePreAltitudeEnergyPreTermsDerivationsSecondOrder"])return;
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  if (!called["computeDerivationsPreTermsSecondOrder"])computeDerivationsPreTermsSecondOrder();
  if (!called["computePreAltitudeEnergyPreTermsDerivations"])computePreAltitudeEnergyPreTermsDerivations();
  const real w = ribbonWidth;
  int order = FramePreTerms.size();
  assert(order > 0);

  dPreAltitudeEnergyPreTermdaa.resize(order);
  dPreAltitudeEnergyPreTermdba.resize(order);
  dPreAltitudeEnergyPreTermdna.resize(order);
  dPreAltitudeEnergyPreTermdma.resize(order);
  dPreAltitudeEnergyPreTermdab.resize(order);
  dPreAltitudeEnergyPreTermdbb.resize(order);
  dPreAltitudeEnergyPreTermdnb.resize(order);
  dPreAltitudeEnergyPreTermdmb.resize(order);
  dPreAltitudeEnergyPreTermdan.resize(order);
  dPreAltitudeEnergyPreTermdbn.resize(order);
  dPreAltitudeEnergyPreTermdnn.resize(order);
  dPreAltitudeEnergyPreTermdmn.resize(order);
  dPreAltitudeEnergyPreTermdam.resize(order);
  dPreAltitudeEnergyPreTermdbm.resize(order);
  dPreAltitudeEnergyPreTermdnm.resize(order);
  dPreAltitudeEnergyPreTermdmm.resize(order);
  for(int idx=0;idx<2;idx++)
  {
    dPreAltitudeEnergyPreTermdaa[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdba[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdna[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdma[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdab[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdbb[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdnb[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdmb[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdan[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdbn[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdnn[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdmn[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdam[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdbm[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdnm[idx]=Eigen::Vector3r(0., 0., 0.);
    dPreAltitudeEnergyPreTermdmm[idx]=Eigen::Vector3r(0., 0., 0.);
  }

  for (int k = 2; k < order; k++) {
    dPreAltitudeEnergyPreTermdaa[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdaa[k - 1]);
    dPreAltitudeEnergyPreTermdba[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdba[k - 1]);
    dPreAltitudeEnergyPreTermdna[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdna[k - 1]);
    dPreAltitudeEnergyPreTermdma[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdma[k - 1]);
    dPreAltitudeEnergyPreTermdab[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdab[k - 1]);
    dPreAltitudeEnergyPreTermdbb[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdbb[k - 1]);
    dPreAltitudeEnergyPreTermdnb[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdnb[k - 1]);
    dPreAltitudeEnergyPreTermdmb[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdmb[k - 1]);
    dPreAltitudeEnergyPreTermdan[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdan[k - 1]);
    dPreAltitudeEnergyPreTermdbn[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdbn[k - 1]);
    dPreAltitudeEnergyPreTermdnn[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdnn[k - 1]);
    dPreAltitudeEnergyPreTermdmn[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdmn[k - 1]);
    dPreAltitudeEnergyPreTermdam[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdam[k - 1]);
    dPreAltitudeEnergyPreTermdbm[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdbm[k - 1]);
    dPreAltitudeEnergyPreTermdnm[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdnm[k - 1]);
    dPreAltitudeEnergyPreTermdmm[k]=(1. / static_cast<real>(k)) * (w * dPosPreTermsdmm[k - 1]);
  }
  called["computePreAltitudeEnergyPreTermsDerivationsSecondOrder"] = true;
}


Eigen::Vector3r SubSegmentComputations::preAltitudeEnergy(real s)
{
  if (!called["computePreAltitudeEnergyPreTerms"])computePreAltitudeEnergyPreTerms();
  int order = preAltitudeEnergyPreTerms.size();
  computeSPowers(s);
  Eigen::Vector3r res(0., 0., 0.);
  //for (int i = 2; i < order; i++)
  for (int i = order-1; i > 1; i--)
    res += spowers[i] * preAltitudeEnergyPreTerms[i];
  res = frameOr * res;
  res += preAltitudeEnergyLinearTerm * spowers[1];
  return res;
}

Eigen::Vector3r SubSegmentComputations::preAltitudeRotationalEnergy(real s)
{
    if (!called["computePreAltitudeEnergyPreTerms"])computePreAltitudeEnergyPreTerms();
  int order = preAltitudeEnergyPreTerms.size();
  computeSPowers(s);
  Eigen::Vector3r res(0., 0., 0.);
  //for (int i = 2; i < order; i++)
  for (int i = order-1; i > 1; i--)
    res += spowers[i] * preAltitudeEnergyPreTerms[i];
  return res;
}


Eigen::Matrix<real, 3, 4> SubSegmentComputations::gradPreAltitudeEnergy(real s)
{
  if (!called["computePreAltitudeEnergyPreTermsDerivations"])computePreAltitudeEnergyPreTermsDerivations();
  Eigen::Matrix<real, 3, 4> res;
  res.setZero();
  int order = dPreAltitudeEnergyPreTermda.size();
  computeSPowers(s);
  //for (int i = 2; i < order; i++) {
  for (int i = order-1; i > 1; i--) {
    res.col(0) += spowers[i] * dPreAltitudeEnergyPreTermda[i];
    res.col(1) += spowers[i] * dPreAltitudeEnergyPreTermdb[i];
    res.col(2) += spowers[i] * dPreAltitudeEnergyPreTermdn[i];
    res.col(3) += spowers[i] * dPreAltitudeEnergyPreTermdm[i];
  }
  return frameOr * res;
}

Eigen::Matrix<Eigen::Vector3r, 3, 1> SubSegmentComputations::dPreAltitudeEnergydPos(real s)
{
  Eigen::Matrix<Eigen::Vector3r, 3, 1> res;
  res << Eigen::Vector3r(s * ribbonWidth, 0., 0.), Eigen::Vector3r(0., s * ribbonWidth, 0.), Eigen::Vector3r(0., 0., s * ribbonWidth);
  return res;
}

Eigen::Matrix<Eigen::Matrix3r, 3, 1> SubSegmentComputations::dPreAltitudeEnergydFrame(real s)
{
  if (!called["computePreAltitudeEnergyPreTerms"])computePreAltitudeEnergyPreTerms();
  int order = preAltitudeEnergyPreTerms.size();
  computeSPowers(s);
  Eigen::Vector3r K(0., 0., 0.);
  //for (int i = 2; i < order; i++)
  for (int i = order-1; i > 1; i--)
    K += spowers[i] * preAltitudeEnergyPreTerms[i];
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> res;
  res.setConstant(Eigen::Matrix3r::Zero());
  Eigen::Matrix3r temp;
  Eigen::Vector3r vtemp;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      temp.setZero();
      temp(i, j) = 1.;
      vtemp = temp * K;
      for (int a = 0; a < 3; a++)
        res[a](i, j) += vtemp[a];
    }
  }
  return res;
}

Eigen::Matrix<Eigen::Matrix<Eigen::Vector4r, 3, 3>, 3, 1> SubSegmentComputations::d2PreAltitudeEnergydFramedabnm(real s)
{
  if (!called["computePreAltitudeEnergyPreTermsDerivations"])computePreAltitudeEnergyPreTermsDerivations();
  Eigen::Vector3r Ka, Kb, Kn, Km;
  Ka.setZero();
  Kb.setZero();
  Kn.setZero();
  Km.setZero();
  int order = dPreAltitudeEnergyPreTermda.size();
  computeSPowers(s);
  //for (int i = 1; i < order; i++) {
  for (int i = order-1; i > 0; i--) {
    Ka += spowers[i] * dPreAltitudeEnergyPreTermda[i];
    Kb += spowers[i] * dPreAltitudeEnergyPreTermdb[i];
    Kn += spowers[i] * dPreAltitudeEnergyPreTermdn[i];
    Km += spowers[i] * dPreAltitudeEnergyPreTermdm[i];
  }
  Eigen::Matrix<Eigen::Matrix<Eigen::Vector4r, 3, 3>, 3, 1> res;
  Eigen::Matrix3r temp;
  Eigen::Vector3r va, vb, vn, vm;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      temp.setZero();
      temp(i, j) = 1.;
      va = temp * Ka;
      vb = temp * Kb;
      vn = temp * Kn;
      vm = temp * Km;
      for (int a = 0; a < 3; a++) {
        res[a](i, j)[0] = va[a];
        res[a](i, j)[1] = vb[a];
        res[a](i, j)[2] = vn[a];
        res[a](i, j)[3] = vm[a];
      }
    }
  }
  return res;
}


Eigen::Matrix<Eigen::Matrix4r, 3, 1> SubSegmentComputations::hessianPreAltitudeEnergy(real s)
{
  if (!called["computePreAltitudeEnergyPreTermsDerivationsSecondOrder"])computePreAltitudeEnergyPreTermsDerivationsSecondOrder();
  Eigen::Matrix<Eigen::Matrix4r, 3, 1> preRes;
  preRes.setConstant(Eigen::Matrix4r::Zero());
  int order = dPreAltitudeEnergyPreTermdaa.size();
  computeSPowers(s);
  //for (int i = 1; i < order; i++) {
  for (int i = order-1; i > 0; i--) {
    for (int j = 0; j < 3; j++) {
      preRes[j](0, 0) += spowers[i] * dPreAltitudeEnergyPreTermdaa[i][j];
      preRes[j](1, 0) += spowers[i] * dPreAltitudeEnergyPreTermdba[i][j];
      preRes[j](2, 0) += spowers[i] * dPreAltitudeEnergyPreTermdna[i][j];
      preRes[j](3, 0) += spowers[i] * dPreAltitudeEnergyPreTermdma[i][j];
      preRes[j](0, 1) += spowers[i] * dPreAltitudeEnergyPreTermdab[i][j];
      preRes[j](1, 1) += spowers[i] * dPreAltitudeEnergyPreTermdbb[i][j];
      preRes[j](2, 1) += spowers[i] * dPreAltitudeEnergyPreTermdnb[i][j];
      preRes[j](3, 1) += spowers[i] * dPreAltitudeEnergyPreTermdmb[i][j];
      preRes[j](0, 2) += spowers[i] * dPreAltitudeEnergyPreTermdan[i][j];
      preRes[j](1, 2) += spowers[i] * dPreAltitudeEnergyPreTermdbn[i][j];
      preRes[j](2, 2) += spowers[i] * dPreAltitudeEnergyPreTermdnn[i][j];
      preRes[j](3, 2) += spowers[i] * dPreAltitudeEnergyPreTermdmn[i][j];
      preRes[j](0, 3) += spowers[i] * dPreAltitudeEnergyPreTermdam[i][j];
      preRes[j](1, 3) += spowers[i] * dPreAltitudeEnergyPreTermdbm[i][j];
      preRes[j](2, 3) += spowers[i] * dPreAltitudeEnergyPreTermdnm[i][j];
      preRes[j](3, 3) += spowers[i] * dPreAltitudeEnergyPreTermdmm[i][j];
    }
  }
  Eigen::Matrix<Eigen::Matrix4r, 3, 1> res;
  for (int i = 0; i < 3; i++)res[i] = frameOr(i, 0) * preRes[0] + frameOr(i, 1) * preRes[1] + frameOr(i, 2) * preRes[2];
  return res;
}

Eigen::Matrix<Eigen::Vector3r, 3, 1> SubSegmentComputations::dPosdPos(real s)
{
  Eigen::Matrix<Eigen::Vector3r, 3, 1>res;
  res << Eigen::Vector3r(1., 0., 0.), Eigen::Vector3r(0., 1., 0.), Eigen::Vector3r(0., 0., 1.);
  return res;
}

Eigen::Matrix<Eigen::Matrix3r, 3, 3> SubSegmentComputations::dFramedFrame(real s)
{
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  int order = FramePreTerms.size();
  computeSPowers(s);
  Eigen::Matrix3r K = FramePreTerms[0];
  //for (int i = 1; i < order; i++)
  for (int i = order-1; i > 0; i--)
    K += spowers[i] * FramePreTerms[i];
  Eigen::Matrix<Eigen::Matrix3r, 3, 3> res;
  res.setConstant(Eigen::Matrix3r::Zero());
  Eigen::Matrix3r temp;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      temp.setZero();
      temp(i, j) = 1.;
      temp = temp * K;
      for (int a = 0; a < 3; a++) {
        for (int b = 0; b < 3; b++) {
          res(a, b)(i, j) = temp(a, b);
        }
      }
    }
  }
  return res;
}

Eigen::Matrix<Eigen::Matrix3r, 3, 1> SubSegmentComputations::dPosdFrame(real s)
{
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  int order = PosPreTerms.size();
  computeSPowers(s);
  Eigen::Vector3r K(0., 0., 0.);
  //for (int i = 1; i < order; i++)
  for (int i = order-1; i > 0; i--)
    K += spowers[i] * PosPreTerms[i];
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> res;
  Eigen::Matrix3r temp;
  Eigen::Vector3r vtemp;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      temp.setZero();
      temp(i, j) = 1.;
      vtemp = temp * K;
      for (int b = 0; b < 3; b++) {
        res[b](i, j) = vtemp(b);
      }
    }
  }
  return res;
}

Eigen::Matrix<Eigen::Matrix<Eigen::Vector4r, 3, 3>, 3, 3> SubSegmentComputations::d2FramedFramedabnm(real s)
{
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  int order = PosPreTerms.size();
  computeSPowers(s);
  Eigen::Matrix3r Ka = Eigen::Matrix3r::Zero();
  Eigen::Matrix3r Kb = Eigen::Matrix3r::Zero();
  Eigen::Matrix3r Kn = Eigen::Matrix3r::Zero();
  Eigen::Matrix3r Km = Eigen::Matrix3r::Zero();
  //for (int i = 1; i < order; i++) {
  for (int i = order-1; i > 0; i--) {
    Ka += spowers[i] * dFramePreTermsda[i];
    Kb += spowers[i] * dFramePreTermsdb[i];
    Kn += spowers[i] * dFramePreTermsdn[i];
    Km += spowers[i] * dFramePreTermsdm[i];
  }

  Eigen::Matrix<Eigen::Matrix<Eigen::Vector4r, 3, 3>, 3, 3> res;
  Eigen::Matrix<Eigen::Vector4r, 3, 3> tmp1;
  tmp1.setConstant(Eigen::Vector4r(0., 0., 0., 0.));
  res.setConstant(tmp1);
  Eigen::Matrix3r tmpa;
  Eigen::Matrix3r tmpb;
  Eigen::Matrix3r tmpn;
  Eigen::Matrix3r tmpm;
  Eigen::Matrix3r temp;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      temp.setZero();
      temp(i, j) = 1.;
      tmpa = temp * Ka;
      tmpb = temp * Kb;
      tmpn = temp * Kn;
      tmpm = temp * Km;
      for (int a = 0; a < 3; a++) {
        for (int b = 0; b < 3; b++) {
          res(a, b)(i, j)[0] = tmpa(a, b);
          res(a, b)(i, j)[1] = tmpb(a, b);
          res(a, b)(i, j)[2] = tmpn(a, b);
          res(a, b)(i, j)[3] = tmpm(a, b);
        }
      }
    }
  }
  return res;
}

Eigen::Matrix<Eigen::Matrix<Eigen::Vector4r, 3, 3>, 3, 1> SubSegmentComputations::d2PosdFramedabnm(real s)
{
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  int order = PosPreTerms.size();
  computeSPowers(s);
  Eigen::Vector3r Ka = Eigen::Vector3r::Zero();
  Eigen::Vector3r Kb = Eigen::Vector3r::Zero();
  Eigen::Vector3r Kn = Eigen::Vector3r::Zero();
  Eigen::Vector3r Km = Eigen::Vector3r::Zero();
  //for (int i = 1; i < order; i++) {
  for (int i = order-1; i > 0; i--) {
    Ka += spowers[i] * dPosPreTermsda[i];
    Kb += spowers[i] * dPosPreTermsdb[i];
    Kn += spowers[i] * dPosPreTermsdn[i];
    Km += spowers[i] * dPosPreTermsdm[i];
  }

  Eigen::Matrix<Eigen::Matrix<Eigen::Vector4r, 3, 3>, 3, 1> res;
  Eigen::Matrix<Eigen::Vector4r, 3, 3> tmp1;
  tmp1.setConstant(Eigen::Vector4r(0., 0., 0., 0.));
  res.setConstant(tmp1);
  Eigen::Vector3r tmpa;
  Eigen::Vector3r tmpb;
  Eigen::Vector3r tmpn;
  Eigen::Vector3r tmpm;
  Eigen::Matrix3r temp;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      temp.setZero();
      temp(i, j) = 1.;
      tmpa = temp * Ka;
      tmpb = temp * Kb;
      tmpn = temp * Kn;
      tmpm = temp * Km;
      for (int b = 0; b < 3; b++) {
        res(b)(i, j)[0] = tmpa(b);
        res(b)(i, j)[1] = tmpb(b);
        res(b)(i, j)[2] = tmpn(b);
        res(b)(i, j)[3] = tmpm(b);
      }
    }
  }
  return res;
}

void SubSegmentComputations::computeSPowers(real s)
{
  int order = FramePreTerms.size();
  order+=10;//Maybe too big, but whatever it's nothing compare to the rest
  assert(order > 0);
  if ((PosPreTerms.size() == spowers.size()) && (sPowersCurrentS == s))
    return;
  spowers.resize(order);
  spowers[0] = 1;
  spowers[1] = s;
  for (int i = 2; i < order; i++) {
    if (i % 2)
      spowers[i] = spowers[i / 2] * spowers[i / 2 + 1];
    else
      spowers[i] = spowers[i / 2] * spowers[i / 2];
  }
}


template<int i,int j,int k> auto operatorDotProdSerie(std::vector<Eigen::Matrix<real,j,i>> U, std::vector<Eigen::Matrix<real,j,k>> V) ->
std::vector<std::remove_const_t<decltype((U.at(0).transpose()*V.at(0)).eval())>>
{
  using C=std::remove_const_t<decltype((U.at(0).transpose()*V.at(0)).eval())>;
  std::vector<C> res;
  size_t tu=U.size();
  size_t tv=V.size();
  size_t order = std::min(tu, tv);
  res.push_back(U.at(0).transpose()*V.at(0));
  for(size_t ind=1; ind<order; ind++)
  {
    C tsum=U.at(0).transpose()*V.at(ind);
    for(size_t jnd=1; jnd<=ind; jnd++)
      tsum += U.at(jnd).transpose()*V.at(ind-jnd);
    res.push_back(tsum);
  }
  return res;
}

template<int i,int j,int k> auto  operatorSerieLMul(Eigen::Matrix<real,i,j> A, std::vector<Eigen::Matrix<real,j,k>> U)->
  std::vector<std::remove_const_t<decltype((A*U.at(0)).eval())>>
{
  using C=std::remove_const_t<decltype((A*U.at(0)).eval())>;
  std::vector<C> res;
  size_t t=U.size();
  for(size_t ind=0; ind<t; ind++)
  {
    res.push_back(A*U.at(ind));
  }
  return res;
}

template<class T> std::vector<T> operatorDerivateSerie(std::vector<T> U)
{
  std::vector<T> res;
  size_t nbTerms=U.size();
  for (size_t i = 1; i < nbTerms; i++)
    res.push_back(i*U.at(i));
  return res;
}

template<class T> std::vector<T> operatorIntegrateSerie(std::vector<T> U)
{
  std::vector<T> res;
  res.push_back(T::Zero());
  size_t nbTerms=U.size();
  for (size_t i = 0; i < nbTerms; i++)
    res.push_back((1./(i+1.))*U.at(i));
  return res;
}

template<class T> T sumSerie(std::vector<T> U, const std::vector<real>& spowers)
{
  size_t t=U.size();
  T res = U.at(0);
  for (size_t i = 1; i < t; i++)
    res += spowers.at(i) * U.at(i);
  return res;
}

real SubSegmentComputations::unscaledKineticEnergy(real es)
{
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  computeSPowers(es);
  real w=ribbonWidth;
  real w2=w*w;
  real w3=w*w2;
  real w5=w3*w2;
  
  real en = eta.A;
  real em = eta.B;
  real eadot = omega1Dot.A;
  real ebdot = omega1Dot.B;
  real endot = etaDot.A;
  real emdot = etaDot.B;
  
  real term1=w*es*dposOrdt.dot(dposOrdt);
  SerieMat sK(FramePreTerms,es);
  SerieMat sdKda(dFramePreTermsda, es);
  SerieMat sdKdb(dFramePreTermsdb, es);
  SerieMat sdKdn(dFramePreTermsdn, es);
  SerieMat sdKdm(dFramePreTermsdm, es);
  SerieMat sdKds = sK.derivate();
  SerieVec su(PosPreTerms,es);
  SerieVec sduda(dPosPreTermsda, es);
  SerieVec sdudb(dPosPreTermsdb, es);
  SerieVec sdudn(dPosPreTermsdn, es);
  SerieVec sdudm(dPosPreTermsdm, es);
  real term2aa = eadot*sduda.dot(sduda).integrate().sum()*eadot;
  real term2ba = ebdot*sdudb.dot(sduda).integrate().sum()*eadot;
  real term2na = endot*sdudn.dot(sduda).integrate().sum()*eadot;
  real term2ma = emdot*sdudm.dot(sduda).integrate().sum()*eadot;
  real term2ab = term2ba;
  real term2bb = ebdot*sdudb.dot(sdudb).integrate().sum()*ebdot;
  real term2nb = endot*sdudn.dot(sdudb).integrate().sum()*ebdot;
  real term2mb = emdot*sdudm.dot(sdudb).integrate().sum()*ebdot;
  real term2an = term2na;
  real term2bn = term2nb;
  real term2nn = endot*sdudn.dot(sdudn).integrate().sum()*endot;
  real term2mn = emdot*sdudm.dot(sdudn).integrate().sum()*endot;
  real term2am = term2ma;
  real term2bm = term2mb;
  real term2nm = term2mn;
  real term2mm = emdot*sdudm.dot(sdudm).integrate().sum()*emdot;
  
  real term2=w*(term2aa + term2ab + term2an + term2am
    + term2ba + term2bb + term2bn + term2bm
    + term2na + term2nb + term2nn + term2nm
    + term2ma + term2mb + term2mn + term2mm
  );
  
  SerieVec sdFrameOrdtu=dframeOrdt*su;
  real term3 = sdFrameOrdtu.dot(sdFrameOrdtu).integrate().sum()*w;
  real term4a = (frameOr*sduda.integrate()).dot(dposOrdt).sum()*eadot;
  real term4b = (frameOr*sdudb.integrate()).dot(dposOrdt).sum()*ebdot;
  real term4n = (frameOr*sdudn.integrate()).dot(dposOrdt).sum()*endot;
  real term4m = (frameOr*sdudm.integrate()).dot(dposOrdt).sum()*emdot;
  real term4 = (2.*w) * (term4a + term4b + term4n + term4m);
  real term5 = (2.*w) * ((dframeOrdt*su.integrate().sum()).dot(dposOrdt));
  real term6a = (frameOr*sduda).dot(dframeOrdt*su).integrate().sum()*eadot;
  real term6b = (frameOr*sdudb).dot(dframeOrdt*su).integrate().sum()*ebdot;
  real term6n = (frameOr*sdudn).dot(dframeOrdt*su).integrate().sum()*endot;
  real term6m = (frameOr*sdudm).dot(dframeOrdt*su).integrate().sum()*emdot;
  real term6 = (2.*w) * (term6a + term6b + term6n + term6m);
  std::vector<Eigen::Vector3r> voneeta_ctr
    {Eigen::Vector3r(1., 0., em),Eigen::Vector3r(0.,0.,en)};
  SerieVec voneeta(voneeta_ctr, es);
  real w36 = w3/6.;
  real term7a = (frameOr*(sdKda*voneeta).integrate()).dot(dposOrdt).sum()*eadot;
  real term7b = (frameOr*(sdKdb*voneeta).integrate()).dot(dposOrdt).sum()*ebdot;
  real term7n = (frameOr*(sdKdn*voneeta).integrate()).dot(dposOrdt).sum()*endot;
  real term7m = (frameOr*(sdKdm*voneeta).integrate()).dot(dposOrdt).sum()*emdot;
  real term7 = (w36*en) * (term7a + term7b + term7n + term7m);
  real term8aa = eadot*(sduda.dot(sdKda*voneeta)).integrate().sum()*eadot;
  real term8ab = eadot*(sduda.dot(sdKdb*voneeta)).integrate().sum()*ebdot;
  real term8an = eadot*(sduda.dot(sdKdn*voneeta)).integrate().sum()*endot;
  real term8am = eadot*(sduda.dot(sdKdm*voneeta)).integrate().sum()*emdot;
  real term8ba = ebdot*(sdudb.dot(sdKda*voneeta)).integrate().sum()*eadot;
  real term8bb = ebdot*(sdudb.dot(sdKdb*voneeta)).integrate().sum()*ebdot;
  real term8bn = ebdot*(sdudb.dot(sdKdn*voneeta)).integrate().sum()*endot;
  real term8bm = ebdot*(sdudb.dot(sdKdm*voneeta)).integrate().sum()*emdot;
  real term8na = endot*(sdudn.dot(sdKda*voneeta)).integrate().sum()*eadot;
  real term8nb = endot*(sdudn.dot(sdKdb*voneeta)).integrate().sum()*ebdot;
  real term8nn = endot*(sdudn.dot(sdKdn*voneeta)).integrate().sum()*endot;
  real term8nm = endot*(sdudn.dot(sdKdm*voneeta)).integrate().sum()*emdot;
  real term8ma = emdot*(sdudm.dot(sdKda*voneeta)).integrate().sum()*eadot;
  real term8mb = emdot*(sdudm.dot(sdKdb*voneeta)).integrate().sum()*ebdot;
  real term8mn = emdot*(sdudm.dot(sdKdn*voneeta)).integrate().sum()*endot;
  real term8mm = emdot*(sdudm.dot(sdKdm*voneeta)).integrate().sum()*emdot;
  real term8 = (w36*en) * (term8aa + term8ab + term8an + term8am
    + term8ba + term8bb + term8bn + term8bm
    + term8na + term8nb + term8nn + term8nm
    + term8ma + term8mb + term8mn + term8mm);
  real term9a = (dframeOrdt*su).dot(sdKda*voneeta).integrate().sum()*eadot;
  real term9b = (dframeOrdt*su).dot(sdKdb*voneeta).integrate().sum()*ebdot;
  real term9n = (dframeOrdt*su).dot(sdKdn*voneeta).integrate().sum()*endot;
  real term9m = (dframeOrdt*su).dot(sdKdm*voneeta).integrate().sum()*emdot;
  real term9 = (w36*en) * (term9a + term9b + term9n + term9m);
  real term10 = (w36*en) * (dposOrdt.dot(dframeOrdt*(sK*voneeta).integrate().sum()));
  real term11a = (frameOr*sduda).dot(dframeOrdt*(sK*voneeta)).integrate().sum()*eadot;
  real term11b = (frameOr*sdudb).dot(dframeOrdt*(sK*voneeta)).integrate().sum()*ebdot;
  real term11n = (frameOr*sdudn).dot(dframeOrdt*(sK*voneeta)).integrate().sum()*endot;
  real term11m = (frameOr*sdudm).dot(dframeOrdt*(sK*voneeta)).integrate().sum()*emdot;
  real term11 = (w36*en) * (term11a + term11b + term11n + term11m);
  real term12 = (w36*en) * (dframeOrdt*su).dot(dframeOrdt*sK*voneeta).integrate().sum();
  std::vector<real> etadot_ctr{em,en};
  Serie etadot(etadot_ctr, es);
  real term13 = term13 = (-w36)*dposOrdt.dot(frameOr*(etadot*sdKds*voneeta).integrate().sum());
  real term14a = sduda.dot(etadot*sdKds*voneeta).integrate().sum()*eadot;
  real term14b = sdudb.dot(etadot*sdKds*voneeta).integrate().sum()*ebdot;
  real term14n = sdudn.dot(etadot*sdKds*voneeta).integrate().sum()*endot;
  real term14m = sdudm.dot(etadot*sdKds*voneeta).integrate().sum()*emdot;
  real term14 = (-w36) * (term14a + term14b + term14n + term14m);
  real term15 = (-w36) * (dframeOrdt*su).dot(etadot*frameOr*sdKds*voneeta).integrate().sum();
  real term16aa = eadot * (sdKda*voneeta).dot(sdKda*voneeta).integrate().sum() * eadot;
  real term16ba = ebdot * (sdKdb*voneeta).dot(sdKda*voneeta).integrate().sum() * eadot;
  real term16na = endot * (sdKdn*voneeta).dot(sdKda*voneeta).integrate().sum() * eadot;
  real term16ma = emdot * (sdKdm*voneeta).dot(sdKda*voneeta).integrate().sum() * eadot;
  real term16ab = eadot * (sdKda*voneeta).dot(sdKdb*voneeta).integrate().sum() * ebdot;
  real term16bb = ebdot * (sdKdb*voneeta).dot(sdKdb*voneeta).integrate().sum() * ebdot;
  real term16nb = endot * (sdKdn*voneeta).dot(sdKdb*voneeta).integrate().sum() * ebdot;
  real term16mb = emdot * (sdKdm*voneeta).dot(sdKdb*voneeta).integrate().sum() * ebdot;
  real term16an = eadot * (sdKda*voneeta).dot(sdKdn*voneeta).integrate().sum() * endot;
  real term16bn = ebdot * (sdKdb*voneeta).dot(sdKdn*voneeta).integrate().sum() * endot;
  real term16nn = endot * (sdKdn*voneeta).dot(sdKdn*voneeta).integrate().sum() * endot;
  real term16mn = emdot * (sdKdm*voneeta).dot(sdKdn*voneeta).integrate().sum() * endot;
  real term16am = eadot * (sdKda*voneeta).dot(sdKdm*voneeta).integrate().sum() * emdot;
  real term16bm = ebdot * (sdKdb*voneeta).dot(sdKdm*voneeta).integrate().sum() * emdot;
  real term16nm = endot * (sdKdn*voneeta).dot(sdKdm*voneeta).integrate().sum() * emdot;
  real term16mm = emdot * (sdKdm*voneeta).dot(sdKdm*voneeta).integrate().sum() * emdot;
  real term16 = (w3/12.)* (term16aa + term16ab + term16an + term16am
    + term16ba + term16bb + term16bn + term16bm
    + term16na + term16nb + term16nn + term16nm
    + term16ma + term16mb + term16mn + term16mm);
  real term17 = (w3/12.) *  (dframeOrdt*sK*voneeta).dot(dframeOrdt*sK*voneeta).integrate().sum();
  real term18a = (frameOr*sdKda*voneeta).dot(dframeOrdt*sK*voneeta).integrate().sum()*eadot;
  real term18b = (frameOr*sdKdb*voneeta).dot(dframeOrdt*sK*voneeta).integrate().sum()*ebdot;
  real term18n = (frameOr*sdKdn*voneeta).dot(dframeOrdt*sK*voneeta).integrate().sum()*endot;
  real term18m = (frameOr*sdKdm*voneeta).dot(dframeOrdt*sK*voneeta).integrate().sum()*emdot;
  real term18 = w36 * (term18a + term18b + term18n + term18m);
  real term19 = (w5/12.)*(sdKds*voneeta).dot(etadot*etadot*sdKds*voneeta).integrate().sum();

  real eEc_fm = term1 + term2 + term3
                      + term4 + term5 + term6
                      + term7 + term8 + term9
                      + term10 + term11 + term12
                      + term13 + term14 + term15
                      + term16 + term17 + term18
                      + term19;

  real Ec = eEc_fm / 2.;
  return Ec;
}

Eigen::Matrix4r SubSegmentComputations::unscaledd2KineticEnergydqdot2(real es)
{
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  computeSPowers(es);
  real w=ribbonWidth;
  real w2=w*w;
  real w3=w*w2;
  real w5=w3*w2;
  
  real en = eta.A;
  real em = eta.B;
  real w36 = w3/6.;
  std::vector<Eigen::Vector3r> voneeta_ctr
    {Eigen::Vector3r(1., 0., em),Eigen::Vector3r(0.,0.,en)};
  SerieVec voneeta(voneeta_ctr, es);
  Serie zeroOne({0.,1.},es);
  Serie zeroZeroOne({0.,0.,1.},es);
  SerieMat sK(FramePreTerms, es);
  SerieMat sdKda(dFramePreTermsda, es);
  SerieMat sdKdb(dFramePreTermsdb, es);
  SerieMat sdKdn(dFramePreTermsdn, es);
  SerieMat sdKdm(dFramePreTermsdm, es);
  SerieMat sdKds = sK.derivate();
  SerieVec su(PosPreTerms, es);
  SerieVec sduda(dPosPreTermsda, es);
  SerieVec sdudb(dPosPreTermsdb, es);
  SerieVec sdudn(dPosPreTermsdn, es);
  SerieVec sdudm(dPosPreTermsdm, es);
  
  real term1aa = 2.*w*sduda.dot(sduda).integrate().sum();
  real term1ab = 2.*w*sduda.dot(sdudb).integrate().sum();
  real term1an = 2.*w*sduda.dot(sdudn).integrate().sum();
  real term1am = 2.*w*sduda.dot(sdudm).integrate().sum();
  real term1ba = 2.*w*sdudb.dot(sduda).integrate().sum();
  real term1bb = 2.*w*sdudb.dot(sdudb).integrate().sum();
  real term1bn = 2.*w*sdudb.dot(sdudn).integrate().sum();
  real term1bm = 2.*w*sdudb.dot(sdudm).integrate().sum();
  real term1na = 2.*w*sdudn.dot(sduda).integrate().sum();
  real term1nb = 2.*w*sdudn.dot(sdudb).integrate().sum();
  real term1nn = 2.*w*sdudn.dot(sdudn).integrate().sum();
  real term1nm = 2.*w*sdudn.dot(sdudm).integrate().sum();
  real term1ma = 2.*w*sdudm.dot(sduda).integrate().sum();
  real term1mb = 2.*w*sdudm.dot(sdudb).integrate().sum();
  real term1mn = 2.*w*sdudm.dot(sdudn).integrate().sum();
  real term1mm = 2.*w*sdudm.dot(sdudm).integrate().sum();
  
  real term2aa=2.*w36*en*sduda.dot(sdKda*voneeta).integrate().sum();
  real term2ab=w36*en*(sduda.dot(sdKdb*voneeta).integrate().sum()+sdudb.dot(sdKda*voneeta).integrate().sum());
  real term2an=w36*en*(sduda.dot(sdKdn*voneeta).integrate().sum()+sdudn.dot(sdKda*voneeta).integrate().sum());
  real term2am=w36*en*(sduda.dot(sdKdm*voneeta).integrate().sum()+sdudm.dot(sdKda*voneeta).integrate().sum());
  real term2ba=term2ab;
  real term2bb=2.*w36*en*sdudb.dot(sdKdb*voneeta).integrate().sum();
  real term2bn=w36*en*(sdudb.dot(sdKdn*voneeta).integrate().sum()+sdudn.dot(sdKdb*voneeta).integrate().sum());
  real term2bm=w36*en*(sdudb.dot(sdKdm*voneeta).integrate().sum()+sdudm.dot(sdKdb*voneeta).integrate().sum());
  real term2na=term2an;
  real term2nb=term2bn;
  real term2nn=2.*w36*en*sdudn.dot(sdKdn*voneeta).integrate().sum();
  real term2nm=w36*en*(sdudn.dot(sdKdm*voneeta).integrate().sum()+sdudm.dot(sdKdn*voneeta).integrate().sum());
  real term2ma=term2am;
  real term2mb=term2bm;
  real term2mn=term2nm;
  real term2mm=2.*w36*en*sdudm.dot(sdKdm*voneeta).integrate().sum();
  
  real term3an=-w36*(zeroOne * (sduda.dot(sdKds*voneeta))).integrate().sum();
  real term3am=-w36*(sduda.dot(sdKds*voneeta)).integrate().sum();
  real term3bn=-w36*(zeroOne * (sdudb.dot(sdKds*voneeta))).integrate().sum();
  real term3bm=-w36*(sdudb.dot(sdKds*voneeta)).integrate().sum();
  real term3na=term3an;
  real term3nb=term3bn;
  real term3nn=-2.*w36*(zeroOne * (sdudn.dot(sdKds*voneeta))).integrate().sum();
  real term3nm=-w36*((zeroOne * (sdudm.dot(sdKds*voneeta))).integrate().sum()+(sdudn.dot(sdKds*voneeta)).integrate().sum());
  real term3ma=term3am;
  real term3mb=term3bm;
  real term3mn=term3nm;
  real term3mm=-2.*w36*(sdudn.dot(sdKds*voneeta)).integrate().sum();
  
  real term4aa=w36*(sdKda*voneeta).dot(sdKda*voneeta).integrate().sum();
  real term4ab=w36*(sdKda*voneeta).dot(sdKdb*voneeta).integrate().sum();
  real term4an=w36*(sdKda*voneeta).dot(sdKdn*voneeta).integrate().sum();
  real term4am=w36*(sdKda*voneeta).dot(sdKdm*voneeta).integrate().sum();
  real term4ba=w36*(sdKdb*voneeta).dot(sdKda*voneeta).integrate().sum();
  real term4bb=w36*(sdKdb*voneeta).dot(sdKdb*voneeta).integrate().sum();
  real term4bn=w36*(sdKdb*voneeta).dot(sdKdn*voneeta).integrate().sum();
  real term4bm=w36*(sdKdb*voneeta).dot(sdKdm*voneeta).integrate().sum();
  real term4na=w36*(sdKdn*voneeta).dot(sdKda*voneeta).integrate().sum();
  real term4nb=w36*(sdKdn*voneeta).dot(sdKdb*voneeta).integrate().sum();
  real term4nn=w36*(sdKdn*voneeta).dot(sdKdn*voneeta).integrate().sum();
  real term4nm=w36*(sdKdn*voneeta).dot(sdKdm*voneeta).integrate().sum();
  real term4ma=w36*(sdKdm*voneeta).dot(sdKda*voneeta).integrate().sum();
  real term4mb=w36*(sdKdm*voneeta).dot(sdKdb*voneeta).integrate().sum();
  real term4mn=w36*(sdKdm*voneeta).dot(sdKdn*voneeta).integrate().sum();
  real term4mm=w36*(sdKdm*voneeta).dot(sdKdm*voneeta).integrate().sum();
  
  
  real term5nn=w5/40.*(zeroZeroOne*(sdKds*voneeta).dot(sdKds*voneeta)).integrate().sum();
  real term5nm=w5/40.*(zeroOne*(sdKds*voneeta).dot(sdKds*voneeta)).integrate().sum();
  real term5mn=term5nm;
  real term5mm=w5/40.*(sdKds*voneeta).dot(sdKds*voneeta).integrate().sum();
  
  Eigen::Matrix4r res;
  res<<
    term1aa+term2aa+term4aa, term1ab+term2ab+term4ab, term1an+term2an+term3an+term4an, term1am+term2am+term3am+term4am,
    term1ba+term2ba+term4ba, term1bb+term2bb+term4bb, term1bn+term2bn+term3bn+term4bn, term1bm+term2bm+term3bm+term4bm,
    term1na+term2na+term3na+term4na, term1nb+term2nb+term3nb+term4nb, term1nn+term2nn+term3nn+term4nn+term5nn, term1nm+term2nm+term3nm+term4nm+term5nm,
    term1ma+term2ma+term3ma+term4ma, term1mb+term2mb+term3mb+term4mb, term1mn+term2mn+term3mn+term4mn+term5mn,term1mm+term2mm+term3mm+term4mm+term5mm;
  return res/2.;
    
}

Eigen::Vector4r SubSegmentComputations::unscaledLangrangianKineticB(real es)
{
  if (!called["computeSeriePreTerms"])computeSeriePreTerms();
  if (!called["computeDerivationsPreTerms"])computeDerivationsPreTerms();
  if (!called["computeDerivationsPreTermsSecondOrder"])computeDerivationsPreTermsSecondOrder();
  computeSPowers(es);
  real w=ribbonWidth;
  real w2=w*w;
  real w3=w*w2;
  real w5=w3*w2;
  
  real en = eta.A;
  real em = eta.B;
  real eadot = omega1Dot.A;
  real ebdot = omega1Dot.B;
  real endot = etaDot.A;
  real emdot = etaDot.B;
  real w36 = w3/6.;
  std::vector<Eigen::Vector3r> voneeta_ctr
    {Eigen::Vector3r(1., 0., em),Eigen::Vector3r(0.,0.,en)};
  SerieVec voneeta(voneeta_ctr, es);
  Serie zeroOne({0.,1.},es);
  Serie zeroZeroOne({0.,0.,1.},es);
  std::vector<real> etadot_ctr{emdot,endot};
  Serie setadot(etadot_ctr, es);
  std::vector<real> eta_ctr{em,en};
  Serie seta(eta_ctr, es);
  SerieMat sK(FramePreTerms, es);
  SerieMat sdKda(dFramePreTermsda, es);
  SerieMat sdKdb(dFramePreTermsdb, es);
  SerieMat sdKdn(dFramePreTermsdn, es);
  SerieMat sdKdm(dFramePreTermsdm, es);
  SerieMat sdKds = sK.derivate();
  SerieVec su(PosPreTerms, es);
  SerieVec sduda(dPosPreTermsda, es);
  SerieVec sdudb(dPosPreTermsdb, es);
  SerieVec sdudn(dPosPreTermsdn, es);
  SerieVec sdudm(dPosPreTermsdm, es);
  SerieVec sd2udada(dPosPreTermsdaa, es);
  SerieVec sd2udadb(dPosPreTermsdab, es);
  SerieVec sd2udadn(dPosPreTermsdan, es);
  SerieVec sd2udadm(dPosPreTermsdam, es);
  SerieVec sd2udbda(dPosPreTermsdba, es);
  SerieVec sd2udbdb(dPosPreTermsdbb, es);
  SerieVec sd2udbdn(dPosPreTermsdbn, es);
  SerieVec sd2udbdm(dPosPreTermsdbm, es);
  SerieVec sd2udnda(dPosPreTermsdna, es);
  SerieVec sd2udndb(dPosPreTermsdnb, es);
  SerieVec sd2udndn(dPosPreTermsdnn, es);
  SerieVec sd2udndm(dPosPreTermsdnm, es);
  SerieVec sd2udmda(dPosPreTermsdma, es);
  SerieVec sd2udmdb(dPosPreTermsdmb, es);
  SerieVec sd2udmdn(dPosPreTermsdmn, es);
  SerieVec sd2udmdm(dPosPreTermsdmm, es);
  SerieMat sd2Kdada(dFramePreTermsdaa, es);
  SerieMat sd2Kdadb(dFramePreTermsdab, es);
  SerieMat sd2Kdadn(dFramePreTermsdan, es);
  SerieMat sd2Kdadm(dFramePreTermsdam, es);
  SerieMat sd2Kdbda(dFramePreTermsdba, es);
  SerieMat sd2Kdbdb(dFramePreTermsdbb, es);
  SerieMat sd2Kdbdn(dFramePreTermsdbn, es);
  SerieMat sd2Kdbdm(dFramePreTermsdbm, es);
  SerieMat sd2Kdnda(dFramePreTermsdna, es);
  SerieMat sd2Kdndb(dFramePreTermsdnb, es);
  SerieMat sd2Kdndn(dFramePreTermsdnn, es);
  SerieMat sd2Kdndm(dFramePreTermsdnm, es);
  SerieMat sd2Kdmda(dFramePreTermsdma, es);
  SerieMat sd2Kdmdb(dFramePreTermsdmb, es);
  SerieMat sd2Kdmdn(dFramePreTermsdmn, es);
  SerieMat sd2Kdmdm(dFramePreTermsdmm, es);
  
  SerieMat sd2Kdsda = sdKda.derivate();
  SerieMat sd2Kdsdb = sdKdb.derivate();
  SerieMat sd2Kdsdn = sdKdn.derivate();
  SerieMat sd2Kdsdm = sdKdm.derivate();
  
  
  Eigen::Vector4r term1aa; 
  term1aa << sd2udada.dot(sduda).integrate().sum(), sd2udadb.dot(sduda).integrate().sum(), sd2udadn.dot(sduda).integrate().sum(), sd2udadm.dot(sduda).integrate().sum();
  term1aa*=-2*w*eadot*eadot;
  Eigen::Vector4r term1ab; 
  term1ab << sd2udada.dot(sdudb).integrate().sum(), sd2udadb.dot(sdudb).integrate().sum(), sd2udadn.dot(sdudb).integrate().sum(), sd2udadm.dot(sdudb).integrate().sum();
  term1ab*=-2*w*eadot*ebdot;
  Eigen::Vector4r term1an; 
  term1an << sd2udada.dot(sdudn).integrate().sum(), sd2udadb.dot(sdudn).integrate().sum(), sd2udadn.dot(sdudn).integrate().sum(), sd2udadm.dot(sdudn).integrate().sum();
  term1an*=-2*w*eadot*endot;
  Eigen::Vector4r term1am; 
  term1am << sd2udada.dot(sdudm).integrate().sum(), sd2udadb.dot(sdudm).integrate().sum(), sd2udadn.dot(sdudm).integrate().sum(), sd2udadm.dot(sdudm).integrate().sum();
  term1am*=-2*w*eadot*emdot;
  
  Eigen::Vector4r term1ba; 
  term1ba << sd2udbda.dot(sduda).integrate().sum(), sd2udbdb.dot(sduda).integrate().sum(), sd2udbdn.dot(sduda).integrate().sum(), sd2udbdm.dot(sduda).integrate().sum();
  term1ba*=-2*w*ebdot*eadot;
  Eigen::Vector4r term1bb; 
  term1bb << sd2udbda.dot(sdudb).integrate().sum(), sd2udbdb.dot(sdudb).integrate().sum(), sd2udbdn.dot(sdudb).integrate().sum(), sd2udbdm.dot(sdudb).integrate().sum();
  term1bb*=-2*w*ebdot*ebdot;
  Eigen::Vector4r term1bn; 
  term1bn << sd2udbda.dot(sdudn).integrate().sum(), sd2udbdb.dot(sdudn).integrate().sum(), sd2udbdn.dot(sdudn).integrate().sum(), sd2udbdm.dot(sdudn).integrate().sum();
  term1bn*=-2*w*ebdot*endot;
  Eigen::Vector4r term1bm; 
  term1bm << sd2udbda.dot(sdudm).integrate().sum(), sd2udbdb.dot(sdudm).integrate().sum(), sd2udbdn.dot(sdudm).integrate().sum(), sd2udbdm.dot(sdudm).integrate().sum();
  term1bm*=-2*w*ebdot*emdot;
  
  Eigen::Vector4r term1na; 
  term1na << sd2udnda.dot(sduda).integrate().sum(), sd2udndb.dot(sduda).integrate().sum(), sd2udndn.dot(sduda).integrate().sum(), sd2udndm.dot(sduda).integrate().sum();
  term1na*=-2*w*endot*eadot;
  Eigen::Vector4r term1nb; 
  term1nb << sd2udnda.dot(sdudb).integrate().sum(), sd2udndb.dot(sdudb).integrate().sum(), sd2udndn.dot(sdudb).integrate().sum(), sd2udndm.dot(sdudb).integrate().sum();
  term1nb*=-2*w*endot*ebdot;
  Eigen::Vector4r term1nn; 
  term1nn << sd2udnda.dot(sdudn).integrate().sum(), sd2udndb.dot(sdudn).integrate().sum(), sd2udndn.dot(sdudn).integrate().sum(), sd2udndm.dot(sdudn).integrate().sum();
  term1nn*=-2*w*endot*endot;
  Eigen::Vector4r term1nm; 
  term1nm << sd2udnda.dot(sdudm).integrate().sum(), sd2udndb.dot(sdudm).integrate().sum(), sd2udndn.dot(sdudm).integrate().sum(), sd2udndm.dot(sdudm).integrate().sum();
  term1nm*=-2*w*endot*emdot;
  
  Eigen::Vector4r term1ma; 
  term1ma << sd2udmda.dot(sduda).integrate().sum(), sd2udmdb.dot(sduda).integrate().sum(), sd2udmdn.dot(sduda).integrate().sum(), sd2udmdm.dot(sduda).integrate().sum();
  term1ma*=-2*w*emdot*eadot;
  Eigen::Vector4r term1mb; 
  term1mb << sd2udmda.dot(sdudb).integrate().sum(), sd2udmdb.dot(sdudb).integrate().sum(), sd2udmdn.dot(sdudb).integrate().sum(), sd2udmdm.dot(sdudb).integrate().sum();
  term1mb*=-2*w*emdot*ebdot;
  Eigen::Vector4r term1mn; 
  term1mn << sd2udmda.dot(sdudn).integrate().sum(), sd2udmdb.dot(sdudn).integrate().sum(), sd2udmdn.dot(sdudn).integrate().sum(), sd2udmdm.dot(sdudn).integrate().sum();
  term1mn*=-2*w*emdot*endot;
  Eigen::Vector4r term1mm; 
  term1mm << sd2udmda.dot(sdudm).integrate().sum(), sd2udmdb.dot(sdudm).integrate().sum(), sd2udmdn.dot(sdudm).integrate().sum(), sd2udmdm.dot(sdudm).integrate().sum();
  term1mm*=-2*w*emdot*emdot;
  
  Eigen::Vector4r term1 = term1aa + term1ab + term1an + term1am + term1ba + term1bb + term1bn + term1bm + term1na + term1nb + term1nn + term1nm + term1ma + term1mb + term1mn + term1mm;
  
  Eigen::Vector4r term2;
  term2 << (dframeOrdt*sduda).dot(dframeOrdt*su).integrate().sum(), (dframeOrdt*sdudb).dot(dframeOrdt*su).integrate().sum(), (dframeOrdt*sdudn).dot(dframeOrdt*su).integrate().sum(), (dframeOrdt*sdudm).dot(dframeOrdt*su).integrate().sum();
  term2*=2*w;
  
  Eigen::Vector4r term3;
  term3 << (dframeOrdt*sduda).dot(dposOrdt).integrate().sum(), (dframeOrdt*sdudb).dot(dposOrdt).integrate().sum(), (dframeOrdt*sdudn).dot(dposOrdt).integrate().sum(), (dframeOrdt*sdudm).dot(dposOrdt).integrate().sum();
  term3*=2*w;
  
  real term4aa = -w36*eadot*sduda.dot(sdKda*voneeta).integrate().sum()*eadot;
  real term4ab = -w36*eadot*sduda.dot(sdKdb*voneeta).integrate().sum()*ebdot;
  real term4an = -w36*eadot*sduda.dot(sdKdn*voneeta).integrate().sum()*endot;
  real term4am = -w36*eadot*sduda.dot(sdKdm*voneeta).integrate().sum()*emdot;
  real term4ba = -w36*ebdot*sdudb.dot(sdKda*voneeta).integrate().sum()*eadot;
  real term4bb = -w36*ebdot*sdudb.dot(sdKdb*voneeta).integrate().sum()*ebdot;
  real term4bn = -w36*ebdot*sdudb.dot(sdKdn*voneeta).integrate().sum()*endot;
  real term4bm = -w36*ebdot*sdudb.dot(sdKdm*voneeta).integrate().sum()*emdot;
  real term4na = -w36*eadot*sdudn.dot(sdKda*voneeta).integrate().sum()*eadot;
  real term4nb = -w36*eadot*sdudn.dot(sdKdb*voneeta).integrate().sum()*ebdot;
  real term4nn = -w36*eadot*sdudn.dot(sdKdn*voneeta).integrate().sum()*endot;
  real term4nm = -w36*eadot*sdudn.dot(sdKdm*voneeta).integrate().sum()*emdot;
  real term4ma = -w36*ebdot*sdudm.dot(sdKda*voneeta).integrate().sum()*eadot;
  real term4mb = -w36*ebdot*sdudm.dot(sdKdb*voneeta).integrate().sum()*ebdot;
  real term4mn = -w36*ebdot*sdudm.dot(sdKdn*voneeta).integrate().sum()*endot;
  real term4mm = -w36*ebdot*sdudm.dot(sdKdm*voneeta).integrate().sum()*emdot;
  Eigen::Vector4r term4;
  term4 << 0., 0., term4aa + term4ab + term4an + term4am + term4ba + term4bb + term4bn + term4bm + term4na + term4nb + term4nn + term4nm + term4ma + term4mb + term4mn + term4mm, 0.;
  
  
  Eigen::Vector4r term5aa; 
  term5aa << sd2udada.dot(sdKda*voneeta).integrate().sum(), sd2udadb.dot(sdKda*voneeta).integrate().sum(), sd2udadn.dot(sdKda*voneeta).integrate().sum(), sd2udadm.dot(sdKda*voneeta).integrate().sum();
  term5aa*=-w36*en*eadot*eadot;
  Eigen::Vector4r term5ab; 
  term5ab << sd2udada.dot(sdKdb*voneeta).integrate().sum(), sd2udadb.dot(sdKdb*voneeta).integrate().sum(), sd2udadn.dot(sdKdb*voneeta).integrate().sum(), sd2udadm.dot(sdKdb*voneeta).integrate().sum();
  term5ab*=-w36*en*eadot*ebdot;
  Eigen::Vector4r term5an; 
  term5an << sd2udada.dot(sdKdn*voneeta).integrate().sum(), sd2udadb.dot(sdKdn*voneeta).integrate().sum(), sd2udadn.dot(sdKdn*voneeta).integrate().sum(), sd2udadm.dot(sdKdn*voneeta).integrate().sum();
  term5an*=-w36*en*eadot*endot;
  Eigen::Vector4r term5am; 
  term5am << sd2udada.dot(sdKdm*voneeta).integrate().sum(), sd2udadb.dot(sdKdm*voneeta).integrate().sum(), sd2udadn.dot(sdKdm*voneeta).integrate().sum(), sd2udadm.dot(sdKdm*voneeta).integrate().sum();
  term5am*=-w36*en*eadot*emdot;
  
  Eigen::Vector4r term5ba; 
  term5ba << sd2udbda.dot(sdKda*voneeta).integrate().sum(), sd2udbdb.dot(sdKda*voneeta).integrate().sum(), sd2udbdn.dot(sdKda*voneeta).integrate().sum(), sd2udbdm.dot(sdKda*voneeta).integrate().sum();
  term5ba*=-w36*en*ebdot*eadot;
  Eigen::Vector4r term5bb; 
  term5bb << sd2udbda.dot(sdKdb*voneeta).integrate().sum(), sd2udbdb.dot(sdKdb*voneeta).integrate().sum(), sd2udbdn.dot(sdKdb*voneeta).integrate().sum(), sd2udbdm.dot(sdKdb*voneeta).integrate().sum();
  term5bb*=-w36*en*ebdot*ebdot;
  Eigen::Vector4r term5bn; 
  term5bn << sd2udbda.dot(sdKdn*voneeta).integrate().sum(), sd2udbdb.dot(sdKdn*voneeta).integrate().sum(), sd2udbdn.dot(sdKdn*voneeta).integrate().sum(), sd2udbdm.dot(sdKdn*voneeta).integrate().sum();
  term5bn*=-w36*en*ebdot*endot;
  Eigen::Vector4r term5bm; 
  term5bm << sd2udbda.dot(sdKdm*voneeta).integrate().sum(), sd2udbdb.dot(sdKdm*voneeta).integrate().sum(), sd2udbdn.dot(sdKdm*voneeta).integrate().sum(), sd2udbdm.dot(sdKdm*voneeta).integrate().sum();
  term5bm*=-w36*en*ebdot*emdot;
  
  Eigen::Vector4r term5na; 
  term5na << sd2udnda.dot(sdKda*voneeta).integrate().sum(), sd2udndb.dot(sdKda*voneeta).integrate().sum(), sd2udndn.dot(sdKda*voneeta).integrate().sum(), sd2udndm.dot(sdKda*voneeta).integrate().sum();
  term5na*=-w36*en*endot*eadot;
  Eigen::Vector4r term5nb; 
  term5nb << sd2udnda.dot(sdKdb*voneeta).integrate().sum(), sd2udndb.dot(sdKdb*voneeta).integrate().sum(), sd2udndn.dot(sdKdb*voneeta).integrate().sum(), sd2udndm.dot(sdKdb*voneeta).integrate().sum();
  term5nb*=-w36*en*endot*ebdot;
  Eigen::Vector4r term5nn; 
  term5nn << sd2udnda.dot(sdKdn*voneeta).integrate().sum(), sd2udndb.dot(sdKdn*voneeta).integrate().sum(), sd2udndn.dot(sdKdn*voneeta).integrate().sum(), sd2udndm.dot(sdKdn*voneeta).integrate().sum();
  term5nn*=-w36*en*endot*endot;
  Eigen::Vector4r term5nm; 
  term5nm << sd2udnda.dot(sdKdm*voneeta).integrate().sum(), sd2udndb.dot(sdKdm*voneeta).integrate().sum(), sd2udndn.dot(sdKdm*voneeta).integrate().sum(), sd2udndm.dot(sdKdm*voneeta).integrate().sum();
  term5nm*=-w36*en*endot*emdot;
  
  Eigen::Vector4r term5ma; 
  term5ma << sd2udmda.dot(sdKda*voneeta).integrate().sum(), sd2udmdb.dot(sdKda*voneeta).integrate().sum(), sd2udmdn.dot(sdKda*voneeta).integrate().sum(), sd2udmdm.dot(sdKda*voneeta).integrate().sum();
  term5ma*=-w36*en*emdot*eadot;
  Eigen::Vector4r term5mb; 
  term5mb << sd2udmda.dot(sdKdb*voneeta).integrate().sum(), sd2udmdb.dot(sdKdb*voneeta).integrate().sum(), sd2udmdn.dot(sdKdb*voneeta).integrate().sum(), sd2udmdm.dot(sdKdb*voneeta).integrate().sum();
  term5mb*=-w36*en*emdot*ebdot;
  Eigen::Vector4r term5mn; 
  term5mn << sd2udmda.dot(sdKdn*voneeta).integrate().sum(), sd2udmdb.dot(sdKdn*voneeta).integrate().sum(), sd2udmdn.dot(sdKdn*voneeta).integrate().sum(), sd2udmdm.dot(sdKdn*voneeta).integrate().sum();
  term5mn*=-w36*en*emdot*endot;
  Eigen::Vector4r term5mm; 
  term5mm << sd2udmda.dot(sdKdm*voneeta).integrate().sum(), sd2udmdb.dot(sdKdm*voneeta).integrate().sum(), sd2udmdn.dot(sdKdm*voneeta).integrate().sum(), sd2udmdm.dot(sdKdm*voneeta).integrate().sum();
  term5mm*=-w36*en*emdot*emdot;
  
  Eigen::Vector4r term5 = term5aa + term5ab + term5an + term5am + term5ba + term5bb + term5bn + term5bm + term5na + term5nb + term5nn + term5nm + term5ma + term5mb + term5mn + term5mm;
  
  
  Eigen::Vector4r term6aa; 
  term6aa << (sd2Kdada*voneeta).dot(sduda).integrate().sum(), (sd2Kdadb*voneeta).dot(sduda).integrate().sum(), (sd2Kdadn*voneeta).dot(sduda).integrate().sum(), (sd2Kdadm*voneeta).dot(sduda).integrate().sum();
  term6aa*=-w36*en*eadot*eadot;
  Eigen::Vector4r term6ab; 
  term6ab << (sd2Kdada*voneeta).dot(sdudb).integrate().sum(), (sd2Kdadb*voneeta).dot(sdudb).integrate().sum(), (sd2Kdadn*voneeta).dot(sdudb).integrate().sum(), (sd2Kdadm*voneeta).dot(sdudb).integrate().sum();
  term6ab*=-w36*en*eadot*ebdot;
  Eigen::Vector4r term6an; 
  term6an << (sd2Kdada*voneeta).dot(sdudn).integrate().sum(), (sd2Kdadb*voneeta).dot(sdudn).integrate().sum(), (sd2Kdadn*voneeta).dot(sdudn).integrate().sum(), (sd2Kdadm*voneeta).dot(sdudn).integrate().sum();
  term6an*=-w36*en*eadot*endot;
  Eigen::Vector4r term6am; 
  term6am << (sd2Kdada*voneeta).dot(sdudm).integrate().sum(), (sd2Kdadb*voneeta).dot(sdudm).integrate().sum(), (sd2Kdadn*voneeta).dot(sdudm).integrate().sum(), (sd2Kdadm*voneeta).dot(sdudm).integrate().sum();
  term6am*=-w36*en*eadot*emdot;
  
  Eigen::Vector4r term6ba; 
  term6ba << (sd2Kdbda*voneeta).dot(sduda).integrate().sum(), (sd2Kdbdb*voneeta).dot(sduda).integrate().sum(), (sd2Kdbdn*voneeta).dot(sduda).integrate().sum(), (sd2Kdbdm*voneeta).dot(sduda).integrate().sum();
  term6ba*=-w36*en*ebdot*eadot;
  Eigen::Vector4r term6bb; 
  term6bb << (sd2Kdbda*voneeta).dot(sdudb).integrate().sum(), (sd2Kdbdb*voneeta).dot(sdudb).integrate().sum(), (sd2Kdbdn*voneeta).dot(sdudb).integrate().sum(), (sd2Kdbdm*voneeta).dot(sdudb).integrate().sum();
  term6bb*=-w36*en*ebdot*ebdot;
  Eigen::Vector4r term6bn; 
  term6bn << (sd2Kdbda*voneeta).dot(sdudn).integrate().sum(), (sd2Kdbdb*voneeta).dot(sdudn).integrate().sum(), (sd2Kdbdn*voneeta).dot(sdudn).integrate().sum(), (sd2Kdbdm*voneeta).dot(sdudn).integrate().sum();
  term6bn*=-w36*en*ebdot*endot;
  Eigen::Vector4r term6bm; 
  term6bm << (sd2Kdbda*voneeta).dot(sdudm).integrate().sum(), (sd2Kdbdb*voneeta).dot(sdudm).integrate().sum(), (sd2Kdbdn*voneeta).dot(sdudm).integrate().sum(), (sd2Kdbdm*voneeta).dot(sdudm).integrate().sum();
  term6bm*=-w36*en*ebdot*emdot;
  
  Eigen::Vector4r term6na; 
  term6na << (sd2Kdnda*voneeta).dot(sduda).integrate().sum(), (sd2Kdndb*voneeta).dot(sduda).integrate().sum(), (sd2Kdndn*voneeta).dot(sduda).integrate().sum(), (sd2Kdndm*voneeta).dot(sduda).integrate().sum();
  term6na*=-w36*en*endot*eadot;
  Eigen::Vector4r term6nb; 
  term6nb << (sd2Kdnda*voneeta).dot(sdudb).integrate().sum(), (sd2Kdndb*voneeta).dot(sdudb).integrate().sum(), (sd2Kdndn*voneeta).dot(sdudb).integrate().sum(), (sd2Kdndm*voneeta).dot(sdudb).integrate().sum();
  term6nb*=-w36*en*endot*ebdot;
  Eigen::Vector4r term6nn; 
  term6nn << (sd2Kdnda*voneeta).dot(sdudn).integrate().sum(), (sd2Kdndb*voneeta).dot(sdudn).integrate().sum(), (sd2Kdndn*voneeta).dot(sdudn).integrate().sum(), (sd2Kdndm*voneeta).dot(sdudn).integrate().sum();
  term6nn*=-w36*en*endot*endot;
  Eigen::Vector4r term6nm; 
  term6nm << (sd2Kdnda*voneeta).dot(sdudm).integrate().sum(), (sd2Kdndb*voneeta).dot(sdudm).integrate().sum(), (sd2Kdndn*voneeta).dot(sdudm).integrate().sum(), (sd2Kdndm*voneeta).dot(sdudm).integrate().sum();
  term6nm*=-w36*en*endot*emdot;
  
  Eigen::Vector4r term6ma; 
  term6ma << (sd2Kdmda*voneeta).dot(sduda).integrate().sum(), (sd2Kdmdb*voneeta).dot(sduda).integrate().sum(), (sd2Kdmdn*voneeta).dot(sduda).integrate().sum(), (sd2Kdmdm*voneeta).dot(sduda).integrate().sum();
  term6ma*=-w36*en*emdot*eadot;
  Eigen::Vector4r term6mb; 
  term6mb << (sd2Kdmda*voneeta).dot(sdudb).integrate().sum(), (sd2Kdmdb*voneeta).dot(sdudb).integrate().sum(), (sd2Kdmdn*voneeta).dot(sdudb).integrate().sum(), (sd2Kdmdm*voneeta).dot(sdudb).integrate().sum();
  term6mb*=-w36*en*emdot*ebdot;
  Eigen::Vector4r term6mn; 
  term6mn << (sd2Kdmda*voneeta).dot(sdudn).integrate().sum(), (sd2Kdmdb*voneeta).dot(sdudn).integrate().sum(), (sd2Kdmdn*voneeta).dot(sdudn).integrate().sum(), (sd2Kdmdm*voneeta).dot(sdudn).integrate().sum();
  term6mn*=-w36*en*emdot*endot;
  Eigen::Vector4r term6mm; 
  term6mm << (sd2Kdmda*voneeta).dot(sdudm).integrate().sum(), (sd2Kdmdb*voneeta).dot(sdudm).integrate().sum(), (sd2Kdmdn*voneeta).dot(sdudm).integrate().sum(), (sd2Kdmdm*voneeta).dot(sdudm).integrate().sum();
  term6mm*=-w36*en*emdot*emdot;
  
  Eigen::Vector4r term6 = term6aa + term6ab + term6an + term6am + term6ba + term6bb + term6bn + term6bm + term6na + term6nb + term6nn + term6nm + term6ma + term6mb + term6mn + term6mm;
  
  real term7aa_partn = -w36*en*eadot*(sduda.dot(sdKda.col(2))*zeroOne).integrate().sum()*eadot;
  real term7ab_partn = -w36*en*eadot*(sduda.dot(sdKdb.col(2))*zeroOne).integrate().sum()*ebdot;
  real term7an_partn = -w36*en*eadot*(sduda.dot(sdKdn.col(2))*zeroOne).integrate().sum()*endot;
  real term7am_partn = -w36*en*eadot*(sduda.dot(sdKdm.col(2))*zeroOne).integrate().sum()*emdot;
  real term7ba_partn = -w36*en*ebdot*(sdudb.dot(sdKda.col(2))*zeroOne).integrate().sum()*eadot;
  real term7bb_partn = -w36*en*ebdot*(sdudb.dot(sdKdb.col(2))*zeroOne).integrate().sum()*ebdot;
  real term7bn_partn = -w36*en*ebdot*(sdudb.dot(sdKdn.col(2))*zeroOne).integrate().sum()*endot;
  real term7bm_partn = -w36*en*ebdot*(sdudb.dot(sdKdm.col(2))*zeroOne).integrate().sum()*emdot;
  real term7na_partn = -w36*en*endot*(sdudn.dot(sdKda.col(2))*zeroOne).integrate().sum()*eadot;
  real term7nb_partn = -w36*en*endot*(sdudn.dot(sdKdb.col(2))*zeroOne).integrate().sum()*ebdot;
  real term7nn_partn = -w36*en*endot*(sdudn.dot(sdKdn.col(2))*zeroOne).integrate().sum()*endot;
  real term7nm_partn = -w36*en*endot*(sdudn.dot(sdKdm.col(2))*zeroOne).integrate().sum()*emdot;
  real term7ma_partn = -w36*en*emdot*(sdudm.dot(sdKda.col(2))*zeroOne).integrate().sum()*eadot;
  real term7mb_partn = -w36*en*emdot*(sdudm.dot(sdKdb.col(2))*zeroOne).integrate().sum()*ebdot;
  real term7mn_partn = -w36*en*emdot*(sdudm.dot(sdKdn.col(2))*zeroOne).integrate().sum()*endot;
  real term7mm_partn = -w36*en*emdot*(sdudm.dot(sdKdm.col(2))*zeroOne).integrate().sum()*emdot;
  
  real term7aa_partm = -w36*en*eadot*(sduda.dot(sdKda.col(2))).integrate().sum()*eadot;
  real term7ab_partm = -w36*en*eadot*(sduda.dot(sdKdb.col(2))).integrate().sum()*ebdot;
  real term7an_partm = -w36*en*eadot*(sduda.dot(sdKdn.col(2))).integrate().sum()*endot;
  real term7am_partm = -w36*en*eadot*(sduda.dot(sdKdm.col(2))).integrate().sum()*emdot;
  real term7ba_partm = -w36*en*ebdot*(sdudb.dot(sdKda.col(2))).integrate().sum()*eadot;
  real term7bb_partm = -w36*en*ebdot*(sdudb.dot(sdKdb.col(2))).integrate().sum()*ebdot;
  real term7bn_partm = -w36*en*ebdot*(sdudb.dot(sdKdn.col(2))).integrate().sum()*endot;
  real term7bm_partm = -w36*en*ebdot*(sdudb.dot(sdKdm.col(2))).integrate().sum()*emdot;
  real term7na_partm = -w36*en*endot*(sdudn.dot(sdKda.col(2))).integrate().sum()*eadot;
  real term7nb_partm = -w36*en*endot*(sdudn.dot(sdKdb.col(2))).integrate().sum()*ebdot;
  real term7nn_partm = -w36*en*endot*(sdudn.dot(sdKdn.col(2))).integrate().sum()*endot;
  real term7nm_partm = -w36*en*endot*(sdudn.dot(sdKdm.col(2))).integrate().sum()*emdot;
  real term7ma_partm = -w36*en*emdot*(sdudm.dot(sdKda.col(2))).integrate().sum()*eadot;
  real term7mb_partm = -w36*en*emdot*(sdudm.dot(sdKdb.col(2))).integrate().sum()*ebdot;
  real term7mn_partm = -w36*en*emdot*(sdudm.dot(sdKdn.col(2))).integrate().sum()*endot;
  real term7mm_partm = -w36*en*emdot*(sdudm.dot(sdKdm.col(2))).integrate().sum()*emdot;
  
  Eigen::Vector4r term7;
  term7<<0., 0., term7aa_partn + term7ab_partn + term7an_partn + term7am_partn + term7ba_partn + term7bb_partn + term7bn_partn + term7bm_partn + term7na_partn + term7nb_partn + term7nn_partn + term7nm_partn + term7ma_partn + term7mb_partn + term7mn_partn + term7mm_partn,
  term7aa_partm + term7ab_partm + term7an_partm + term7am_partm + term7ba_partm + term7bb_partm + term7bn_partm + term7bm_partm + term7na_partm + term7nb_partm + term7nn_partm + term7nm_partm + term7ma_partm + term7mb_partm + term7mn_partm + term7mm_partm;
  
  Eigen::Vector4r term8; 
  term8 << 0., 0., w36*((dframeOrdt*sK*voneeta).dot(dposOrdt).integrate().sum()), 0.;
  Eigen::Vector4r term9; 
  term9 << 0., 0., w36*((dframeOrdt*su).dot(dframeOrdt*sK*voneeta).integrate().sum()), 0.;
  
  Eigen::Vector4r term10;
  term10 << (dframeOrdt*sdKda*voneeta).dot(dposOrdt).integrate().sum(), (dframeOrdt*sdKdb*voneeta).dot(dposOrdt).integrate().sum(),  (dframeOrdt*sdKdn*voneeta).dot(dposOrdt).integrate().sum(),  (dframeOrdt*sdKdm*voneeta).dot(dposOrdt).integrate().sum();
  term10*=w36*en;
  
  Eigen::Vector4r term11;
  term11 << (dframeOrdt*sK*voneeta).dot(dframeOrdt*sduda).integrate().sum(), (dframeOrdt*sK*voneeta).dot(dframeOrdt*sdudb).integrate().sum(),  (dframeOrdt*sK*voneeta).dot(dframeOrdt*sdudn).integrate().sum(),  (dframeOrdt*sK*voneeta).dot(dframeOrdt*sdudm).integrate().sum();
  term11*=w36*en;
  
  Eigen::Vector4r term12;
  term12 << (dframeOrdt*sdKda*voneeta).dot(dframeOrdt*su).integrate().sum(), (dframeOrdt*sdKdb*voneeta).dot(dframeOrdt*su).integrate().sum(),  (dframeOrdt*sdKdn*voneeta).dot(dframeOrdt*su).integrate().sum(),  (dframeOrdt*sdKdm*voneeta).dot(dframeOrdt*su).integrate().sum();
  term12*=w36*en;
  
  Eigen::Vector4r term13;
  term13 << 0., 0.,  ((dframeOrdt*sK.col(2)).dot(dposOrdt)*zeroOne).integrate().sum(),  (dframeOrdt*sK.col(2)).dot(dposOrdt).integrate().sum();
  term13*=w36*en;
  
  Eigen::Vector4r term14;
  term14 << 0., 0.,  ((dframeOrdt*sK.col(2)).dot(dframeOrdt*su)*zeroOne).integrate().sum(),  (dframeOrdt*sK.col(2)).dot(dframeOrdt*su).integrate().sum();
  term14*=w36*en;
  
  Eigen::Vector4r term15a;
  term15a << eadot*(sd2udada.dot(sdKds*voneeta)*setadot).integrate().sum(), eadot*(sd2udadb.dot(sdKds*voneeta)*setadot).integrate().sum(), eadot*(sd2udadn.dot(sdKds*voneeta)*setadot).integrate().sum(), eadot*(sd2udadm.dot(sdKds*voneeta)*setadot).integrate().sum();
  Eigen::Vector4r term15b;
  term15b << ebdot*(sd2udbda.dot(sdKds*voneeta)*setadot).integrate().sum(), ebdot*(sd2udbdb.dot(sdKds*voneeta)*setadot).integrate().sum(), ebdot*(sd2udbdn.dot(sdKds*voneeta)*setadot).integrate().sum(), ebdot*(sd2udbdm.dot(sdKds*voneeta)*setadot).integrate().sum();
  Eigen::Vector4r term15n;
  term15n << endot*(sd2udnda.dot(sdKds*voneeta)*setadot).integrate().sum(), endot*(sd2udndb.dot(sdKds*voneeta)*setadot).integrate().sum(), endot*(sd2udndn.dot(sdKds*voneeta)*setadot).integrate().sum(), endot*(sd2udndm.dot(sdKds*voneeta)*setadot).integrate().sum();
  Eigen::Vector4r term15m;
  term15m << emdot*(sd2udmda.dot(sdKds*voneeta)*setadot).integrate().sum(), emdot*(sd2udmdb.dot(sdKds*voneeta)*setadot).integrate().sum(), emdot*(sd2udmdn.dot(sdKds*voneeta)*setadot).integrate().sum(), emdot*(sd2udmdm.dot(sdKds*voneeta)*setadot).integrate().sum();
  Eigen::Vector4r term15=w36*(term15a+term15b+term15n+term15m);
  
  Eigen::Vector4r term16a;
  term16a << eadot*(sduda.dot(sd2Kdsda*voneeta)*setadot).integrate().sum(), eadot*(sduda.dot(sd2Kdsdb*voneeta)*setadot).integrate().sum(), eadot*(sduda.dot(sd2Kdsdn*voneeta)*setadot).integrate().sum(), eadot*(sduda.dot(sd2Kdsdm*voneeta)*setadot).integrate().sum();
  Eigen::Vector4r term16b;
  term16b << ebdot*(sdudb.dot(sd2Kdsda*voneeta)*setadot).integrate().sum(), ebdot*(sdudb.dot(sd2Kdsdb*voneeta)*setadot).integrate().sum(), ebdot*(sdudb.dot(sd2Kdsdn*voneeta)*setadot).integrate().sum(), ebdot*(sdudb.dot(sd2Kdsdm*voneeta)*setadot).integrate().sum();
  Eigen::Vector4r term16n;
  term16n << endot*(sdudn.dot(sd2Kdsda*voneeta)*setadot).integrate().sum(), endot*(sdudn.dot(sd2Kdsdb*voneeta)*setadot).integrate().sum(), endot*(sdudn.dot(sd2Kdsdn*voneeta)*setadot).integrate().sum(), endot*(sdudn.dot(sd2Kdsdm*voneeta)*setadot).integrate().sum();
  Eigen::Vector4r term16m;
  term16m << emdot*(sdudm.dot(sd2Kdsda*voneeta)*setadot).integrate().sum(), emdot*(sdudm.dot(sd2Kdsdb*voneeta)*setadot).integrate().sum(), emdot*(sdudm.dot(sd2Kdsdn*voneeta)*setadot).integrate().sum(), emdot*(sdudm.dot(sd2Kdsdm*voneeta)*setadot).integrate().sum();
  Eigen::Vector4r term16=w36*(term16a+term16b+term16n+term16m);
  
  real term17a_partn = eadot*(sduda.dot(sdKds.col(2))*(setadot*zeroOne)).integrate().sum();
  real term17b_partn = ebdot*(sdudb.dot(sdKds.col(2))*(setadot*zeroOne)).integrate().sum();
  real term17n_partn = endot*(sdudn.dot(sdKds.col(2))*(setadot*zeroOne)).integrate().sum();
  real term17m_partn = emdot*(sdudm.dot(sdKds.col(2))*(setadot*zeroOne)).integrate().sum();
  real term17a_partm = eadot*(sduda.dot(sdKds.col(2))*setadot).integrate().sum();
  real term17b_partm = ebdot*(sdudb.dot(sdKds.col(2))*setadot).integrate().sum();
  real term17n_partm = endot*(sdudn.dot(sdKds.col(2))*setadot).integrate().sum();
  real term17m_partm = emdot*(sdudm.dot(sdKds.col(2))*setadot).integrate().sum();
  Eigen::Vector4r term17;
  term17 << 0., 0., w36*(term17a_partn+term17b_partn+term17n_partn+term17m_partn), w36*(term17a_partm+term17b_partm+term17n_partm+term17m_partm);
  
  Eigen::Vector4r term18aa; 
  term18aa << (sd2Kdada*voneeta).dot(sdKda*voneeta).integrate().sum(), (sd2Kdadb*voneeta).dot(sdKda*voneeta).integrate().sum(), (sd2Kdadn*voneeta).dot(sdKda*voneeta).integrate().sum(), (sd2Kdadm*voneeta).dot(sdKda*voneeta).integrate().sum();
  term18aa*=-w36*eadot*eadot;
  Eigen::Vector4r term18ab; 
  term18ab << (sd2Kdada*voneeta).dot(sdKdb*voneeta).integrate().sum(), (sd2Kdadb*voneeta).dot(sdKdb*voneeta).integrate().sum(), (sd2Kdadn*voneeta).dot(sdKdb*voneeta).integrate().sum(), (sd2Kdadm*voneeta).dot(sdKdb*voneeta).integrate().sum();
  term18ab*=-w36*eadot*ebdot;
  Eigen::Vector4r term18an; 
  term18an << (sd2Kdada*voneeta).dot(sdKdn*voneeta).integrate().sum(), (sd2Kdadb*voneeta).dot(sdKdn*voneeta).integrate().sum(), (sd2Kdadn*voneeta).dot(sdKdn*voneeta).integrate().sum(), (sd2Kdadm*voneeta).dot(sdKdn*voneeta).integrate().sum();
  term18an*=-w36*eadot*endot;
  Eigen::Vector4r term18am; 
  term18am << (sd2Kdada*voneeta).dot(sdKdm*voneeta).integrate().sum(), (sd2Kdadb*voneeta).dot(sdKdm*voneeta).integrate().sum(), (sd2Kdadn*voneeta).dot(sdKdm*voneeta).integrate().sum(), (sd2Kdadm*voneeta).dot(sdKdm*voneeta).integrate().sum();
  term18am*=-w36*eadot*emdot;
  
  Eigen::Vector4r term18ba; 
  term18ba << (sd2Kdbda*voneeta).dot(sdKda*voneeta).integrate().sum(), (sd2Kdbdb*voneeta).dot(sdKda*voneeta).integrate().sum(), (sd2Kdbdn*voneeta).dot(sdKda*voneeta).integrate().sum(), (sd2Kdbdm*voneeta).dot(sdKda*voneeta).integrate().sum();
  term18ba*=-w36*ebdot*eadot;
  Eigen::Vector4r term18bb; 
  term18bb << (sd2Kdbda*voneeta).dot(sdKdb*voneeta).integrate().sum(), (sd2Kdbdb*voneeta).dot(sdKdb*voneeta).integrate().sum(), (sd2Kdbdn*voneeta).dot(sdKdb*voneeta).integrate().sum(), (sd2Kdbdm*voneeta).dot(sdKdb*voneeta).integrate().sum();
  term18bb*=-w36*ebdot*ebdot;
  Eigen::Vector4r term18bn; 
  term18bn << (sd2Kdbda*voneeta).dot(sdKdn*voneeta).integrate().sum(), (sd2Kdbdb*voneeta).dot(sdKdn*voneeta).integrate().sum(), (sd2Kdbdn*voneeta).dot(sdKdn*voneeta).integrate().sum(), (sd2Kdbdm*voneeta).dot(sdKdn*voneeta).integrate().sum();
  term18bn*=-w36*ebdot*endot;
  Eigen::Vector4r term18bm; 
  term18bm << (sd2Kdbda*voneeta).dot(sdKdm*voneeta).integrate().sum(), (sd2Kdbdb*voneeta).dot(sdKdm*voneeta).integrate().sum(), (sd2Kdbdn*voneeta).dot(sdKdm*voneeta).integrate().sum(), (sd2Kdbdm*voneeta).dot(sdKdm*voneeta).integrate().sum();
  term18bm*=-w36*ebdot*emdot;
  
  Eigen::Vector4r term18na; 
  term18na << (sd2Kdnda*voneeta).dot(sdKda*voneeta).integrate().sum(), (sd2Kdndb*voneeta).dot(sdKda*voneeta).integrate().sum(), (sd2Kdndn*voneeta).dot(sdKda*voneeta).integrate().sum(), (sd2Kdndm*voneeta).dot(sdKda*voneeta).integrate().sum();
  term18na*=-w36*endot*eadot;
  Eigen::Vector4r term18nb; 
  term18nb << (sd2Kdnda*voneeta).dot(sdKdb*voneeta).integrate().sum(), (sd2Kdndb*voneeta).dot(sdKdb*voneeta).integrate().sum(), (sd2Kdndn*voneeta).dot(sdKdb*voneeta).integrate().sum(), (sd2Kdndm*voneeta).dot(sdKdb*voneeta).integrate().sum();
  term18nb*=-w36*endot*ebdot;
  Eigen::Vector4r term18nn; 
  term18nn << (sd2Kdnda*voneeta).dot(sdKdn*voneeta).integrate().sum(), (sd2Kdndb*voneeta).dot(sdKdn*voneeta).integrate().sum(), (sd2Kdndn*voneeta).dot(sdKdn*voneeta).integrate().sum(), (sd2Kdndm*voneeta).dot(sdKdn*voneeta).integrate().sum();
  term18nn*=-w36*endot*endot;
  Eigen::Vector4r term18nm; 
  term18nm << (sd2Kdnda*voneeta).dot(sdKdm*voneeta).integrate().sum(), (sd2Kdndb*voneeta).dot(sdKdm*voneeta).integrate().sum(), (sd2Kdndn*voneeta).dot(sdKdm*voneeta).integrate().sum(), (sd2Kdndm*voneeta).dot(sdKdm*voneeta).integrate().sum();
  term18nm*=-w36*endot*emdot;
  
  Eigen::Vector4r term18ma; 
  term18ma << (sd2Kdmda*voneeta).dot(sdKda*voneeta).integrate().sum(), (sd2Kdmdb*voneeta).dot(sdKda*voneeta).integrate().sum(), (sd2Kdmdn*voneeta).dot(sdKda*voneeta).integrate().sum(), (sd2Kdmdm*voneeta).dot(sdKda*voneeta).integrate().sum();
  term18ma*=-w36*emdot*eadot;
  Eigen::Vector4r term18mb; 
  term18mb << (sd2Kdmda*voneeta).dot(sdKdb*voneeta).integrate().sum(), (sd2Kdmdb*voneeta).dot(sdKdb*voneeta).integrate().sum(), (sd2Kdmdn*voneeta).dot(sdKdb*voneeta).integrate().sum(), (sd2Kdmdm*voneeta).dot(sdKdb*voneeta).integrate().sum();
  term18mb*=-w36*emdot*ebdot;
  Eigen::Vector4r term18mn; 
  term18mn << (sd2Kdmda*voneeta).dot(sdKdn*voneeta).integrate().sum(), (sd2Kdmdb*voneeta).dot(sdKdn*voneeta).integrate().sum(), (sd2Kdmdn*voneeta).dot(sdKdn*voneeta).integrate().sum(), (sd2Kdmdm*voneeta).dot(sdKdn*voneeta).integrate().sum();
  term18mn*=-w36*emdot*endot;
  Eigen::Vector4r term18mm; 
  term18mm << (sd2Kdmda*voneeta).dot(sdKdm*voneeta).integrate().sum(), (sd2Kdmdb*voneeta).dot(sdKdm*voneeta).integrate().sum(), (sd2Kdmdn*voneeta).dot(sdKdm*voneeta).integrate().sum(), (sd2Kdmdm*voneeta).dot(sdKdm*voneeta).integrate().sum();
  term18mm*=-w36*emdot*emdot;
  
  Eigen::Vector4r term18 = term18aa + term18ab + term18an + term18am + term18ba + term18bb + term18bn + term18bm
                         + term18na + term18nb + term18nn + term18nm + term18ma + term18mb + term18mn + term18mm;
  
  Eigen::Vector4r term19;
  term19 << (dframeOrdt*sdKda*voneeta).dot(dframeOrdt*sK*voneeta).integrate().sum(),
            (dframeOrdt*sdKdb*voneeta).dot(dframeOrdt*sK*voneeta).integrate().sum(),
            (dframeOrdt*sdKdn*voneeta).dot(dframeOrdt*sK*voneeta).integrate().sum(),
            (dframeOrdt*sdKdm*voneeta).dot(dframeOrdt*sK*voneeta).integrate().sum();
                         
  term19*=w36;
  
  real mw540=-(w5/40.);
  
  Eigen::Vector4r term20; 
  term20 << ((sd2Kdsda*voneeta).dot(sdKds*voneeta)*seta*seta).integrate().sum(), ((sd2Kdsdb*voneeta).dot(sdKds*voneeta)*seta*seta).integrate().sum(), ((sd2Kdsdn*voneeta).dot(sdKds*voneeta)*seta*seta).integrate().sum(), ((sd2Kdsdm*voneeta).dot(sdKds*voneeta)*seta*seta).integrate().sum();
  term20*=mw540;
  
  real term21aa_partn = -w36*eadot*(sdKda.col(2).dot(sdKda*voneeta)*zeroOne).integrate().sum()*eadot;
  real term21aa_partm = -w36*eadot*sdKda.col(2).dot(sdKda*voneeta).integrate().sum()*eadot;
  real term21ab_partn = -w36*eadot*(sdKda.col(2).dot(sdKdb*voneeta)*zeroOne).integrate().sum()*ebdot;
  real term21ab_partm = -w36*eadot*sdKda.col(2).dot(sdKdb*voneeta).integrate().sum()*ebdot;
  real term21an_partn = -w36*eadot*(sdKda.col(2).dot(sdKdn*voneeta)*zeroOne).integrate().sum()*endot;
  real term21an_partm = -w36*eadot*sdKda.col(2).dot(sdKdn*voneeta).integrate().sum()*endot;
  real term21am_partn = -w36*eadot*(sdKda.col(2).dot(sdKdm*voneeta)*zeroOne).integrate().sum()*emdot;
  real term21am_partm = -w36*eadot*sdKda.col(2).dot(sdKdm*voneeta).integrate().sum()*emdot;
  
  real term21ba_partn = -w36*ebdot*(sdKdb.col(2).dot(sdKda*voneeta)*zeroOne).integrate().sum()*eadot;
  real term21ba_partm = -w36*ebdot*sdKdb.col(2).dot(sdKda*voneeta).integrate().sum()*eadot;
  real term21bb_partn = -w36*ebdot*(sdKdb.col(2).dot(sdKdb*voneeta)*zeroOne).integrate().sum()*ebdot;
  real term21bb_partm = -w36*ebdot*sdKdb.col(2).dot(sdKdb*voneeta).integrate().sum()*ebdot;
  real term21bn_partn = -w36*ebdot*(sdKdb.col(2).dot(sdKdn*voneeta)*zeroOne).integrate().sum()*endot;
  real term21bn_partm = -w36*ebdot*sdKdb.col(2).dot(sdKdn*voneeta).integrate().sum()*endot;
  real term21bm_partn = -w36*ebdot*(sdKdb.col(2).dot(sdKdm*voneeta)*zeroOne).integrate().sum()*emdot;
  real term21bm_partm = -w36*ebdot*sdKdb.col(2).dot(sdKdm*voneeta).integrate().sum()*emdot;
  
  real term21na_partn = -w36*endot*(sdKdn.col(2).dot(sdKda*voneeta)*zeroOne).integrate().sum()*eadot;
  real term21na_partm = -w36*endot*sdKdn.col(2).dot(sdKda*voneeta).integrate().sum()*eadot;
  real term21nb_partn = -w36*endot*(sdKdn.col(2).dot(sdKdb*voneeta)*zeroOne).integrate().sum()*ebdot;
  real term21nb_partm = -w36*endot*sdKdn.col(2).dot(sdKdb*voneeta).integrate().sum()*ebdot;
  real term21nn_partn = -w36*endot*(sdKdn.col(2).dot(sdKdn*voneeta)*zeroOne).integrate().sum()*endot;
  real term21nn_partm = -w36*endot*sdKdn.col(2).dot(sdKdn*voneeta).integrate().sum()*endot;
  real term21nm_partn = -w36*endot*(sdKdn.col(2).dot(sdKdm*voneeta)*zeroOne).integrate().sum()*emdot;
  real term21nm_partm = -w36*endot*sdKdn.col(2).dot(sdKdm*voneeta).integrate().sum()*emdot;
  
  real term21ma_partn = -w36*emdot*(sdKdm.col(2).dot(sdKda*voneeta)*zeroOne).integrate().sum()*eadot;
  real term21ma_partm = -w36*emdot*sdKdm.col(2).dot(sdKda*voneeta).integrate().sum()*eadot;
  real term21mb_partn = -w36*emdot*(sdKdm.col(2).dot(sdKdb*voneeta)*zeroOne).integrate().sum()*ebdot;
  real term21mb_partm = -w36*emdot*sdKdm.col(2).dot(sdKdb*voneeta).integrate().sum()*ebdot;
  real term21mn_partn = -w36*emdot*(sdKdm.col(2).dot(sdKdn*voneeta)*zeroOne).integrate().sum()*endot;
  real term21mn_partm = -w36*emdot*sdKdm.col(2).dot(sdKdn*voneeta).integrate().sum()*endot;
  real term21mm_partn = -w36*emdot*(sdKdm.col(2).dot(sdKdm*voneeta)*zeroOne).integrate().sum()*emdot;
  real term21mm_partm = -w36*emdot*sdKdm.col(2).dot(sdKdm*voneeta).integrate().sum()*emdot;
  
  Eigen::Vector4r term21;
  term21 << 0., 0., term21aa_partn + term21ab_partn + term21an_partn + term21am_partn + term21ba_partn + term21bb_partn + term21bn_partn + term21bm_partn + term21na_partn + term21nb_partn + term21nn_partn + term21nm_partn + term21ma_partn + term21mb_partn + term21mn_partn + term21mm_partn, term21aa_partm + term21ab_partm + term21an_partm + term21am_partm + term21ba_partm + term21bb_partm + term21bn_partm + term21bm_partm + term21na_partm + term21nb_partm + term21nn_partm + term21nm_partm + term21ma_partm + term21mb_partm + term21mn_partm + term21mm_partm;
  
  Eigen::Vector4r term22;
  term22 << 0., 0., w36*((dframeOrdt*sK.col(2)).dot(dframeOrdt*sK*voneeta)*zeroOne).integrate().sum(), w36*(dframeOrdt*sK.col(2)).dot(dframeOrdt*sK*voneeta).integrate().sum();
  
  Eigen::Vector4r term23;
  term23 << 0., 0., mw540*((sdKds.col(2)).dot(sdKds*voneeta)*zeroOne*seta*seta).integrate().sum(), mw540*((sdKds.col(2)).dot(sdKds*voneeta)*seta*seta).integrate().sum();
  
  return (term1+term2+term3+term4+term5+term6+term7+term8+term9+term10
         +term11+term12+term13+term14+term15+term16+term17+term18+term19+term20
         +term21+term22+term23
  )/2.;
}

