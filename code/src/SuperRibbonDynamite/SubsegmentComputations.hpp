/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef SUBSEGMENT_COMPUTATIONS_HPP
#define SUBSEGMENT_COMPUTATIONS_HPP

#include <Eigen/Dense>
#include <vector>
#include <utility>
#include <iostream>
#include <map>
#include <BaseCore/LinearScalar.hpp>
#include <BaseCore/real.hpp>
#include <Serie.hpp>



class SubSegmentComputations
{
public:
  void Place(Eigen::Vector3r pos, Eigen::Matrix3r frame, Eigen::Vector3r dposOrdt, Eigen::Matrix3r dframeOrdt);
  void Setup(LinearScalar omega1, LinearScalar eta, LinearScalar omega1Dot, LinearScalar etaDot);
  void SetRibbonWidth(real w);

  /**
   * \brief Upper bound of the rest of the serie at order \c order
   */
  real estimRemainderOfTheSerie(int order, real s = 1.);

  /**
   * \brief Upper bound on the order of the serie to get a reasonnable precision
   */
  int estimNeededOrder();

  /**
   * \brief Upper bound on the maximal abscissa to use to avoid computation issues with the serie
   */
  real estimSMax();


  Eigen::Matrix3r Frame(real s);
  Eigen::Matrix3r PreFrame(real s);//Frame = FrameOr*PreFrame
  Eigen::Vector3r Pos(real s);
  Eigen::Vector3r PreTranslation(real s);//Pos = PosOr+FrameOr*PreTranslation

  std::pair<Eigen::Vector3r, Eigen::Matrix3r> PosFrame(real s);

  Eigen::Vector3r preAltitudeEnergy(real s);
  Eigen::Vector3r preAltitudeRotationalEnergy(real s);//preAltitudeEnergy=surface*pos+frameOr*preAltitudeRotationalEnergy
  Eigen::Matrix<real, 3, 4> gradPreAltitudeEnergy(real s);
  Eigen::Matrix<Eigen::Vector3r, 3, 1> dPreAltitudeEnergydPos(real s);
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPreAltitudeEnergydFrame(real s);
  
  Eigen::Matrix<Eigen::Matrix4r, 3, 1> hessianPreAltitudeEnergy(real s);
  Eigen::Matrix<Eigen::Matrix<Eigen::Vector4r, 3, 3>, 3, 1> d2PreAltitudeEnergydFramedabnm(real s);

  Eigen::Matrix<Eigen::Matrix3r, 3, 3> dFramedFrame(real s);
  Eigen::Matrix<Eigen::Matrix3r, 3, 1> dPosdFrame(real s);

  Eigen::Matrix3r dFrameda(real s);
  Eigen::Matrix3r dFramedb(real s);
  Eigen::Matrix3r dFramedn(real s);
  Eigen::Matrix3r dFramedm(real s);
  Eigen::Vector3r dPosda(real s);
  Eigen::Vector3r dPosdb(real s);
  Eigen::Vector3r dPosdn(real s);
  Eigen::Vector3r dPosdm(real s);
  
  Eigen::Matrix<Eigen::Matrix4r, 3, 3> hessianFrame(real s);
  Eigen::Matrix<Eigen::Matrix4r, 3, 1> hessianPos(real s);
  Eigen::Matrix<Eigen::Matrix<Eigen::Vector4r, 3, 3>, 3, 3> d2FramedFramedabnm(real s);
  Eigen::Matrix<Eigen::Matrix<Eigen::Vector4r, 3, 3>, 3, 1> d2PosdFramedabnm(real s);

  Eigen::Matrix<Eigen::Vector3r, 3, 1> dPosdPos(real s);
  void computeSeriePreTerms(int order);
  
  real unscaledKineticEnergy(real s);//multiply by areaDensity to get Ec
  Eigen::Matrix4r unscaledd2KineticEnergydqdot2(real s);//multiply by areaDensity to get Ec
  Eigen::Vector4r unscaledLangrangianKineticB(real s);//multiply by areaDensity to get Ec
private:
  std::map<std::string, bool> called;
  void computeSeriePreTerms();
  void computeDerivationsPreTerms();
  void computePreAltitudeEnergySerieDerivations();
  void computePreAltitudeEnergyPreTerms();
  void computePreAltitudeEnergyPreTermsDerivations();
  void computePreAltitudeEnergyPreTermsDerivationsSecondOrder();
  
  void computeDerivationsPreTermsSecondOrder();

  real C(real s);
  Eigen::Matrix3r skewOf(Eigen::Vector3r);
  Eigen::Vector3r posOr;
  Eigen::Matrix3r frameOr;
  Eigen::Vector3r dposOrdt;
  Eigen::Matrix3r dframeOrdt;
  Eigen::Matrix3r lambda0Sq;
  Eigen::Matrix3r lambda1Sq;
  Eigen::Matrix3r lambda2Sq;
  Eigen::Vector3r lambda0;
  Eigen::Vector3r lambda1;
  Eigen::Vector3r lambda2;
  real lambda;

  LinearScalar omega1;                                      // (A, B)
  LinearScalar eta;                                         // (N, M)
  LinearScalar omega1Dot;                                   // (A, B)
  LinearScalar etaDot;                                      // (N, M)
  real ribbonWidth;

  std::vector<Eigen::Matrix3r> FramePreTerms;
  std::vector<Eigen::Vector3r> PosPreTerms;

  // Derivations

  Eigen::Vector3r dLambda0db;
  Eigen::Vector3r dLambda0dm;
  Eigen::Vector3r dLambda1da;
  Eigen::Vector3r dLambda1db;
  Eigen::Vector3r dLambda1dn;
  Eigen::Vector3r dLambda1dm;
  Eigen::Vector3r dLambda2da;
  Eigen::Vector3r dLambda2dn;
  Eigen::Matrix3r dLambda0dbSq;
  Eigen::Matrix3r dLambda0dmSq;
  Eigen::Matrix3r dLambda1daSq;
  Eigen::Matrix3r dLambda1dbSq;
  Eigen::Matrix3r dLambda1dnSq;
  Eigen::Matrix3r dLambda1dmSq;
  Eigen::Matrix3r dLambda2daSq;
  Eigen::Matrix3r dLambda2dnSq;

  std::vector<Eigen::Matrix3r> dFramePreTermsda;
  std::vector<Eigen::Matrix3r> dFramePreTermsdb;
  std::vector<Eigen::Matrix3r> dFramePreTermsdn;
  std::vector<Eigen::Matrix3r> dFramePreTermsdm;
  std::vector<Eigen::Vector3r> dPosPreTermsda;
  std::vector<Eigen::Vector3r> dPosPreTermsdb;
  std::vector<Eigen::Vector3r> dPosPreTermsdn;
  std::vector<Eigen::Vector3r> dPosPreTermsdm;

  std::vector<Eigen::Vector3r> preAltitudeEnergyPreTerms;
  Eigen::Vector3r preAltitudeEnergyLinearTerm;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermda;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdb;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdn;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdm;
  
  //Second order derivations
  
  Eigen::Vector3r dLambda0dbm;
  Eigen::Vector3r dLambda0dmb;
  Eigen::Vector3r dLambda1dam;
  Eigen::Vector3r dLambda1dbn;
  Eigen::Vector3r dLambda1dnb;
  Eigen::Vector3r dLambda1dma;
  Eigen::Vector3r dLambda2dan;
  Eigen::Vector3r dLambda2dna;
  Eigen::Matrix3r dLambda0dbmSq;
  Eigen::Matrix3r dLambda0dmbSq;
  Eigen::Matrix3r dLambda1damSq;
  Eigen::Matrix3r dLambda1dbnSq;
  Eigen::Matrix3r dLambda1dnbSq;
  Eigen::Matrix3r dLambda1dmaSq;
  Eigen::Matrix3r dLambda2danSq;
  Eigen::Matrix3r dLambda2dnaSq;
  
  std::vector<Eigen::Matrix3r> dFramePreTermsdaa;
  std::vector<Eigen::Matrix3r> dFramePreTermsdba;
  std::vector<Eigen::Matrix3r> dFramePreTermsdna;
  std::vector<Eigen::Matrix3r> dFramePreTermsdma;
  std::vector<Eigen::Matrix3r> dFramePreTermsdab;
  std::vector<Eigen::Matrix3r> dFramePreTermsdbb;
  std::vector<Eigen::Matrix3r> dFramePreTermsdnb;
  std::vector<Eigen::Matrix3r> dFramePreTermsdmb;
  std::vector<Eigen::Matrix3r> dFramePreTermsdan;
  std::vector<Eigen::Matrix3r> dFramePreTermsdbn;
  std::vector<Eigen::Matrix3r> dFramePreTermsdnn;
  std::vector<Eigen::Matrix3r> dFramePreTermsdmn;
  std::vector<Eigen::Matrix3r> dFramePreTermsdam;
  std::vector<Eigen::Matrix3r> dFramePreTermsdbm;
  std::vector<Eigen::Matrix3r> dFramePreTermsdnm;
  std::vector<Eigen::Matrix3r> dFramePreTermsdmm;
  std::vector<Eigen::Vector3r> dPosPreTermsdaa;
  std::vector<Eigen::Vector3r> dPosPreTermsdba;
  std::vector<Eigen::Vector3r> dPosPreTermsdna;
  std::vector<Eigen::Vector3r> dPosPreTermsdma;
  std::vector<Eigen::Vector3r> dPosPreTermsdab;
  std::vector<Eigen::Vector3r> dPosPreTermsdbb;
  std::vector<Eigen::Vector3r> dPosPreTermsdnb;
  std::vector<Eigen::Vector3r> dPosPreTermsdmb;
  std::vector<Eigen::Vector3r> dPosPreTermsdan;
  std::vector<Eigen::Vector3r> dPosPreTermsdbn;
  std::vector<Eigen::Vector3r> dPosPreTermsdnn;
  std::vector<Eigen::Vector3r> dPosPreTermsdmn;
  std::vector<Eigen::Vector3r> dPosPreTermsdam;
  std::vector<Eigen::Vector3r> dPosPreTermsdbm;
  std::vector<Eigen::Vector3r> dPosPreTermsdnm;
  std::vector<Eigen::Vector3r> dPosPreTermsdmm;
  
  
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdaa;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdba;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdna;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdma;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdab;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdbb;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdnb;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdmb;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdan;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdbn;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdnn;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdmn;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdam;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdbm;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdnm;
  std::vector<Eigen::Vector3r> dPreAltitudeEnergyPreTermdmm;
  
  std::vector<real> spowers;
  real sPowersCurrentS = 0.;
  void computeSPowers(real s);
};

#endif // SUBSEGMENT_COMPUTATIONS_HPP

