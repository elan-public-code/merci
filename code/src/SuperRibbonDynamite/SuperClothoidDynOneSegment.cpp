/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "SuperClothoidDynOneSegment.hpp"
#include "FunctionSegmentSuperClothoid.hpp"
#include <iomanip>

SuperClothoidDynOneSegment::SuperClothoidDynOneSegment()
{
  sysPt.setZero();
  sysPtdot.setZero();
  R.setIdentity();
  r.setZero();
  length=1.;
  width=0.001;
  thickness=0.001;
  Y=1.e7;
  areaDensity=1e3;
  nu=0.4;
  virtuaDamping=0.;
  useRK4=true;
  dt=1e-3;
  G<<0,-10,0;
  curvNat.setZero();
}


void SuperClothoidDynOneSegment::set(SuperClothoidDynOneSegment::PtV pt, SuperClothoidDynOneSegment::PtV speed)
{
  cached = false;
  sysPt = pt;
  sysPtdot = speed;
}

SuperClothoidDynOneSegment::PtV SuperClothoidDynOneSegment::getPoint() const
{
  return sysPt;
}

SuperClothoidDynOneSegment::PtV SuperClothoidDynOneSegment::getPointSpeed() const
{
  return sysPtdot;
}

void SuperClothoidDynOneSegment::print(std::ostream& os) const
{
  os << std::left << std::setw(6) << std::setfill(' ') << " "<<'|';
  os << std::left << std::setw(11) << std::setfill(' ') << " w1A"<<'|';
  os << std::left << std::setw(11) << std::setfill(' ') << " w1B"<<'|';
  os << std::left << std::setw(11) << std::setfill(' ') << " w2A"<<'|';
  os << std::left << std::setw(11) << std::setfill(' ') << " w2B"<<'|';
  os << std::left << std::setw(11) << std::setfill(' ') << " w3A"<<'|';
  os << std::left << std::setw(11) << std::setfill(' ') << " w3B"<<'|';
  os << std::endl;
  os << std::left << std::setw(79) << std::setfill('=') << '='<<std::endl;
  os << std::left << std::setw(6) << std::setfill(' ') << "val."<<"| ";
  os << std::left << std::setw(10) << std::setfill(' ') << sysPt[0] <<"| ";
  os << std::left << std::setw(10) << std::setfill(' ') << sysPt[1] <<"| ";
  os << std::left << std::setw(10) << std::setfill(' ') << sysPt[2] <<"| ";
  os << std::left << std::setw(10) << std::setfill(' ') << sysPt[3] <<"| ";
  os << std::left << std::setw(10) << std::setfill(' ') << sysPt[4] <<"| ";
  os << std::left << std::setw(10) << std::setfill(' ') << sysPt[5] <<"| ";
  os << std::endl;
  os << std::left << std::setw(6) << std::setfill(' ') << "dot."<<"| ";
  os << std::left << std::setw(10) << std::setfill(' ') << sysPtdot[0] <<"| ";
  os << std::left << std::setw(10) << std::setfill(' ') << sysPtdot[1] <<"| ";
  os << std::left << std::setw(10) << std::setfill(' ') << sysPtdot[2] <<"| ";
  os << std::left << std::setw(10) << std::setfill(' ') << sysPtdot[3] <<"| ";
  os << std::left << std::setw(10) << std::setfill(' ') << sysPtdot[4] <<"| ";
  os << std::left << std::setw(10) << std::setfill(' ') << sysPtdot[5] <<"| ";
  os << std::endl;
}

std::ostream & SuperClothoidDynOneSegment::operator<<(std::ostream& os) const
{
  print(os);
  return os;
}


void SuperClothoidDynOneSegment::setupGeo(Eigen::Vector3d pos, Eigen::Matrix3d frame, double width, double thickness, double ribbonLength, SuperClothoidDynOneSegment::PtV curvNat)
{
  cached=false;
  r=pos;
  R=frame;
  this->width=width;
  this->thickness=thickness;
  length=ribbonLength;
  this->curvNat=curvNat;
}

void SuperClothoidDynOneSegment::setupPhy(Eigen::Vector3d gravity, double Y, double areaDensity, double nu)
{
  cached=false;
  G=gravity;
  this->Y=Y;
  this->areaDensity=areaDensity;
  this->nu=nu;
}

void SuperClothoidDynOneSegment::setupTimeDynamic(double dt, bool useRK4, double virtuaDamping)
{
  cached=false;
  this->dt=dt;
  this->useRK4=useRK4;
  this->virtuaDamping=virtuaDamping;
}

// SuperClothoidDynOneSegment::PtV SuperClothoidDynOneSegment::computeAcc()
// {
//   if(cached)return acc;
//   FunctionSegmentSuperClothoid fun;
//   fun.G = G;
//   fun.Y = Y;
//   fun.nu = nu;
//   fun.thickness = thickness;
//   fun.areaDensity = areaDensity;
//   fun.width = width;
// 
//  //In arguments
//   fun.q = sysPt;
//   fun.qdot = sysPtdot;
//   fun.qnat = curvNat;
//   fun.length = length;
//   
//  //In functions
//   fun.R = R;
//   fun.d1Rdq.setConstant(Eigen::Matrix3d::Zero());
//   fun.d1Rdt.setZero();
//   fun.d2Rdtdq.setConstant(Eigen::Matrix3d::Zero()) ;
//   fun.d2Rdtdqdot.setConstant(Eigen::Matrix3d::Zero());
//   fun.d2Rdqdq.setConstant(Eigen::Matrix3d::Zero());
//   
//   fun.d1rdq.setConstant(Eigen::Vector3d::Zero());
//   fun.d2rdtdq.setConstant(Eigen::Vector3d::Zero());
//   fun.d2rdtdqdot.setConstant(Eigen::Vector3d::Zero());
//   fun.d2rdqdq.setConstant(Eigen::Vector3d::Zero());
//   fun.d1rdt.setZero();
//   fun.r = r;
//   fun();
//   Eigen::FullPivHouseholderQR<Eigen::Matrix6r> solver(fun.d2Ecdqdot2);
//   solver.setThreshold(1.e-16);
//   std::cout<<" =========== Solving =========== "<<std::endl;;
//   std::cout<<fun.d2Ecdqdot2<<std::endl<<std::endl;
//   std::cout<<fun.sysB.transpose()<<std::endl<<std::endl;
//   Eigen::SelfAdjointEigenSolver<Eigen::Matrix6r> eigensolver(fun.d2Ecdqdot2);
//   std::cout<<"eigenvalues: "<<eigensolver.eigenvalues().transpose()<<std::endl<<std::endl;
//   
//   std::cout<<"eigenvectors "<<std::endl<<eigensolver.eigenvectors()<<std::endl;
//   Eigen::Vector6r acceleration = solver.solve(fun.sysB);
//   std::cout<<"acceleration = "<<acceleration.transpose()<<std::endl;
//   this->acc=acceleration;
//   this->E=fun.Eg+fun.Ew;
//   cached = true;
//   return acceleration;
// }

SuperClothoidDynOneSegment::PtV SuperClothoidDynOneSegment::computeAccImpl(double dt)
{
  if(cached)return acc;
  FunctionSegmentSuperClothoid fun;
  fun.G = G;
  fun.Y = Y;
  fun.nu = nu;
  fun.thickness = thickness;
  fun.areaDensity = areaDensity;
  fun.width = width;

 //In arguments
  fun.q = sysPt;
  fun.qdot = sysPtdot;
  fun.qnat = curvNat;
  fun.length = length;
  
 //In functions
  fun.R = R;
  fun.d1Rdq.setConstant(Eigen::Matrix3d::Zero());
  fun.d1Rdt.setZero();
  fun.d2Rdtdq.setConstant(Eigen::Matrix3d::Zero()) ;
  fun.d2Rdtdqdot.setConstant(Eigen::Matrix3d::Zero());
  fun.d2Rdqdq.setConstant(Eigen::Matrix3d::Zero());
  
  fun.d1rdq.setConstant(Eigen::Vector3d::Zero());
  fun.d2rdtdq.setConstant(Eigen::Vector3d::Zero());
  fun.d2rdtdqdot.setConstant(Eigen::Vector3d::Zero());
  fun.d2rdqdq.setConstant(Eigen::Vector3d::Zero());
  fun.d1rdt.setZero();
  fun.r = r;
  fun();
  
  Eigen::Matrix6r matA=fun.d2Ecdqdot2+dt*(dt+virtuaDamping)*fun.hessEw;//(fun.hessEg+fun.hessEw);
  Eigen::FullPivHouseholderQR<Eigen::Matrix6r> solver(matA);
  solver.setThreshold(1.e-16);
  std::cout<<" =========== Solving =========== "<<std::endl;
  std::cout<<matA<<std::endl<<std::endl;
  //std::cout<<"Ew:"<<std::endl<<fun.hessEw<<std::endl<<std::endl;
  std::cout<<fun.sysB.transpose()<<std::endl<<std::endl;
  Eigen::SelfAdjointEigenSolver<Eigen::Matrix6r> eigensolver(matA);
  std::cout<<"eigenvalues: "<<eigensolver.eigenvalues().transpose()<<std::endl<<std::endl;
  
  std::cout<<"eigenvectors "<<std::endl<<eigensolver.eigenvectors()<<std::endl;
  Eigen::Vector6r acceleration = solver.solve(fun.sysB);
  std::cout<<"acceleration = "<<acceleration.transpose()<<std::endl;
  this->acc=acceleration;
  this->E=fun.Eg+fun.Ew;
  cached = true;
  return acceleration;
}


double SuperClothoidDynOneSegment::computeE()
{
  if(cached)return E;
  computeAccImpl(dt);
  return E;
}

void SuperClothoidDynOneSegment::DummyStep()
{
  auto& os=std::cout;
  //PtV acc = computeAcc();
  PtV acc = computeAccImpl(dt);
//   os << "Point 0"<<std::endl;
//   print(os);
  this->set(sysPt+dt*sysPtdot,sysPtdot+dt*acc);
//   os << "Point 1"<<std::endl;
//     print(os);
}

void SuperClothoidDynOneSegment::RK4()
{
  //std::ofstream os;
  auto& os=std::cout;
  Eigen::VectorXd k1, k2, k3, k4;
  double dt2=dt*dt;
  PtV yn=sysPt;
  PtV yndot=sysPtdot;
  os << "RK4"<<std::endl;
  {
    os << "Point 0"<<std::endl;
    print(os);
    k1=computeAccImpl(dt);
  }
  {
    set(yn+0.5*dt*yndot,yndot+0.5*dt*k1);
    os << "Point 1"<<std::endl;
    print(os);
    k2=computeAccImpl(dt);
  }
  {
    set(yn+0.5*dt*yndot+0.25*dt2*k1,yndot+0.5*dt*k2);
    os << "Point 2"<<std::endl;
    print(os);
    k3=computeAccImpl(dt);
  }
  {
    set(yn+dt*yndot+0.5*dt2*k2,yndot+dt*k3);
    os << "Point 3"<<std::endl;
    print(os);
    k4=computeAccImpl(dt);
  }
  
  Eigen::VectorXd ynp=yn+dt*yndot+(dt2/6.)*(k1+k2+k3);
  Eigen::VectorXd ynpdot=yndot+(dt/6)*(k1+2*k2+2*k3+k4);
  
  set(ynp,ynpdot);
  std::cout << "Point final"<<std::endl;
  print(os);
}

void SuperClothoidDynOneSegment::step()
{
  if(virtuaDamping>0.)
  {
    sysPtdot*=(1.-virtuaDamping);
  }
  if(useRK4)RK4();
  else DummyStep();
}
