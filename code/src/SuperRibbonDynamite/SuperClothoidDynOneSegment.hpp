/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef SUPERCLOTHOIDDYNONESEGMENT_H
#define SUPERCLOTHOIDDYNONESEGMENT_H

#include <array>
#include <iostream>
#include <Eigen/Dense>

class SuperClothoidDynOneSegment
{
public:
  SuperClothoidDynOneSegment();
  using PtV = Eigen::Matrix<double,6,1>;
  PtV getPoint() const;
  PtV getPointSpeed() const;
  void set(PtV pt, PtV speed);
  void print(std::ostream& os) const;
  std::ostream& operator<<(std::ostream& os) const;
  void setupGeo(Eigen::Vector3d pos, Eigen::Matrix3d frame, double Width, double thickness, double ribbonLength, PtV curvNat);
  void setupPhy(Eigen::Vector3d gravity, double Y, double areaDensity, double nu);
  void setupTimeDynamic(double dt, bool useRK4, double virtuaDamping);
  void step();
//   PtV computeAcc();
  PtV computeAccImpl(double dt);
  double computeE();
private:
  void RK4();
  void DummyStep();
  
  PtV sysPt;
  PtV sysPtdot;
  
  double virtuaDamping;
  bool useRK4;
  double dt;
  
  
  Eigen::Vector3d G;
  double Y;
  double areaDensity;
  double width;
  double thickness;
  double nu;
  double length;
  Eigen::Matrix3d R;
  Eigen::Vector3d r;
  PtV curvNat;
  
  bool cached=false;
  PtV acc;
  double E;
};

#endif // SUPERCLOTHOIDDYNONESEGMENT_H
