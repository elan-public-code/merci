/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "SystemDynamite.hpp"
#include "MathOperations.hpp"
#include <iostream>

SystemDynamite::SystemDynamite(MovingRibbon sr, Eigen::Vector3d G) :
  sr(sr), G(G)
{
}

void SystemDynamite::operator()()
{
  auto t=sr.getNbSegments();
  segmentsFun.resize(t);
  E=0;
  Ec=0.;
  Eg=0.;
  Ew=0.;
  for(size_t i=0;i<t;i++)
  {
    segmentsFun[i].areaDensity = sr.getAreaDensity();
    segmentsFun[i].width       = sr.getCrossSection()[0];
    segmentsFun[i].D           = sr.getD();
    segmentsFun[i].G           = G;
    

    //In arguments
    if(i==0)
    {
      auto[pos,frame]=sr.getPlace();
      segmentsFun[0].r=pos;
      segmentsFun[0].R=frame;
      segmentsFun[0].a = sr.vpointa(0);
      segmentsFun[0].adot = sr.vpointadot(0);
      segmentsFun[0].b = sr.vpointb();
      segmentsFun[0].bdot = sr.vpointbdot();
      segmentsFun[0].m = sr.vpointm();
      segmentsFun[0].mdot = sr.vpointmdot();
      segmentsFun[0].n = sr.vpointn(0);
      segmentsFun[0].ndot = sr.vpointndot(0);
      segmentsFun[0].anat = sr.vpointanat(0);
      segmentsFun[0].bnat = sr.vpointbnat();
      segmentsFun[0].length = sr.segmentLength(0);
    }
    else
    {
      segmentsFun[i].r = segmentsFun[i-1].endPos;
      segmentsFun[i].R = segmentsFun[i-1].endFrame;
      segmentsFun[i].a = sr.vpointa(i);
      segmentsFun[i].adot = sr.vpointadot(i);
      segmentsFun[i].b = segmentsFun[i-1].b+segmentsFun[i-1].length*segmentsFun[i-1].a;
      segmentsFun[i].bdot = segmentsFun[i-1].bdot+segmentsFun[i-1].length*segmentsFun[i-1].adot;
      segmentsFun[i].m = segmentsFun[i-1].m+segmentsFun[i-1].length*segmentsFun[i-1].n;
      segmentsFun[i].mdot = segmentsFun[i-1].mdot+segmentsFun[i-1].length*segmentsFun[i-1].ndot;
      segmentsFun[i].n = sr.vpointn(i);
      segmentsFun[i].ndot = sr.vpointndot(i);
      segmentsFun[i].anat = sr.vpointanat(i);
      segmentsFun[i].bnat = segmentsFun[i-1].bnat+segmentsFun[i-1].length*sr.vpointanat(i);
      segmentsFun[i].length = sr.segmentLength(i);
    }
    segmentsFun[i]();
    E+=segmentsFun[i].Ew+segmentsFun[i].Eg+segmentsFun[i].Ec;
    Ec+=segmentsFun[i].Ec;
    Eg+=segmentsFun[i].Eg;
    Ew+=segmentsFun[i].Ew;
  }
  computeGradPosFrame();
  Eigen::VectorXd dLdq = computedLdq();
  Eigen::MatrixXd d2Ecdqdot2 = computed2Ecdqdot2();
  Eigen::MatrixXd d2Ecdqdotdq = computed2Ecdqdotdq();
  Eigen::VectorXd qdot(sr.vpointSize()/2);
  qdot[sr.vpointIDb()/2]=sr.vpointb();
  qdot[sr.vpointIDm()/2]=sr.vpointm();
  for(size_t j=0;j<t;j++)
  {
    qdot[sr.vpointIDa(j)/2]=sr.vpointa(j);
    qdot[sr.vpointIDn(j)/2]=sr.vpointn(j);
  }
   std::cout<<"dLdq = "<<dLdq.transpose()<<std::endl<<std::endl;
//    std::cout<<"Eg = "<<Eg<<std::endl;
//    std::cout<<"Ew = "<<Ew<<std::endl;
//    std::cout<<"Ec = "<<Ec<<std::endl<<std::endl;
//    std::cout<<"d2Ecdqdot2 = "<<std::endl<<d2Ecdqdot2<<std::endl<<std::endl;
  std::cout<<"d2Ecdqdotdq = "<<std::endl<<d2Ecdqdotdq<<std::endl<<std::endl;
//    std::cout<<"endPos = "<<std::endl<<segmentsFun[0].endPos.transpose()<<std::endl<<std::endl;
//    std::cout<<"endFrame = "<<std::endl<<segmentsFun[0].endFrame<<std::endl<<std::endl;
  Eigen::VectorXd B=dLdq-d2Ecdqdotdq*qdot;
  
  int n = qdot.size();
  Eigen::MatrixXd I=Eigen::MatrixXd::Identity(n, n);
  Eigen::FullPivHouseholderQR<Eigen::MatrixXd> solver(d2Ecdqdot2+1e-8*I);
  solver.setThreshold(1.e-16);
  std::cout<<" =========== Solving =========== "<<std::endl;;
  std::cout<<d2Ecdqdot2<<std::endl<<std::endl;
  std::cout<<B.transpose()<<std::endl<<std::endl;
  Eigen::SelfAdjointEigenSolver<Eigen::MatrixXr> eigensolver(d2Ecdqdot2);
  std::cout<<"eigenvalues: "<<eigensolver.eigenvalues().transpose()<<std::endl<<std::endl;
  
  std::cout<<"eigenvectors "<<std::endl<<eigensolver.eigenvectors()<<std::endl;
  acceleration = solver.solve(B);
  std::cout<<"acceleration = "<<acceleration.transpose()<<std::endl;
}

void SystemDynamite::computeGradPosFrame()
{
  using MathOperations::compDeriv;
  gradPos.clear();
  gradFrames.clear();
  
  Eigen::Matrix<Eigen::Matrix<double,3,1>,3,1> dPosdPos;
  dPosdPos<<Eigen::Vector3d(1.,0.,0.),Eigen::Vector3d(0.,1.,0.),Eigen::Vector3d(0.,0.,1.);
  
  size_t t = sr.getNbSegments();
  for(size_t segmentID = 1; segmentID<=t;segmentID++)
  {
    gradPos[{segmentID,sr.vpointIDa(segmentID-1)}]=segmentsFun.at(segmentID-1).dPosda;
    gradPos[{segmentID,sr.vpointIDn(segmentID-1)}]=segmentsFun.at(segmentID-1).dPosdn;
    gradFrames[{segmentID,sr.vpointIDa(segmentID-1)}]=segmentsFun.at(segmentID-1).dFrameda;
    gradFrames[{segmentID,sr.vpointIDn(segmentID-1)}]=segmentsFun.at(segmentID-1).dFramedn;
    
    Eigen::FullPivHouseholderQR<Eigen::Matrix3d> invSolver(segmentsFun.at(segmentID-1).R);
    invSolver.setThreshold(1.e-16);
    Eigen::Matrix3d Rinverse = invSolver.inverse();
    
    Eigen::Matrix3d K = Rinverse*segmentsFun.at(segmentID-1).endFrame;
    Eigen::Vector3d T = Rinverse*(segmentsFun.at(segmentID-1).endPos-segmentsFun.at(segmentID-1).r);
    
    auto dPosdFrame=getdPosdFrame(T);
    auto dFramedFrame=getdFramedFrame(K);
  
    for(size_t j=0; j<segmentID-1;j++)
    {
      gradPos[{segmentID,sr.vpointIDa(j)}] = sr.segmentLength(j) * segmentsFun.at(segmentID-1).dPosdb
        + compDeriv(dPosdFrame, gradFrames[{segmentID-1,sr.vpointIDa(j)}]) 
        + compDeriv(dPosdPos, gradPos[{segmentID-1,sr.vpointIDa(j)}]);
      gradPos[{segmentID,sr.vpointIDn(j)}]=sr.segmentLength(j) * segmentsFun.at(segmentID-1).dPosdm
        + compDeriv(dPosdFrame, gradFrames[{segmentID-1,sr.vpointIDn(j)}]) 
        + compDeriv(dPosdPos, gradPos[{segmentID-1,sr.vpointIDn(j)}]);
      gradFrames[{segmentID,sr.vpointIDa(j)}]=sr.segmentLength(j) * segmentsFun.at(segmentID-1).dFramedb
        + compDeriv(dFramedFrame, gradFrames[{segmentID-1,sr.vpointIDa(j)}]);
      gradFrames[{segmentID,sr.vpointIDn(j)}]=sr.segmentLength(j) * segmentsFun.at(segmentID-1).dFramedm
        + compDeriv(dFramedFrame, gradFrames[{segmentID-1,sr.vpointIDn(j)}]);
    }
    
    
    gradPos[{segmentID,sr.vpointIDb()}]=segmentsFun.at(segmentID-1).dPosdb;
    gradPos[{segmentID,sr.vpointIDm()}]=segmentsFun.at(segmentID-1).dPosdm;
    gradFrames[{segmentID,sr.vpointIDb()}]=segmentsFun.at(segmentID-1).dFramedb;
    gradFrames[{segmentID,sr.vpointIDm()}]=segmentsFun.at(segmentID-1).dFramedm;

    if(segmentID>1)
    {
      gradPos[{segmentID,sr.vpointIDb()}] +=
        compDeriv(dPosdFrame, gradFrames.at({segmentID-1,sr.vpointIDb()})) + compDeriv(dPosdPos, gradPos[{segmentID-1,sr.vpointIDb()}]);
      gradPos[{segmentID,sr.vpointIDm()}] +=
        compDeriv(dPosdFrame, gradFrames[{segmentID-1,sr.vpointIDm()}]) + compDeriv(dPosdPos, gradPos[{segmentID-1,sr.vpointIDm()}]);
      gradFrames[{segmentID,sr.vpointIDb()}] +=
        compDeriv(dFramedFrame, gradFrames[{segmentID-1,sr.vpointIDb()}]);
      gradFrames[{segmentID,sr.vpointIDm()}] +=
        compDeriv(dFramedFrame, gradFrames[{segmentID-1,sr.vpointIDm()}]);
    }
  }
}

Eigen::Matrix<Eigen::Matrix3d, 3, 1> SystemDynamite::getdPosdFrame(const Eigen::Vector3d& preTrans) const
{
  Eigen::Matrix<Eigen::Matrix3d, 3, 1> out;

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++) {
      Eigen::Matrix3d tM;
      Eigen::Vector3d tMv;
      tM.setZero();
      tM(i, j) = 1.;
      tMv = tM * preTrans;
      for (int k = 0; k < 3; k++)
        out[k](i, j) = tMv(k);
    }
  return out;
}

Eigen::Matrix<Eigen::Matrix3d, 3, 3> SystemDynamite::getdFramedFrame(const Eigen::Matrix3d& preFrame) const
{
  Eigen::Matrix<Eigen::Matrix3d, 3, 3> out;

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++) {
      Eigen::Matrix3d tM;
      tM.setZero();
      tM(i, j) = 1.;
      tM = tM * preFrame;
      for (int k = 0; k < 3; k++)
        for (int l = 0; l < 3; l++)
          out(k, l)(i, j) = tM(k, l);
    }
  return out;
}

Eigen::VectorXd SystemDynamite::computedLdq()
{
  using MathOperations::contractGeneralDotP;
  Eigen::VectorXd grad(sr.vpointSize()/2);
  grad.setZero();
  size_t t=sr.getNbSegments();
  for(size_t segmentID=0;segmentID<t;segmentID++)
  {
    Eigen::Vector4d tmp = segmentsFun.at(segmentID).gradL;
    //std::cout<<"gradLSgt("<<segmentID<<") = "<<tmp.transpose()<<std::endl;
    grad[sr.vpointIDa(segmentID)/2]+=tmp[0];
    grad[sr.vpointIDn(segmentID)/2]+=tmp[2];
    
    const Eigen::Matrix3d dEnergydFrameSegment = segmentsFun.at(segmentID).dLdFrame;
    const Eigen::Vector3d dEnergydPosSegment = segmentsFun.at(segmentID).dLdPos;
    for(size_t j=0;j<segmentID;j++)
    {
      grad[sr.vpointIDa(j)/2]+=sr.segmentLength(j)*tmp[1];
      grad[sr.vpointIDn(j)/2]+=sr.segmentLength(j)*tmp[3];
      
      grad[sr.vpointIDn(j)/2] += 
        contractGeneralDotP(dEnergydFrameSegment, gradFrames.at({segmentID,sr.vpointIDn(j)})) 
        + dEnergydPosSegment.dot(gradPos.at({segmentID,sr.vpointIDn(j)}));
      grad[sr.vpointIDa(j)/2] += 
        contractGeneralDotP(dEnergydFrameSegment, gradFrames.at({segmentID,sr.vpointIDa(j)})) 
        + dEnergydPosSegment.dot(gradPos.at({segmentID,sr.vpointIDa(j)}));
    }
  }
  
  
  {
    int t = sr.getNbSegments();
    grad[sr.vpointIDb()/2]=0.;
    grad[sr.vpointIDm()/2]=0.;
    for (int i = t - 1; i > 0; i--) {
      double dpreEdbSegment = segmentsFun.at(i).gradL[1];
      double dpreEdmSegment = segmentsFun.at(i).gradL[3];
      double gradIb = 
        dpreEdbSegment 
        + contractGeneralDotP(segmentsFun.at(i).dLdFrame, gradFrames.at({i,sr.vpointIDb()})) 
        + segmentsFun.at(i).dLdPos.dot(gradPos.at({i,sr.vpointIDb()}));
      grad[sr.vpointIDb()/2] += gradIb;
      double gradIm = 
        dpreEdmSegment 
        + contractGeneralDotP(segmentsFun.at(i).dLdFrame, gradFrames.at({i,sr.vpointIDm()})) 
        + segmentsFun.at(i).dLdPos.dot(gradPos.at({i,sr.vpointIDm()}));
      grad[sr.vpointIDm()/2] += gradIm;
    }
    grad[sr.vpointIDb()/2] += segmentsFun.at(0).gradL[1];
    grad[sr.vpointIDm()/2] += segmentsFun.at(0).gradL[3];
      
  }
  return grad;
}

Eigen::MatrixXd SystemDynamite::computed2Ecdqdot2()
{
  size_t t = segmentsFun.size();
  Eigen::MatrixXd res(sr.vpointSize()/2,sr.vpointSize()/2);
  res.setZero();
  
  //i=0
  //std::cout<<"d2Ecdqdot2Temp(0) ="<<std::endl<<segmentsFun[0].d2Ecdqdot2<<std::endl;
  res(sr.vpointIDa(0)/2,sr.vpointIDa(0)/2) += segmentsFun[0].d2Ecdqdot2(0,0);
  res(sr.vpointIDa(0)/2,sr.vpointIDb( )/2) += segmentsFun[0].d2Ecdqdot2(0,1);
  res(sr.vpointIDa(0)/2,sr.vpointIDn(0)/2) += segmentsFun[0].d2Ecdqdot2(0,2);
  res(sr.vpointIDa(0)/2,sr.vpointIDm( )/2) += segmentsFun[0].d2Ecdqdot2(0,3);
  res(sr.vpointIDb( )/2,sr.vpointIDa(0)/2) += segmentsFun[0].d2Ecdqdot2(1,0);
  res(sr.vpointIDb( )/2,sr.vpointIDb( )/2) += segmentsFun[0].d2Ecdqdot2(1,1);
  res(sr.vpointIDb( )/2,sr.vpointIDn(0)/2) += segmentsFun[0].d2Ecdqdot2(1,2);
  res(sr.vpointIDb( )/2,sr.vpointIDm( )/2) += segmentsFun[0].d2Ecdqdot2(1,3);
  res(sr.vpointIDn(0)/2,sr.vpointIDa(0)/2) += segmentsFun[0].d2Ecdqdot2(2,0);
  res(sr.vpointIDn(0)/2,sr.vpointIDb( )/2) += segmentsFun[0].d2Ecdqdot2(2,1);
  res(sr.vpointIDn(0)/2,sr.vpointIDn(0)/2) += segmentsFun[0].d2Ecdqdot2(2,2);
  res(sr.vpointIDn(0)/2,sr.vpointIDm( )/2) += segmentsFun[0].d2Ecdqdot2(2,3);
  res(sr.vpointIDm( )/2,sr.vpointIDa(0)/2) += segmentsFun[0].d2Ecdqdot2(3,0);
  res(sr.vpointIDm( )/2,sr.vpointIDb( )/2) += segmentsFun[0].d2Ecdqdot2(3,1);
  res(sr.vpointIDm( )/2,sr.vpointIDn(0)/2) += segmentsFun[0].d2Ecdqdot2(3,2);
  res(sr.vpointIDm( )/2,sr.vpointIDm( )/2) += segmentsFun[0].d2Ecdqdot2(3,3);
  
  for(size_t i=1;i<t;i++)
  {
    const Eigen::Matrix4d d2Ecdqdot2=segmentsFun.at(i).d2Ecdqdot2;
    //std::cout<<"d2Ecdqdot2Temp("<<i<<") ="<<std::endl<<d2Ecdqdot2<<std::endl;
    res(sr.vpointIDb()/2,sr.vpointIDb()/2) += d2Ecdqdot2(1,1);
    res(sr.vpointIDb()/2,sr.vpointIDm()/2) += d2Ecdqdot2(1,3);
    res(sr.vpointIDm()/2,sr.vpointIDb()/2) += d2Ecdqdot2(3,1);
    res(sr.vpointIDm()/2,sr.vpointIDm()/2) += d2Ecdqdot2(3,3);
    
    res(sr.vpointIDb()/2,sr.vpointIDa(i)/2) += d2Ecdqdot2(1,0);
    res(sr.vpointIDb()/2,sr.vpointIDn(i)/2) += d2Ecdqdot2(1,2);
    res(sr.vpointIDm()/2,sr.vpointIDa(i)/2) += d2Ecdqdot2(3,0);
    res(sr.vpointIDm()/2,sr.vpointIDn(i)/2) += d2Ecdqdot2(3,2);
    
    res(sr.vpointIDa(i)/2,sr.vpointIDb()/2) += d2Ecdqdot2(0,1);
    res(sr.vpointIDn(i)/2,sr.vpointIDb()/2) += d2Ecdqdot2(2,1);
    res(sr.vpointIDa(i)/2,sr.vpointIDm()/2) += d2Ecdqdot2(0,3);
    res(sr.vpointIDn(i)/2,sr.vpointIDm()/2) += d2Ecdqdot2(2,3);
    
    res(sr.vpointIDa(i)/2,sr.vpointIDa(i)/2) += d2Ecdqdot2(0,0);
    res(sr.vpointIDa(i)/2,sr.vpointIDn(i)/2) += d2Ecdqdot2(0,2);
    res(sr.vpointIDn(i)/2,sr.vpointIDa(i)/2) += d2Ecdqdot2(2,0);
    res(sr.vpointIDn(i)/2,sr.vpointIDn(i)/2) += d2Ecdqdot2(2,2);
    
    for(size_t j=0;j<i;j++)
    {
      double sl = sr.segmentLength(j);
      res(sr.vpointIDb()/2,sr.vpointIDa(j)/2) += sl*d2Ecdqdot2(1,1);
      res(sr.vpointIDb()/2,sr.vpointIDn(j)/2) += sl*d2Ecdqdot2(1,3);
      res(sr.vpointIDm()/2,sr.vpointIDa(j)/2) += sl*d2Ecdqdot2(3,1);
      res(sr.vpointIDm()/2,sr.vpointIDn(j)/2) += sl*d2Ecdqdot2(3,3);
      
      res(sr.vpointIDa(j)/2,sr.vpointIDb()/2) += sl*d2Ecdqdot2(1,1);
      res(sr.vpointIDn(j)/2,sr.vpointIDb()/2) += sl*d2Ecdqdot2(3,1);
      res(sr.vpointIDa(j)/2,sr.vpointIDm()/2) += sl*d2Ecdqdot2(1,3);
      res(sr.vpointIDn(j)/2,sr.vpointIDm()/2) += sl*d2Ecdqdot2(3,3);
      
      res(sr.vpointIDa(i)/2,sr.vpointIDa(j)/2) += sl*d2Ecdqdot2(0,1);
      res(sr.vpointIDa(i)/2,sr.vpointIDn(j)/2) += sl*d2Ecdqdot2(0,3);
      res(sr.vpointIDn(i)/2,sr.vpointIDa(j)/2) += sl*d2Ecdqdot2(2,1);
      res(sr.vpointIDn(i)/2,sr.vpointIDn(j)/2) += sl*d2Ecdqdot2(2,3);
      
      res(sr.vpointIDa(j)/2,sr.vpointIDa(i)/2) += sl*d2Ecdqdot2(1,0);
      res(sr.vpointIDn(j)/2,sr.vpointIDa(i)/2) += sl*d2Ecdqdot2(3,0);
      res(sr.vpointIDa(j)/2,sr.vpointIDn(i)/2) += sl*d2Ecdqdot2(1,2);
      res(sr.vpointIDn(j)/2,sr.vpointIDn(i)/2) += sl*d2Ecdqdot2(3,2);
    }
    
    for(size_t j=0;j<i;j++)
    {
      double slj=sr.segmentLength(j);
      for(size_t k=0;k<i;k++)
      {
        res(sr.vpointIDa(j)/2,sr.vpointIDa(k)/2) += slj*sr.segmentLength(k)*d2Ecdqdot2(1,1);
        res(sr.vpointIDa(j)/2,sr.vpointIDn(k)/2) += slj*sr.segmentLength(k)*d2Ecdqdot2(1,3);
        res(sr.vpointIDn(j)/2,sr.vpointIDa(k)/2) += slj*sr.segmentLength(k)*d2Ecdqdot2(3,1);
        res(sr.vpointIDn(j)/2,sr.vpointIDn(k)/2) += slj*sr.segmentLength(k)*d2Ecdqdot2(3,3);
      }
    }
  }
  return res;
}

Eigen::MatrixXd SystemDynamite::computed2Ecdqdotdq()
{
  size_t t = segmentsFun.size();
  Eigen::MatrixXd res(sr.vpointSize()/2,sr.vpointSize()/2);
  res.setZero();
  
   //i=0
  //std::cout<<"d2EcdqdotdqTemp ="<<std::endl<<segmentsFun[0].d2Ecdqdotdq<<std::endl;
  res(sr.vpointIDadot(0)/2,sr.vpointIDa(0)/2) += segmentsFun[0].d2Ecdqdotdq(0,0);
  res(sr.vpointIDadot(0)/2,sr.vpointIDb( )/2) += segmentsFun[0].d2Ecdqdotdq(0,1);
  res(sr.vpointIDadot(0)/2,sr.vpointIDn(0)/2) += segmentsFun[0].d2Ecdqdotdq(0,2);
  res(sr.vpointIDadot(0)/2,sr.vpointIDm( )/2) += segmentsFun[0].d2Ecdqdotdq(0,3);
  res(sr.vpointIDbdot( )/2,sr.vpointIDa(0)/2) += segmentsFun[0].d2Ecdqdotdq(1,0);
  res(sr.vpointIDbdot( )/2,sr.vpointIDb( )/2) += segmentsFun[0].d2Ecdqdotdq(1,1);
  res(sr.vpointIDbdot( )/2,sr.vpointIDn(0)/2) += segmentsFun[0].d2Ecdqdotdq(1,2);
  res(sr.vpointIDbdot( )/2,sr.vpointIDm( )/2) += segmentsFun[0].d2Ecdqdotdq(1,3);
  res(sr.vpointIDndot(0)/2,sr.vpointIDa(0)/2) += segmentsFun[0].d2Ecdqdotdq(2,0);
  res(sr.vpointIDndot(0)/2,sr.vpointIDb( )/2) += segmentsFun[0].d2Ecdqdotdq(2,1);
  res(sr.vpointIDndot(0)/2,sr.vpointIDn(0)/2) += segmentsFun[0].d2Ecdqdotdq(2,2);
  res(sr.vpointIDndot(0)/2,sr.vpointIDm( )/2) += segmentsFun[0].d2Ecdqdotdq(2,3);
  res(sr.vpointIDmdot( )/2,sr.vpointIDa(0)/2) += segmentsFun[0].d2Ecdqdotdq(3,0);
  res(sr.vpointIDmdot( )/2,sr.vpointIDb( )/2) += segmentsFun[0].d2Ecdqdotdq(3,1);
  res(sr.vpointIDmdot( )/2,sr.vpointIDn(0)/2) += segmentsFun[0].d2Ecdqdotdq(3,2);
  res(sr.vpointIDmdot( )/2,sr.vpointIDm( )/2) += segmentsFun[0].d2Ecdqdotdq(3,3);
  
  for(size_t i=1;i<t;i++)
  {
    const Eigen::Matrix4d d2Ecdqdotdq=segmentsFun[i].d2Ecdqdotdq;
    //std::cout<<"d2EcdqdotdqTemp ="<<std::endl<<d2Ecdqdotdq<<std::endl;
    res(sr.vpointIDbdot()/2,sr.vpointIDb()/2) += d2Ecdqdotdq(1,1);
    res(sr.vpointIDbdot()/2,sr.vpointIDm()/2) += d2Ecdqdotdq(1,3);
    res(sr.vpointIDmdot()/2,sr.vpointIDb()/2) += d2Ecdqdotdq(3,1);
    res(sr.vpointIDmdot()/2,sr.vpointIDm()/2) += d2Ecdqdotdq(3,3);

    res(sr.vpointIDbdot()/2,sr.vpointIDa(i)/2) += d2Ecdqdotdq(1,0);
    res(sr.vpointIDbdot()/2,sr.vpointIDn(i)/2) += d2Ecdqdotdq(1,2);
    res(sr.vpointIDmdot()/2,sr.vpointIDa(i)/2) += d2Ecdqdotdq(3,0);
    res(sr.vpointIDmdot()/2,sr.vpointIDn(i)/2) += d2Ecdqdotdq(3,2);
    
    res(sr.vpointIDadot(i)/2,sr.vpointIDb()/2) += d2Ecdqdotdq(0,1);
    res(sr.vpointIDndot(i)/2,sr.vpointIDb()/2) += d2Ecdqdotdq(2,1);
    res(sr.vpointIDadot(i)/2,sr.vpointIDm()/2) += d2Ecdqdotdq(0,3);
    res(sr.vpointIDndot(i)/2,sr.vpointIDm()/2) += d2Ecdqdotdq(2,3);

    res(sr.vpointIDadot(i)/2,sr.vpointIDa(i)/2) += d2Ecdqdotdq(0,0);
    res(sr.vpointIDadot(i)/2,sr.vpointIDn(i)/2) += d2Ecdqdotdq(0,2);
    res(sr.vpointIDndot(i)/2,sr.vpointIDa(i)/2) += d2Ecdqdotdq(2,0);
    res(sr.vpointIDndot(i)/2,sr.vpointIDn(i)/2) += d2Ecdqdotdq(2,2);
    
    for(size_t j=0;j<i;j++)
    {
      double sl = sr.segmentLength(j);
      res(sr.vpointIDbdot()/2,sr.vpointIDa(j)/2) += sl*d2Ecdqdotdq(1,1);
      res(sr.vpointIDbdot()/2,sr.vpointIDn(j)/2) += sl*d2Ecdqdotdq(1,3);
      res(sr.vpointIDmdot()/2,sr.vpointIDa(j)/2) += sl*d2Ecdqdotdq(3,1);
      res(sr.vpointIDmdot()/2,sr.vpointIDn(j)/2) += sl*d2Ecdqdotdq(3,3);

      res(sr.vpointIDadot(j)/2,sr.vpointIDb()/2) += sl*d2Ecdqdotdq(1,1);
      res(sr.vpointIDndot(j)/2,sr.vpointIDb()/2) += sl*d2Ecdqdotdq(3,1);
      res(sr.vpointIDadot(j)/2,sr.vpointIDm()/2) += sl*d2Ecdqdotdq(1,3);
      res(sr.vpointIDndot(j)/2,sr.vpointIDm()/2) += sl*d2Ecdqdotdq(3,3);

      res(sr.vpointIDadot(i)/2,sr.vpointIDa(j)/2) += sl*d2Ecdqdotdq(0,1);
      res(sr.vpointIDadot(i)/2,sr.vpointIDn(j)/2) += sl*d2Ecdqdotdq(0,3);
      res(sr.vpointIDndot(i)/2,sr.vpointIDa(j)/2) += sl*d2Ecdqdotdq(2,1);
      res(sr.vpointIDndot(i)/2,sr.vpointIDn(j)/2) += sl*d2Ecdqdotdq(2,3);
     
      res(sr.vpointIDadot(j)/2,sr.vpointIDa(i)/2) += sl*d2Ecdqdotdq(1,0);
      res(sr.vpointIDndot(j)/2,sr.vpointIDa(i)/2) += sl*d2Ecdqdotdq(3,0);
      res(sr.vpointIDadot(j)/2,sr.vpointIDn(i)/2) += sl*d2Ecdqdotdq(1,2);
      res(sr.vpointIDndot(j)/2,sr.vpointIDn(i)/2) += sl*d2Ecdqdotdq(3,2);
    }
    
    for(size_t j=0;j<i;j++)
    {
      double slj=sr.segmentLength(j);
      for(size_t k=0;k<i;k++)
      {
        res(sr.vpointIDadot(j)/2,sr.vpointIDa(k)/2) += slj*sr.segmentLength(k)*d2Ecdqdotdq(1,1);
        res(sr.vpointIDadot(j)/2,sr.vpointIDn(k)/2) += slj*sr.segmentLength(k)*d2Ecdqdotdq(1,3);
        res(sr.vpointIDndot(j)/2,sr.vpointIDa(k)/2) += slj*sr.segmentLength(k)*d2Ecdqdotdq(3,1);
        res(sr.vpointIDndot(j)/2,sr.vpointIDn(k)/2) += slj*sr.segmentLength(k)*d2Ecdqdotdq(3,3);
      }
    }
  }
  return res;
}
