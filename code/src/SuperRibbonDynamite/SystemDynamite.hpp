/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef SYSTEMDYNAMITE_H
#define SYSTEMDYNAMITE_H

#include <vector>
#include <map>
#include "DynSegment.hpp"
#include "MovingRibbon.hpp"

class SystemDynamite
{
public:
  SystemDynamite(MovingRibbon sr, Eigen::Vector3d G);
  void operator()();
  Eigen::VectorXd acceleration;
  double E;
  double Ec;
  double Eg;
  double Ew;
private:
  MovingRibbon sr;
  Eigen::Vector3d G;
  std::vector<DynSegment> segmentsFun;
  void computeGradPosFrame();
  Eigen::VectorXd computedLdq();
  Eigen::MatrixXd computed2Ecdqdot2();
  Eigen::MatrixXd computed2Ecdqdotdq();
  Eigen::Matrix<Eigen::Matrix3d, 3, 3> getdFramedFrame(const Eigen::Matrix3d& preFrame) const;
  Eigen::Matrix<Eigen::Matrix3d, 3, 1> getdPosdFrame(const Eigen::Vector3d& preTrans) const;
  std::map<std::pair<size_t,size_t>, Eigen::Vector3d> gradPos;
  std::map<std::pair<size_t,size_t>, Eigen::Matrix3d> gradFrames;
};

#endif // SYSTEMDYNAMITE_H
