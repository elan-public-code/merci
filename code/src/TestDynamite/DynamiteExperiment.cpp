/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "DynamiteExperiment.hpp"
#include <iostream>


DynamiteExperiment::DynamiteExperiment(std::ostream& o) :
  G(0.,-9.81,0.),
  os(o),
  dt(0.001),
  correctE(false)
{
  mr.setPhysicalParams(0.1, 100, 0.5);
  Eigen::Matrix3d frame=Eigen::AngleAxisd(M_PI/8.,Eigen::Vector3d(0.,0.,1.)).toRotationMatrix();
  Eigen::Vector3d pos(0.,0.,0.);
  mr.place(pos,frame);
  int nbSegments = 1;
  mr.setNbSegments(nbSegments);
  mr.vpointb()=0.1;
  mr.vpointm()=0.;
  mr.vpointbdot()=0.1;
  mr.vpointmdot()=0.;
  for(int i=0; i<nbSegments; i++){
    mr.segmentLength(i)=0.1;
    mr.vpointa(i)=0.1;
    mr.vpointn(i)=0.;
    mr.vpointadot(i)=0.1;
    mr.vpointndot(i)=0.;
  }
  os<<mr.getPointAndSpeed().first.transpose()<<std::endl;
  SystemDynamite sys(mr, G);
  sys();
  E=sys.E;
  realE=E;
}

void DynamiteExperiment::step()
{
  MovingRibbon mro=RK4(mr, dt, G);
//   MovingRibbon mro=DummyStep(mr, dt, G);
  iterNb++;
  if(correctE)
  {
    auto [pt, v] = mro.getPointAndSpeed();
    v=0.95*v;
    mro.setPointAndSpeed(pt,v);
  }
  mr=mro;
  std::cout << "Point final"<<std::endl;
  mr.printPoint();
  realE=getEnergy(mr);
  auto point = mr.getPointAndSpeed().first;
  /*if((iterNb%10)==0)*/
  os<<point.transpose()<<std::endl;
}

double DynamiteExperiment::getE()
{
  return realE;
}

double DynamiteExperiment::getEnergy(const MovingRibbon& rib) const
{
  SystemDynamite sys(rib, G);
  sys();
  return sys.E;
}

void DynamiteExperiment::CorrectionStep(const Eigen::VectorXd& point, const Eigen::VectorXd& speed)
{
  {
    SystemDynamite sys(mr, G);
    sys();
    realE=sys.E;
  }
  if(realE<E)
  {
    E=realE;
    return ;
  }
  lastCorrFact=0.01;
  do
  {
    lastCorrFact*=2.;
    //std::cout<<"E="<<realE<<" > "<<E<<" too high, correction by "<<lastCorrFact<<std::endl;
    SystemDynamite sys(mr, G);
    mr.setPointAndSpeed(point,(1.-lastCorrFact)*speed);
    sys();
    realE=sys.E;
  }
  while(realE>E);
  E=realE;
}
