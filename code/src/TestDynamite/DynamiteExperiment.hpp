/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef DYNAMITEEXPERIMENT_H
#define DYNAMITEEXPERIMENT_H

#include <MovingRibbon.hpp>
#include <SystemDynamite.hpp>
#include <RungeKutta4.hpp>
#include <fstream>

class DynamiteExperiment
{
public:
  DynamiteExperiment(std::ostream& os);
  void step();
  double getE();
private:
  void CorrectionStep(const Eigen::VectorXd& point, const Eigen::VectorXd& speed);
  double getEnergy(const MovingRibbon&) const;
  Eigen::Vector3d G;
  MovingRibbon mr;
  std::ostream& os;
  double E;
  double dt;
  double lastCorrFact=0.;
  double realE;
  bool correctE;
  int iterNb=0;
};

#endif // DYNAMITEEXPERIMENT_H
