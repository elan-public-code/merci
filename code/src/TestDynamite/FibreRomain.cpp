/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "FibreRomain.hpp"
extern "C" {
#include <RomainClotho/clothoids/sclothoid_interface.h>
#include <RomainClotho/core/mem.h>
#include <RomainClotho/core/algorithms.h>
}

#include <iostream>

void FibreRomain::step()
{
  cloc_scalar_t* lengths = cloc_calloc(1, cloc_scalar_t);
  lengths[0]=this->length;
  cloc_scalar_t* curvatures = cloc_calloc(6, cloc_scalar_t);
  cloc_scalar_t* curvatures_dot = cloc_calloc(6, cloc_scalar_t);
  cloc_scalar_t* curvatures_nat = cloc_calloc(6, cloc_scalar_t);
  for(int i=0;i<3;i++) {
    curvatures[i]=this->point[2*i];
    curvatures_dot[i]=this->point[2*i+1];
    curvatures[3+i]=this->point[2*i]+this->point[2*i+6]*length;
    curvatures_dot[3+i]=this->point[2*i+1]+this->point[2*i+1+6]*length;
  }
  for(int i=0;i<3;i++)curvatures_nat[i]=this->natCurv[i];
  for(int i=0;i<3;i++)curvatures_nat[i+3]=this->natCurv[i]+this->length*this->natCurv[3+i];
  cloc_pacurvature_t pac_dot;
  cloc_pacurvature_init(&pac_dot, lengths, curvatures_dot, 1);
  cloc_pacurvature_t pac;
  cloc_pacurvature_init(&pac, lengths, curvatures, 1);
  cloc_pacurvature_t pac_nat;
  cloc_pacurvature_init(&pac_nat, lengths, curvatures_nat, 1);
  cloc_sclothoid_t ssc;
  cloc_sclothoid_init(&ssc);
  cloc_sclothoid_set_radius1(&ssc, this->crossSection[0]);
  cloc_sclothoid_set_radius2(&ssc, this->crossSection[1]);
  cloc_sclothoid_set_young_modulus(&ssc, this->Y);
  cloc_sclothoid_set_poisson_ratio(&ssc, this->nu);
  cloc_sclothoid_set_volumic_mass(&ssc, this->volumicDensity);
  cloc_sclothoid_set_internal_friction(&ssc, this->virtuaDamping);
  cloc_sclothoid_set_air_damping(&ssc, 0.);
  for(int i=0;i<3;i++)cloc_sclothoid_get_gravity(&ssc)[i] = this->G[i];
  cloc_sclothoid_change_curvature(&ssc, &pac);
  cloc_sclothoid_change_curvature_dot(&ssc, &pac_dot);
  cloc_sclothoid_change_natural_curvature(&ssc, &pac_nat);
  cloc_sclothoid_stepforward(&ssc, this->dt);
  auto curv_pa=cloc_sclothoid_get_curvature(&ssc);
  auto curv_pa_dot=cloc_sclothoid_get_curvature_dot(&ssc);
  for(int i=0;i<3;i++) {
    point[2*i]    = curv_pa->curvatures[i];
    point[2*i+1]  = curv_pa_dot->curvatures[i];
    point[6+2*i]  = (curv_pa->curvatures[3+i]-curv_pa->curvatures[i])/length;
    point[6+2*i+1]= (curv_pa_dot->curvatures[3+i]-curv_pa_dot->curvatures[i])/length;
  }
  cloc_pacurvature_destroy(&pac);
  cloc_pacurvature_destroy(&pac_nat);
  cloc_pacurvature_destroy(&pac_dot);
  cloc_free(lengths);
  cloc_free(curvatures);
  cloc_free(curvatures_dot);
  cloc_free(curvatures_nat);
  cloc_sclothoid_destroy(&ssc);
}

void initRomainFibreClass(sol::state& lua)
{
    lua.new_usertype<FibreRomain>(
    "Fibre",       sol::constructors<FibreRomain()>(),
    "G",            &FibreRomain::G,
    "natCurv",      &FibreRomain::natCurv,
    "Y",            &FibreRomain::Y,
    "volumicDensity",&FibreRomain::volumicDensity,
    "nu",           &FibreRomain::nu,
    "length",       &FibreRomain::length,
    "point",        &FibreRomain::point,
    "virtuaDamping",&FibreRomain::virtuaDamping,
    "crossSection", &FibreRomain::crossSection,
    "dt",           &FibreRomain::dt,
    "step",         &FibreRomain::step
    );
}
