/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef FIBREROMAIN_H
#define FIBREROMAIN_H

#include "sol.hpp"

class FibreRomain
{
public:
  std::array<double,3> G;
  std::array<double,6> natCurv;
  double Y;
  double volumicDensity;
  double length;
  double nu;
  std::array<double,12> point={1.};
  double virtuaDamping=0;
  std::array<double,2> crossSection;
  double dt=1.e-3;
  void step();
private:
  std::vector<std::array<double, 6>> stepData;
};

void initRomainFibreClass(sol::state &lua);

#endif // FIBREROMAIN_H
