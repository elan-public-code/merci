/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "OneSegmentExperiment.hpp"
#include <MovingRibbon.hpp>
#include <SystemDynamite.hpp>
#include <RungeKutta4.hpp>


OneSegmentExperiment::OneSegmentExperiment(std::string filename) :
  posOr({0.,0.,0.}),
  frameOr({1.,0.,0.,0.,1.,0.,0.,0.,1.}),
  G({0.,-9.81,0.}),
  natCurv({0.,0.}),
  D(1.),
  areaDensity(100.),
  length(0.1),
  nu(0.5),
  point({0.,0.,0.,0.,0.,0.,0.,0.}),
  useRK4(false),
  crossSection({0.01,0.0001}),
  os(filename)
{
}

double OneSegmentExperiment::step()
{
  MovingRibbon mr;
  mr.setPhysicalParams(D, areaDensity,nu);
  mr.setCrossSection(crossSection);
  Eigen::Vector3d epos;epos<<posOr[0],posOr[1],posOr[2];
  Eigen::Matrix3d emat;emat<<frameOr[0],frameOr[1],frameOr[2],
                             frameOr[3],frameOr[4],frameOr[5],
                             frameOr[6],frameOr[7],frameOr[8];
  mr.place(epos,emat);
  int nbSegments = 1;
  mr.setNbSegments(nbSegments);
  mr.vpointb()=point[0];
  mr.vpointm()=point[2];
  mr.vpointbdot()=point[1];
  mr.vpointmdot()=point[3];
  mr.segmentLength(0)=length;
  mr.vpointa(0)=point[4];
  mr.vpointn(0)=point[6];
  mr.vpointadot(0)=point[5];
  mr.vpointndot(0)=point[7];
  mr.vpointanat(0)=natCurv[0];
  mr.vpointbnat()=natCurv[1];
  Eigen::Vector3d eG;eG<<G[0],G[1],G[2];
  MovingRibbon mro=useRK4?RK4(mr,dt,eG):DummyStep(mr,dt,eG);
  if(virtuaDamping>0.)
  {
    auto [pt, speed] =mro.getPointAndSpeed();
    speed*=virtuaDamping;
    mro.setPointAndSpeed(pt,speed);
  }
  SystemDynamite sys(mro,eG);
  sys();
  energy=sys.E;
  auto [pt, speed] =mro.getPointAndSpeed();
  os<<pt.transpose()<<std::endl;
  for(int i=0;i<4;i++) {
    point[2*i]=pt(i);
    point[2*i+1]=speed(i);
  }
  return energy;
}


void OneSegmentExperiment::printPoint()
{
  MovingRibbon mr;
  mr.setCrossSection(crossSection);
  Eigen::Vector3d epos;epos<<posOr[0],posOr[1],posOr[2];
  Eigen::Matrix3d emat;emat<<frameOr[0],frameOr[1],frameOr[2],
                             frameOr[3],frameOr[4],frameOr[5],
                             frameOr[6],frameOr[7],frameOr[8];
  mr.place(epos,emat);
  mr.setNbSegments(1);
  mr.vpointb()=point[0];
  mr.vpointm()=point[2];
  mr.vpointbdot()=point[1];
  mr.vpointmdot()=point[3];
  mr.segmentLength(0)=length;
  mr.vpointa(0)=point[4];
  mr.vpointn(0)=point[6];
  mr.vpointadot(0)=point[5];
  mr.vpointndot(0)=point[7];
  mr.printPoint();
}

double OneSegmentExperiment::getEnergy()
{
  return energy;
}

void initOneSegmentClass(sol::state& lua)
{
    lua.new_usertype<OneSegmentExperiment>(
    "DynSeg",       sol::constructors<OneSegmentExperiment(std::string)>(),
    "G",            &OneSegmentExperiment::G,
    "natCurv",      &OneSegmentExperiment::natCurv,
    "pos",          &OneSegmentExperiment::posOr,
    "frame",        &OneSegmentExperiment::frameOr,
    "D",            &OneSegmentExperiment::D,
    "areaDensity",  &OneSegmentExperiment::areaDensity,
    "nu",           &OneSegmentExperiment::nu,
    "length",       &OneSegmentExperiment::length,
    "point",        &OneSegmentExperiment::point,
    "virtuaDamping",&OneSegmentExperiment::virtuaDamping,
    "energy",       sol::property(&OneSegmentExperiment::getEnergy),
    "useRK4",       &OneSegmentExperiment::useRK4,
    "crossSection", &OneSegmentExperiment::crossSection,
    "dt",           &OneSegmentExperiment::dt,
    "step",         &OneSegmentExperiment::step,
    "printPoint",   &OneSegmentExperiment::printPoint
    );
}
