/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef ONESEGMENTEXPERIMENT_H
#define ONESEGMENTEXPERIMENT_H

#include <array>
#include <fstream>
#include <string>

#include "../sol.hpp"

class OneSegmentExperiment
{
public:
  OneSegmentExperiment(std::string filename);
  std::array<double,3> posOr;
  std::array<double,9> frameOr;
  std::array<double,3> G;
  std::array<double,2> natCurv;
  double D;
  double areaDensity;
  double length;
  double nu;
  std::array<double,8> point;
  double virtuaDamping=0;
  bool useRK4;
  std::array<double,2> crossSection;
  double dt=1.e-3;
  double step();
  void printPoint();
  double getEnergy();
private:
  double energy=0.;
  std::ofstream os;
};

void initOneSegmentClass(sol::state &lua);

#endif // ONESEGMENTEXPERIMENT_H
