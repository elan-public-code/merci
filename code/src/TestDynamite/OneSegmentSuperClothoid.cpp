/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "OneSegmentSuperClothoid.hpp"
#include <BaseCore/real.hpp>

SGeoSetup::SGeoSetup() :
  pos({0.,0.,0.}),
  frame({1.,0.,0.,0.,1.,0.,0.,0.,1.}),
  ribbonWidth(0.001),
  ribbonThickness(0.001),
  ribbonLength(1),
  curvNat({0.,0.,0.,0.,0.,0.})
{
}

STimeSetup::STimeSetup() :
  dT(0.001),
  useRK4(false),
  virtuaDamping(0.)
{
}

SPhySetup::SPhySetup() :
  G({0.,-9.81,0.}),
  Y(1.e6),
  areaDensity(1000.),
  nu(0.5)
{
}

OneSegmentSuperClothoid::OneSegmentSuperClothoid()
{
}

std::array<double, 6> OneSegmentSuperClothoid::getPoint()
{
  auto pt=fun.getPoint();
  return {pt[0],pt[1],pt[2],pt[3],pt[4],pt[5]};
}

std::array<double, 6> OneSegmentSuperClothoid::getPointSpeed()
{
  auto pt=fun.getPointSpeed();
  return {pt[0],pt[1],pt[2],pt[3],pt[4],pt[5]};
}

void OneSegmentSuperClothoid::set(std::array<double, 6> pt, std::array<double, 6> speed)
{
  Eigen::Vector6r ptx;
  Eigen::Vector6r pts;
  ptx<<pt[0],pt[1],pt[2],pt[3],pt[4],pt[5];
  pts<<speed[0],speed[1],speed[2],speed[3],speed[4],speed[5];
  fun.set(ptx,pts);
}

void OneSegmentSuperClothoid::print()
{
  fun.print(std::cout);
}

void OneSegmentSuperClothoid::setupGeo(SGeoSetup setup)
{
  auto& posOr=setup.pos;
  auto& frameOr=setup.frame;
  auto& cNat=setup.curvNat;
  Eigen::Vector3d epos;epos<<posOr[0],posOr[1],posOr[2];
  Eigen::Matrix3d emat;emat<<frameOr[0],frameOr[1],frameOr[2],
                             frameOr[3],frameOr[4],frameOr[5],
                             frameOr[6],frameOr[7],frameOr[8];
  Eigen::Vector6r qnat;qnat<<cNat[0],cNat[1],cNat[2],cNat[3],cNat[4],cNat[5];
  fun.setupGeo(epos, emat, setup.ribbonWidth, setup.ribbonThickness, setup.ribbonLength, qnat);
}

void OneSegmentSuperClothoid::setupPhy(SPhySetup setup)
{
  auto& G=setup.G;
  Eigen::Vector3d eG;eG<<G[0],G[1],G[2];
  fun.setupPhy(eG,setup.Y,setup.areaDensity,setup.nu);
}

void OneSegmentSuperClothoid::setupTimeDynamic(STimeSetup setup)
{
  fun.setupTimeDynamic(setup.dT,setup.useRK4,setup.virtuaDamping);
}

double OneSegmentSuperClothoid::getEnergy()
{
  return fun.computeE();
}

// std::array<double, 6> OneSegmentSuperClothoid::getAcc()
// {
//   auto pt=fun.computeAcc();
//   return {pt[0],pt[1],pt[2],pt[3],pt[4],pt[5]};
// }

void OneSegmentSuperClothoid::step()
{
  fun.step();
}

void initOneSegmentSCloto(sol::state& lua)
{
     lua.new_usertype<SGeoSetup>(
    "SGeoSetup",    sol::constructors<SGeoSetup()>(),
    "Cnat",         &SGeoSetup::curvNat,
    "pos",          &SGeoSetup::pos,
    "frame",        &SGeoSetup::frame,
    "length",       &SGeoSetup::ribbonLength,
    "thickness",    &SGeoSetup::ribbonThickness,
    "width",        &SGeoSetup::ribbonWidth
    );
    
   lua.new_usertype<STimeSetup>(
    "STimeSetup",   sol::constructors<STimeSetup()>(),
    "virtuaDamping",&STimeSetup::virtuaDamping,
    "useRK4",       &STimeSetup::useRK4,
    "dt",           &STimeSetup::dT
    );
    
    lua.new_usertype<SPhySetup>(
    "SPhySetup",    sol::constructors<SPhySetup()>(),
    "G",            &SPhySetup::G,
    "Y",            &SPhySetup::Y,
    "areaDensity",  &SPhySetup::areaDensity,
    "nu",           &SPhySetup::nu
    );
    
    lua.new_usertype<OneSegmentSuperClothoid>(
    "SClotho",   sol::constructors<OneSegmentSuperClothoid()>(),
    "set",          &OneSegmentSuperClothoid::set,
    "print",        &OneSegmentSuperClothoid::print,
    "setupGeo",     &OneSegmentSuperClothoid::setupGeo,
    "setupPhy",     &OneSegmentSuperClothoid::setupPhy,
    "setupTime",    &OneSegmentSuperClothoid::setupTimeDynamic,
    "step",         &OneSegmentSuperClothoid::step,
    "energy",       sol::property(&OneSegmentSuperClothoid::getEnergy),
    //"acc",          sol::property(&OneSegmentSuperClothoid::getAcc),
    "pt",           sol::property(&OneSegmentSuperClothoid::getPoint),
    "ptDot",        sol::property(&OneSegmentSuperClothoid::getPointSpeed)
    );
}
