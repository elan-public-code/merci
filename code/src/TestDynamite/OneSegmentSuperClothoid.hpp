/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef ONESEGMENTSUPERCLOTHOID_H
#define ONESEGMENTSUPERCLOTHOID_H

#include <array>
#include <fstream>
#include <string>

#include "../sol.hpp"
#include <SuperClothoidDynOneSegment.hpp>

struct SGeoSetup
{
  SGeoSetup();
  std::array<double,3> pos;
  std::array<double,9> frame;
  double ribbonWidth;
  double ribbonThickness;
  double ribbonLength;
  std::array<double,6> curvNat;
};
struct STimeSetup
{
  STimeSetup();
  double dT;
  bool useRK4;
  double virtuaDamping;
};
struct SPhySetup
{
  SPhySetup();
  std::array<double,3> G;
  double Y;
  double areaDensity;
  double nu;
};

class OneSegmentSuperClothoid
{
public:
  OneSegmentSuperClothoid();
  std::array<double,6> getPoint();
  std::array<double,6> getPointSpeed();
  void set(std::array<double,6> pt, std::array<double,6> speed);
  void print();
  void setupGeo(SGeoSetup setup);
  void setupPhy(SPhySetup setup);
  void setupTimeDynamic(STimeSetup setup);
  void step();
  //std::array<double,6> getAcc();
  double getEnergy();
private:
  SuperClothoidDynOneSegment fun;
};

void initOneSegmentSCloto(sol::state &lua);

#endif // ONESEGMENTSUPERCLOTHOID_H
