/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "OneSegmentTigeApprox.hpp"

GeoSetup::GeoSetup() :
  pos({0.,0.,0.}),
  frame({1.,0.,0.,0.,1.,0.,0.,0.,1.}),
  ribbonWidth(0.01),
  ribbonLength(1),
  curvANat(0.),
  curvBNat(0.)
{
}

TimeSetup::TimeSetup() :
  dT(0.001),
  useRK4(false),
  virtuaDamping(0.)
{
}

PhySetup::PhySetup() :
  G({0.,-9.81,0.}),
  D(1.),
  areaDensity(100.),
  nu(0.5)
{
}

OneSegmentTigeApprox::OneSegmentTigeApprox()
{
}

std::array<double, 4> OneSegmentTigeApprox::getPoint()
{
  auto pt=fun.getPoint();
  return {pt[0],pt[1],pt[2],pt[3]};
}

std::array<double, 4> OneSegmentTigeApprox::getPointSpeed()
{
  auto pt=fun.getPointSpeed();
  return {pt[0],pt[1],pt[2],pt[3]};
}

void OneSegmentTigeApprox::set(std::array<double, 4> pt, std::array<double, 4> speed)
{
  fun.set(Eigen::Vector4d(pt[0],pt[1],pt[2],pt[3]),Eigen::Vector4d(speed[0],speed[1],speed[2],speed[3]));
}

void OneSegmentTigeApprox::print()
{
  fun.print(std::cout);
}

void OneSegmentTigeApprox::setupGeo(GeoSetup setup)
{
  auto& posOr=setup.pos;
  auto& frameOr=setup.frame;
  Eigen::Vector3d epos;epos<<posOr[0],posOr[1],posOr[2];
  Eigen::Matrix3d emat;emat<<frameOr[0],frameOr[1],frameOr[2],
                             frameOr[3],frameOr[4],frameOr[5],
                             frameOr[6],frameOr[7],frameOr[8];
  fun.setupGeo(epos, emat, setup.ribbonWidth, setup.ribbonLength, setup.curvANat, setup.curvBNat);
}

void OneSegmentTigeApprox::setupPhy(PhySetup setup)
{
  auto& G=setup.G;
  Eigen::Vector3d eG;eG<<G[0],G[1],G[2];
  fun.setupPhy(eG,setup.D,setup.areaDensity,setup.nu);
}

void OneSegmentTigeApprox::setupTimeDynamic(TimeSetup setup)
{
  fun.setupTimeDynamic(setup.dT,setup.useRK4,setup.virtuaDamping);
}

void OneSegmentTigeApprox::step()
{
  fun.step();
}

double OneSegmentTigeApprox::getEnergy()
{
  return fun.computeE();
}

std::array<double, 4> OneSegmentTigeApprox::getAcc()
{
  auto pt=fun.computeAcc();
  return {pt[0],pt[1],pt[2],pt[3]};
}



void initOneSegmentTigeApprox(sol::state &lua)
{
   lua.new_usertype<GeoSetup>(
    "GeoSetup",       sol::constructors<GeoSetup()>(),
    "natA",         &GeoSetup::curvANat,
    "natB",         &GeoSetup::curvBNat,
    "pos",          &GeoSetup::pos,
    "frame",        &GeoSetup::frame,
    "length",       &GeoSetup::ribbonLength,
    "width",        &GeoSetup::ribbonWidth
    );
    
   lua.new_usertype<TimeSetup>(
    "TimeSetup",       sol::constructors<TimeSetup()>(),
    "virtuaDamping",&TimeSetup::virtuaDamping,
    "useRK4",       &TimeSetup::useRK4,
    "dt",           &TimeSetup::dT
    );
    
    lua.new_usertype<PhySetup>(
    "PhySetup",       sol::constructors<PhySetup()>(),
    "G",            &PhySetup::G,
    "D",            &PhySetup::D,
    "areaDensity",  &PhySetup::areaDensity,
    "nu",           &PhySetup::nu
    );
    
    lua.new_usertype<OneSegmentTigeApprox>(
    "Simplistic",   sol::constructors<OneSegmentTigeApprox()>(),
    "set",          &OneSegmentTigeApprox::set,
    "print",        &OneSegmentTigeApprox::print,
    "setupGeo",     &OneSegmentTigeApprox::setupGeo,
    "setupPhy",     &OneSegmentTigeApprox::setupPhy,
    "setupTime",    &OneSegmentTigeApprox::setupTimeDynamic,
    "step",         &OneSegmentTigeApprox::step,
    "energy",       sol::property(&OneSegmentTigeApprox::getEnergy),
    "acc",          sol::property(&OneSegmentTigeApprox::getAcc),
    "pt",           sol::property(&OneSegmentTigeApprox::getPoint),
    "ptDot",        sol::property(&OneSegmentTigeApprox::getPointSpeed)
    );
}
