/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef ONESEGMENTTIGEAPPROX_H
#define ONESEGMENTTIGEAPPROX_H

#include <array>
#include <fstream>
#include <string>

#include "../sol.hpp"
#include <RubanDynOneSegmentApproxTige.hpp>

struct GeoSetup
{
  GeoSetup();
  std::array<double,3> pos;
  std::array<double,9> frame;
  double ribbonWidth;
  double ribbonLength;
  double curvANat;
  double curvBNat;
};
struct TimeSetup
{
  TimeSetup();
  double dT;
  bool useRK4;
  double virtuaDamping;
};
struct PhySetup
{
  PhySetup();
  std::array<double,3> G;
  double D;
  double areaDensity;
  double nu;
};

class OneSegmentTigeApprox
{
public:
  OneSegmentTigeApprox();
  std::array<double,4> getPoint();
  std::array<double,4> getPointSpeed();
  void set(std::array<double,4> pt, std::array<double,4> speed);
  void print();
  void setupGeo(GeoSetup setup);
  void setupPhy(PhySetup setup);
  void setupTimeDynamic(TimeSetup setup);
  void step();
  std::array<double,4> getAcc();
  double getEnergy();
private:
  RubanDynOneSegmentApproxTige fun;
};

void initOneSegmentTigeApprox(sol::state &lua);

#endif // ONESEGMENTTIGEAPPROX_H
