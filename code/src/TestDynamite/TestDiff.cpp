/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include "TestDiff.hpp"
#include <PartSymbolic.hpp>
//#include <PartTest.hpp>

MovingRibbon TestDiff::genMR()
{
  MovingRibbon mr;
  mr.setPhysicalParams(1., 10., 0.5);
  int nbSegments = 10;
  mr.setNbSegments(nbSegments);
  mr.vpointb()=1.;
  mr.vpointm()=1.;
  mr.vpointbdot()=-1.;
  mr.vpointmdot()=-1.;
  mr.vpointa(0)=1.;
  mr.vpointn(0)=1.;
  mr.vpointadot(0)=1.;
  mr.vpointndot(0)=1.;
  for(int i=0; i<nbSegments; i++){
    mr.segmentLength(i)=0.1;
    mr.vpointa(i)=-0.1;
    mr.vpointn(i)=-0.1;
    mr.vpointadot(i)=0.1;
    mr.vpointndot(i)=0.1;
  }
  return mr;
}

DynSegment TestDiff::genDynSeg()
{
  DynSegment part;
  part.areaDensity=10.;
  part.width=0.1;
  part.D=1.2;
  part.G = Eigen::Vector3d(0.,0.,9.81);
  //part.G.setZero();
  
  part.r.setZero();
  part.R.setIdentity();
  part.a=1;
  part.adot=1;
  part.b=1;
  part.bdot=1;
  part.n=1;
  part.ndot=1;
  part.m=1;
  part.mdot=1;
  part.length=0.05;
  return part;
}


bool TestDiff::testGradL()
{
  //FunctionPartApproxLogAndEc part;
  DynSegment part = genDynSeg();
  
  
  std::function<Eigen::VectorXr()> getSystemPoint=[&part]()->Eigen::VectorXr
    {Eigen::VectorXr r(4); 
      r<< part.a, part.b, part.n, part.m;
      return r; };
  std::function<real(const Eigen::VectorXr &)> getEnergyValueAt=[&part](const Eigen::VectorXr& pt)->real
  {
    part.a=pt[0];
    part.b=pt[1];
    part.n=pt[2];
    part.m=pt[3];
    part();
    return part.L;
  };
  
  std::function<Eigen::VectorXr(const Eigen::VectorXr &)> getEnergyGradientAt=[&part](const Eigen::VectorXr & pt)->Eigen::VectorXr
  {
    part.a=pt[0];
    part.b=pt[1];
    part.n=pt[2];
    part.m=pt[3];
    part();
    return part.gradL;
  };
  
  FiniteDifferencesTest test(getSystemPoint,getEnergyValueAt,getEnergyGradientAt);
  test.setParams(1.e-6,1.e-5);
  test.run("gradL");
  test.setParams(1.e-5,1.e-5);
  test.run("gradL");
  test.setParams(1.e-4,1.e-5);
  return test.run("gradL");
}

bool TestDiff::testd2Ecdqdot2()
{
  DynSegment part = genDynSeg();
  
  
  std::function<Eigen::VectorXr()> getSystemPoint=[&part]()->Eigen::VectorXr
    {Eigen::VectorXr r(4); 
      r<< part.adot, part.bdot, part.ndot, part.mdot;
      return r; };
  std::function<real(const Eigen::VectorXr &)> getEnergyValueAt=[&part](const Eigen::VectorXr& pt)->real
  {
    part.adot=pt[0];
    part.bdot=pt[1];
    part.ndot=pt[2];
    part.mdot=pt[3];
    part();
    return part.Ec;
  };
  
  std::function<Eigen::MatrixXr(const Eigen::VectorXr &)> getEnergyHessianAt=[&part](const Eigen::VectorXr & pt)->Eigen::MatrixXr
  {
    part.adot=pt[0];
    part.bdot=pt[1];
    part.ndot=pt[2];
    part.mdot=pt[3];
    part();
    return part.d2Ecdqdot2;
  };
  
  FiniteDifferencesTestSecondOrder test(getSystemPoint,getEnergyValueAt,getEnergyHessianAt);
  test.setParams(1.e-6,1.e-5);
  test.run("d2Ecdqdot2");
  test.setParams(1.e-5,1.e-5);
  test.run("d2Ecdqdot2");
  test.setParams(1.e-4,1.e-5);
  return test.run("d2Ecdqdot2");
}
