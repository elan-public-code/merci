/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#ifndef TESTDIFF_H
#define TESTDIFF_H

#include <Algos/FiniteDifferencesTest.hpp>
#include <MovingRibbon.hpp>
#include <DynSegment.hpp>

class TestDiff
{
public:
  static MovingRibbon genMR();
  static DynSegment genDynSeg();
  static bool testGradL();
  static bool testd2Ecdqdot2();
};

#endif // TESTDIFF_H
