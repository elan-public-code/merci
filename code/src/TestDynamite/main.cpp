/********************************************************
This file is licensed with the CeCILL V2.1 license
The license can be found in the root directory or online
at http://www.cecill.info in French and English version. 

Copyright:
2018-2021 Université Grenoble Alpes
2017-2018 ENS Lyon
2017-2021 INRIA
Author:
Raphaël Charrondière
********************************************************/
#include <iostream>
#include <fstream>
#include <SuperRibbonGeometrieSerie.hpp>
#include "TestDiff.hpp"
#include "DynamiteExperiment.hpp"
#include "OneSegmentExperiment.hpp"
#include "OneSegmentTigeApprox.hpp"
#include "OneSegmentSuperClothoid.hpp"
#include "FibreRomain.hpp"

double estimSMax(double vA, double vB, double vN, double vM)
{
  double T_2 = (52. / 2.) * std::log(2) / 2.;
  double l0 = std::max(abs(vB), abs(vM * vB));
  double l1 = std::max(abs(vA), abs(vM * vA + vN * vB));
  double l2 = abs(vN * vA);
  l0 = std::max(static_cast<double>(1.), l0);
  l1 = std::max(static_cast<double>(1.), l1);
  l2 = std::max(static_cast<double>(1.), l2);

  if (std::isnan(l0) || std::isnan(l1) || std::isnan(l2)) {
    return std::numeric_limits<double>::infinity();
  }

  double res = std::numeric_limits<double>::infinity();
  if (l0 + l1 + l2 < T_2) { //Smax>1
    res = std::cbrt(T_2 / (l0 + l1 + l2));
  } else { //Smax<1
    res = T_2 / (l0 + l1 + l2);
  }

  if (res < 1.e-8) {
    res = std::numeric_limits<double>::infinity();
  }
  return res / 2.;
}

void testEnd()
{
  DynSegment part;
  part.areaDensity=10.;
  part.width=0.1;
  part.D=1.;
  part.G = Eigen::Vector3d(1.,1.,0.);
  
  part.r.setZero();
  part.R.setIdentity();
  part.a=1;
  part.adot=0.;
  part.b=1;
  part.bdot=0.;
  part.n=1;
  part.ndot=0.;
  part.m=1;
  part.mdot=0.;
  part.length=0.2;
  part();
  SuperRibbonGeometrieSerie serie;
  serie.Place(part.r,part.R);
  serie.Setup(LinearScalar(part.a,part.b),LinearScalar(part.n,part.m));
  using std::cout;
  using std::endl;
  cout<<(estimSMax(part.a,part.b,part.n,part.m)>part.length?"OK":"Too long segment")<<endl;
  auto sp = serie.Pos(part.length);
  auto sf = serie.Frame(part.length);
  cout<<"Pos,frame: "<<part.endPos.transpose()<<endl;
  cout<<part.endFrame<<endl;
  cout<<"Reference:"<<endl;
  cout<<"Pos,frame: "<<sp.transpose()<<endl;
  cout<<sf<<endl;
  
  cout<<endl<<"d2Ecdqdot2"<<endl;
  cout<<part.d2Ecdqdot2<<endl;
  cout<<endl<<"dposda/b/n/m"<<endl;
  cout<<part.dPosda.transpose()<<endl;
  cout<<part.dPosdb.transpose()<<endl;
  cout<<part.dPosdn.transpose()<<endl;
  cout<<part.dPosdm.transpose()<<endl;
  cout<<"Reference:"<<endl;
  cout<<serie.dPosda(part.length).transpose()<<endl;
  cout<<serie.dPosdb(part.length).transpose()<<endl;
  cout<<serie.dPosdn(part.length).transpose()<<endl;
  cout<<serie.dPosdm(part.length).transpose()<<endl;
  
  cout<<"With ribbon"<<endl;
  MovingRibbon mr;
  mr.setPhysicalParams(part.D, part.areaDensity,part.nu);
  mr.setNbSegments(1);
  mr.vpointb()=part.b;
  mr.vpointm()=part.m;
  mr.vpointbdot()=part.bdot;
  mr.vpointmdot()=part.mdot;
  mr.vpointa(0)=part.a;
  mr.vpointn(0)=part.n;
  mr.vpointadot(0)=part.adot;
  mr.vpointndot(0)=part.ndot;
  mr.segmentLength(0)=part.length;
  mr.setCrossSection({part.width,part.width/1000});
  SystemDynamite sys(mr,part.G);
  sys();
}

int main(int argc, char** argv)
{
  sol::state lua;
  initOneSegmentClass(lua);
  initOneSegmentTigeApprox(lua);
  initOneSegmentSCloto(lua);
  initRomainFibreClass(lua);
  lua.open_libraries(sol::lib::base, sol::lib::math, sol::lib::io, sol::lib::table, sol::lib::string);
  for(int i=1;i<argc;i++)
    lua.script_file(argv[i]);
  

  return 0;
//   testEnd();
//   TestDiff::testGradL();
//   TestDiff::testd2Ecdqdot2();
//   return 0;
  std::ofstream os("out.txt");
  DynamiteExperiment exp(os);
  
  for(int i=0;i<30*5;i++)
  {
    std::cout<<"====================================="<<std::endl;
    exp.step();
    std::cout<<i<<"/1000 E="<<exp.getE()<<std::endl;
    //std::cout<<'\r'<<i<<"/1000 E="<<exp.getE()<<std::flush;
  }
  std::cout<<"====================================="<<std::endl;
  std::cout<<'\r'<<1000<<"/1000 E="<<exp.getE()<<std::endl;
  return 0;
}
