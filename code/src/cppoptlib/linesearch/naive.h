#ifndef NAIVE_H
#define NAIVE_H

#include "../meta.h"

namespace cppoptlib {


  template<typename ProblemType, int Ord>
    class NaiveLineSearch {

  public:
    using Scalar = typename ProblemType::Scalar;
    using TVector = typename ProblemType::TVector;

    static Scalar linesearch(const TVector &x, const TVector &searchDir,
                             ProblemType &objFunc,
                             const Scalar tInit = 1.,
                             const Scalar tMin = 1.e-15,
                             const Scalar rho = 0.5) {

      const Scalar val0 = objFunc.value(x);
      Scalar t = tInit;

      TVector xTest;
      Scalar valTest;
      
      do {

        xTest = x + t * searchDir;
        valTest = objFunc.value(xTest);

        if (valTest <= val0) {
          return t;
        }

        t *= rho;
        
      } while (t >= tMin);
      
      return 0.;

    }
    
  };







} // namespace cppoptlib


#endif // NAIVE_H
