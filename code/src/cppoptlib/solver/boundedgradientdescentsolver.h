// CppNumericalSolver
#ifndef BOUNDEDGRADIENTDESCENTSOLVER_H_
#define BOUNDEDGRADIENTDESCENTSOLVER_H_

#include <Eigen/Core>
#include "isolver.h"
#include "../linesearch/morethuente.h"
#include "../../Log/Logger.hpp"

namespace cppoptlib {

template<typename ProblemType>
class BoundedGradientDescentSolver : public ISolver<ProblemType, 1> {

public:
  using Superclass = ISolver<ProblemType, 1>;
  using typename Superclass::Scalar;
  using typename Superclass::TVector;
  
private:
  Scalar MaxStepSize(const ProblemType &problem, TVector &x, TVector dir)
  {
    TVector ub = problem.upperBound();
    TVector lb = problem.lowerBound();
    int t = dir.size();
    assert(ub.size() == x.size());
    assert(lb.size() == x.size());
    Scalar res = 1.e15;
    Scalar infty=std::numeric_limits<Scalar>::infinity();
    for(int i=0;i<t;i++)
    {
      if(dir[i]==0.)continue;
      if(dir[i]>0.&&ub[i]==infty)continue;
      if(dir[i]<0.&&lb[i]==-infty)continue;
      if(dir[i]>0.)res = std::min(res,(ub[i]-x[i])/dir[i]);
      else if(dir[i]<0.)res = std::min(res,(lb[i]-x[i])/dir[i]);
    }
    return res;
  }
  
public:

  /**
   * @brief minimize
   * @details [long description]
   *
   * @param objFunc [description]
   */
  void minimize(ProblemType &objFunc, TVector &x0) {
    {
      LoggerMsg startingMsg("Calling Bounded Gradient Descent");
      std::stringstream pv;
      pv<<x0.transpose();
      std::stringstream ub;
      ub<<objFunc.upperBound().transpose();
      std::stringstream lb;
      lb<<objFunc.lowerBound().transpose();
      startingMsg.addChild("Point value",pv.str());
      startingMsg.addChild("upperBound",ub.str());
      startingMsg.addChild("lowerBound",lb.str());
      getLogger()->Write(LoggerLevel::INFORMATION, "BGD", startingMsg);
    }
    TVector direction(x0.rows());
    this->m_current.reset();
    do {
      Scalar fOr=objFunc.value(x0);
      objFunc.gradient(x0, direction);
      Scalar maxss = MaxStepSize(objFunc,x0,-direction);
      const Scalar rate = MoreThuente<ProblemType, 1>::linesearch(x0, -direction, objFunc, std::min(static_cast<Scalar>(1.0),maxss-static_cast<Scalar>(1.e-8)), maxss);
      x0 = x0 - rate * direction;
      Scalar f=objFunc.value(x0);
      this->m_current.gradNorm = direction.template lpNorm<Eigen::Infinity>();
      this->m_current.fDelta = std::abs(f-fOr);
      // std::cout << "iter: "<<iter<< " f = " <<  objFunc.value(x0) << " ||g||_inf "<<gradNorm  << std::endl;
      ++this->m_current.iterations;
      this->m_status = checkConvergence(this->m_stop, this->m_current);
      {
        LoggerMsg iterMsg("Iteration done");
        std::stringstream ssi;
        std::stringstream ssg;
        std::stringstream ssvd;
        std::stringstream ssv;
        ssi<<this->m_current.iterations << '/' << this->m_stop.iterations;
        ssg<<this->m_current.gradNorm << '>' << this->m_stop.gradNorm;
        ssvd<<this->m_current.fDelta << '>' << this->m_stop.fDelta;
        ssv<<f;
        iterMsg.addChild("Iteration",ssi.str());
        iterMsg.addChild("Gradient",ssg.str());
        iterMsg.addChild("value delta",ssvd.str());
        iterMsg.addChild("value",ssv.str());
        getLogger()->Write(LoggerLevel::INFORMATION, "BGD", iterMsg);
      }
    } while (objFunc.callback(this->m_current, x0) && (this->m_status == Status::Continue));
    {
      std::stringstream sss;
      std::stringstream ssc;
      std::stringstream sscs;
      sss<<this->m_status;
      ssc<<this->m_stop;
      sscs<<this->m_current;
      LoggerMsg stopMsg("Stop");
      stopMsg.addChild("Stop status",sss.str());
      stopMsg.addChild("Stop criteria",ssc.str());
      stopMsg.addChild("Current status",sscs.str());
      getLogger()->Write(LoggerLevel::INFORMATION, "BGD", stopMsg);
    }
  }

};

} /* namespace cppoptlib */

#endif /* GRADIENTDESCENTSOLVER_H_ */
