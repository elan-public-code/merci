// CppNumericalSolver
#include <iostream>
#include <Eigen/LU>
#include "isolver.h"
#include "../linesearch/armijo.h"
#include "../linesearch/morethuente.h"
#include "../linesearch/wolfeheuristic.h"
#include "../../Log/Logger.hpp"

#ifndef BOUNDEDNEWTONDESCENTSOLVER_H_
#define BOUNDEDNEWTONDESCENTSOLVER_H_

namespace cppoptlib {
  
template<typename Scalar_, int Dim_ = Eigen::Dynamic>
class GradientProblem : public Problem<Scalar_,Dim_>{
public:
  static const int Dim = Dim_;
  typedef Scalar_ Scalar;
  using TVector   = Eigen::Matrix<Scalar, Dim, 1>;
  
  GradientProblem(Problem<Scalar,Dim>& pb):base(pb){
  }
  virtual Scalar_ value(const  TVector &x){ 
    TVector grad;
    base.gradient(x, grad);
    return grad.norm();
  }
private:
  Problem<Scalar,Dim>& base;
};


template<typename ProblemType>
class BoundedNewtonDescentSolver : public ISolver<ProblemType, 2> {
  public:
    using Superclass = ISolver<ProblemType, 2>;
    using typename Superclass::Scalar;
    using typename Superclass::TVector;
    using typename Superclass::THessian;

  private:
    Scalar MaxStepSize(const ProblemType &problem, TVector &x, TVector dir)
    {
      TVector ub = problem.upperBound();
      TVector lb = problem.lowerBound();
      int t = dir.size();
      assert(ub.size() == x.size());
      assert(lb.size() == x.size());
      Scalar res = 1.e15;
      Scalar infty=std::numeric_limits<Scalar>::infinity();
      for(int i=0;i<t;i++)
      {
        if(dir[i]==0.)continue;
        if(dir[i]>0.&&ub[i]==infty)continue;
        if(dir[i]<0.&&lb[i]==-infty)continue;
        if(dir[i]>0.)res = std::min(res,(ub[i]-x[i])/dir[i]);
        else if(dir[i]<0.)res = std::min(res,(lb[i]-x[i])/dir[i]);
      }
      return res;
    }
    
    Scalar myLineSearch(ProblemType &objFunc, const TVector &x0, const Scalar& E0,const TVector& gradient, const THessian& hessian,TVector &direction,const Scalar& alpha_max)
    {
      auto quadEval = [&E0,&gradient,&hessian](const TVector& dir)->Scalar
        {return E0+gradient.dot(dir)+Scalar(0.5)*dir.transpose()*hessian*dir;};
      auto quadErr=[&quadEval,&objFunc,&E0](const TVector& x0, const TVector& dir)->Scalar
        {
          Scalar E = objFunc.value(x0+dir);
          Scalar Es = quadEval(dir);
          return std::abs((E-Es)/(E-E0+1.e-32));
        };
        Scalar delta = 1.e-10;
        Scalar alphaTol=std::min(alpha_max,Scalar(1.0));
        const Scalar errTol=0.1;
        while(delta<alphaTol)
        {
          TVector ddir=delta*direction;
          if(quadErr(x0,ddir)>static_cast<Scalar>(errTol))
          {
            return delta/2.;
          }
          delta*=2.0;
        }
        TVector ddir=alphaTol*direction;
        if(quadErr(x0,ddir)>static_cast<Scalar>(errTol))
        {
            return delta/2.;
        }
        return alphaTol;
    }
    
    std::string errorSecondOrder(ProblemType &objFunc, TVector &x0, TVector &direction)
    {
      std::stringstream ss;
      Scalar delta=0.5;
      for(int i=0;i<20;i++)delta*=0.5;
      delta = 1.e-10;
      Scalar deltaM,deltaC;
      int prog;
      Scalar E0=objFunc.value(x0);
      TVector grad; objFunc.gradient(x0, grad);
      THessian hessian; objFunc.hessian(x0, hessian);
      auto quadEval = [&E0,&grad,&hessian](const TVector& dir)->Scalar
        {return E0+grad.dot(dir)+Scalar(0.5)*dir.transpose()*hessian*dir;};
      auto quadErr=[&quadEval,&objFunc,&E0](const TVector& x0, const TVector& dir)->Scalar
        {
          Scalar E = objFunc.value(x0+dir);
          Scalar Es = quadEval(dir);
          return std::abs((E-Es)/(E-E0+1.e-32));
        };
      //ss<<"Error at full step: "<<quadErr(x0,direction);
      ss<<", Second order > 1‰ ";
      for(prog=0;prog<40;prog++)
      {
        TVector ddir=delta*direction;
        if(quadErr(x0,ddir)>static_cast<Scalar>(0.001))
        {
          deltaM=delta;
          break;
        }
        delta*=2;
      }
      if(prog>=40)
      {
        ss<<"over big value";
        return ss.str();
      }
      ss<<"at delta~"<<deltaM<<" (dirnorm="<<(deltaM*direction).norm()<<") and > 1% ";
      for(;prog<40;prog++)
      {
        TVector ddir=delta*direction;
        if(quadErr(x0,ddir)>static_cast<Scalar>(0.01))
        {
          deltaC=delta;
          break;
        }
        delta*=2;
      }
      if(prog>=40)
      {
        ss<<"over big value";
        return ss.str();
      }
      ss<<"at delta~"<<deltaC<<"(dirnorm="<<(deltaM*direction).norm()<<")";
      return ss.str();
    }
  public:
    void minimize(ProblemType &objFunc, TVector &x0) {
        GradientProblem objGrad(objFunc);
        //ProblemType& objGradr=static_cast<ProblemType&>(static_cast<typename ProblemType::Superclass&>(objGrad));
        const int DIM = x0.rows();
        TVector grad = TVector::Zero(DIM);
        THessian hessian = THessian::Zero(DIM, DIM);
        this->m_current.reset();
        do {
            objFunc.gradient(x0, grad);
            objFunc.hessian(x0, hessian);
            //hessian += (1e-10) * THessian::Identity(DIM, DIM);
            //Eigen::FullPivLU<THessian> solver(hessian);
            Eigen::FullPivHouseholderQR<THessian> solver(hessian);
            TVector delta_x = solver.solve(-grad);
            int dok=solver.dimensionOfKernel();
            TVector solError=hessian*delta_x+grad;
            Scalar errlpMax=solError.template lpNorm<Eigen::Infinity>();
            Scalar convexity = delta_x.dot(hessian*delta_x);
            Scalar normD=delta_x.norm();
            Scalar nconv = convexity/(normD*normD);
            //if(convexity<0.)delta_x*=-1.;
            if(convexity<0.)delta_x=-grad;
            Scalar alpha_max = MaxStepSize(objFunc, x0, delta_x); 
            alpha_max=alpha_max*static_cast<Scalar>(1.-1.e-8);
            Scalar alpha_init = std::min(static_cast<Scalar>(1.0),alpha_max);
            const Scalar rate = MoreThuente<ProblemType, 1>::linesearch(x0,  delta_x ,  objFunc, alpha_init, alpha_max);
            //const Scalar rate=alpha_init;
            
            //const Scalar rate = Armijo<ProblemType, 1>::linesearch(x0,  delta_x ,  objFunc, alpha_init);
            //const Scalar rate = WolfeHeuristic<double, ProblemType, 1>::linesearch(x0,  delta_x ,  objFunc, alpha_init, alpha_max);
            Scalar Eor = objFunc.value(x0);
            //const Scalar rate = myLineSearch(objFunc, x0, Eor, grad, hessian, delta_x, alpha_max);
            Scalar E = objFunc.value(x0 + rate * delta_x);
            //if((rate>alpha_max)||(Eor<E)||isnan(E)){
            if((rate>alpha_max)||std::isnan(E)){
              LoggerMsg msg("NewtonStep line search failed");
              std::stringstream funVal;
              funVal<<Eor<<" => "<<E;
              msg.addChild("Objective",funVal.str());
              std::stringstream lsrate;
              lsrate<<rate; 
              if(rate==alpha_max)
                lsrate<<" (max)";
              msg.addChild("Line search",lsrate.str());
              std::stringstream grNm;
              grNm<<this->m_current.gradNorm<<", "<<hessian.norm();
              msg.addChild("Gradient, hessian norm",grNm.str());
              std::stringstream conv;
              conv<<convexity;
              msg.addChild("convex",conv.str());
              std::stringstream sysErr;
              sysErr<<errlpMax<<", dimension of kernel "<<dok;
              msg.addChild("Solver error",sysErr.str());
              
              /*std::string quadErr=errorSecondOrder(objFunc,x0,delta_x);
              msg.addChild("Quadratic error",quadErr);*/
              getLogger()->Write(LoggerLevel::WARNING,"Bounded Newton Gradient Descent",msg);
              std::cout<<"x0: "<<x0.transpose()<<std::endl;
              std::cout<<"delta_x: "<<delta_x.transpose()<<std::endl;
              std::cout<<"hessian: "<<std::endl<<hessian<<std::endl;
              break;
            }
            TVector rdir=delta_x;
            rdir.setRandom();
            //std::string quadErr=errorSecondOrder(objFunc,x0,delta_x);//rdir);//delta_x);
            x0 = x0 + rate * delta_x;
            ++this->m_current.iterations;
            this->m_current.gradNorm = grad.template lpNorm<Eigen::Infinity>();
            this->m_current.fDelta=Eor-E;
            this->m_status = checkConvergence(this->m_stop, this->m_current);
            LoggerMsg msg("NewtonStep");
            msg.addChild("Iteration",std::to_string(this->m_current.iterations));
            std::stringstream funVal;
            funVal<<Eor<<" => "<<E;
            msg.addChild("Objective",funVal.str());
            std::stringstream deltaVal;
            deltaVal<<Eor-E;
            msg.addChild("Delta",deltaVal.str());
            std::stringstream grNm;
            grNm<<this->m_current.gradNorm<<", "<<hessian.norm();
              msg.addChild("Gradient, hessian norm",grNm.str());
              msg.addChild("d'Hd",convexity);
              msg.addChild("convex",nconv);
            std::stringstream lsrate;
            lsrate<<rate; 
            if(rate==alpha_max)
              lsrate<<" (max)";
            msg.addChild("Line search",lsrate.str());
            //msg.addChild("Quadratic error",quadErr);
            std::stringstream sysErr;
            sysErr<<errlpMax<<", dimension of kernel "<<dok;
            msg.addChild("Solver error",sysErr.str());
            getLogger()->Write(LoggerLevel::INFORMATION,"Bounded Newton Gradient Descent",msg);
        } while (objFunc.callback(this->m_current, x0) && (this->m_status == Status::Continue));
    }
};

}
/* namespace cppoptlib */

#endif /* BOUNDEDNEWTONDESCENTSOLVER_H_ */
