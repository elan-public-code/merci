// CppNumericalSolver
#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/Eigenvalues>
#include <Eigen/SVD>

#include "isolver.h"
#include "../linesearch/armijo.h"
#include "../linesearch/wolfeheuristic.h"
#include "../linesearch/morethuente.h"
#include "../linesearch/naive.h"

#ifndef CG_SOLVER_H_
#define CG_SOLVER_H_


// P preconditioner
// M = P^T P; M^-1 used
template <bool usePreconditioner = false,
          bool useTrustRegion = false,
          class PrecoType = Eigen::MatrixXd>
unsigned int cgNewtonSolver(const Eigen::MatrixXd &lhs,
                            Eigen::VectorXd &x,
                            const Eigen::VectorXd &rhs,
                            unsigned int maxIter,
                            const PrecoType *PPtr = NULL,
                            const PrecoType *invMPtr = NULL,
                            const double trustRadius = 0.,
                            bool *trustIsMaxStep = NULL) {
 
  const unsigned int n = rhs.rows();
 
  Eigen::VectorXd r(n);
  Eigen::VectorXd p(n);
  Eigen::VectorXd s(n);
  
  Eigen::VectorXd y(n);

  Eigen::VectorXd xPrev(n);
  Eigen::VectorXd xTest(n);

  const double radius2 = trustRadius * trustRadius;

  // Threshold of the residual
  const double tmp =  rhs.squaredNorm();
  const double thr = (std::min)((std::min)(0.5 * 0.5, sqrt(tmp)) * tmp, 1.); // Super linear
  //const double thr = (std::min)(0.5 * 0.5, tmp) * tmp; // Quadratic

  // Residual vector
  r = rhs - lhs * x;

  if (usePreconditioner) {
    y = (*invMPtr) * r;
    p = y;
  } else {
    p = r;
  }

  double res = r.squaredNorm();
  double delta;
  if (usePreconditioner) {
    delta = r.dot(y);
  } else {
    delta = res;
  }
  unsigned int k = 0u;
  for (; (res > thr) && (k < maxIter); ++k) {

    s = lhs * p;

    // Curvature check
    const double curvature = p.dot(s);
    const double alpha = delta / curvature;

    // Negative curvature - Stop
    if (curvature < 0. ) {
      if (useTrustRegion) {
        // Steihaug CG (Trust-region Newton) :
        // alpha / || x += tau * alpha || = radius and min m
        // Preconditioned CG : norm of || P x ||
        xTest = (usePreconditioner) ? (*PPtr) * x : x;
        const Eigen::VectorXd pTest =
          (usePreconditioner) ? (*PPtr) * p : p;
        const double xNorm2 = xTest.squaredNorm();
        const double xdotP = xTest.dot(pTest);
        const double pNorm2 = pTest.squaredNorm();
        const double alphaP =
          (- xdotP + sqrt(xdotP * xdotP - pNorm2 * (xNorm2 - radius2)))
          / pNorm2;
        const double alphaM =
          (- xdotP - sqrt(xdotP * xdotP - pNorm2 * (xNorm2 - radius2)))
          / pNorm2;
        const Eigen::VectorXd xP = x + alphaP * p;
        const Eigen::VectorXd xM = x + alphaM * p;
        // m(x) = (fx) + g.x + xT H x
        // lhs = H
        // rhs = -g
        const double mP = -(xP.dot(rhs)) + 0.5 * (xP.dot(lhs * xP));
        const double mM = -(xM.dot(rhs)) + 0.5 * (xM.dot(lhs * xM));
        if (mP < mM) {
          x = xP;
        } else {
          x = xM;
        }
        *trustIsMaxStep = true;
      } else {
        if (k == 0) {
          // CG / PCG : gradient by default
          if (usePreconditioner) {
            // ??
            x += alpha * r;
          } else {
            x += alpha * p;
          }
        }
      }
      std::cout << "(Newton) Curvature : " << curvature << std::endl;
      break;
    }
    
    // x update
    if (useTrustRegion) {
      xPrev = x;
    }
    x += alpha * p;
    if (useTrustRegion) {
      // x out of the trust region ?
      xTest = (usePreconditioner) ? (*PPtr) * x : x;
      if (xTest.squaredNorm() >= radius2) {
        std::cout << "(Newton) Outside of trust region" << std::endl;
        // correcting so as ||x|| = radius
        xTest = (usePreconditioner) ? (*PPtr) * xPrev : xPrev;
        const Eigen::VectorXd pTest =
          (usePreconditioner) ? (*PPtr) * p : p;
        const double xNorm2 = xTest.squaredNorm();
        const double xdotP = xTest.dot(pTest);
        const double pNorm2 = pTest.squaredNorm();
        const double alphaP =
          (- xdotP + sqrt(xdotP * xdotP - pNorm2 * (xNorm2 - radius2)))
          / pNorm2;
        x = xPrev + alphaP * p;
        *trustIsMaxStep = true;
        break;
      }
    } 
    
    r -= alpha * s;

    if (usePreconditioner) {
      y = (*invMPtr) * r;
    }
    
    res = r.squaredNorm();
    const double deltaPrev = delta;
    if (usePreconditioner) {
      delta = r.dot(y);
    } else {
      delta = res;
    }
    const double beta = delta / deltaPrev;

    if (usePreconditioner) {
      p = y + beta * p;
    } else {
      p = r + beta * p;
    }
  }

  std::cout << "(Newton) Delta : " << delta << " vs " << thr << std::endl;
  return k;
  
}



namespace cppoptlib {

enum class NewtonType {
  Naive,
  CG,
  PreconditionedCG,
  TrustRegionPrecoCG,
  Regularized,
  CGReg
};

 template<typename ProblemType,
   NewtonType type = NewtonType::Naive,
   int LineSearchSetup = 2>
class MickaelNewtonDescentSolver : public ISolver<ProblemType, 2> {
  public:
    using Superclass = ISolver<ProblemType, 2>;
    using typename Superclass::Scalar;
    using typename Superclass::TVector;
    using typename Superclass::THessian;

    size_t DIM;
    TVector x_old;
    TVector grad;
    //THessian hessian;
    
    TVector direction;
    Scalar rate;

    // CG, PreconditionedCG, TrustRegionPrecoCG
    unsigned int cgMaxIter;

    // Regularized
    Scalar reg;
    Scalar regMin;

    // TrustRegionPrecoCG
    Scalar radius;
    Scalar radiusMax;
    Scalar eta1;
    Scalar eta2;
    Scalar eta3;
    Scalar t1;
    Scalar t2;
    Scalar value0;
    TVector grad0;

    // CGReg
    bool switchReg;
  
    inline void init(ProblemType &objFunc, TVector &x0) {
      DIM = x0.rows();
      x_old = x0;
      grad = TVector::Zero(DIM);
      direction = TVector::Zero(DIM);
      rate = 0.;
      this->m_current.reset();

      // CG, PreconditionedCG, TrustRegionPrecoCG
      cgMaxIter = DIM * ((unsigned int)log(DIM));

      // Regulatized
      reg = 1.;
      regMin = 1.e-15;

      // TrustRegionPrecoCG
      radius = 1.;
      radiusMax = 10.;
      eta1 = 0.; // <= eta2
      eta2 = 0.25; // <= eta3
      eta3 = 0.75;
      t1 = 0.5;
      t2 = 4.;

      // CGReg
      switchReg = false;
      if (type == NewtonType::CGReg) {
        cgMaxIter = DIM / 2.;
      }
    }


    template <bool usePreconditioner = false,
              class PrecoType = Eigen::MatrixXd>
    inline bool updateTrustRegion(ProblemType &objFunc, TVector &x0,
                                  const bool isMaxStep,
                                  const Scalar val,
                                  const TVector &grad,
                                  const THessian &hess,
                                  const PrecoType *precoPtr = NULL) {


      /// Evaluate rho
      
      // Used after a CG iteration
      // objFunc(x0) still cached

      const Scalar f0 = val;
      const Scalar m0 = val; 
      const Scalar mp = val + grad.dot(direction) + 0.5 * direction.dot(hess * direction);
      // Reevaluate
      const TVector xp = x0 + direction;
      const Scalar fp = objFunc.value(xp);


      Scalar rho = (f0 - fp) / (m0 - mp);
      std::cout << "(Newton) rho " << rho << std::endl;
      
      /// Update radius
      const Scalar stepSize =
      (usePreconditioner) ?
      ((*precoPtr) * direction).norm() :
      direction.norm();
      
      if (rho < eta2) {
        radius = t1 * stepSize;
      } else {
        if ((rho > eta3) && isMaxStep) {
          radius = (std::min)(t2 * radius, radiusMax);
        }          
      }
      std::cout << "(Newton) Radius : " << radius << std::endl;
      
      // Accept step ?
      if (rho <= eta1) {
        direction.setZero();
        return false;
      }
      return true;
      
    }

    inline bool preUpdate(ProblemType &objFunc, TVector &x0) {
      objFunc.gradient(x0, grad);

      this->m_current.gradNorm = grad.template lpNorm<Eigen::Infinity>();
      this->m_status = checkConvergence(this->m_stop, this->m_current);
      if (this->m_status != Status::Continue) {
        return false;
      }
      
      THessian hessian;
      objFunc.hessian(x0, hessian);
      
      /// Optimal but costly regularizer
      /*
      Eigen::EigenSolver<THessian> es(hessian, false);
      Scalar minlambda = es.eigenvalues().real().minCoeff();
      Scalar cplx = es.eigenvalues().imag().maxCoeff();
      Scalar error;
      std::cout << "(Newton) Min eigen value  " 
                << minlambda << std::endl;
      std::cout << "(Newton) Complex ?  " 
                << cplx << std::endl;
                
      if (minlambda < 0.) {
        reg = -1.1 * minlambda;
        std::cout << "(Newton) Regularizer " << std::endl;
        THessian regHess = hessian + (reg * THessian::Identity(DIM, DIM));
        direction = regHess.llt().solve(-grad);
        error = (regHess * direction + grad).norm();
      } else {
        std::cout << "(Newton) No regularizer " << std::endl;
        direction = hessian.llt().solve(-grad);
        error = (hessian * direction + grad).norm();
      }
      std::cout << "(Newton) Error " << error << std::endl;
      return true;
      */

      /// Regularizer strategy
      if ((type == NewtonType::Regularized)
          || ((type == NewtonType::CGReg) && switchReg)) {
        
        Scalar error;
        grad *= -1;
  
        const double tmp = grad.squaredNorm();
        //const double thr = (std::min)(0.5 * 0.5 * tmp, 1.); // Linear
        const double thr = (std::min)((std::min)(0.5 * 0.5, sqrt(tmp)) * tmp, 1.); // Super linear

        do {
          if (reg > 0.) {
            std::cout << "(Newton) Regularizer " << reg << std::endl;
            THessian toto = hessian + reg * THessian::Identity(DIM, DIM);
            direction = toto.llt().solve(grad);
            error = (toto * direction - grad).squaredNorm();
          } else {
            std::cout << "(Newton) No regularizer" << std::endl;
            direction = hessian.llt().solve(grad);
            error = (hessian * direction - grad).squaredNorm();
          }
          
          std::cout << "(Newton) Error : " << error << "  vs thr : " << thr << std::endl;
          if (error < thr) {
            break;
          }
          
          if (reg < regMin) {
            reg = regMin;
          } else {
            reg *= 2.;
          }
        } while (true);
        
        reg /= 2.;
        if (reg < regMin) {
          reg = 0.;
        }
        
      }        

      /// Newton CG
      else if ((type == NewtonType::CG)
               || (type == NewtonType::PreconditionedCG)
               || (type == NewtonType::TrustRegionPrecoCG)
               || ((type == NewtonType::CGReg) || (!switchReg))) {
        grad *= -1;
        direction.setZero();
        unsigned int k;
        if ((type == NewtonType::CG)
            || (type == NewtonType::CGReg)) {
          k = cgNewtonSolver<>(hessian, direction, grad, cgMaxIter);
          if ((type == NewtonType::CGReg) && (k >= cgMaxIter)) {
            switchReg = true;
          }
        } else if (type == NewtonType::PreconditionedCG) {
//           const bool usePreco = true;
//           k = cgNewtonSolver<usePreco>(hessian, direction, grad, cgMaxIter,
//                                        objFunc.getPrecoPtr(),
//                                        objFunc.getInvPrecoSquarePtr());
          throw "Crash volontaire";
        } else if (type == NewtonType::TrustRegionPrecoCG) {
          // Saving data for the trust region upadte
          value0 = objFunc.value(x0);
          grad0 = -grad;
          // No need to save hessian
          throw "Crash volontaire";
        }
        std::cout << "(Newton) CG " << k << std::endl;
        std::cout << "(Newton) Descent ? " << direction.dot(grad) / (grad.norm() * direction.norm()) << std::endl;
        std::cout << "(Newton) Norm " << direction.norm() << std::endl;
      }

      /// Naive
      else if (type == NewtonType::Naive) {
        direction = hessian.llt().solve(-grad);
        Scalar error = (hessian * direction + grad).norm();
        std::cout << "(Newton) Error " << error << std::endl;
      }

      
      return true;
    }
    
    inline void linesearch(ProblemType &objFunc, TVector &x0) {
      if (type != NewtonType::TrustRegionPrecoCG) {
        rate = MoreThuente<ProblemType, 2>::linesearch(x0, direction, objFunc);
        std::cout << "(Newton) More-Thuente - step : " << rate << std::endl;
      } else {
        rate = 1.;
      }
    }
    
    inline void update(ProblemType &objFunc, TVector &x0) {
      x0 += rate * direction;
    }
    
    inline void postUpdate(ProblemType &objFunc, TVector &x0) {
      this->m_current.xDelta = (x_old-x0).template lpNorm<Eigen::Infinity>();
      std::cout << "(Newton) Move : " <<  this->m_current.xDelta  << std::endl;
      std::cout << "(Newton) ------------- " << std::endl;
      x_old = x0;
      ++this->m_current.iterations;
    }
      
  void minimize(ProblemType &objFunc, TVector & x0) {

    init(objFunc, x0);
    
    do {
      if (!(preUpdate(objFunc, x0))) {
        break;
      }
      linesearch(objFunc, x0);
      update(objFunc, x0);
      postUpdate(objFunc, x0);
    } while (objFunc.callback(this->m_current, x0));// && (this->m_status == Status::Continue));

    std::cout << "Newton - Exit : " << this->m_status << std::endl;
    this->m_current.print(std::cout);
  }
};

}
/* namespace cppoptlib */

#endif /* NEWTONDESCENTSOLVER_H_ */
