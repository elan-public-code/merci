// CppNumericalSolver
#include <iostream>
#include <Eigen/LU>
#include "isolver.h"
#include "../linesearch/armijo.h"
#include "../linesearch/morethuente.h"
#include "../../Log/Logger.hpp"

#ifndef NEWTONDESCENTSOLVER_H_
#define NEWTONDESCENTSOLVER_H_

namespace cppoptlib {

template<typename ProblemType>
class NewtonDescentSolver : public ISolver<ProblemType, 2> {
  public:
    using Superclass = ISolver<ProblemType, 2>;
    using typename Superclass::Scalar;
    using typename Superclass::TVector;
    using typename Superclass::THessian;

    void minimize(ProblemType &objFunc, TVector &x0) {
        const int DIM = x0.rows();
        TVector grad = TVector::Zero(DIM);
        THessian hessian = THessian::Zero(DIM, DIM);
        this->m_current.reset();
        do {
            objFunc.gradient(x0, grad);
            objFunc.hessian(x0, hessian);
            //hessian += (1e-5) * THessian::Identity(DIM, DIM);
            
            //TVector delta_x = hessian.lu().solve(-grad);
            Eigen::FullPivHouseholderQR<THessian> solver(hessian);
            TVector delta_x = solver.solve(-grad);
            int dok=solver.dimensionOfKernel();
            
            //const double rate = MoreThuente<ProblemType, 1>::linesearch(x0, delta_x, objFunc) ;
            const double rate = 1.0;
            x0 = x0 + rate * delta_x;
            ++this->m_current.iterations;
            this->m_current.gradNorm = grad.template lpNorm<Eigen::Infinity>();
            this->m_status = checkConvergence(this->m_stop, this->m_current);
            LoggerMsg msg("Newton Step");
            msg.addChild("Iteration",std::to_string(this->m_current.iterations));
            std::stringstream funVal;
            funVal<<objFunc.value(x0);
            msg.addChild("Objective",funVal.str());
            std::stringstream grNm;
            grNm<<this->m_current.gradNorm;
            msg.addChild("Gradient norm",grNm.str());
            std::stringstream lsrate;
            lsrate<<rate; 
            msg.addChild("Line search",lsrate.str());
            msg.addChild("Dimension of kernel",std::to_string(dok));
            getLogger()->Write(LoggerLevel::INFORMATION,"Newton",msg);
            if(rate<1.e-24)break;
        } while (objFunc.callback(this->m_current, x0) && (this->m_status == Status::Continue));
    }
};

}
/* namespace cppoptlib */

#endif /* NEWTONDESCENTSOLVER_H_ */
