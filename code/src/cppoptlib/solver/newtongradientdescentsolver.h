// CppNumericalSolver
#include <iostream>
#include <Eigen/LU>
#include "isolver.h"
#include "../linesearch/armijo.h"
#include "../linesearch/morethuente.h"
#include "../linesearch/wolfeheuristic.h"
#include "../../Log/Logger.hpp"

#ifndef NEWTONGRADIENTDESCENTSOLVER_H_
#define NEWTONGRADIENTDESCENTSOLVER_H_

namespace cppoptlib {
template<typename ProblemType>
class NewtonGradientDescentSolver : public ISolver<ProblemType, 2> {
  public:
    using Superclass = ISolver<ProblemType, 2>;
    using typename Superclass::Scalar;
    using typename Superclass::TVector;
    using typename Superclass::THessian;

  private:
    
  public:
    void minimize(ProblemType &objFunc, TVector &x0) {
        const int DIM = x0.rows();
        TVector grad = TVector::Zero(DIM);
        THessian hessian = THessian::Zero(DIM, DIM);
        this->m_current.reset();
        do {
            objFunc.gradient(x0, grad);
            objFunc.hessian(x0, hessian);
            Eigen::FullPivHouseholderQR<THessian> solver(hessian);
            TVector delta_x = solver.solve(-grad);
            int dok=solver.dimensionOfKernel();
            TVector solError=hessian*delta_x+grad;
            Scalar errlpMax=solError.template lpNorm<Eigen::Infinity>();
            Scalar convexity = delta_x.dot(hessian*delta_x);
            //if(convexity<0.)delta_x*=-1.;
            if(convexity<0.)delta_x=-grad;
            //const Scalar rate = WolfeHeuristic<real, ProblemType, 1>::linesearch(x0,  delta_x ,  objFunc);
            const Scalar rate = Armijo<ProblemType, 1>::linesearch(x0,  delta_x ,  objFunc);
            Scalar Eor = objFunc.value(x0);
            Scalar E = objFunc.value(x0 + rate * delta_x);
            //if((rate>alpha_max)||(Eor<E)||isnan(E)){
            if(std::isnan(E)){
              LoggerMsg msg("NewtonStep line search failed");
              std::stringstream funVal;
              funVal<<Eor<<" => "<<E;
              msg.addChild("Objective",funVal.str());
              std::stringstream lsrate;
              lsrate<<rate; 
              msg.addChild("Line search",lsrate.str());
              std::stringstream grNm;
              grNm<<this->m_current.gradNorm<<", "<<hessian.norm();
              msg.addChild("Gradient, hessian norm",grNm.str());
              std::stringstream conv;
              conv<<convexity;
              msg.addChild("convex",conv.str());
              std::stringstream sysErr;
              sysErr<<errlpMax<<", dimension of kernel "<<dok;
              msg.addChild("Solver error",sysErr.str());
              
              getLogger()->Write(LoggerLevel::WARNING,"Newton Gradient Descent",msg);
              std::cout<<"x0: "<<x0.transpose()<<std::endl;
              std::cout<<"delta_x: "<<delta_x.transpose()<<std::endl;
              std::cout<<"hessian: "<<std::endl<<hessian<<std::endl;
              break;
            }
            TVector rdir=delta_x;
            rdir.setRandom();
            x0 = x0 + rate * delta_x;
            ++this->m_current.iterations;
            this->m_current.gradNorm = grad.template lpNorm<Eigen::Infinity>();
            this->m_current.fDelta=Eor-E;
            this->m_status = checkConvergence(this->m_stop, this->m_current);
            LoggerMsg msg("NewtonStep");
            msg.addChild("Iteration",std::to_string(this->m_current.iterations));
            std::stringstream funVal;
            funVal<<Eor<<" => "<<E;
            msg.addChild("Objective",funVal.str());
            std::stringstream deltaVal;
            deltaVal<<Eor-E;
            msg.addChild("Delta",deltaVal.str());
            std::stringstream grNm;
            grNm<<this->m_current.gradNorm<<", "<<hessian.norm();
              msg.addChild("Gradient, hessian norm",grNm.str());
              std::stringstream conv;
              conv<<convexity;
              msg.addChild("convex",conv.str());
            std::stringstream lsrate;
            lsrate<<rate; 
            msg.addChild("Line search",lsrate.str());
            //msg.addChild("Quadratic error",quadErr);
            std::stringstream sysErr;
            sysErr<<errlpMax<<", dimension of kernel "<<dok;
            msg.addChild("Solver error",sysErr.str());
            getLogger()->Write(LoggerLevel::INFORMATION,"Newton Gradient Descent",msg);
        } while (objFunc.callback(this->m_current, x0) && (this->m_status == Status::Continue));
    }
};

}
/* namespace cppoptlib */

#endif /* NEWTONGRADIENTDESCENTSOLVER_H_ */
