// CppNumericalSolver
#include <iostream>
#include <Eigen/LU>
#include "isolver.h"
#include "../linesearch/armijo.h"
#include "../linesearch/morethuente.h"
#include "../../Log/Logger.hpp"

#ifndef NEWTONREGULARIZEDDESCENTSOLVER_H_
#define NEWTONREGULARIZEDDESCENTSOLVER_H_

namespace cppoptlib {

template<typename ProblemType>
class NewtonRegularizedDescentSolver : public ISolver<ProblemType, 2> {
  public:
    using Superclass = ISolver<ProblemType, 2>;
    using typename Superclass::Scalar;
    using typename Superclass::TVector;
    using typename Superclass::THessian;
    
    void minimize(ProblemType &objFunc, TVector &x0) {
        const int DIM = x0.rows();
        TVector grad = TVector::Zero(DIM);
        THessian hessian = THessian::Zero(DIM, DIM);
        this->m_current.reset();
        do {
            objFunc.gradient(x0, grad);
            objFunc.hessian(x0, hessian);
            //Eigen::FullPivHouseholderQR<THessian> solver(hessian);
            //Eigen::FullPivLU<THessian> solver(hessian);
            //Eigen::CompleteOrthogonalDecomposition<THessian> solver(hessian);
            //Eigen::JacobiSVD<THessian,Eigen::FullPivHouseholderQRPreconditioner> solver(hessian);
            
            double lambdacond=0.;
            //Step 2 : negatives eigenvalues
            
            Eigen::SelfAdjointEigenSolver<Eigen::MatrixXr> eigensolver(hessian);
            real lambdamin = eigensolver.eigenvalues()[0];
            TVector delta_x;
            if (lambdamin<1.e-8)
            {
              lambdacond=std::abs(lambdamin)*10.0;//+1.e-8;
              hessian += lambdacond * THessian::Identity(DIM, DIM);

            }
            //Step 3 : solve
            
            Eigen::FullPivHouseholderQR<THessian> solver(hessian);
            delta_x = solver.solve(-grad);
            
            //const double rate = MoreThuente<ProblemType, 1>::linesearch(x0, delta_x, objFunc) ;
            const double rate = Armijo<ProblemType, 1>::linesearch(x0, delta_x, objFunc) ;
            //const double rate = 1.0;
            x0 = x0 + rate * delta_x;
            ++this->m_current.iterations;
            this->m_current.gradNorm = grad.template lpNorm<Eigen::Infinity>();
            this->m_status = checkConvergence(this->m_stop, this->m_current);
            LoggerMsg msg("Step");
            msg.addChild("Iteration",std::to_string(this->m_current.iterations));
            msg.addChild("Objective",objFunc.value(x0));
            msg.addChild("Gradient norm",this->m_current.gradNorm);
            msg.addChild("Line search",rate);
            msg.addChild("Dir norm",delta_x.norm());
            msg.addChild("vp min",lambdamin);
            msg.addChild("Regularization",lambdacond);
            /*if( std::abs(lambdamin)<1.e-10)
            {
              auto smsg=msg.addChild("Zero ev","Extra infos");
              smsg->addChild("vp",eigensolver.eigenvalues().transpose());
              real hlp=hessian.template lpNorm<Eigen::Infinity>();
              smsg->addChild("hessian inf",hlp);
            }*/
            getLogger()->Write(LoggerLevel::INFORMATION,"Newton Regularized",msg);
            if(rate<1.e-24)break;
        } while (objFunc.callback(this->m_current, x0) && (this->m_status == Status::Continue));
    }
};

}
/* namespace cppoptlib */

#endif /* NEWTONREGULARIZEDDESCENTSOLVER_H_ */
